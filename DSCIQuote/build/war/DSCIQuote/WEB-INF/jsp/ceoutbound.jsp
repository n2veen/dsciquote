<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ page isELIgnored="false" %>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
        body {font-family: Arial, Helvetica, sans-serif;background-color:#eaeef1; margin:0; padding:0}
        .imgcontainer {
            text-align: left;
            position: relative;
            background-color:#fff;
            width:100%;
            padding:10px ;
            margin : 0;
            
        }
        .imgcontainer img{width:145px}
        p{text-align:center; padding-top:10%}
        </style>
</head>
<body onload="document.forms['outboundForm'].submit()">
<div class="imgcontainer">
    <img src="<%=request.getContextPath()%>/jsp/assets/images/omnia-logo.svg">
</div>
<p>You will be redirected shortly</p>
<form class="modal" action="${ceURL}" method="post" name="outboundForm" id="outboundForm">
<input type="hidden" placeholder="AuthToken" name = "Auth-Token" value = "${authToken}" >
   
</form>
</body>
</html>
