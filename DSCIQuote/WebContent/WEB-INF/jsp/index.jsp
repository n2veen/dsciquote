<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
  <!doctype html>
  <html class="no-js" ng-app="quoteUi">

  <head>
    <meta charset="utf-8">
    <title>quoteUi</title>
    <meta name="description" content="QuoteUI">
    <meta name="author" content="wipro.com">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="icon" type="image/x-icon" href="/DSCIQuote/jsp/favicon.ico" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"> -->
    <link href="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <link rel="stylesheet" href="/DSCIQuote/jsp/styles/vendor.css">
    <link rel="stylesheet" href="/DSCIQuote/jsp/styles/app.css">
    <script src="/DSCIQuote/jsp/scripts/vendor.js"></script>
    <script src="/DSCIQuote/jsp/scripts/app.js"></script>
  </head>

  <body>
    <!--[if lt IE 10]>
      <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <!-- showSpinners=false hides the spinner -->
    <span us-spinner="{radius:30, width:8, length: 16}" spinner-on="$root.showSpinners"></span>
    <!-- <input style="margin-top:200px; width:500px;" type="text" 
                  id="fg"
                  ng-model="name"
                  g-places-autocomplete > -->
    <div ui-view></div>
    <script>
      // This example displays an address form, using the autocomplete feature
      // of the Google Places API to help users fill in the information.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'short_name',
        locality: 'short_name',
        administrative_area_level_1: 'short_name',
        country: 'short_name',
        postal_code: 'short_name'
      };
      //window.onload = function() {
      //  window.setTimeout(initAutocomplete, 500);
      //};

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        var options = {
          types: ['establishment'],
          componentRestrictions: {
            country: "us"
          }
        };
        var input = document.getElementById('businessName_ID');
        // console.log("++++++++",input);

        autocomplete = new google.maps.places.Autocomplete(input, options);
      }
    </script>

    <!-- BEGIN ProvideSupport.com Graphics Chat Button Code -->
    <!-- <div id="ps">
<div id="ciY4uU" style="z-index:100;position:absolute"></div>
<div id="scY4uU" class="chatwith" style="display:inline"></div>
<div id="sdY4uU" class="chatwith" style="display:none"></div>
<script type="text/javascript">
  var seY4uU = document.createElement("script");
  seY4uU.type = "text/javascript";
  var seY4uUs = (location.protocol.indexOf("https") == 0 ? "https" : "http") +
    "://image.providesupport.com/js/1can3j12idg1u11q88et4ph2ni/safe-standard.js?ps_h=Y4uU&ps_t=" + new Date().getTime();
  setTimeout("seY4uU.src=seY4uUs;document.getElementById('sdY4uU').appendChild(seY4uU)", 1)

</script>
<noscript>
  <div style="display:inline">
    <a href="http://www.providesupport.com?messenger=1can3j12idg1u11q88et4ph2ni">Live Chat</a>
  </div>
</noscript> -->
    <!-- END ProvideSupport.com Graphics Chat Button Code -->

    <script src="//maps.googleapis.com/maps/api/js?key=AIzaSyDplLPGy7A8zHHnlo6G4td5cs_oKZHkE6A&libraries=places"></script>
    <!-- <script src="http://jvandemo.github.io/angularjs-google-maps/dist/angularjs-google-maps.js"></script> -->
    <script>
      function psY4uUn() {
        return (new Date()).getTime();
      }
      var psY4uUol = false;

      function psY4uUow() {
        if (psY4uUol || (1 == 1)) {
          var pswo = "menubar=0,location=0,scrollbars=auto,resizable=1,status=0,width=650,height=680";
          var pswn = "pscw_" + psY4uUn();
          var url = "http://messenger.providesupport.com/messenger/1can3j12idg1u11q88et4ph2ni.html?ps_l=" +
            escape(document.location) + "";
          window.open(url, pswn, pswo);
        } else if (1 == 2) {
          document.location = "http://";
        }
      }
    </script>
  </body>

  </html>