<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<HEAD>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META http-equiv="Content-Style-Type" content="text/css">
<%@page import = "com.wipro.dsci.quote.performance.TimingCollection" %>
<%@page import = "com.wipro.dsci.quote.performance.TimingData" %>
<%@page import = "java.util.Map" %>
<%@page import = "java.util.Iterator" %>
<%@page import="java.util.Properties"%>

<TITLE>Performance analysis</TITLE>
</HEAD>

<BODY>

<h1><center>TIME ANALYZER </center>
</h1>

<% TimingCollection timingCollection = (TimingCollection) request.getAttribute("timingData"); %>

<h5 align = "right">Last ResetTime : <%= timingCollection.getFormattedLastResetDateTime() %></h5>

<form name = "frmIndex" method = "POST" >

<% Map timingData =  (Map) timingCollection.getTimingData();
	boolean flag= true;
	Iterator   it = timingData.keySet().iterator();
		if(it.hasNext() == false )	{%>
			 No data available
		<%}
		while(it.hasNext()) {
			String eventType  = (String)it.next();
	        if(eventType != null) {
%>
<table border="1" cellspacing="0" cellpadding="2" align="center">		
<tr>
	<td><b>Event<b></td>
	<td><b>Min Time in ms</b></td>
	<td><b>Max Time in ms</b></td>
	<td><b>No. Of Invocations</b></td>
	<td><b>Avg Time in ms</b></td>
</tr>			
<%
		        Map eventTypeMap = (Map) timingData.get(eventType);
		        if(eventTypeMap!= null) {
		            Iterator iter = eventTypeMap.keySet().iterator();
					flag= true;
		            while(iter.hasNext()) {
		                String event = (String) iter.next();
						
		                TimingData timeData =(TimingData) eventTypeMap.get(event);
		                if(timeData != null) {
		                    if(flag) {
								flag=false;
						%>
						<tr>
							<b>Event Type : <%= eventType %></b>
						</tr>
						<% } %>
						<tr>
							<td><%= event %></td>
								<td><%= timeData.getMinValue() %></td>
								<td><%= timeData.getMaxValue() %></td>
								<td><%= timeData.getNumOfInvocations() %></td>
								<td><%= timeData.getAverage() %></td>
							</tr>
						<% 
		                }
		            }
		        }
		    }  
%></table><br><br><%
	    }
%>



<table border="0" cellspacing="0" cellpadding="2" align="center">
<tr>
	<td align="center">
	<input type="submit" onClick="javascript:clickOnSubmit();" name="submitButtonName" value="Refresh">
	<input type="reset" onClick="javascript:clickOnReset();" name="submitButtonName1" value="Reset"></td>
</tr>

</table>
<input type ="hidden" name ="command"   value=""/>
</form>

</BODY>


<script language="javascript" 	type="text/javascript">

	function clickOnReset(){
		
		document.frmIndex.action='timer';
		document.frmIndex.command.value='reset';
		document.frmIndex.method = 'POST';
		document.frmIndex.submit();
	}


	function clickOnSubmit(){
		document.frmIndex.action='timer';
		document.frmIndex.command.value='refresh';
		document.frmIndex.method = 'POST';
		document.frmIndex.submit();
	}
</script>


</html>