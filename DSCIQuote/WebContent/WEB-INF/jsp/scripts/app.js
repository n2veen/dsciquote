(function() {
  'use strict';

  angular.module('quoteUi', [
    'ngAnimate',
    'ngCookies',
    'ngTouch',
    'ngSanitize',
    'ngMessages',
    'ngAria',
    'ngResource',
    'ui.router',
    'ui.bootstrap',
    'ngUSStates',
    'toastr',
    'angularSpinner',
    'ui.mask',
    'ngMask',
    'ng-currency',
    'google.places',
    'typeahead-focus',
    'ngIdle'
  ]).config(function(IdleProvider, KeepaliveProvider) {
             // IdleProvider.idle(60);
            IdleProvider.idle(7170);
            IdleProvider.timeout(30);
            KeepaliveProvider.interval(10);
        });
})();

(function () {
  'use strict';

  angular
    // underwriting
    .module('quoteUi')
    .controller('UnderwritingController', UnderwritingController)
    .filter('camelCase', function () {
      // Error message label should be camelCase format
      return function (input) {
        input = input || '';
        return input.replace(/\w\S*/g, function (txt) {
          return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
      };
    })
    .directive('asDate', function () {
      return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
          modelCtrl.$formatters.push(function () {
            var transformedInput;
            transformedInput = new Date(attrs.asDate);
            modelCtrl.$setViewValue(transformedInput);
            modelCtrl.$render();
            return transformedInput;
          });
        }
      };
    });

  /** @ngInject */
  function UnderwritingController(
    $http,
    $state,
    $window,
    $rootScope,
    ErrorToast,
    toastr,
    ParseJSON,
    $scope,
    Conf,
    $timeout,
    Validator,
    underwritingService,
    $log,
    $location,
    $interval,
    util
  ) {
    var vm = this;
    // $rootScope.pageInfo = $location.path();
    vm.maxLength;
    vm.appianData = [];
    vm.underwritingForm = {};
    // console.log("building controller");
    vm.message = [];
    vm.dateFormatError = [false, false];
    vm.underwriting = [];
    vm.underwritingQuestions = [];
    vm.policy = [];
    vm.underwritingForm.userValue = [];
    vm.dateDisabled = [false, false];
    vm.policyPremiumBool;
    vm.policyPremiumErrorMesg;
    vm.policyPremiumSave;

    // vm.numericValidation = numericValidation;
    // vm.formats = [, 'yyyy/MM/dd', 'shortDate'];
    vm.format = 'MM/dd/yyyy';

    // datepicker can have different configuration
    vm.dateOptions = [];
    vm.opened = [];
    vm.showMsg = false;
    vm.optionData;
    vm.popup = [];
    var minDateForTO;
    vm.popup1 = {
      opened: true
    };
    vm.popup[3] = {
      opened: true
    };
    vm.popup[4] = {
      opened: false
    };
    vm.dt2 = vm.dt;
    vm.cedge = false;
    vm.name = null;
    vm.dateFormatError = [false, false];

    // methods
    // vm.openComponentModal = openComponentModal;
    vm.triggerSubmit = triggerSubmit;
    // vm.dateFormat = dateFormat;
    vm.getFormData = getFormData;
    vm.displaySeq = displaySeq;
    vm.setLength = setLength;
    // vm.formatDate = formatDate;
    vm.openPopUp = openPopUp;
    vm.polPremiumRangeAppian = polPremiumRangeAppian;
    vm.open = open;
    // vm.keyChanged = keyChanged;
    // vm.value = value;
    vm.returnFields = returnFields;
    vm.stringify = stringify;
    // vm.inputNumberValidation = inputNumberValidation;
    // vm.changed = changed;
    // vm.showMessage = showMessage;
    vm.keyupevt = keyupevt;
    // vm.checkPolicyPremiumRange = checkPolicyPremiumRange;
    vm.setDefaultDate = setDefaultDate;
    vm.changeFormat = changeFormat;
    vm.maskCurrency = maskCurrency;
    vm.replaceCommaDollar = replaceCommaDollar;
    vm.underwritingQuestions;
    vm.changeDate = changeDate;

    $interval(function () {

      vm.underwriting = vm.returnFields(vm.underwriting, vm.underwritingForm.userValue);
    }, 10);

    $scope.$watch('vm.underwriting',function () {
      $rootScope.pageInfo = [{
        "currentPage": $location.path(),
        "currentPageData": [vm.underwriting],
        "element": ['UNDERWRITINGQUESTIONS'],
        "operation": ['SAVE_UW_QUESTIONS_IN_APPIAN']
      }];
    },true);

    vm.getFormData();
    vm.triggerDefaultDate = triggerDefaultDate;

    function triggerDefaultDate(newValue) {
      return new Date(newValue);
    }

    function changeDate(systemRefId, ngModel, index) {
      // console.log(moment(ngModel).isValid());
      // console.log(new Date(ngModel) + 1);
       console.log('First line in method',ngModel)

      if (systemRefId == 'UNIG_PriorCarrierInformationInput.TermEffectiveDate' && moment(ngModel).isValid() && ngModel) {

        var dateForOneDayGap = new Date(moment.parseZone(ngModel).format('LLL'));
        var dateForOneYearGap = new Date(moment.parseZone(ngModel).format('LLL'));



        console.log('date after momentjs', dateForOneDayGap, dateForOneYearGap);

        dateForOneDayGap = new Date(moment.parseZone(ngModel).format('LLL'));
        dateForOneYearGap = new Date(moment.parseZone(ngModel).format('LLL'));


        dateForOneYearGap.setYear(dateForOneYearGap.getFullYear() + 1);
        dateForOneDayGap.setDate(dateForOneDayGap.getDate() + 1);



        console.log('date after +1', dateForOneDayGap, dateForOneYearGap);

        dateForOneDayGap = moment(dateForOneDayGap, 'YYYY-MM-DD').format('MM/DD/YYYY');
        dateForOneYearGap = moment(dateForOneYearGap, 'YYYY-MM-DD').format('MM/DD/YYYY');

        vm.dateOptions[index + 1] = {
          dateDisabled: false,
          formatYear: 'yy',
          minDate: dateForOneDayGap,
          startingDay: 0,
          showButtonBar: true,
          showWeeks: false
        };

        vm.underwritingForm.userValue['UNIG_PriorCarrierInformationInput.TermExpirationDate'] = dateForOneYearGap;
      }

      // console.log(vm.dateOptions);
    }

    function replaceCommaDollar(data) {
      if (data) {
        data = data.toString().replace('$', '');
        data = data.toString().replace(/,/g, "");
        return data;
      }

    }


    function maskCurrency(systemRefId, input) {
      var temp1;
      if (systemRefId.toLowerCase() == 'unig_priorcarrierinformationinput.policypremium' && input) {

        if (isNaN(input)) {

          temp1 = input.toString();

          if (temp1.indexOf('-') > 0) {
            temp1 = temp1.replace(/-/g, '');
            return '$' + temp1.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
          }
          // console.log(temp1.length)
          // console.log(temp1.indexOf('-'))
          if (temp1.indexOf('-') != -1 && temp1.length == 1) {

            return null;
          }
          if (temp1.indexOf('.') != -1 && temp1.length == 1) {

            return null;
          }

          if (temp1.indexOf('_') !== -1) {
            vm.errorMessagePol = true;
            return input;
          }

          if ((temp1.indexOf('$') == 0 && temp1.length == 1)) {
            vm.errorMessagePol = false;
            return;
          }

          if ((temp1.indexOf('-') == 0 && temp1.length == 1) ||
            ((temp1.match(/-/g) || []).length > 1) ||
            (temp1.indexOf('-', 1) !== -1) ||
            (temp1 == '$-')) {

            vm.errorMessagePol = true;
            return input;
          }
          if ((temp1.indexOf('$-') !== -1) &&
            (temp1.length > 2)) {
            vm.errorMessagePol = false;

          }
          if ((temp1.indexOf('.') == 0 && temp1.length == 1) ||
            (temp1.match(/\./g) || []).length > 1 ||
            (temp1 == '-.' && temp1.length == 2)) {

            vm.errorMessagePol = true;
            return input;
          }
          vm.errorMessagePol = false;

          input = input.toString().replace(/,/g, '');
          input = input.toString().replace(/\$/g, '');
          input = parseFloat(input);
          input = input.toFixed(input % 1 === 0 ? 0 : 2);
          if (input.includes('-')) {
            input = input.toString().replace(/-/g, '');
            return '-$' + input.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
          }
          return '$' + input.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        } // End of IF(isNAN)

        input = parseFloat(input);
        input = input.toFixed(input % 1 === 0 ? 0 : 2);
        // console.log(input);
        return '$' + input.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
      }
      return input;
    }

    function changeFormat($event) {
      // console.log("5555",$event.target.value);
      var firstSlash = $event.target.value.indexOf('/');
      var secondSlash = $event.target.value.indexOf('/', firstSlash + 1);
      // console.log(firstSlash);
      // console.log(secondSlash);

      if (firstSlash == 1 && secondSlash == 4) {
        var temp = '0' + $event.target.value[0];
        return temp + '/' + $event.target.value.substr(2);
      }


      if (firstSlash == 2 && secondSlash == 4) {
        var temp2 = '0' + $event.target.value[3];
        return $event.target.value.substr(0, 2) + '/' + temp2 + '/' + $event.target.value.substr(5);
      }

      if (firstSlash == 1 && secondSlash == 3) {
        var temp1 = '0' + $event.target.value[0];
        var temp3 = '0' + $event.target.value[2];
        return temp1 + '/' + temp3 + '/' + $event.target.value.substr(4);
      }

      return $event.target.value;
    } // END of changeFormat Function


    function setDefaultDate($event, sysRefId) {
      // console.log('565', $event);
      if (vm.underwritingForm.userValue[sysRefId] != null)
        vm.underwritingForm.userValue[sysRefId] = $event.target.value;
    }


    function keyupevt(index, event) {
      // console.log(index);
      vm.val = event.target.value;
      validateDateField(index, vm.val);
    }

    function validateDateField(index, value) {
      // console.log("jap" + value, index);
      if (value !== '' && !moment(value, 'MM/DD/YYYY').isValid()) {
        vm.dateFormatError[index] = true;
        // console.log("7878");
        vm.message[index] = "Please give in 'mm/dd/yyyy' format";
      } else {
        vm.message[index] = null;
        vm.dateFormatError[index] = false;
      }
    }
    /** **************************** */

    /** ******************************* */
    /**
	 * 
	 * @param {*}
	 *            systemRefId
	 * @param {*}
	 *            $event
	 */

    // function inputNumberValidation($event, systemRefId) {
    // if (systemRefId.toLowerCase() == 'policy_premium') {
    // console.log($event.keyCode);
    // if (
    // $event.keyCode == 45 ||
    // $event.keyCode == 46 ||
    // $event.keyCode == 101 ||
    // $event.keyCode == 43
    // ) {

    // $event.preventDefault();
    // }
    // }
    // }

    // function formatDate(data) {
    // var dtt = new Date(2020, 5, 22);
    // return dtt;
    // }

    /**
	 * desc
	 */

    // function disabled(data) {
    // var date = data.date,
    // mode = data.mode;
    // return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    // }

    /**
	 * desc
	 * 
	 * @param {*}
	 *            id
	 */
    function openPopUp(id) {
      // $log.info(id);
      vm.popup1.opened = true;
    }

    /**
	 * 
	 * @param {*}
	 * @param {*}
	 *            systemRefId
	 * @param {*}
	 *            data
	 */

    function setLength($event, systemRefId, data) {
      // console.log($event.keyCode);
      if (systemRefId.toLowerCase() == 'unig_priorcarrierinformationinput.carriername' && data) {

        var text, selectText;

        if (data.length > 39) {

          text = $event.target;
          selectText = text.value.substr(text.selectionStart, text.selectionEnd - text.selectionStart);
          // console.log(selectText);
          if (selectText) {
            return;
          }

          if ($event.keyCode == 8 || // backspace
            $event.keyCode == 46 || // delete
            $event.keyCode == 37 || // left arrow
            $event.keyCode == 39 || // right arraow
            $event.keyCode == 36 || // HOME
            $event.keyCode == 35) { // end
            return;
          }
          $event.preventDefault();

        }


      }

    } // end of Length Validation

    // function value(control) {
    // var t;

    // vm.defaultValue;
    // for (t = 0; t < vm.policy.length; t++) {
    // if (vm.policy[t].datasetName == control) {
    // if (control.toLowerCase().indexOf('from') != -1) {
    // vm.defaultValue = vm.underwriting['policy_date_from'];
    // // console.log("from -");
    // }
    // if (control.toLowerCase().indexOf('to') != -1) {
    // vm.defaultValue = vm.underwriting['policy_date_to'];
    // }
    // }
    // }
    // // console.log("data ---- ",vm.defaultValue);
    // if (vm.defaultValue) {
    // return vm.defaultValue.split('\\').join('');
    // }
    // }

    /**
	 * desc
	 * 
	 * @param {*}
	 *            index
	 */
    function open(index) {
      $timeout(function () {
        vm.opened[index] = true;
      });
    }

    /**
	 * desc // *
	 * 
	 * @param {*}
	 *            index // *
	 * @param {*}
	 *            event
	 */
    // function keyChanged(index, event) {
    // var currentValue = vm.underwritingForm.userValue[vm.dateUUID[index]];
    // }
    /**
	 * desc
	 */
    // function submit() {
    // //
    // }

    function returnFields(source, form) {
      // iterate the GROUP FIELDS

      for (var j in source) {
        if (source[j][0] != null) {
          var currentNode = source[j];
          for (var b = 0; b < currentNode.length; b++) {
            var item = currentNode[b];
            if (typeof form[item.systemRefId] !== 'undefined') {
              item.userValue = stringify(
                form[item.systemRefId],
                item.controlName
              );
            } else if (typeof form[item.systemRefID] !== 'undefined') {
              item.userValue = stringify(
                form[item.systemRefID],
                item.controlName
              );
            }
          }
        }
      }

      return source;
    }

    /**
	 * Converting date into string
	 * 
	 * @param {*}
	 *            inputVal
	 * @param {*}
	 *            controlName
	 */
    function stringify(inputVal, controlName) {
      var current = inputVal;
      // $log.info(controlName);
      // if (controlName.toLowerCase() == 'date') {
      // // return dateToString(current);
      // }
      return current;
    }

    function displaySeq(response, formModel) {
      var input = response;
      for (var a = 0; a < input.length; a++) {
        if (typeof input[a].userValue !== 'undefined') {
          // var formModel;
          if (input[a].systemRefId != null)
            formModel[input[a].systemRefId] = input[a].userValue;
          else if (input[a].systemRefID != null)
            formModel[input[a].systemRefID] = input[a].userValue;

        } else {
          if (input[a].controlName !== 'Header') {
            input[a].userValue = "";
            if (input[a].systemRefId != null)
              formModel[input[a].systemRefId] = '';
            else if (input[a].systemRefID != null)
              formModel[input[a].systemRefID] = '';

          }
        }
        // console.log("FORM MODEL = ",formModel);
      }

      for (var i = 0; i < response.length; i++) {
        response.sort(fieldSorter(['displaySequence', 'id']));
        if (typeof response[i].validationName !== 'undefined') {
          var currEntVal = response[i].validationName;
          var isRequired = false;
          var inputType = 'text';
          // var pattern;
          for (var ai = 0; ai < currEntVal.length; ai++) {
            if (
              currEntVal[ai].toLowerCase() == 'required' &&
              response[i].appearUI.toLowerCase() == 'y'
            ) {
              isRequired = true;
            }
            if (currEntVal[ai].toLowerCase() == 'numeric') {
              inputType = 'number';
              // pattern = '[0-9]+';
            }
            if (currEntVal[ai].toLowerCase() == 'currency') {
              // response[i].controlName = 'currency';
              inputType = 'text';
            }
            if (currEntVal[ai].toLowerCase() == 'alphanumeric') {
              // pattern = '[a-zA-Z0-9 ]+';
              inputType = 'text';
            }
          }
          // $log.info(pattern);
          response[i].isRequired = isRequired;
          response[i].inputType = inputType;
        } else {
          // no validation rule we assing defaults
          // console.log('defailt validation rules');
          response[i].isRequired = false;
          response[i].inputType = 'text';
        }
      }
      // }
      return response;
    }



    function fieldSorter(fields) {
      return function (a, b) {
        return fields
          .map(function (o) {
            var dir = 1;
            if (o[0] === '-') {
              dir = -1;
              o = o.substring(1);
            }
            if (a[o] > b[o]) return dir;
            if (a[o] < b[o]) return -dir;
            return 0;
          })
          .reduce(function firstNonZeroValue(p, n) {
            return p ? p : n;
          }, 0);
      };
    }


    function polPremiumRangeAppian() {

      var appianEventData;
      var systemRefId = null;
      var policyPremiumData = null;

      for (var loop = 0; loop < vm.policy.length; loop++) {
        if (vm.policy[loop].systemRefId.toLowerCase() == 'unig_priorcarrierinformationinput.policypremium') {
          systemRefId = vm.policy[loop].systemRefId;
          policyPremiumData = vm.underwritingForm.userValue[vm.policy[loop].systemRefId];
          break;
        }
      }

      policyPremiumData = vm.replaceCommaDollar(policyPremiumData);



      if (systemRefId.toLowerCase() == 'unig_priorcarrierinformationinput.policypremium' && policyPremiumData) {
        for (var appianPolicyPremiumLoop = 0; appianPolicyPremiumLoop < vm.policy.length; appianPolicyPremiumLoop++) {
          if (vm.policy[appianPolicyPremiumLoop].systemRefId.toLowerCase() == 'unig_priorcarrierinformationinput.policypremium') {
            if (vm.policy[appianPolicyPremiumLoop].eventName) {
              if (Array.isArray(vm.policy[appianPolicyPremiumLoop].eventName)) {

                if (vm.policy[appianPolicyPremiumLoop].eventName[0] &&
                  vm.policy[appianPolicyPremiumLoop].eventName[0].indexOf(':') != -1) {

                  appianEventData = vm.policy[appianPolicyPremiumLoop].eventName[0].split(':')[1];

                  break;
                } else {
                  ErrorToast.showError('1. No APPIAN CALL because Event Name is EMPTY / NULL or Not in correct format.');
                  break;
                }
              } else {
                appianEventData = vm.policy[appianPolicyPremiumLoop].eventName.split(':')[1];
                break;
              }
            } else {
              ErrorToast.showError('2. No APPIAN CALL because Event Name is EMPTY / ' + vm.policy[appianPolicyPremiumLoop].eventName);
              break;
            }
          }
        } // End For Loop

        if (appianEventData) {
          var policyPremiumAJAXData = {
            elementType: ['APPIANRULE'],
            operationType: [appianEventData],
            policyPremiumData: [policyPremiumData]
          };

          // Policy Premium AJAX call

          underwritingService
            .policyPremiumAppianCall(policyPremiumAJAXData)
            .then(function (response) {
                // $log.info('*-*-*-*', response);

                for (var policyLoop = 0; policyLoop < vm.policy.length; policyLoop++) {
                  for (var policyAppianData = 0; policyAppianData < response.data.length; policyAppianData++) {
                    if (vm.policy[policyLoop].systemRefId.toLowerCase() == response.data[policyAppianData].sysRefId.toLowerCase()) {
                      if (response.data[policyAppianData].error) {
                        vm.policyPremiumBool = true;
                        vm.policyPremiumErrorMesg =
                          response.data[policyAppianData].error;
                      } else {
                        vm.policyPremiumBool = false;
                        vm.policyPremiumErrorMesg = null;
                      }
                    }
                  } // End of Inner Loop
                } // End of outer Loop

                if (vm.policyPremiumBool) {
                  vm.policyPremiumSave = 1;
                } else {
                  vm.policyPremiumSave = 0;
                }


                if (vm.policyPremiumSave == 1) {
                  return;
                } else {
                  var theRes = vm.returnFields(vm.underwriting, vm.underwritingForm.userValue);
                  // // console.log('data saving to appian', theRes);

                  // var dataPlusAppian = angular.extend(theRes,
					// vm.appianData);
                  // console.log('underwriting appian data', vm.appianData );
                  var data = {
                    elementType: ['UNDERWRITINGQUESTIONS'],
                    operationType: ['SAVE_UW_QUESTIONS_IN_APPIAN'],
                    data: [theRes]
                  };
                     console.log('underwriting save called');

                  underwritingService.submitUnderwriting(data).then(
                    function () {
                      // $log.info(res);
                      util.setPageIndex('app.pricingandcoverage');
                      $state.go('app.pricingandcoverage');
                    },
                    function (err) {
                      ErrorToast.showError(err.detail);
                      // $log.info(err);
                      // console.log('underwriting submit - error',
						// err.detail);
                    }
                  );
                }
              },
              function (err) {
                ErrorToast.showError(err.detail);

                // var response = {
                // data:[
                // {
                // sysRefId:'UNIG_PriorCarrierInformationInput.PolicyPremium',
                // error:'This is appian Error message'
                // }
                // ]
                // };
                // for (var policyLoop = 0; policyLoop < vm.policy.length;
				// policyLoop++) {
                // for (var policyAppianData = 0; policyAppianData <
				// response.data.length; policyAppianData++) {
                // if (vm.policy[policyLoop].systemRefId.toLowerCase() ==
				// response.data[policyAppianData].sysRefId.toLowerCase()) {
                // if (response.data[policyAppianData].error) {
                // vm.policyPremiumBool = true;
                // vm.policyPremiumErrorMesg =
                // response.data[policyAppianData].error;
                // } else {
                // vm.policyPremiumBool = false;
                // vm.policyPremiumErrorMesg = null;
                // }
                // }
                // } // End of Inner Loop
                // } // End of outer Loop

                // if (vm.policyPremiumBool) {
                // vm.policyPremiumSave = 1;
                // } else {
                // vm.policyPremiumSave = 0;
                // }

              } // End of Function Error Message

            );

        } // End of appianEventData

      }

    }
    /**
	 * desc
	 */
    function triggerSubmit(isValid) {
      // $log.info(isValid);

      if (!isValid) {
        return false;
      }

      polPremiumRangeAppian();


      // console.log(policyPremiumData);
      // End of IF

    } // End of SAVE AND CONTINUE

    /**
	 * Get form meta data
	 */
    function getFormData() {
      // call service
      var data = {
        elementType: ['UNDERWRITINGQUESTIONS'],
        operationType: ['VIEW_UNDERWRITING_QUESTIONS']
      };
      underwritingService.fetchUnderwritingForm(data).then(
        function (response) {
          // console.log('fetchUnderwritingForm - success', response.data);
          // console.log("454545");
          vm.underwriting = angular.copy(response.data);

          // vm.formData = angular.copy(response.data);
          vm.formats = ['MM/dd/yyyy', 'yyyy/MM/dd', 'shortDate'];
          vm.format = vm.formats[0];
          // ParseJSON.mapForm(vm.underwriting);
          vm.underwritingQuestions = displaySeq(vm.underwriting['underwriting_questions'], vm.underwritingForm.userValue);
          vm.policy = displaySeq(vm.underwriting['prior_policy'], vm.underwritingForm.userValue);

          vm.policy.forEach(function (obj, arrayIndex) {
            // console.log('%d: %s', i, value.userValue);

            if (obj.controlName.toLowerCase() != 'header') {
              if (obj.systemRefId == 'UNIG_PriorCarrierInformationInput.TermEffectiveDate') { // FROM
																								// Date

                minDateForTO = new Date(obj.userValue);
                minDateForTO.setDate(minDateForTO.getDate());
                vm.dateOptions[arrayIndex] = {
                  dateDisabled: false,
                  formatYear: 'yy',
                  startingDay: 0,
                  showButtonBar: true,
                  showWeeks: false
                };

              }
              if (obj.systemRefId == 'UNIG_PriorCarrierInformationInput.TermExpirationDate') { // TO
																								// Date

                vm.dateOptions[arrayIndex] = {
                  dateDisabled: false,
                  formatYear: 'yy',
                  minDate: minDateForTO,
                  startingDay: 0,
                  showButtonBar: true,
                  showWeeks: false
                };

              }
            }
          }); // End of forEach Loop
          // console.log(vm.dateOptions);
          // underwritingAppian();
        },
        function (err) {
          ErrorToast.showError(err.detail);
          // console.log('getFormData - error', err.detail);
          // $log.info(err);
        }
      );
    }

    // function underwritingAppian() {
    // var appianEventData, appianEventUserData;
    // for (var aapianEventName = 0;aapianEventName <
	// vm.underwritingQuestions.length; aapianEventName++) {
    // if ( vm.underwritingQuestions[aapianEventName].systemRefId.toLowerCase()
	// =='select_the_number_of_claims_ar') {
    // if (vm.underwritingQuestions[aapianEventName].eventName) {

    // appianEventData =
	// vm.underwritingQuestions[aapianEventName].eventName[0].split(':')[1];
    // appianEventUserData =vm.underwritingQuestions[aapianEventName].userValue;
    // break;
    // }
    // }
    // }
    // var data1 = {
    // elementType: ['APPIANRULE'],
    // operationType: [appianEventData],
    // response: [appianEventUserData]
    // };
    // // console.log('underwritingAppianRule2112 - success',data1 );
    // underwritingService.underwritingAppianRule(data1).then(
    // function(res) {
    // // console.log(
    // // 'underwritingAppianRule - success',
    // // res.data
    // // );
    // vm.appianData = angular.copy(res.data);
    // for (var k = 0; k < vm.appianData.length; k++) {
    // for (var l = 0; l < vm.underwritingQuestions.length; l++) {
    // if (vm.appianData[k].sysRefId.toLowerCase() ==
	// vm.underwritingQuestions[l].systemRefId.toLowerCase()) {
    // // vm.underwritingForm.userValue[vm.underwritingQuestions[l].systemRefId]
	// = vm.appianData[k].value[0];
    // if(Array.isArray(vm.appianData[k].value)){
    // vm.underwritingForm.userValue[vm.underwritingQuestions[l].systemRefId] =
	// vm.appianData[k].value[0];
    // }
    // else{
    // vm.underwritingForm.userValue[vm.underwritingQuestions[l].systemRefId] =
	// vm.appianData[k].value;
    // }

    // }
    // }
    // }
    // },
    // function(err) {
    // $log.info(err);
    // //console.log('underwritingAppianRule - error', err.detail);
    // // var res = [
    // // {
    // // sysRefId: 'Any_claims__losses_or_loss_inc',
    // // value: ['Yes'],
    // // error: null,
    // // visible: null,
    // // eligible: null,
    // // enable: null,
    // // warning: null
    // // },
    // // {
    // // sysRefId: 'Select_the_check_box_if__Loss_',
    // // value: ['Yes'],
    // // error: null,
    // // visible: null,
    // // eligible: true,
    // // enable: null,
    // // warning: null
    // // }
    // // ];

    // // vm.appianData = angular.copy(res);
    // // //console.log(vm.appianData);

    // // for (var k = 0; k < vm.appianData.length; k++) {
    // // for (var l = 0; l < vm.underwritingQuestions.length; l++) {
    // // if (
    // // vm.appianData[k].sysRefId.toLowerCase() ==
    // // vm.underwritingQuestions[l].systemRefId.toLowerCase()
    // // ) {
    // // vm.underwritingForm.userValue[
    // // vm.underwritingQuestions[l].systemRefId
    // // ] =
    // // vm.appianData[k].value[0];
    // // }
    // // }
    // // }

    // // console.log(vm.underwritingForm);
    // }
    // );
    // } // underwriting controller
  }
})();

(function() {
  'use strict';

  angular
    // refer
    .module('quoteUi')
    .controller('ReferRedirectController', ReferRedirectController)
    .config(function (IdleProvider, KeepaliveProvider) {
      IdleProvider.idle(60);
      // IdleProvider.idle(7170);
      IdleProvider.timeout(30);
      KeepaliveProvider.interval(10);
    });

  /** @ngInject */
  function ReferRedirectController(util, $state) {
    var vm = this;
    vm.assetPath = util.getImagePath();

    vm.redirectToDashboard = redirectToDashboard;

    /**
	 * Redirect to main page
	 */
    function redirectToDashboard() {
      $state.go('accountview');
    }
  }
})();

(function () {
  'use strict';

  angular
    // refer
    .module('quoteUi')
    .controller('ReferController', ReferController);

  /** @ngInject */
  function ReferController(
    $rootScope,
    Validator,
    $state,
    $location,
    referService,
    util,
    ErrorToast,
    $scope
  ) {
    var vm = this;

    // $rootScope.pageInfo = $location.path();
    // vm.submitted = false;
    // todo: handle button disable, dynamic form validation

    $rootScope.pageInfo = [{
      currentPage: $location.path(),
      currentPageData: [vm.formMeta],
      element: ['REFERUNDERWRITER'],
      operation: ['SAVEANDEXIT']
    }];

    $rootScope.$on('onsave', function () {
      $rootScope.pageInfo = [{
        currentPage: $location.path(),
        currentPageData: [vm.formMeta],
        element: ['REFERUNDERWRITER'],
        operation: ['SAVEANDEXIT']
      }];
    });

    vm.forms = [];
    vm.validations = [];
    vm.saveDisabled = true;

    // methods
    vm.getFormMeta = getFormMeta;
    vm.addNewInsured = addNewInsured;
    vm.triggerSubmit = triggerSubmit;
    vm.removeNamedInsured = removeNamedInsured;
    vm.submitNamedForm = submitNamedForm;
    vm.changeType = changeType;
    vm.validatePhone = validatePhone;
    vm.validateEmail = validateEmail;
    vm.valueUpdated = valueUpdated;
    vm.onSaveRefer = onSaveRefer;

    $rootScope['maskOpts' + '0'] = {
      mask: '(999) 999-9999',
      restrict: 'reject',
      validate: 'true',
      limit: 'true'
    };

    //

    vm.getFormMeta();

    /**
	 * Trigger update
	 * 
	 * @param {*}
	 *            item
	 */
    function valueUpdated(item) {
      item.isUpdated = true;
    }

    /**
	 * Validate email
	 * 
	 * @param {*}
	 *            value
	 */
    function validateEmail(value) {
      if (!value || !value.match(/([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-zA-Z]{2,6}(?:\.[a-zA-Z]{2})?)\S+/)) {
        vm.emailErr = true;
      } else {
        vm.emailErr = false;
      }
    }

    /**
	 * Validate phone
	 * 
	 * @param {*}
	 *            value
	 */
    function validatePhone(value) {
      if (value && (value.replace(/[^\d]/g, '').length > 10 || value.replace(/[^\d]/g, '').length < 10)) {
        vm.invalidPhone = true;
      } else {
        vm.invalidPhone = false;
      }
    }

    /**
	 * Remove name insured
	 * 
	 * @param {*}
	 *            index
	 */
    function removeNamedInsured(index) {
      var duckId = vm.formMeta.insuredInformationMetadata[index].duckCreekId;
      if (typeof duckId !== 'undefined') {
        var data = {
          elementType: ['REFERUNDERWRITER'],
          operationType: ['DELETE_NAMED_INSURED'],
          duckID: [duckId],
          data: [vm.formMeta]
        };
        referService.deleteNameInsured(data).then(function () {
          vm.formMeta.insuredInformationMetadata.splice(index, 1);
        }, function (err) {
          ErrorToast.showError(err.detail);
        });
      }
    }

    /**
	 * Name insured appian validation
	 * 
	 * @param {*}
	 *            formData
	 */
    function changeType(formData) {
      var business = _.findWhere(formData, {
        systemRefId: 'AdditionalOtherInterestInput.UNIG_DBA'
      });
      var type = _.findWhere(formData, {
        systemRefId: 'AdditionalOtherInterestInput.EntityType'
      });

      if (typeof type !== 'undefined' && typeof business !== 'undefined' && type.userValue != null && business.userValue != null) {

        var data = {
          elementType: ['APPIANRULE'],
          operationType: ['BRVALIDATESELECTTYPEANDDBA'],
          var1: [type.userValue],
          var2: [business.userValue]
        };

        referService.validateType(data).then(
          function (res) {
            _.each(res.data, function (item) {
              var row = _.findWhere(formData, {
                systemRefId: item.sysRefId
              });
              if (typeof row !== 'undefined' && item.visible == true) {
                row.appearUI = 'Y';
              } else if (typeof row !== 'undefined' && item.visible == false) {
                row.appearUI = 'N';
              }
            });

            _.each(vm.formMeta.insuredInformationMetadata, function (item) {
              item.insuredInformation = _.sortBy(
                item.insuredInformation,
                'displaySequence'
              );
            });

            // insured information
            _.each(vm.formMeta.insuredInformationMetadata, function (meta) {
              var displaySeqIns = util.displaySeqCount(
                _.where(meta.insuredInformation, {
                  appearUI: 'Y'
                })
              );
              _.each(meta.insuredInformation, function (item) {
                item.screenColumns = 12 / displaySeqIns[item.displaySequence];
                if (item.validationName.indexOf('Required') != -1) {
                  item.isRequired = true;
                }
              });
            });

          },
          function (err) {
            ErrorToast.showError(err.detail);
          }
        );
      }
    }

    /**
	 * Submit name insured
	 * 
	 * @param {*}
	 *            index
	 * @param {*}
	 *            formData
	 * @param {*}
	 *            isValid
	 * @param {*}
	 *            isPrimary
	 */
    function submitNamedForm(index, formData, isValid, isPrimary, isCb) {
      // form.$setSubmitted();
      var data = {};
      var operationType;
      if (isPrimary) {
        operationType = 'SAVENAMEINSURED';
      } else {
        operationType = 'SAVEADDITIONALNAMEINSURED';
      }

      if (vm.formMeta.insuredInformationMetadata[index].isSaved == true) {
        operationType = 'EDIT_SECONDARY_NAMED_INSURED';
        data.duckId = [vm.formMeta.insuredInformationMetadata[index].duckCreekId];
      }

      vm.forms[index] = true;
      if (isValid) {
        vm.validations[index] = true;

        data.elementType = ['REFERUNDERWRITER'];
        data.operationType = [operationType];
        data.data = [vm.formMeta];

        referService.saveNameInsured(data).then(
          function (res) {
            delete vm.formMeta.insuredInformationMetadata[index].isUpdated;
            vm.formMeta.insuredInformationMetadata[index].isSaved = true;
            vm.saveDisabled = false;
            if (res.data.producerList && res.data.producerList.length > 0 && isPrimary) {
              var producer = _.findWhere(vm.formMeta.licensedProducerSigningApplication, {
                systemRefId: 'AgentPrivate.WebserviceAgtLicDropDown'
              });
              if (typeof producer !== 'undefined') {
                producer.valueName = res.data.producerList;
              }
            }

            // if (res.data.agentContactInfoData.length > 0 && isPrimary) {
            // var obj;
            // _.each(res.data.agentContactInfoData[0], function (value, key) {
            // obj = _.findWhere(vm.formMeta.agentContactInfo, {
            // systemRefId: key
            // });
            // if (typeof obj !== 'undefined') {
            // obj.userValue = value;
            // }
            // });
            // }
            if (isCb) {
              vm.onSaveRefer();
            }
          },
          function (err) {
            ErrorToast.showError(err.detail);
          }
        );
      } else {
        vm.validations[index] = false;
      }
    }



    /**
	 * Save main form
	 * 
	 * @param {*}
	 *            isValid
	 * @param {*}
	 *            theForm
	 */
    function triggerSubmit(isValid, theForm) {
      theForm.$setSubmitted();
      var isDynamicValid = true;
      var namedServiceTrigger = false,
        index, isPrimary, formData;
      for (var i = 0; i < vm.formMeta.insuredInformationMetadata.length; i++) {
        vm.forms[i] = true;
        if (vm.formMeta.insuredInformationMetadata[i].isUpdated) {
          namedServiceTrigger = true;
          formData = vm.formMeta.insuredInformationMetadata[i];
          index = i;
          break;
        }
      }

      for (var j = 0; j < vm.formMeta.insuredInformationMetadata.length; j++) {
        if (!vm.validations[j]) {
          isDynamicValid = false;
          break;
        }
      }

      if (isValid && isDynamicValid && !vm.invalidPhone && !vm.emailErr && !namedServiceTrigger) {
        vm.onSaveRefer();
      } else if (isValid && isDynamicValid && !vm.invalidPhone && !vm.emailErr && namedServiceTrigger) {
        isPrimary = index == 0 ? true : false;
        vm.submitNamedForm(index, formData, true, isPrimary, true);
      }
    }

    function onSaveRefer() {
      var data = {
        elementType: ['REFERUNDERWRITER'],
        operationType: ['SAVEREFERUNDERWRITER'],
        data: [vm.formMeta]
      };
      referService.saveRefertoUnderwriter(data).then(
        function () {
          $state.go('refer_redirect');
        },
        function (err) {
          ErrorToast.showError(err.detail);
        }
      );
    }



    /**
	 * Get more widgets
	 */
    function addNewInsured() {
      var data = {
        elementType: ['REFERUNDERWRITER'],
        operationType: ['VIEWNAMEINSURED']
      };
      referService.fetchNameInsured(data).then(
        function (res) {
          vm.saveDisabled = true;
          res.data.insuredInformation = _.sortBy(
            res.data.insuredInformation,
            'displaySequence'
          );

          var displaySeqIns = util.displaySeqCount(
            res.data.insuredInformation
          );
          _.each(res.data.insuredInformation, function (item) {
            item.screenColumns = 12 / displaySeqIns[item.displaySequence];
            if (item.validationName.indexOf('Required') != -1) {
              item.isRequired = true;
            }
            // if (item.controlName == 'Header') {
            // vm.insuredInfoHdr = item;
            // }
          });
          vm.formMeta.insuredInformationMetadata.push(res.data);
        },
        function (err) {
          ErrorToast.showError(err.detail);
        }
      );
    }

    /**
	 * Get form meta
	 */
    function getFormMeta() {
      var data = {
        elementType: ['REFERUNDERWRITER'],
        operationType: ['VIEWREFERUNDERWRITER']
      };
      referService.fetchForm(data).then(
        function (res) {
          vm.formMeta = res.data;
          vm.formMeta.explanationOfApplication = _.sortBy(
            vm.formMeta.explanationOfApplication,
            'displaySequence'
          );
          vm.formMeta.agentContactInfo = _.sortBy(
            vm.formMeta.agentContactInfo,
            'displaySequence'
          );

          _.each(vm.formMeta.insuredInformationMetadata, function (item) {
            item.insuredInformation = _.sortBy(
              item.insuredInformation,
              'displaySequence'
            );
          });

          vm.formMeta.licensedProducerSigningApplication = _.sortBy(
            vm.formMeta.licensedProducerSigningApplication,
            'displaySequence'
          );
          var displaySeqLic = util.displaySeqCount(
            vm.formMeta.licensedProducerSigningApplication
          );
          var displaySeqExp = util.displaySeqCount(
            vm.formMeta.explanationOfApplication
          );
          // licensed
          _.each(vm.formMeta.licensedProducerSigningApplication, function (
            item
          ) {
            item.screenColumns = 12 / displaySeqLic[item.displaySequence];

            if (item.validationName.indexOf('Required') != -1) {
              item.isRequired = true;
            }
            if (item.controlName == 'Header') {
              vm.licInfoHdr = item;
            }
          });

          // explanation of application
          _.each(vm.formMeta.explanationOfApplication, function (item) {
            item.screenColumns = 12 / displaySeqExp[item.displaySequence];
            if (item.validationName.indexOf('Required') != -1) {
              item.isRequired = true;
            }
            if (item.controlName == 'Header') {
              vm.explanationInfoHdr = item;
            }
          });
          var displaySeqAgent = util.displaySeqCount(
            vm.formMeta.agentContactInfo
          );

          // agent contact info
          _.each(vm.formMeta.agentContactInfo, function (item) {
            item.screenColumns = 12 / displaySeqAgent[item.displaySequence];
            if (item.validationName.indexOf('Required') != -1) {
              item.isRequired = true;
            }
            if (item.controlName == 'Header') {
              vm.agentInfoHdr = item;
            }
          });


          // insured information
          _.each(vm.formMeta.insuredInformationMetadata, function (meta) {
            var displaySeqIns = util.displaySeqCount(
              meta.insuredInformation
            );
            _.each(meta.insuredInformation, function (item) {
              item.screenColumns = 12 / displaySeqIns[item.displaySequence];
              if (item.validationName.indexOf('Required') != -1) {
                item.isRequired = true;
              }
              if (item.controlName == 'Header') {
                vm.insuredInfoHdr = item;
              }
            });
          });
        },
        function (err) {
          ErrorToast.showError(err.detail);
        }
      );
    }
  }
})();

(function () {
  'use strict';

  angular
    .module('quoteUi')
    .controller('QuoteController', QuoteController)
    // controller for modal
    .controller('LOBModalInstanceCtrl', function ($uibModalInstance, param) {
      var $ctrl = this;
      $ctrl.lobLabel = param.lobLabel;
      $ctrl.ok = function () {
        $uibModalInstance.close();
      };
      $ctrl.cancel = function () {
        $uibModalInstance.dismiss();
      };
    });

  /** @ngInject */
  function QuoteController(
    $scope,
    $timeout,
    $state,
    $window,
    ErrorToast,
    $rootScope,
    Conf,
    $uibModal,
    toastr,
    ParseJSON,
    Validator,
    quoteService,
    $location,
    util,
    $log
  ) {
    var vm = this;

    vm.lobForm = {};
    vm.dateUUID = [];
    var minDate = new Date();
    var minDays = 5;
    minDate.setDate(minDate.getDate() - minDays);

    vm.lobData = {};
    vm.lobData.uuid = {};
    vm.LOB = [];
    vm.dateDisabled = [false, false];

    vm.formats = ['MM/dd/yyyy', 'yyyy/MM/dd', 'shortDate'];
    vm.format = vm.formats[0];

    // datepicker can have different configuration
    vm.dateOptions = [];
    vm.message = [];
    vm.dateOptions['PolicyInput.UNIG_EffectiveDate'] = {
      dateDisabled: false,
      formatYear: 'yy',
      startingDay: 0,
      showButtonBar: false,
      showWeeks: false
    };
    vm.dateOptions['policy_effective_date'] = vm.dateOptions['PolicyInput.UNIG_EffectiveDate'];

    vm.dateOptions['PolicyInput.ExpirationDate'] = {
      dateDisabled: false,
      formatYear: 'yy',
      startingDay: 0,
      showButtonBar: false,
      showWeeks: false
    };
    vm.dateOptions['policy_end_date'] = vm.dateOptions['PolicyInput.ExpirationDate'];

    vm.opened = [];
    vm.showMsg = false;
    vm.optionData;
    vm.popup = [];
    vm.popup1 = {
      opened: true
    };
    vm.popup[3] = {
      opened: true
    };
    vm.popup[4] = {
      opened: false
    };
    vm.dt2 = vm.dt;
    vm.cedge = false;
    vm.dateFormatError = [false, false];

    // methods
    vm.openComponentModal = openComponentModal;
    vm.formatDate = formatDate;
    vm.openPopUp = openPopUp;
    vm.open = open;
    vm.changed = changed;
    vm.showMessage = showMessage;
    vm.keyupevt = keyupevt;
    vm.submitForm = submitForm;
    vm.getLobForm = getLobForm;

    $rootScope.pageInfo = [{
      currentPage: $location.path(),
      currentPageData: [],
      element: [],
      operation: []
    }];

    vm.getLobForm();

    /**
	 * desc
	 * 
	 * @param {*}
	 *            lobLabel
	 */
    function openComponentModal(lobLabel, item) {
      var modalInstance = $uibModal.open({
        templateUrl: 'LOBModalContent.html',
        controller: 'LOBModalInstanceCtrl',
        controllerAs: '$ctrl',
        resolve: {
          param: function () {
            return {
              lobLabel: lobLabel
            };
          }
        }
      });

      modalInstance.result.then(
        function () {
          if (vm.lobMeta.lob[0].url != null) {
            $window.open(vm.lobMeta.lob[0].url, '_blank');
          } else if (typeof item.url !== 'undefined' && item.url != null) {
            $window.open(item.url, '_blank');
          }
        },
        function () {}
      );
    }

    /**
	 * Get LOB metadata
	 */
    function getLobForm() {
      var data = {
        elementType: ['STARTYOURQUOTE'],
        operationType: ['VIEWSTARTYOURQUOTE'],
        action: ['GET']
      };

      // fetch meta from service
      quoteService.fetchLobForm(data).then(
        function (res) {
          vm.lobMeta = res.data;

          var from = _.findWhere(vm.lobMeta.policyType, {
            systemRefId: 'PolicyInput.UNIG_EffectiveDate'
          });
          if (typeof from === 'undefined') {
            from = _.findWhere(vm.lobMeta.policyType, {
              systemRefId: 'policy_effective_date'
            });
          }

          changed(0, from.userValue, from.systemRefId);

          var stateVal = _.findWhere(vm.lobMeta.policyType, {
            systemRefId: 'PolicyInput.PrimaryRatingState'
          });
          if (typeof stateVal === 'undefined') {
            stateVal = _.findWhere(vm.lobMeta.policyType, {
              systemRefId: 'primary_risk_state'
            });
          }
          stateVal = stateVal.userValue;
          if (angular.isDefined(stateVal)) {
            vm.showMessage(stateVal);
          }
          vm.mvpLob = _.find(vm.lobMeta.lob[0].valueName, function (item) {
            return item.lobName == 'Business Owners' && item.eligibility == false && item.isQuoteEligible == true && item.productId == null;
          });

          vm.eligibleLobs = _.findWhere(vm.lobMeta.lob[0].valueName, {
            eligibility: true
          });
          vm.notEligibleLobs = _.find(vm.lobMeta.lob[0].valueName, function (item) {
            return item.lobName != 'Business Owners' && item.eligibility == false;
          });
          // _.findWhere(vm.lobMeta.lob[0].valueName, {
          // eligibility: false
          // });
          if (typeof vm.eligibleLobs === 'undefined') {
            vm.isError = true;
          }

          var isLobSelected = _.findWhere(vm.lobMeta.lob[0].valueName, {
            userValue: 'Yes'
          });
          if (typeof isLobSelected !== 'undefined') {
            vm.lobSelected = true;
          }
        },
        function (err) {
          ErrorToast.showError(err.detail);
        }
      );
    }

    /**
	 * desc
	 */
    function formatDate() {
      var dtt = new Date(2020, 5, 22);
      return dtt;
    }

    /**
	 * desc
	 */
    function openPopUp() {
      vm.popup1.opened = true;
    }

    /**
	 * desc
	 * 
	 * @param {*}
	 *            index
	 */
    function open(index) {
      $timeout(function () {
        vm.opened[index] = true;
      });
    }

    /**
	 * Process LOB date appian rule and adjust date
	 * 
	 * @param {*}
	 *            index
	 * @param {*}
	 *            date
	 */
    function changed(index, date, systemRefId) {
      var dateValues = _.where(vm.lobMeta.policyType, {
        controlName: 'Date'
      });

      var nextObj;
      if (
        systemRefId === 'PolicyInput.ExpirationDate' ||
        systemRefId === 'policy_end_date'
      ) {
        index = 1;
        nextObj = _.findWhere(vm.lobMeta.policyType, {
          systemRefId: 'PolicyInput.UNIG_EffectiveDate'
        });
        if (typeof nextObj === 'undefined') {
          nextObj = _.findWhere(vm.lobMeta.policyType, {
            systemRefId: 'policy_effective_date'
          });
        }
      } else {
        index = 0;
        nextObj = _.findWhere(vm.lobMeta.policyType, {
          systemRefId: 'PolicyInput.ExpirationDate'
        });
        if (typeof nextObj === 'undefined') {
          nextObj = _.findWhere(vm.lobMeta.policyType, {
            systemRefId: 'policy_end_date'
          });
        }
      }
      var nextIndex = vm.lobMeta.policyType.indexOf(nextObj);

      vm.endDateDiff = false;
      vm.lobError = false;
      vm.effectiveDateDiff = false;

      vm.today = new Date();
      $log.info(1, date);

      if (moment(date, 'MM/DD/YYYY').format('MM/DD/YYYY') == 'Invalid date') {
        date = moment(date, 'YYYY-MM-DD').format('MM/DD/YYYY');
        $log.info(2, date);
      } else {
        date = moment(date, 'MM/DD/YYYY').format('MM/DD/YYYY');
        $log.info(3, date);
      }
      var dateFormatted = moment(date, 'MM/DD/YYYY').format('YYYY-MM-DD');
      $log.info(4, dateFormatted);
      date = new Date(moment.parseZone(dateFormatted).format('LLL')); // new
																		// Date(dateFormatted);
      $log.info(5, date);
      // date = new Date(date);
      vm.effectiveDate = date;

      var diff = daysBetween(new Date(), date);

      if ((diff <= -5 || diff >= 60) && index == 0) {
        vm.lobError = true;
      }

      if (angular.isUndefined(date) || date === '') {
        return false;
      }

      var maxDate = angular.copy(date);
      var isLeap = util.leapYear((maxDate.getFullYear() + 1));
      if (!isLeap) {
        var maxDays = 365; // go back 5 days!
      } else {
        var maxDays = 366; // go back 5 days!
      }

      if (index === 0) {
        var _secondMinDate = angular.copy(date);
        $log.info(6, maxDate);
        maxDate.setDate(maxDate.getDate() + maxDays);
        $log.info(7, maxDate);
        vm.lobMeta.policyType[nextIndex].userValue = maxDate;
        vm.dateOptions[
          'PolicyInput.ExpirationDate'
        ].minDate = _secondMinDate.setDate(_secondMinDate.getDate() + 1);
        vm.dateOptions['PolicyInput.ExpirationDate'].maxDate = maxDate;

        vm.dateOptions[
          'policy_end_date'
        ].minDate = _secondMinDate.setDate(_secondMinDate.getDate() + 1);
        vm.dateOptions['policy_end_date'].maxDate = maxDate;

        validateDateField(
          1,
          moment(maxDate, 'YYYY-MM-DD').format('MM/DD/YYYY')
        );
      }

      vm.endDate = new Date(date);
      var theDiff = daysBetween(begin, end);
      vm.endDateDiff = false;
      if ((theDiff > maxDays && index === 1) || (theDiff < 1 && index === 1)) {
        maxDate = new Date(angular.copy(dateValues[0]));
        $log.info(8, maxDate);
        maxDate.setDate(maxDate.getDate() + maxDays);
        $log.info(9, maxDate);
        vm.lobMeta.policyType[nextIndex].userValue = maxDate;
        vm.dateOptions[1].maxDate = maxDate;
        validateDateField(
          1,
          moment(maxDate, 'YYYY-MM-DD').format('MM/DD/YYYY')
        );
      }

      var begin = moment(dateValues[0].userValue, 'MM-DD-YYYY').format(
        'MM/DD/YYYY'
      );
      if (begin == 'Invalid date') {
        begin = moment(dateValues[0].userValue, 'YYYY-MM-DD').format(
          'MM/DD/YYYY'
        );
      }
      var end = moment(dateValues[1].userValue, 'YYYY-MM-DD').format(
        'MM/DD/YYYY'
      );

      $log.info('begin', begin);
      $log.info('end', end);

      var data = {
        elementType: ['APPIANRULE'],
        operationType: ['LENGTHVALIDATION'],
        date1: [begin],
        date2: [end]
      };

      // call lob date rule
      quoteService.dateRule(data).then(
        function (res) {
          vm.appianError = res.data[0].error;
          vm.appianWarning = res.data[0].warning;
          if (res.data[0].error != null) {
            vm.endDateDiff = true;
            vm.isError = true;
          } else if (res.data[0].warning != null) {
            vm.isError = false;
            vm.endDateDiff = true;
          } else {
            vm.isError = false;
          }
        },
        function (err) {
          ErrorToast.showError(err.detail);
        }
      );
    }

    /**
	 * Process LOB primary risk state change rule
	 * 
	 * @param {*}
	 *            selectedValue
	 */
    function showMessage(selectedValue) {
      vm.AppianRule = 'BRVALIDATERISKSTATES'; // eventName.split(':')[1];
      var data = {
        elementType: ['APPIANRULE'],
        operationType: [vm.AppianRule],
        data: [selectedValue]
      };
      if (selectedValue == null) {
        return false;
      }

      // call appian rule service
      quoteService.riskStateRule(data).then(
        function (res) {
          vm.selectedValue = res.data[0].enable;
          if (vm.selectedValue) {
            vm.showMsg = false;
            vm.optionData = selectedValue.lobName;
          } else {
            vm.showMsg = true;
            vm.optionData = selectedValue.lobName;
          }
          if (
            res.data[0].error != null &&
            typeof res.data[0].error !== 'undefined'
          ) {
            vm.lobError = true;
            vm.isError = true;
            vm.appianError = res.data[0].error;
          } else {
            vm.lobError = false;
            vm.isError = false;
            vm.appianError = null;
          }
        },
        function (err) {
          ErrorToast.showError(err.detail);
        }
      );
    }

    /**
	 * Keyup event
	 * 
	 * @param {*}
	 *            index
	 * @param {*}
	 *            value
	 */
    function validateDateField(index, value) {
      if (value !== '' && !moment(value, 'MM/DD/YYYY').isValid()) {
        vm.dateFormatError[index] = true;
        vm.message[index] = "Please give in 'mm/dd/yyyy' format";
      } else {
        vm.message[index] = null;
        vm.dateFormatError[index] = false;
      }
    }

    /**
	 * Keyup event
	 * 
	 * @param {*}
	 *            index
	 * @param {*}
	 *            event
	 */
    function keyupevt(index, event, systemRefId) {
      vm.val = event.target.value;
      validateDateField(index, vm.val);
      if (vm.val && vm.val.length == 10 && (systemRefId == 'PolicyInput.ExpirationDate' || systemRefId == 'policy_end_date')) {
        vm.val = moment(vm.val, 'MM/DD/YYYY').format(
          'YYYY-MM-DD'
        );

        var nextObj = _.findWhere(vm.lobMeta.policyType, {
          systemRefId: 'PolicyInput.UNIG_EffectiveDate'
        });
        if (typeof nextObj === 'undefined') {
          nextObj = _.findWhere(vm.lobMeta.policyType, {
            systemRefId: 'policy_effective_date'
          });
        }
        var effectiveDt = nextObj.userValue || new Date();
        if (effectiveDt instanceof Date == false) {
          effectiveDt = moment(effectiveDt, 'YYYY-MM-DD').format(
            'YYYY-MM-DD'
          );
          effectiveDt = new Date(effectiveDt);
        }
        var actual = moment(effectiveDt).add(1, 'years');
        if (moment(vm.val).isAfter(actual)) {
          var obj = _.findWhere(vm.lobMeta.policyType, {
            systemRefId: 'PolicyInput.ExpirationDate'
          });
          if (typeof obj === 'undefined') {
            obj = _.findWhere(vm.lobMeta.policyType, {
              systemRefId: 'policy_end_date'
            });
          }
          obj.userValue = new Date(actual);
        }
      }
    }

    /**
	 * desc
	 * 
	 * @param {*}
	 *            date
	 */
    function treatAsUTC(date) {
      var result = new Date(date);
      result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
      return result;
    }

    /**
	 * desc
	 * 
	 * @param {*}
	 *            startDate
	 * @param {*}
	 *            endDate
	 */
    function daysBetween(startDate, endDate) {
      var millisecondsPerDay = 24 * 60 * 60 * 1000;
      return (treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay;
    }

    /**
	 * Submit form
	 * 
	 * @param {*}
	 *            isValid
	 * @param {*}
	 *            theForm
	 */
    function submitForm(isValid, theForm) {
      Validator.triggerValidation(theForm);
      if (!isValid) {
        return false;
      }
      vm.submitted = true;
      var inData = angular.copy(vm.lobMeta);
      var effectiveDate = _.findWhere(inData.policyType, {
        systemRefId: 'policy_effective_date'
      });
      if (typeof effectiveDate === 'undefined') {
        effectiveDate = _.findWhere(inData.policyType, {
          systemRefId: 'PolicyInput.UNIG_EffectiveDate'
        });
      }
      if (typeof effectiveDate !== 'undefined') {
        effectiveDate.userValue = moment.parseZone(effectiveDate.userValue).format('MM/DD/YYYY');
      }

      var expiryDate = _.findWhere(inData.policyType, {
        systemRefId: 'policy_end_date'
      });
      if (typeof expiryDate === 'undefined') {
        expiryDate = _.findWhere(inData.policyType, {
          systemRefId: 'PolicyInput.ExpirationDate'
        });
      }
      if (typeof expiryDate !== 'undefined') {
        expiryDate.userValue = moment.parseZone(expiryDate.userValue).format('MM/DD/YYYY');
      }

      var data = {
        elementType: ['STARTYOURQUOTE'],
        operationType: ['SAVESTARTYOURQUOTE'],
        data: [inData]
      };

      // process LOB form
      quoteService.submitLOBData(data).then(
        function () {
          $window.sessionStorage.setItem('isQuoteGenerated', 'y');
          util.setPageIndex('app.location');
          $state.go('app.location');
        },
        function (err) {
          ErrorToast.showError(err.detail);
        }
      );
    }

    vm.lobNewValue = function (value) {
      if (value == 'Yes') {
        vm.lobSelected = true;
      }
      $rootScope.lobLeftNav = true;
    };
  }
})();

(function () {
  'use strict';
  angular
    .module('quoteUi')
    .controller('QualificationController', QualificationController);

  /** @ngInject */
  function QualificationController(
    $state,
    $window,
    ErrorToast,
    $rootScope,
    Conf,
    ParseJSON,
    qualificationService,
    $location,
    Validator,
    util
  ) {
    var vm = this;
    // $rootScope.pageInfo = $location.path();
    vm.eligibilityForm = {};
    vm.eligibilityData = [];
    vm.eligibilityData.uuid = [];
    vm.appianEligibility = true;
    vm.formValidated = false;

    // methods
    vm.loadEligibilityMeta = loadEligibilityMeta;
    vm.checkReadyForSubmit = checkReadyForSubmit;
    vm.checkEligibility = checkEligibility;
    vm.getIndexBySystemRefId = getIndexBySystemRefId;
    vm.postAppianSuccess = postAppianSuccess;
    vm.setAppianRule = setAppianRule;
    vm.onRadioBtnChange = onRadioBtnChange;
    vm.triggerSubmit = triggerSubmit;
    vm.openLink = openLink;
    vm.loadChildQuestion = loadChildQuestion;
    vm.loadRenderEligibilityMeta = loadRenderEligibilityMeta;

    // $rootScope.pageInfo = [{"currentPage":
	// $location.path(),"currentPageData": vm.questions, "element":
	// ['UNDERWRITINGQUESTIONS'], "operation" :
	// ['SAVE_UW_QUESTIONS_IN_APPIAN']}];
    // $rootScope.pageInfo = [
    // {
    // currentPage: $location.path(),
    // currentPageData: vm.questions,
    // element: ['ELIGIBILTY'],
    // operation: ['SAVEELIGIBILTY']
    // }
    // ];

    $rootScope.pageInfo = [{
      currentPage: $location.path(),
      currentPageData: [],
      element: [],
      operation: [],
    }, ];

    if ($window.sessionStorage.getItem('eligibilitydet') != 'SAVE') {
      vm.params = {
        elementType: ['ELIGIBILTY'],
        operationType: ['VIEWELIGIBILTY'],
        action: ['GET'],
      };
    } else {
      vm.params = {
        elementType: ['ELIGIBILTY'],
        operationType: ['VIEWELIGIBILTY'],
        action: ['SAVE'],
      };
    }

    vm.loadEligibilityMeta();

    function loadChildQuestion(q) {
      if (q.eventAssociatedID == 1) {
        var data = {
          elementType: ['ELIGIBILTY'],
          operationType: ['SAVERERENDERELIGIBILITY'],
          action: ['SAVE'],
          data: [vm.questions]
        };
        vm.loadRenderEligibilityMeta(data);
      }
    }

    /**
	 * Open external links
	 * 
	 * @param {*}
	 *            code
	 * @param {*}
	 *            e
	 */
    function openLink(code, e) {
      e.preventDefault();
      eval(code);
    }

    function loadRenderEligibilityMeta(data) {
      qualificationService.submitEligibilityForm(data).then(
        function (res) {
          vm.questions = res.data;
          vm.questions = _.sortBy(vm.questions, 'displaySequence');
          if (vm.questions.length == 0) {
            vm.disableSubmit = true;
          }

          _.each(vm.questions, function (item) {
            _.each(item.qualificationQuestion, function (obj) {
              if (
                obj.validationName.indexOf('Required') != -1 ||
                obj.validationName.indexOf('required') != -1
              ) {
                obj.isRequired = true;
              }
              if (obj.url != null) {
                if (obj.url.search(';') != -1) {
                  obj.url = obj.url.replace(";", '');
                  // obj.url = obj.url.replace ("');", '');
                }
              }
            });
          });
        },
        function (err) {
          vm.disableSubmit = true;
          ErrorToast.showError(err.detail);
        }
      );
    }

    /**
	 * Load eligibility form meta
	 */
    function loadEligibilityMeta() {
      var data = {
        elementType: ['ELIGIBILTY'],
        operationType: ['VIEWELIGIBILTY'],
        action: ['GET'],
      };

      // call service
      qualificationService.fetchEligibilityMeta(data).then(
        function (res) {
          vm.questions = res.data;
          vm.questions = _.sortBy(vm.questions, 'displaySequence');
          if (vm.questions.length == 0) {
            vm.disableSubmit = true;
          }

          _.each(vm.questions, function (item) {
            _.each(item.qualificationQuestion, function (obj) {
              if (
                obj.validationName.indexOf('Required') != -1 ||
                obj.validationName.indexOf('required') != -1
              ) {
                obj.isRequired = true;
              }
              if (obj.url != null) {
                if (obj.url.search(';') != -1) {
                  obj.url = obj.url.replace(";", '');
                  // obj.url = obj.url.replace ("');", '');
                }
              }
            });
          });
        },
        function (err) {
          vm.disableSubmit = true;
          ErrorToast.showError(err.detail);
        }
      );
    }

    /**
	 * submit eligibility form
	 */
    function triggerSubmit(isValid, items) {
      Validator.triggerValidation(items);
      if (!isValid) {
        return false;
      }

      var data = {
        elementType: ['ELIGIBILTY'],
        operationType: ['SAVEELIGIBILTY'],
        action: ['SAVE'],
        data: [vm.questions],
      };

      qualificationService.submitEligibilityForm(data).then(
        function () {
          util.setPageIndex('app.quote');
          $state.go('app.quote');
        },
        function (err) {
          ErrorToast.showError(err.detail);
        }
      );
    }

    /**
	 * desc
	 * 
	 * @param {*}
	 *            aUUID
	 */
    function getQuestionObj(aUUID) {
      var questions = vm.questions.concat(vm.policyQuestions);
      for (var i in questions) {
        if (questions[i].uuid === aUUID) {
          return questions[i];
        }
      }
      return null;
    }

    /**
	 * desc
	 */
    function checkReadyForSubmit() {
      for (var i in vm.eligibilityData.uuid) {
        if (
          vm.eligibilityData.uuid[i] == '' ||
          vm.eligibilityData.uuid[i] == undefined
        ) {
          var questionObj = getQuestionObj(i);
          if (questionObj.appearUI.toLowerCase() !== 'n') {
            return false;
          }
        }
      }
      return true;
    }

    /**
	 * desc
	 */
    function checkEligibility() {
      var questions = vm.questions;
      for (var i in questions) {
        if (questions[i].appianRuleData.eligible == false) {
          return false;
        }
      }
      return true;
    }

    /**
	 * desc
	 * 
	 * @param {*}
	 *            aString
	 */
    function getIndexBySystemRefId(aString) {
      var questions = vm.questions;
      for (var i in questions) {
        if (questions[i].systemRefId == aString) {
          return i;
        }
      }
      return -1;
    }

    /**
	 * desc
	 * 
	 * @param {*}
	 *            questionInd
	 * @param {*}
	 *            appianResponse
	 */
    function postAppianSuccess(questionInd, appianResponse) {
      vm.questions[questionInd].appianRuleData = appianResponse;
      if (appianResponse.visible == true) {
        var question =
          vm.questions[getIndexBySystemRefId(appianResponse.SysRefId)];
        question.appearUI = 'Y';
        if (appianResponse.value != null) {
          vm.eligibilityData.uuid[question.uuid] = appianResponse.value[0];
        }
        question.userValue = vm.eligibilityData.uuid[question.uuid];
      }
    }

    /**
	 * Set appian rule
	 * 
	 * @param {*}
	 *            aIndex
	 * @param {*}
	 *            appianRule
	 * @param {*}
	 *            theRes
	 */
    function setAppianRule(aIndex, appianRule, theRes) {
      var data = {
        elementType: ['AppianRule'],
        operationType: [appianRule],
        action: ['SAVE'],
        data: [theRes],
      };
      qualificationService.checkAppianRule(data).then(
        function (res) {
          var appianResponse = res.data;
          for (var i in appianResponse) {
            postAppianSuccess(aIndex, appianResponse[i]);
          }
        },
        function (err) {
          ErrorToast.showError(err.detail);
        }
      );
    }

    /**
	 * desc
	 * 
	 * @param {*}
	 *            buttonIndex
	 */
    function onRadioBtnChange(buttonIndex) {
      var question = vm.questions[buttonIndex];
      question.userValue = vm.eligibilityData.uuid[question.uuid];
      var questionEventName = question.eventName;
      var theRes = question.userValue;
      for (var i in questionEventName) {
        var appianRule = questionEventName[i].split('RULE:')[1];
        setAppianRule(buttonIndex, appianRule, theRes);
      }
    }
  }
})();

(function () {
  'use strict';

  angular
    // pricing
    .module('quoteUi')
    .controller('PricingAndCoverageController', PricingAndCoverageController)
    .controller('AddCoverageInstanceCtrl', AddCoverageInstanceCtrl)
    .controller('CreateproposalCtrl', CreateproposalCtrl)
    .controller('AddUWContentCtrl', AddUWContentCtrl)
    .controller('AddCoverageConfirmCtrl', AddCoverageConfirmCtrl)

    .controller('AddFrequentlyCtrl', AddFrequentlyCtrl);

  /** @ngInject */
  function AddCoverageConfirmCtrl($uibModalInstance,param ) {
    var $ctrl = this;

    $ctrl.ok = function () {
      $uibModalInstance.close();
    };
    $ctrl.cancel = function () {
      $uibModalInstance.dismiss();
    };
  }





  /** @ngInject */
  function AddUWContentCtrl($uibModalInstance, param) {
    var $ctrl = this;
    $ctrl.value = param.value;
    $ctrl.coverageName = param.coverageName;
    $ctrl.followQuestObj = param.followQuestObj;
    $ctrl.getWidgetType = getWidgetType;
    $ctrl.getWidgetClass = getWidgetClass;


    addUserValue($ctrl.value.CoverageUWQuestions);

    /**
	 * Add userValue property
	 * 
	 * @param {*}
	 *            aArray
	 */
    function addUserValue(aArray) {
      _.each(aArray, function (item) {
        if (Array.isArray(item) === false && typeof item === "object" && item !== null && Object.keys(item).indexOf("userValue") === -1) {
          item.userValue = (item.defaultValue.toLowerCase() != "na") ? item.defaultValue : "";
        }
        if (Array.isArray(item.valueName)) {
          item.widgetValues = _.pluck(item.valueName, 'value');
          item.widgetCaptions = _.pluck(item.valueName, 'caption');
        }
        /*
		 * for (var property in item) { if (Array.isArray(item[property])) {
		 * addUserValue(item[property]); } }
		 */
      });
    }
    /**
	 * Get Widget Class
	 * 
	 * @param {*}
	 *            aString
	 */
    function getWidgetClass(aString) {
      var wClass = "";
      switch (aString) {
        case 'radio':
          wClass = 'checkradio-wrap';
          break;
        case 'textarea':
          wClass = 'pricing-coveragepopup form-text-wrap';
          break;
        case 'select':
          wClass = 'pricing-coveragepopup dropdown-wrap form-control';
          break;
        case 'checkbox':
          wClass = 'col-md-4 pricing-coveragepopup checkbox-wrap';
          break;
        case 'Label':
          wClass = 'pull-right';
          break;

      }
      return wClass;
    }
    /**
	 * Get Widget Type
	 * 
	 * @param aString
	 */
    function getWidgetType(aString) {
      var wType = "";
      switch (aString) {
        case 'radio':
          wType = 'radio';
          break;
        case 'textarea':
          wType = 'text';
          break;
        case 'select':
          wType = 'dropdown';
          break;
        case 'checkbox':
          wType = 'checkbox';
          break;
      }
      return wType;
    }


    $ctrl.onSubmit = function () {
      var submitVar = $ctrl.value;
      $uibModalInstance.close(submitVar);


    };
    $ctrl.onCancel = function () {
      $uibModalInstance.dismiss();
    };
  }





  /** @ngInject */
  function AddFrequentlyCtrl($uibModalInstance, param) {
    var $ctrl = this;

    $ctrl.onMainChange = onMainChange;
    $ctrl.onSubChange = onSubChange;
    $ctrl.onSearchChange = onSearchChange;

    $ctrl.onSelectAll = onSelectAll;

    $ctrl.searchValue = "";
    $ctrl.accordion = null;
    $ctrl.value = param.value;
    $ctrl.selectedAll = false;

    var selectedCoverages = [];

    addUserValue($ctrl.value);
    /**
	 * Add userValue property
	 * 
	 * @param {*}
	 *            aArray
	 */
    function addUserValue(aArray) {
      _.each(aArray, function (item) {
        if (Array.isArray(item) === false && typeof item === "object" && item !== null && Object.keys(item).indexOf("userValue") === -1) {
          item.userValue = "";
        }
        for (var property in item) {
          if (Array.isArray(item[property])) {
            addUserValue(item[property]);
          }
        }
      });
    }
    /**
	 * return selected Groups
	 * 
	 * @param {*}
	 *            aArray
	 */
    function selectedGroups(aArray) {
      return _.filter(aArray, function (aItem) {
        return aItem.userValue === true;
      });
    }

    function onSelectAll() {
      _.each($ctrl.value, function (val, aIndex) {
        // onMainChange(aIndex);
        var groupObj = $ctrl.value[aIndex];

        if ($ctrl.selectedAll != true) {
          groupObj.userValue = false;
        } else {
          var checkboxElem = document.getElementById("freq_item_btn_" + aIndex);
          checkboxElem.indeterminate = false;
          groupObj.userValue = true;
        }

        selectAllSubGroups(groupObj);
        // selectAllOnGroup();

      });
    }

    function onSearchChange() {
      for (var i in $ctrl.value) {
        var groupObj = $ctrl.value[i];
        if (groupObj.subGroups !== null) {
          selectGroupOnSG(groupObj, i);
        }
      }
    }

    function selectAllSubGroups(aGroup) {

      for (var i in aGroup.subGroups) {
        if (aGroup.userValue != true) {
          aGroup.subGroups[i].userValue = false;
        } else {
          aGroup.subGroups[i].userValue = true;
        }
      }
    }

    function selectGroupOnSG(aGroup, aParentInd) {
      var selectedSubGroups = selectedGroups(aGroup.subGroups);
      var checkboxElem = document.getElementById("freq_item_btn_" + aParentInd);
      if (checkboxElem != null && selectedSubGroups.length >= 1) {
        checkboxElem.indeterminate = true;
      } else if (checkboxElem != null && selectedSubGroups.length <= 1) {
        checkboxElem.indeterminate = false;
      }
      if (aGroup.subGroups.length === selectedSubGroups.length) {
        checkboxElem.indeterminate = false;
        aGroup.userValue = true;
      } else {
        aGroup.userValue = false;
      }

    }

    function onMainChange(aParentInd) {
      var groupObj = $ctrl.value[aParentInd];
      selectAllSubGroups(groupObj);
      selectAllOnGroup();
    }

    function getIndeterminate() {
      for (var i in $ctrl.value) {
        var numOfSel = selectedGroups($ctrl.value[i].subGroups).length;
        if (numOfSel >= 1) {
          return true;
        }
      }
      return false;
    }

    function selectAllOnGroup() {
      var selectAllBtn = document.getElementById("select_all_btn");
      selectAllBtn.indeterminate = getIndeterminate();
      var selectedGroupArry = selectedGroups($ctrl.value);
      if ($ctrl.value.length === selectedGroupArry.length) {
        selectAllBtn.indeterminate = false;
        $ctrl.selectedAll = true;
      } else {

        $ctrl.selectedAll = false;
      }

    }

    function onSubChange(aParentInd, coverageObj) {
      if (coverageObj.userValue === true && selectedCoverages.indexOf(coverageObj.coverageId) === -1) {
        selectedCoverages.push(coverageObj);
      }
      var groupObj = $ctrl.value[aParentInd];
      selectGroupOnSG(groupObj, aParentInd);
      selectAllOnGroup();
    }

    $ctrl.submit = function () {
      $uibModalInstance.close($ctrl.value);
    };
    $ctrl.cancel = function () {
      $uibModalInstance.dismiss();
    };
  }

  /** @ngInject */
  function CreateproposalCtrl($uibModalInstance, $rootScope, $location, premiumsummaryService) {
    var $ctrl = this;

    $ctrl.generatepdf = generatepdf;

    function generatepdf(ptc, pdesc) {
      var data1 = {
        elementType: ['premiumsummary'],
        operationType: ['CREATE_PROPOSAL'],
        termsandConditions: [ptc],
        ProposalDescription: [pdesc]
      };
      premiumsummaryService.createProposal(data1).then(
        function (res) {
          var proposallnk = res.data.url[0];
          window.open(proposallnk, "_blank");
          $ctrl.ok();
        },
        function () {

        }
      );
    }
    $ctrl.ok = function () {
      $uibModalInstance.close();
    };
    $ctrl.cancel = function () {
      $uibModalInstance.dismiss();
    };
  }
  /** @ngInject */
  function AddCoverageInstanceCtrl($uibModalInstance, $rootScope, $timeout, param, pricingService, ErrorToast) {
    var $ctrl = this;

    $ctrl.value = param.value;
    $ctrl.formats = ['MM/dd/yyyy', 'yyyy/MM/dd', 'shortDate'];

    $ctrl.format = $ctrl.formats[0];
    $ctrl.opened = [];
    $ctrl.buildings = [];
    $ctrl.questions = null;
    $ctrl.questionuuid = null;
    $ctrl.coverageName = param.coverageName;
    $ctrl.getWidgetType = getWidgetType;
    $ctrl.widgetChange = widgetChange;
    $ctrl.getWidgetClass = getWidgetClass;
    $ctrl.onSubmit = onSubmit;
    $ctrl.onCancel = onCancel;
    $ctrl.setQuestions = setQuestions;
    $ctrl.open = open;
    $ctrl.setQuestions();

    $ctrl.dateOptions= {
      dateDisabled: false,
      formatYear: 'yy',
      startingDay: 0,
      showButtonBar: false,
      showWeeks: false
    };



    $ctrl.checkEditable = function (aString) {
      if (aString != "y") {
        return true;
      }
      return false;
    };

    /**
	 * desc
	 * 
	 * @param {*}
	 *            index
	 */
    function open(systemRefID) {
      $timeout(function () {
        $ctrl.opened[systemRefID] = true;
      });
    }


    /**
	 * desc
	 * 
	 * @param {*}
	 *            aArray
	 */
    function displaySeqCount(aArray) {
      return _.chain(aArray).countBy(function (aObj) {
        return Number(aObj.displaySequence);
      })._wrapped;
    }

    function pluckProp(aArray, propNameStr) {
      // return _.pluck(aArray, propNameStr);
      var newArray = [];
      for (var i in aArray) {
        var item = aArray[i];
        if (item[propNameStr] !== null) {
          newArray.push(item[propNameStr]);
        }

      }
      return newArray;
    }
    /**
	 * Add userValue property
	 * 
	 * @param {*}
	 *            aArray
	 */
    function addUserValue(aArray) {
      // console.log("addUserValue....:::",aArray)
      var numofDisplaySeq = displaySeqCount(aArray);
      _.each(aArray, function (item) {
        // console.log("item....:::",item)
        if (Array.isArray(item) === false && typeof item === "object" && item !== null && Object.keys(item).indexOf("userValue") === -1) {
          (Object.keys(item).indexOf("controlName") !== -1 && item.controlName === 'Check_Box' && Array.isArray(item.valueName) === true) ? item.userValue = []: item.userValue = "";
          // (item.controlName === 'Check_Box') ? item.userValue = []:
			// item.userValue = "";

          if (item.defaultValue != null) {
            item.userValue = item.defaultValue;
          }
          item.screenColumns = (item.displaySequence !== undefined) ? 12 / numofDisplaySeq[item.displaySequence] : 12;
        }
        if (Array.isArray(item.valueName) === true) {
          item.widgetValues = pluckProp(item.valueName, 'value');
          item.widgetCaptions = pluckProp(item.valueName, 'caption');
        }


        for (var property in item) {

          if (property === "components" || property === "subComponents") {
            item[property] = sortComps(item[property]);
          }
          if (Array.isArray(item[property])) {
            addUserValue(item[property]);
          }
        }
      });
    }

    /**
	 * Sort component array
	 * 
	 * @param {*}
	 *            aCompArray
	 */
    function sortComps(aCompArray) {
      if (aCompArray !== null) {
        var sortedArry = _.sortBy(aCompArray, function (anItem) {
          return Number(anItem.displaySequence);
        });
      } else {
        return null;
      }
      return sortedArry;
    }



    /**
	 * Set questions Array
	 */
    function setQuestions() {
      addUserValue($ctrl.value.Policy.locations);
      $ctrl.value.Policy.components = sortComps($ctrl.value.Policy.components);
      addUserValue($ctrl.value.Policy.components);
      console.log($ctrl.value.Policy);
    }

    function onAppianSuccess(aObject, aArray) {
      // console.log(aObject[0])
      _.each(aArray, function (item) {
        if (Array.isArray(item) === false && typeof item === "object" && item !== null && Object.keys(item).indexOf("systemRefID") !== -1 && item.systemRefID === aObject[0].sysRefId) {
          if (Array.isArray(aObject[0].value) === true && typeof aObject[0].value[0] === "string" && aObject[0].value[0] === "None") {
            item.editable = "N";
          }
          if (Array.isArray(aObject[0].value) === true && typeof aObject[0].value[0] !== "string" && item.controlName.toLowerCase() !== "date") {
            item.editable = "Y";
            item.valueName = aObject[0].value;
            item.widgetCaptions = pluckProp(aObject[0].value, 'caption');
            item.widgetValues = pluckProp(aObject[0].value, 'value');
          }
          if (aObject[0].error != null) {
            item.validationError = aObject[0].error;
          }
        } else if (typeof item === "object") {
          for (var property in item) {
            if (Array.isArray(item[property])) {
              item[property] = onAppianSuccess(aObject, item[property]);
            }
          }

        }

      });
      return aArray;
    }


    /**
	 * Save P&C JSON
	 */

    function callAppian(operationType, aData) {
      // console.log("callAppian...:::", aData);
      var data = {
        data: [aData],
        elementType: ['APPIANRULE'],
        operationType: [operationType]
      };
      pricingService.getAppianData(data).then(
        function (res) {
          console.log("Success::::", res.data);
          $ctrl.value = onAppianSuccess(res.data, $ctrl.value);
          // console.log($ctrl.value);
        },
        function (err) {
          ErrorToast.showError(err.detail);
          // console.log(err.detail)
        }
      );
    }
    /**
	 * On Widget Value Change
	 */
    function widgetChange(aObject) {
      $timeout(function () {
        // console.log("Appian......:::", aObject.userValue)
        if (aObject.eventName != null) {
          callAppian(aObject.eventName[0].split("RULE:")[1], aObject.userValue);
        }
      }, 1);
    }
    /**
	 * Get Widget Class
	 * 
	 * @param {*}
	 *            aString
	 */
    function getWidgetClass(aString) {
      var wClass = "";
      switch (aString) {
        case 'Radio_Button':
          wClass = 'checkradio-wrap';
          break;
        case 'Text_Box':
          wClass = 'pricing-coveragepopup form-text-wrap';
          break;
        case 'Drop_Down':
          wClass = 'pricing-coveragepopup dropdown-wrap form-control';
          break;
        case 'Check_Box':
          wClass = 'col-md-4 pricing-coveragepopup checkbox-wrap';
          break;
        case 'Label':
          wClass = 'pull-right';
          break;

      }
      return wClass;
    }
    /**
	 * Get Widget Type
	 * 
	 * @param aString
	 */
    function getWidgetType(aString) {
      var wType = "";
      switch (aString) {
        case 'Radio_Button':
          wType = 'radio';
          break;
        case 'Text_Box':
          wType = 'text';
          break;
        case 'Drop_Down':
          wType = 'dropdown';
          break;
        case 'Check_Box':
          wType = 'checkbox';
          break;
      }
      return wType;
    }
    /**
	 * On form submit
	 */
    function onSubmit() {
      // console.log("onSubmit...:::");
      var submitVar = $ctrl.value;
      $uibModalInstance.close(submitVar);
    }
    /**
	 * On form cancel
	 */
    function onCancel() {
      $uibModalInstance.dismiss();
    }
  }

  /** @ngInject */
  function PricingAndCoverageController($uibModal, pricingService, $rootScope, $location, $window, ErrorToast, util, $state) {
    var vm = this;

    vm.pricingData = [];

    var selectedClass = "selected-plan-bg";
    var selectedFooterClass = "footer-selected-plan";
    var disabledClass = "plan-disabled";
    var addedCoverageObjs = [];
    var addedCoverageInd = 0;

    vm.stdPlanSelected = "true";
    vm.tldPlanSelected = "false";
    vm.currencyType = "$";
    vm.tailerValueModified = false;

    vm.stdRadioDisabled = false;

    vm.stdPlanSelectedTxt = "blueLink";
    vm.tldPlanSelectedTxt = "blackLink";

    vm.stdPlanClass = selectedClass;
    vm.tldPlanClass = disabledClass;

    vm.stdPlanFooterClass = selectedFooterClass;
    vm.tldPlanFooterClass = disabledClass;


    // methods
    vm.fetchPricingDetails = fetchPricingDetails;
    vm.getGroupRowHeight = getGroupRowHeight;
    vm.openComponentModal = openComponentModal;
    vm.onApplyButton = onApplyButton;
    vm.onFreqDelete = onFreqDelete;
    
    vm.onCreateProposal = onCreateProposal;
    vm.planIndex = 0;
    vm.onStdPlan = onStdPlan;
    vm.onTldPlan = onTldPlan;
    vm.openAddFreqModal = openAddFreqModal;
    vm.goToPremiumSummary = goToPremiumSummary;
    vm.goToRefer = goToRefer;
    vm.widgetChange = widgetChange;
    vm.onCoverageCheck = onCoverageCheck;
    vm.getWidgetClass = getWidgetClass;
    vm.getWidgetType = getWidgetType;

    vm.fetchPricingDetails();

    $rootScope.appQuoteBindable = true;

    $rootScope.pageInfo = [{
      "currentPage": $location.path(),
      "currentPageData": vm.pricingData,
      "element": ['PRICINGANDCOVERAGE'],
      "operation": ['GETCOVERAGES']
    }];

    function onFreqDelete(aGroupIndex, aSubGroupIndex){
      var value = '';
      var modalInstance = $uibModal.open({
        templateUrl: 'AddCoverageConfirm.html',
        controller: 'AddCoverageConfirmCtrl',
        controllerAs: '$ctrl',
        windowClass: 'AddCoverageConfirm',
        size: 'lg',
        resolve: {
          param: function () {
            return {
              value: value
            };
          }
        }
      });
      modalInstance.result.then(
        function () {
          if(aSubGroupIndex===null){
            vm.pricingData[1].groups.splice(aGroupIndex,1);
          }else if(aSubGroupIndex!==null){
            vm.pricingData[1].groups[aGroupIndex].subGroups.splice(aSubGroupIndex,1);
          }
        },
        function () {}
      );





    }

    function onCoverageCheck(){
      vm.tailerValueModified = true;
    }
    function onApplyButton(){
      vm.tailerValueModified = false;
      var data = {
        data: [vm.pricingData],
        elementType: ['PRICINGANDCOVERAGE'],
        operationType: ['APPLYPRICE']
      };
      pricingService.submitPricingData(data).then(
        function () {
          // console.log("Success");
          updatePriceData(res.data);

        },
        function (err) {
          ErrorToast.showError(err.detail);
          // console.log(err.detail)
        }
      );



    }
    function widgetChange(aObject){
      vm.tailerValueModified = true;
    }
    /**
	 * Add userValue property
	 * 
	 * @param {*}
	 *            aArray
	 */
    function addUserValue(aArray) {
      _.each(aArray, function (item) {
        if (Array.isArray(item) === false && typeof item === "object" && item !== null && Object.keys(item).indexOf("userValue") === -1) {
          item.userValue = "";
          (Object.keys(item).indexOf("compControlName") !== -1 && item["compControlName"] === "Placeholder_Text")? item.userValue = item.compDisplayValue[0] : item.userValue = "";
        }
        if (Array.isArray(item) === false && typeof item === "object" && item !== null && Object.keys(item).indexOf("mandatoryCoverage") !== -1) {
          if(Object.keys(item).indexOf("coverageIncluded") === -1){
            item.coverageIncluded = true;
          }
        }
        for (var property in item) {
          if (Array.isArray(item[property])) {

            addUserValue(item[property]);
          }
        }
      });
    }


    /**
	 * Get Widget Class
	 * 
	 * @param {*}
	 *            aString
	 */
    function getWidgetClass(aString) {
      var wClass = "";
      switch (aString) {
        case 'Radio_Button':
          wClass = 'checkradio-wrap';
          break;
        case 'Text_Box':
          wClass = 'pricing-coveragepopup form-text-wrap';
          break;
        case 'Drop_Down':
          wClass = 'pricing-coveragepopup dropdown-wrap form-control';
          break;
        case 'Check_Box':
          wClass = 'col-md-4 pricing-coveragepopup checkbox-wrap';
          break;
        case 'Label':
          wClass = 'pull-right';
          break;
          case 'Placeholder_Text':
          wClass = '';
          break;



      }
      return wClass;
    }
    /**
	 * Get Widget Type
	 * 
	 * @param aString
	 */
    function getWidgetType(aString) {
      var wType = "";
      switch (aString) {
        case 'Radio_Button':
          wType = 'radio';
          break;
        case 'Text_Box':
          wType = 'text';
          break;
        case 'Drop_Down':
          wType = 'dropdown';
          break;
        case 'Check_Box':
          wType = 'checkbox';
          break;
          case 'Placeholder_Text':
          wType = 'label';
          break;


      }
      return wType;
    }




    function goToPremiumSummary() {
      saveCoverage('app.premiumsummary');
      // util.setPageIndex('app.premiumsummary');
      // $state.go('app.premiumsummary');
    }

    function goToRefer() {
      saveCoverage('app.refer');
      // util.setPageIndex('app.refer');
      // $state.go('app.refer');
    }
    /**
	 * Save P&C JSON
	 */

    function saveCoverage(aString) {
      // if (vm.planIndex === 0) {
      var data = {
        data: [vm.pricingData],
        elementType: ['PRICINGANDCOVERAGE'],
        operationType: ['SAVECOVERAGE']
      };
      pricingService.submitPricingData(data).then(
        function () {
          // console.log("Success");
          util.setPageIndex(aString);
          $state.go(aString);
        },
        function (err) {
          ErrorToast.showError(err.detail);
          // console.log(err.detail)
        }
      );
      // }
    }
    /**
	 * On Standard Plan
	 */
    function onStdPlan() {
      vm.planIndex = 0;
      vm.stdPlanSelectedTxt = "blueLink";
      vm.tldPlanSelectedTxt = "blackLink";
      vm.tldPlanSelected = "false";
      vm.pricingData[1].userValue = vm.tldPlanSelected;
      vm.stdPlanClass = selectedClass;
      vm.tldPlanClass = disabledClass;

      vm.stdPlanFooterClass = selectedFooterClass;
      vm.tldPlanFooterClass = disabledClass;
    }

    /**
	 * On Tailored Plan
	 */
    function onTldPlan() {
      vm.pricingData[1].userValue = vm.tldPlanSelected;
      vm.planIndex = 1;
      vm.stdPlanSelectedTxt = "blackLink";
      vm.tldPlanSelectedTxt = "blueLink";
      vm.stdPlanSelected = "false";
      vm.stdRadioDisabled = true;
      vm.stdPlanClass = disabledClass;
      vm.tldPlanClass = selectedClass;

      vm.stdPlanFooterClass = disabledClass;
      vm.tldPlanFooterClass = selectedFooterClass;
    }

    function sequencePlans() {

      var dataArry = angular.copy(vm.pricingData);
      for (var i in dataArry) {
        if (dataArry[i].packageName === "Standard Plan") {
          // console.log(i + "....:", dataArry[i].packageName);
          vm.pricingData[0] = dataArry[i];
        }
        if (dataArry[i].packageName === "Tailored Plan") {
          // console.log(i + "....:", dataArry[i].packageName);
          vm.pricingData[1] = dataArry[i];
         // addUserValue(vm.pricingData[1].groups);
        }
        if (dataArry[i].packageName === "Frequently Added Coverages") {
          // console.log(i + "....:", dataArry[i].packageName);
          vm.pricingData[2] = dataArry[i];
        }


      }
      addUserValue(vm.pricingData);

     // console.log("...:::", vm.pricingData);

    }

    function openNextCoverageModel(){
      var coveragesLen = addedCoverageObjs.length-1;
      if (addedCoverageObjs.length !== 0 && addedCoverageInd < coveragesLen) {
        addedCoverageInd++;
        openComponentModal(addedCoverageObjs[addedCoverageInd]);
      }


    }


    function updatePriceData(aData){
      vm.pricingData = aData;
      sequencePlans();
      vm.pricingData[0].groups = sortGroup(vm.pricingData[0].groups);
      vm.pricingData[1].groups = sortGroup(vm.pricingData[1].groups);

      if(vm.pricingData[1].userValue==="true"){
        vm.tldPlanSelected = vm.pricingData[1].userValue;
        onTldPlan();
      }

      console.log("updatePriceData.....:::",vm.pricingData);

    }

    function postUWPopupReqFail(aObject) {
      var data = {
        operationType: ['GETCOVERAGES'],
        elementType: ['PRICINGANDCOVERAGE'],
        data: [vm.pricingData],
        coverageName: [aObject.coverageName],
        coverageId: [aObject.coverageId]
      };
      pricingService.getPricingData(data).then(
        function (res) {
          // console.log(".res.data..:::", res.data);

          vm.pricingData = res.data;

          if(vm.pricingData[vm.planIndex].quoteBindable.toLowerCase() !== 'true' || vm.pricingData[vm.planIndex].AgencyValid.toLowerCase() !== 'true') {
             $rootScope.appQuoteBindable = false;
          }

          // vm.bbb = vm.pricingData[vm.planIndex].quoteBindable;
          console.log("bbb..." + $rootScope.appQuoteBindable);

        // sequencePlans();
        // vm.pricingData[0].groups = sortGroup(vm.pricingData[0].groups);
        // vm.pricingData[1].groups = sortGroup(vm.pricingData[1].groups);
          updatePriceData(res.data);
         // console.log("postUWPopupReqFail.....:::",vm.pricingData);
          openNextCoverageModel();
          
        },
        function () {
          // console.log('error', err);
        }
      );


    }
    /**
	 * On UW Popup
	 */
    function onUWPopup(aObject, followQuestObj) {
      var data = {
        elementType: ["PRICINGANDCOVERAGE"],
        operationType: ["fetchUWQuestions"],
        header: [aObject.coverageName]
      };
      pricingService.getUWData(data).then(
        function (res) {
          if (res.data.reload !== true && Object.keys(res.data).indexOf("reload")) {
            var value = res.data;
            var modalInstance = $uibModal.open({
              templateUrl: 'AddUWContent.html',
              controller: 'AddUWContentCtrl',
              controllerAs: '$ctrl',
              windowClass: 'AddUWContentpopup',
              size: 'lg',
              resolve: {
                param: function () {
                  return {
                    value: value,
                    followQuestObj: followQuestObj,
                    coverageName: aObject.coverageName
                  };
                }
              }
            });
            modalInstance.result.then(
              function (submitVar) {

                // console.log("childuwquestions..........:", submitVar);

                var data = {
                  data: [submitVar],
                  elementType: ['PRICINGANDCOVERAGE'],
                  operationType: ['childuwquestions']
                };


                pricingService.submitModalData(data).then(
                  function () {
                    // console.log("Success");
                    openNextCoverageModel();

                  },
                  function (err) {
                    ErrorToast.showError(err.detail);
                    // console.log(err.detail)
                  }
                );
      

              },
              function () {}
            );
          } else {
            postUWPopupReqFail(aObject);
          }


        },
        function () {
          // console.log('error', err);
        }
      );



    }


    /**
	 * On Create proposal button
	 */
    function onCreateProposal() {
      var value = '';
      var modalInstance = $uibModal.open({
        templateUrl: 'Createproposal.html',
        controller: 'CreateproposalCtrl',
        controllerAs: '$ctrl',
        windowClass: 'Createproposalpopup',
        size: 'lg',
        resolve: {
          param: function () {
            return {
              value: value
            };
          }
        }
      });
      modalInstance.result.then(
        function () {},
        function () {}
      );
    }

    function returnSelectedObjs(aArray) {
      var selectedObjs = [];
      for (var a in aArray) {
        if (aArray[a].subGroups !== null) {
          for (var b in aArray[a].subGroups) {
            if (aArray[a].subGroups[b].userValue === true) {
              selectedObjs.push(aArray[a].subGroups[b]);
            }
          }
        } else {
          if (aArray[a].userValue === true) {
            selectedObjs.push(aArray[a]);
          }
        }
      }
      return selectedObjs;
    }
    /**
	 * On Add a coverage button
	 */
    function openAddFreqModal() {
      onTldPlan();
      var data = {
        elementType: ["PRICINGANDCOVERAGE"],
        operationType: ["GETOPTIONALS"]
      };
      pricingService.getAddCoveragesData(data).then(
        function (res) {
          if (res.data !== 'null') {
            var value = res.data;
            var modalInstance = $uibModal.open({
              templateUrl: 'AddFrequently.html',
              controller: 'AddFrequentlyCtrl',
              controllerAs: '$ctrl',
              windowClass: 'AddFrequentlyPopup',
              size: 'lg',
              resolve: {
                param: function () {
                  return {
                    value: value
                  };
                }
              }
            });
            modalInstance.result.then(
              function (submitVar) {
                var selectedObjs = returnSelectedObjs(submitVar);
                addedCoverageObjs = selectedObjs;
            
                // console.log(selectedObjs);
                if (addedCoverageObjs.length !== 0) {
                  openComponentModal(addedCoverageObjs[addedCoverageInd]);
                }

              },
              function () {}
            );
          } else {
            vm.fetchPricingDetails();
          }


        },
        function () {
          // console.log('error', err);
        }
      );
    }
    /**
	 * Open Popup
	 */
    function openComponentModal(aObject) {
    // console.log("openComponentModal.....:::",aObject);
      vm.tldPlanSelected = "true";
      onTldPlan();
      var data = {
        elementType: ["PRICINGANDCOVERAGE"],
        operationType: ["GETCOMPONENTS"],
        coverageName: [aObject.coverageName],
        coverageId: [aObject.coverageId]
      };
      pricingService.getModalData(data).then(
        function (res) {
          if (res.data.reload !== true && Object.keys(res.data).indexOf("reload")) {
            var value = res.data;
            // if (vm.tldPlanSelected === 'true') {
            var modalInstance = $uibModal.open({
              templateUrl: 'AddCoverageContent.html',
              controller: 'AddCoverageInstanceCtrl',
              controllerAs: '$ctrl',
              windowClass: 'pricing-coveragepopup',
              size: 'lg',
              resolve: {
                param: function () {
                  return {
                    value: value,
                    coverageName: aObject.coverageName
                  };
                }
              }
            });
            modalInstance.result.then(
              function (submitVar) {
                // console.log("SAVECOMPONENTS..........:", submitVar);

                var data = {
                  data: [submitVar],
                  elementType: ['PRICINGANDCOVERAGE'],
                  operationType: ['SAVECOMPONENTS']
                };
                pricingService.submitModalData(data).then(
                  function () {
                    onUWPopup(aObject, submitVar);
                  },
                  function (err) {
                    ErrorToast.showError(err.detail);
                    // console.log(err.detail)
                    onUWPopup(aObject, submitVar);
                  }
                );
              },
              function () {
                // console.log('modal-component dismissed at: ' + new Date());
              }
            );
            // }
          } else {
            // vm.fetchPricingDetails();
            postUWPopupReqFail(aObject);
          }
        },
        function () {
          // console.log('error', err);
        }
      );
    }
    /**
	 * Get Maximum value length
	 * 
	 * @param {*}
	 *            aObject
	 * @param {*}
	 *            aIndex
	 */
    function getSubGroupCompLength(aObject, aGroupIndex, aSubGroupIndex) {
      if (aObject.groups.length > aGroupIndex) {
        var screenDiff = ($window.innerWidth < 1338 && $window.innerWidth > 991) ? 1337 - $window.innerWidth : 0;
        var textLeng = 40 - screenDiff;
        // var components =
		// aObject.groups[aGroupIndex].subGroups[aSubGroupIndex].components;
        var subGroups = _.filter(aObject.groups[aGroupIndex].subGroups, function (aSubGroup) {
          return aSubGroup.appearUI.toLowerCase() !== 'n';
        });
        var components = subGroups[aSubGroupIndex].components;
        if (aObject.groups[aGroupIndex].datasetName === "Extension Endorsement") {
          // console.log("subGroups[aSubGroupIndex].datasetName...:::",
			// subGroups[aSubGroupIndex].datasetName);
          // console.log("subGroups...:::", subGroups.length);
        }
        // var hiddenComponents = 0;//_.filter(components, function(aComponent){
		// return aComponent.compAppearUI.toLowerCase() == 'n'; });
        var longCompVals = 0; // _.filter(components, function(aComponent){
								// return aComponent.compDisplayValue[0].length
								// > textLeng &&
								// aComponent.compAppearUI.toLowerCase() !==
								// 'n'; });
        var numOfHelpTexts = 0;
        var numOfNonHelpTexts = 0;
        _.each(components, function (aComponent) {
          // if(aComponent.compAppearUI.toLowerCase() ===
			// 'n'){hiddenComponents++;}
          if (aComponent.compDisplayValue[0].length > textLeng && aComponent.compAppearUI.toLowerCase() !== 'n' && aComponent.compShowAsHelp.toLowerCase() === 'y') {
            longCompVals++;
          }
          if (aComponent.compAppearUI.toLowerCase() !== 'n' && aComponent.compShowAsHelp.toLowerCase() === 'y') {
            numOfHelpTexts++;
          }
          if (aComponent.compAppearUI.toLowerCase() !== 'n' && aComponent.compShowAsHelp.toLowerCase() !== 'y') {
            numOfNonHelpTexts++;
          }
        });
        // var numOfHiddenComps = hiddenComponents - 1;
        var numOfComps = (numOfHelpTexts >= numOfNonHelpTexts) ? numOfHelpTexts + 1 : numOfNonHelpTexts; // components.length
        /*
		 * if(aObject.groups[aGroupIndex].subGroups[aSubGroupIndex].datasetName==="Business
		 * Income From Dependent Properties"){
		 * console.log(aObject.groups[aGroupIndex].subGroups[aSubGroupIndex].datasetName);
		 * console.log({aGroupIndex:aGroupIndex,aSubGroupIndex:aSubGroupIndex,longCompVals:longCompVals,numOfHelpTexts:numOfHelpTexts,numOfNonHelpTexts:numOfNonHelpTexts,numOfComps:numOfComps}); }
		 */
        numOfComps = numOfComps + longCompVals;
        return numOfComps; // - numOfHiddenComps;
      }
      return 0;
    }
    /**
	 * Get Maximum value length
	 * 
	 * @param {*}
	 *            aGroupIndex
	 * @param {*}
	 *            aSubGroupIndex
	 */
    function getMaxValueLength(aGroupIndex, aSubGroupIndex) {
      var StdLength = getSubGroupCompLength(vm.pricingData[0], aGroupIndex, aSubGroupIndex);
      var tailrdLength = getSubGroupCompLength(vm.pricingData[1], aGroupIndex, aSubGroupIndex);
      if (tailrdLength > StdLength) {
        return tailrdLength;
      }
      return StdLength;
    }

    /**
	 * Group row height
	 * 
	 * @param {*}
	 *            aGroupIndex
	 * @param {*}
	 *            aSubGroupIndex
	 */
    function getGroupRowHeight(aGroupIndex, aSubGroupIndex) {
      var numberOfRows = getMaxValueLength(aGroupIndex, aSubGroupIndex);
      var subGroups = _.filter(vm.pricingData[vm.planIndex].groups[aGroupIndex].subGroups, function (aSubGroup) {
        return aSubGroup.appearUI.toLowerCase() !== 'n';
      });
      var dataSetNameLeng = subGroups[aSubGroupIndex].datasetName.length;
      var heightObj = null;
      var rowHeight = 35;
      if (numberOfRows > 1) {
        var heightVal = numberOfRows * rowHeight;
        if (dataSetNameLeng > 42) {
          heightVal = heightVal + rowHeight;
        }
        heightObj = {
          height: heightVal + 'px'
        };
      }
      heightObj = {
        height: heightVal + 'px'
      };
      // }
      return heightObj;
    }
    /**
	 * Sort group
	 * 
	 * @param {*}
	 *            aGroupArray
	 */
    function sortGroup(aGroupArray) {
      var sortedArry = _.sortBy(aGroupArray, function (anItem) {
        if (anItem.subGroups === null) {
          return anItem.groupDisplaySequence;
        }
        anItem.subGroups = sortSubGroup(anItem.subGroups);
        return anItem.subGroups[0].groupDisplaySequence;
      });
      return sortedArry;
    }
    /**
	 * Sort sub group
	 * 
	 * @param {*}
	 *            aGroupArray
	 */
    function sortSubGroup(aGroupArray) {
      var sortedArry = _.sortBy(aGroupArray, function (anItem) {
        if (anItem.components !== null) {
          anItem.components = sortComps(anItem.components);
        }
        return anItem.coverageDisplaySequence;
      });
      return sortedArry;
    }
    /**
	 * Sort component array
	 * 
	 * @param {*}
	 *            aCompArray
	 */
    function sortComps(aCompArray) {
      var sortedArry = _.sortBy(aCompArray, function (anItem) {
        return anItem.compDisplaySequence;
      });
      return sortedArry;
    }
    /**
	 * Fetch pricing data
	 */
    function fetchPricingDetails() {
      var data = {
        operationType: ['GETCOVERAGES'],
        elementType: ['PRICINGANDCOVERAGE']
      };
      pricingService.getPricingData(data).then(
        function (res) {
          // console.log(".res.data..:::", res.data);

          vm.pricingData = res.data;

          // vm.aaa = vm.pricingData[vm.planIndex].quoteBindable;

          if(vm.pricingData[vm.planIndex].quoteBindable.toLowerCase() !== 'true' || vm.pricingData[vm.planIndex].AgencyValid.toLowerCase() !== 'true') {
             $rootScope.appQuoteBindable = false;
          }

          console.log("aaa..." + $rootScope.appQuoteBindable);


          if (_.has(vm.pricingData, 'stackTrace')) {
            vm.serverError = true;
            ErrorToast.showError('Server exception.');
            return;
          } else {
            vm.serverError = false;
          }
        // sequencePlans();
        // vm.pricingData[0].groups = sortGroup(vm.pricingData[0].groups);
        // vm.pricingData[1].groups = sortGroup(vm.pricingData[1].groups);
          updatePriceData(res.data);
        },
        function (err) {
          ErrorToast.showError(err.detail);
        }
      );
    }
  }
})();

(function () {
  'use strict';

  angular
    //
    .module('quoteUi')
    .controller('PremiumSummaryController', PremiumSummaryController)
    .controller('CreateproposalCtrl', CreateproposalCtrl);
  /** @ngInject */
  function CreateproposalCtrl($uibModalInstance, $rootScope, $location, premiumsummaryService) {
    var $ctrl = this;
    $rootScope.pageInfo = $location.path();
    $ctrl.generatepdf = generatepdf;

    function generatepdf(ptc, pdesc) {
      var data1 = {
        elementType: ['premiumsummary'],
        operationType: ['CREATE_PROPOSAL'],
        termsandConditions: [ptc],
        ProposalDescription: [pdesc]
      };
      premiumsummaryService.createProposal(data1).then(
        function (res) {
          var proposallnk = res.data.url[0];
          window.open(proposallnk, "_blank");
          $ctrl.ok();
        },
        function () {

        }
      );
    }
    $ctrl.ok = function () {
      $uibModalInstance.close();
    };
    $ctrl.cancel = function () {
      $uibModalInstance.dismiss();
    };
  }


  /** @ngInject */
  function PremiumSummaryController($uibModal, premiumsummaryService, $location, $state, $rootScope, util) {
    var vm = this;
    vm.premiumsummaryData = [];
    vm.premiumresponsedata=[];
    vm.premiumcovgData=[];
    vm.openComponentModal = openComponentModal;
    vm.goToIssuePolicy = goToIssuePolicy;
    vm.maskCurrency = maskCurrency;
    getFormData();
    // vm.premiumsummaryValues = {
    // premiumsummaryData: vm.premiumsummaryData
    // };

    $rootScope.pageInfo = [{
      "currentPage": $location.path(),
      "currentPageData": [vm.premiumresponsedata],
      "element": ['PREMIUMSUMMARY'],
      "operation": ['SAVE_AND_EXIT_PREMIUM_SUMMARY']
    }];
    $rootScope.$on('onsave', function () {
      $rootScope.pageInfo = [{
        "currentPage": $location.path(),
        "currentPageData": [vm.premiumresponsedata],
        "element": ["PREMIUMSUMMARY"],
        "operation": ["SAVE_AND_EXIT_PREMIUM_SUMMARY"]
      }];
    });

    /* function for getting the premium-summary formdata */
    function getFormData() {
      var data = {
        elementType: ['PREMIUMSUMMARY'],
        operationType: ['VIEW_PREMIUM_SUMMARY']
      };
      premiumsummaryService.getPremiumsummaryData(data).then(
        function (response) {
          vm.premiumresponsedata = response.data;
          vm.premiumcovgData=vm.premiumresponsedata[1];
          for(var k=0;k<vm.premiumcovgData.length;k++){
            if(vm.premiumcovgData[k].packageName=='Tailored Plan')
            vm.premiumsummaryData=vm.premiumcovgData[k];
          }
          vm.quoteAmount = vm.maskCurrency(vm.premiumresponsedata[0].QuoteAmount);
          vm.startDate = vm.premiumresponsedata[0].startDate;
          vm.endDate = vm.premiumresponsedata[0].endDate;
          vm.premiumsummaryData.groups = sortGroup(vm.premiumsummaryData.groups);
        },
        function () {}
      );
    }

    /**
	 * Sort group
	 * 
	 * @param {*}
	 *            aGroupArray
	 */
    function sortGroup(aGroupArray) {
      var sortedArry = _.sortBy(aGroupArray, function (anItem) {
        if (anItem.subGroups === null) {
          return anItem.groupDisplaySequence;
        }
        anItem.subGroups = sortSubGroup(anItem.subGroups);
        return anItem.subGroups[0].groupDisplaySequence;
      });
      return sortedArry;
    }
    /**
	 * Sort sub group
	 * 
	 * @param {*}
	 *            aGroupArray
	 */
    function sortSubGroup(aGroupArray) {
      var sortedArry = _.sortBy(aGroupArray, function (anItem) {
        if (anItem.components !== null) {
          anItem.components = sortComps(anItem.components);
        }
        return anItem.coverageDisplaySequence;
      });
      return sortedArry;
    }
    /**
	 * Sort component array
	 * 
	 * @param {*}
	 *            aCompArray
	 */
    function sortComps(aCompArray) {
      var sortedArry = _.sortBy(aCompArray, function (anItem) {
        return anItem.compDisplaySequence;
      });
      return sortedArry;
    }

    /* start modal window for create proposal */
    function openComponentModal() {
      var value = 'test value';
      var modalInstance = $uibModal.open({
        templateUrl: 'Createproposal.html',
        controller: 'CreateproposalCtrl',
        controllerAs: '$ctrl',
        windowClass: 'Createproposalpopup',
        size: 'lg',
        resolve: {
          param: function () {
            return {
              value: value
            };
          }
        }
      });
      modalInstance.result.then(
        function () {},
        function () {}
      );
    }
    /* end modal window for create proposal */

    /* quote amount seperate dby coma's */
    function maskCurrency(input) {
      var temp1;
      temp1 = input.toString();
      temp1 = temp1.replace(/-/g, '');
      return temp1.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    }

    function goToIssuePolicy() {
      util.setPageIndex('app.beforeyouissue');
      $state.go('app.beforeyouissue');
    }

  }
})();

(function () {
  'use strict';
  angular
    // location

    .module ('quoteUi')
    .controller ('LocationController', LocationController)
    // controller for modal

    .controller ('locationRemoveCtrl', function (
      $uibModalInstance,
      param,
      $rootScope,
      Validator,
      ParseJSON,
      ErrorToast,
      locationService,
      $log
    ) {
      var $locRemCtrl = this;

      $locRemCtrl.locationId = param.locationId;
      $locRemCtrl.locIndex = param.locIndex;

      $locRemCtrl.locRemoveConfirm = locRemoveConfirm;

      $locRemCtrl.locCancel = locCancel;

      /**
		 * 
		 * Close the modal if Yes button click
		 * 
		 */

      function locRemoveConfirm () {
        var locRemObj = {};

        locRemObj.locationId = $locRemCtrl.locationId;
        locRemObj.locIndex = $locRemCtrl.locIndex;

        var data = {
          elementType: ['RISK'],

          operationType: ['DELETELOCATION'],

          data: [locRemObj],
        };
        // call service

        locationService.deleteLocations (data).then (
          function () {
            $rootScope.$emit ('CallParentMethod', {});

            $uibModalInstance.close ();
          },
          function (err) {
            $log.info (err);

            $rootScope.$emit ('CallParentMethod', {});

            $uibModalInstance.dismiss ();

            // ErrorToast.showError(err.detail);
          }
        );
      }

      /**
		 * 
		 * Dismiss the modal if No button click
		 * 
		 */

      function locCancel () {
        // $rootScope.$emit('CallParentMethod', {});

        $uibModalInstance.dismiss ();
      }
    })
    // controller for modal

    .controller ('buildingRemoveCtrl', function (
      $uibModalInstance,
      param,
      $rootScope,
      Validator,
      ParseJSON,
      ErrorToast,
      locationService,
      $log
    ) {
      var $buildRemCtrl = this;

      $buildRemCtrl.locationId = param.locationId;

      $buildRemCtrl.buildingId = param.buildingId;
      $buildRemCtrl.buildIndex = param.buildIndex;
      $buildRemCtrl.locIndex = param.locIndex;

      $buildRemCtrl.buildRemoveConfirm = buildRemoveConfirm;

      $buildRemCtrl.buildCancel = buildCancel;

      /**
		 * 
		 * Close the modal if Yes button click
		 * 
		 */

      function buildRemoveConfirm (
        locationId,
        buildingId,
        buildIndex,
        locIndex
      ) {
        var buildRemObj = {};

        buildRemObj.locationId = locationId;

        buildRemObj.buildingId = buildingId;

        buildRemObj.buildIndex = buildIndex;

        buildRemObj.locIndex = locIndex;

        var data = {
          elementType: ['RISK'],

          operationType: ['DELETEBUILDING'],

          data: [buildRemObj],
        };

        // console.log("data build",data);

        // call service

        locationService.deleteLocations (data).then (
          function () {
            $rootScope.$emit ('CallParentMethod', {});
            $uibModalInstance.close ();
          },
          function (err) {
            $log.info (err);
            $rootScope.$emit ('CallParentMethod', {});

            $uibModalInstance.dismiss ();

            // ErrorToast.showError(err.detail);
          }
        );
      }

      /**
		 * 
		 * Dismiss the modal if No button click
		 * 
		 */

      function buildCancel () {
        // $rootScope.$emit('CallParentMethod', {});

        $uibModalInstance.dismiss ();
      }
    })
    .controller ('editConfirmCtrl', function (
      $uibModalInstance,
      param,
      $rootScope,
      Validator,
      ParseJSON,
      ErrorToast,
      locationService
    ) {
      var $editConfirmCtrl = this;

      $editConfirmCtrl.editConfirm = editConfirm;

      $editConfirmCtrl.editCancel = editCancel;
      $editConfirmCtrl.locationDataRes = param.locationDataRes;
      $editConfirmCtrl.locID = param.locationIndex.locIndex;
      $editConfirmCtrl.formSendData = param.formSendData;

      // $rootScope.pageInfo = [{"currentPage":
		// $location.path(),"currentPageData":
		// $editConfirmCtrl.formSendData.data, "element":
		// $editConfirmCtrl.formSendData.elementType, "operation" :
		// $editConfirmCtrl.formSendData.operationType,
      // "subjectType": $editConfirmCtrl.formSendData.subjectType
      // }];

      /**
		 * 
		 * Close the modal if Yes button click
		 * 
		 */

      function editConfirm () {
        $editConfirmCtrl.formSendData.locationIndex = [
          $editConfirmCtrl.locID - 1,
        ];
        // var data = {
        // subjectType:['EDIT'],
        // elementType: ['RISK'],
        // operationType: ['SAVELOCATION'],
        // data: [$editConfirmCtrl.locationDataRes[0]],
        // locationIndex:[$editConfirmCtrl.locID - 1]
        // };

        // call service
        locationService.saveLocations ($editConfirmCtrl.formSendData).then (
          function () {
            $editConfirmCtrl.formSendData = {
              elementType: null,
              operationType: null,
              data: null,
            };
            $rootScope.$emit ('editConfirm', {});
            $rootScope.$emit ('CallParentMethod', {});
            $uibModalInstance.close ();
          },
          function () {
            $uibModalInstance.dismiss ();
            // console.log('save location - error', err.detail);
          }
        );
      }

      /**
		 * 
		 * Dismiss the modal if No button click
		 * 
		 */

      function editCancel () {
        $uibModalInstance.dismiss ();
      }
    });

  /** @ngInject */

  function LocationController (
    USStateService,
    $scope,
    $rootScope,
    $uibModal,
    $window,
    $log,
    $state,
    Conf,
    Validator,
    ParseJSON,
    ErrorToast,
    locationService,
    $filter,
    $location,
    util
  ) {
    var vm = this;
    // $rootScope.pageInfo = $location.path();
    vm.locationCount = 1;

    vm.showAnnualSaleErrorMesg = 0;

    vm.viewMode = false;

    vm.showBuildingForm = false;

    vm.showLocationForm = true;

    vm.errormsg = false;

    // vm.sysRefid = 'Located_in_a_self_enclosed_mal';

    // two flags that select if we are editing existing building or adding new
	// one

    vm.isEditingBuilding = false;

    vm.isAddingBuilding = false;

    vm.defaultLocation = true;

    vm.defaultlocationPanel = {};

    vm.locData = [];

    vm.defaultLocPage = false;

    vm.locationAddress = null;

    vm.locDisable = false;

    vm.saveDisable = false;
    vm.locationSerChecking = false;
    vm.locationSerChecking1 = false;

    vm.editingExistingLocation = false;
    vm.Emptybuilding = false;

    vm.editEmptybuilding = false;
    // buildunderwriting
    vm.Emptysecbuilding = false;
    vm.buildUnderCancel = false;
    // end buildunderwriting

    vm.locID = null;
    vm.stateErrMsg = null;
    vm.stateSummErrMsg = null;
    vm.protErrMsg = null;
    vm.showState = false;
    vm.showOccupantForm = false;
    vm.locservicerror = false;
    vm.addAnotherOccupant = true;
    vm.editOccuDisable = false;
    // buildunderwriting
    vm.buildunderqa = false;
    vm.disableSubmit = false;
    vm.disableForm = false;
    vm.cancelHide = true;
    vm.disableunderSubmit = true;
    vm.addsavebuild = false;
    vm.addAditionalCall = false;
    vm.multipleOccupancyDC = null;
    vm.applicantInterestDC = null;
    vm.ocuDataLength;
    vm.OccAppianValidation;
    vm.OccCountAppianValidation;
    vm.savedBuildingOccupants = false;
    vm.showOccupantError = false;
    vm.businessClassCaptionValue = businessClassCaptionValue;
    vm.businessClassValues;
    vm.businessClassValues1 = [];
    vm.appianBefSave = false;
    vm.appianBefSaveUnder = false;
    vm.appianBefSaveLoc = false;
    vm.appianBefSaveLoc1 = false;

    vm.bClassValues;
    // vm.saveFinal = false;

    // end buildunderwriting
    vm.lengthOfZip;
    vm.Lobstate;
    vm.businessClass;
    vm.locediterror;

    vm.buildingOccupantId;
    vm.locationBuildingId;

    // vm.businessClassValidations = businessClassValidations;

    // the selected location to be edited. if FALSE add new location

    // vm.editID = false;

    // building form

    vm.buildingData = [];

    vm.locationDataUUID = {};

    vm.buildingDataUUID = {};

    vm.buildingUnderDataUUID = {};

    vm.occupantDataUUID = {};

    vm.locationData = [];

    vm.stateList = [];

    vm.appianObj = {};

    vm.locationCallObj = {};

    vm.yearBuiltAppianobj = {};
    vm.yearBuiltAppianobjUnder = {};

    vm.LocationSaveData = {
      LocationsData: [],
    };

    vm.addOcuData = [];

    vm.occupantDetailsForm = {};
    vm.noResults = [];
    vm.locationMasterData = [];
    vm.addinglocData = [];

    vm.eventMode = null;
    vm.zipCodeValidate = null;

    vm.buildingEventMode = null;
    vm.buildingUnderEventMode = null;

    var googleJSON;
    vm.googleData = false;
    vm.businessClassError = 0;
    vm.formSendData = {
      elementType: null,
      operationType: null,
      data: null,
    };
    vm.locCheckIndex;
    vm.locCheckSumIndex;
    vm.annualSaleCountErr = null;
    vm.fullLocationData = null;

    // methods

    vm.ziplengthmax = ziplengthmax;

    vm.ziplengthmin = ziplengthmin;

    vm.getLocationData = getLocationData;

    vm.getExpandedLocData = getExpandedLocData;

    vm.ZIPCodeValidation = ZIPCodeValidation;

    vm.saveBuilding = saveBuilding;

    vm.addNewLocation = addNewLocation;

    vm.saveLoc = saveLoc;

    vm.saveLocation = saveLocation;

    vm.saveExpandedLocation = saveExpandedLocation;

    vm.getIndexBySearch = getIndexBySearch;

    vm.viewModeBtn = viewModeBtn;

    vm.addNewBuilding = addNewBuilding;

    vm.buildEditMode = buildEditMode;

    vm.editLocation = editLocation;

    vm.editBuilding = editBuilding;

    vm.duplicateBuilding = duplicateBuilding;

    vm.removeBuilding = removeBuilding;

    vm.locModalPopup = locModalPopup;

    vm.buildModalPopup = buildModalPopup;

    vm.locEditMode = locEditMode;

    vm.addAditional = addAditional;

    vm.addAditionaldcCall = addAditionaldcCall;

    vm.validationTextfields = validationTextfields;

    vm.locationDisable = locationDisable;

    vm.validInputfeilds = validInputfeilds;

    vm.appianSystemRefId = appianSystemRefId;

    vm.getLocationCheckData = getLocationCheckData;

    vm.yearBuiltAddField = yearBuiltAddField;
    vm.yearBuiltAddFieldUnder = yearBuiltAddFieldUnder;

    vm.removeLocation = removeLocation;

    vm.locationCancel = locationCancel;

    vm.addNewOccupant = addNewOccupant;
    vm.editOccupant = editOccupant;
    vm.removeOccupant = removeOccupant;
    vm.updateLoc = updateLoc;
    vm.startsWith = startsWith;
    vm.businessClassValidate = businessClassValidate;
    vm.locationCallSystemRefId = locationCallSystemRefId;
    vm.stateValidation = stateValidation;
    vm.stateSummaryValidation = stateSummaryValidation;

    vm.setModal = setModal;
    vm.googleAPIAddress = googleAPIAddress;
    vm.locationSeviceCall = locationSeviceCall;
    vm.locationSeviceCallBlur = locationSeviceCallBlur;
    vm.zipCodeLookUP = zipCodeLookUP;
    vm.buildingCancel = buildingCancel;
    vm.protectionClassLookup = protectionClassLookup;
    vm.locationMagicFill = locationMagicFill;
    vm.saveBuildingUnderwriting = saveBuildingUnderwriting;
    vm.addBuildingUnderwriting = addBuildingUnderwriting;
    vm.getChildUnderwriting = getChildUnderwriting;
    vm.buildingUnderCancel = buildingUnderCancel;
    vm.addAditionalUnder = addAditionalUnder;
    vm.appianSystemRefIdUnder = appianSystemRefIdUnder;
    vm.yearUnder = yearUnder;
    vm.saveOccupant = saveOccupant;

    vm.item = {};

    vm.streetAutocompleteOptions = {
      componentRestrictions: {
        country: 'us',
      },
      // types: ['Addresses']
    };

    function locationMagicFill (buildingJSON) {
      _.find (buildingJSON.BuildingQuestions, function (loc) {
        if (
          loc.systemRefId == 'BuildingInput.YearBuilt' &&
          vm.yearBuilt != 'No data available'
        ) {
          vm.buildingDataUUID[loc.uuid] = vm.yearBuilt;
          if (vm.buildingDataUUID[loc.uuid] != null) {
            yearBuiltAddField (
              vm.buildingDataUUID[loc.uuid],
              buildingJSON.BuildingQuestions,
              loc.systemRefId,
              loc.eventName,
              vm.buildingFormElement,
              vm.buildingData,
              vm.buildingEventMode
            );
          }
        }
        if (
          loc.systemRefId == 'BuildingInput.UNIG_TotalSquareFeet' &&
          vm.squareFootage != 'No data available'
        ) {
          vm.buildingDataUUID[loc.uuid] = vm.squareFootage.replace (/\D/g, '');
          if (vm.buildingDataUUID[loc.uuid] != null) {
            validInputfeilds (
              vm.buildingDataUUID[loc.uuid],
              buildingJSON.BuildingQuestions,
              loc.eventName,
              loc.systemRefId,
              vm.buildingFormElement,
              vm.buildingData,
              vm.buildingEventMode
            );
          }
        }
        if (
          loc.systemRefId == 'BuildingInput.ConstructionCode' &&
          vm.constructionCode != 'No data available'
        ) {
          switch (vm.constructionCode) {
            case '1':
              vm.buildingDataUUID[loc.uuid] = 'Frame-1';
              vm.constructionVeriskBox = 'Frame-1';
              break;
            case '2':
              vm.buildingDataUUID[loc.uuid] = 'Joisted Masonry-2';
              vm.constructionVeriskBox = 'Joisted Masonry-2';
              break;
            case '3':
              vm.buildingDataUUID[loc.uuid] = 'Non-Combustible-3';
              vm.constructionVeriskBox = 'Non-Combustible-3';
              break;
            case '4':
              vm.buildingDataUUID[loc.uuid] = 'Masonry Non-Comb-4';
              vm.constructionVeriskBox = 'Masonry Non-Comb-4';
              break;
            case '5':
              vm.buildingDataUUID[loc.uuid] = 'Modified Fire Resistive-5 or 6';
              vm.constructionVeriskBox = 'Modified Fire Resistive-5 or 6';
              break;
            case '6':
              vm.buildingDataUUID[loc.uuid] = 'Fire Resistive-6';
              vm.constructionVeriskBox = 'Fire Resistive-6';
              break;
            default:
              vm.constructionVeriskBox = 'No data available';
              break;
          } // End of Switch
        }
        // else {
        // vm.constructionVeriskBox = 'No data available';
        // }
      }); // End of Find Loop
    } // End of locationMagicFill()

    function googleAPIAddress () {
      var googleAdd = [], temp;
      for (var i = 0; i < googleJSON.address_components.length; i++) {
        if (
          googleJSON.address_components[i].types.indexOf (
            'administrative_area_level_1'
          ) != -1
        ) {
          // state

          temp = googleJSON.address_components[i].types.indexOf (
            'administrative_area_level_1'
          );
          googleAdd[googleJSON.address_components[i].types[temp]] =
            googleJSON.address_components[i].short_name;
        } else if (
          googleJSON.address_components[i].types.indexOf (
            'administrative_area_level_3'
          ) != -1 || // LocationInput.City
          googleJSON.address_components[i].types.indexOf ('locality') != -1
        ) {
          if (
            googleJSON.address_components[i].types.indexOf (
              'administrative_area_level_3'
            ) != -1
          ) {
            temp = googleJSON.address_components[i].types.indexOf (
              'administrative_area_level_3'
            );
            googleAdd[googleJSON.address_components[i].types[temp]] =
              googleJSON.address_components[i].short_name;
          }
          if (
            googleJSON.address_components[i].types.indexOf ('locality') != -1
          ) {
            temp = googleJSON.address_components[i].types.indexOf ('locality');
            googleAdd[googleJSON.address_components[i].types[temp]] =
              googleJSON.address_components[i].short_name;
          }
        } else if (
          googleJSON.address_components[i].types.indexOf ('postal_code') != -1
        ) {
          // ZipCode
          temp = googleJSON.address_components[i].types.indexOf ('postal_code');
          googleAdd[googleJSON.address_components[i].types[temp]] =
            googleJSON.address_components[i].short_name;
        } else if (
          googleJSON.address_components[i].types.indexOf ('route') != -1
        ) {
          // street address - street name
          temp = googleJSON.address_components[i].types.indexOf ('route');
          googleAdd[googleJSON.address_components[i].types[temp]] =
            googleJSON.address_components[i].short_name;
        } else if (
          googleJSON.address_components[i].types.indexOf ('street_number') != -1
        ) {
          // street address - street number
          temp = googleJSON.address_components[i].types.indexOf (
            'street_number'
          );
          googleAdd[googleJSON.address_components[i].types[temp]] =
            googleJSON.address_components[i].short_name;
        } else if (
          googleJSON.address_components[i].types.indexOf ('premise') != -1
        ) {
          // APT/Suite - building name
          temp = googleJSON.address_components[i].types.indexOf ('premise');
          googleAdd[googleJSON.address_components[i].types[temp]] =
            googleJSON.address_components[i].short_name;
        } else if (
          googleJSON.address_components[i].types.indexOf ('subpremise') != -1
        ) {
          // APT/Suite - building number
          temp = googleJSON.address_components[i].types.indexOf ('subpremise');
          googleAdd[googleJSON.address_components[i].types[temp]] =
            googleJSON.address_components[i].short_name;
        }
      }
      // console.log(googleAdd);

      // console.log(vm.explocData[0].LocationAddress);
      for (var j = 0; j < vm.explocData[0].LocationAddress.length; j++) {
        if (
          vm.explocData[0].LocationAddress[j].systemRefId ==
          'LocationInput.ZipCode'
        ) {
          // zipcode

          if (_.has (googleAdd, 'postal_code')) {
            vm.explocData[0].LocationAddress[j].userValue = _.property (
              'postal_code'
            ) (googleAdd);
          } else {
            vm.explocData[0].LocationAddress[j].userValue = '';
          }
        } else if (
          vm.explocData[0].LocationAddress[j].systemRefId ==
          'LocationInput.Address2'
        ) {
          // APT/Suite

          var premise, subpremise;
          vm.explocData[0].LocationAddress[j].userValue = null;

          if (_.has (googleAdd, 'premise') || _.has (googleAdd, 'subpremise')) {
            if (_.has (googleAdd, 'premise')) {
              premise = _.property ('premise') (googleAdd);
              vm.explocData[0].LocationAddress[j].userValue = premise;
            }
            if (_.has (googleAdd, 'subpremise')) {
              subpremise = _.property ('subpremise') (googleAdd);
              vm.explocData[0].LocationAddress[j].userValue = subpremise;
            }

            if (subpremise && premise) {
              vm.explocData[0].LocationAddress[j].userValue =
                subpremise + ' ' + premise;
            }
          }
        } else if (
          vm.explocData[0].LocationAddress[j].systemRefId ==
          'LocationInput.Address1'
        ) {
          // Street address

          vm.explocData[0].LocationAddress[j].userValue = '';
          if (
            _.has (googleAdd, 'route') ||
            _.has (googleAdd, 'street_number')
          ) {
            var route, streetNumber;

            if (_.has (googleAdd, 'route')) {
              route = _.property ('route') (googleAdd);
              vm.explocData[0].LocationAddress[j].userValue = route;
            }
            if (_.has (googleAdd, 'street_number')) {
              streetNumber = _.property ('street_number') (googleAdd);
              vm.explocData[0].LocationAddress[j].userValue = streetNumber;
            }

            if (route && streetNumber) {
              vm.explocData[0].LocationAddress[j].userValue =
                streetNumber + ' ' + route;
            }
          }
        }
      }
    } // end of googleAPIAdress Function

    if (vm.editingExistingLocation) {
      vm.locData[0].location[vm.editID] = angular.copy (vm.locationPanel);
    } else {
      // vm.locData[0].location.push(vm.locDataAdd);
    }

    vm.editingExistingLocation = false;

    // we set current location to false

    // vm.editID = false;

    function updateLoc (item, addr, uuid, locObj, locationIndex) {
      googleJSON = angular.copy (addr);

      if (addr == undefined) {
        vm.gAddr = '';
        vm.googleData = false;
      }
      if (addr && addr.address_components != undefined) {
        vm.googleData = true;
        vm.gAddr = addr.adr_address;
        vm.gPhone = addr.formatted_phone_number;
        vm.lat = addr.geometry.location.lat ();
        vm.lng = addr.geometry.location.lng ();
        vm.data = addr.geometry.location
          .toString ()
          .replace (/(\s\s)*(\s(?=\[.*?\]\s))*\[.*?\](\s\s)*/g, '');
        // vm.lat.replace(/[()]/g, "");
        // console.log(vm.data);
      }

      var googleAdd = [], temp;
      var premise, subpremise;
      console.log ('googleJSON', googleJSON);
      if (!googleJSON.hasOwnProperty ('address_components')) {
        return;
      }
      // console.log('*/-*/-*/-*/-*/-*/',googleJSON);
      for (var i = 0; i < googleJSON.address_components.length; i++) {
        if (
          googleJSON.address_components[i].types.indexOf (
            'administrative_area_level_1'
          ) != -1
        ) {
          // state

          temp = googleJSON.address_components[i].types.indexOf (
            'administrative_area_level_1'
          );
          googleAdd[googleJSON.address_components[i].types[temp]] =
            googleJSON.address_components[i].short_name;
        } else if (
          googleJSON.address_components[i].types.indexOf (
            'administrative_area_level_3'
          ) != -1 || // city
          googleJSON.address_components[i].types.indexOf ('locality') != -1
        ) {
          if (
            googleJSON.address_components[i].types.indexOf (
              'administrative_area_level_3'
            ) != -1
          ) {
            temp = googleJSON.address_components[i].types.indexOf (
              'administrative_area_level_3'
            );
            googleAdd[googleJSON.address_components[i].types[temp]] =
              googleJSON.address_components[i].short_name;
          }
          if (
            googleJSON.address_components[i].types.indexOf ('locality') != -1
          ) {
            temp = googleJSON.address_components[i].types.indexOf ('locality');
            googleAdd[googleJSON.address_components[i].types[temp]] =
              googleJSON.address_components[i].short_name;
          }
        } else if (
          googleJSON.address_components[i].types.indexOf ('postal_code') != -1
        ) {
          // ZipCode
          temp = googleJSON.address_components[i].types.indexOf ('postal_code');
          googleAdd[googleJSON.address_components[i].types[temp]] =
            googleJSON.address_components[i].short_name;
        } else if (
          googleJSON.address_components[i].types.indexOf ('route') != -1
        ) {
          // street address - street name
          temp = googleJSON.address_components[i].types.indexOf ('route');
          googleAdd[googleJSON.address_components[i].types[temp]] =
            googleJSON.address_components[i].short_name;
        } else if (
          googleJSON.address_components[i].types.indexOf ('street_number') != -1
        ) {
          // street address - street number
          temp = googleJSON.address_components[i].types.indexOf (
            'street_number'
          );
          googleAdd[googleJSON.address_components[i].types[temp]] =
            googleJSON.address_components[i].short_name;
        } else if (
          googleJSON.address_components[i].types.indexOf ('premise') != -1
        ) {
          // APT/Suite - building name

          temp = googleJSON.address_components[i].types.indexOf ('premise');
          googleAdd[googleJSON.address_components[i].types[temp]] =
            googleJSON.address_components[i].short_name;
        } else if (
          googleJSON.address_components[i].types.indexOf ('subpremise') != -1
        ) {
          // APT/Suite - building number
          temp = googleJSON.address_components[i].types.indexOf ('subpremise');
          googleAdd[googleJSON.address_components[i].types[temp]] =
            googleJSON.address_components[i].short_name;
        } else if (
          googleJSON.address_components[i].types.indexOf ('country') != -1
        ) {
          // APT/Suite - building number
          temp = googleJSON.address_components[i].types.indexOf ('country');
          googleAdd[googleJSON.address_components[i].types[temp]] =
            googleJSON.address_components[i].short_name;
        }
      } // End of loop
      vm.street = '';
      vm.streetAPTSuite = '';
      vm.cityStateZipcode = '';
      vm.country = '';
      // console.log('*/-*/-*/-*/-*/-*/',googleAdd);
      if (
        _.has (googleAdd, 'administrative_area_level_3') || // city
        _.has (googleAdd, 'locality')
      ) {
        if (_.has (googleAdd, 'administrative_area_level_3')) {
          vm.cityStateZipcode = _.property ('administrative_area_level_3') (
            googleAdd
          );
        } else if (_.has (googleAdd, 'locality')) {
          vm.cityStateZipcode = _.property ('locality') (googleAdd);
        }
      }

      if (_.has (googleAdd, 'administrative_area_level_1')) {
        // state
        if (vm.cityStateZipcode) {
          vm.cityStateZipcode =
            vm.cityStateZipcode +
            ', ' +
            _.property ('administrative_area_level_1') (googleAdd);
        } else {
          vm.cityStateZipcode = _.property ('administrative_area_level_1') (
            googleAdd
          );
        }
      }

      if (_.has (googleAdd, 'postal_code')) {
        // zipcode
        if (vm.cityStateZipcode) {
          vm.cityStateZipcode =
            vm.cityStateZipcode + ', ' + _.property ('postal_code') (googleAdd);
        } else {
          vm.cityStateZipcode = _.property ('postal_code') (googleAdd);
        }
      }

      if (_.has (googleAdd, 'country')) {
        // country
        vm.country = _.property ('country') (googleAdd);
      }

      if (_.has (googleAdd, 'route') || _.has (googleAdd, 'street_number')) {
        // Street address

        var route, streetNumber;
        if (_.has (googleAdd, 'route')) {
          route = _.property ('route') (googleAdd);
          vm.street = route;
        }
        if (_.has (googleAdd, 'street_number')) {
          streetNumber = _.property ('street_number') (googleAdd);
          vm.street = streetNumber;
        }
        if (route && streetNumber) {
          vm.street = streetNumber + ', ' + route;
        }
      }

      if (_.has (googleAdd, 'premise') || _.has (googleAdd, 'subpremise')) {
        // APT/Suite
        if (vm.streetAPTSuite) {
          if (_.has (googleAdd, 'premise')) {
            premise = _.property ('premise') (googleAdd);
            vm.streetAPTSuite = vm.streetAPTSuite + ', ' + premise;
          }
          if (_.has (googleAdd, 'subpremise')) {
            subpremise = _.property ('subpremise') (googleAdd);
            vm.streetAPTSuite = vm.streetAPTSuite + ', ' + subpremise;
          }
          if (subpremise && premise) {
            vm.streetAPTSuite =
              vm.streetAPTSuite + ', ' + subpremise + ', ' + premise;
          }
        } else {
          if (_.has (googleAdd, 'premise')) {
            premise = _.property ('premise') (googleAdd);
            vm.streetAPTSuite = premise;
          }
          if (_.has (googleAdd, 'subpremise')) {
            subpremise = _.property ('subpremise') (googleAdd);
            vm.streetAPTSuite = subpremise;
          }
          if (subpremise && premise) {
            vm.streetAPTSuite = subpremise + ', ' + premise;
          }
        }
      }

      locObj.LocationAddress.forEach (function (place) {
        if (place.systemRefId == 'LocationInput.Address1') {
          place.userValue = vm.street;
        }

        if (place.systemRefId == 'LocationInput.Address2') {
          place.userValue = vm.streetAPTSuite;
        }

        if (place.systemRefId == 'LocationInput.ZipCode') {
          place.userValue = googleAdd.postal_code;
          vm.lengthOfZip = false;

          zipCodeLookUP (
            place.systemRefId,
            place.eventName,
            vm.lengthOfZip,
            place.userValue,
            locationIndex
          );
        }
      });
    }

    function setModal () {
      // console.log(vm.item.userValue, 'retun val');
      return vm.item.userValue;
    }

    vm.viewModeBtn ();

    function locModalPopup (locationId, locIndex) {
      // console.log('locationId1', locationId);

      vm.locmodalInstance = $uibModal.open ({
        templateUrl: 'blocks/modal/locDialog.html',

        windowClass: 'gradientModal',

        controller: 'locationRemoveCtrl',

        controllerAs: '$locRemCtrl',
        backdrop: 'static',
        keyboard: false,

        resolve: {
          param: function () {
            return {
              locationId: locationId,
              locIndex: locIndex,
            };
          },
        },
      });
    }

    var temp1;

    function buildModalPopup (locationId, buildingId, buildIndex, locIndex) {
      vm.buildmodalInstance = $uibModal.open ({
        templateUrl: 'blocks/modal/buildDialog.html',

        windowClass: 'gradientModal',

        controller: 'buildingRemoveCtrl',

        controllerAs: '$buildRemCtrl',
        backdrop: 'static',
        keyboard: false,

        resolve: {
          param: function () {
            return {
              locationId: locationId,

              buildingId: buildingId,
              buildIndex: buildIndex,
              locIndex: locIndex,
            };
          },
        },
      });
    }

    function editModalPopup (locationDataRes, locId, formSendData) {
      var locationIndex = {};
      locationIndex.locIndex = locId;
      vm.buildmodalInstance = $uibModal.open ({
        templateUrl: 'blocks/modal/editDialog.html',

        windowClass: 'gradientModal',

        controller: 'editConfirmCtrl',

        controllerAs: '$editConfirmCtrl',
        backdrop: 'static',
        keyboard: false,
        resolve: {
          param: function () {
            return {
              locationDataRes: locationDataRes,
              locationIndex: locationIndex,
              formSendData: formSendData,
            };
          },
        },
      });
    }

    getLocationCheckData ();

    /**
	 * 
	 * appianSystemRefId
	 * 
	 * @param {*}
	 *            buildingQuestion
	 * 
	 */

    function appianSystemRefId (buildingQuestion) {
      // 6 appaian rule validation

      _.chain (buildingQuestion).map (function (bDataa) {
        if (
          bDataa.defaultValue != null &&
          bDataa.userValue == null &&
          vm.buildingDataUUID[bDataa.uuid] == null
        ) {
          bDataa.userValue = bDataa.defaultValue;
        }
        if (
          bDataa.systemRefId == 'BuildingInput.UNIG_ApplicantInterest' ||
          bDataa.systemRefId == 'BuildingInput.UNIG_TotalSquareFeet' ||
          bDataa.systemRefId == 'BuildingInput.UNIG_MultipleOccupancyBldg' ||
          bDataa.systemRefId == 'CovBuildingInput.Limit' ||
          bDataa.systemRefId == 'OccupancyOutput.BOP_SquareFootage' ||
          bDataa.systemRefId == 'CovPersonalPropertyInput.Limit'
        ) {
          var a = bDataa.systemRefId;

          var b = bDataa.userValue;

          vm.appianObj[a] = b;
        }
      });

      /* for yearbuilt appian onjects */

      for (var ai = 0; ai < buildingQuestion.length; ai++) {
        if (
          buildingQuestion[ai].systemRefId == 'BuildingInput.YearBuilt' ||
          buildingQuestion[ai].systemRefId ==
            'Have_all_of_the_following_been' ||
          buildingQuestion[ai].systemRefId == 'Electrical' ||
          buildingQuestion[ai].systemRefId == 'Plumbing' ||
          buildingQuestion[ai].systemRefId == 'Roof' ||
          buildingQuestion[ai].systemRefId == 'Furnace'
        ) {
          var c = buildingQuestion[ai].systemRefId;

          var d = null;

          vm.yearBuiltAppianobj[c] = d;
          // console.log('vm.yearBuiltAppianobj', vm.yearBuiltAppianobj);
        }
      }
    }

    /**
	 * 
	 * locationCallSystemRefId
	 * 
	 * @param {*}
	 *            locationQuestion
	 * 
	 */

    function locationCallSystemRefId (locationQuestion, locationId) {
      // locationservice call
      for (var ikk = 0; ikk < vm.explocData[0].LocationAddress.length; ikk++) {
        if (
          vm.explocData[0].LocationAddress[ikk].systemRefId ==
            'LocationInput.Address1' ||
          vm.explocData[0].LocationAddress[ikk].systemRefId ==
            'LocationInput.Address2' ||
          vm.explocData[0].LocationAddress[ikk].systemRefId ==
            'LocationInput.City' ||
          vm.explocData[0].LocationAddress[ikk].systemRefId ==
            'LocationInput.State' ||
          vm.explocData[0].LocationAddress[ikk].systemRefId ==
            'LocationInput.ZipCode'
        ) {
          var ab = vm.explocData[0].LocationAddress[ikk].systemRefId;

          var bc = vm.explocData[0].LocationAddress[ikk].userValue;

          vm.locationCallObj[ab] = bc;
        }
      }
      vm.locationCallObj.locationId = vm.explocData[0].locationId;
    }
    /**
	 * 
	 * stateValidation
	 * 
	 * @param {*}
	 *            userValue
	 * 
	 * @param {*}
	 *            systemRefId
	 * 
	 * @param {*}
	 *            eventName
	 * 
	 */
    function stateValidation (
      userValue,
      systemRefId,
      eventName,
      locationIndex
    ) {
      // console.log("state------------------------------------------------------",userValue,
		// systemRefId, eventName);
      var stateValidateobj = {};

      stateValidateobj.lobstate = vm.Lobstate;
      stateValidateobj.locstate = userValue;

      for (var j in eventName) {
        var appianRule = eventName[j].split ('RULE:')[1];
      }

      var data = {
        elementType: ['APPIANRULE'],

        operationType: [appianRule],

        data: [stateValidateobj],
      };
      // console.log("state------------------------------------------------------",data);
      locationService.validateStateAppianRule (data).then (
        function (res) {
          _.chain (res.data).map (function (response) {
            if (response.sysRefId == systemRefId) {
              if (response.error != null) {
                vm.stateErrMsg = response.error;
              } else {
                vm.locID = locationIndex;
                locationSeviceCall (vm.explocData[0].LocationAddress);
                vm.stateErrMsg = response.error;
              }
            }
          });
        },
        function () {}
      );
    }

    /**
	 * 
	 * stateSummaryValidation
	 * 
	 * @param {*}
	 *            userValue
	 * 
	 * @param {*}
	 *            systemRefId
	 * 
	 * @param {*}
	 *            eventName
	 * 
	 */
    function stateSummaryValidation (userValue, systemRefId, eventName) {
      // console.log("summ
		// state------------------------------------------------------",userValue,
		// systemRefId, eventName);
      var stateValidateobj = {};
      stateValidateobj.lobstate = vm.Lobstate;
      stateValidateobj.locstate = userValue;

      for (var j in eventName) {
        var appianRule = eventName[j].split ('RULE:')[1];
      }

      var data = {
        elementType: ['APPIANRULE'],

        operationType: [appianRule],

        data: [stateValidateobj],
      };
      // console.log("summ
		// state------------------------------------------------------",data);
      locationService.validateStateAppianRule (data).then (
        function (res) {
          _.chain (res.data).map (function (response) {
            if (response.sysRefId == systemRefId) {
              if (response.error != null) {
                vm.stateSummErrMsg = response.error;
                console.log ('vm.stateSummErrMsg ', vm.stateSummErrMsg);
              } else {
                vm.stateSummErrMsg = response.error;
              }
            }
          });
        },
        function () {}
      );
    }

    /**
	 * 
	 * validInputfeilds
	 * 
	 * @param {*}
	 *            buildinguuid
	 * 
	 * @param {*}
	 *            buildingQuestion
	 * 
	 * @param {*}
	 *            eventName
	 * 
	 * @param {*}
	 *            systemRefId
	 * 
	 */

    function validInputfeilds (
      buildinguuid,
      buildingQuestion,
      eventName,
      systemRefId,
      buildingFormElement,
      buildingDatas,
      buildingEventMode
    ) {
      Object.keys (vm.appianObj).forEach (function (key) {
        if (key == systemRefId) {
          var c = buildinguuid;

          vm.appianObj[key] = c;
        }
      });

      for (var j in eventName) {
        var appianRule = eventName[j].split ('RULE:')[1];
      }

      if (vm.addOcuData.length > 0) {
        vm.ocuDataLength = vm.addOcuData.length;
      } else {
        vm.ocuDataLength = 0;
      }

      vm.appianObj.no_of_occupant = vm.ocuDataLength;

      if (appianRule != null && appianRule != undefined) {
        if (appianRule == 'BRVALIDATEAPPLICANTINTANDDEPENDENTS') {
          var data = {
            elementType: ['APPIANRULE'],

            operationType: [appianRule],

            data: [vm.appianObj],
          };

          locationService.validateBuildingAppianRule (data).then (
            function (res) {
              for (var i = 0; i < res.data.length; i++) {
                if (res.data[i].sysRefId == 'Add_another_occupant') {
                  // if (res.data[i].error != null) {
                  vm.OccAppianValidation = res.data[i].visible;

                  if (vm.OccAppianValidation && vm.savedBuildingOccupants) {
                    vm.showOccupantForm = true;
                    vm.showBuildingForm = true;
                    vm.addAnotherOccupant = true;
                  } else {
                    vm.addAnotherOccupant = false;
                  }
                }

                // Error handling for Occupants

                if (
                  res.data[i].error == 'No of Occupants cannot be more than 1'
                ) {
                  // if (res.data[i].error != null) {
                  vm.OccCountAppianValidation = res.data[i].visible;

                  if (
                    vm.OccCountAppianValidation &&
                    vm.savedBuildingOccupants
                  ) {
                    vm.showOccupantError = true;
                    // vm.disableSubmit = true;
                  } else {
                    vm.showOccupantError = false;
                    // vm.disableSubmit = false;
                  }
                }

                if (
                  res.data[i].sysRefId == 'OccupancyOutput.BOP_SquareFootage'
                ) {
                  if (res.data[i].error != null) {
                    vm.OccSquareFootageMsg = res.data[i].error;
                  } else {
                    vm.OccSquareFootageMsg = res.data[i].error;
                  }
                }

                if (
                  res.data[i].sysRefId == 'BuildingInput.UNIG_TotalSquareFeet'
                ) {
                  if (res.data[i].error != null) {
                    vm.squareFootageMsg = res.data[i].error;
                  } else {
                    vm.squareFootageMsg = res.data[i].error;
                  }
                }

                if (res.data[i].sysRefId == 'CovBuildingInput.Limit') {
                  if (res.data[i].error != null) {
                    vm.buildingLimitMsg = res.data[i].error;
                  } else {
                    vm.buildingLimitMsg = res.data[i].error;
                  }
                }

                if (res.data[i].sysRefId == 'CovPersonalPropertyInput.Limit') {
                  if (res.data[i].error != null) {
                    vm.buildingPLimitMsg = res.data[i].error;
                  } else {
                    vm.buildingPLimitMsg = res.data[i].error;
                  }
                }

                if (
                  res.data[i].sysRefId == 'BuildingInput.UNIG_TotalSquareFeet'
                ) {
                  if (res.data[i].mandatory == false) {
                    for (var x = 0; x < buildingQuestion.length; x++) {
                      if (
                        buildingQuestion[x].systemRefId ==
                        'BuildingInput.UNIG_TotalSquareFeet'
                      ) {
                        buildingQuestion[x].isRequired = false;
                      }
                    }
                  }

                  if (res.data[i].mandatory == true) {
                    for (var a = 0; a < buildingQuestion.length; a++) {
                      if (
                        buildingQuestion[a].systemRefId ==
                        'BuildingInput.UNIG_TotalSquareFeet'
                      ) {
                        buildingQuestion[a].isRequired = true;
                      }
                    }
                  }
                }

                if (
                  res.data[i].sysRefId ==
                  'BuildingInput.UNIG_MallWatchmanQuestion'
                ) {
                  if (res.data[i].visible == true) {
                    for (var b = 0; b < buildingQuestion.length; b++) {
                      if (
                        buildingQuestion[b].systemRefId ==
                        'BuildingInput.UNIG_MallWatchmanQuestion'
                      ) {
                        buildingQuestion[b].appearUI = 'Y';
                      }
                    }
                  }

                  if (
                    res.data[i].visible == null ||
                    res.data[i].visible == false
                  ) {
                    for (var c = 0; c < buildingQuestion.length; c++) {
                      if (
                        buildingQuestion[c].systemRefId ==
                        'BuildingInput.UNIG_MallWatchmanQuestion'
                      ) {
                        buildingQuestion[c].appearUI = 'N';
                      }
                    }
                  }
                }

                if (res.data[i].sysRefId == 'RiskInput.OccupancyPercentage') {
                  if (res.data[i].value != null) {
                    for (var j = 0; j < buildingQuestion.length; j++) {
                      if (
                        buildingQuestion[j].systemRefId ==
                        'RiskInput.OccupancyPercentage'
                      ) {
                        Object.keys (vm.buildingDataUUID).forEach (function (
                          key
                        ) {
                          if (key == buildingQuestion[j].uuid) {
                            vm.buildingDataUUID[key] = res.data[i].value[0];
                          }
                        });
                      }
                    }
                  }
                }
              }

              if (vm.appianBefSave == true) {
                vm.appianBefSave = false;
                vm.saveBuilding (
                  buildingFormElement,
                  buildingDatas,
                  buildingEventMode
                );
              }
            },
            function () {
              // ErrorToast.showError(err.detail);
              // console.log('checkTheAppianRule - error', err);
            }
          );
        }
      }
    }

    /**
	 * 
	 * for yearBuiltAddField
	 * 
	 * @param {*}
	 *            input
	 * 
	 * @param {*}
	 *            bData
	 * 
	 * @param {*}
	 *            sysID
	 * 
	 * @param {*}
	 *            eventName
	 * 
	 */

    function yearBuiltAddField (
      input,
      bData,
      sysID,
      eventName,
      buildingFormElement,
      buildingDatas,
      buildingEventMode
    ) {
      Object.keys (vm.yearBuiltAppianobj).forEach (function (key) {
        if (key == sysID) {
          var userinput = input;

          vm.yearBuiltAppianobj[key] = userinput;
        }
      });

      // console.log('vm.yearBuiltAppianobj', vm.yearBuiltAppianobj);
      for (var j in eventName) {
        var appianRule = eventName[j].split ('RULE:')[1];
      }

      if (sysID == 'BuildingInput.YearBuilt') {
        var data = {
          elementType: ['APPIANRULE'],

          operationType: [appianRule],

          data: [vm.yearBuiltAppianobj],
        };

        locationService.validateBuildingAppianRule (data).then (
          function (res) {
            for (var k = 0; k < res.data.length; k++) {
              /* for YEar built error message */
              if (res.data[k].sysRefId == 'BuildingInput.YearBuilt') {
                if (res.data[k].error != null) {
                  vm.yearerrmsg = res.data[k].error;
                } else {
                  vm.yearerrmsg = null;
                }
              }
            }

            if (vm.appianBefSave == true) {
              vm.appianBefSave = false;
              vm.saveBuilding (
                buildingFormElement,
                buildingDatas,
                buildingEventMode
              );
            }
          },
          function () {
            // ErrorToast.showError(err.detail);
            // console.log('checkTheAppianRule - error', err);
          }
        );
      }
    }

    /**
	 * for validationTextfields
	 * 
	 * @param {*}
	 *            sysId
	 * @param {*}
	 *            input
	 */
    function validationTextfields (
      sysId,
      input,
      buildingFormElement,
      buildingDatas,
      buildingEventMode
    ) {
      var data;

      /* start for Building Limit Field */
      // console.log(input);
      if (
        (sysId == 'CovBuildingInput.Limit' && input != undefined) ||
        (sysId == 'CovPersonalPropertyInput.Limit' && input)
      ) {
        if (isNaN (input)) {
          input = input.toString ().replace (/,/g, '');
          input = input.toString ().replace (/\$/g, '');
          input = parseFloat (input);
          input = input.toFixed (input % 1 === 0 ? 0 : 2);

          if (input.includes ('-')) {
            input = input.toString ().replace (/-/g, '');
            return input.toString ().replace (/\B(?=(\d{3})+(?!\d))/g, ',');
          }
          // console.log(input.toString().replace(/\B(?=(\d{3})+(?!\d))/g,
			// ','));
          return input.toString ().replace (/\B(?=(\d{3})+(?!\d))/g, ',');
        }

        input = parseFloat (input);
        input = input.toFixed (input % 1 === 0 ? 0 : 2);
        return input.toString ().replace (/\B(?=(\d{3})+(?!\d))/g, ',');
      }

      /* start for Number of Stories field */

      if (sysId == 'BuildingInput.NumberOfStories') {
        for (var k = 0; k < vm.buildingData[0].BuildingQuestions.length; k++) {
          if (
            vm.buildingData[0].BuildingQuestions[k].systemRefId ==
            'BuildingInput.NumberOfStories'
          ) {
            vm.RuleName = vm.buildingData[0].BuildingQuestions[
              k
            ].eventName[0].split ('RULE:')[1];
          }
        }

        data = {
          elementType: ['APPIANRULE'],
          operationType: [vm.RuleName],
          data: [input],
        };

        locationService.validateBuildingAppianRule (data).then (
          function (res) {
            // console.log('checkTheAppianRule - success', res);
            vm.numberofstorieserrmsg = res.data[0].error;

            if (vm.appianBefSave == true) {
              vm.appianBefSave = false;
              vm.saveBuilding (
                buildingFormElement,
                buildingDatas,
                buildingEventMode
              );
            }
          },
          function () {
            // ErrorToast.showError(err.detail);
            // console.log('checkTheAppianRule - error', err);
          }
        );
      }

      /* end for Number of Stories field */

      /* start for permanently installed equipment Field */

      if (sysId == 'RiskInput.PermanentlyInstalledEquipmentLimit') {
        for (
          var kl = 0;
          kl < vm.buildingData[0].BuildingQuestions.length;
          kl++
        ) {
          if (
            vm.buildingData[0].BuildingQuestions[kl].systemRefId ==
            'RiskInput.PermanentlyInstalledEquipmentLimit'
          ) {
            vm.RuleName = vm.buildingData[0].BuildingQuestions[
              kl
            ].eventName[0].split ('RULE:')[1];
          }
        }

        data = {
          elementType: ['APPIANRULE'],
          operationType: [vm.RuleName],
          data: [input],
        };

        locationService.validateBuildingAppianRule (data).then (
          function (res) {
            // console.log('checkTheAppianRule - success', res);
            vm.perielerrmsg = res.data[0].error;
            if (vm.appianBefSave == true) {
              vm.appianBefSave = false;
              vm.saveBuilding (
                buildingFormElement,
                buildingDatas,
                buildingEventMode
              );
            }
          },
          function () {
            // ErrorToast.showError(err.detail);
            // console.log('checkTheAppianRule - error', err);
          }
        );
      }
      /* end for permanently installed equipment Field */
      return input;
    }

    function addAditionaldcCall (sysID, bData, input) {
      var data;

      if (sysID == 'BuildingInput.UNIG_ApplicantInterest') {
        vm.applicantInterestDC = input;
      }

      if (sysID == 'BuildingInput.UNIG_MultipleOccupancyBldg') {
        vm.multipleOccupancyDC = input;
      }

      // var inputData = {
      // 'applicantInterest': vm.applicantInterestDC,
      // 'multipleOccupancy': vm.multipleOccupancyDC
      // }

      var inputData = {
        BuildingQuestions: [
          {
            systemRefId: 'BuildingInput.UNIG_ApplicantInterest',
            typeName: 'Building',
            userValue: vm.applicantInterestDC,
          },
          {
            systemRefId: 'BuildingInput.UNIG_MultipleOccupancyBldg',
            typeName: 'Building',
            userValue: vm.multipleOccupancyDC,
          },
        ],
        buildingId: [vm.locationBuildingId],
      };

      if (
        sysID == 'BuildingInput.UNIG_ApplicantInterest' ||
        sysID == 'BuildingInput.UNIG_MultipleOccupancyBldg'
      ) {
        data = {
          elementType: ['Risk'],
          operationType: ['UPDATEBUILDING'],
          data: [inputData],
        };

        locationService.saveBuildingApplicantDC (data).then (
          function (res) {},
          function () {
            // ErrorToast.showError(err.detail);
            // console.log('checkTheAppianRule - error', err);
          }
        );
      }

      /* end for Mixed Construction Subset additional field */
    }

    /**
	 * desc for addAditional
	 * 
	 * @param {*}
	 *            sysId
	 * @param {*}
	 *            buildingdata
	 * @param {*}
	 *            modeldata
	 */
    function addAditional (
      sysID,
      bData,
      input,
      buildingFormElement,
      buildingDatas,
      buildingEventMode
    ) {
      var data;
      /* start for Does the building contain any appartments */

      if (sysID == 'Does_any_building_contain_any_') {
        for (var i = 0; i < vm.buildingData[0].BuildingQuestions.length; i++) {
          if (
            vm.buildingData[0].BuildingQuestions[i].systemRefId ==
            'Does_any_building_contain_any_'
          ) {
            vm.RuleName = vm.buildingData[0].BuildingQuestions[
              i
            ].eventName[0].split ('RULE:')[1];
          }
        }

        data = {
          elementType: ['APPIANRULE'],
          operationType: [vm.RuleName],
          data: [input],
        };

        locationService.validateBuildingAppianRule (data).then (
          function (res) {
            if (res.data[0].value == true) {
              for (var i = 0; i < bData.length; i++) {
                if (bData[i].systemRefId == 'Provide_the_number_of_habitati') {
                  bData[i].appearUI = 'Y';
                }
                if (bData[i].systemRefId == 'Provide_the_total_square_foota') {
                  bData[i].appearUI = 'Y';
                }
              }
            }

            if (res.data[0].value == false) {
              for (var m = 0; m < bData.length; m++) {
                if (bData[m].systemRefId == 'Provide_the_number_of_habitati') {
                  bData[m].appearUI = 'N';
                }
                if (bData[m].systemRefId == 'Provide_the_total_square_foota') {
                  bData[m].appearUI = 'N';
                }
              }
            }

            if (vm.appianBefSave == true) {
              vm.appianBefSave = false;
              vm.saveBuilding (
                buildingFormElement,
                buildingDatas,
                buildingEventMode
              );
            }
          },
          function () {
            // ErrorToast.showError(err.detail);
            // console.log('checkTheAppianRule - error', err);
          }
        );
      }

      /* start for Mixed_Construction additional field */

      if (sysID == 'BuildingInput.ConstructionCode') {
        for (
          var ln = 0;
          ln < vm.buildingData[0].BuildingQuestions.length;
          ln++
        ) {
          if (
            vm.buildingData[0].BuildingQuestions[ln].systemRefId ==
            'BuildingInput.ConstructionCode'
          ) {
            vm.RuleName = vm.buildingData[0].BuildingQuestions[
              ln
            ].eventName[0].split ('RULE:')[1];
          }
        }

        data = {
          elementType: ['APPIANRULE'],
          operationType: [vm.RuleName],
          data: [input],
        };

        locationService.validateBuildingAppianRule (data).then (
          function (res) {
            if (res.data[0].visible == true) {
              for (var i = 0; i < bData.length; i++) {
                if (bData[i].systemRefId == 'BuildingInput.MixedConstruction') {
                  bData[i].appearUI = 'Y';
                }
              }
            } else {
              for (var xi = 0; xi < bData.length; xi++) {
                if (
                  bData[xi].systemRefId == 'BuildingInput.MixedConstruction' ||
                  bData[xi].systemRefId == 'BuildingInput.MixedConstructionSub'
                ) {
                  bData[xi].appearUI = 'N';
                  bData[xi].displayValue = null;

                  Object.keys (vm.buildingDataUUID).forEach (function (key) {
                    if (key == bData[xi].uuid) {
                      vm.buildingDataUUID[key] = null;
                    }
                  });
                }
              }
            }
            if (vm.appianBefSave == true) {
              vm.appianBefSave = false;
              vm.saveBuilding (
                buildingFormElement,
                buildingDatas,
                buildingEventMode
              );
            }
          },
          function () {
            // ErrorToast.showError(err.detail);
          }
        );
      }

      /* end for Mixed_Construction additional field */
      /* start for Mixed Construction Subset additional field */

      if (sysID == 'BuildingInput.MixedConstruction') {
        for (
          var hn = 0;
          hn < vm.buildingData[0].BuildingQuestions.length;
          hn++
        ) {
          if (
            vm.buildingData[0].BuildingQuestions[hn].systemRefId ==
            'BuildingInput.MixedConstruction'
          ) {
            vm.RuleName = vm.buildingData[0].BuildingQuestions[
              hn
            ].eventName[0].split ('RULE:')[1];
          }
        }

        data = {
          elementType: ['APPIANRULE'],
          operationType: [vm.RuleName],
          data: [input],
        };

        locationService.validateBuildingAppianRule (data).then (
          function (res) {
            if (res.data[0].visible == true) {
              for (var i = 0; i < bData.length; i++) {
                if (
                  bData[i].systemRefId == 'BuildingInput.MixedConstructionSub'
                ) {
                  bData[i].appearUI = 'Y';
                }
              }
            } else {
              for (var si = 0; si < bData.length; si++) {
                if (
                  bData[si].systemRefId == 'BuildingInput.MixedConstructionSub'
                ) {
                  bData[si].appearUI = 'N';
                }
              }
            }

            if (vm.appianBefSave == true) {
              vm.appianBefSave = false;
              vm.saveBuilding (
                buildingFormElement,
                buildingDatas,
                buildingEventMode
              );
            }
          },
          function () {
            // ErrorToast.showError(err.detail);
          }
        );
      }
      /* end for Mixed Construction Subset additional field */
    }

    /**
	 * Get getLocationCheckData
	 */
    function getLocationCheckData () {
      console.log ('aaa...');
      var data = {
        elementType: ['RISK'],
        operationType: ['VIEWLOCATION'],
      };

      // call service
      locationService.fetchLocationData (data).then (
        function (res) {
          if (res.data.Locationpage) {
            if (res.data.Locationpage[0] == 'true') {
              vm.locationMasterData = res.data.LocationsData;
              vm.Lobstate = res.data.SYQ[0];
              vm.businessAddressLocationPage = res.data.businessAddress;
              vm.businessClass = res.data.businessClass[0];

              getExpandedLocData ();
            }
          } else {
            vm.Lobstate = res.data.SYQ[0];
            vm.businessAddressLocationPage = res.data.businessAddress;
            vm.businessClass = res.data.businessClass[0];

            _.chain (res.data.LocationsData).map (function (
              locationMasterData
            ) {
              _.chain (locationMasterData.BuildingData).map (function (
                BuildingDataSE
              ) {
                console.log ('locationMasterData', locationMasterData);
                if (locationMasterData.editableLeft == true) {
                  locationMasterData.editableLeft = false;
                  addNewLocation (locationMasterData);
                } else if (
                  locationMasterData.editableLeft == true &&
                  locationMasterData.subjectType[0] == 'EDIT'
                ) {
                  locationMasterData.editableLeft = false;

                  vm.editLocation (
                    locationMasterData.locationId[0],
                    res.data.LocationsData.indexOf (locationMasterData),
                    locationMasterData
                  );
                } else if (BuildingDataSE.editableBuildingLeft == true) {
                  BuildingDataSE.editableBuildingLeft = false;

                  vm.editBuilding (
                    locationMasterData.locationId[0],
                    BuildingDataSE.buildingId,
                    locationMasterData.locationName,
                    res.data.LocationsData.indexOf (locationMasterData),
                    locationMasterData.BuildingData.indexOf (BuildingDataSE),
                    BuildingDataSE
                  );
                } else if (BuildingDataSE.buildingLeft == true) {
                  BuildingDataSE.buildingLeft = false;

                  vm.addNewBuilding (
                    res.data.LocationsData.indexOf (locationMasterData),
                    locationMasterData.BuildingData.length,
                    locationMasterData.locationId[0],
                    locationMasterData.locationName,
                    locationMasterData.BuildingData,
                    false,
                    BuildingDataSE
                  );
                } else {
                  console.log ('locationMasterData', locationMasterData);
                  getLocationData ();
                }
              });
            });
          }
        },
        function () {
          // ErrorToast.showError(err.detail);
        }
      );
    }

    /**
	 * protectionClassLookup
	 */

    function protectionClassLookup (
      sysRefID,
      eventName,
      terrdata,
      LocationAddress,
      locationId,
      locationIndex
    ) {
      // console.log("prot
		// class------------------------------------------------------",
		// sysRefID, eventName, terrdata, LocationAddress, locationId);
      if (sysRefID == 'LocationBusinessOwnersInput.UNIG_Territory') {
        if (
          eventName &&
          Array.isArray (eventName) &&
          eventName[0] &&
          eventName[0].indexOf (':') !== -1
        ) {
          // AJAX call for ZIPCODE
          var protectEventName = eventName[0].split (':')[1];
          var protectAJAXData = {
            elementType: ['RISK'],
            operationType: [protectEventName],
            data: [terrdata],
            locationId: locationId,
          };
          // console.log("prot------------------------------------------------------",
			// protectAJAXData);
          // $log.info('Zip Code Look UP Sending = ',zipCodeAJAXData);
          locationService.protectionClassLookup (protectAJAXData).then (
            function (res) {
              for (
                var proctectionLoop = 0;
                proctectionLoop < LocationAddress.length;
                proctectionLoop++
              ) {
                for (
                  var protectlookUPLoop = 0;
                  protectlookUPLoop < res.data.length;
                  protectlookUPLoop++
                ) {
                  if (
                    LocationAddress[proctectionLoop].systemRefId ==
                    res.data[protectlookUPLoop].systemRefId
                  ) {
                    if (vm.veriskSuggestionBox && locationIndex == 1) {
                      vm.protectionClassCode = vm.protectionClassCode.trim ();
                      console.log (
                        'vm.protectionClassCode',
                        vm.protectionClassCode
                      );
                      _.find (
                        LocationAddress[proctectionLoop].valueName,
                        function (loc) {
                          console.log (
                            'isNaN (loc.caption)',
                            isNaN (loc.caption),
                            loc.caption,
                            'loc.caption',
                            vm.protectionClassCode
                          );
                          if (
                            isNaN (loc.caption) &&
                            loc.caption == vm.protectionClassCode
                          ) {
                            LocationAddress[proctectionLoop].userValue =
                              loc.caption;
                            console.log ('  aa loc', loc);
                            return true;
                          }
                          if (
                            parseInt (loc.caption) ==
                              parseInt (vm.protectionClassCode) &&
                            isNaN (vm.protectionClassCode) != true
                          ) {
                            console.log (
                              'parseInt (loc.caption)',
                              parseInt (loc.caption)
                            );
                            console.log (
                              'parseInt (vm.protectionClassCode)',
                              parseInt (vm.protectionClassCode)
                            );
                            LocationAddress[proctectionLoop].userValue =
                              loc.caption;
                            console.log ('  bb loc', loc);
                            return true;
                          } else {
                            console.log ('  cc loc', loc);
                            LocationAddress[proctectionLoop].userValue = null;
                          }
                        }
                      ); // End of .find
                      console.log (
                        '  LocationAddress[proctectionLoop]',
                        LocationAddress[proctectionLoop]
                      );
                    } // End of If

                    // LocationAddress[proctectionLoop].userValue =
					// res.data[protectlookUPLoop].userValue;
                    LocationAddress[proctectionLoop].valueName =
                      res.data[protectlookUPLoop].valueName;

                    if (res.data[protectlookUPLoop].valueName.length == 1) {
                      _.find (
                        LocationAddress[proctectionLoop].valueName,
                        function (loc) {
                          LocationAddress[proctectionLoop].userValue =
                            loc.caption;
                        }
                      );
                    }
                  }
                } // END OF INNER LOOP
              } // End of outer Loop
            },
            function () {
              // $log.info(err);
              // ErrorToast.showError('Appian Call Unsuccessful');
            }
          );
        } else {
          // ErrorToast.showError(
          // 'No Appian Call because either Event Name is Null or Event Name
			// is not a Array or colon (:) is missing'
          // );
        }
      } // If end
    } // End of protectionClassLookup

    /**
	 * zipCodeLookUP
	 */
    function zipCodeLookUP (
      sysRefID,
      eventName,
      zipCodeLength,
      ZIPdata,
      locationIndex
    ) {
      if (
        sysRefID == 'LocationInput.ZipCode' &&
        !zipCodeLength &&
        ZIPdata &&
        ZIPdata.length > 0
      ) {
        if (ZIPdata != vm.zipCodeValidate) {
          vm.protectionClassCode = null;
          if (
            eventName &&
            Array.isArray (eventName) &&
            eventName[0] &&
            eventName[0].indexOf (':') !== -1
          ) {
            // AJAX call for ZIPCODE
            var zipCodeEventName = eventName[0].split (':')[1];
            var zipCodeAJAXData = {
              elementType: ['RISK'],
              operationType: [zipCodeEventName],
              data: [ZIPdata],
              locationId: vm.explocData[0].locationId,
            };

            // $log.info('Zip Code Look UP Sending = ',zipCodeAJAXData);
            locationService.zipCodeLookUP (zipCodeAJAXData).then (
              function (res) {
                // console.log(res)

                _.find (res.data, function (obj) {
                  if (
                    obj.systemRefId ==
                    'LocationBusinessOwnersInput.ProtectionClass'
                  ) {
                    vm.zipCodeDataForProtectionClass = angular.copy (
                      obj.valueName
                    );
                  }
                });

                for (
                  var businessAddressLoop = 0;
                  businessAddressLoop < vm.explocData[0].LocationAddress.length;
                  businessAddressLoop++
                ) {
                  for (
                    var zipCodelookUPLoop = 0;
                    zipCodelookUPLoop < res.data.length;
                    zipCodelookUPLoop++
                  ) {
                    if (
                      vm.explocData[0].LocationAddress[businessAddressLoop]
                        .systemRefId == res.data[zipCodelookUPLoop].systemRefId
                    ) {
                      vm.explocData[0].LocationAddress[
                        businessAddressLoop
                      ].userValue =
                        res.data[zipCodelookUPLoop].userValue;

                      vm.explocData[0].LocationAddress[
                        businessAddressLoop
                      ].valueName =
                        res.data[zipCodelookUPLoop].valueName;
                    }
                  } // END OF INNER LOOP
                } // End of outer Loop

                _.chain (vm.explocData[0].LocationAddress).map (function (
                  resdata
                ) {
                  if (resdata.systemRefId == 'LocationInput.State') {
                    stateValidation (
                      resdata.userValue,
                      resdata.systemRefId,
                      resdata.eventName,
                      locationIndex
                    );
                  }
                });
                vm.lengthOfZip = false;
              },
              function () {
                // $log.info(err);
                // ErrorToast.showError('Appian Call Unsuccessful');
              }
            );
          } else {
            // ErrorToast.showError(
            // 'No Appian Call because either Event Name is Null or Event Name
			// is not a Array or colon (:) is missing'
            // );
          }
          vm.zipCodeValidate = ZIPdata;
        } else {
          _.chain (vm.explocData[0].LocationAddress).map (function (
            resZipData
          ) {
            if (
              vm.veriskSuggestionBox &&
              resZipData.systemRefId ==
                'LocationBusinessOwnersInput.ProtectionClass'
            ) {
              vm.protectionClassCode = vm.protectionClassCode.trim ();

              _.find (resZipData.valueName, function (loc) {
                if (
                  isNaN (loc.caption) &&
                  loc.caption == vm.protectionClassCode
                ) {
                  resZipData.userValue = loc.caption;

                  return true;
                } else if (
                  parseInt (loc.caption) == parseInt (vm.protectionClassCode)
                ) {
                  resZipData.userValue = loc.caption;

                  return true;
                } else {
                }
              }); // End of .find
            } // End of If
          });
          // If end
          return;
        }
      } // If end
    } // End of zipCodeLookUP

    /**
	 * Get locationSeviceCallBlur
	 */
    function locationSeviceCallBlur (
      LocationAddress,
      locationId,
      explocData,
      locationForm,
      eventMode,
      locID
    ) {
      vm.locID = locID;
      console.log ('vm.locationSerChecking1', vm.locationSerChecking1);
      console.log ('vm.locationSerChecking', vm.locationSerChecking);
      console.log ('locID', locID);
      if (vm.locationSerChecking1 == true) {
        return;
      }
      locationSeviceCall (
        LocationAddress,
        locationId,
        explocData,
        locationForm,
        eventMode,
        locID
      );
    }

    /**
	 * Get locationSeviceCall
	 */
    function locationSeviceCall (
      LocationAddress,
      locationId,
      explocData,
      locationForm,
      eventMode,
      locID
    ) {
      console.log ('vm.locID', vm.locID);
      console.log ('vm.locationSerChecking', vm.locationSerChecking);
      if (vm.locationSerChecking == true) {
        return;
      }

      for (var ikk = 0; ikk < vm.explocData[0].LocationAddress.length; ikk++) {
        if (
          vm.explocData[0].LocationAddress[ikk].systemRefId ==
            'LocationInput.Address1' ||
          vm.explocData[0].LocationAddress[ikk].systemRefId ==
            'LocationInput.Address2' ||
          vm.explocData[0].LocationAddress[ikk].systemRefId ==
            'LocationInput.City' ||
          vm.explocData[0].LocationAddress[ikk].systemRefId ==
            'LocationInput.State' ||
          vm.explocData[0].LocationAddress[ikk].systemRefId ==
            'LocationInput.ZipCode'
        ) {
          var ab = vm.explocData[0].LocationAddress[ikk].systemRefId;
          var bc = vm.explocData[0].LocationAddress[ikk].userValue;

          vm.locationCallObj[ab] = bc;
        }
      }

      vm.locationCallObj.locationId = vm.explocData[0].locationId;

      if (
        vm.locationCallObj['LocationInput.Address1'] &&
        vm.locationCallObj['LocationInput.City'] &&
        vm.locationCallObj['LocationInput.State'] &&
        vm.locationCallObj['LocationInput.ZipCode']
      ) {
        console.log (' vm.locationCallObj ', vm.locationCallObj);
        var data = {
          data: [vm.locationCallObj],
          elementType: ['RISK'],
          operationType: ['LOCATIONSERVICE'],
        };

        locationService.locationSeviceCall (data).then (
          function (res) {
            _.chain (res.data).map (function (LocationsevItem) {
              if (LocationsevItem.systemRefId == 'scrubStatus') {
                if (
                  LocationsevItem.userValue == 'Group1 Unavailable.Contact IT'
                ) {
                  vm.locservicerror = true;
                } else {
                  vm.locservicerror = false;
                }
              }
              if (LocationsevItem.systemRefId == 'scrubStatus') {
                if (
                  LocationsevItem.userValue.toLowerCase () == 'success' ||
                  LocationsevItem.userValue.toLowerCase () == 'warning' ||
                  LocationsevItem.userValue.toLowerCase () == 'failure'
                ) {
                  vm.locservicerror = false;
                  if (LocationsevItem.systemRefId == 'latitude') {
                    vm.lat = LocationsevItem.userValue;
                  }
                  if (LocationsevItem.systemRefId == 'longitude') {
                    vm.lng = LocationsevItem.userValue;
                  }

                  for (
                    var LocationAddressLoop = 0;
                    LocationAddressLoop <
                    vm.explocData[0].LocationAddress.length;
                    LocationAddressLoop++
                  ) {
                    for (
                      var zipCodelookUPLoop = 0;
                      zipCodelookUPLoop < res.data.length;
                      zipCodelookUPLoop++
                    ) {
                      if (
                        vm.explocData[0].LocationAddress[LocationAddressLoop]
                          .systemRefId ==
                        res.data[zipCodelookUPLoop].systemRefId
                      ) {
                        vm.explocData[0].LocationAddress[
                          LocationAddressLoop
                        ].userValue =
                          res.data[zipCodelookUPLoop].userValue;
                      }
                    } // END OF INNER LOOP
                  } // End of outer Loop

                  // Since Location service is SuccessFull, start checking
					// locationAddress == businessAddress
                  // if YES then 'vm.checkLocAddEqualBusAdd' will be TRUE else
					// FALSE

                  vm.checkLocAddEqualBusAdd = true;
                  // console.log(LocationAddress);
                  // console.log("?????????",vm.businessAddressLocationPage);
                  _.find (LocationAddress, function (loc) {
                    // console.log(loc.systemRefId);
                    // console.log(loc.userValue);

                    if (loc.controlName.toLowerCase () != 'header') {
                      if (loc.systemRefId == 'LocationInput.Address1') {
                        // Address Line 1

                        if (
                          loc.userValue !=
                          vm.businessAddressLocationPage.streetAddress
                        ) {
                          // console.log("1");
                          vm.checkLocAddEqualBusAdd = false;
                          return;
                        }
                      } else if (loc.systemRefId == 'LocationInput.Address2') {
                        // Address Line 2

                        if (
                          loc.userValue != vm.businessAddressLocationPage.suite
                        ) {
                          // console.log("2");
                          vm.checkLocAddEqualBusAdd = false;
                          return;
                        }
                      } else if (loc.systemRefId == 'LocationInput.City') {
                        // City

                        if (
                          loc.userValue != vm.businessAddressLocationPage.city
                        ) {
                          // console.log("3");
                          vm.checkLocAddEqualBusAdd = false;
                          return;
                        }
                      } else if (loc.systemRefId == 'state') {
                        // State

                        if (
                          loc.userValue != vm.businessAddressLocationPage.state
                        ) {
                          // console.log("4");
                          vm.checkLocAddEqualBusAdd = false;
                          return;
                        }
                      } else if (loc.systemRefId == 'LocationInput.ZipCode') {
                        // ZipCode

                        if (
                          loc.userValue != vm.businessAddressLocationPage.zip
                        ) {
                          // console.log("5");

                          vm.checkLocAddEqualBusAdd = false;
                          return;
                        }
                      }
                    } // if condition for 'Header'
                  }); // each Loop

                  // console.log("vm.checkLocAddEqualBusAdd = ",
					// vm.checkLocAddEqualBusAdd);

                  if (vm.checkLocAddEqualBusAdd && vm.locID == 1) {
                    /**
					 * Parameters for Verisk
					 */
                    vm.tempBA = {};
                    vm.tempBA.StreetName =
                      vm.businessAddressLocationPage.streetAddress;

                    vm.tempBA.City = vm.businessAddressLocationPage.city;

                    vm.tempBA.State = vm.businessAddressLocationPage.state;

                    vm.tempBA.Zip = vm.businessAddressLocationPage.zip;

                    vm.tempBA.BusinessName =
                      vm.businessAddressLocationPage.businessName;

                    var veriskData = {
                      elementType: ['BUSINESSCLASS'],
                      operationType: ['GETVERISK'],
                      data: [[vm.tempBA]],
                    };

                    // console.log("versik Call = ",veriskData);
                    /**
					 * AJAX Call FOR VERISK
					 */
                    locationService.veriskCall (veriskData).then (
                      function (response) {
                        // console.log("verisk response on location ",response);
                        if (response.data.veriskData) {
                          if (
                            response.data.veriskData
                              .veriskBusinessRecordsMatched == 1
                          ) {
                            vm.veriskSuggestionBox = true;
                            vm.yearBuilt = 'No data available';
                            vm.squareFootage = 'No data available';
                            vm.constructionVeriskBox = 'No data available';
                            vm.constructionCode = 'No data available';

                            // vm.businessInfoMagicButton = true;

                            // protectionClassCode of verisk response
                            if (
                              response.data.veriskData.locations[0].buildings[0]
                                .protectionClassCode
                            ) {
                              vm.protectionClassCode =
                                response.data.veriskData.locations[0].buildings[0].protectionClassCode;

                              vm.protectionClassCode = vm.protectionClassCode.trim ();

                              console.log (
                                '>>>>>>>>>>>>',
                                vm.protectionClassCode
                              );
                              _.find (
                                vm.zipCodeDataForProtectionClass,
                                function (obj) {
                                  console.log ('<<<<<<<<<<<<<<', obj);

                                  if (isNaN (obj.caption)) {
                                    if (obj.caption == vm.protectionClassCode) {
                                      // console.log("c");

                                      _.find (
                                        vm.locationMasterData[0]
                                          .LocationAddress,
                                        function (obj1) {
                                          if (
                                            obj1.systemRefId ==
                                            'LocationBusinessOwnersInput.ProtectionClass'
                                          ) {
                                            obj1.userValue = obj.caption;
                                            console.log (
                                              '@@@@@@@@@@@@@@@@',
                                              vm.locationMasterData[0]
                                                .LocationAddress
                                            );
                                            return true;
                                          }
                                        }
                                      );

                                      return;
                                    }
                                  } else {
                                    // console.log("z=v");

                                    if (
                                      parseInt (obj.caption) ==
                                      parseInt (vm.protectionClassCode)
                                    ) {
                                      _.find (
                                        vm.locationMasterData[0]
                                          .LocationAddress,
                                        function (obj1) {
                                          if (
                                            obj1.systemRefId ==
                                            'LocationBusinessOwnersInput.ProtectionClass'
                                          ) {
                                            obj1.userValue = obj.caption;
                                            console.log (
                                              '1241242342313213',
                                              vm.locationMasterData[0]
                                                .LocationAddress
                                            );
                                            return true;
                                          }
                                        }
                                      );
                                      return;
                                    }
                                  }
                                }
                              );
                            }
                            // Occupancies of verisk response

                            if (
                              response.data.veriskData.locations[0].buildings[0]
                                .occupancies
                            ) {
                              vm.veriskOccupancies =
                                response.data.veriskData.locations[0].buildings[0].occupancies;
                            }
                            // Sprinklered of verisk response

                            if (
                              response.data.veriskData.locations[0].buildings[0]
                                .sprinklered
                            ) {
                              vm.veriskSprinkler =
                                response.data.veriskData.locations[0].buildings[0].sprinklered;
                            }
                            // Year Built in verisk response
                            if (
                              response.data.veriskData.locations[0].buildings[0]
                                .yearBuilt
                            ) {
                              vm.yearBuilt =
                                response.data.veriskData.locations[0].buildings[0].yearBuilt;
                            }
                            // SquareFootage in verisk response
                            if (
                              response.data.veriskData.locations[0].buildings[0]
                                .squareFootage
                            ) {
                              vm.squareFootage =
                                response.data.veriskData.locations[0].buildings[0].squareFootage;
                            }
                            // ConstructionCode in verisk response
                            if (
                              response.data.veriskData.locations[0].buildings[0]
                                .constructionCode
                            ) {
                              vm.constructionCode =
                                response.data.veriskData.locations[0].buildings[0].constructionCode;
                            }
                          } else {
                            vm.veriskSuggestionBox = false;
                          }
                        } else {
                          vm.veriskSuggestionBox = false;
                        }
                      },
                      function () {
                        // ErrorToast.showError("VERISK CALL FAILED");
                      }
                    );
                  } else {
                    // checking vm.checkLocAddEqualBusAdd
                    // ErrorToast.showError("Location Address != business
					// Address");
                  }
                }
              }
            });
            vm.locationSerChecking = true;
            vm.locationSerChecking1 = true;
            if (vm.appianBefSaveLoc == true) {
              vm.appianBefSaveLoc = false;
              saveExpandedLocation (
                locationId,
                locID,
                explocData,
                locationForm
              );
            }
            if (vm.appianBefSaveLoc1 == true) {
              vm.appianBefSaveLoc1 = false;
              saveLocation (
                locationId,
                explocData,
                locationForm,
                eventMode,
                locID
              );
            }
          },
          function (err) {
            ErrorToast.showError (err.detail);
          }
        );
      }
    }

    /**
	 * Get expanded view location data
	 */
    function getExpandedLocData () {
      var ZipCodeValidate;
      vm.explocData = ParseJSON.displaySeq (vm.locationMasterData);

      locationCallSystemRefId (
        vm.explocData[0].LocationAddress,
        vm.explocData[0].locationId
      );

      _.chain (vm.explocData[0].LocationAddress).map (function (LocationItem) {
        if (LocationItem.systemRefId == 'LocationInput.ZipCode') {
          ZipCodeValidate = LocationItem;
          // console.log("ZipCodeValidate.systemRefId",ZipCodeValidate.systemRefId);
          zipCodeLookUP (
            ZipCodeValidate.systemRefId,
            ZipCodeValidate.eventName,
            vm.lengthOfZip,
            ZipCodeValidate.userValue,
            vm.explocData.length
          );
        }
      });

      vm.viewModeBtn ();
      vm.editingExistingLocation = true;

      vm.formSendData.elementType = ['RISK'];
      vm.formSendData.operationType = ['SAVELOCATION'];
      vm.formSendData.data = [vm.explocData[0]];

      $rootScope.pageInfo = [
        {
          currentPage: $location.path (),
          currentPageData: vm.formSendData.data,
          element: vm.formSendData.elementType,
          operation: vm.formSendData.operationType,
        },
      ];
      // },
      // function(err) {
      // ErrorToast.showError(err.detail);

      // }
      // );
    }

    $rootScope.$on ('editConfirm', function () {
      vm.explocData = [];
      vm.formSendData = {
        elementType: null,
        operationType: null,
        data: null,
      };
    });
    $rootScope.$on ('CallParentMethod', function () {
      getLocationData ();
    });

    /**
	 * Get location data
	 */
    function getLocationData () {
      var data = {
        elementType: ['RISK'],
        operationType: ['VIEWLOCATION'],
      };

      // call service
      locationService.fetchLocationData (data).then (
        function (res) {
          vm.fullLocationData = res.data;
          // vm.locData = angular.copy(res.data.LocationsData);
          vm.locData = ParseJSON.displaySeq (res.data.LocationsData);

          vm.locID = vm.locData.length;
          vm.saveDisable = false;
          vm.addsavebuild = false;
          vm.buildUnderCancel = false;
          vm.buildingLimitMsg = null;
          vm.yearerrmsg = null;
          vm.numberofstorieserrmsg = null;
          vm.OccSquareFootageMsg = null;
          vm.squareFootageMsg = null;
          vm.buildingPLimitMsg = null;
          vm.appianBefSave = false;
          vm.locationSerChecking = false;
          vm.locationSerChecking1 = false;
          vm.appianBefSaveUnder = false;
          vm.appianBefSaveLoc = false;
          vm.appianBefSaveLoc1 = false;
          vm.disableSubmit = false;
          vm.cancelHide = true;
          vm.zipCodeValidate = null;
          vm.protErrMsg = null;

          _.chain (vm.locData).map (function (locData, index) {
            vm.Emptybuilding = _.isEmpty (locData.BuildingData[0]);
            if (vm.Emptybuilding == true) {
              vm.locCheckIndex = index;
            }
          });

          _.chain (vm.locData[0].LocationAddress).map (function (resdata) {
            if (resdata.systemRefId == 'LocationInput.State') {
              stateSummaryValidation (
                resdata.userValue,
                resdata.systemRefId,
                resdata.eventName
              );
            }
          });

          locationDisable (vm.locData);
          vm.viewModeBtn ();
          vm.defaultLocPage = true;
        },
        function () {
          // ErrorToast.showError(err.detail);
        }
      );
    }

    /**
	 * desc ziplengthmax
	 * 
	 * @param {*}
	 *            $event
	 * @param {*}
	 *            value
	 * @param {*}
	 *            sysID
	 */
    function ziplengthmax ($event, value, sysID) {
      if (
        value.length != null &&
        value.length >= 5 &&
        sysID == 'LocationInput.ZipCode'
      ) {
        $event.preventDefault ();
      }

      if (value.length >= 50 && sysID == 'LocationInput.Address1') {
        $event.preventDefault ();
      }
    }

    /**
	 * desc ziplengthmin
	 * 
	 * @param {*}
	 *            $event
	 * @param {*}
	 *            value
	 * @param {*}
	 *            sysID
	 */
    function ziplengthmin ($event, value, sysID) {
      if (value.length < 5 && sysID == 'LocationInput.ZipCode') {
        return true;
      } else {
        return false;
      }
    }

    /**
	 * Make zip code field only contains number.No character and NO special
	 * character
	 * 
	 * @param {*}
	 *            $event
	 */
    function ZIPCodeValidation ($event) {
      if (
        // ($event.keyCode >= 32 && $event.keyCode <= 47) ||
        ($event.keyCode < 96 && $event.keyCode > 105) ||
        ($event.keyCode >= 65 && $event.keyCode <= 90) ||
        $event.keyCode > 105
      ) {
        $event.preventDefault ();
      }

      switch ($event.key) {
        case '!':
        case '@':
        case '#':
        case '$':
        case '%':
        case '^':
        case '&':
        case '*':
        case '(':
        case ')':
          $event.preventDefault ();
          break;
      }
    } // END of number validation

    /**
	 * desc locEditMode
	 */
    function locEditMode () {
      vm.viewMode = true;
      vm.showBuildingForm = false;
      vm.showLocationForm = false;
    }

    /**
	 * desc
	 * 
	 * @param {*}
	 *            locIndex
	 */
    function removeLocation (locationId, locIndex) {
      locModalPopup (locationId, locIndex);
    }

    /**
	 * desc
	 * 
	 * @param {*}
	 *            locIndex
	 * @param {*}
	 *            buildIndex
	 */
    function removeBuilding (locationId, buildingId, locIndex, buildIndex) {
      buildModalPopup (locationId, buildingId, buildIndex, locIndex);
    }

    /**
	 * desc duplicateBuilding
	 * 
	 * @param {*}
	 *            locIndex
	 * @param {*}
	 *            buildIndex
	 * @param {*}
	 *            building
	 */
    function duplicateBuilding (
      locId,
      buildId,
      building,
      locIndex,
      buildIndex
    ) {
      // console.log("building",building);
      // console.log("buildId",buildId);
      // console.log("locId",locId);
      var data = {
        elementType: ['RISK'],
        operationType: ['DUPLICATEBUILDING'],
        data: [building],
        locIndex: [locIndex],
        buildIndex: [buildIndex],
      };
      // call service

      locationService.duplicateBuilding (data).then (
        function () {
          getLocationData ();
        },
        function () {
          // ErrorToast.showError(err.detail);
        }
      );

      // vm.locData[0].location[locIndex].buildings.push(
      // angular.copy(vm.locData[0].location[locIndex].buildings[buildIndex])
      // );
    }

    /**
	 * desc
	 * 
	 * @param {*}
	 *            locIndex
	 * @param {*}
	 *            buildIndex
	 */

    function editBuilding (
      locationId,
      buildingId,
      locationName,
      locIndex,
      buildIndex,
      BuildingDataSE
    ) {
      var data;
      if (BuildingDataSE != undefined) {
        vm.buildingData = [];

        // vm.addOcuData = [];

        vm.locID = locIndex + 1;
        vm.buildingID = buildIndex;
        vm.annualSaleCountErr = null;
        vm.currentBuild = vm.locData[locIndex].BuildingData[buildIndex];

        vm.editEmptybuilding = _.isEmpty (vm.currentBuild);

        if (vm.editEmptybuilding) {
          data = {
            firstBuilding: [true],
            dataType: ['MASTER'],
            locationIndex: [locIndex],
            elementType: ['RISK'],
            operationType: ['VIEW_BUILDING'],
            locationId: [locationId],
          };

          // call service
          locationService.fetchBuildingData (data).then (
            function (res) {
              var occData = {
                elementType: ['OCCUPANT'],
                operationType: ['OCCUPANT_BUSINESSCLASSLOOKUP'],
              };

              locationService
                .fetchBusinessClass (occData)
                .then (function (res) {
                  vm.businessClassValues = res.data[0].valueName;
                  vm.businessClassValues1 = res.data[0].valueName;
                });

              vm.savedBuildingOccupants = true;

              // Enabling Occupant Section

              if (vm.OccAppianValidation && vm.savedBuildingOccupants) {
                vm.showOccupantForm = true;
                vm.showBuildingForm = true;
              }

              vm.currentBuild = res.data.BuildingData[0];
              vm.currentBuild.locationId = locationId;
              vm.currentBuild.locationName = locationName;
              vm.locationBuildingId = vm.currentBuild.buildingId;
              // vm.addOcuData = vm.currentBuild.occupantData;
              vm.buildingData.push (vm.currentBuild);
              vm.buildingData = ParseJSON.buildingParse (
                vm.buildingData,
                vm.buildingDataUUID
              );
              vm.buildingEventMode = 'add';
              appianSystemRefId (vm.buildingData[0].BuildingQuestions);

              vm.formSendData.elementType = ['RISK'];
              vm.formSendData.operationType = ['SAVEBUILDING'];
            },
            function () {
              // console.log('getBuildingData - error', err.detail);
              // ErrorToast.showError(err.detail);
            }
          );
        } else {
          data = {
            elementType: ['RISK'],
            operationType: ['EDITBUILDING'],
            buildingId: [buildingId],
          };

          locationService.fetchBuildingData (data).then (function (res) {
            // console.log("vm.currentBuild outside",vm.currentBuild);
            vm.buildingData.push (vm.currentBuild);
            vm.addAnotherOccupant = true;

            var data = {
              elementType: ['APPIANRULE'],

              operationType: ['DISPLAY_PERMANENTLY_INSTALLED_EQUIPMENT'],

              data: [
                {
                  opCode: vm.businessClass,
                  state: vm.Lobstate,
                },
              ],
            };
            // console.log("business class
			// ------------------------------------------------------",data);
            locationService.validateStateAppianRule (data).then (
              function (appianRes) {
                _.chain (appianRes.data).map (function (response) {
                  _.chain (vm.buildingData[0].BuildingQuestions).map (function (
                    buildResponse
                  ) {
                    if (response.sysRefId == buildResponse.systemRefId) {
                      if (
                        response.visible == true &&
                        response.visible != null &&
                        response.visible != undefined
                      ) {
                        buildResponse.appearUI = 'Y';
                      }
                      if (response.visible == false) {
                        buildResponse.appearUI = 'N';
                      }
                    }
                  });
                });
              },
              function () {}
            );
            vm.buildingData = ParseJSON.buildingParse (
              vm.buildingData,
              vm.buildingDataUUID
            );
            vm.buildingEventMode = 'edit';
            // vm.addOcuData = [];
            // vm.addOcuData = vm.currentBuild.occupantData;

            vm.addOcuData = [];
            for (var i = 0; i < vm.currentBuild.occupantData.length; i++) {
              if (i > 0) {
                vm.addOcuData.push (vm.currentBuild.occupantData[i]);
              }
            }
            vm.editEmptybuilding = false;

            appianSystemRefId (vm.buildingData[0].BuildingQuestions);

            addBuildingUnderwriting (locationId, buildingId, 'edit');
          });
        }

        vm.isEditingBuilding = true;
        vm.isAddingBuilding = false;

        vm.viewMode = true;
        vm.showBuildingForm = true;
        vm.showLocationForm = true;
        vm.formSendData.elementType = ['RISK'];
        vm.formSendData.operationType = ['SAVEBUILDING'];

        vm.formSendData.data = [vm.locData[locIndex].BuildingData[buildIndex]];
        vm.formSendData.data[0].subjectType = ['EDIT'];
        $rootScope.pageInfo = [
          {
            currentPage: $location.path (),
            currentPageData: vm.formSendData.data,
            element: vm.formSendData.elementType,
            operation: vm.formSendData.operationType,
          },
        ];
      } else {
        vm.buildingData = [];
        // vm.addOcuData = [];
        vm.locID = locIndex + 1;
        vm.buildingID = buildIndex;
        vm.annualSaleCountErr = null;
        vm.currentBuild = vm.locData[locIndex].BuildingData[buildIndex];

        vm.editEmptybuilding = _.isEmpty (vm.currentBuild);

        if (vm.editEmptybuilding) {
          data = {
            firstBuilding: [true],
            dataType: ['MASTER'],
            locationIndex: [locIndex],
            elementType: ['RISK'],
            operationType: ['VIEW_BUILDING'],
            locationId: [locationId],
          };

          // call service
          locationService.fetchBuildingData (data).then (
            function (res) {
              var occData = {
                elementType: ['OCCUPANT'],
                operationType: ['OCCUPANT_BUSINESSCLASSLOOKUP'],
              };

              locationService
                .fetchBusinessClass (occData)
                .then (function (res) {
                  vm.businessClassValues = res.data[0].valueName;
                  vm.businessClassValues1 = res.data[0].valueName;
                });
              var data = {
                elementType: ['APPIANRULE'],

                operationType: ['DISPLAY_PERMANENTLY_INSTALLED_EQUIPMENT'],

                data: [
                  {
                    opCode: vm.businessClass,
                    state: vm.Lobstate,
                  },
                ],
              };
              // console.log("business class
				// ------------------------------------------------------",data);
              locationService.validateStateAppianRule (data).then (
                function (appianReseSE) {
                  _.chain (appianReseSE.data).map (function (responseeSE) {
                    _.chain (
                      res.data.BuildingData[0].BuildingQuestions
                    ).map (function (buildResponseeSE) {
                      if (
                        responseeSE.sysRefId == buildResponseeSE.systemRefId
                      ) {
                        if (
                          responseeSE.visible == true &&
                          responseeSE.visible != null &&
                          responseeSE.visible != undefined
                        ) {
                          buildResponseeSE.appearUI = 'Y';
                        }
                        if (responseeSE.visible == false) {
                          buildResponseeSE.appearUI = 'N';
                        }
                      }
                    });
                  });
                },
                function () {}
              );

              vm.currentBuild = res.data.BuildingData[0];
              vm.currentBuild.locationId = locationId;
              vm.currentBuild.locationName = locationName;
              vm.locationBuildingId = vm.currentBuild.buildingId;
              // vm.addOcuData = vm.currentBuild.occupantData;
              vm.buildingData.push (vm.currentBuild);
              vm.buildingData = ParseJSON.buildingParse (
                vm.buildingData,
                vm.buildingDataUUID
              );
              vm.buildingEventMode = 'add';
              appianSystemRefId (vm.buildingData[0].BuildingQuestions);

              vm.formSendData.elementType = ['RISK'];
              vm.formSendData.operationType = ['SAVEBUILDING'];
            },
            function () {
              // console.log('getBuildingData - error', err.detail);
              // ErrorToast.showError(err.detail);
            }
          );
        } else {
          data = {
            elementType: ['RISK'],
            operationType: ['EDITBUILDING'],
            buildingId: [buildingId],
          };

          locationService.fetchBuildingData (data).then (function (res) {
            console.log ('vm.currentBuild outside', vm.currentBuild);
            vm.buildingData.push (vm.currentBuild);

            var data = {
              elementType: ['APPIANRULE'],

              operationType: ['DISPLAY_PERMANENTLY_INSTALLED_EQUIPMENT'],

              data: [
                {
                  opCode: vm.businessClass,
                  state: vm.Lobstate,
                },
              ],
            };
            // console.log("business class
			// ------------------------------------------------------",data);
            locationService.validateStateAppianRule (data).then (
              function (appianRes) {
                _.chain (appianRes.data).map (function (response) {
                  _.chain (vm.buildingData[0].BuildingQuestions).map (function (
                    buildResponse
                  ) {
                    if (response.sysRefId == buildResponse.systemRefId) {
                      if (
                        response.visible == true &&
                        response.visible != null &&
                        response.visible != undefined
                      ) {
                        buildResponse.appearUI = 'Y';
                      }
                      if (response.visible == false) {
                        buildResponse.appearUI = 'N';
                      }
                    }
                  });
                });
              },
              function () {}
            );
            vm.buildingData = ParseJSON.buildingParse (
              vm.buildingData,
              vm.buildingDataUUID
            );
            vm.buildingEventMode = 'edit';

            vm.addOcuData = [];
            for (var i = 0; i < vm.currentBuild.occupantData.length; i++) {
              if (i > 0) {
                vm.addOcuData.push (vm.currentBuild.occupantData[i]);
              }
            }

            vm.editEmptybuilding = false;
            vm.disableunderSubmit = true;
            vm.disableSubmit = false;
            appianSystemRefId (vm.buildingData[0].BuildingQuestions);

            addBuildingUnderwriting (locationId, buildingId, 'edit');
          });
        }

        vm.isEditingBuilding = true;
        vm.isAddingBuilding = false;

        vm.viewMode = true;
        vm.showBuildingForm = true;
        vm.showLocationForm = true;
        vm.formSendData.elementType = ['RISK'];
        vm.formSendData.operationType = ['SAVEBUILDING'];

        vm.formSendData.data = [vm.locData[locIndex].BuildingData[buildIndex]];
        vm.formSendData.data[0].subjectType = ['EDIT'];
        $rootScope.pageInfo = [
          {
            currentPage: $location.path (),
            currentPageData: vm.formSendData.data,
            element: vm.formSendData.elementType,
            operation: vm.formSendData.operationType,
          },
        ];
      }
    }

    /**
	 * Load location edit form
	 * 
	 * @param {*}
	 *            locationID
	 */
    function editLocation (locationId, locIndex, locationMasterData) {
      if (locationMasterData != undefined) {
        vm.explocData = [];
        vm.addlocData = [];
        vm.annualSaleCountErr = null;

        vm.currentLoc = locationMasterData;
        vm.addlocData.push (vm.currentLoc);
        vm.explocData = ParseJSON.displaySeq (vm.addlocData);
        _.chain (vm.explocData[0].LocationAddress).map (function (resdata) {
          if (resdata.systemRefId == 'LocationInput.State') {
            stateValidation (
              resdata.userValue,
              resdata.systemRefId,
              resdata.eventName,
              locIndex + 1
            );
          }
        });

        vm.annualSaleCountEr = null;
        vm.eventMode = 'edit';
        vm.locID = locIndex + 1;
        vm.viewMode = true;
        vm.showBuildingForm = false;
        vm.showLocationForm = false;
        vm.formSendData.elementType = ['RISK'];
        vm.formSendData.operationType = ['SAVELOCATION'];

        vm.formSendData.data = [vm.explocData[0]];
        vm.formSendData.data[0].subjectType = ['EDIT'];
        $rootScope.pageInfo = [
          {
            currentPage: $location.path (),
            currentPageData: vm.formSendData.data,
            element: vm.formSendData.elementType,
            operation: vm.formSendData.operationType,
          },
        ];
      } else {
        vm.explocData = [];
        vm.addlocData = [];
        vm.annualSaleCountErr = null;
        var data = {
          elementType: ['RISK'],
          operationType: ['VIEW_EDIT_LOCATION'],
          locationIndex: [locIndex],
          locationId: [locationId],
        };

        // call service
        locationService.editLocation (data).then (
          function (res) {
            vm.currentLoc = res.data.LocationsData[0];
            vm.addlocData.push (vm.currentLoc);
            vm.explocData = ParseJSON.displaySeq (vm.addlocData);
            locationSeviceCall (vm.explocData[0].LocationAddress);
            vm.annualSaleCountEr = null;
            vm.eventMode = 'edit';
            vm.locID = locIndex + 1;
            vm.viewMode = true;
            vm.showBuildingForm = false;
            vm.showLocationForm = false;
            vm.formSendData.elementType = ['RISK'];
            vm.formSendData.operationType = ['SAVELOCATION'];

            vm.formSendData.data = [vm.explocData[0]];
            vm.formSendData.data[0].subjectType = ['EDIT'];
            $rootScope.pageInfo = [
              {
                currentPage: $location.path (),
                currentPageData: vm.formSendData.data,
                element: vm.formSendData.elementType,
                operation: vm.formSendData.operationType,
              },
            ];
          },
          function () {
            // ErrorToast.showError(err.detail);
            // console.log('getLocationData - error', err.detail);
          }
        );
      }
    }

    /**
	 * Load location cancel
	 * 
	 */
    function locationCancel (currLocItem, locIndex, eventMode) {
      var locCanObj = {};
      var data;
      locCanObj.locationId = currLocItem.locationId[0];
      locCanObj.locIndex = locIndex - 1;
      if (eventMode == 'add') {
        data = {
          elementType: ['RISK'],

          operationType: ['CANCELLOCATION'],

          data: [locCanObj],
        };
        // call service

        locationService.deletebuilding (data).then (
          function () {
            vm.formSendData = {
              elementType: null,
              operationType: null,
              data: null,
            };
            vm.explocData = [];
            getLocationData ();
            viewModeBtn ();
          },
          function (err) {
            ErrorToast.showError (err.detail);
          }
        );
      }
      if (eventMode == 'edit') {
        data = {
          elementType: ['RISK'],

          operationType: ['CANCEL_EDIT_LOCATION'],

          data: [locCanObj],
        };
        // call service

        locationService.deletebuilding (data).then (
          function () {
            vm.formSendData = {
              elementType: null,
              operationType: null,
              data: null,
            };
            vm.explocData = [];
            getLocationData ();
            viewModeBtn ();
          },
          function (err) {
            ErrorToast.showError (err.detail);
          }
        );
      }
    }

    /**
	 * desc saveOccupant
	 * 
	 * @param {*}
	 *            theForm
	 */
    function saveOccupant (ocuData, index, occupants) {
      vm.saveIndex = index;
      vm.disableunderSubmit = true;
      vm.disableSubmit = false;
      vm.buildingOccupantId = ocuData.occupantId;

      vm.OccupantData = businessClassCaptionValue (
        occupants,
        vm.businessClassValues
      );

      // for (var i = 0; i < vm.OccupantData.length; i++) {
      // for (var j = 0; j < vm.OccupantData[i].occupantQuestions.length; j++)
		// {

      if (
        vm.OccupantData.occupantQuestions[0].subjectType &&
        vm.OccupantData.occupantQuestions[0].subjectType == 'save'
      ) {
        vm.OccupantData.occupantQuestions[0].subjectType = 'edit';
      } else {
        vm.OccupantData.occupantQuestions[0].subjectType = 'save';
      }

      vm.OccupantData.occupantQuestions[1].userValue =
        vm.OccupantData.occupantQuestions[1].defaultValue;
      vm.OccupantData.occupantQuestions[2].userValue =
        vm.OccupantData.occupantQuestions[2].defaultValue;
      // }
      // }

      var data = {
        elementType: ['OCCUPANT'],
        operationType: ['SAVE_OCCUPANT'],
        buildingId: [vm.locationBuildingId],
        occupantId: [vm.buildingOccupantId],
        data: [vm.OccupantData],
      };

      // call service
      locationService.saveOccupants (data).then (
        function (res) {
          // console.log ('res', res);
          vm.addOcuData = ParseJSON.displaySeq (
            vm.addOcuData,
            'occupantQuestions'
          );
          vm.editOccuDisable = true;

          vm.addOcuData[vm.saveIndex].occupantQuestions[0].disableStatus = true;
          vm.addOcuData[vm.saveIndex].occupantQuestions[0].occInfo = true;
          vm.addOcuData[vm.saveIndex].occupantQuestions[0].occSaved = true;
        },
        function (err) {
          vm.addOcuData[vm.saveIndex].occupantQuestions[0].occInfo = false;
          ErrorToast.showError (err.detail);
          // console.log('save location - error', err.detail);
        }
      );
    }

    /**
	 * desc addNewOccupant
	 */

    // vm.showOccupantForm = true;
    // vm.showBuildingForm = true;

    function addNewOccupant (addingOcuData, buildingDetails, occList) {
      vm.disableunderSubmit = true;
      vm.disableSubmit = false;

      vm.buildingOccupantId = [buildingDetails[0].occupantData[0].occupantId];
      vm.locationBuildingId = buildingDetails[0].buildingId;

      for (var i = 0; i < buildingDetails[0].BuildingQuestions.length; i++) {
        if (
          buildingDetails[0].BuildingQuestions[i].datasetName ==
          'Applicant interest in building'
        ) {
          vm.buildingAppInterest =
            buildingDetails[0].BuildingQuestions[i].displayValue;
        }
      }

      // Applicant interest in building

      var data = {
        elementType: ['OCCUPANT'],
        operationType: ['VIEW_OCCUPANT'],
        buildingId: [vm.locationBuildingId],
        riskId: [buildingDetails[0].riskId],
        occupantId: vm.buildingOccupantId,
        multipleOccupancyBldg: [vm.multipleOccupancyDC],
        applicantInterest: [vm.applicantInterestDC],
      };

      // call service
      locationService.fetchOccupantMaster (data).then (
        function (res) {
          vm.addingOcuDatas = angular.copy (res.data.occupantData);

          vm.addOcuData = vm.addOcuData.concat (vm.addingOcuDatas);

          vm.addOcuData = ParseJSON.displaySeq (
            vm.addOcuData,
            'occupantQuestions'
          );
        },
        function (err) {
          ErrorToast.showError (err.detail);
        }
      );
    }

    /**
	 * desc
	 * 
	 * @param {*}
	 *            occIndex
	 */
    function startsWith (businessClassAndCode, viewValue) {
      var data = [];
      var textTemp;
      var test = [];
      var temp1 = [];

      for (var i = 0; i < businessClassAndCode.length; i++) {
        if (
          businessClassAndCode[i].Caption
            .toLowerCase ()
            .match (viewValue.toLowerCase ())
        ) {
          textTemp = businessClassAndCode[i].Caption
            .toLowerCase ()
            .indexOf (viewValue.toLowerCase ());
          data.push ({
            id: textTemp,
            data: businessClassAndCode[i].Caption,
            value: businessClassAndCode[i].Value,
          });
        }
      }

      test = $filter ('orderBy') (data, 'id');

      for (var key in test) {
        temp1.push ({Caption: test[key].data, Value: test[key].value});
      }

      if (temp1) {
        vm.bClassValues = temp1;
        return temp1;
      } else {
        vm.bClassValues = businessClassAndCode;
        return businessClassAndCode;
      }
    } // END of startsWith

    function businessClassCaptionValue (value, businessClassArray) {
      _.each (businessClassArray, function (obj1) {
        if (obj1.Caption == value.occupantQuestions[0].userValue) {
          // value.userValue = obj1.Value;
          value.occupantQuestions[0].dataValues = obj1.Value;
          value.occupantQuestions[0]['dataValue'] = obj1.Value;
          return;
        }
      });

      return value;
    }

    function businessClassValidate (bClass, index) {
      console.log ('11...' + JSON.stringify (vm.businessClassValues));
      console.log ('22...' + JSON.stringify (vm.businessClassValues1));

      for (var i = 0; i < vm.businessClassValues.length; i++) {
        if (
          vm.businessClassValues[i].hasOwnProperty ('Caption') &&
          vm.businessClassValues[i].Caption.toLowerCase () ==
            bClass.toLowerCase ()
        ) {
          vm.addOcuData[index].occupantQuestions[0].occSearch = false;
          // vm.disableSubmit = false;
          // vm.noResults = false;
          break;
        } else {
          // vm.noResults = true;
          vm.addOcuData[index].occupantQuestions[0].occSearch = true;
          // vm.disableSubmit = true;
        }
      }
    }

    // /**
    // * desc
    // * @param {*} occIndex
    // */
    function removeOccupant (occIndex, occData, id) {
      vm.disableunderSubmit = true;
      vm.disableSubmit = false;
      vm.buildingOccupantId = occData.occupantId;
      vm.currentOccIndex = occIndex + 1;

      var data = {
        elementType: ['OCCUPANT'],
        operationType: ['DELETEOCCUPANT'],
        occupantId: [vm.buildingOccupantId],
        OccupantIndex: [vm.currentOccIndex],
      };

      // call service
      locationService.removeOccupants (data).then (
        function (res) {
          vm.addOcuData.splice (occIndex, 1);

          if (vm.addOcuData.length < 1) {
            vm.showOccupantError = false;
          }

          for (var i = 0; i < vm.addOcuData.length; i++) {
            if (!vm.addOcuData[i].occupantQuestions[0].disableStatus) {
              // vm.addOcuData[i].occupantQuestions[0].occSaved = true;
              // vm.submitSaveBuilding = false;
              // vm.disableSubmit = true;
            }
            // }
          }

          if (!vm.addOcuData) {
            vm.disableSubmit = false;
          }
        },
        function (err) {
          // vm.addOcuData.splice (occIndex, 1);
          ErrorToast.showError (err.detail);
        }
      );
    }

    // Editing Occupant

    function editOccupant (index, occData, id, count) {
      vm.disableunderSubmit = true;
      vm.disableSubmit = false;
      vm.buildingOccupantId = occData.occupantId;

      var data = {
        elementType: ['OCCUPANT'],
        operationType: ['ENABLE_EDIT_OCCUPANT'],
        occupantId: [vm.buildingOccupantId],
        // OccupantIndex: [vm.currentOccIndex],
        OccupantIndex: [count],
      };

      // call service
      locationService.editOccupants (data).then (
        function (res) {
          vm.addOcuData[index].occupantQuestions[count].disableStatus = false;
        },
        function (err) {
          ErrorToast.showError (err.detail);
        }
      );
    }

    /**
	 * desc
	 */

    function addNewLocation (locationSEdata) {
      if (locationSEdata != undefined) {
        vm.addinglocData.push (locationSEdata);

        vm.explocData = ParseJSON.displaySeq (vm.addinglocData);

        vm.eventMode = 'add';
        vm.editingExistingLocation = false;
        vm.viewMode = true;
        vm.locID = vm.locData.length + 1;
        // console.log('vm.locID', vm.locID);
        vm.showBuildingForm = false;
        vm.showLocationForm = false;
        vm.formSendData.elementType = ['RISK'];
        vm.formSendData.operationType = ['SAVELOCATION'];
        vm.formSendData.data = [vm.explocData[0]];
        $rootScope.pageInfo = [
          {
            currentPage: $location.path (),
            currentPageData: vm.formSendData.data,
            element: vm.formSendData.elementType,
            operation: vm.formSendData.operationType,
          },
        ];
      } else {
        var data = {
          dataType: ['MASTER'],
          elementType: ['RISK'],
          operationType: ['VIEWLOCATION'],
        };
        vm.annualSaleCountErr = null;
        // call service
        locationService.fetchLocationmaster (data).then (
          function (res) {
            vm.addinglocData = angular.copy (res.data.LocationsData);

            vm.explocData = ParseJSON.displaySeq (vm.addinglocData);

            vm.eventMode = 'add';
            vm.editingExistingLocation = false;
            vm.viewMode = true;
            vm.locID = vm.locData.length + 1;
            // console.log('vm.locID', vm.locID);
            vm.showBuildingForm = false;
            vm.showLocationForm = false;
            vm.formSendData.elementType = ['RISK'];
            vm.formSendData.operationType = ['SAVELOCATION'];
            vm.formSendData.data = [vm.explocData[0]];
            $rootScope.pageInfo = [
              {
                currentPage: $location.path (),
                currentPageData: vm.formSendData.data,
                element: vm.formSendData.elementType,
                operation: vm.formSendData.operationType,
              },
            ];
          },
          function () {
            // ErrorToast.showError(err.detail);
            // console.log('getLocationData - error', err.detail);
          }
        );
        // vm.editingExistingLocation = false;
        // resetlocationpanel();
        // vm.viewMode = true;
        // vm.showBuildingForm = false;
        // vm.showLocationForm = false;
      }
    }

    /**
	 * desc
	 * 
	 * @param {*}
	 *            id
	 */
    function buildEditMode (id) {
      vm.viewMode = true;
      vm.showBuildingForm = true;
      vm.showLocationForm = true;
      vm.editID = id;
    }

    /**
	 * desc getChildUnderwriting
	 * 
	 * @param {*}
	 */
    function getChildUnderwriting (
      buildingData,
      systemRefId,
      buildingQuestions,
      userValue,
      eventName
    ) {
      var buildingChildDataRes = {};

      buildingChildDataRes = angular.copy (
        ParseJSON.returnFields (buildingData, vm.buildingUnderDataUUID)
      );

      var data = {
        dataType: ['MASTER'],
        elementType: ['UNDERWRITINGQUESTIONS'],
        operationType: ['VIEW_BUILD_UNDERWRITING_CHILD'],
        data: [buildingChildDataRes],
        buildingId: [buildingData.buildingId],
        child: ['YES'],
      };

      // call service
      locationService.fetchBuildingUnderData (data).then (
        function (res) {
          vm.buildingUnderData = res.data.BuildingUnderWritingdata;

          _.chain (vm.buildingUnderData[0].BuildingQuestions).map (function (
            checkdata
          ) {
            if (
              checkdata.systemRefId == 'BuildingInput.UNIG_BuildingAnnualSales'
            ) {
              checkdata.defaultValue =
                '$' +
                checkdata.defaultValue
                  .toString ()
                  .replace (/\B(?=(\d{3})+(?!\d))/g, ',');

              checkdata.userValue = checkdata.defaultValue;
              if (
                checkdata.defaultValue != null &&
                checkdata.defaultValue != undefined &&
                checkdata.defaultValue.includes ('$') != true
              ) {
                checkdata.userValue =
                  '$' +
                  checkdata.userValue
                    .toString ()
                    .replace (/\B(?=(\d{3})+(?!\d))/g, ',');
              }
            }

            if (
              checkdata.systemRefId == 'Have_all_of_the_following_been' &&
              checkdata.userValue != null
            ) {
              yearUnder (
                checkdata.systemRefId,
                vm.buildingUnderData[0].BuildingQuestions,
                checkdata.userValue,
                checkdata.eventName
              );
            }
          });
          console.log (
            'vm.buildingUnderData[0].BuildingQuestions',
            vm.buildingUnderData[0].BuildingQuestions
          );
          // _.chain (vm.buildingUnderData[0].BuildingQuestions).map (function
			// (
          // checkdata
          // ) {
          // if (
          // checkdata.systemRefId ==
          // 'UNIG_BuildingUndQuestionsInput.ElectricalYearUpdated' &&
          // checkdata.userValue == ''
          // ) {
          // if (checkdata.systemRefId == '') {
          // }
          // } else if (
          // checkdata.systemRefId == '' &&
          // checkdata.userValue == ''
          // ) {
          // if (checkdata.systemRefId == '') {
          // }
          // } else {
          // if (checkdata.systemRefId == '') {
          // }
          // }
          // if (checkdata.systemRefId == '' && checkdata.userValue == '') {
          // if (checkdata.systemRefId == '') {
          // }
          // } else if (
          // checkdata.systemRefId == '' &&
          // checkdata.userValue == ''
          // ) {
          // if (checkdata.systemRefId == '') {
          // }
          // } else {
          // if (checkdata.systemRefId == '') {
          // }
          // }
          // if (checkdata.systemRefId == '' && checkdata.userValue == '') {
          // if (checkdata.systemRefId == '') {
          // }
          // } else if (
          // checkdata.systemRefId == '' &&
          // checkdata.userValue == ''
          // ) {
          // if (checkdata.systemRefId == '') {
          // }
          // } else {
          // if (checkdata.systemRefId == '') {
          // }
          // }
          // if (checkdata.systemRefId == '' && checkdata.userValue == '') {
          // if (checkdata.systemRefId == '') {
          // }
          // } else if (
          // checkdata.systemRefId == '' &&
          // checkdata.userValue == ''
          // ) {
          // if (checkdata.systemRefId == '') {
          // }
          // } else {
          // if (checkdata.systemRefId == '') {
          // }
          // }
          // });
          vm.buildingUnderData = ParseJSON.buildingParse (
            vm.buildingUnderData,
            vm.buildingUnderDataUUID
          );
          console.log ('vm.buildingUnderData', vm.buildingUnderData);
          appianSystemRefIdUnder (vm.buildingUnderData[0].BuildingQuestions);

          if (
            userValue != null &&
            eventName != null &&
            systemRefId != 'Have_all_of_the_following_been'
          ) {
            vm.addAditionalCall = true;
            vm.addAditionalUnder (
              systemRefId,
              buildingQuestions,
              userValue,
              eventName,
              vm.buildingUnderFormElement,
              vm.buildingUnderData,
              vm.buildingEventMode
            );
            vm.yearBuiltAddFieldUnder (
              systemRefId,
              buildingQuestions,
              userValue,
              eventName
            );
          }
          if (
            userValue != null &&
            eventName != null &&
            systemRefId == 'Have_all_of_the_following_been'
          ) {
            yearUnder (systemRefId, buildingQuestions, userValue, eventName);
          }
          vm.disableSubmit = true;
          vm.disableunderSubmit = false;
          vm.cancelHide = false;
          vm.buildunderqa = true;
          vm.isEditingBuilding = false;
          vm.isAddingBuilding = true;
          vm.defaultLocPage = true;
          vm.viewMode = true;
          vm.showBuildingForm = true;
          vm.showLocationForm = true;
          vm.underappainCheck = true;
        },
        function () {
          // console.log('getBuildingData - error', err.detail);
          // ErrorToast.showError(err.detail);
        }
      );
    }

    /**
	 * desc addBuildingUnderwriting
	 * 
	 * @param {*}
	 *            buildingId
	 */
    function addBuildingUnderwriting (locationId, buildingId, eventMode) {
      var data = {
        dataType: ['MASTER'],
        elementType: ['UNDERWRITINGQUESTIONS'],
        operationType: ['VIEW_BUILD_UNDERWRITING_QUESTIONS'],
        buildingId: [buildingId],
        locationId: [locationId],
      };

      // call service
      locationService.fetchBuildingUnderData (data).then (
        function (res) {
          vm.buildingUnderData = res.data.BuildingUnderWritingdata;

          _.chain (vm.buildingUnderData[0].BuildingQuestions).map (function (
            checkdata
          ) {
            if (
              checkdata.systemRefId == 'BuildingInput.UNIG_BuildingAnnualSales'
            ) {
              checkdata.defaultValue =
                '$' +
                checkdata.defaultValue
                  .toString ()
                  .replace (/\B(?=(\d{3})+(?!\d))/g, ',');
              checkdata.userValue = checkdata.defaultValue;
              if (
                checkdata.defaultValue != null &&
                checkdata.defaultValue != undefined &&
                checkdata.defaultValue.includes ('$') != true
              ) {
                checkdata.userValue =
                  '$' +
                  checkdata.userValue
                    .toString ()
                    .replace (/\B(?=(\d{3})+(?!\d))/g, ',');
              }
            }

            if (
              checkdata.systemRefId == 'Have_all_of_the_following_been' &&
              checkdata.userValue != null
            ) {
              yearUnder (
                checkdata.systemRefId,
                vm.buildingUnderData[0].BuildingQuestions,
                checkdata.userValue,
                checkdata.eventName
              );
            }
            console.log ('checkdata', checkdata);
          });
          vm.buildingUnderData = ParseJSON.buildingParse (
            vm.buildingUnderData,
            vm.buildingUnderDataUUID
          );
          if (eventMode == 'edit') {
            vm.disableSubmit = false;
            vm.disableunderSubmit = true;
          } else {
            vm.disableSubmit = true;
            vm.disableunderSubmit = false;
          }

          vm.cancelHide = false;

          vm.buildunderqa = true;

          vm.buildingUnderEventMode = eventMode;
          appianSystemRefIdUnder (vm.buildingUnderData[0].BuildingQuestions);
          vm.defaultLocPage = true;
          vm.viewMode = true;
          vm.showBuildingForm = true;
          vm.showLocationForm = true;
        },
        function () {
          // console.log('getBuildingData - error', err.detail);
          // ErrorToast.showError(err.detail);
        }
      );
    }

    // end building underwriting

    /**
	 * desc
	 * 
	 * @param {*}
	 *            locIndex
	 */
    function addNewBuilding (
      locIndex,
      buildIndex,
      locationId,
      locationName,
      BuildingData,
      checkFirstBuildingOrNot,
      BuildingDataSE
    ) {
      if (BuildingDataSE != undefined) {
        vm.buildingData.push (BuildingDataSE);
        var data = {
          elementType: ['APPIANRULE'],

          operationType: ['DISPLAY_PERMANENTLY_INSTALLED_EQUIPMENT'],

          data: [
            {
              opCode: vm.businessClass,
              state: vm.Lobstate,
            },
          ],
        };
        // console.log("business class
		// ------------------------------------------------------",data);
        locationService.validateStateAppianRule (data).then (
          function (appianResSE) {
            vm.annualSaleCountErr = null;

            _.chain (appianResSE.data).map (function (responseSE) {
              _.chain (vm.buildingData[0].BuildingQuestions).map (function (
                buildResponseSE
              ) {
                if (responseSE.sysRefId == buildResponseSE.systemRefId) {
                  if (
                    responseSE.visible == true &&
                    responseSE.visible != null &&
                    responseSE.visible != undefined
                  ) {
                    buildResponseSE.appearUI = 'Y';
                  }
                  if (responseSE.visible == false) {
                    buildResponseSE.appearUI = 'N';
                  }
                }
              });
            });
          },
          function () {}
        );
        _.chain (vm.buildingData[0].BuildingQuestions).map (function (
          checkdata
        ) {
          if (
            checkdata.systemRefId ==
            'RiskInput.PermanentlyInstalledEquipmentLimit'
          ) {
            checkdata.defaultValue =
              '$' +
              checkdata.defaultValue
                .toString ()
                .replace (/\B(?=(\d{3})+(?!\d))/g, ',');
            if (
              checkdata.defaultValue != null &&
              checkdata.defaultValue != undefined &&
              checkdata.defaultValue.includes ('$') != true
            ) {
              checkdata.userValue =
                '$' +
                checkdata.userValue
                  .toString ()
                  .replace (/\B(?=(\d{3})+(?!\d))/g, ',');
            }
          }
        });

        vm.buildingData = ParseJSON.buildingParse (
          vm.buildingData,
          vm.buildingDataUUID
        );

        if (checkFirstBuildingOrNot) {
          if (vm.veriskSuggestionBox) {
            vm.veriskSuggestionBox = true;

            // console.log("++++++++++", vm.buildingData);

            _.find (vm.buildingData[0].BuildingQuestions, function (loc) {
              if (
                loc.systemRefId == 'BuildingInput.ConstructionCode' &&
                vm.constructionCode != 'No data available'
              ) {
                switch (vm.constructionCode) {
                  case '1':
                    vm.constructionVeriskBox = 'Frame-1';
                    break;
                  case '2':
                    vm.constructionVeriskBox = 'Joisted Masonry-2';
                    break;
                  case '3':
                    vm.constructionVeriskBox = 'Non-Combustible-3';
                    break;
                  case '4':
                    vm.constructionVeriskBox = 'Masonry Non-Comb-4';
                    break;
                  case '5':
                    vm.constructionVeriskBox = 'Modified Fire Resistive-5 or 6';
                    break;
                  case '6':
                    vm.constructionVeriskBox = 'Fire Resistive-6';
                    break;
                  default:
                    vm.constructionVeriskBox = 'No data available';
                    break;
                } // End of Switch
              }

              if (
                loc.systemRefId == 'BuildingInput.UNIG_MultipleOccupancyBldg' &&
                vm.veriskOccupancies > 1
              ) {
                vm.buildingDataUUID[loc.uuid] = 'Yes';
              }
              if (
                loc.systemRefId == 'BuildingInput.UNIG_MultipleOccupancyBldg' &&
                vm.veriskOccupancies == 1
              ) {
                vm.buildingDataUUID[loc.uuid] = 'No';
              }
              if (
                loc.systemRefId == 'BuildingInput.Sprinkler' &&
                vm.veriskSprinkler == 'true'
              ) {
                vm.buildingDataUUID[loc.uuid] = true;
              }
              // console.log(vm.buildingData);
            });
          }
        } else {
          vm.veriskSuggestionBox = false;
        }

        vm.buildingData[0].locationId = locationId;
        vm.buildingData[0].locationName = locationName;
        // vm.addOcuData = vm.buildingData[0].occupantData;
        vm.buildingEventMode = 'add';
        vm.isEditingBuilding = false;
        vm.isAddingBuilding = true;
        vm.defaultLocPage = true;
        vm.viewMode = true;
        vm.showBuildingForm = false;
        vm.showLocationForm = false;
        vm.locID = locIndex + 1;

        if (_.isEmpty (BuildingData[0])) {
          vm.buildingID = 0;
        } else {
          vm.buildingID = buildIndex;
        }

        appianSystemRefId (vm.buildingData[0].BuildingQuestions);

        vm.formSendData.elementType = ['RISK'];
        vm.formSendData.operationType = ['SAVEBUILDING'];
        vm.formSendData.data = [vm.buildingData[0]];
        $rootScope.pageInfo = [
          {
            currentPage: $location.path (),
            currentPageData: vm.formSendData.data,
            element: vm.formSendData.elementType,
            operation: vm.formSendData.operationType,
          },
        ];
      } else {
        vm.addOcuData = [];
        vm.showOccupantForm = false;
        vm.showBuildingForm = false;

        vm.Emptysecbuilding = _.isEmpty (BuildingData[0]);
        if (buildIndex == 1 && vm.Emptysecbuilding == true) {
          var firstBuilding = true;
        } else {
          firstBuilding = false;
        }

        var data = {
          firstBuilding: [firstBuilding],
          locationIndex: [locIndex],
          dataType: ['MASTER'],
          elementType: ['RISK'],
          operationType: ['VIEW_BUILDING'],
          locationId: [locationId],
        };

        // call service
        locationService.fetchBuildingData (data).then (
          function (res) {
            var occData = {
              elementType: ['OCCUPANT'],
              operationType: ['OCCUPANT_BUSINESSCLASSLOOKUP'],
            };

             locationService.fetchBusinessClass (occData).then (function (res) {
             vm.businessClassValues = res.data[0].valueName;
             vm.businessClassValues1 = res.data[0].valueName;
             });

            var data = {
              elementType: ['APPIANRULE'],

              operationType: ['DISPLAY_PERMANENTLY_INSTALLED_EQUIPMENT'],

              data: [
                {
                  opCode: vm.businessClass,
                  state: vm.Lobstate,
                },
              ],
            };
            // console.log("business class
			// ------------------------------------------------------",data);
            locationService.validateStateAppianRule (data).then (
              function (appianRes) {
                vm.annualSaleCountErr = null;

                _.chain (appianRes.data).map (function (response) {
                  _.chain (
                    res.data.BuildingData[0].BuildingQuestions
                  ).map (function (buildResponse) {
                    if (response.sysRefId == buildResponse.systemRefId) {
                      if (
                        response.visible == true &&
                        response.visible != null &&
                        response.visible != undefined
                      ) {
                        buildResponse.appearUI = 'Y';
                      }
                      if (response.visible == false) {
                        buildResponse.appearUI = 'N';
                      }
                    }
                  });
                });
              },
              function () {}
            );

            vm.buildingData = res.data.BuildingData;
            vm.locationBuildingId = vm.buildingData[0].buildingId;
            _.chain (vm.buildingData[0].BuildingQuestions).map (function (
              checkdata
            ) {
              if (
                checkdata.systemRefId ==
                'RiskInput.PermanentlyInstalledEquipmentLimit'
              ) {
                checkdata.defaultValue =
                  '$' +
                  checkdata.defaultValue
                    .toString ()
                    .replace (/\B(?=(\d{3})+(?!\d))/g, ',');
                if (
                  checkdata.defaultValue != null &&
                  checkdata.defaultValue != undefined &&
                  checkdata.defaultValue.includes ('$') != true
                ) {
                  checkdata.userValue =
                    '$' +
                    checkdata.userValue
                      .toString ()
                      .replace (/\B(?=(\d{3})+(?!\d))/g, ',');
                }
              }
            });
            vm.buildingData = ParseJSON.buildingParse (
              vm.buildingData,
              vm.buildingDataUUID
            );
            if (checkFirstBuildingOrNot) {
              if (vm.veriskSuggestionBox) {
                vm.veriskSuggestionBox = true;

                // console.log("++++++++++", vm.buildingData);

                _.find (vm.buildingData[0].BuildingQuestions, function (loc) {
                  if (
                    loc.systemRefId == 'BuildingInput.ConstructionCode' &&
                    vm.constructionCode != 'No data available'
                  ) {
                    console.log ('vm.constructionCod', vm.constructionCode);
                    switch (vm.constructionCode) {
                      case '1':
                        vm.constructionVeriskBox = 'Frame-1';
                        break;
                      case '2':
                        vm.constructionVeriskBox = 'Joisted Masonry-2';
                        break;
                      case '3':
                        vm.constructionVeriskBox = 'Non-Combustible-3';
                        break;
                      case '4':
                        vm.constructionVeriskBox = 'Masonry Non-Comb-4';
                        break;
                      case '5':
                        vm.constructionVeriskBox =
                          'Modified Fire Resistive-5 or 6';
                        break;
                      case '6':
                        vm.constructionVeriskBox = 'Fire Resistive-6';
                        break;
                      default:
                        vm.constructionVeriskBox = 'No data available';
                        break;
                    } // End of Switch
                  }
                  if (
                    loc.systemRefId ==
                      'BuildingInput.UNIG_MultipleOccupancyBldg' &&
                    vm.veriskOccupancies > 1
                  ) {
                    vm.buildingDataUUID[loc.uuid] = 'Yes';
                  }
                  if (
                    loc.systemRefId ==
                      'BuildingInput.UNIG_MultipleOccupancyBldg' &&
                    vm.veriskOccupancies == 1
                  ) {
                    vm.buildingDataUUID[loc.uuid] = 'No';
                  }
                  if (
                    loc.systemRefId == 'BuildingInput.Sprinkler' &&
                    vm.veriskSprinkler == 'true'
                  ) {
                    vm.buildingDataUUID[loc.uuid] = true;
                  }
                  // console.log(vm.buildingData);
                });
              }
            } else {
              vm.veriskSuggestionBox = false;
            }

            vm.buildingData[0].locationId = locationId;
            vm.buildingData[0].locationName = locationName;
            // vm.addOcuData = vm.buildingData[0].occupantData;
            vm.buildingEventMode = 'add';
            vm.isEditingBuilding = false;
            vm.isAddingBuilding = true;
            vm.defaultLocPage = true;
            vm.viewMode = true;
            vm.showBuildingForm = true;
            vm.showLocationForm = true;
            vm.savedBuildingOccupants = false;
            // vm.showBuildingForm = false;
            vm.locID = locIndex + 1;

            if (_.isEmpty (BuildingData[0])) {
              vm.buildingID = 0;
            } else {
              vm.buildingID = buildIndex;
            }

            appianSystemRefId (vm.buildingData[0].BuildingQuestions);

            vm.formSendData.elementType = ['RISK'];
            vm.formSendData.operationType = ['SAVEBUILDING'];
            vm.formSendData.data = [vm.buildingData[0]];
            $rootScope.pageInfo = [
              {
                currentPage: $location.path (),
                currentPageData: vm.formSendData.data,
                element: vm.formSendData.elementType,
                operation: vm.formSendData.operationType,
              },
            ];
          },
          function () {
            // console.log('getBuildingData - error', err.detail);
            // ErrorToast.showError(err.detail);
          }
        );
      }
    }

    /**
	 * viewModeBtn
	 */
    function viewModeBtn () {
      // vm.defaultLocPage = true;
      vm.viewMode = false;
      vm.showBuildingForm = false;
      vm.showLocationForm = true;
    }

    $rootScope.$on ('exitLocationName', function (event, nArray) {
      var locationName = null;
      locationName = getIndexBySearch (nArray);
      $rootScope.locationNameSE = locationName;
      // $rootScope.$emit ('exitEmitLocationName', locationName);
    });

    /**
	 * getIndexBySearch
	 * 
	 * @param {*}
	 *            nArray
	 */
    function getIndexBySearch (nArray) {
      var sArray = nArray[0].LocationAddress;
      var input = [];
      // console.log("sArray",sArray);
      for (var i = 0; i < sArray.length; i++) {
        if (sArray[i].systemRefId == 'LocationInput.Address1') {
          input.push (sArray[i].userValue);
        }

        if (sArray[i].systemRefId == 'LocationInput.Address2') {
          if (
            sArray[i].userValue != null &&
            sArray[i].userValue != undefined &&
            sArray[i].userValue.length > 0
          ) {
            input.push (sArray[i].userValue);
          }
        }

        if (sArray[i].systemRefId == 'LocationInput.City') {
          input.push (sArray[i].userValue);
        }

        if (sArray[i].systemRefId == 'LocationInput.State') {
          input.push (sArray[i].userValue);
        }

        if (sArray[i].systemRefId == 'LocationInput.ZipCode') {
          input.push (sArray[i].userValue);
        }
        var input1 = input.join (', ');
      }
      return input1;
    }

    /**
	 * desc
	 * 
	 * @param {*}
	 *            locationArray
	 * @param {*}
	 *            defaultlocationForm
	 * @param {*}
	 *            locationID
	 */
    function saveExpandedLocation (
      locationId,
      locIndex,
      locationArray,
      defaultlocationForm
    ) {
      Validator.triggerValidation (defaultlocationForm);
      console.log ('locationArray', locationArray);
      _.chain (locationArray[0].LocationAddress).map (function (checkdata) {
        if (
          (checkdata.systemRefId ==
            'LocationBusinessOwnersInput.ProtectionClass' &&
            checkdata.userValue == '10W') ||
          (checkdata.systemRefId ==
            'LocationBusinessOwnersInput.ProtectionClass' &&
            checkdata.userValue == '10')
        ) {
          console.log ('checkdata.userValue', checkdata.userValue);
          vm.protErrMsg = 'Protection Class 10 or 10W is not eligible.';
          console.log ('vm.protErrMsg', vm.protErrMsg);
        }
      });

      if (vm.lengthOfZip || vm.stateErrMsg != null || vm.protErrMsg != null) {
        vm.showState = true;
        console.log ('vm.protErrMsg inside', vm.protErrMsg);
        return;
      } else {
        vm.showState = false;
      }

      if (defaultlocationForm.$valid) {
        locationArray[0].locationName[0] = getIndexBySearch (locationArray);

        vm.formSendData.locationIndex = [locIndex];

        console.log ('vm.protErrMsg ', vm.protErrMsg);
        // call service
        locationService.saveLocations (vm.formSendData).then (
          function (res) {
            vm.protErrMsg = null;
            vm.formSendData = {
              elementType: null,
              operationType: null,
              data: null,
            };

            vm.locData = res.data.LocationsData;

            _.chain (vm.locData).map (function (checkdata) {
              if (checkdata.locationId[0] == locationId) {
                vm.locIndex = vm.locData.indexOf (checkdata);
                vm.checkLocationData = checkdata;
                vm.checkLocationDataBuild =
                  vm.checkLocationData.BuildingData.length;
              }
            });

            addNewBuilding (
              vm.locIndex,
              vm.checkLocationDataBuild,
              vm.checkLocationData.locationId[0],
              vm.checkLocationData.locationName,
              vm.checkLocationData.BuildingData,
              true
            );
          },
          function () {
            // ErrorToast.showError(err.detail);
          }
        );
      }
    }

    /**
	 * desc
	 * 
	 * @param {*}
	 *            locationArray
	 * @param {*}
	 *            defaultlocationForm
	 * @param {*}
	 *            locationID
	 */
    function saveLocation (
      locationId,
      locationArray,
      defaultlocationForm,
      eventMode,
      locID
    ) {
      Validator.triggerValidation (defaultlocationForm);
      _.chain (locationArray[0].LocationAddress).map (function (checkdata) {
        if (
          (checkdata.systemRefId ==
            'LocationBusinessOwnersInput.ProtectionClass' &&
            checkdata.userValue == '10W') ||
          (checkdata.systemRefId ==
            'LocationBusinessOwnersInput.ProtectionClass' &&
            checkdata.userValue == '10')
        ) {
          vm.protErrMsg = 'Protection Class 10 or 10W is not eligible.';
          console.log ('vm.protErrMsg', vm.protErrMsg);
        }
      });
      if (vm.lengthOfZip || vm.stateErrMsg != null || vm.protErrMsg != null) {
        vm.showState = true;
        return;
      } else {
        vm.showState = false;
      }

      if (defaultlocationForm.$valid) {
        if (eventMode == 'add') {
          locationArray[0].locationName[0] = getIndexBySearch (locationArray);
          vm.formSendData.locationIndex = [locID - 1];
          // data = {
          // elementType: ['RISK'],
          // operationType: ['SAVELOCATION'],
          // data: [locationArray[0]],
          // locationIndex:[locID - 1]
          // };

          // call service
          locationService.saveLocations (vm.formSendData).then (
            function (res) {
              vm.protErrMsg = null;
              vm.explocData = [];
              vm.formSendData = {
                elementType: null,
                operationType: null,
                data: null,
              };
              vm.locData = res.data.LocationsData;
              _.chain (vm.locData).map (function (checkdata) {
                if (checkdata.locationId[0] == locationId) {
                  vm.locIndex1 = vm.locData.indexOf (checkdata);
                  vm.checkLocationData1 = checkdata;
                }
              });

              addNewBuilding (
                vm.locIndex1,
                vm.checkLocationData1.BuildingData.length,
                vm.checkLocationData1.locationId[0],
                vm.checkLocationData1.locationName,
                vm.checkLocationData1.BuildingData,
                false
              );
            },
            function () {
              // ErrorToast.showError(err.detail);
            }
          );
        }

        if (eventMode == 'edit') {
          locationArray[0].locationName[0] = getIndexBySearch (
            locationArray,
            vm.locData
          );
          if (locID == 1) {
            editModalPopup (locationArray, locID, vm.formSendData);
          } else {
            vm.formSendData.locationIndex = [locID - 1];
            vm.formSendData.data = [locationArray[0]];
            // call service
            locationService.saveLocations (vm.formSendData).then (
              function () {
                vm.explocData = [];
                vm.formSendData = {
                  elementType: null,
                  operationType: null,
                  data: null,
                };
                getLocationData ();
              },
              function () {
                // ErrorToast.showError(err.detail);
                // console.log('save location - error', err.detail);
              }
            );
          }
        }
      }
    }

    /**
	 * desc
	 * 
	 * @param {*}
	 *            theForm
	 */
    // ,ocuData,occupantDetailsForm needed to add in function//
    function saveBuildingUnderwriting (
      theForm,
      buildingData,
      buildingEventMode
    ) {
      var buildingUnderDataRes = {};

      var data;
      Validator.triggerValidation (theForm);

      if (vm.errorMessageAnnualSale == true) {
        vm.showAnnualSaleErrorMesg = 1;
      } else {
        vm.showAnnualSaleErrorMesg = 0;
      }

      if (
        vm.yearerrmsg != null ||
        vm.numberofstorieserrmsg != null ||
        vm.annualsaleserrmsg != null ||
        vm.OccSquareFootageMsg != null ||
        vm.squareFootageMsg != null ||
        vm.showAnnualSaleErrorMesg == 1 ||
        vm.buildingLimitMsg != null ||
        vm.buildingPLimitMsg != null ||
        vm.businessClassError == 1
      ) {
        vm.showyearUnder = true;
        return;
      } else {
        vm.showyearUnder = false;
      }
      if (theForm.$valid) {
        buildingUnderDataRes = angular.copy (
          ParseJSON.returnFields (buildingData[0], vm.buildingUnderDataUUID)
        );
        data = {
          elementType: ['UNDERWRITINGQUESTIONS'],
          operationType: ['SAVE_BUILD_UNDERWRITING_QUESTIONS'],
          data: [buildingUnderDataRes],
        };
        // call service
        locationService.saveBuildings (data).then (
          function () {
            vm.disableSubmit = false;
            vm.buildunderqa = false;
            vm.cancelHide = true;

            getLocationData ();
          },
          function (err) {
            ErrorToast.showError (err.detail);
            // console.log('save location - error', err.detail);
          }
        );
      }
    }
    $rootScope.$on ('exitbuildingres', function (event, buildingData) {
      var exitBuildingDataRes = {};
      // console.log("buildingData exit",buildingData);
      exitBuildingDataRes = angular.copy (
        ParseJSON.returnFields (buildingData[0], vm.buildingDataUUID)
      );
      $rootScope.exitBuildingDataResSE = exitBuildingDataRes;
    });
    /**
	 * desc
	 * 
	 * @param {*}
	 *            theForm
	 */
    // ,ocuData,occupantDetailsForm needed to add in function//

    function saveBuilding (theForm, buildingData, buildingEventMode) {
      var buildingDataRes = {};

      Validator.triggerValidation (theForm);

      vm.submitSaveBuilding = true;

      if (
        vm.yearerrmsg != null ||
        vm.numberofstorieserrmsg != null ||
        vm.OccSquareFootageMsg != null ||
        vm.squareFootageMsg != null ||
        vm.buildingLimitMsg != null ||
        vm.buildingPLimitMsg != null
      ) {
        vm.showyear = true;
        return;
      } else {
        vm.showyear = false;
      }

      for (var i = 0; i < vm.addOcuData.length; i++) {
        // for (var j=0; j<vm.addOcuData[i].occupantQuestions.length; j++){

        if (vm.addOcuData[i].occupantQuestions[0].disableStatus) {
          vm.addOcuData[i].occupantQuestions[0].occSaved = true;
        } else {
          vm.addOcuData[i].occupantQuestions[0].occSaved = false;
          vm.submitSaveBuilding = false;
        }
        // }
      }

      if (theForm.$valid && vm.submitSaveBuilding && !vm.showOccupantError) {
        if (buildingEventMode == 'add') {
          if (vm.addsavebuild == false) {
            for (var j = 0; j < buildingData[0].BuildingQuestions.length; j++) {
              if (
                buildingData[0].BuildingQuestions[j].systemRefId ==
                  'Building_code_effectiveness_grading' ||
                buildingData[0].BuildingQuestions[j].systemRefId ==
                  'Building_code_effectiveness_grading_earthquake'
              ) {
                Object.keys (vm.buildingDataUUID).forEach (function (key) {
                  if (key == buildingData[0].BuildingQuestions[j].uuid) {
                    vm.buildingDataUUID[key] =
                      buildingData[0].BuildingQuestions[j].defaultValue;
                  }
                });
              }
            }

            buildingDataRes = angular.copy (
              ParseJSON.returnFields (buildingData[0], vm.buildingDataUUID)
            );
            buildingDataRes.addBuilding = true;

            console.log ('Occupant....' + JSON.stringify (vm.addOcuData));

            for (var i = 0; i < vm.addOcuData.length; i++) {
              buildingDataRes.occupantData.push (vm.addOcuData[i]);
            }

            vm.formSendData.data = [buildingDataRes];

            locationService.saveBuildings (vm.formSendData).then (
              function (res) {
                vm.savedBuildingOccupants = true;

                // Enabling Occupant Section

                if (vm.OccAppianValidation && vm.savedBuildingOccupants) {
                  vm.showOccupantForm = true;
                  vm.showBuildingForm = true;
                  vm.addAnotherOccupant = true;
                }

                // vm.showOccupantForm = true;
                if (
                  res.data.addBuilding &&
                  res.data.buildingId == buildingData[0].buildingId
                ) {
                  vm.formSendData = {
                    elementType: null,
                    operationType: null,
                    data: null,
                  };

                  vm.addsavebuild = true;
                  vm.buildingEventMode = 'saveEditFirst';
                } else {
                  vm.addsavebuild = false;
                }

                addBuildingUnderwriting (
                  buildingData[0].locationId,
                  buildingData[0].buildingId,
                  'add'
                );
              },
              function () {}
            );
          }
          // if (vm.addsavebuild == true) {
          // console.log (' 2 vm.addsavebuild', vm.addsavebuild);
          // console.log ('2 vm.formSendData', vm.formSendData);
          // buildingDataRes = angular.copy (
          // ParseJSON.returnFields (buildingData[0], vm.buildingDataUUID)
          // );
          // vm.formSendData.operationType = ['EDITBUILDING'];
          // vm.formSendData.subjectType = ['EDIT'];
          // vm.formSendData.data = [buildingDataRes];

          // // call service

          // // console.log("building save edit",vm.formSendData);
          // locationService.saveBuildings (vm.formSendData).then (
          // function () {
          // vm.formSendData = {
          // elementType: null,
          // operationType: null,
          // data: null,
          // };
          // vm.buildunderqa = true;
          // addBuildingUnderwriting (buildingData[0].buildingId, 'add');
          // },
          // function () {}
          // );
          // }
        }

        if (
          buildingEventMode == 'edit' ||
          buildingEventMode == 'saveEditFirst'
        ) {
          buildingDataRes = angular.copy (
            ParseJSON.returnFields (buildingData[0], vm.buildingDataUUID)
          );

          if (buildingDataRes) {
            // for (var j = 0; j < vm.buildingDataRes.occupantData.length; j++)
			// {
            // if(j > 0){
            // buildingDataRes.occupantData[j].splice (j, 1);
            var primaryOccData = buildingDataRes.occupantData[0];
            buildingDataRes.occupantData = [];
            buildingDataRes.occupantData.push (primaryOccData);
            // }
            // }
          }

          // console.log("Buil...." + JSON.stringify(buildingDataRes));

          for (var i = 0; i < vm.addOcuData.length; i++) {
            buildingDataRes.occupantData.push (vm.addOcuData[i]);
          }

          // console.log("Occupantsss...." + JSON.stringify(buildingDataRes));

          // for (var i = 0; i < vm.addOcuData.length; i++) {
          // vm.occDetailsExist = false;
          // for (var j = 0; j < buildingDataRes.occupantData.length; j++) {
          // if(buildingDataRes.occupantData[j].occupantId ==
			// vm.addOcuData[i].occupantId){
          // vm.occDetailsExist = true;
          // }

          // }
          // if (!vm.occDetailsExist) {
          // buildingDataRes.occupantData.push (vm.addOcuData[i]);
          // }
          // }

          vm.formSendData.operationType = ['SAVEBUILDING'];
          vm.formSendData.elementType = ['RISK'];
          vm.formSendData.data = [buildingDataRes];
          vm.formSendData.data[0].subjectType = ['EDIT'];
          // call service

          locationService.saveBuildings (vm.formSendData).then (
            function () {
              vm.formSendData = {
                elementType: null,
                operationType: null,
                data: null,
              };

              vm.buildunderqa = true;
              addBuildingUnderwriting (
                buildingData[0].locationId,
                buildingData[0].buildingId,
                'saveEditFirst'
              );
            },
            function () {
              // ErrorToast.showError(err.detail);
              // console.log('save location - error', err.detail);
            }
          );
        }
      }
    }

    /**
	 * buildingCancel
	 */
    function buildingCancel (buildingID, locID, building, buildingEventMode) {
      var buildCanObj = {};
      var buildCancelIndex = null;
      if (vm.locData[locID].BuildingData.length == 1) {
        var cancelFirstBuilding = _.isEmpty (vm.locData[locID].BuildingData[0]);
      }

      if (
        cancelFirstBuilding != true &&
        vm.locData[locID].BuildingData.length > 0
      ) {
        _.chain (vm.locData).map (function (resdata) {
          buildCancelIndex += resdata.BuildingData.length;
        });

        buildCanObj.locationId = building.locationId;
        buildCanObj.buildingId = building.buildingId;
        buildCanObj.buildingIndex = buildCancelIndex;

        var data = {
          elementType: ['RISK'],

          operationType: ['CANCEL_NEW_BUILDING'],

          data: [buildCanObj],
        };
        // call service

        locationService.deletebuilding (data).then (
          function () {
            vm.formSendData = {
              elementType: null,
              operationType: null,
              data: null,
            };
            getLocationData ();
            viewModeBtn ();
          },
          function () {
            // ErrorToast.showError(err.detail);
          }
        );
      } else {
        getLocationData ();
        viewModeBtn ();
      }
    }

    /**
	 * buildingUnderCancel
	 */
    function buildingUnderCancel (
      buildingID,
      locID,
      building,
      buildingEventMode
    ) {
      var buildUnderCanObj = {};
      var buildUnderCancelIndex = null;

      vm.buildUnderCancel = false;
      _.chain (vm.locData).map (function (resdata) {
        buildUnderCancelIndex += resdata.BuildingData.length;
      });

      buildUnderCanObj.locationId = building.locationId;
      buildUnderCanObj.buildingId = building.buildingId;

      var data = {
        elementType: ['RISK'],

        operationType: ['DELETEBUILDING'],

        data: [buildUnderCanObj],
      };
      // call service

      locationService.deletebuilding (data).then (
        function () {
          vm.formSendData = {
            elementType: null,
            operationType: null,
            data: null,
          };
          vm.buildunderqa = false;
          getLocationData ();
          viewModeBtn ();
        },
        function () {
          // ErrorToast.showError(err.detail);
        }
      );
      // }
    }

    /**
	 * Location disable
	 */
    function locationDisable (locData) {
      for (var i = 0; i < locData.length; i++) {
        if (
          locData[i].BuildingData.length == 0 ||
          locData[i].BuildingData.length == null ||
          locData[i].BuildingData.length == undefined
        ) {
          vm.locDisable = true;
        } else {
          vm.locDisable = false;
        }
      }
    }

    /**
	 * Save and continue
	 */
    function saveLoc () {
      var annualData = {
        elementType: ['UNDERWRITINGQUESTIONS'],
        operationType: ['VIEW_BUILD_UNDERWRITING_ANNUAL'],
      };
      // call service
      locationService.fetchBuildingData (annualData).then (
        function (res) {
          var annualSaleBuild = res.data.annualSales;
          var data = {
            elementType: ['APPIANRULE'],
            operationType: ['BR_ANNUAL_SALES_TOTAL'],
            data: [annualSaleBuild],
          };
          locationService.validateBuildingAppianRule (data).then (
            function (appianRes) {
              if (
                appianRes.data[0].sysRefId ==
                'BuildingInput.UNIG_BuildingAnnualSales'
              ) {
                if (appianRes.data[0].error != null) {
                  vm.annualSaleCountErr = appianRes.data[0].error;
                  return;
                } else {
                  vm.annualSaleCountErr = appianRes.data[0].error;
                  for (var y = 0; y < vm.locData.length; y++) {
                    if (
                      vm.locData[y].BuildingData.length == 0 ||
                      vm.locData[y].BuildingData.length == null ||
                      vm.locData[y].BuildingData.length == undefined ||
                      vm.stateSummErrMsg != null ||
                      vm.annualSaleCountErr != null
                    ) {
                      vm.saveDisable = true;
                      return;
                    } else {
                      vm.saveDisable = false;
                    }
                  }

                  if (vm.saveDisable == false && vm.Emptybuilding == false) {
                    var sdata = {
                      elementType: ['RISK'],
                      operationType: ['SUMMARY_PAGE_SAVE_IN_APPIAN'],
                      data: [vm.fullLocationData],
                    };

                    locationService.fetchLocationData (sdata).then (
                      function (Res) {
                        util.setPageIndex ('app.additionalinterest');

                        $state.go ('app.additionalinterest');
                      },
                      function () {}
                    );
                  }
                }
              }
            },
            function () {}
          );
        },
        function () {
          // console.log('getBuildingData - error', err.detail);
          // ErrorToast.showError(err.detail);
        }
      );
    } // End of saveLoc() Function

    // underwriting appian rules

    /**
	 * desc for addAditionalUnder
	 * 
	 * @param {*}
	 *            sysId
	 * @param {*}
	 *            buildingdata
	 * @param {*}
	 *            input
	 */
    function addAditionalUnder (
      sysID,
      bData,
      input,
      eventName,
      theForms,
      buildingDatas,
      buildingEventModes
    ) {
      var data;
      var appianObjUnder = {};
      appianObjUnder[sysID] = input;
      if (
        sysID != 'Electrical' ||
        sysID != 'Plumbing' ||
        sysID != 'Roof' ||
        sysID != 'Furnace'
      ) {
        appianObjUnder[sysID] = input;
        vm.RuleName = eventName[0].split ('RULE:')[1];

        data = {
          elementType: ['UNDERWRITINGQUESTIONS'],
          operationType: [vm.RuleName],
          data: [appianObjUnder],
        };

        locationService.validateBuildingAppianRule (data).then (
          function (res) {
            console.log ('addAditional add cgecking', res.data);
            _.chain (bData).map (function (bDataa) {
              _.chain (res.data).map (function (response) {
                if (bDataa.systemRefId == response.sysRefId) {
                  if (response.value != null) {
                    Object.keys (vm.buildingUnderDataUUID).forEach (function (
                      key
                    ) {
                      if (key == bDataa.uuid) {
                        vm.buildingUnderDataUUID[key] = response.value[0];
                        console.log (
                          'vm.buildingUnderDataUUID addAditional checking',
                          vm.buildingUnderDataUUID
                        );
                      }
                    });
                  }

                  if (
                    response.visible == true &&
                    response.visible != null &&
                    response.visible != undefined
                  ) {
                    bDataa.appearUI = 'Y';
                  }
                  if (response.visible == false) {
                    bDataa.appearUI = 'N';
                  }
                }
                if (
                  bDataa.systemRefId == 'BuildingInput.UNIG_BuildingAnnualSales'
                ) {
                  if (response.error != null) {
                    vm.annualsaleserrmsg = response.error;
                  } else {
                    vm.annualsaleserrmsg = response.error;
                  }
                }
              });
            });

            vm.addAditionalCall = false;

            if (vm.appianBefSaveUnder == true) {
              vm.appianBefSaveUnder = false;
              saveBuildingUnderwriting (
                theForms,
                buildingDatas,
                buildingEventModes
              );
            }
          },
          function () {
            // ErrorToast.showError(err.detail);
          }
        );
      }
    }

    /**
	 * desc for appianSystemRefIdUnder
	 * 
	 * @param {*}
	 *            buildingQuestion
	 */

    function appianSystemRefIdUnder (buildingQuestion) {
      /* for yearbuilt appian onjects */

      _.chain (buildingQuestion).map (function (bDataa) {
        if (
          bDataa.systemRefId == 'Electrical' ||
          bDataa.systemRefId == 'Plumbing' ||
          bDataa.systemRefId == 'Roof' ||
          bDataa.systemRefId == 'Furnace'
        ) {
          var c = bDataa.systemRefId;
          if (
            bDataa.defaultValue != null &&
            bDataa.defaultValue != undefined &&
            bDataa.userValue == null
          ) {
            var d = bDataa.defaultValue;
          } else {
            var d = bDataa.userValue;
          }

          vm.yearBuiltAppianobjUnder[c] = d;
          // console.log('vm.yearBuiltAppianobj', vm.yearBuiltAppianobj);
        }
      });
    }

    /**
	 * desc for yearBuiltAddField
	 * 
	 * @param {*}
	 */

    function yearBuiltAddFieldUnder (input, bData, sysID, eventName) {
      if (
        sysID == 'Electrical' ||
        sysID == 'Plumbing' ||
        sysID == 'Roof' ||
        sysID == 'Furnace'
      ) {
        Object.keys (vm.yearBuiltAppianobjUnder).forEach (function (key) {
          if (key == sysID) {
            var userinput = input;

            vm.yearBuiltAppianobjUnder[key] = userinput;
          }
        });

        var appianRule = eventName[0].split ('RULE:')[1];
        var data = {
          elementType: ['UNDERWRITINGQUESTIONS'],

          operationType: [appianRule],

          data: [vm.yearBuiltAppianobjUnder],
        };

        locationService.validateBuildingAppianRule (data).then (
          function (res) {
            _.chain (bData).map (function (bDataa) {
              _.chain (res.data).map (function (response) {
                if (bDataa.systemRefId == response.sysRefId) {
                  if (response.value != null) {
                    Object.keys (vm.buildingUnderDataUUID).forEach (function (
                      key
                    ) {
                      if (key == bDataa.uuid) {
                        vm.buildingUnderDataUUID[key] = response.value[0];
                      }
                    });
                  }

                  if (response.visible == true) {
                    bDataa.appearUI = 'Y';
                  }
                  if (response.visible == false) {
                    bDataa.appearUI = 'N';
                  }
                }
              });
            });
          },
          function () {
            // ErrorToast.showError(err.detail);
          }
        );
      }
    }

    /**
	 * desc for yearUnder
	 * 
	 * @param {*}
	 *            sysId
	 * @param {*}
	 *            buildingdata
	 * @param {*}
	 *            input
	 */
    function yearUnder (sysID, bData, input, eventName) {
      var data;

      var appianObjUnder = {};
      appianObjUnder[sysID] = input;
      vm.RuleName = eventName[0].split ('RULE:')[1];

      if (
        sysID != 'Electrical' ||
        sysID != 'Plumbing' ||
        sysID != 'Roof' ||
        sysID != 'Furnace'
      ) {
        data = {
          elementType: ['UNDERWRITINGQUESTIONS'],
          operationType: [vm.RuleName],
          data: [appianObjUnder],
        };

        locationService.validateBuildingAppianRule (data).then (
          function (res) {
            // console.log ('res', res);
            // console.log (
            // 'start vm.buildingUnderDataUUID',
            // vm.buildingUnderDataUUID
            // );

            if (
              res.data[0].dataset != null &&
              res.data[0].dataset != undefined
            ) {
              vm.buildingUnderData[0].BuildingQuestions = vm.buildingUnderData[0].BuildingQuestions.concat (
                res.data[0].dataset
              );
              vm.buildingUnderData[0] = angular.copy (
                ParseJSON.returnFields (
                  vm.buildingUnderData[0],
                  vm.buildingUnderDataUUID
                )
              );
              vm.buildingUnderData = ParseJSON.buildingParse (
                vm.buildingUnderData,
                vm.buildingUnderDataUUID
              );
              appianSystemRefIdUnder (
                vm.buildingUnderData[0].BuildingQuestions
              );
            }

            if (res.data[0].dataset == null) {
              vm.buildingUnderData[0].BuildingQuestions = _.reject (
                vm.buildingUnderData[0].BuildingQuestions,
                function (buildingQuestion) {
                  return (
                    buildingQuestion.systemRefId ==
                      'Which_of_the_below_has_not_bee' ||
                    buildingQuestion.systemRefId == 'Electrical' ||
                    buildingQuestion.systemRefId == 'Plumbing' ||
                    buildingQuestion.systemRefId == 'Roof' ||
                    buildingQuestion.systemRefId == 'Furnace'
                  );
                }
              );
            }

            _.chain (res.data[0].rule).map (function (response) {
              _.chain (bData).map (function (bDataa) {
                if (bDataa.systemRefId == response.sysRefId) {
                  // console.log (
                  // 'bDataa.systemRefId == response.sysRefId',
                  // bDataa.systemRefId,
                  // 'jb',
                  // response.sysRefId
                  // );
                  if (response.value != null) {
                    // console.log ('year under response.value',
					// response.value);
                    Object.keys (vm.buildingUnderDataUUID).forEach (function (
                      key
                    ) {
                      if (key == bDataa.uuid) {
                        vm.buildingUnderDataUUID[key] = response.value[0];
                      }
                    });
                  }
                  // console.log ('middle year build bData', bData);
                  // console.log (
                  // 'middle vm.buildingUnderDataUUID',
                  // vm.buildingUnderDataUUID
                  // );
                  if (
                    response.visible == true &&
                    response.visible != null &&
                    response.visible != undefined
                  ) {
                    bDataa.appearUI = 'Y';
                  }
                  if (response.visible == false) {
                    bDataa.appearUI = 'N';
                  }
                }
              });
            });

            // console.log ('end year build bData', bData);
            // console.log (
            // 'end vm.buildingUnderDataUUID',
            // vm.buildingUnderDataUUID
            // );
          },
          function () {
            // ErrorToast.showError(err.detail);
          }
        );
      }
    }
  }
}) ();

(function () {
  'use strict';

  angular
    //
    .module('quoteUi')
    .controller('LobController', LobController);

  LobController.$inject = [
    'lobService',
    '$state',
    'Validator',
    '$location',
    '$rootScope',
    'ErrorToast',
    'util'
  ];

  function LobController(
    lobService,
    $state,
    Validator,
    $location,
    $rootScope,
    ErrorToast,
    util
  ) {
    var vm = this;
    // $rootScope.pageInfo = $location.path();

    // properties

    // methods
    vm.submitForm = submitForm;
    vm.loadFormMeta = loadFormMeta;
    vm.validateLobRules = validateLobRules;

    $rootScope.pageInfo = [{
      currentPage: $location.path(),
      currentPageData: [],
      element: [],
      operation: []
    }];

    vm.loadFormMeta();

    /**
	 * Load form data
	 */
    function loadFormMeta() {
      var data = {
        elementType: ['LOB'],
        operationType: ['VIEWLOBDETAILS'],
        action: ['GET']
      };

      lobService.getMeta(data).then(
        function (res) {
          vm.lobs = res.data;
          vm.lobs = _.sortBy(vm.lobs, 'displaySequence');
          if (vm.lobs.length == 0) {
            vm.disableSubmit = true;
            vm.message =
              'Based on the information provided, this risk is not eligible for coverage with Utica National. Please contact your underwriter if you have any additional questions.';
          }
          // vm.lobRefIds = _.pluck(vm.lobs, 'systemRefId');
          // vm.validateLobRules();
        },
        function (err) {
          ErrorToast.showError(err.detail, 'Fetch LOB');
        }
      );
    }

    /**
	 * Submit LOB form
	 * 
	 * @param {*}
	 *            valid
	 * @param {*}
	 *            items
	 */
    function submitForm(valid, items) {
      Validator.triggerValidation(items);
      if (!valid) {
        return false;
      }

      var appianData = {
        elementType: ['APPIANRULE'],
        operationType: ['VALIDATELOB'],
        data: vm.lobs
      };

      var data = {
        elementType: ['LOB'],
        operationType: ['SAVELOBDETAILS'],
        data: vm.lobs
      };

      lobService.validateLob(appianData).then(
        function (res) {
          if (typeof res.data[1] !== 'undefined' && res.data[1].enable) {
            lobService.saveLob(data).then(
              function () {
                util.setPageIndex('app.qualification');
                $state.go('app.qualification');
              },
              function () {
                //
              }
            );
          } else {
            if (
              typeof res.data[0] !== 'undefined' &&
              res.data[0].error !== null
            ) {
              vm.message = res.data[0].error;
            }
          }
        },
        function () {
          //
        }
      );
    }

    /**
	 * Validate with appian
	 */
    function validateLobRules() {
      var data = {
        elementType: ['APPIANRULE'],
        operationType: ['VALIDATELOB'],
        data: vm.lobs
      };
      lobService.validateLob(data).then(
        function (res) {
          if (typeof res.data[1] !== 'undefined' && !res.data[1].enable) {
            vm.disableSubmit = false;
            vm.message = res.data[0].error;
          } else {
            if (typeof res.data[1] !== 'undefined' && res.data[1].enable) {
              vm.disableSubmit = false;
            }
            vm.message = null;
          }
        },
        function () {
          //
        }
      );
    }
  }
})();

(function () {
  'use strict';

  angular
    .module('quoteUi')
    .controller('additionalInterestController', additionalInterestController);

  /** @ngInject */

  function additionalInterestController(
    $scope,
    $rootScope,
    $uibModal,
    $window,
    ParseJSON,
    Validator,
    $log,
    $http,
    $state,
    additionalinterestService,
    $location,
    util
  ) {
    var vm = this;

    vm.getFormData = getFormData;
    vm.getEditableData = getEditableData;
    vm.selectedInterest = [];
    vm.additionalinterest;
    vm.additionalinterestMaster;
    vm.newLocation;
    vm.addLocations = [];
    vm.displaySeq = displaySeq;
    vm.interestselection = false;
    vm.defaultValue = true;
    vm.addInterestSection = true;
    vm.editableAI = [];
    vm.AISelection;
    vm.lengthValidation = lengthValidation;
    vm.addinterestSubmit = addinterestSubmit;
    vm.deleteAI = deleteAI;
    vm.uncheckedAI = false;
    vm.submitValue;
    vm.selectedLocation = selectedLocation;
    vm.zipcodelookup = zipcodelookup;
    vm.selectedBuilding;
    vm.totalBuilding = 0;
    vm.addLocationInfo = false;

    $rootScope.pageInfo = [{
      "currentPage": $location.path(),
      "currentPageData": vm.selectedInterest,
      "element": ['ADDITIONALINTERESTS'],
      "operation": ['SAVE_ADDITIONAL_INTERESTS_IN_APPIAN']
    }];

    // vm.getFormData();
    vm.getEditableData();

    function selectedLocation(values, index, parentIndex, id, userName) {

      // get building

      if (userName === 'LOCATION') {

        var data = {
          elementType: ['ADDITIONALINTERESTS'],
          operationType: ['GET_BUILDING_BY_LOCATION'],
          locationName: [values]
        };

        additionalinterestService.fetchBuilding(data).then(
          function (response) {
            vm.selectedBuilding = angular.copy(response.data);
            vm.matchingValue = 0;
            vm.interestCount = 0;
            vm.parentCount = 0;

            for (var i = 0; i < vm.selectedInterest.length; i++) {
              for (var j = 0; j < vm.selectedInterest[i].length; j++) {

                if (vm.selectedInterest[i][j].uuid === id) {
                  vm.matchingValue = 1;
                  vm.interestCount = i;
                  vm.parentCount = j + 1;
                }
              }
            }

            if (vm.matchingValue == 1 && userName === 'LOCATION') {
              vm.selectedInterest[vm.interestCount][vm.parentCount].valueName = [];

              for (var k = 0; k < vm.selectedBuilding.length; k++) {
                vm.selectedInterest[vm.interestCount][vm.parentCount].valueName.push(vm.selectedBuilding[k].buildingName);

              }
            }

          },
          function () {
            // console.log('selectedBuilding - error', err.detail);
          }
        );

      } else if (userName === 'BUILDING') {
        // console.log("Building Name.." + values);
        vm.addLocationInfo = true;

        for (var k = 0; k < vm.selectedBuilding.length; k++) {
          if (values === vm.selectedBuilding[k].buildingName) {
            vm.selectedInterest[parentIndex][index].uuid = vm.selectedBuilding[k].buildinguuid;
            // vm.selectedInterest[parentIndex][index].locationExists =
			// vm.selectedInterest[parentIndex][index-1].userValue;
          }
        }

        // for(var k = 0; k < vm.selectedInterest[parentIndex].length; k++) {

        vm.selectedInterest[parentIndex][index].locationExists = vm.selectedInterest[parentIndex][index - 1].userValue + '' + values;

        // }




      }
      // console.log("vm.selectedInterest..." +
		// JSON.stringify(vm.selectedInterest));

    }
    // vm.totalBuilding = 0;

    vm.addLocation = function (values, index) {

      vm.locationNewValue = [];
      vm.buildingAIValue;
      vm.locationAIValue;
      vm.addedLocation = false;


      for (var i = 0; i < values.length; i++) {

        if (values[i].datasetName === 'LOCATION') {
          vm.addedLocation = true;
          vm.locationAIValue = angular.copy(values[i]);
          vm.locationAIValue.userValue = '';
          vm.locationAIValue.displaySequence = vm.locationAIValue.displaySequence + 1;
          vm.locationAIValue.duplicate = false;
        }

        if (values[i].datasetName === 'BUILDING') {
          vm.addLocationInfo = true;
          values[i].AICount = values[i].AICount + 1;
          // vm.aaa = angular.copy(values[i].AICount);
          vm.addedLocation = true;
          vm.buildingAIValue = angular.copy(values[i]);
          vm.buildingAIValue.displaySequence = vm.buildingAIValue.displaySequence + 1;
          vm.buildingAIValue.userValue = '';
          vm.buildingAIValue.valueName = [];
          vm.buildingAIValue.duplicate = false;

        }
      }

      if (vm.addedLocation) {
        vm.selectedInterest[index].push(vm.locationAIValue);
        vm.selectedInterest[index].push(vm.buildingAIValue);
        // vm.buildingAIValue.AICount = vm.aaa + 1;
        // vm.totalBuilding = vm.totalBuilding + 1;

      }
      // console.log("aaa..." + JSON.stringify(vm.selectedInterest));

    };

    vm.cancelAddedLocation = function (index, parentIndex) {


      vm.cancelPosition = index - 1;
      vm.selectedInterest[parentIndex].splice(vm.cancelPosition, 2);

      for (var i = 0; i < vm.selectedInterest[parentIndex].length; i++) {

        if (vm.selectedInterest[parentIndex][i].datasetName === 'BUILDING') {
          vm.selectedInterest[parentIndex][i].AICount = vm.selectedInterest[parentIndex][i].AICount - 1;
        }

      }


      // vm.selectedInterest = checkDuplicateInObject(vm.selectedInterest)
    };

    vm.addInterest = function (value, detail) {

      vm.AISelection = detail;
      vm.selectedValue = 0;
      vm.interestCount = 0;
      vm.matchingValue = 0;


      for (var k = 0; k < vm.selectedInterest.length; k++) {
        for (var l = 0; l < vm.selectedInterest[k].length; l++) {
          if (vm.selectedInterest[k][l].correspondingEntTyp == value) {
            vm.selectedValue = 1;
            vm.interestCount = k;
          }
        }
      }

      var adduuid = {};
      vm.addedAI = ParseJSON.parse(
        vm.additionalinterestWidget[vm.AISelection],
        adduuid
      );


      // vm.buildingAIValue.AICount = vm.totalBuilding;


      // console.log("addedAI..." + JSON.stringify(vm.addedAI));
      for (var m = 0; m < vm.addedAI.length; m++) {
        vm.addedAI[m].AICount = vm.totalBuilding;
        vm.ValLength = null;
        vm.ValLengthAlt = null;

        if (vm.addedAI[m].validationName[1]) {
          vm.ValLength = vm.addedAI[m].validationName[1].replace(/[^\d]/g, ''); // keep
																				// numbers
																				// only

        } else {
          vm.ValLengthAlt = vm.addedAI[m].validationName[0].replace(/[^\d]/g, ''); // keep
																					// numbers
																					// only
        }

        if (vm.ValLength) {
          vm.addedAI[m].validationLength = vm.ValLength;
        }

        if (vm.ValLengthAlt) {
          vm.addedAI[m].validationLength = vm.ValLengthAlt;
        }

      }
      for (var i = 0; i < vm.selectedInterest.length; i++) {
        for (var j = 0; j < vm.addedAI.length; j++) {
          if (vm.selectedInterest[i][0].uuid === vm.addedAI[j].uuid) {
            vm.matchingValue = 1;
          }
        }
      }

      if (value !== 'No additional interest to add') {
        vm.interestselection = true;
        vm.addInterestSection = true;
        vm.defaultValue = false;

        if (vm.selectedValue == 1 && vm.matchingValue == 1) {
          vm.selectedInterest.splice(vm.interestCount, 1);
        } else {
          vm.selectedInterest.push(vm.addedAI);
        }
      } else {
        vm.defaultValue = true;
      }

      if (vm.selectedInterest.length <= 0) {
        vm.interestselection = false;
        vm.defaultValue = true;
      }
      // console.log("ccc..." + JSON.stringify(vm.selectedInterest));
    };

    // get data

    function getFormData() {
      var data = {
        elementType: ['ADDITIONALINTERESTS'],
        operationType: ['VIEW_ADDITIONAL_INTERESTS'],
        masterData: ['true']
      };

      additionalinterestService.fetchAddInterestForm(data).then(
        function (response) {
          vm.additionalinterest = angular.copy(response.data);

          vm.additionalinterestsection = displaySeq(
            vm.additionalinterest['Additional_Interests']
          );
          vm.additionalinterestWidget = vm.additionalinterest['Widgets'];
          vm.additionalinterestMaster = displaySeq(
            vm.additionalinterest['Additional_Interests']
          );
        },
        function () {
          // console.log('getFormData - error', err.detail);
        }
      );
    }

    function displaySeq(response) {
      vm.finalValue = response;
      vm.byDate = vm.finalValue.slice(0);
      vm.byDate.sort(function (a, b) {
        return a.displaySequence - b.displaySequence;
      });
      return vm.byDate;
    }

    function lengthValidation(sysID, modelData, $event, validationvalues) {
      for (var i = 0; i < validationvalues.length; i++) {
        var validationValue = validationvalues[i].includes('max_length');
        if (validationValue == true) {
          var vlength = validationvalues[i];
        }
      }

      var maxlength = Number(vlength.substring(vlength.indexOf(':') + 1));
      if (sysID == 'NAME_OF_THE_PERSON_OR_ORG' && modelData != undefined) {
        if (modelData.length >= maxlength) {
          $event.preventDefault();
        }
      }
      if (sysID == 'STREET_ADDRESS' && modelData != undefined) {
        if (modelData.length >= maxlength) {
          $event.preventDefault();
        }
      }
      if (sysID == 'APT__SUITE' && modelData != undefined) {
        if (modelData.length >= maxlength) {
          $event.preventDefault();
        }
      }
      if (sysID == 'CITY' && modelData != undefined) {
        if (modelData.length >= maxlength) {
          $event.preventDefault();
        }
      }
      if (sysID == 'LEASED_EQUIPMENT_DESCRIPTION' && modelData != undefined) {
        if (modelData.length >= maxlength) {
          $event.preventDefault();
        }
      }
      if (sysID == 'DESCRIPTION_OF_PROPERTY' && modelData != undefined) {
        if (modelData.length >= maxlength) {
          $event.preventDefault();
        }
      }
      if (sysID == 'ZIPCODE' && modelData != undefined) {
        if (modelData.length > 4) {
          $event.preventDefault();
        }
      }
    }


    function checkDuplicateInObject(inputArray) {
      // var seenDuplicate = false;
      // testObject = {};

      inputArray.map(function (parentItem) {
        var testObject = {};
        parentItem.map(function (item) {
          // console.log("aaa.." + JSON.stringify(item));
          var itemPropertyName = item['locationExists'];
          if (itemPropertyName in testObject) {
            // console.log("bbb.." + JSON.stringify(testObject));
            testObject[itemPropertyName].duplicate = true;
            item.duplicate = true;
            // seenDuplicate = true;
          } else {
            testObject[itemPropertyName] = item;
            testObject[itemPropertyName].duplicate = false;
          }
        });
      });

      return inputArray;
    }

    // Save & Continue

    function addinterestSubmit(theForm, value) {

      vm.duplicateBuilding = false;

      vm.selectedInterest = checkDuplicateInObject(value);


      // console.log("Final Result..." + JSON.stringify(vm.selectedInterest));

      for (var i = 0; i < vm.selectedInterest.length; i++) {
        for (var j = 0; j < vm.selectedInterest[i].length; j++) {
          if (vm.selectedInterest[i][j].datasetName === 'BUILDING' && vm.selectedInterest[i][j].duplicate === true) {
            // console.log("ccc..." + vm.selectedInterest[i][j].duplicate);
            vm.duplicateBuilding = true;
          }
        }
      }

      $rootScope.AIUpdatedValue = value;

      Validator.triggerValidation(theForm);
      if (theForm.$valid && !vm.duplicateBuilding) {
        // console.log("Success...");
        // vm.addInterestEditable = true;
        vm.addInterestSection = false;
        vm.selectedInterest = [];

        // save AI Information


        if (value.length > 0) {
          vm.submitValue = value;
          for (var k = 0; k < vm.editableAI.length; k++) {
            vm.duplicateTransData = false;
            for (var l = 0; l < value.length; l++) {

              if(vm.editableAI[k][0].uuid === value[l][0].uuid) {
                vm.duplicateTransData = true;
                // vm.submitValue.push(vm.editableAI[k]);
          }
        }
        if(!vm.duplicateTransData) {
            vm.submitValue.push(vm.editableAI[k]);
        }
      } 

        } else {
          vm.submitValue = vm.editableAI;
        }


        // console.log("vm.submitValue..." + JSON.stringify(vm.submitValue));

        var data = {
          elementType: ['ADDITIONALINTERESTS'],
          operationType: ['SAVE_ADDITIONAL_INTERESTS_IN_APPIAN'],
          data: vm.submitValue
        };

        additionalinterestService.submitAddInterest(data).then(
          function () {
            vm.uncheckedAI = true;
            vm.addInterestEditable = true;

            if (vm.editableAI.length > 0 || vm.defaultValue) {
              util.setPageIndex('app.underwriting');
              $state.go('app.underwriting');
            } else {
              getEditableData();
            }
          },
          function () {
            vm.uncheckedAI = true;
            vm.addInterestEditable = false;
            // getEditableData();
            // getFormData();
          });
      }
    }

    // editing AI

    vm.editAI = function (value, id) {

      vm.addInterestSection = true;
      vm.matchingValues = 0;

      for (var i = 0; i < vm.selectedInterest.length; i++) {
        if (vm.selectedInterest[i][0].uuid === id) {
          vm.matchingValues = 1;
        }
      }

      if (vm.matchingValues != 1) {
        vm.selectedInterest.push(value);
      }
    };

    // deleting AI

    function deleteAI(value, index, id) {

      vm.matchingValue = 0;
      vm.interestCount = 0;
      vm.editableAI.splice(index, 1);
      if (vm.editableAI.length <= 0) {
        vm.addInterestEditable = false;
        vm.interestselection = false;
      }

      for (var i = 0; i < vm.selectedInterest.length; i++) {
        if (vm.selectedInterest[i][0].uuid === id) {
          vm.matchingValue = 1;
          vm.interestCount = i;
        }
      }

      if (vm.matchingValue == 1) {
        vm.selectedInterest.splice(vm.interestCount, 1);
      }
    }

    // get editable AI

    function getEditableData() {
      vm.uncheckedAI = true;
      vm.additionalinterestsection = [];
      vm.additionalinterestsection = vm.additionalinterestMaster;
      vm.uncheckedAI = true;

      var data = {
        elementType: ['ADDITIONALINTERESTS'],
        operationType: ['VIEW_ADDITIONAL_INTERESTS'],
        masterData: ['false']
      };

      additionalinterestService.fetchEditableAddInterestForm(data).then(
        function (response) {
          getFormData();
          vm.uncheckedAI = false;
          vm.editableAI = angular.copy(response.data);

          if (vm.editableAI.length > 0) {
            vm.addInterestEditable = true;
          } else {
            vm.addInterestEditable = false;
          }
        },
        function () {
          getFormData();
          // console.log('getFormData - error', err.detail);
        }
      );
    }

    // zipcode lookup

    function zipcodelookup(mdata, fdata, sysID) {

      if (sysID == 'ZIPCODE') {

        // for (var i = 0; i < fdata.length; i++) {
        // if (fdata[i].systemRefId == 'ZIPCODE') {
        // var ruleName = fdata[i].eventName[0].split('RULE:')[1];
        // }
        // }

        var additionalInterestId = fdata[0].additionalInterestId;
        var locationBuildingReferenceId = fdata[0].locationBuildingReferenceId;
        var aitype = fdata[0].correspondingEntTyp.split(':')[0];
        var assignedLocation = fdata[0].assignedLocation;
        var data = {
          elementType: ["APPIANRULE"],
          operationType: ["ZIPCODELOOKUP1"],
          section: ["9"],
          data: [mdata],
          type: [aitype],
          additionalInterestId: [additionalInterestId],
          locationBuildingReferenceId: [locationBuildingReferenceId],
          assignedLocation:[assignedLocation]
        };

        additionalinterestService.zipcodeLookup(data).then(
          function (res) {
            fdata[0].additionalInterestId = res.data[2].userValue;
            fdata[0].locationBuildingReferenceId = res.data[3].userValue;
            fdata[0].assignedLocation = res.data[4].userValue;
            for (var j = 0; j < res.data.length; j++) {
              if (res.data[j].systemRefId == 'CITY') {
                var zipcity = res.data[j].userValue;
              }

              if (res.data[j].systemRefId == 'STATE') {
                var zipstate = res.data[j].userValue;
              }
            }

            for (var i = 0; i < fdata.length; i++) {
              if (fdata[i].systemRefId == 'CITY') {
                fdata[i].userValue = zipcity;
              }

              if (fdata[i].systemRefId == 'STATE') {
                fdata[i].userValue = zipstate;
              }
            }
          },
          function () {

          });
      }
    }

  }
})();

(function () {
  'use strict';

  angular
    .module('quoteUi')
    .controller('EligibilityController', EligibilityController);

  /** @ngInject */
  function EligibilityController(
    eligibilityService,
    $rootScope,
    $location,
    util
  ) {
    var vm = this;

    var tempInsured = [{
        id: 4000051,
        datasetName: 'First Name ',
        parentDatasetId: null,
        additionalInfo: null,
        mappingId: 9999545,
        systemRefId: 'first_name',
        entityTypeId: 35,
        entityType: 'Widget',
        applicabilityId: null,
        applicabilityCriteria: null,
        applicabilityCriteriaValue: null,
        defaultValue: null,
        datasetVersion: null,
        displaySequence: 1,
        valueSeqId: null,
        valueId: null,
        valueName: null,
        valueType: null,
        displayValue: null,
        typeSeqId: 1470,
        typeId: 149,
        typeName: 'ReferUnderwriter',
        validationSeqId: 101,
        validationId: 1,
        validationName: ['Required'],
        controlTypeId: 3,
        controlName: 'Text_Box',
        eventAssociatedId: null,
        eventName: null,
        url: null,
        appearUI: 'Y',
        editable: null,
        screenColumns: 6,
        isRequired: true
      },
      {
        id: 4000053,
        datasetName: 'Last Name ',
        parentDatasetId: null,
        additionalInfo: null,
        mappingId: 9999547,
        systemRefId: 'last_name',
        entityTypeId: 35,
        entityType: 'Widget',
        applicabilityId: null,
        applicabilityCriteria: null,
        applicabilityCriteriaValue: null,
        defaultValue: null,
        datasetVersion: null,
        displaySequence: 1,
        valueSeqId: null,
        valueId: null,
        valueName: null,
        valueType: null,
        displayValue: null,
        typeSeqId: 1470,
        typeId: 149,
        typeName: 'ReferUnderwriter',
        validationSeqId: 101,
        validationId: 1,
        validationName: ['Required'],
        controlTypeId: 3,
        controlName: 'Text_Box',
        eventAssociatedId: null,
        eventName: null,
        url: null,
        appearUI: 'Y',
        editable: null,
        screenColumns: 6,
        isRequired: true
      }
    ];

    $rootScope.pageInfo = [{
      currentPage: $location.path(),
      currentPageData: [],
      element: [],
      operation: []
    }];

    vm.getFormMeta = getFormMeta;
    vm.submitForm = submitForm;
    vm.addWidget = addWidget;

    vm.getFormMeta();

    /**
	 * Get form meta
	 */
    function getFormMeta() {
      eligibilityService.fetchEligibilityMeta(null).then(
        function (res) {
          // sort
          vm.formMeta = res.data;
          vm.formMeta.agentContactInfo = _.sortBy(
            vm.formMeta.agentContactInfo,
            'displaySequence'
          );
          vm.formMeta.insuredInformation = _.sortBy(
            vm.formMeta.insuredInformation,
            'displaySequence'
          );
          // agent info
          _.each(vm.formMeta.agentContactInfo, function (item) {
            var displaySeq = util.displaySeqCount(vm.formMeta.agentContactInfo);
            item.screenColumns = 12 / displaySeq[item.displaySequence];
            if (item.validationName.indexOf('Required') != -1) {
              item.isRequired = true;
            }
            if (item.controlName == 'Header') {
              vm.agentInfoHdr = item;
            }
          });
          //
          _.each(vm.formMeta.insuredInformation, function (item) {
            var displaySeq = util.displaySeqCount(
              vm.formMeta.insuredInformation
            );
            item.screenColumns = 12 / displaySeq[item.displaySequence];
            if (item.validationName.indexOf('Required') != -1) {
              item.isRequired = true;
            }
            if (item.controlName == 'Header') {
              vm.insuredInformationHdr = item;
            }
          });
        },
        function () {}
      );
    }

    /**
	 * Submit form
	 * 
	 * @param {*}
	 *            isValid
	 */
    function submitForm() {
      //
    }

    function addWidget() {
      vm.formMeta.additionalInsuredInformation.push(tempInsured);
    }
  }
})();

(function () {
  'use strict';

  angular
    // details
    .module('quoteUi')
    .controller('DetailsController', DetailsController);

  /** @ngInject */
  function DetailsController(
    $scope,
    $state,
    $rootScope,
    $stateParams,
    util,
    $window,
    accountsService,
    ErrorToast
  ) {
    var vm = this;
    // vm.formatDate = formatDate;
    vm.assetPath = util.getImagePath();
    // vm.getAction = getAction();
    /**
	 * On Create proposal button
	 */
    // function formatDate(aDate) {
    // return moment(aDate).format('YYYY-MM-DD');
    // }

    vm.handofurl = '/DSCIQuote/outbound?landingPageInCE=Select_Line_of_Business&lob=CarrierBusinessOwners';

    var accountId = $stateParams.accountId.replace(':', '');

    vm.addr = $stateParams.adderss.replace(':', '');

    // console.log(accountId, accountId2);

    var data = {
      elementType: ['ACCOUNT'],
      operationType: ['EDITACCOUNT'],
      clientId: [accountId]
    };

    accountsService.fetchAccountDet(data).then(
      function (res) {
        vm.pageContent = typeof res.data !== 'undefined' ? res.data : [];

        vm.acDet = [];
        vm.lobdata = [];
        
        vm.isDsci = res.data.isDSCIData.BOP_isDsciQuote;
        vm.nonLob = res.data.isDSCIData.BOP_isDsciLob;
        /*
		 * vm.pageContent.controls.forEach(function(item) {
		 * 
		 * if (item.action != undefined) { if (item.action.id ==
		 * 'MaintainAccount_10E') { vm.lobdata[0].quoteid =
		 * item.action.methods[0].policyId; } }
		 * 
		 * if (item.field != undefined) { if (item.field.name ==
		 * 'clientDetails.clientID') { vm.acDet.clientid = item.field.value; }
		 * if (item.field.name == 'clientDetails.name') { vm.acDet.name =
		 * item.field.value; } if (item.field.name ==
		 * 'misc.UNIG_XPathBopPrimaryState') { vm.acDet.riskstate =
		 * item.field.value; } if ( item.field.name ==
		 * 'ClassSearch.UNIG_XPathBOPClassSearchResultsReadOnly' ) {
		 * vm.acDet.bclass = item.field.value; } if (item.field.caption == 'BOP
		 * Effective Date') { item.field.caption = 'Business Owners Policy';
		 * vm.lobdata.push(item.field); } if ( item.field.name ==
		 * 'UticaUmbrellaBOPInformation.BOPUnderwritingStatusXPath' ) {
		 * vm.lobdata[0].status = item.field.value; } if ( item.field.name ==
		 * 'UticaUmbrellaBOPInformation.BOPTotalPolicyPremiumXPath' ) {
		 * vm.lobdata[0].premium = item.field.value; }
		 *  } });
		 */
      },
      function (err) {
        if (err.status == 401) {
          $state.go('accountview');
        }
      }
    );


   
    vm.getAction = function (pid, ac, status) {
      // console.log("Action is ====>", ac, "isDSCI--->",
		// vm.isDsci,"isNonlob--->", vm.nonLob);
      switch (status) {
        case "Submission Referred":
            $window.location = '/DSCIQuote/outbound?landingPageInCE=Pricing_page&lob=CarrierBusinessOwners&quoteId=' + pid;
            break;
        case "Submission Authorized":
            $window.location = '/DSCIQuote/outbound?landingPageInCE=Issue_Policy_page&lob=CarrierBusinessOwners&quoteId=' + pid;
            break;
        default:
          if(vm.isDsci != "FALSE"){
            console.log("true");
          var appianhdr = {
            elementType: ['ACCOUNT'],
            operationType: ['RESUMEQUOTE'],
            policy_id: [pid],
            action: [ac]
          };

          accountsService.getAction(appianhdr).then(
            function (res) {
              util.setPageIndex(res.data.state);
              console.log(res.data.state, vm.nonLob);
              if(res.data.state == 'QuoteId_not_available_in_Appian'){
                $window.location = '/DSCIQuote/outbound?landingPageInCE=Select_Line_of_Business&lob=CarrierBusinessOwners';
              }else{
                if(vm.nonLob == "FALSE"){
                  $window.location = '/DSCIQuote/outbound?landingPageInCE=My_Dashboard_page&lob=CarrierBusinessOwners';
                }
                else{
                $state.go(res.data.state);
                }
              }
            },
            function (err) {
              ErrorToast.showError(err);
            }
          );
        }
        else{
          $window.location = '/DSCIQuote/outbound?landingPageInCE=My_Dashboard_page&lob=CarrierBusinessOwners';
        }
        break;
  }
  };
  }
})();

(function () {
  'use strict';

  angular
    .module('quoteUi')
    .controller('BusinessDetailsController', BusinessDetailsController);


  /** @ngInject */
  function BusinessDetailsController(
    $state,
    $window,
    ErrorToast,
    $rootScope,
    ParseJSON,
    $scope,
    Conf,
    $timeout,
    toastr,
    Validator,
    $q,
    businessService,
    $filter,
    $location,
    $transitions,
    $anchorScroll,
    $uibModal,
    $http,
    $sce,
    util
  ) {
    var vm = this;

    vm.checkInvalidYear;
    vm.appianData = [];
    vm.flag = '0';
    vm.businessClassError = 0;
    vm.invalidAnnualMessage = 0;
    vm.saleFlag = 0;
    vm.invalidPhoneFormat;
    vm.invalidPhoneFormatFlag;
    vm.salesError;
    vm.lengthOfZip;
    vm.tempZip;
    vm.businessDetailsForm = {};
    vm.businessData;
    var now = new Date();
    vm.currentYear = now.getFullYear();
    vm.businessDetailsForm.uuid = [];
    vm.maskAttributes = [];
    vm.detailExplanation;
    vm.businessStatus;
    vm.detailExplanationSetValue;
    vm.priorEXPSetValue;
    vm.googleData = false;
    vm.gAddr;
    vm.lat;
    vm.lng;
    vm.streetAPTSuite = "";
    vm.cityStateZipcode = "";
    vm.country = "";
    vm.selectedVerifyBusiness = null;
    var googleJSON = [];
    var verifyBusinessID;
    var checkAllFieldOfBCandBN = false;
    var doNotCallVeriskAgain = false;
    vm.verifyBusinessDropDown = [];
    var checkUSPS_ExperianCallAlreadyTrigger;
    vm.businessClassAndCode= [];

    $rootScope['maskOpts' + '0'] = {
      mask: '(999) 999-9999',
      restrict: 'reject',
      validate: 'true',
      limit: 'true'
    };

    /**
	 * Methods in Business Detail Page
	 */

    // Business Detail Data on page Load Method
    vm.getBusinessDetailsMeta = getBusinessDetailsMeta;

    // Save Business Detail Data Method
    vm.triggerSubmit = triggerSubmit;

    // Google address render on Google Suggestion BOX
    vm.updateLoc = updateLoc;

    // On click of magic fill button Get complete google address from address
	// component field in google JSON and render them in there respective field
    vm.googleAPIAddress = googleAPIAddress;

    // On click of magic fill button Get year + annualSale + BusinessPhone from
	// verisk JSON and render them in there respective field
    vm.businessInfoMagicFill = businessInfoMagicFill;

    // Google address render on street address search
    vm.gAutofill = gAutofill;

    // check if the length of zipCode is less than 5
    vm.checkZipLessThan5 = checkZipLessThan5;

    // Trigger zipCode lookup call to get city and state
    vm.zipCodeLookUP = zipCodeLookUP;

    // Trigger USPS/Experian call to get suggested address for verify business
	// drop down
    vm.USPSandEXPERIAN = USPSandEXPERIAN;

    // Alert window when user select any business name from verify Business Drop
	// Down Field.
    vm.openPOPUP = openPOPUP;

    // When user select business Name from Verify Business Field then client ID
	// is generated + get all the Business Class
    vm.sendDataToExperian = sendDataToExperian;

    // Check if user's selected or typed business class is matched with Business
	// Class in Drop down.
    vm.businessClassValidation = businessClassValidation;

    // Whatever value user select from verify Busiess Field, Take only Business
	// Name from the list and set it to ng-model. Check verify Business In UI.
    vm.assignVerifyBusinessNameTOngModal = assignVerifyBusinessNameTOngModal;

    // When user select business class then set corresponding value in ng-model.
	// Check business CLass JSON to understand.
    vm.businessClassCaptionValue = businessClassCaptionValue;

    // trigger verisk call to get suggested Class + Year + Annual Sales +
	// Business Phone
    vm.verisk = verisk;


    // Appian Rule For Business Info (NEED CODE REFACTORING)
    vm.checkTheAppianRule = checkTheAppianRule;
    vm.checkAppainForBusinessDetail = checkAppainForBusinessDetail;
    vm.eligibilityAppianRuleForYearStarted = eligibilityAppianRuleForYearStarted;

    vm.scrollTo = scrollTo;
    vm.phoneLengthErrorMessage = phoneLengthErrorMessage;
    vm.makeNULLfor2HiddenField = makeNULLfor2HiddenField;
    vm.hide2FieldWhenYearNULL = hide2FieldWhenYearNULL;

    /**
	 * These Functions are currently not in use.
	 */
    // vm.startsWith = startsWith;
    // vm.replaceSpecialChracterPHONE = replaceSpecialChracterPHONE;





    $rootScope.pageInfo = [{
      "currentPage": $location.path(),
      "currentPageData": [],
      "element": [],
      "operation": []
    }];
    vm.getBusinessDetailsMeta();

    vm.autocompleteOptions = {
      componentRestrictions: {
        country: 'us'
      },
      // types: ['geocode']
      types: ['establishment']
    };
    vm.streetAutocompleteOptions = {
      componentRestrictions: {
        country: 'us'
      },
      // types: ['Addresses']
    };

    /**
	 * 
	 * This transition stop user going back from BD Page to BYB and account Page
	 * using browser back button. This transition invoke when user click on
	 * browser back button
	 */
    $transitions.onStart({}, function (trans) {
      if (trans.from().name == 'app.businessdetails' && (trans.to().name == 'app.beforeyoubegin')) {
        return false;
      }
      // if (trans.from().name == 'app.businessdetails' && !(trans.to().name
		// == 'accountview')) {
      // return false;
      // }
    });



    function openPOPUP(businessName, address, defaultValue) {

      vm.verifyBNModalInstance = $uibModal.open({

        templateUrl: 'blocks/modal/verifyPOPUP.html',

        windowClass: 'gradientModal',

        backdrop: 'static',

        keyboard: false,

        controller: function ($scope, $uibModalInstance) {

          $scope.verifyBusinessNamePOPUP = businessName;
          $scope.verifyBusinessCompanyAddressPOPUP = address;
          $scope.defaultValue = defaultValue;

          $scope.closePOPUP = function () {
            vm.selectedVerifyBusiness = null;
            $uibModalInstance.close();
          };
          $scope.sendExperianID = function () {


            $uibModalInstance.close();
            // vm.selectedVerifyBusiness = businessName;
            sendDataToExperian(verifyBusinessID);
          };

        },

        controllerAs: 'vm',

        resolve: {
          param: function () {
            return {};
          }
        }
      });
    } // End OF POP UP


    function assignVerifyBusinessNameTOngModal(data, id) {

      vm.selectedVerifyBusiness = data;
      verifyBusinessID = id;
    }


    function sendDataToExperian(id) {

      var verifyJSON, booleanDefaultValueNotInTheList = false;

      /**
		 * Taking Verify Business data on based of ID associated with it. So,
		 * once ID is matched then corresponding verify Business data is send to
		 * back-end.
		 */
      _.each(vm.verifyBusiness, function (obj1) {
        if (obj1.controlName.toLowerCase() != 'header') {
          _.each(obj1.valueName, function (obj2) {
            if (obj2.id == id) {
              verifyJSON = obj2;
              return;
            }
          });
        }
      });

      /**
		 * Here what ever Business Name + Address user selected in verify
		 * Business that data ( Business Name + Business Address ) is populate
		 * to Business Location Widget + Business Name Widget
		 */
      _.each(vm.verifyBusiness, function (obj) {

        _.each(obj.valueName, function (obj2) {
          if (obj2.id == id && obj2.defaultValue == false) {
            booleanDefaultValueNotInTheList = true;
            _.each(vm.businessAddress, function (obj1) {

              // for Street Address
              if (obj1.systemRefId == 'mailingAddress.address1') {

                obj1.userValue = obj2['item.addressLine1'];

              }
              // for APT/Suite
              if (obj1.systemRefId == 'mailingAddress.address2') {
                obj1.userValue = obj2['item.addressLine2'];
              }
              // for city
              if (obj1.systemRefId == 'mailingAddress.city') {
                obj1.userValue = obj2['item.city'];
              }
              // for state
              if (obj1.systemRefId == 'mailingAddress.state') {
                obj1.userValue = vm.businessDetailsForm.uuid[obj1.uuid];
              }
              // for zipcode
              if (obj1.systemRefId == 'mailingAddress.ZIP') {
                obj1.userValue = vm.businessDetailsForm.uuid[obj1.uuid];
              }
            });

            _.each(vm.businessName, function (obj1) {
              if (obj1.systemRefId == 'clientDetails.name') {
                obj1.userValue = obj2['item.firmName'];
                obj.userValue = obj2['item.firmName'];

              }
            });

          }// checking
        });

      });
      if (booleanDefaultValueNotInTheList){
        vm.businessAddress = ParseJSON.parse(vm.businessAddress, vm.businessDetailsForm.uuid);
        vm.businessName = ParseJSON.parse(vm.businessName, vm.businessDetailsForm.uuid);
      }
      vm.verifyBusiness = ParseJSON.parse(vm.verifyBusiness, vm.businessDetailsForm.uuid);

      /**
		 * dataEXP - send to back-end
		 * 
		 */
      var dataEXP = {
        data: [verifyJSON],
        elementType: ['BUSINESSCLASS'],
        operationType: ['SELECTEXPERIAN']
      };

      /**
		 * AJAX Call
		 */
      businessService.experianClientID(dataEXP).then(

        function (response) {

          // console.log('Client ID ', res);

          // Here we get all the Business class once user select value
          // from verify Business Field + after client ID creation.
          // AND
          // also make Business Class field Editable;
          if (response.data.classSearchResults){

            vm.businessClassAndCode = response.data.classSearchResults;

            _.each(vm.businessClassAndUserValue, function (obj1) {
              if (obj1.controlName.toLowerCase() != 'header') {
                obj1.editable = 'Y';
                obj1.valueName = vm.businessClassAndCode;
              }
            });
          }


          // This make Verify Business Field Non-Editable;
          _.each(vm.verifyBusiness, function (obj1) {
            if (obj1.controlName.toLowerCase() != 'header') {
              obj1.editable = 'N';
            }
          });

        },
        function () {
          // ErrorToast.showError("VERIFY BUSINESS EXPERIAN CALL FAILED");

        }
      );

    }


    function businessClassCaptionValue(value, businessClassArray) {

      var tempBusinessClass;

      _.each(businessClassArray, function (obj1) {
        if (obj1.value == value) {
          tempBusinessClass = obj1.caption;
          vm.noResults = false;
          return;
        }
      });

      if (tempBusinessClass) {
        return tempBusinessClass;
      } else {
        return value;
      }

    }

    /**
	 * Call USPS and Experian Services to validate business location Address
	 */
    function USPSandEXPERIAN(ifZIPCode) {


      var demoBCUSPS = false;
      var demoBLUSPS = false;
      var USPSJSONObject = angular.copy(ParseJSON.returnFields(vm.businessData, vm.businessDetailsForm.uuid));
      var businessAddressUSPS = angular.copy(ParseJSON.returnFields(vm.businessAddress, vm.businessDetailsForm.uuid));
      var businessNameUSPS = angular.copy(ParseJSON.returnFields(vm.businessName, vm.businessDetailsForm.uuid));



      if (ifZIPCode == true) {
        return;
      }

      _.each(businessNameUSPS, function (obj) {

        if (obj.controlName.toLowerCase() != 'header' && (obj.appearUI && obj.appearUI.toLowerCase() == 'y') && !obj.userValue) {

          demoBCUSPS = true;
          checkAllFieldOfBCandBN = true;
          return;
        }

      });
      if (demoBCUSPS) {
        return;
      }

      _.each(businessAddressUSPS, function (obj1) {

        if (obj1.controlName.toLowerCase() != 'header' && (obj1.appearUI && obj1.appearUI.toLowerCase() == 'y') && obj1.isRequired == true && !obj1.userValue) {

          demoBLUSPS = true;
          checkAllFieldOfBCandBN = true;
          return;
        }

      });

      if (demoBLUSPS) {
        return;
      }


      /**
		 * This variable become false which mean that all fields in Business
		 * Location Widget and Business Name Widget are filled with user value.
		 * This variable will be use in verisk() function call where we check
		 * that if 'checkAllFieldOfBCandBN' == false it mean that all input
		 * fields in Business Location Widget and Business Name Widget are not
		 * empty.
		 * 
		 * Why we use this variable? Because it save time to loop through in
		 * Business Location Widget and Business Name Widget to check whether
		 * the input field are filled or not. In USPSandEXPERIAN() we are
		 * already checking it. so no need to check this thing in verisk()
		 * function. Since checkAllFieldOfBCandBN = false; it mean we already
		 * checked all fields in Business Location Widget and Business Name
		 * Widget in USPSandEXPERIAN(). (check demoBLUSPS and demoBCUSPS)
		 */
      checkAllFieldOfBCandBN = false;
      verisk();

      if (checkUSPS_ExperianCallAlreadyTrigger) {
        return;
      }

      var data = {
        data: [USPSJSONObject],
        elementType: ['BUSINESSCLASS'],
        operationType: ['EXPERIAN']

      };
      // console.log('USPS Data = ', data);

      businessService.USPSandExperian(data).then(function (res) {
          // console.log('USPS/EXP - success', res.data);
          checkUSPS_ExperianCallAlreadyTrigger = true;
          vm.verifyBusinessDropDown = [];
          // Verify Business Widget
          vm.verifyBusiness = ParseJSON.parse(res.data['Verify_Business'], vm.businessDetailsForm.uuid);

            if (!res.data.scrubStatus) {

              if (res.data.errorMessage) {
                vm.showUSPSErrorMessage = true;
                vm.USPSFailureErrorMessage = res.data.errorMessage;
              }

              // vm.verifyBusinessDropDown.push({
              // 'id': '',
              // 'BusinessName': '',
              // 'City': '',
              // 'streetAddress': '',
              // 'completeAddress': '',
              // 'defaultValue': ''
              // });
              return;
          }

          // Business Name Widget
          vm.businessName = ParseJSON.parse(res.data['Business_Name'], vm.businessDetailsForm.uuid);
          // Business Location Widget
          vm.businessAddress = ParseJSON.parse(res.data['Business_Address'], vm.businessDetailsForm.uuid);

          _.each(vm.verifyBusiness, function (obj1) {

            if (obj1.controlName.toLowerCase() != 'header') {
              _.each(obj1.valueName, function (obj2) {

                var businessNameVerify = '';
                var cityVerify = '';
                var addressLine1Verify = '';
                var addressLine2Verify = '';
                var expID;
                var defaultValue;

                for (var key in obj2) {

                  switch (key) {

                    case 'item.firmName':
                                          businessNameVerify = obj2[key];
                                          break;

                    case 'item.city':
                                          cityVerify = obj2[key];
                                          break;

                    case 'item.addressLine1':

                                          addressLine1Verify = addressLine1Verify + obj2[key];
                                          break;

                    case 'item.addressLine2':

                                          addressLine2Verify = addressLine2Verify + obj2[key];
                                          break;

                    case 'id':
                                          expID = obj2[key];
                                          break;
                    case 'defaultValue':
                                          defaultValue = obj2[key];
                                          break;

                  }
                }
                var showAddressInPOPUP;
                if (addressLine1Verify && addressLine2Verify){
                    showAddressInPOPUP = addressLine1Verify + ', ' + addressLine2Verify + ', ' + cityVerify;
                }
                if (!addressLine1Verify && !addressLine2Verify) {
                    showAddressInPOPUP = cityVerify;
                }
                if (addressLine1Verify && !addressLine2Verify) {
                    showAddressInPOPUP = addressLine1Verify + ', ' + cityVerify;
                }
                if (!addressLine1Verify && addressLine2Verify) {
                    showAddressInPOPUP = addressLine2Verify + ', ' + cityVerify;
                }


                vm.verifyBusinessDropDown.push({
                  'id': expID,
                  'BusinessName': businessNameVerify,
                  'City': cityVerify,
                  'streetAddress': addressLine1Verify,
                  'completeAddress': showAddressInPOPUP,
                  'defaultValue': defaultValue
                });


              }); // End of Inner each loop

            }
          }); // End of outer each loop

        },
        function () {
          // ErrorToast.showError("USPS/Experian Call Failed");

        }); // End of Ajax Call


    } // End of USPSandEXPERIAN


    /**
	 * To understand the functionality of this function visit DS-2242 / DS-2243
	 */
    function verisk() {

      var checkBCUserValue = false;
      var businessNameVerisk;
      var stateVerisk;
      var cityVerisk;
      var zipCodeVerisk;
      var streetVerisk;
      var formatVeriskResponseBC;


      if (doNotCallVeriskAgain) {
        return;
      }

      _.each(vm.businessClassAndUserValue, function (obj) {
        if (obj.controlName.toLowerCase() != 'header') {
          obj.userValue = vm.businessDetailsForm.uuid[obj.uuid];
          return;
        }
      });

      _.each(vm.businessClassAndUserValue, function (obj) {
        if (obj.controlName.toLowerCase() != 'header') {

          if (!obj.userValue) {
            checkBCUserValue = true;
            return;
          } else {
            vm.productIDParameter["businessClass"] = obj.userValue;
            return;
          }
        }
      });


      /***********************************************************************
		 * This Condition check that whether all the input fields of Business
		 * Name widget + Business Location Widget + Business Class Widget are
		 * empty the return the function and not tigger verisk call. BUT all
		 * fields are filled then this condition (checkBCUserValue ||
		 * checkAllFieldOfBCandBN) become false and we proceed with verisk call.
		 */
      if (checkBCUserValue || checkAllFieldOfBCandBN) {
        return;
      }

      if (vm.productIDParameter.businessOwnersPolicy == false) {
        // ErrorToast.showError("Intend Question : NO - Verisk Failed");
        return;
      }
      /**
		 * Parameters for product ID
		 */
      var dataForProductID = {
        data: [vm.productIDParameter],
        elementType: ['BUSINESSCLASS'],
        operationType: ['FETCHPRODUCTID']
      };


      _.each(vm.businessName, function (obj) {
        if (obj.controlName.toLowerCase() != 'header') {

          businessNameVerisk = obj.userValue;
        }
      });

      _.each(vm.businessAddress, function (obj) {
        if (obj.controlName.toLowerCase() != 'header') {
          if (obj.systemRefId == 'mailingAddress.address1') {
            streetVerisk = obj.userValue;
          }
          if (obj.systemRefId == 'mailingAddress.state') {
            stateVerisk = obj.userValue;
          }
          if (obj.systemRefId == 'mailingAddress.city') {
            cityVerisk = obj.userValue;
          }
          if (obj.systemRefId == 'mailingAddress.ZIP') {
            zipCodeVerisk = obj.userValue;
          }
        }
      });

      /**
		 * Parameters for Verisk
		 */
      var veriskData = {
        "elementType" : ['BUSINESSCLASS'],
        "operationType": ["GETVERISK"],
        "data": [
          [{
            "StreetName": streetVerisk,
            "City": cityVerisk,
            "State": stateVerisk,
            "Zip": zipCodeVerisk,
            "BusinessName": businessNameVerisk
          }]
        ]
      };

      // console.log("Product ID Parameter : ", dataForProductID);
      // console.log("varisk Parameter : ", veriskData);

      /**
		 * AJAX Call FOR VERISK PRODUCT ID
		 */
      businessService.productIDforVerisk(dataForProductID).then(

        function (response) {

          // console.log("Product ID Response ", response);

          if (response.data.productId) {

            /**
			 * AJAX Call FOR VERISK
			 */
            businessService.veriskCall(veriskData).then(

              function (response) {

                  doNotCallVeriskAgain = true;
                  // console.log("verisk Success Response ", response);

                  vm.veriskYearStarted = 'No data available';
                  vm.veriskAnnualSales = 'No data available';
                  vm.veriskPhoneNumber = 'No data available';
                  vm.veriskBusinessName = [];

                  if (response.data.veriskData) {

                    if (response.data.veriskData.veriskBusinessRecordsMatched == 1) {

                        vm.veriskData = true;
                        vm.businessInfoMagicButton = true;

                      if (response.data.veriskData.businessData.yearStarted){
                          vm.veriskYearStarted = response.data.veriskData.businessData.yearStarted;
                        }
                      if (response.data.veriskData.businessData.annualSales) {
                        vm.veriskAnnualSales = response.data.veriskData.businessData.annualSales;
                      }
                      if (response.data.veriskData.locations[0].buildings[0].phone){
                        vm.veriskPhoneNumber = response.data.veriskData.locations[0].buildings[0].phone;
                      }

                      if (response.data.veriskData.businessData.primarySicCode && response.data.veriskData.businessData.primarySicDescription) {

                        formatVeriskResponseBC = '[' + response.data.veriskData.businessData.primarySicCode + '] ' + response.data.veriskData.businessData.primarySicDescription;
                        vm.veriskBusinessName.push(formatVeriskResponseBC);
                      }
                      if (response.data.veriskData.businessData.secondarySicCode && response.data.veriskData.businessData.secondarySicDescription) {

                        formatVeriskResponseBC = '[' + response.data.veriskData.businessData.secondarySicCode + '] ' + response.data.veriskData.businessData.secondarySicDescription;
                        vm.veriskBusinessName.push(formatVeriskResponseBC);
                      }
                      if (response.data.veriskData.businessData.tertiarySicCode && response.data.veriskData.businessData.tertiarySicDescription) {

                        formatVeriskResponseBC = '[' + response.data.veriskData.businessData.tertiarySicCode + '] ' + response.data.veriskData.businessData.tertiarySicDescription;
                        vm.veriskBusinessName.push(formatVeriskResponseBC);
                      }

                      if (!(response.data.veriskData.businessData.primarySicCode && response.data.veriskData.businessData.primarySicDescription) &&
                        !(response.data.veriskData.businessData.secondarySicCode && response.data.veriskData.businessData.secondarySicDescription) &&
                        !(response.data.veriskData.businessData.tertiarySicCode && response.data.veriskData.businessData.tertiarySicDescription)) {

                          vm.veriskBusinessName = ['No data available'];
                      }

                    } else {
                      vm.veriskData = false;
                    }

                  } else {
                    vm.veriskData = false;
                  }

                },
              function () {
                // ErrorToast.showError("VERISK CALL FAILED");
              }
            );

          } else {
            // ErrorToast.showError("PRODUCT ID IS NULL");

          }

        },
        function () {
          // ErrorToast.showError("VERISK CALL FOR PRODUCT ID IS FAILED");
        }
      );

    }


    function updateLoc(addr, uuid) {

      googleJSON = angular.copy(addr);
      // console.log(googleJSON);
      if (addr == undefined) {
        vm.gAddr = '';
        vm.googleData = false;
      }
      if (addr && addr.address_components != undefined) {
        vm.googleData = true;
        vm.gAddr = addr.adr_address;
        vm.gPhone = addr.formatted_phone_number;
        vm.lat = addr.geometry.location.lat();
        vm.lng = addr.geometry.location.lng();
        vm.data = addr.geometry.location.toString().replace(/(\s\s)*(\s(?=\[.*?\]\s))*\[.*?\](\s\s)*/g, '');
        vm.businessDetailsForm.uuid[uuid] = addr.name;

      }

      var googleAdd = [],
        temp;
      var premise, subpremise;


      if (!googleJSON || !googleJSON.hasOwnProperty("address_components")) {
        return;
      }


      for (var i = 0; i < googleJSON.address_components.length; i++) {
        if (googleJSON.address_components[i].types.indexOf('administrative_area_level_1') != -1) { // state

          temp = googleJSON.address_components[i].types.indexOf('administrative_area_level_1');
          googleAdd[googleJSON.address_components[i].types[temp]] = googleJSON.address_components[i].short_name;

        } else if (googleJSON.address_components[i].types.indexOf('administrative_area_level_3') != -1 || // city
          googleJSON.address_components[i].types.indexOf('locality') != -1) {
          if (googleJSON.address_components[i].types.indexOf('administrative_area_level_3') != -1) {

            temp = googleJSON.address_components[i].types.indexOf('administrative_area_level_3');
            googleAdd[googleJSON.address_components[i].types[temp]] = googleJSON.address_components[i].short_name;
          }
          if (googleJSON.address_components[i].types.indexOf('locality') != -1) {
            temp = googleJSON.address_components[i].types.indexOf('locality');
            googleAdd[googleJSON.address_components[i].types[temp]] = googleJSON.address_components[i].short_name;
          }
        } else if (googleJSON.address_components[i].types.indexOf('postal_code') != -1) { // ZipCode
          temp = googleJSON.address_components[i].types.indexOf('postal_code');
          googleAdd[googleJSON.address_components[i].types[temp]] = googleJSON.address_components[i].short_name;

        } else if (googleJSON.address_components[i].types.indexOf('route') != -1) { // street
																					// address
																					// -
																					// street
																					// name
          temp = googleJSON.address_components[i].types.indexOf('route');
          googleAdd[googleJSON.address_components[i].types[temp]] = googleJSON.address_components[i].short_name;

        } else if (googleJSON.address_components[i].types.indexOf('street_number') != -1) { // street
																							// address
																							// -
																							// street
																							// number
          temp = googleJSON.address_components[i].types.indexOf('street_number');
          googleAdd[googleJSON.address_components[i].types[temp]] = googleJSON.address_components[i].short_name;

        } else if (googleJSON.address_components[i].types.indexOf('premise') != -1) { // APT/Suite
																						// -
																						// building
																						// name

          temp = googleJSON.address_components[i].types.indexOf('premise');
          googleAdd[googleJSON.address_components[i].types[temp]] = googleJSON.address_components[i].short_name;

        } else if (googleJSON.address_components[i].types.indexOf('subpremise') != -1) { // APT/Suite
																							// -
																							// building
																							// number
          temp = googleJSON.address_components[i].types.indexOf('subpremise');
          googleAdd[googleJSON.address_components[i].types[temp]] = googleJSON.address_components[i].short_name;
        } else if (googleJSON.address_components[i].types.indexOf("country") != -1) { // APT/Suite
																						// -
																						// building
																						// number
          temp = googleJSON.address_components[i].types.indexOf('country');
          googleAdd[googleJSON.address_components[i].types[temp]] = googleJSON.address_components[i].short_name;
        }
      } // End of loop
      vm.streetAPTSuite = "";
      vm.cityStateZipcode = "";
      vm.country = "";
      // console.log('*/-*/-*/-*/-*/-*/',googleAdd);
      if (_.has(googleAdd, "administrative_area_level_3") || // city
        _.has(googleAdd, "locality")) {

        if (_.has(googleAdd, "locality")) {
          vm.cityStateZipcode = _.property('locality')(googleAdd);
        } else if (_.has(googleAdd, "administrative_area_level_3")) {
          vm.cityStateZipcode = _.property('administrative_area_level_3')(googleAdd);

        }
      }

      if (_.has(googleAdd, "administrative_area_level_1")) { // state
        if (vm.cityStateZipcode) {
          vm.cityStateZipcode = vm.cityStateZipcode + ', ' + _.property('administrative_area_level_1')(googleAdd);
        } else {
          vm.cityStateZipcode = _.property('administrative_area_level_1')(googleAdd);
        }
      }


      if (_.has(googleAdd, "postal_code")) { // zipcode
        if (vm.cityStateZipcode) {
          vm.cityStateZipcode = vm.cityStateZipcode + ', ' + _.property('postal_code')(googleAdd);
        } else {
          vm.cityStateZipcode = _.property('postal_code')(googleAdd);
        }

      }

      if (_.has(googleAdd, "country")) { // country
        vm.country = _.property('country')(googleAdd);
      }



      if (_.has(googleAdd, 'route') || _.has(googleAdd, 'street_number')) { // Street
																			// address

        var route, streetNumber;
        if (_.has(googleAdd, 'route')) {
          route = _.property('route')(googleAdd);
          vm.streetAPTSuite = route;
        }
        if (_.has(googleAdd, 'street_number')) {
          streetNumber = _.property('street_number')(googleAdd);
          vm.streetAPTSuite = streetNumber;
        }
        if (route && streetNumber) {
          vm.streetAPTSuite = streetNumber + ', ' + route;
        }
      }

      if (_.has(googleAdd, 'premise') || _.has(googleAdd, 'subpremise')) { // APT/Suite
        if (vm.streetAPTSuite) {
          if (_.has(googleAdd, 'premise')) {
            premise = _.property('premise')(googleAdd);
            vm.streetAPTSuite = vm.streetAPTSuite + ', ' + premise;
          }
          if (_.has(googleAdd, 'subpremise')) {
            subpremise = _.property('subpremise')(googleAdd);
            vm.streetAPTSuite = vm.streetAPTSuite + ', ' + subpremise;
          }
          if (subpremise && premise) {
            vm.streetAPTSuite = vm.streetAPTSuite + ', ' + subpremise + ', ' + premise;
          }
        } else {
          if (_.has(googleAdd, 'premise')) {
            premise = _.property('premise')(googleAdd);
            vm.streetAPTSuite = premise;
          }
          if (_.has(googleAdd, 'subpremise')) {
            subpremise = _.property('subpremise')(googleAdd);
            vm.streetAPTSuite = subpremise;
          }
          if (subpremise && premise) {
            vm.streetAPTSuite = subpremise + ', ' + premise;
          }
        }

      }

    } // End of updateLoc()

    function googleAPIAddress() {
      var googleAdd = [],
        temp;
      for (var i = 0; i < googleJSON.address_components.length; i++) {
        if (googleJSON.address_components[i].types.indexOf('administrative_area_level_1') != -1) { // state

          temp = googleJSON.address_components[i].types.indexOf('administrative_area_level_1');
          googleAdd[googleJSON.address_components[i].types[temp]] = googleJSON.address_components[i].short_name;

        } else if (googleJSON.address_components[i].types.indexOf('administrative_area_level_3') != -1 || // city

          googleJSON.address_components[i].types.indexOf('locality') != -1) {
          if (googleJSON.address_components[i].types.indexOf('administrative_area_level_3') != -1) {

            temp = googleJSON.address_components[i].types.indexOf('administrative_area_level_3');
            googleAdd[googleJSON.address_components[i].types[temp]] = googleJSON.address_components[i].short_name;
          }
          if (googleJSON.address_components[i].types.indexOf('locality') != -1) {
            temp = googleJSON.address_components[i].types.indexOf('locality');
            googleAdd[googleJSON.address_components[i].types[temp]] = googleJSON.address_components[i].short_name;
          }
        } else if (googleJSON.address_components[i].types.indexOf('postal_code') != -1) { // ZipCode
          temp = googleJSON.address_components[i].types.indexOf('postal_code');
          googleAdd[googleJSON.address_components[i].types[temp]] = googleJSON.address_components[i].short_name;

        } else if (googleJSON.address_components[i].types.indexOf('route') != -1) { // street
																					// address
																					// -
																					// street
																					// name
          temp = googleJSON.address_components[i].types.indexOf('route');
          googleAdd[googleJSON.address_components[i].types[temp]] = googleJSON.address_components[i].short_name;

        } else if (googleJSON.address_components[i].types.indexOf('street_number') != -1) { // street
																							// address
																							// -
																							// street
																							// number
          temp = googleJSON.address_components[i].types.indexOf('street_number');
          googleAdd[googleJSON.address_components[i].types[temp]] = googleJSON.address_components[i].short_name;

        } else if (googleJSON.address_components[i].types.indexOf('premise') != -1) { // APT/Suite
																						// -
																						// building
																						// name

          temp = googleJSON.address_components[i].types.indexOf('premise');
          googleAdd[googleJSON.address_components[i].types[temp]] = googleJSON.address_components[i].short_name;

        } else if (googleJSON.address_components[i].types.indexOf('subpremise') != -1) { // APT/Suite
																							// -
																							// building
																							// number
          temp = googleJSON.address_components[i].types.indexOf('subpremise');
          googleAdd[googleJSON.address_components[i].types[temp]] = googleJSON.address_components[i].short_name;
        }
      }
      // console.log(googleAdd);




      for (var j = 0; j < vm.businessAddress.length; j++) {

        // if(vm.businessAddress[j].systemRefId == 'mailingAddress.city'){

        // // console.log(_.has(googleAdd,"locality") );
        // // console.log(_.property('locality')(googleAdd) );
        // if(_.has(googleAdd,"administrative_area_level_3") || //city
        // _.has(googleAdd,"locality") ){

        // if(_.has(googleAdd,"administrative_area_level_3") ){
        // vm.businessDetailsForm.uuid[vm.businessAddress[j].uuid] =
		// _.property('administrative_area_level_3')(googleAdd);
        // }else if(_.has(googleAdd,"locality")){
        // vm.businessDetailsForm.uuid[vm.businessAddress[j].uuid] =
		// _.property('locality')(googleAdd);
        // }else{
        // vm.businessDetailsForm.uuid[vm.businessAddress[j].uuid] = "";
        // }
        // }

        // }else if(vm.businessAddress[j].systemRefId ==
		// 'mailingAddress.state'){

        // if(_.has(googleAdd,"administrative_area_level_1")){ // state
        // vm.businessDetailsForm.uuid[vm.businessAddress[j].uuid] =
		// _.property('administrative_area_level_1')(googleAdd);
        // }else{
        // vm.businessDetailsForm.uuid[vm.businessAddress[j].uuid] = "";
        // }

        // }
        if (vm.businessAddress[j].systemRefId == 'mailingAddress.ZIP') { // zipcode

          if (_.has(googleAdd, "postal_code")) {
            vm.businessDetailsForm.uuid[vm.businessAddress[j].uuid] = _.property('postal_code')(googleAdd);
            vm.lengthOfZip = false;
            vm.zipCodeLookUP(vm.businessAddress[j].systemRefId, vm.businessAddress[j].eventName, vm.lengthOfZip, vm.businessDetailsForm.uuid[vm.businessAddress[j].uuid]);
          } else {
            vm.businessDetailsForm.uuid[vm.businessAddress[j].uuid] = "";
          }

        } else if (vm.businessAddress[j].systemRefId == 'mailingAddress.address2') { // APT/Suite

          var premise, subpremise;
          vm.businessDetailsForm.uuid[vm.businessAddress[j].uuid] = null;

          if (_.has(googleAdd, 'premise') || _.has(googleAdd, 'subpremise')) {
            if (_.has(googleAdd, 'premise')) {
              premise = _.property('premise')(googleAdd);
              vm.businessDetailsForm.uuid[vm.businessAddress[j].uuid] = premise;
            }
            if (_.has(googleAdd, 'subpremise')) {
              subpremise = _.property('subpremise')(googleAdd);
              vm.businessDetailsForm.uuid[vm.businessAddress[j].uuid] = subpremise;
            }
            if (subpremise && premise) {
              vm.businessDetailsForm.uuid[vm.businessAddress[j].uuid] = subpremise + ' ' + premise;
            }
          }
        } else if (vm.businessAddress[j].systemRefId == 'mailingAddress.address1') { // Street
																						// address

          vm.businessDetailsForm.uuid[vm.businessAddress[j].uuid] = '';
          if (_.has(googleAdd, 'route') || _.has(googleAdd, 'street_number')) {

            var route, streetNumber;
            if (_.has(googleAdd, 'route')) {
              route = _.property('route')(googleAdd);
              vm.businessDetailsForm.uuid[vm.businessAddress[j].uuid] = route;
            }
            if (_.has(googleAdd, 'street_number')) {
              streetNumber = _.property('street_number')(googleAdd);
              vm.businessDetailsForm.uuid[vm.businessAddress[j].uuid] = streetNumber;
            }
            if (route && streetNumber) {
              vm.businessDetailsForm.uuid[vm.businessAddress[j].uuid] =
                streetNumber + ' ' + route;
            }
          }
        }
      }
    } // end of googleAPIAdress Function

    /**
	 * 
	 * @param {*}
	 *            businessInfoJSON year + annualSale + PhoneNumber coming from
	 *            Verisk JSON will render in there respective input fields on
	 *            click of magic fill button.
	 */
    function businessInfoMagicFill(businessInfoJSON){

      _.each(businessInfoJSON, function (obj1) {

        if (obj1.systemRefId == 'misc.YearBusinessStarted' &&
            vm.veriskYearStarted &&
            vm.veriskYearStarted != 'No data available') {

            vm.businessDetailsForm.uuid[obj1.uuid] = vm.veriskYearStarted;
            eligibilityAppianRuleForYearStarted(obj1, vm.veriskYearStarted);
        }
        else if (obj1.systemRefId == "misc.TotalAnnualSales" &&
                  vm.veriskAnnualSales &&
                  vm.veriskAnnualSales != 'No data available') {

          if (vm.veriskAnnualSales.indexOf('-',1) == -1){
            vm.businessDetailsForm.uuid[obj1.uuid] = vm.veriskAnnualSales;
            checkTheAppianRule(vm.veriskAnnualSales, obj1.systemRefId);
            }

        }
        else if (obj1.systemRefId == "BusinessContact.BusinessPhone" &&
                  vm.veriskPhoneNumber &&
                  vm.veriskPhoneNumber != 'No data available') {

          vm.businessDetailsForm.uuid[obj1.uuid] = $filter('veriskPhoneFormat')(vm.veriskPhoneNumber);
        }
      });

    } // End of businessInfoMagicFill()

    function zipCodeLookUP(sysRefID, eventName, zipCodeLength, ZIPdata) {
      if (sysRefID == 'mailingAddress.ZIP' &&
        !zipCodeLength &&
        ZIPdata &&
        ZIPdata.length > 0) {

        if (eventName &&
          Array.isArray(eventName) &&
          eventName[0] &&
          eventName[0].indexOf(':') !== -1) {
          // AJAX call for ZIPCODE
          var zipCodeEventName = eventName[0].split(':')[1];
          var zipCodeAJAXData = {
            elementType: ['APPIANRULE'],
            operationType: [zipCodeEventName],
            section: ['2'],
            data: [ZIPdata]
          };
          // console.log('Zip Code Look UP Sending = ',zipCodeAJAXData);
          businessService.zipCodeLookUP(zipCodeAJAXData).then(
            function (res) {
              // console.log('Zip Code Look UP Receiving = ',res);

              if (res.data[0].systemRefId != 'mailingAddress.ZIP') {
                for (var businessAddressLoop = 0; businessAddressLoop < vm.businessAddress.length; businessAddressLoop++) {
                  for (var zipCodelookUPLoop = 0; zipCodelookUPLoop < res.data.length; zipCodelookUPLoop++) {
                    if (vm.businessAddress[businessAddressLoop].systemRefId == res.data[zipCodelookUPLoop].systemRefId) {
                      vm.businessDetailsForm.uuid[vm.businessAddress[businessAddressLoop].uuid] = res.data[zipCodelookUPLoop].userValue;
                    }
                  } // END OF INNER LOOP
                } // End of outer Loop

                vm.lengthOfZip = false;

                USPSandEXPERIAN(false);
              } else {
                vm.lengthOfZip = true;
                for (var businessAddressLoop1 = 0; businessAddressLoop1 < vm.businessAddress.length; businessAddressLoop1++) {

                  if (vm.businessAddress[businessAddressLoop1].systemRefId == 'mailingAddress.city') {
                    vm.businessDetailsForm.uuid[vm.businessAddress[businessAddressLoop1].uuid] = null;
                  }
                  if (vm.businessAddress[businessAddressLoop1].systemRefId == 'mailingAddress.state') {
                    vm.businessDetailsForm.uuid[vm.businessAddress[businessAddressLoop1].uuid] = null;
                  }

                }

              }
            },
            function () {

              // ErrorToast.showError('Error LOG: ZipCodeLookUP Call Failed');

              // var res = [
              // {
              // "systemRefId": "mailingAddress.city",
              // "userValue": "PORTSMOUTH"
              // },
              // {
              // "systemRefId": "mailingAddress.state",
              // "userValue": "NH"
              // },
              // {
              // "systemRefId": "COUNTY",
              // "userValue": "ROCKINGHAM"
              // }];

              // // var res = [
              // // {
              // // "systemRefId": "mailingAddress.ZIP"
              // // }];

              // if(res[0].systemRefId != "mailingAddress.ZIP"){
              // for(var businessAddressLoop =0 ; businessAddressLoop <
				// vm.businessAddress.length; businessAddressLoop++){
              // for(var zipCodelookUPLoop =0 ; zipCodelookUPLoop < res.length
				// ; zipCodelookUPLoop++){
              // if(vm.businessAddress[businessAddressLoop].systemRefId ==
				// res[zipCodelookUPLoop].systemRefId){
              // vm.businessDetailsForm.uuid[vm.businessAddress[businessAddressLoop].uuid]
				// = res[zipCodelookUPLoop].userValue;
              // }
              // }// END OF INNER LOOP
              // }// End of outer Loop
              // vm.lengthOfZip = false;
              // }else{
              // vm.lengthOfZip = true;
              // }

              // USPSandEXPERIAN();
            }
          );
        } else {
          // ErrorToast.showError('No Appian Call because either Event Name is
			// Null or Event Name is not a Array or colon (:) is missing');
        }


      } // If end
    } // End of zipCodeLookUP()

    function makeNULLfor2HiddenField(systemRefId) {

      if (systemRefId == 'misc.YearBusinessStarted' && vm.businessStatus == false) {

        for (var nullBusinessStatus = 0; nullBusinessStatus < vm.businessInfo.length; nullBusinessStatus++) {

          if (vm.businessInfo[nullBusinessStatus].systemRefId == 'misc.UNIG_BusinessStatus') {

            vm.businessDetailsForm.uuid[vm.businessInfo[nullBusinessStatus].uuid] = null;
            vm.businessInfo[nullBusinessStatus].userValue = '';
            break;
          }
        }
      }
    } // End of makeNULLfor2HiddenField()

    // function replaceSpecialChracterPHONE(phoneData, sysRef) {
    // if (sysRef == 'BusinessContact.BusinessPhone' && phoneData) {
    // phoneData = phoneData.toString().replace(/-/g, '');

    // // console.log(phoneData);
    // return phoneData;
    // }
    // return phoneData;
    // }


    function phoneLengthErrorMessage(phoneData, sysRef) {

      if (sysRef == 'BusinessContact.BusinessPhone') {

        if (phoneData &&
          (phoneData.replace(/[^\d]/g, '').length > 10 ||
            phoneData.replace(/[^\d]/g, '').length < 10)) {

          vm.invalidPhoneFormat = true;
        } else {
          vm.invalidPhoneFormat = false;
        }
      }

    } // End of phoneLengthErrorMessage function;

    // function scrollTo(id) {
    // $location.hash(id);
    // $anchorScroll.yOffset = 10000;
    // $anchorScroll();
    // }
    /**
	 * 
	 * @param {*}
	 *            systemRefId
	 * @param {*}
	 *            data
	 */

    function gAutofill(gData) {
      // console.log(gData);
      var temp;
      var googleAdd = [];
      if (gData && gData.address_components != undefined) {
        for (var i = 0; i < gData.address_components.length; i++) {
          if (gData.address_components[i].types.indexOf('administrative_area_level_1') != -1) { // state

            temp = gData.address_components[i].types.indexOf('administrative_area_level_1');
            googleAdd[gData.address_components[i].types[temp]] = gData.address_components[i].short_name;

          } else if (gData.address_components[i].types.indexOf('administrative_area_level_3') != -1 || // city

            gData.address_components[i].types.indexOf('locality') != -1) {
            if (gData.address_components[i].types.indexOf('administrative_area_level_3') != -1) {

              temp = gData.address_components[i].types.indexOf('administrative_area_level_3');
              googleAdd[gData.address_components[i].types[temp]] = gData.address_components[i].short_name;
            }
            if (gData.address_components[i].types.indexOf('locality') != -1) {
              temp = gData.address_components[i].types.indexOf('locality');
              googleAdd[gData.address_components[i].types[temp]] = gData.address_components[i].short_name;
            }
          } else if (gData.address_components[i].types.indexOf('postal_code') != -1) { // ZipCode
            temp = gData.address_components[i].types.indexOf('postal_code');
            googleAdd[gData.address_components[i].types[temp]] = gData.address_components[i].short_name;

          } else if (gData.address_components[i].types.indexOf('route') != -1) { // street
																					// address
																					// -
																					// street
																					// name
            temp = gData.address_components[i].types.indexOf('route');
            googleAdd[gData.address_components[i].types[temp]] = gData.address_components[i].short_name;

          } else if (gData.address_components[i].types.indexOf('street_number') != -1) { // street
																							// address
																							// -
																							// street
																							// number
            temp = gData.address_components[i].types.indexOf('street_number');
            googleAdd[gData.address_components[i].types[temp]] = gData.address_components[i].short_name;

          } else if (gData.address_components[i].types.indexOf('premise') != -1) { // APT/Suite
																					// -
																					// building
																					// name

            temp = gData.address_components[i].types.indexOf('premise');
            googleAdd[gData.address_components[i].types[temp]] = gData.address_components[i].short_name;

          } else if (gData.address_components[i].types.indexOf('subpremise') != -1) { // APT/Suite
																						// -
																						// building
																						// number
            temp = gData.address_components[i].types.indexOf('subpremise');
            googleAdd[gData.address_components[i].types[temp]] = gData.address_components[i].short_name;
          }
        }

        for (var j = 0; j < vm.businessAddress.length; j++) {

          if (vm.businessAddress[j].systemRefId == 'mailingAddress.ZIP') { // zipcode

            if (_.has(googleAdd, "postal_code")) {
              vm.businessDetailsForm.uuid[vm.businessAddress[j].uuid] = _.property('postal_code')(googleAdd);
              vm.lengthOfZip = false;
              vm.zipCodeLookUP(vm.businessAddress[j].systemRefId, vm.businessAddress[j].eventName, vm.lengthOfZip, vm.businessDetailsForm.uuid[vm.businessAddress[j].uuid]);
            } else {
              vm.businessDetailsForm.uuid[vm.businessAddress[j].uuid] = "";
            }

          } else if (vm.businessAddress[j].systemRefId == 'mailingAddress.address2') { // APT/Suite

            var premise, subpremise;
            vm.businessDetailsForm.uuid[vm.businessAddress[j].uuid] = null;

            if (_.has(googleAdd, 'premise') || _.has(googleAdd, 'subpremise')) {
              if (_.has(googleAdd, 'premise')) {
                premise = _.property('premise')(googleAdd);
                vm.businessDetailsForm.uuid[vm.businessAddress[j].uuid] = premise;
              }
              if (_.has(googleAdd, 'subpremise')) {
                subpremise = _.property('subpremise')(googleAdd);
                vm.businessDetailsForm.uuid[vm.businessAddress[j].uuid] = subpremise;
              }
              if (subpremise && premise) {
                vm.businessDetailsForm.uuid[vm.businessAddress[j].uuid] = subpremise + ' ' + premise;
              }
            }
          } else if (vm.businessAddress[j].systemRefId == 'mailingAddress.address1') { // Street
																						// address

            vm.businessDetailsForm.uuid[vm.businessAddress[j].uuid] = '';
            if (_.has(googleAdd, 'route') || _.has(googleAdd, 'street_number')) {

              var route, streetNumber;
              if (_.has(googleAdd, 'route')) {
                route = _.property('route')(googleAdd);
                vm.businessDetailsForm.uuid[vm.businessAddress[j].uuid] = route;
              }
              if (_.has(googleAdd, 'street_number')) {
                streetNumber = _.property('street_number')(googleAdd);
                vm.businessDetailsForm.uuid[vm.businessAddress[j].uuid] = streetNumber;
              }
              if (route && streetNumber) {
                vm.businessDetailsForm.uuid[vm.businessAddress[j].uuid] =
                  streetNumber + ' ' + route;
              }
            }
          }
        }

      }

    }

    function hide2FieldWhenYearNULL(systemRefId, data) {

      if (systemRefId == 'misc.YearBusinessStarted' &&
        data == null &&
        (vm.detailExplanation == true || vm.businessStatus == true)) {

        vm.detailExplanation = false;
        vm.businessStatus = false;
      }
    }

    // function startsWith(businessClassAndCode, viewValue) {
    // var data = [];
    // var textTemp;
    // var test = [];
    // var temp1 = [];
    // // console.log('--',viewValue);
    // for (var i = 0; i < businessClassAndCode.length; i++) {
    // if (
    // businessClassAndCode[i].toLowerCase().match(viewValue.toLowerCase())
    // ) {
    // textTemp = businessClassAndCode[i]
    // .toLowerCase()
    // .indexOf(viewValue.toLowerCase());
    // data.push({
    // id: textTemp,
    // data: businessClassAndCode[i]
    // });
    // }
    // }

    // // console.log(data);
    // test = $filter('orderBy')(data, 'id');
    // // console.log(test);

    // for (var key in test) {
    // temp1.push(test[key].data);
    // }
    // // console.log("33333",temp1);

    // if (temp1) {
    // return temp1;
    // } else {
    // return vm.businessClassAndCode;
    // }
    // } // END of startsWith


    /**
	 * desc
	 * 
	 * @param {*}
	 *            data
	 */
    function checkZipLessThan5(data) {
      if (data) {
        var temp = data.toString();
        if (temp.length < 5 && temp.length > 0) {
          return true;
        }
      }
      return false;
    }


    /**
	 * Business class validation This function is checking whether the user's
	 * typed Business Class is matching with the Business Classes in the drop
	 * down. If it is a match then we will set 'vm.noResults' == true which mean
	 * that user's typed Business Class is matched with one of the Business
	 * Classes in the drop down which is Correct. BUT if match not found then we
	 * will set 'vm.noResults' == false which mean that user's typed Business
	 * Class is not matched with any of the Business Classes in the drop down
	 * which is Incorrect.
	 * 
	 * Finally, we will use 'vm.noResults' to print the error message. When
	 * 'vm.noResults' == true it mean that we are ready to display error message
	 * BUT error message must be visible on click of save&Continue. For that we
	 * are using 'vm.businessClassError' variable Which we will use during save.
	 * 
	 * @param {*}
	 *            businessClassData
	 */
    function businessClassValidation(businessClassData) {

      for (var businessClassLoop = 0; businessClassLoop < vm.businessClassAndCode.length; businessClassLoop++) {

        if (vm.businessClassAndCode[businessClassLoop].hasOwnProperty('caption') &&
          vm.businessClassAndCode[businessClassLoop].caption.toLowerCase() == businessClassData.toLowerCase() && businessClassData) {

          vm.noResults = false;
          break;
        }
        vm.noResults = true;
      }

    }

    /**
	 * desc
	 * 
	 * @param {*}
	 *            userData
	 * @param {*}
	 *            systemRefID
	 */
    function checkTheAppianRule(userData, systemRefID) {

      vm.widgetData = {};

      if ((vm.widgetData = vm.checkAppainForBusinessDetail(vm.businessData.Business_Info))) {

        for (var i = 0; i < vm.widgetData.length; i++) {

          if (systemRefID == 'misc.YearBusinessStarted') {

            vm.checkInvalidYear = vm.eligibilityAppianRuleForYearStarted(vm.widgetData[i], userData);
            return;

          } else if (systemRefID == 'misc.TotalAnnualSales' && userData) {

            vm.checkEventName = vm.widgetData[i].eventName[0].split(':')[1];

            if (userData) {
              userData = userData.toString().replace(/,/g, '');
              userData = userData.toString().replace('$', '');
            }

            var data = {
              elementType: ['APPIANRULE'],
              operationType: ['BRVALIDATEANNUALSALES'],
              data: [userData]
            };
            // console.log('checkTheAppianRule - data', data);

            vm.salesErrorMsg = '';
            if (vm.errorMessageAnnualSale == false ||
              vm.errorMessageAnnualSale == undefined) {

              businessService.validateBusinessAppianRule(data).then(
                function (res) {
                  // console.log('checkTheAppianRule - success', res);
                  if (res.data[0].error) {

                    vm.salesError = true;
                    vm.salesErrorMsg = res.data[0].error;

                  } else {
                    vm.salesError = false;
                  }
                },
                function () {
                  // ErrorToast.showError(err);
                });
            }
            return;

          } else if (systemRefID == 'BusinessContact.BusinessPhone') {
            return;
          }

        } // END of For Loop
      } // END of IF
    } // End of checkTheAppianRule()

    /**
	 * Check which widget in business Detail Page haveappianRule
	 * 
	 * @param {*}
	 *            checkAppianRule
	 */
    function checkAppainForBusinessDetail(checkAppianRule) {
      var tempAppianRule = [];

      for (var i = 0; i < checkAppianRule.length; i++) {
        if (checkAppianRule[i].eventName != null) {
          tempAppianRule.push(checkAppianRule[i]);
        }
      }

      if (tempAppianRule.length != 0) {
        var arrayData = JSON.stringify(tempAppianRule);
        var arrayJOSN = JSON.parse(arrayData);
        return arrayJOSN;
      } else {
        return false;
      }
    }

    /**
	 * Eligibilty Appian rule based on selected business class in business
	 * detail page
	 * 
	 * @param {*}
	 *            checkAppianRule
	 * @param {*}
	 *            yearStart
	 */
    function eligibilityAppianRuleForYearStarted(checkAppianRule, yearStart) {
      vm.checkEventName = checkAppianRule.eventName[0].split(':')[1];

      if (yearStart) {
        var data = {
          elementType: ['APPIANRULE'],
          operationType: [vm.checkEventName],
          year: [yearStart]
        };
        // console.log('eligibilityAppianRuleForYearStarted - data', data);

        // call service
        businessService.yearStartedRule(data).then(
          function (res) {
            // console.log('eligibilityAppianRuleForYearStarted -
			// success',res.data);
            vm.appianData = angular.copy(res.data);

            for (var i = 0; i < vm.appianData.length; i++) {
              /* Year Rule */
              if (vm.appianData[i].sysRefId == 'misc.YearBusinessStarted') {
                if (vm.appianData[i].error) {
                  vm.checkInvalidYear = true;
                  vm.invalidYear = vm.appianData[i].error;
                } else {
                  vm.checkInvalidYear = false;
                }
              }

              /* Detail Explanation Rule */
              if (vm.appianData[i].sysRefId == 'Details_Explanation') {
                // if(vm.appianData[i].visible == true) {
                // vm.detailExplanation = true;
                // }
                // else {
                // vm.detailExplanation = false;
                // }

                if (Array.isArray(vm.appianData[i].value)) {
                  vm.detailExplanationSetValue = vm.appianData[i].value[0];
                  // console.log('++++++',vm.appianData[i].value[0]);
                } else {
                  vm.detailExplanationSetValue = vm.appianData[i].value;
                  // console.log('-----',vm.appianData[i].value);
                }
              }

              /* Business Status Rule */
              if (vm.appianData[i].sysRefId == 'misc.UNIG_BusinessStatus') {
                if (vm.appianData[i].visible == true) {
                  for (var hideAndShow = 0; hideAndShow < vm.businessInfo.length; hideAndShow++ ) {
                    if (vm.businessInfo[hideAndShow].systemRefId == 'misc.UNIG_BusinessStatus') {
                      vm.businessInfo[hideAndShow].appearUI = 'Y';
                      // console.log(vm.businessInfo);
                      break;
                    }
                  }
                  vm.businessStatus = true;
                } else {
                  for (var hideAndShow1 = 0; hideAndShow1 < vm.businessInfo.length; hideAndShow1++) {
                    if (vm.businessInfo[hideAndShow1].systemRefId == 'misc.UNIG_BusinessStatus') {
                      vm.businessInfo[hideAndShow1].appearUI = 'N';
                      // console.log(vm.businessInfo);
                      break;
                    }
                  }
                  vm.businessStatus = false;
                }
              }

              /* Prior Experience Rule */
              if (vm.appianData[i].sysRefId == 'misc.UNIG_PriorExperience') {
                if (Array.isArray(vm.appianData[i].value)) {
                  vm.priorEXPSetValue = vm.appianData[i].value[0];
                } else {
                  vm.priorEXPSetValue = vm.appianData[i].value;
                }
              }
            }
          },
          function () {
            // ErrorToast.showError(err.detail);
            // vm.checkInvalidYear = true;
            // vm.invalidYear = "Invalid Year Started";
            // if(yearStart == 2016)
            // {
            // vm.detailExplanation = true;
            // // vm.businessStatus = true;
            // }else{
            // vm.detailExplanation = false;
            // vm.businessStatus = false;
            // }
          }
        );
      }
    } // End of Appian rule function



    /**
	 * Get business form
	 */
    function getBusinessDetailsMeta() {
      var data = {
        elementType: ['BUSINESSCLASS'],
        operationType: ['VIEWBUSINESSDETAILS']
      };
      // console.log('getBusinessDetailsMeta - data', data);


      businessService.fetchBusinessForm(data).then(
        function (res) {
          // console.log('getBusinessDetailsMeta - succes', res);
          vm.pageLoadShowWidget  = true;

          vm.businessData = angular.copy(res.data);

          if( vm.businessData['suggestionBox']){

            vm.googleData                         = vm.businessData['suggestionBox'].googleData;
            vm.lat                                = vm.businessData['suggestionBox'].longitude;
            vm.lng                                = vm.businessData['suggestionBox'].latitude;
            vm.streetAPTSuite                     = vm.businessData['suggestionBox'].streetAPTSuite;
            vm.cityStateZipcode                   = vm.businessData['suggestionBox'].cityStateZipcode;
            vm.country                            = vm.businessData['suggestionBox'].country;
            vm.businessInfoMagicButton            = vm.businessData['suggestionBox'].businessInfoMagicButton;
            vm.veriskData                         = vm.businessData['suggestionBox'].veriskData;
            vm.veriskBusinessName                 = vm.businessData['suggestionBox'].veriskBusinessName;
            vm.veriskPhoneNumber                  = vm.businessData['suggestionBox'].veriskPhoneNumber;
            vm.veriskAnnualSales                  = vm.businessData['suggestionBox'].veriskAnnualSales;
            vm.veriskYearStarted                  = vm.businessData['suggestionBox'].veriskYearStarted;
            checkUSPS_ExperianCallAlreadyTrigger  = vm.businessData['suggestionBox'].checkUSPSExperianCallAlreadyTrigger;
            doNotCallVeriskAgain                  = vm.businessData['suggestionBox'].doNotCallVeriskAgain;
          }

          // Taking Before You Begin Data for Verisk
          vm.productIDParameter = vm.businessData['beforeYouBeginData'];
          // Disable Complete Page on base of true or false value
          vm.disableCompletePage = vm.businessData['quoteCreated'];
          // Business Name Widget
          vm.businessName = ParseJSON.parse(vm.businessData['Business_Name'], vm.businessDetailsForm.uuid);
          // Business Location Widget
          vm.businessAddress = ParseJSON.parse(vm.businessData['Business_Address'], vm.businessDetailsForm.uuid);
          // Verify Business Widget
          vm.verifyBusiness = ParseJSON.parse(vm.businessData['Verify_Business'], vm.businessDetailsForm.uuid);
          // vm.verifyBusinessDropDown = [];


          // _.each(vm.verifyBusiness, function (obj1) {

          // if (obj1.controlName.toLowerCase() != 'header') {
          // _.each(obj1.valueName, function (obj2) {

          // var businessNameVerify = ' ';
          // var cityVerify = ' ';
          // var addressLine1Verify = ' ';
          // var addressLine2Verify = ' ';
          // var expID;
          // var defaultValue;

          // for (var key in obj2) {

          // switch (key) {

          // case 'item.firmName':
          // businessNameVerify = obj2[key];
          // break;

          // case 'item.city':
          // cityVerify = obj2[key];
          // break;

          // case 'item.addressLine1':

          // addressLine1Verify = addressLine1Verify + obj2[key];
          // break;

          // case 'item.addressLine2':

          // addressLine2Verify = addressLine2Verify + obj2[key];
          // break;

          // case 'id':
          // expID = obj2[key];
          // break;
          // case 'defaultValue':
          // defaultValue = obj2[key];
          // break;

          // }
          // }

          // var showAddressInPOPUP = addressLine1Verify + ', ' +
			// addressLine2Verify + ', ' + cityVerify;

          // vm.verifyBusinessDropDown.push({
          // 'id': expID,
          // 'BusinessName': businessNameVerify,
          // 'City': cityVerify,
          // 'streetAddress': addressLine1Verify + ' ' + addressLine2Verify,
          // 'completeAddress': showAddressInPOPUP,
          // 'defaultValue': defaultValue
          // });


          // });

          // }
          // });

          // Business Class Widget
          vm.businessClassAndUserValue = ParseJSON.parse(vm.businessData['Business_Class'], vm.businessDetailsForm.uuid);

          _.each(vm.businessClassAndUserValue, function (obj1) {
            if (obj1.controlName.toLowerCase() != 'header' && obj1.valueName) {
                vm.businessClassAndCode = obj1.valueName;
              }
          });

          // Business Info Widget
          vm.businessInfo = ParseJSON.parse(vm.businessData['Business_Info'], vm.businessDetailsForm.uuid, $scope.maskAttributes);

          $scope.maskAttributes2 = [];
          for (var key in $scope.maskAttributes) {
            var value = $scope.maskAttributes[key];
            $rootScope['mask' + key] = angular.copy(value);
          }



          // UUID REFERENCE FOR STATIC FIELDS
          vm.businessInfoUUID = [];
          $timeout(function () {
            vm.businessInfoUUID[0] = $('#Year_Started').data('uuid');
            vm.businessInfoUUID[1] = $('#Annual_Sales').data('uuid');
          });
          // for (var userValueBusinessClass = 0; userValueBusinessClass <
			// vm.businessClassAndUserValue.length; userValueBusinessClass++) {

          // if
			// (vm.businessClassAndUserValue[userValueBusinessClass].hasOwnProperty("userValue")
			// &&
          // vm.businessClassAndUserValue[userValueBusinessClass].userValue &&
          // vm.businessClassAndUserValue[userValueBusinessClass].controlName.toLowerCase()
			// == 'drop_down') {

          // for (var userValueBusinessClass1 = 0; userValueBusinessClass1 <
			// vm.businessClassAndUserValue[userValueBusinessClass].valueName[0].length;
			// userValueBusinessClass1++) {

          // if
			// (vm.businessClassAndUserValue[userValueBusinessClass].userValue
			// ==
          // vm.businessClassAndUserValue[userValueBusinessClass].valueName[0][userValueBusinessClass1].value)
			// {

          // vm.businessDetailsForm.uuid[vm.businessClassAndUserValue[userValueBusinessClass].uuid]
			// =
          // vm.businessClassAndUserValue[userValueBusinessClass].valueName[0][userValueBusinessClass1].caption;
          // break;
          // }
          // }
          // }
          // }

        },
        function (err) {
          if (err.status == 401) {
            $state.go('accountview');
          }
        }
      );
    }





    /**
	 * Submit business form
	 * 
	 * @param {*}
	 *            isValid
	 */
    function triggerSubmit(isValid) {
      // Validator.triggerValidation(theForm);

      /**
		 * BUSINESS CLASS ERROR This condition check if the user choose the
		 * business class from the drop down OR the Business Class user typed in
		 * business class field is matched with drop down value then, if no
		 * match found then "vm.noResults" become true and we display error
		 * message but we need to show error message on click of 'save&continue'
		 * button. SO, we are checking if "vm.noResults" become true inside
		 * triggerSubmit() function, it mean there is no match found for
		 * Business class so display error message in UI.
		 */
      if (vm.noResults == true) {
        vm.businessClassError = 1;
      } else {
        vm.businessClassError = 0;
      }

      /**
		 * ANNUAL SALES - APPIAN BASED ERROR This condition check if the user
		 * entered the correct annual sale. Appian will tell whether the entered
		 * annual sales is correct ot not. If not then appian send the error
		 * message to be display in UI. If appian send the error message then
		 * "vm.salesError" become true telling us that annual sale is wrong and
		 * there is error message coming from Appian. When "vm.salesError"
		 * become true, we need to display error message but we need to show
		 * error message on click of 'save&continue' button. SO, we are checking
		 * if "vm.salesError" become true inside triggerSubmit() function, it
		 * mean annual sale is wrong So display error message in UI.
		 */
      if (vm.salesError == true) {
        vm.saleFlag = '1';
      } else {
        vm.saleFlag = '0';
      }

      /**
		 * ANNUAL SALES - FORMAT BASED ERROR This condition check if the user
		 * entered the correct annual sale FORMAT. If FORMAT is not correct then
		 * "vm.errorMessageAnnualSale" become true telling us that FORMAT for
		 * annual sale is wrong. When "vm.salesError" become true, we need to
		 * display error message but we need to show error message on click of
		 * 'save&continue' button. SO, we are checking if "vm.salesError" become
		 * true inside triggerSubmit() function, it mean FORMAT of annual sale
		 * is wrong So display error message in UI.
		 */
      if (vm.errorMessageAnnualSale == true) {
        vm.invalidAnnualMessage = 1;
      } else {
        vm.invalidAnnualMessage = 0;
      }

      /**
		 * YEAR STARTED - APPIAN BASED ERROR This condition check if the user
		 * entered the correct year. Appian will tell whether the entered year
		 * is correct ot not. If not then appian send the error message to be
		 * display in UI. If appian send the error message then
		 * "vm.checkInvalidYear" become true telling us that year is wrong and
		 * there is error message coming from Appian. When "vm.checkInvalidYear"
		 * become true, we need to display error message but we need to show
		 * error message on click of 'save&continue' button. SO, we are checking
		 * if "vm.checkInvalidYear" become true inside triggerSubmit() function,
		 * it mean annual sale is wrong So display error message in UI.
		 */
      if (vm.checkInvalidYear == true) {
        vm.flag = '1';
      } else {
        vm.flag = '0';
      }

      /**
		 * BUSINESS PHONE - FORMAT BASED ERROR This condition check if the user
		 * entered the correct Business Phone FORMAT. If FORMAT is not correct
		 * then "vm.invalidPhoneFormat" become true telling us that FORMAT for
		 * Business Phone is wrong. When "vm.invalidPhoneFormat" become true, we
		 * need to display error message but we need to show error message on
		 * click of 'save&continue' button. SO, we are checking if
		 * "vm.invalidPhoneFormat" become true inside triggerSubmit() function,
		 * it mean FORMAT of Business Phone is wrong So display error message in
		 * UI.
		 */
      if (vm.invalidPhoneFormat == true) {
        vm.invalidPhoneFormatFlag = 1;
      } else {
        vm.invalidPhoneFormatFlag = 0;
      }

      /**
		 * Here we check if any of the above 'if' condition is true it mean
		 * there is error message that is display in UI. So in that case there
		 * should not be any ajax call for saving the page data + not navigate
		 * to next page. So we check the condition below and if any one become
		 * true, we revert using return statement.
		 */
      if (
        vm.businessClassError == 1 || // BUSINESS CLASS ERROR
        vm.saleFlag == '1' || // ANNUAL SALES - APPIAN BASED ERROR
        vm.invalidAnnualMessage == 1 || // ANNUAL SALES - FORMAT BASED ERROR
        vm.lengthOfZip || // ZIPCODE LENGTH IS LESS THAN 5
        vm.flag == '1' || // YEAR STARTED - APPIAN BASED ERROR
        vm.invalidPhoneFormatFlag == 1) { // BUSINESS PHONE - FORMAT BASED
											// ERROR
        return;
      }
      /**
		 * Here we check if USPS have error message then DO NOT allow user to
		 * save.
		 */
      if(vm.showUSPSErrorMessage){
        return;
      }

      /**
		 * This condition validate input field on click of save&continue.Check
		 * if they are empty or not. If they are empty return else continue to
		 * save functionality
		 */
      if (!isValid) {
        return false;
      }

      /**
		 * Here we merge all data from ng-model (vm.businessDetailsForm.uuid) to
		 * JSON (userValue and other key..)
		 */
      var theRes = angular.copy(ParseJSON.returnFields(vm.businessData, vm.businessDetailsForm.uuid));

      /**
		 * When user manually entered the Business Class (without selecting
		 * Business Class from drop down.) AND if the entered the Business Class
		 * by user matched with Business Class in drop-down but user did choose
		 * it from drop down in such case typeahead-select-on-exact &
		 * typeahead-input-formatter won't work because they work when user
		 * select from drop-down. So, on the time of saving we are replacing
		 * caption with value in Business class field.
		 * 
		 * This condition match for that scenario when user manually enter the
		 * Business Class and he/she gets more than 1 suggestions for buisness
		 * class in drop-down but he/she did not select from drop-down and leave
		 * the business class whatever he/she entered.
		 * 
		 */
      var breakLoop;
      for (var businessClass = 0; businessClass < theRes.Business_Class.length; businessClass++) {
        if (theRes.Business_Class[businessClass].systemRefId == 'ClassSearch.UNIG_ClassSearchResults') {

          for (var businessClassDropDown = 0; businessClassDropDown < vm.businessClassAndCode.length; businessClassDropDown++) {

            if (vm.businessClassAndCode[businessClassDropDown].caption.toLowerCase() == theRes.Business_Class[businessClass].userValue.toLowerCase()) {
                theRes.Business_Class[businessClass].userValue = vm.businessClassAndCode[businessClassDropDown].value;
                breakLoop = true;
                break;
            }
          }
          if (breakLoop){
              break;
          }
        }
      } // END of FOR LOOP

      /**
		 * This field set the value of 'Is Mailing Address Also Primary
		 * Location' to 'YES' in back-end after appian response
		 */
      for (var question = 0; question < theRes.Business_Address.length; question++) {
        if (theRes.Business_Address[question].systemRefId == 'misc.IsMailingAddressAlsoPrimaryLocation') {
          theRes.Business_Address[question].userValue = 'Yes';
          break;
        }
      } // END of FOR LOOP

      /**
		 * This field set the value of 'Details_Explanation' to
		 * 'vm.detailExplanationSetValue' in back-end after appian response.
		 * Appian will provide tha value which is assign to
		 * 'vm.detailExplanationSetValue' variable. SAME for 'PriorExperience'
		 * record.
		 */
      for (var ques = 0; ques < theRes.Business_Info.length; ques++) {
        if (theRes.Business_Info[ques].systemRefId == 'Details_Explanation') {
          theRes.Business_Info[ques].userValue = vm.detailExplanationSetValue;

        }
        if (theRes.Business_Info[ques].systemRefId == 'misc.UNIG_PriorExperience') {
          theRes.Business_Info[ques].userValue = vm.priorEXPSetValue;
        }
      } // END of FOR LOOP

      /**
		 * Check if user change the YEAR_STARTED then BusinessStatus field
		 * should hide and ngModel is set to null
		 */
      if (vm.businessStatus == false) {
        for (var j1 = 0; j1 < theRes.Business_Info.length; j1++) {
          if (theRes.Business_Info[j1].systemRefId == 'misc.UNIG_BusinessStatus') {
            theRes.Business_Info[j1].userValue = '';
            break;
          }
        }
      }

      /**
		 * This 'suggestionBox' key is added for re-rendering purpose of 1)
		 * magic box 2) magic box button 3) magic box google data 4) magic box
		 * verisk data 5) stop verisk + USPS + Experian
		 */
      theRes['suggestionBox'] = {};
      // for Google Suggestion BOX + magic fill button in buisness location
      theRes['suggestionBox'].googleData                      = vm.googleData;
      // for Longitude
      theRes['suggestionBox'].longitude                       = vm.lat;
      // for Latitude
      theRes['suggestionBox'].latitude                        = vm.lng;
      // for google address
      theRes['suggestionBox'].streetAPTSuite                  = vm.streetAPTSuite;
      theRes['suggestionBox'].cityStateZipcode                = vm.cityStateZipcode;
      theRes['suggestionBox'].country                         = vm.country;
      // for magic fill button in business info
      theRes['suggestionBox'].businessInfoMagicButton         = vm.businessInfoMagicButton;
      // for versik Box
      theRes['suggestionBox'].veriskData                      = vm.veriskData;
      // for verisk business Name
      theRes['suggestionBox'].veriskBusinessName              = vm.veriskBusinessName;
      // for verisk Phone Number
      theRes['suggestionBox'].veriskPhoneNumber               = vm.veriskPhoneNumber;
      // for verisk Annual Sale
      theRes['suggestionBox'].veriskAnnualSales               = vm.veriskAnnualSales;
      // for verisk year
      theRes['suggestionBox'].veriskYearStarted               = vm.veriskYearStarted;
      // for not triggering USPS/Experian on re-rendering
      theRes['suggestionBox'].checkUSPSExperianCallAlreadyTrigger = checkUSPS_ExperianCallAlreadyTrigger;
      // for not triggering verisk on re-rendering
      theRes['suggestionBox'].doNotCallVeriskAgain            = doNotCallVeriskAgain;




      /**
		 * This is the final data that is send to Back-end
		 */
      var data = {
        data: [theRes],
        elementType: ['BUSINESSCLASS'],
        operationType: ['SAVE_BDETAILS_IN_APPIAN']
      };
      // console.log('Final Data :- ', data);

      /**
		 * AJAX call for SAVE
		 */
      businessService.submitBusinessForm(data).then(
        function () {
          // console.log('save response', res.data);
          $window.sessionStorage.setItem('lobdet', 'SAVE');
          util.setPageIndex('app.lob');
          $state.go('app.lob');
        },
        function () {
          // ErrorToast.showError("Save Failed");
        });

    } // End of Save&Continue

  }
})();

(function () {
  'use strict';

  angular
    // building
    .module('quoteUi')
    .controller('BuildingController', BuildingController);

  /** @ngInject */
  function BuildingController(
    $scope,
    $rootScope,
    $uibModal,
    $window,
    $log,
    $state,
    ErrorToast,
    util,
    $location,
    buildingService
  ) {
    var vm = this;

    $rootScope.pageInfo = [{
      currentPage: $location.path(),
      currentPageData: {},
      element: '',
      operation: '',
      subjectType: ''
    }];

    // methods
    vm.getLocationMeta = getLocationMeta;
    vm.protectionClassLookup = protectionClassLookup;
    vm.zipCodeLookUp = zipCodeLookUp;
    vm.stateValidation = stateValidation;

    // invoke
    vm.getLocationMeta();

    /**
	 * Get location form meta
	 */
    function getLocationMeta() {
      var data = {
        elementType: ['RISK'],
        operationType: ['VIEWLOCATION']
      };
      buildingService.getLocationMeta(data).then(
        function (res) {
          vm.formMeta = res.data.LocationsData[0];

          if (res.data.Locationpage[0] == 'true') {
            vm.lobState = res.data.SYQ[0];
          }
          vm.formMeta.LocationAddress = _.sortBy(vm.formMeta.LocationAddress, 'displaySequence');

          var displaySeq = util.displaySeqCount(vm.formMeta.LocationAddress);

          _.each(vm.formMeta.LocationAddress, function (item) {
            item.screenColumns = 12 / displaySeq[item.displaySequence];

            if (item.validationName.indexOf('Required') != -1) {
              item.isRequired = true;
            }
          });
        },
        function (err) {
          ErrorToast.showError(err.detail);
        }
      );
    }

    /**
	 * Protection/Territory lookup
	 * 
	 * @param {*}
	 *            terrData
	 * @param {*}
	 *            locationId
	 */
    function protectionClassLookup(terrData, locationId) {
      var data = {
        elementType: ['RISK'],
        operationType: ['TERRITORY_LOOKUP'],
        data: [terrData],
        locationId: locationId
      };

      buildingService.createService(data).then(
        function () {
          // console.log(res);
        },
        function (err) {
          ErrorToast.showError(err.detail);
        }
      );
    }

    /**
	 * Zip code lookup and fill
	 * 
	 * @param {*}
	 *            zipCode
	 * @param {*}
	 *            locationId
	 */
    function zipCodeLookUp(zipCode, locationId) {
      var data = {
        elementType: ['RISK'],
        operationType: ['ZIPCODELOOKUP'],
        data: [zipCode],
        locationId: locationId
      };

      if (zipCode.length == 5) {
        buildingService.createService(data).then(
          function (res) {
            _.each(res.data, function (item) {
              var obj = _.findWhere(vm.formMeta.LocationAddress, {
                systemRefId: item.systemRefId
              });
              if (item.validationName.indexOf('Required') != -1) {
                item.isRequired = true;
              }
              item.screenColumns = obj.screenColumns;
              if (typeof obj !== 'undefined') {
                obj = item;
              }
            });
          },
          function (err) {
            ErrorToast.showError(err.detail);
          }
        );
      }
    }

    /**
	 * Validate state
	 * 
	 * @param {*}
	 *            state
	 */
    function stateValidation(state, systemRefId) {

      var data = {
        elementType: ['APPIANRULE'],
        operationType: ['BRVALIDATESTATE'],
        data: [{
          lobstate: vm.lobState,
          locstate: state
        }]
      };

      buildingService.createService(data).then(
        function (res) {
          var obj = _.findWhere(res.data, {
            sysRefId: systemRefId
          });

          if (typeof obj !== 'undefined' && obj.error != null) {
            vm.stateErrMsg = obj.error;
          } else {
            vm.stateErrMsg = null;
          }
        },
        function (err) {
          ErrorToast.showError(err.detail);
        }
      );
    }
  }
})();

(function () {
  'use strict';

  angular
    // beforeyouissue
    .module('quoteUi')
    .controller('BeforeYouIssueController', BeforeYouIssueController)
    .controller('issuepolicyCtrl', issuepolicyCtrl)
    .filter('camelCase', function () {
      // Error message label should be camelCase format
      return function (input) {
        input = input || '';
        return input.replace(/\w\S*/g, function (txt) {
          return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
      };
    }).filter('reverse', function () {
      return function (items) {
        return items.slice().reverse();
      };
    });

  function issuepolicyCtrl($uibModalInstance, $state, util) {
    var $ctrl = this;
    $ctrl.imagePath = util.getImagePath();
    $ctrl.completepolicy = function () {
      $ctrl.ok();
      $state.go('accountview');
    };
    $ctrl.ok = function () {
      $uibModalInstance.close();
    };
  }

  /** @ngInject */
  function BeforeYouIssueController(
    $http,
    $state,
    $window,
    $rootScope,
    $scope,
    $timeout,
    Validator,
    $log,
    beforeyouissueService,
    $location,
    $uibModal
  ) {
    var vm = this;

    vm.triggerSubmit = triggerSubmit;
    vm.getFormData = getFormData;
    vm.parse = parse;
    vm.onRadioBtnChange = onRadioBtnChange;
    vm.addNewInsuredFunction = addNewInsuredFunction;
    vm.removeNewInsuredFunction = removeNewInsuredFunction;
    vm.openComponentModal = openComponentModal;
    vm.openComponentModalforquote = openComponentModalforquote;
    vm.defaultInfo = defaultInfo;
    vm.checkZipLessThan5 = checkZipLessThan5;
    vm.zipCodeLookUP = zipCodeLookUP;
    vm.checkAppian = checkAppian;
    vm.emailValidation = emailValidation;
    vm.maskCurrency = maskCurrency;
    vm.setDefaultValue = setDefaultValue;
    vm.setDefaultValueNameInsuredArray = setDefaultValueNameInsuredArray;
    vm.checkIfNameInsuredEdited=checkIfNameInsuredEdited;
    vm.savePrimary = savePrimary;
    vm.populateSaveObject = populateSaveObject;
    vm.validateNameInsured = validateNameInsured;
    vm.checkIfPolicyWidgetEdited = checkIfPolicyWidgetEdited;
    vm.checkproducervalue = checkproducervalue;
    vm.selectproductvalue = selectproductvalue;
    vm.phoneLengthErrorMessage = phoneLengthErrorMessage;

    vm.addNewInsureditems = [];
    vm.addNewInsuredArray = [];
    vm.beforeYouIssueFormData = {};
    vm.beforeYouIssueData = [];
    vm.producerdata=[];
    vm.errPrimary=[];
    vm.showMsg;
    vm.saveClicked=[];

    vm.saveDisabled=true; // should be true on page load
    vm.disableProducer= true ;   // should be true on page load
    var nonamedarray=[];
    var selectTypeData;
  // var btnClicked;
    var appianEventData;
    var dataNameInsured ;
    var doingBusinessVar = null;
    vm.invalidPhoneFormat = [];

    // vm.addNewInsured = {
    // beforeYouIssueData: vm.beforeYouIssueData,
    // addNewInsureditems: vm.addNewInsureditems
    // };

    $rootScope.pageInfo = [{
      "currentPage": $location.path(),
      "currentPageData": [vm.beforeYouIssueData],
      "element": ["BEFOREYOUISSUEPOLICY"],
      "operation": ["SAVEANDEXIT"]
    }];
    $rootScope.$on('onsave', function () {
      $rootScope.pageInfo = [{
        "currentPage": $location.path(),
        "currentPageData": [vm.beforeYouIssueData],
        "element": ["BEFOREYOUISSUEPOLICY"],
        "operation": ["SAVEANDEXIT"]
      }];
    });

    vm.getFormData();

    function checkZipLessThan5(data) {
      if (data) {
        var temp = data.toString();
        if (temp.length < 5 && temp.length > 0) {
          return true;
        }
      }
    }

    /**
	 * 
	 * @param {*}
	 *            phoneData
	 * @param {*}
	 *            sysRef Check if user's entered phone is in correct format
	 */
    function phoneLengthErrorMessage(phoneData, sysRef,parent,index) {

      // console.log(phoneData);
      // console.log(sysRef);
      // console.log(parent);
      // console.log(index);

      if (sysRef == 'BUSINESS_PHONE' ||
          sysRef == 'LOSS_CONTACT_PHONE' ||
          sysRef == 'ACCOUNTING_RECORDS_PHONE' ) {

        if (phoneData &&
          (phoneData.replace(/[^\d]/g, '').length > 10 ||
            phoneData.replace(/[^\d]/g, '').length < 10)) {

          vm.invalidPhoneFormat[parent + index] = true;
        } else {
          vm.invalidPhoneFormat[parent + index] = false;
        }
      }

      // console.log(vm.invalidPhoneFormat);

    } // End of phoneLengthErrorMessage function;


    function zipCodeLookUP(arrayInsured, sysRefID, zipCodeLength, ZIPdata) {
    // console.log(arrayInsured, sysRefID, zipCodeLength, ZIPdata);

      if (sysRefID == 'ZIP_CODE' && !zipCodeLength && ZIPdata) {
          var zipCodeAJAXData = {
            elementType: ['BEFOREYOUISSUEPOLICY'],
            operationType: ['ZIPCODE_LOOKUP'],
            section: ['2'],
            data:[ZIPdata]
          };
          // console.log('Zip Code Look UP Sending = ',zipCodeAJAXData);
          beforeyouissueService.zipCodeLookUP(zipCodeAJAXData).then(
            function (res) {
              if (res.data[0].systemRefId != "ZIP_CODE") {
                for (var policyAddressLoop = 0; policyAddressLoop < arrayInsured.length; policyAddressLoop++) {
                  for (var zipCodelookUPLoop = 0; zipCodelookUPLoop < res.data.length; zipCodelookUPLoop++) {
                    if(arrayInsured[policyAddressLoop].systemRefId){
                    if (arrayInsured[policyAddressLoop].systemRefId.toLowerCase() == res.data[zipCodelookUPLoop].systemRefId.toLowerCase()) {
                     arrayInsured[policyAddressLoop].userValue=res.data[zipCodelookUPLoop].userValue;
                    }
                  }
                  } // END OF INNER LOOP
                } // End of outer Loop
                vm.lengthOfZip = false;
              } else {
                vm.lengthOfZip = true;
                for (var policyAddressLoop1 = 0; policyAddressLoop1 < arrayInsured.length; policyAddressLoop1++) {
                  if ((arrayInsured[policyAddressLoop1].systemRefId == 'CITY')||(arrayInsured[policyAddressLoop1].systemRefId == 'STATE') ){
                    arrayInsured[policyAddressLoop1].userValue = null;
                  }
                }
              }
            },

            function () {

                  // var res=[
                  // {
                  // "userValue": "NEW YORK",
                  // "systemRefId": "city"
                  // },
                  // {
                  // "userValue": "NY",
                  // "systemRefId": "State"
                  // }
                  // ];

                  // if (res[0].systemRefId != "mailingAddress.ZIP") {
                  // for (var policyAddressLoop = 0; policyAddressLoop <
					// arrayInsured.length; policyAddressLoop++) {
                  // for (var zipCodelookUPLoop = 0; zipCodelookUPLoop <
					// res.length; zipCodelookUPLoop++) {
                  // if(arrayInsured[policyAddressLoop].systemRefId){
                  // if
					// (arrayInsured[policyAddressLoop].systemRefId.toLowerCase()
					// == res[zipCodelookUPLoop].systemRefId.toLowerCase()) {
                  // arrayInsured[policyAddressLoop].userValue=res[zipCodelookUPLoop].userValue;
                  // }
                  // }
                  // } // END OF INNER LOOP
                  // } // End of outer Loop
                  // vm.lengthOfZip = false;
                  // } else {
                  // vm.lengthOfZip = true;
                  // for (var policyAddressLoop1 = 0; policyAddressLoop1 <
					// arrayInsured.length; policyAddressLoop1++) {
                  // if ((arrayInsured[policyAddressLoop1].systemRefId ==
					// 'CITY')||(arrayInsured[policyAddressLoop1].systemRefId ==
					// 'STATE') ){
                  // arrayInsured[policyAddressLoop1].userValue = null;
                  // }
                  // }
                  // }
            }
          );

      } // If end
    } // End of zipCodeLookUP

    // modalwindow for Issue policy
    function openComponentModal() {
      var value = 'test value';
      var modalInstance = $uibModal.open({
        templateUrl: 'issuepolicy.html',
        controller: 'issuepolicyCtrl',
        controllerAs: '$ctrl',
        windowClass: 'issuepolicypopup',
        size: 'lg',
        backdrop: 'static',
        resolve: {
          param: function () {
            return {
              value: value
            };
          }
        }
      });
      modalInstance.result.then(
        function () {},
        function () {}
      );
    }

    // modalwindow for quoteanotherline
    function openComponentModalforquote() {
      var value = 'test value';
      var modalInstance = $uibModal.open({
        templateUrl: 'quoteanotherline.html',
        controller: 'issuepolicyCtrl',
        controllerAs: '$ctrl',
        windowClass: 'issuepolicypopup',
        size: 'lg',
        backdrop: 'static',
        resolve: {
          param: function () {
            return {
              value: value
            };
          }
        }
      });
      modalInstance.result.then(
        function () {},
        function () {}
      );
    }
    /**
	 * 
	 * @param {*}
	 *            insuredArray
	 * @param {*}
	 *            ngModelCheckBox Appian rule for enabling or diabling text box
	 *            based on checkbox value
	 */
    function checkAppian(insuredArray, ngModelCheckBox) {
      for (var appianLoop = 0; appianLoop < insuredArray.length; appianLoop++) {
        if(insuredArray[appianLoop].systemRefId){
        if ((insuredArray[appianLoop].systemRefId.toLowerCase() == 'same_as_business_information1') || (insuredArray[appianLoop].systemRefId.toLowerCase() == 'same_as_business_information2') || (insuredArray[appianLoop].systemRefId.toLowerCase() == 'same_as_business_information3')) {
          if (insuredArray[appianLoop].eventName) {
            if (Array.isArray(insuredArray[appianLoop].eventName)) {
              if (insuredArray[appianLoop].eventName[0] && insuredArray[appianLoop].eventName[0].indexOf(':') != -1) {
                appianEventData = insuredArray[appianLoop].eventName[0].split(':')[1];
                // break;
              } else {
               // ErrorToast.showError('1. No APPIAN CALL because Event Name is
				// EMPTY / NULL or Not in correct format.');
            // break;
              }
            } else {
              appianEventData = insuredArray[appianLoop].eventName.split(':')[1];
              // break;
            }
          } else {
           // ErrorToast.showError('2. No APPIAN CALL because Event Name is
			// EMPTY / ' + insuredArray[appianLoop].eventName);
            // break;
          }
        } // End of IF condition
      }
    }

      var checkBoxValue;
      if (ngModelCheckBox == true) {
        checkBoxValue = "Yes";   // enable = true , non editable
      } else if (ngModelCheckBox == false || ngModelCheckBox == undefined ) {
        checkBoxValue = "No"; // enable = false , editable
      }

    // console.log(checkBoxValue)

      var dataCheckAppian = {
        elementType: ['APPIANRULE'],
        operationType: [appianEventData],
        var1: [checkBoxValue]
      };

      beforeyouissueService.appianRule(dataCheckAppian).then(function (res) {
          var appianResponse = angular.copy(res.data);
          for (var insuredLoop = 0; insuredLoop < insuredArray.length; insuredLoop++) {
            for (var appianResLoop = 0; appianResLoop < appianResponse.length; appianResLoop++) {
              if (insuredArray[insuredLoop].systemRefId.toLowerCase() == appianResponse[appianResLoop].sysRefId.toLowerCase()) {
                if (appianResponse[appianResLoop].enable == true) { // if enable
																	// is true
																	// then
																	// field
																	// should be
																	// non
																	// editable
                  insuredArray[insuredLoop].editable = 'N';
                } else if (appianResponse[appianResLoop].enable == false) {
                  insuredArray[insuredLoop].editable = 'Y';
                }
              }
            }
          }
        },
        function () {
        });

    }

    /**
	 * EMail validation
	 */

    function emailValidation(systemRefId, ngModelData) {
      if (systemRefId=="BUSINESS_EMAIL") {
        if (!ngModelData.match(
            /([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)\S+/)) {
          vm.busemailErr = true;
        } else {
          vm.busemailErr = false;
        }
      }
      if (systemRefId=='LOSS_CONTACT_EMAIL') {
        if (!ngModelData.match(
            /([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)\S+/)) {
          vm.lossemailErr = true;
        } else {
          vm.lossemailErr = false;
        }
      }
      if (systemRefId=='ACCOUNTING_RECORDS_EMAIL') {
        if (!ngModelData.match(
            /([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)\S+/)) {
          vm.accemailErr = true;
        } else {
          vm.accemailErr = false;
        }
      }
    }

    /**
	 * 
	 * @param {*}
	 *            widgetArray Setting default value for checkbox widgets
	 */
    function setDefaultValue(widgetArray) {
      for (var insuredLoop = 0; insuredLoop < widgetArray.length; insuredLoop++) {
        for (var insuredVar = 0; insuredVar < widgetArray[insuredLoop].length; insuredVar++) {
          if(widgetArray[insuredLoop][insuredVar]){
          if(widgetArray[insuredLoop][insuredVar].controlName){
          if(widgetArray[insuredLoop][insuredVar].controlName!=undefined){
          if (widgetArray[insuredLoop][insuredVar].controlName.toLowerCase() == 'text_box') {
            widgetArray[insuredLoop][insuredVar].defaultValue = widgetArray[insuredLoop][insuredVar].userValue;
          }
          }
        }
      }
      }
      }
    }

/**
 * 
 * @param {*}
 *            nameInsuredArray setting userValue to defaultValue for name
 *            insured widgets for identifying whether it has been edited or not
 */
    function setDefaultValueNameInsuredArray(nameInsuredArray) {
     // console.log("entered in setting default value", nameInsuredArray);
      for (var insuredLoop = 0; insuredLoop < nameInsuredArray.length; insuredLoop++) {
        if (nameInsuredArray[insuredLoop].userValue) {
          nameInsuredArray[insuredLoop].defaultValue = nameInsuredArray[insuredLoop].userValue;
        }
      }
     // console.log("after setting default value", nameInsuredArray);
    }

    /**
	 * 
	 * @param {*}
	 *            insuredArray
	 * @param {*}
	 *            ngModelCheckBox Setting and clearing text box based on
	 *            checkbox vale
	 */
    function defaultInfo(insuredArray, ngModelCheckBox) {
      // console.log(ngModelCheckBox)
      insuredArray[1].defaultValue=!insuredArray[1].defaultValue;
      checkAppian(insuredArray, ngModelCheckBox);

      for (var insuredVar = 0; insuredVar < insuredArray.length; insuredVar++) {
        if(insuredArray[insuredVar].controlName){
        if (insuredArray[insuredVar].controlName.toLowerCase() == 'text_box') {
          if (ngModelCheckBox == true) {
            insuredArray[insuredVar].userValue = insuredArray[insuredVar].defaultValue;
          } else if (ngModelCheckBox == false || ngModelCheckBox == undefined) {
            insuredArray[insuredVar].userValue = "";
          }
        } // end of if text_box
      }
      }
    }

    /**
	 * 
	 * @param {*}
	 *            insuredItemARRAY
	 * @param {*}
	 *            index Delete secondary name insured
	 */
    function removeNewInsuredFunction(insuredItemARRAY, index) {
      var removeWidgetData = {
        elementType: ['BEFOREYOUISSUEPOLICY'],
        operationType: ['DELETE_SECONDARY_NAMED_INSURED'],
        duckId: [insuredItemARRAY[8].DuckId],
        data: [vm.beforeYouIssueData]
      };

        beforeyouissueService.submitBeforeyouissue(removeWidgetData).then(
      function () {
          for (var insuredVar = 0; insuredVar < insuredItemARRAY.length; insuredVar++) {
            insuredItemARRAY[insuredVar].userValue = '';
          }
          vm.addNewInsureditems.splice(index, 1);
          vm.saveDisabled=false;
          var b=nonamedarray[index];
          delete vm.beforeYouIssueData[b];
          nonamedarray.splice(index,1);
        },

        function () {

          for (var insuredVar = 0; insuredVar < insuredItemARRAY.length; insuredVar++) {
            insuredItemARRAY[insuredVar].userValue = '';
          }
          vm.addNewInsureditems.splice(index, 1);
          vm.saveDisabled=false;

        });
    }

/*
 * Add secondary name insured function
 */

    function addNewInsuredFunction() {
      var dataNewWidget = {
        elementType: ['BEFOREYOUISSUEPOLICY'],
        operationType: ['VIEW_SECONDARY_NAMED_INSURED'],
        data: [vm.beforeYouIssueData]
      };

      beforeyouissueService.fetchBeforeyouissueForm(dataNewWidget).then(
        function (response) {
          vm.saveDisabled = true;
          vm.insuredInfoSectionNew = angular.copy(response.data);
          vm.insuredInfoSectionNew.Named_Insured_Info[8].status = 'save'; // data
																			// not
																			// saved
																			// yet
          vm.addNewInsureditems = vm.addNewInsureditems.concat([parse(vm.insuredInfoSectionNew.Named_Insured_Info)]);
          var b=Number(nonamedarray[nonamedarray.length-1].charAt(19))+1;
          var a="Named_Insured_Info_"+b;
          vm.beforeYouIssueData[a]=parse(vm.insuredInfoSectionNew.Named_Insured_Info);
          nonamedarray.push(a);
        },
        function () {

        });
    }

    /**
	 * Appian rule for shwoing follow uo fields on name insured
	 * 
	 */

    function onRadioBtnChange(insuredArray) {

      for (var appianLoop = 0; appianLoop < insuredArray.length; appianLoop++) {

        if (insuredArray[appianLoop].hasOwnProperty("systemRefId") && ((insuredArray[appianLoop].systemRefId.toLowerCase() == 'additionalotherinterestinput.entitytype') ||
            (insuredArray[appianLoop].systemRefId.toLowerCase() == 'additionalotherinterestinput.unig_dba'))) {
          if (insuredArray[appianLoop].systemRefId.toLowerCase() == 'additionalotherinterestinput.entitytype') {
            selectTypeData = insuredArray[appianLoop].userValue;
          }
          if (insuredArray[appianLoop].systemRefId.toLowerCase() == 'additionalotherinterestinput.unig_dba') {
            doingBusinessVar = insuredArray[appianLoop].userValue;
          }
          if (insuredArray[appianLoop].eventName) {
            if (Array.isArray(insuredArray[appianLoop].eventName)) {
              if (insuredArray[appianLoop].eventName[0] &&
                insuredArray[appianLoop].eventName[0].indexOf(':') != -1) {
                appianEventData = insuredArray[appianLoop].eventName[0].split(':')[1];
              } else {
                // comment before deploying
              // ErrorToast.showError('1. No APPIAN CALL because Event Name is
				// EMPTY / NULL or Not in correct format.');
                break;
              }
            } else {
              appianEventData = insuredArray[appianLoop].eventName.split(':')[1];
            }
          } else {
         // ErrorToast.showError('2. No APPIAN CALL because Event Name is
			// EMPTY / ' + insuredArray[appianLoop].eventName);
            // break;
          }
        } // End of IF condition
      } // End of For LOOP

      if (appianEventData && selectTypeData && doingBusinessVar) {

        var appianNameInsuredData = {
          elementType: ['APPIANRULE'],
          operationType: [appianEventData],
          var1: [selectTypeData],
          var2: [doingBusinessVar]
        };

        beforeyouissueService.appianRule(appianNameInsuredData).then(function (res) {
            var appianResponse;
            appianResponse = angular.copy(res.data);
            for (var insuredLoop = 0; insuredLoop < insuredArray.length; insuredLoop++) {
              for (var appianResLoop = 0; appianResLoop < appianResponse.length; appianResLoop++) {
                if (appianResponse[appianResLoop].sysRefId) {
                  if (insuredArray[insuredLoop].systemRefId == appianResponse[appianResLoop].sysRefId) {
                    if (appianResponse[appianResLoop].visible == true) {
                      insuredArray[insuredLoop].appearUI = 'Y';
                    } else if (appianResponse[appianResLoop].visible == false) {
                      insuredArray[insuredLoop].appearUI = 'N';
                    }
                  }
                }
              }
            }
          },

          function () {

          // var res = [{
          // "sysRefId": "AdditionalOtherInterestInput.UNIG_FirstName",
          // "value": null,
          // "error": null,
          // "visible": true,
          // "eligible": null,
          // "enable": null,
          // "warning": null
          // },
          // {
          // "sysRefId": "AdditionalOtherInterestInput.UNIG_MiddleName",
          // "value": null,
          // "error": null,
          // "visible": true,
          // "eligible": null,
          // "enable": null,
          // "warning": null
          // },
          // {
          // "sysRefId": "AdditionalOtherInterestInput.UNIG_LastName",
          // "value": null,
          // "error": null,
          // "visible": true,
          // "eligible": null,
          // "enable": null,
          // "warning": null
          // },
          // {
          // "sysRefId": "Business_Name",
          // "value": null,
          // "error": null,
          // "visible": true,
          // "eligible": null,
          // "enable": null,
          // "warning": null
          // },
          // {
          // "sysRefId": "Assumed_DBA_Name",
          // "value": null,
          // "error": null,
          // "visible": true,
          // "eligible": null,
          // "enable": null,
          // "warning": null
          // },
          // {
          // "sysRefId": "",
          // "value": [
          // "The Insured's Information will be displayed on the policy
			// exactly as you have entered it. Please verify this information is
			// exactly as you would like it to appear."
          // ],
          // "error": null,
          // "visible": false,
          // "eligible": null,
          // "enable": null,
          // "warning": null
          // }
          // ];
          // var appianResponse = angular.copy(res);
          // for (var insuredLoop = 0; insuredLoop < insuredArray.length;
			// insuredLoop++) {
          // for (var appianResLoop = 0; appianResLoop <
			// appianResponse.length; appianResLoop++) {
          // if (appianResponse[appianResLoop].sysRefId) {
          // if (insuredArray[insuredLoop].systemRefId ==
			// appianResponse[appianResLoop].sysRefId) {
          // if (appianResponse[appianResLoop].visible == true) {
          // insuredArray[insuredLoop].appearUI = 'Y';
          // } else if (appianResponse[appianResLoop].visible == false) {
          // insuredArray[insuredLoop].appearUI = 'N';
          // }
          // }
          // }
          // }
          // }

          });
      } //
    }

    /**
	 * 
	 * @param {*}
	 *            response Parse JSon object on page load
	 */
    function parse(response) {

     response= _.sortBy(response, 'displaySequence');

      for (var i = 0; i < response.length; i++) {

        if (typeof response[i].validationName !== 'undefined') {
          var currEntVal = response[i].validationName;
          var isRequired = false;
          var inputType = 'text';

          for (var ai = 0; ai < currEntVal.length; ai++) {
            if (
              currEntVal[ai].toLowerCase() == 'required'
            ) {
              isRequired = true;
            }
            if (currEntVal[ai].toLowerCase() == 'numeric') {
              inputType = 'number';
            }
            if (currEntVal[ai].toLowerCase() == 'currency') {
              inputType = 'text';
            }
            if (currEntVal[ai].toLowerCase() == 'alphanumeric') {
              inputType = 'text';
            }

          }
          if (response[i].controlName.toLowerCase() == 'text_box') {
            response[i].inputType = 'text';
          }
          if (response[i].controlName.toLowerCase() == 'check_box') {
            response[i].inputType = 'checkbox';
          }

          response[i].isRequired = isRequired;
          response[i].inputType = inputType;

        } else {
          response[i].isRequired = false;
          response[i].inputType = 'text';
        }
      }
      return response;
    }

    /**
	 * Convert Number into currency before displaying
	 */

    function maskCurrency(input) {
      var temp1;
      temp1 = input.toString();
      temp1 = temp1.replace(/-/g, '');
      return '$' + temp1.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    }

    /**
	 * Adding secondary name insured into main JSON before saving
	 */
    function populateSaveObject() {
      for (var addNewInsureditemsLoop = 1; addNewInsureditemsLoop  < vm.addNewInsureditems.length; addNewInsureditemsLoop++) {
        var name = "Named_Insured_Info_" + addNewInsureditemsLoop;
        vm.beforeYouIssueData[name] = vm.addNewInsureditems[addNewInsureditemsLoop];
      }
    }

    /**
	 * 
	 * @param {*}
	 *            form Validating Name insured before saving
	 */
    function validateNameInsured(form) {
      for (var arrayIndex = 0; arrayIndex < form.length; arrayIndex++) {
        if ((!form[arrayIndex].controlName) || (form[arrayIndex].controlName.toLowerCase() == 'header')) {
          vm.errPrimary[arrayIndex] = false;
        } else {
          vm.errPrimary[arrayIndex] = true;
        }
        if (form[arrayIndex].controlName) {
          if (form[arrayIndex].controlName.toLowerCase() != 'header') {
            if ((form[arrayIndex].userValue && form[arrayIndex].appearUI.toLowerCase() == 'y') || (form[arrayIndex].appearUI.toLowerCase() == 'n')||(form[arrayIndex].isRequired==false && form[arrayIndex].appearUI.toLowerCase() == 'y')) {
              vm.errPrimary[arrayIndex] = false;
            }
          }
        }
      }
    }
    /**
	 * desc Function for saving name insured
	 */
     vm.ducid='';
    function savePrimary(form, index, sectionIndex) {
      vm.ducid=form[8].DuckId;
      vm.saveClicked[sectionIndex] = true;
      validateNameInsured(form);

      if (_.contains(vm.errPrimary, true)) {
        return false;
      }

      // populateSaveObject();

      if(form[8].status=='save'){
      if (index == 0) {
        dataNameInsured = {
          elementType: ['BEFOREYOUISSUEPOLICY'],
          operationType: ['SAVE_BEFORE_YOU_ISSUE_POLICY'],
          data: [vm.beforeYouIssueData],
          saveComplete: ['false']
        };
      }
      else { // secondary name save
        dataNameInsured = {
          elementType: ['BEFOREYOUISSUEPOLICY'],
          operationType: ['SEC_SAVE_BEFORE_YOU_ISSUE_POLICY'],
          data: [vm.beforeYouIssueData],
          saveComplete: ['secondary']
        };
      }

    // console.log(vm.beforeYouIssueData)
    // console.log("requesting to bckedn",dataNameInsured );

    beforeyouissueService.submitBeforeyouissue(dataNameInsured).then(
      function (response) {
                var producerDropdownInfo = angular.copy(response.data);
                form[8].status='saved';
                setDefaultValueNameInsuredArray(form);
                vm.saveDisabled = false;

                if(producerDropdownInfo.producerDropdown){
                      if (producerDropdownInfo.producerDropdown[0].toLowerCase() == 'true') {
                        vm.disableProducer = false;
                        vm.producerdata[1].valueName = producerDropdownInfo.data;
                        vm.producerdata[1].editable='N';
                      }
                    }

      },

      function () {
      // form[8].status='saved';
      // setDefaultValueNameInsuredArray(form);
      // vm.saveDisabled = false;

      // var response = {"data":["AMATO JR. JOSEPH J.","AMATO SR. JOSEPH
		// R.","AMATO(SR.) JOSEPH R.","BINGER-EVANS TERI","COUGHLIN MICHAEL
		// J.","DOMBROWSKI JENNIFER","DURNIEN NICOLE","FOLEY BRIAN","SIRVENT
		// JOSEPH J."],"producerDropdown":["true"]};
      // var producerDropdownInfo = angular.copy(response);
      // console.log(producerDropdownInfo.data)
      // // enable the dropdown and secondary name insured link
      // if (producerDropdownInfo.producerDropdown[0].toLowerCase() == 'true')
		// {
      // vm.disableProducer = false;
      // vm.producerdata[1].valueName = producerDropdownInfo.data;

            // }



        }
      );
  }
    else if(form[8].status=='saved'){
      checkIfNameInsuredEdited(form);
    }
  }

  /**
	 * 
	 * @param {*}
	 *            form Check If Name insured is edited or not
	 */
    function checkIfNameInsuredEdited(form) {
        for (var insuredLoop = 0; insuredLoop < form.length; insuredLoop++) {
          if (form[insuredLoop].userValue) {
            if ((form[insuredLoop].defaultValue != form[insuredLoop].userValue) && form[insuredLoop].defaultValue) {
              form[8].status = 'edit';
              break;
            }
          }
        }
        //
        if(form[8].status == 'edit'){
          var dataEdited = {
            elementType: ['BEFOREYOUISSUEPOLICY'],
            operationType: ['EDIT_SECONDARY_NAMED_INSURED'],
            duckId: [form[8].DuckId],
            data: [vm.beforeYouIssueData]
          };
           // console.log("requesting backend",dataEdited)
          beforeyouissueService.submitBeforeyouissue(dataEdited).then(
            function () {
              form[8].status='saved';
              setDefaultValueNameInsuredArray(form);
            },
            function () {
          // console.log("error in edit")
            }
          );
        }
    }

   /**
	 * Check if Policy Widget is edited or not
	 */

    function checkIfPolicyWidgetEdited(){
      for(var insuredArrayIndex=2; insuredArrayIndex<vm.addNewInsuredArray[0].length; insuredArrayIndex++){
       if(vm.addNewInsuredArray[0][insuredArrayIndex].userValue && vm.addNewInsuredArray[0][insuredArrayIndex].defaultValue){
          if(vm.addNewInsuredArray[0][insuredArrayIndex].userValue!=vm.addNewInsuredArray[0][insuredArrayIndex].defaultValue||vm.addNewInsuredArray[0][insuredArrayIndex].userValue==undefined){
            vm.addNewInsuredArray[0][9]= {"status":"edit"};
            break;
          }
          else{
            vm.addNewInsuredArray[0][9]= {"status":"notEdited"};
          }
         }
     }
     vm.beforeYouIssueData.Policy_Mailing_Address=vm.addNewInsuredArray[0];

     // for(var insuredArrayIndex=0;
		// insuredArrayIndex<vm.addNewInsuredArray[0].length;
		// insuredArrayIndex++){
     // if(vm.addNewInsuredArray[0][insuredArrayIndex].systemRefId){
     // if(vm.addNewInsuredArray[0][insuredArrayIndex].systemRefId=='Same_as_Business_Information3'){
     // if(vm.addNewInsuredArray[0][insuredArrayIndex].userValue=='false'||vm.addNewInsuredArray[0][insuredArrayIndex].userValue==undefined){
     // vm.addNewInsuredArray[0][9]= {"status":"edit"};
     // }
     // else{
     // vm.addNewInsuredArray[0][9]= {"status":"notEdited"};
     // }
     // }
     // }
     // }
    }

    /**
	 * 
	 * @param {*}
	 *            isValid Issue Policy / Quote Another Line button
	 */
    function triggerSubmit(isValid) {
      checkproducervalue(vm.producerdata);
      vm.submittedbtn=true;
      // console.log(vm.invalidPhoneFormat);
      vm.showPhoneErrorOnSave = vm.invalidPhoneFormat.includes(true);
      if (vm.showPhoneErrorOnSave) {
        return;
      }

        if (!isValid) {
          return false;
        }



        // populateSaveObject();
       // console.log(vm.beforeYouIssueData)

        for(var insuredArrayIndex = 0; insuredArrayIndex < vm.addNewInsureditems.length; insuredArrayIndex++){
          savePrimary(vm.addNewInsureditems[insuredArrayIndex]);
        }

       checkIfPolicyWidgetEdited();

        // if ($(document.activeElement)[0].id == 'issuePolicy') {
        // btnClicked='IP';
        // } else if ($(document.activeElement)[0].id == 'quoteLine') {
        // btnClicked='QP';
        // }

          var data = {
          elementType: ['BEFOREYOUISSUEPOLICY'],
          operationType: ['SAVE_BEFORE_YOU_ISSUE_POLICY'],
           data: [vm.beforeYouIssueData],
           saveComplete : ['true']
        };

      beforeyouissueService.submitBeforeyouissue(data).then(
        function (response) {
      // console.log("response after saving",response);
          var saveResponse = angular.copy(response.data);
          // if(saveResponse.status=='licenseFailed'){// prod dropdown hard
			// stop

          // }
          // else
           if(saveResponse.status=='redirectYes'){ // policy edit repricing
													// change
            $state.go('app.pricingandcoverage');
           }
          else{
            if ($(document.activeElement)[0].id == 'issuePolicy') {
              vm.openComponentModal();
            } else if ($(document.activeElement)[0].id == 'quoteLine') {
              openComponentModalforquote();
            }
          }

        },
        function () {
         // $log.info(err);
        }
      );
    }

    /* start for validating the producerdropdown value */
    function checkproducervalue(producervalue) {
      vm.produceralert = false;
      if (producervalue[1].userValue == 'Not on list') {
        vm.produceralert = true;
      }
    }
    /* end for validating the producerdropdown value */

    /* function for producerdropdown changes */
    function selectproductvalue() {
      vm.produceralert = false;
    }

    /**
	 * Get form meta data
	 */
    function getFormData() {

      var dataPageLoad = {
        elementType: ['BEFOREYOUISSUEPOLICY'],
        operationType: ['VIEW_BEFORE_YOU_ISSUE_POLICY']
      };


      beforeyouissueService.fetchBeforeyouissueForm(dataPageLoad).then(
        function (response) {

          vm.beforeYouIssueData = angular.copy(response.data);
          Object.keys(vm.beforeYouIssueData).forEach(function(key){ if(key.includes("Named_Insured_Info")){ nonamedarray.push(key);} });
          if(nonamedarray.length==1 && vm.beforeYouIssueData.Named_Insured_Info[8].status==undefined){
              vm.beforeYouIssueData.Named_Insured_Info[8].status = 'save';
          }
          vm.insuredInfoSection = parse(vm.beforeYouIssueData.Named_Insured_Info);
          vm.addNewInsureditems = vm.addNewInsureditems.concat([vm.insuredInfoSection]);
          if(nonamedarray.length>1){
            for(var i=1;i<nonamedarray.length;i++){
              vm.addNewInsureditems.push(parse(vm.beforeYouIssueData[nonamedarray[i]]));
            }
          }
          vm.addNewInsuredArray = vm.addNewInsuredArray.concat([parse(vm.beforeYouIssueData.Policy_Mailing_Address)]);
          vm.addNewInsuredArray = vm.addNewInsuredArray.concat([parse(vm.beforeYouIssueData.BUSINESS_ACCOUNT_CONTACT_INFORMATION)]);
          vm.addNewInsuredArray = vm.addNewInsuredArray.concat([parse(vm.beforeYouIssueData.LOSS_CONTROL_CONTACT_INFORMATION)]);
          vm.addNewInsuredArray = vm.addNewInsuredArray.concat([parse(vm.beforeYouIssueData.ACCOUNTING_RECORDS_CONTACT_INFORMATION)]);
          vm.producerdata = parse(vm.beforeYouIssueData.producerdata);
         // console.log(vm.producerdata)
          // vm.premiumValue =
			// vm.maskCurrency(vm.beforeYouIssueData.premiumValue);

          vm.addNewInsuredArray[0][9]={"status":"notEdited"};

          // setDefaultValue(vm.addNewInsuredArray);
          vm.premiumValue = vm.maskCurrency(vm.beforeYouIssueData.premiumValue);
        },
        function () {
          // error block
        }
      );
    }


  }
})();

(function () {
  'use strict';

  angular
    // business details
    .module('quoteUi')
    .controller('BeforeYouBeginController', BeforeYouBeginController);
  // States,

  /** @ngInject */
  function BeforeYouBeginController(
    $state,
    $window,
    ErrorToast,
    $rootScope,
    ParseJSON,
    $scope,
    Conf,
    $timeout,
    toastr,
    Validator,
    $q,
    BeforeYouBeginService,
    $log,
    $location,
    util
  ) {
    var vm = this;

    vm.checkInvalidYear;
    vm.appianData = [];
    vm.flag = '0';
    vm.saleFlag = '0';
    vm.lengthOfZip;
    vm.tempZip;
    vm.emailError;
    vm.businessDetailsForm = {};
    vm.businessData;
    vm.invalidPhoneMsg;
    var now = new Date();
    vm.currentYear = now.getFullYear();
    vm.businessDetailsForm.uuid = [];
    vm.maskAttributes = [];
    vm.dateFormatError = [false, false];
    vm.message = [];
    vm.dataTree = {};
    var temp;
    vm.detailExplanation;
    vm.businessStatus;
    vm.formats = ['MM/dd/yyyy', 'yyyy/MM/dd', 'shortDate'];
    vm.format = vm.formats[0];
    vm.formaterr = false;

    // datepicker can have different configuration
    vm.dateOptions = [];
    vm.message = [];
    vm.dateOptions = {
      dateDisabled: false,
      formatYear: 'yy',
      startingDay: 0,
      showButtonBar: false,
      showWeeks: false,
      'datepicker-mode': 'month',
      'min-mode': 'month'
    };

    vm.opened = [];

    $rootScope['maskOpts' + '0'] = {
      mask: '999-999-9999',
      restrict: 'reject',
      validate: 'true',
      limit: 'true'
    };

    $rootScope.exit = true;

    // methods
    vm.ZIPCodeValidation = ZIPCodeValidation;
    vm.checkLength = checkLength;
    vm.checkZipLessThan5 = checkZipLessThan5;
    vm.maskCurrency = maskCurrency;
    vm.getBusinessDetailsMeta = getBusinessDetailsMeta;
    vm.businessClassValidation = businessClassValidation;
    vm.triggerSubmit = triggerSubmit;
    vm.changePhoneFormat = changePhoneFormat;
    vm.checkAppainForBusinessDetail = checkAppainForBusinessDetail;
    // vm.eligibilityAppianRuleForYearStarted =
	// eligibilityAppianRuleForYearStarted;
    vm.open = open;
    vm.validateDateField = validateDateField;
    vm.keyupevt = keyupevt;
    vm.change = change;
    $rootScope.pageInfo = [{
      currentPage: $location.path(),
      currentPageData: [],
      element: [],
      operation: []
    }];

    vm.getBusinessDetailsMeta();

    /**
	 * Get byb form
	 */
    function getBusinessDetailsMeta() {
      var data = {
        elementType: ['BEFOREYOUBEGIN'],
        operationType: ['VIEWBEFOREYOUBEGIN']
      };

      BeforeYouBeginService.fetchBusinessForm(data).then(
        function (res) {
          // sort
          vm.bybMeta = res.data;
          vm.bybMeta.agent_Info = _.sortBy(
            vm.bybMeta.agent_Info,
            'displaySequence'
          );
          vm.bybMeta.insured_Info = _.sortBy(
            vm.bybMeta.insured_Info,
            'displaySequence'
          );
          // agent info
          _.each(vm.bybMeta.agent_Info, function (item) {
            var displaySeq = util.displaySeqCount(vm.bybMeta.agent_Info);
            item.screenColumns = 12 / displaySeq[item.displaySequence];
            if (item.validationName.indexOf('Required') != -1) {
              item.isRequired = true;
            }
            if (item.controlName == 'Header') {
              vm.agentInfoHdr = item;
            }
          });
          // insured info
          _.each(vm.bybMeta.insured_Info, function (item) {
            var insuredDisplaySeq = util.displaySeqCount(
              vm.bybMeta.insured_Info
            );
            item.screenColumns = 12 / insuredDisplaySeq[item.displaySequence];
            if (item.validationName.indexOf('Required') != -1) {
              item.isRequired = true;
            }
            if (item.controlName == 'Header') {
              vm.binfoHdr = item;
            }
          });
        },
        function (err) {
          ErrorToast.showError(err.detail);
        }
      );
    }

    vm.checkvalid = function (phoneData) {
      if (phoneData &&
        (phoneData.replace(/[^\d]/g, '').length > 10 ||
          phoneData.replace(/[^\d]/g, '').length < 10)) {

        vm.invalidPhone = true;
      } else {
        vm.invalidPhone = false;
      }
    };

    vm.checkvalidemail = function (phoneData) {
    if(phoneData != undefined){

      if (!phoneData.match(
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        )) {
          console.log("regexp matching vm.emailErr = true");
        vm.emailErr = true;
      } else {
        vm.emailErr = false;
      }

      var domain = phoneData.substring(phoneData.lastIndexOf('.') + 1);
      // console.log("this is the domain etxc", domain, domain.length);
      if (domain.length > 6) {
        vm.emailErr = true;
        console.log("domain greater than 6 vm.emailErr = true");
      } // else {
        // vm.emailErr = false;
      // }
    }
    else{
      vm.emailErr = false;
    }
    };

    /**
	 * Submit before you begin form
	 * 
	 * @param {*}
	 *            isValid
	 */
    function triggerSubmit(isValid) {
      if (vm.dateFormatError == true) {
        vm.dateError = 1;
      }
      if (vm.invalidPhone == true) {
        vm.phoneError = 1;
      }
      if (vm.invalidPhone) {
        vm.invalidPhoneMsg = 1;
      } else {
        vm.invalidPhoneMsg = 0;
      }
      if (vm.emailErr == true) {
        vm.emailError = 1;
      } else {
        vm.emailError = 0;
      }
      if (vm.emailError == 1 || vm.invalidPhoneMsg == 1 || vm.dateError == 1) {
        return;
      }


      if (isValid) {
        var inData = angular.copy(vm.bybMeta);
        var date = _.findWhere(inData.insured_Info, {
          systemRefId: 'misc.EffectiveDate'
        });
        if (typeof date !== 'undefined') {
          date.userValue = moment.parseZone(date.userValue).format('MM/DD/YYYY');
        }
        var data = {
          data: [inData],
          elementType: ['BEFOREYOUBEGIN'],
          operationType: ['SAVEBEFOREYOUBEGINSERVICE']
        };
        BeforeYouBeginService.submitForm(data).then(
          function () {
            util.setPageIndex('app.businessdetails');
            $state.go('app.businessdetails');
          },
          function (err) {
            ErrorToast.showError(err.detail);
          }
        );
      }
    }

    /**
	 * Validate phone/email
	 * 
	 * @param {*}
	 *            phoneData
	 * @param {*}
	 *            sysRef
	 */
    function changePhoneFormat(phoneData, sysRef) {
      console.log(sysRef);
      if (sysRef == 'NewManuScript.AgencyContactEmailAcct') {
        if (!phoneData.match(
          /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/
          )) {
            console.log("regexp matching");
          vm.emailErr = true;
        } else {
          vm.emailErr = false;
        }
      }
      if (sysRef == 'NewManuScript.AgencyContactPhoneNumberAcct') {
        if (!phoneData.match(/^(\()?\d{3}(\))?(-|\s)?\d{3}(-|\s)\d{4}$/)) {
          vm.invalidPhone = true;
        } else {
          vm.invalidPhone = false;
        }
        if (
          sysRef == 'NewManuScript.AgencyContactPhoneNumberAcct' &&
          phoneData &&
          phoneData.indexOf('-') == -1 &&
          phoneData.length == 10
        ) {
          vm.invalidPhone = false;
          return (
            phoneData.substr(0, 3) +
            '-' +
            phoneData.substr(3, 3) +
            '-' +
            phoneData.substr(6)
          );
        }

        return phoneData;
      }
      return phoneData;
    }

    /**
	 * desc
	 * 
	 * @param {*}
	 *            index
	 */
    function open(index) {
      $timeout(function () {
        vm.opened[index] = true;
      });
    }

    function change(data) {
      if (data) {
        for (var i = 0; i < vm.businessAddress.length; i++) {
          if (
            vm.businessAddress[i].systemRefId ==
            'NewManuScript.ProducerIDDropDown'
          ) {
            for (var j = 0; j < vm.businessAddress[i].valueName.length; j++) {
              if (
                vm.businessAddress[i].valueName[j].caption == data[i].userValue
              ) {
                data[i].userValue = vm.businessAddress[i].valueName[j].value;
                break;
              }
            }
          }
        }
      }
    }

    /**
	 * ZIP code validation - make zip code field only contains number.No
	 * character and NO special charcater
	 * 
	 * @param {*}
	 *            $event
	 * @param {*}
	 *            sysRef
	 */
    function ZIPCodeValidation($event, sysRef) {
      if (
        $event.keyCode == 65 ||
        $event.keyCode == 90 ||
        $event.keyCode == 67 ||
        $event.keyCode == 86 ||
        $event.keyCode == 88
      ) {
        $event.preventDefault();
      }
      if (
        $event.keyCode != 17 &&
        $event.keyCode != 65 &&
        $event.keyCode != 90 &&
        $event.keyCode != 67 &&
        $event.keyCode != 86 &&
        $event.keyCode != 88
      ) {
        if (
          ($event.keyCode >= 32 && $event.keyCode < 35) ||
          ($event.keyCode == 38 || $event.keyCode == 40) ||
          ($event.keyCode >= 41 && $event.keyCode < 46) ||
          ($event.keyCode < 96 && $event.keyCode > 105) ||
          ($event.keyCode >= 65 && $event.keyCode <= 90) ||
          ($event.keyCode > 105 && $event.keyCode != 116)
        ) {
          if (
            sysRef &&
            sysRef == 'Annual_Sales' &&
            ($event.keyCode == 109 ||
              $event.keyCode == 189 ||
              $event.keyCode == 190 ||
              $event.keyCode == 110)
          ) {
            $log.info();
          } else {
            $event.preventDefault();
          }
        }
      }

      switch ($event.key) {
        case '!':
        case '@':
        case '#':
        case '$':
        case '%':
        case '^':
        case '&':
        case '*':
        case '(':
        case ')':
        case '_':
          $event.preventDefault();
          break;
      }
    }

    /**
	 * Make year upto four char
	 * 
	 * @param {*}
	 *            $event
	 * @param {*}
	 *            data
	 * @param {*}
	 *            fieldName
	 */

    /**
	 * desc
	 * 
	 * @param {*}
	 *            data
	 */
    function checkZipLessThan5(data) {
      if (data) {
        temp = data.toString();
        if (temp.length < 5 && temp.length > 0) {
          return true;
        }
      }
      return false;
    }

    /**
	 * Mask currency
	 * 
	 * @param {*}
	 *            input
	 */

    function maskCurrency(input) {
      if (isNaN(input)) {
        temp = input.toString();
        if (temp.indexOf('$') == 0 && temp.length == 1) {
          vm.errorMessageAnnualSale = false;
          return;
        }

        if (
          (temp.indexOf('-') == 0 && temp.length == 1) ||
          (temp.match(/-/g) || []).length > 1 ||
          temp.indexOf('-', 1) !== -1 ||
          temp == '$-'
        ) {
          vm.errorMessageAnnualSale = true;
          return input;
        }
        if (temp.indexOf('$-') !== -1 && temp.length > 2) {
          vm.errorMessageAnnualSale = false;
        }
        if (
          (temp.indexOf('.') == 0 && temp.length == 1) ||
          (temp.match(/\./g) || []).length > 1 ||
          (temp == '-.' && temp.length == 2)
        ) {
          vm.errorMessageAnnualSale = true;
          return input;
        }
        vm.errorMessageAnnualSale = false;

        input = input.toString().replace(/,/g, '');
        input = input.toString().replace(/\$/g, '');
        input = parseFloat(input);
        input = input.toFixed(input % 1 === 0 ? 0 : 2);
        if (input.includes('-')) {
          input = input.toString().replace(/-/g, '');
          return '-$' + input.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        }
        return '$' + input.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
      }

      input = parseFloat(input);
      input = input.toFixed(input % 1 === 0 ? 0 : 2);
      return '$' + input.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    }

    function keyupevt(index, event) {
      vm.val = event.target.value;
      validateDateField(index, vm.val);
    }

    function validateDateField(index, value) {
      if (value !== '' && !moment(value, 'MM/DD/YYYY').isValid()) {
        vm.dateFormatError[index] = true;
        vm.message[index] = value + ' is not a valid date';
      } else {
        vm.message[index] = null;
        vm.dateFormatError[index] = false;
      }
    }

    function checkLength($event, data, fieldName) {
      if (typeof data === 'undefined') {
        return;
      }
      if (fieldName == 'NewManuScript.AgencyContactEmailAcct') {
        vm.emailErr = false;

        if (data && data.length == 50) {
          if ($event.keyCode == 8) {
            return;
          }
          console.log("this is the adta",data);
          $event.preventDefault();
        }

        if (!data.match(
            /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
          )) {
          vm.emailErr = true;
        } else {
          vm.emailErr = false;
        }
         // if (data = '') {
          // console.log("no data");
          // vm.emailErr = false;
         // }
      }
      if (fieldName == 'NewManuScript.AgencyContactPhoneNumberAcct') {
        var text = $event.target;
        var selectText = text.value.substr(
          text.selectionStart,
          text.selectionEnd - text.selectionStart
        );

        console.log($event.keyCode);

        if (selectText) {
          return;
        }

        if (data && data.length == 10) {
          if (
            $event.keyCode == 8 ||
            $event.keyCode == 9 ||
            $event.keyCode == 46 ||
            $event.keyCode == 37 ||
            $event.keyCode == 39
          ) {
            return;
          }
          console.log(data.length);
          $event.preventDefault();
        }

        if (data && data.toString().length > 11 && $event.keyCode != 8) {
          if (
            data.toString().replace(/[^-]/g, '').length > 0 &&
            $event.key == '-'
          ) {
            $event.preventDefault();
          }
        }

        if (data && data.toString().length > 10 && $event.keyCode != 8) {
          if (
            data.toString().replace(/[^-]/g, '').length > 1 &&
            $event.key == '-'
          ) {
            $event.preventDefault();
          }
        }
        if (data && data.toString().length < 10 && $event.keyCode != 8) {
          if (
            data.toString().replace(/[^-]/g, '').length > 1 &&
            $event.key == '-'
          ) {
            $event.preventDefault();
          }
        }

        if ($event.key == '_') {
          $event.preventDefault();
        }

        if (
          ($event.keyCode != 9 &&
            $event.keyCode != 8 &&
            ($event.keyCode >= 32 && $event.keyCode < 35)) ||
          ($event.keyCode == 38 || $event.keyCode == 40) ||
          ($event.keyCode >= 41 && $event.keyCode < 46) ||
          ($event.keyCode < 96 && $event.keyCode > 105) ||
          ($event.keyCode >= 65 && $event.keyCode <= 90) ||
          ($event.keyCode > 105 && $event.keyCode != 116)
        ) {
          if (
            $event.keyCode == 109 ||
            $event.keyCode == 189 ||
            $event.keyCode == 110 ||
            $event.keyCode == 173
          ) {
            return;
          } else {
            $event.preventDefault();
          }
        }

        switch ($event.key) {
          case '!':
          case '@':
          case '#':
          case '$':
          case '%':
          case '^':
          case '&':
          case '*':
          case '(':
          case ')':
          case '_':
          case '.':
          case '>':
            $event.preventDefault();
            break;
        }
      }
    }

    /**
	 * Business class validation
	 * 
	 * @param {*}
	 *            businessClassData
	 */
    function businessClassValidation(businessClassData) {
      if (vm.businessClassAndCode.indexOf(businessClassData) == -1) {
        vm.noResults = true;
      } else {
        vm.noResults = false;
      }
    }

    /**
	 * Check which widget in business Detail Page haveappianRule
	 * 
	 * @param {*}
	 *            checkAppianRule
	 */
    function checkAppainForBusinessDetail(checkAppianRule) {
      var tempAppianRule = [];
      for (var i = 0; i < checkAppianRule.length; i++) {
        if (checkAppianRule[i].eventName != null) {
          tempAppianRule.push(checkAppianRule[i]);
        }
      }

      if (tempAppianRule.length != 0) {
        var arrayData = JSON.stringify(tempAppianRule);
        var arrayJOSN = JSON.parse(arrayData);
        return arrayJOSN;
      } else {
        return false;
      }
    }


  }
})();

(function () {
  'use strict';

  angular
    .module('quoteUi')
    .controller('AccountController', AccountController)
    .config(function (IdleProvider, KeepaliveProvider) {
     // IdleProvider.idle(1770);
      IdleProvider.idle(7170);
      IdleProvider.timeout(30);
      KeepaliveProvider.interval(10);
    });

  /** @ngInject */

  function AccountController($scope, util, accountsService, $state, $rootScope, Idle, Keepalive, $uibModal) {

    var vm = this;
    vm.searchInput = '';
    vm.noSearchResult = false;
    vm.sortedColumn = '';
    vm.sortedAccountNameDesc = false;
    vm.sortedAccountIDDesc = false;
    vm.sortedAddressDesc = false;

    vm.searchType = 'Account Name';

    vm.assetPath = util.getImagePath();
    var dash = [];

    vm.pageContent = dash[0];
    vm.pageContent = [];

    vm.handofurl = '/DSCIQuote/outbound?landingPageInCE=My_Dashboard_page&lob=CarrierBusinessOwners';

    getBusinessDetailsMeta();

    function getBusinessDetailsMeta() {
      var data = {
        elementType: ['ACCOUNT'],
        operationType: ['VIEWACCOUNT']
      };

      accountsService.fetchAccountData(data).then(
        function (res) {
          vm.tableData =
            typeof res.data.Accounts !== 'undefined' ? res.data.Accounts : [];
          $scope.loadMore();
          if(res.data.Accounts.length == 0){
            vm.noresult = true;
          }
          vm.noSearchResult = false;
        },
        function (err) {
          if (err.status == 401) {
            $state.go('accountview');
          }
        }
      );
    }

    vm.clearsrch = function () {
      $state.reload();
    };

    vm.srchError = false;
    vm.searchTable = function () {
      if (vm.searchInput.trim() !== '') {
        vm.pageContent.tableDataCopyTemp = [];
        var searchText = vm.searchInput.trim();
        vm.tableData.forEach(function (item) {
          switch (vm.searchType) {
            case 'Account Name':
              if (
                item.ClientName.toLowerCase().indexOf(
                  searchText.toLowerCase()
                ) > -1
              ) {
                vm.pageContent.tableDataCopyTemp.push(item);
              }
              break;
            case 'Account ID':
              if (
                item.ClientId.toLowerCase().indexOf(searchText.toLowerCase()) >
                -1
              ) {
                vm.pageContent.tableDataCopyTemp.push(item);
              }
              break;
            case 'Address':
              if (
                item.Address1.toLowerCase().indexOf(searchText.toLowerCase()) >
                -1 ||
                item.Address2.toLowerCase().indexOf(searchText.toLowerCase()) >
                -1 ||
                item.City.toLowerCase().indexOf(searchText.toLowerCase()) >
                -1 ||
                item.State.toLowerCase().indexOf(searchText.toLowerCase()) >
                -1 ||
                item.State.toLowerCase().indexOf(searchText.toLowerCase()) >
                -1 ||
                item.Zip.toLowerCase().indexOf(searchText.toLowerCase()) > -1
              ) {
                vm.pageContent.tableDataCopyTemp.push(item);
              }
              break;
            default:
              break;
          }
        });
        vm.bindData = angular.copy(vm.pageContent.tableDataCopyTemp);
      } else {
        $state.reload();
      }
      if (vm.pageContent.tableDataCopyTemp.length > 0) {
        vm.noSearchResult = true;
        vm.noresult = false;
      } else {
        vm.noSearchResult = false;
        vm.noresult = true;
      }
    };
    vm.limitVar = 0;

    $scope.loadMore = function () {
      if (!vm.noSearchResult) {
        vm.limitVar += 10;
        vm.bindData = vm.tableData.slice(0, vm.limitVar);
      } else {
        vm.limitVar += 10;
        vm.bindData = vm.tableDataCopyTemp.slice(0, vm.limitVar);
      }
      vm.noSearchResult = true;
    };

    // click sort arrows
    /*
	 * vm.clickSortArrow = function(columnName) { vm.sortedColumn = columnName;
	 * var sortOrderDescent = false;
	 * 
	 * switch (vm.sortedColumn) { case 'accountName': vm.sortedAccountNameDesc =
	 * !vm.sortedAccountNameDesc; vm.sortedAccountIDDesc = false;
	 * vm.sortedAddressDesc = false;
	 * 
	 * sortOrderDescent = vm.sortedAccountNameDesc; break; case 'accountID':
	 * vm.sortedAccountIDDesc = !vm.sortedAccountIDDesc;
	 * vm.sortedAccountNameDesc = false; vm.sortedAddressDesc = false;
	 * 
	 * sortOrderDescent = vm.sortedAccountIDDesc; break; case 'address':
	 * vm.sortedAddressDesc = !vm.sortedAddressDesc; vm.sortedAccountNameDesc =
	 * false; vm.sortedAccountIDDesc = false;
	 * 
	 * sortOrderDescent = vm.sortedAddressDesc; break; default: break; }
	 * 
	 * var temp;
	 * 
	 * var dataList_temp = angular.copy(vm.pageContent.tableDataCopy); for (var
	 * i = 0; i < dataList_temp.length - 1; i++) { for (var j = i + 1; j <
	 * dataList_temp.length; j++) { if (sortOrderDescent) { if (
	 * dataList_temp[j][vm.sortedColumn].localeCompare(
	 * dataList_temp[i][vm.sortedColumn] ) === -1 ) { temp = dataList_temp[j];
	 * dataList_temp[j] = dataList_temp[i]; dataList_temp[i] = temp; } } else {
	 * if ( dataList_temp[i][vm.sortedColumn].localeCompare(
	 * dataList_temp[j][vm.sortedColumn] ) === -1 ) { temp = dataList_temp[j];
	 * dataList_temp[j] = dataList_temp[i]; dataList_temp[i] = temp; } } } }
	 * 
	 * vm.pageContent.tableDataCopy = angular.copy(dataList_temp); };
	 */

    start();

    // Timeout Starts


    $scope.started = false;
    vm.exitApp = exitApp;


    function closeModals() {
      if ($scope.warning) {
        $scope.warning.close();
        $scope.warning = null;
      }

      if ($scope.timedout) {
        $scope.timedout.close();
        $scope.timedout = null;
      }
    }

    $scope.$on('IdleStart', function () {
      closeModals();

      $scope.warning = $uibModal.open({
        templateUrl: 'warning-dialog.html',
        windowClass: 'modal-warning'
      });
    });

    $scope.$on('IdleEnd', function () {
      closeModals();
      stop();
    });

    $scope.$on('IdleTimeout', function () {
      stop();
      closeModals();
      exitApp();
    });

    function start() {
      closeModals();
      Idle.watch();
      $scope.started = true;
    }

    function stop() {
      closeModals();
      Idle.unwatch();
      $scope.started = false;

    }


    function exitApp() {

      // $('.blue-banner').remove();
      // $('.container').remove();


      // $('#main-wrapper').attr('class', 'col-md-12');
      $('.OmniaheaderContainer').css('display', 'none');
      $('.contents')
        .text('You have successfully logged off.')
        .css({
          'text-align': 'center',
          'margin-top': '25px'
        });
      // .css('margin-top', '25px');

      // var data = {
      // session: ["12345"],
      // elementType: ['GLOBALUI'],
      // operationType: ['LOGOUT']
      // }

      $state.go('loggedout');
    }

    // Timeout Ends
  }
})();

(function () {
  'use strict';
  angular
    //
    .module ('quoteUi')
    .factory ('Validator', Validator);

  Validator.$inject = [];

  function Validator () {
    return {
      triggerValidation: triggerValidation,
    };

    function triggerValidation (form) {
      angular.forEach (form, function (control, name) {
        // Excludes internal angular properties

        if (typeof name === 'string' && name.charAt (0) !== '$') {
          // To display ngMessages
          if (name != 'vm.occupantDetailsForm') {
            control.$setTouched ();
            control.$validate ();
          }

          // Runs each of the registered validators
        }
      });
    }
  }
}) ();

(function () {
  'use strict';
  angular
    // Common utils/configs
    .module ('quoteUi')
    .factory ('util', UtilFactory);

  UtilFactory.$inject = ['$location', '$http', '$q', '$state', '$window'];

  function UtilFactory ($location, $http, $q, $state, $window) {
    var host = $location.host ();
    var url = $location.absUrl ();
    var inDciQuote = url.search ('DSCIQuote');

    return {
      getServiceURL: getServiceURL,
      getEnv: getEnv,
      requestGET: requestGET,
      requestPOST: requestPOST,
      handleResponse: handleResponse,
      getStateList: getStateList,
      getImagePath: getImagePath,
      displaySeqCount: displaySeqCount,
      getAssetPath: getAssetPath,
      getPageIndex: getPageIndex,
      setPageIndex: setPageIndex,
      leapYear: leapYear,
      handlePageAccess: handlePageAccess,
    };

    /**
	 * Get service URL based on running environment Dev -
	 * http://openappkubernetes.eastus.cloudapp.azure.com:30022/ Dev2
	 * -'http://dscidevkubernetes.eastus.cloudapp.azure.com:30022/ QA -
	 * http://dsciwipro.eastus.cloudapp.azure.com UAT -
	 * http://dsciwipro.eastus.cloudapp.azure.com:8080/
	 */
    function getServiceURL () {
      // var host = $location.host();
      var port = $location.port ();
      var protocol = $location.protocol ();
      return protocol + '://' + host + ':' + port + '/DSCIQuote/';
      // return 'http://10.208.18.217:8080/DSCIQuote/';
    }

    /**
	 * Manual env config
	 */
    function getEnv () {
      // local, proto, dev, qa, stage, prod
      return 'dev';
    }

    function handlePageAccess () {
      // setPageIndex('accountview');
      $state.go ('accountview');
      // if (actualIndex > currentIndex) {
      // return false;
      // // change to true if you are skipping flow
      // }
    }

    function leapYear (year) {
      return (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
    }

    function getPageIndex (state) {
      var routes = [
        'app.beforeyoubegin',
        'app.businessdetails',
        'app.lob',
        'app.qualification',
        'app.quote',
        'app.location',
        'app.additionalinterest',
        'app.underwriting',
        'app.pricingandcoverage',
        'app.refer',
        'app.premiumsummary',
        'app.beforeyouissue',
      ];

      return routes.indexOf (state);
    }

    function setPageIndex (state) {
      $window.sessionStorage.setItem ('pageIndex', getPageIndex (state));
    }

    /**
	 * Display sequence count
	 * 
	 * @param {*}
	 *            arr
	 */
    function displaySeqCount (arr) {
      return _.chain (arr).countBy (function (obj) {
        return obj.displaySequence;
      })._wrapped;
    }

    /**
	 * Process $http ajax call - GET
	 * 
	 * @param {*}
	 *            url
	 * @param {*}
	 *            params
	 */
    function requestGET (url, params) {
      return handleResponse (
        $http.get (url, {
          params: params,
        })
      );
    }

    /**
	 * Process $http ajax call - POST
	 * 
	 * @param {*}
	 *            url
	 * @param {*}
	 *            params
	 */
    function requestPOST (url, params) {
      return handleResponse ($http.post (url, params));
    }

    /**
	 * Create promise out of service call
	 * 
	 * @param {*}
	 *            req
	 */
    function handleResponse (req) {
      var deferred = $q.defer ();
      req.then (
        function (result) {
          if (result) {
            if (
              typeof result.data !== 'undefined' &&
              result.data.errorName !== '' &&
              result.data.errorName !== null &&
              typeof result.data.errorName !== 'undefined'
            ) {
              deferred.reject ({
                detail: result.data.errorMessage,
              });
            }
            if (_.has (result.data, 'stackTrace')) {
              deferred.reject ({
                detail: 'Unhandled server error.',
              });
            }
            deferred.resolve (result);
          } else {
            deferred.resolve (null);
          }
        },
        function (error) {
          deferred.reject ({
            detail: error.statusText,
          });
        }
      );

      return deferred.promise;
    }

    /**
	 * return image path
	 */
    function getImagePath () {
      // var host = $location.host();
      if (getEnv () != 'proto') {
        // var host = $location.host();
        return './jsp/assets/images/';
      } else if (inDciQuote != -1) {
        return './jsp/assets/images/';
      } else {
        return 'assets/images/';
      }
    }

    /**
	 * Get json asset path
	 */
    function getAssetPath () {
      if (getEnv () != 'proto' && host != 'localhost') {
        return 'jsp/assets/';
      } else if (inDciQuote != -1) {
        return 'jsp/assets/';
      } else {
        return 'assets/';
      }
    }

    /**
	 * return state list
	 */
    function getStateList () {
      return [
        'Alabama',
        'Alaska',
        'Arizona',
        'Arkansas',
        'California',
        'Colorado',
        'Connecticut',
        'Delaware',
        'Florida',
        'Georgia',
        'Hawaii',
        'Idaho',
        'Illinois',
        'Indiana',
        'Iowa',
        'Kansas',
        'Kentucky',
        'Louisiana',
        'Maine',
        'Maryland',
        'Massachusetts',
        'Michigan',
        'Minnesota',
        'Mississippi',
        'Missouri',
        'Montana',
        'Nebraska',
        'Nevada',
        'New Hampshire',
        'New Jersey',
        'New Mexico',
        'New York',
        'North Dakota',
        'North Carolina',
        'Ohio',
        'Oklahoma',
        'Oregon',
        'Pennsylvania',
        'Rhode Island',
        'South Carolina',
        'South Dakota',
        'Tennessee',
        'Texas',
        'Utah',
        'Vermont',
        'Virginia',
        'Washington',
        'West Virginia',
        'Wisconsin',
        'Wyoming',
      ];
    }
  }
}) ();

(function() {
  'use strict';

  angular
    // underwriting service
    .module('quoteUi')
    .factory('underwritingService', UnderwritingService);

  UnderwritingService.$inject = ['util'];

  function UnderwritingService(util) {
    var _post = util.requestPOST;
    var _get = util.requestGET;
    var env = util.getEnv();
    var serviceURL = util.getServiceURL();

    return {
      getFormMeta: getFormMeta,
      submitUnderwriting: submitUnderwriting,
      fetchUnderwritingForm: fetchUnderwritingForm,
      underwritingAppianRule: underwritingAppianRule,
      policyPremiumAppianCall: policyPremiumAppianCall
    };

    function fetchUnderwritingForm(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'view', data);
      } else {
        return _get(util.getAssetPath() + 'dataset/underwriting.json', null);
      }
    }
    /**
	 * Get form metadata
	 * 
	 * @param {*}
	 *            data
	 */
    function getFormMeta(data) {
      return _get(
        util.getAssetPath() + 'dataset/UnderwritingQuestion.json',
        data
      );
    }

    function underwritingAppianRule(data) {
      return _post(serviceURL + 'view', data);
    }

    function submitUnderwriting(data) {
      return _post(serviceURL + 'create', data);
    }

    function policyPremiumAppianCall(data) {
      return _post(serviceURL + 'create', data);
    }
  }
})();

(function() {
  'use strict';

  angular
    //
    .module('quoteUi')
    .factory('StringAndValidationUtils', StringAndValidationUtils);

  StringAndValidationUtils.$inject = ['$log'];

  function StringAndValidationUtils($log) {
    return {
      test: test
    };

    function test(response) {
      $log.error('test', response);
    }
  }
})();

(function() {
  'use strict';

  angular
    // Global Save & Exit service
    .module('quoteUi')
    .factory('saveandexitService', SaveAndExitService);

  SaveAndExitService.$inject = ['util'];

  function SaveAndExitService(util) {
    var _post = util.requestPOST;
    var serviceURL = util.getServiceURL();

    return {
      submitTransactionData: submitTransactionData
    };

    function submitTransactionData(data) {
      
      return _post(serviceURL + 'create', data);
    }


  }
})();

(function () {
  'use strict';

  angular
    // refer service
    .module('quoteUi')
    .factory('referService', ReferService);

  ReferService.$inject = ['util'];

  function ReferService(util) {
    var _post = util.requestPOST;
    var _get = util.requestGET;
    var env = util.getEnv();
    var serviceURL = util.getServiceURL();

    return {
      fetchForm: fetchForm,
      fetchNameInsured: fetchNameInsured,
      saveNameInsured: saveNameInsured,
      saveRefertoUnderwriter: saveRefertoUnderwriter,
      validateType: validateType,
      deleteNameInsured: deleteNameInsured
    };

    /**
	 * Fetch form meta
	 * 
	 * @param {*}
	 *            data
	 */
    function fetchForm(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'view', data);
      } else {
        return _get(
          util.getAssetPath() + 'dataset/refer-to-underwriter.json',
          data
        );
      }
    }

    function fetchNameInsured(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'view', data);
      } else {
        return _get(
          util.getAssetPath() + 'dataset/additional-named-insured.json',
          data
        );
      }
    }

    function saveNameInsured(data) {
      return _post(serviceURL + 'create', data);
    }

    function saveRefertoUnderwriter(data) {
      return _post(serviceURL + 'create', data);
    }

    function validateType(data) {
      return _post(serviceURL + 'create', data);
    }

    function deleteNameInsured(data) {
      return _post(serviceURL + 'create', data);
    }

  }
})();

(function () {
  'use strict';

  angular
    // LOB service
    .module('quoteUi')
    .factory('quoteService', QuoteService);

  QuoteService.$inject = ['util'];

  function QuoteService(util) {
    var _post = util.requestPOST;
    var _get = util.requestGET;
    var env = util.getEnv();
    var serviceURL = util.getServiceURL();

    return {
      submitLOBData: submitLOBData,
      riskStateRule: riskStateRule,
      dateRule: dateRule,
      fetchLobForm: fetchLobForm
    };

    /**
	 * Process LOB form
	 * 
	 * @param {*}
	 *            data
	 */
    function submitLOBData(data) {
      return _post(serviceURL + 'create', data);
    }

    /**
	 * Process risk state appian rule
	 * 
	 * @param {*}
	 *            data
	 */
    function riskStateRule(data) {
      return _post(serviceURL + 'view', data);
    }

    /**
	 * Process LOB date rule
	 * 
	 * @param {*}
	 *            data
	 */
    function dateRule(data) {
      return _post(serviceURL + 'view', data);
    }

    /**
	 * Fetch LOB form data/meta
	 * 
	 * @param {*}
	 *            data
	 */
    function fetchLobForm(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'view', data);
      } else {
        return _get(util.getAssetPath() + 'dataset/quote.json', null);
      }
    }
  }
})();

(function() {
  'use strict';

  angular
    // qualification service
    .module('quoteUi')
    .factory('qualificationService', QualificationService);

  QualificationService.$inject = ['util'];

  function QualificationService(util) {
    var _post = util.requestPOST;
    var _get = util.requestGET;
    var env = util.getEnv();
    var serviceURL = util.getServiceURL();

    return {
      fetchEligibilityMeta: fetchEligibilityMeta,
      checkAppianRule: checkAppianRule,
      submitEligibilityForm: submitEligibilityForm
    };

    /**
	 * fetch eligibility metadata
	 * 
	 * @param {*}
	 *            data
	 */
    function fetchEligibilityMeta(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'view', data);
      } else {
        return _get(
          util.getAssetPath() + 'dataset/qualification-questions.json',
          null
        );
      }
    }

    /**
	 * Check appian rule
	 * 
	 * @param {*}
	 *            data
	 */
    function checkAppianRule(data) {
      return _post(serviceURL + 'create', data);
    }

    /**
	 * Submit eligibility form
	 * 
	 * @param {*}
	 *            data
	 */
    function submitEligibilityForm(data) {
      return _post(serviceURL + 'create', data);
    }
  }
})();

(function () {
  'use strict';

  angular
    // pricing service
    .module('quoteUi')
    .factory('pricingService', PricingService);

  PricingService.$inject = ['util'];

  function PricingService(util) {
    var _post = util.requestPOST;
    var _get = util.requestGET;
    var env = util.getEnv();
    var serviceURL = util.getServiceURL();

    return {
      submitModalData: submitModalData,
      getUWData: getUWData,
      submitPricingData: submitPricingData,
      getModalData: getModalData,
      getPricingData: getPricingData,
      getAddCoveragesData: getAddCoveragesData,
      getAppianData: getAppianData
    };

    /**
	 * Submit pricing data
	 * 
	 * @param {*}
	 *            data
	 */
    function submitPricingData(data) {
      return _post(serviceURL + 'create', data);
    }


    /**
	 * Submit add coverage popup data
	 * 
	 * @param {*}
	 *            data
	 */
    function submitModalData(data) {
      return _post(serviceURL + 'create', data);
    }
    /**
	 * Appian data
	 * 
	 * @param {*}
	 *            data
	 */
    function getAppianData(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'create', data);
      } else {
        return _get(
          util.getAssetPath() + 'dataset/followup_appian.json',
          null
        );
      }
    }

    /**
	 * Process add coverage popup data
	 * 
	 * @param {*}
	 *            data
	 */
    function getModalData(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'view', data);
      } else {
        return _get(
          util.getAssetPath() + 'dataset/pricing_modaldata.json',
          null
        );
      }
    }

    /**
	 * Process add coverages popup data
	 * 
	 * @param {*}
	 *            data
	 */
    function getUWData(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'view', data);
      } else {
        return _get(
          util.getAssetPath() + 'dataset/coverageUW-PopUp.json',
          null
        );
      }
    }

    /**
	 * Process add coverages popup data
	 * 
	 * @param {*}
	 *            data
	 */
    function getAddCoveragesData(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'view', data);
      } else {
        return _get(
          util.getAssetPath() + 'dataset/pricing_freq_popup.json',
          null
        );
      }
    }
    /**
	 * Get pricing data
	 * 
	 * @param {*}
	 *            data
	 */
    function getPricingData(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'view', data);
      } else {
        return _get(util.getAssetPath() + 'dataset/pricing.json', null);
      }
    }
  }
})();

(function() {
  'use strict';

  angular
    // premium summary service
    .module('quoteUi')
    .factory('premiumsummaryService', premiumsummaryService);

  premiumsummaryService.$inject = ['util'];

  function premiumsummaryService(util) {
    var _post = util.requestPOST;
    var _get = util.requestGET;
    var env = util.getEnv();
    var serviceURL = util.getServiceURL();

    return {
      getPremiumsummaryData: getPremiumsummaryData,
      createProposal: createProposal
    };

    /**
	 * Get pricing data
	 * 
	 * @param {*}
	 *            data
	 */
    function getPremiumsummaryData(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'view', data);
      } else {
        return _get(util.getAssetPath() + 'dataset/premiumsummary.json', null);
      }
    }

    /**
	 * create proposal service
	 * 
	 * @param {*}
	 *            data
	 */
    function createProposal(data) {
      return _post(serviceURL + 'view', data);
    }
  }
})();

// (function(){
// 'use strict';

angular
  //
  .module('quoteUi')
  .factory('ParseJSON', ParseJSON);

ParseJSON.$inject = [];

function ParseJSON() {
  return {
    parse: parse,
    serialize: serialize,
    mapForm: mapForm,
    returnFields: returnFields,
    checkForStaticField: checkForStaticField,
    displaySeq: displaySeq,
    buildingParse: buildingParse,
  };

  /**
	 * desc
	 * 
	 * @param {*}
	 *            source
	 * @param {*}
	 *            staticField
	 */
  function checkForStaticField(source, staticField) {
    if (typeof source[staticField] === 'undefined') {
      source[staticField] = [];
      source[staticField][0] = [
        {
          userValue: 'test value',
        },
      ];
    }
  }

  /**
	 * desc
	 * 
	 * @param {*}
	 *            source
	 * @param {*}
	 *            form
	 */
  function returnFields(source, form) {
    // iterate the GROUP FIELDS
    for (var i = 0 in source) {
      if (source[i][0] != null) {
        var currentNode = source[i];
        for (var a = 0; a < currentNode.length; a++) {
          var item = currentNode[a];
          if (typeof form[item.uuid] !== 'undefined') {
            item.userValue = stringify(form[item.uuid], item.controlName);
            // <<<<<<< HEAD
            // } else {
            // item.userValue = '';
            // =======
            // >>>>>>> UAT-Tranche2-P2
          }
        }
      }
    }
    return source;
  }

  /**
	 * stub function for converting DATE to string etc.
	 * 
	 * @param {*}
	 *            inputVal
	 * @param {*}
	 *            controlName
	 */
  function stringify(inputVal, controlName) {
    var current = inputVal;
    if (controlName == 'Date') {
      return dateToString(current);
    }
    return current;
  }

  /**
	 * desc
	 */
  function mapForm() {
    //
  }

  /**
	 * desc
	 * 
	 * @param {*}
	 *            obj
	 */
  function serialize(obj) {
    if (typeof obj === 'object') {
      var res = Object.keys(obj)
        // iterate over them and generate the array
        .map(function(k) {
          // generate the array element
          return [+k, obj[k]];
        });
      return res;
    } else {
      return obj;
    }
  }

  /**
	 * desc
	 * 
	 * @param {*}
	 *            response
	 */
  function displaySeq(response) {
    for (var j = 0; j < response.length; j++) {
      if (typeof response[j].locationName === 'undefined') {
        response[j].locationName = [];
      }

      if (typeof response[j].LocationAddress === 'undefined') {
        var input = response[j].occupantQuestions;
      } else {
        var input = response[j].LocationAddress;
      }

      // var input = response[j].LocationAddress;
      input.sort(fieldSorter(['displaySequence', 'id']));
      var occurrences = {};
      for (var a = 0; a < input.length; a++) {
        if (!occurrences[input[a].displaySequence]) {
          occurrences[input[a].displaySequence] = 1;
        } else {
          occurrences[input[a].displaySequence]++;
        }
        if (
          typeof input[a].uuid === 'undefined' &&
          input[a].controlName !== 'Header'
        ) {
          input[a].uuid = generateUUID();
        }
        if (
          typeof input[a].userValue == 'undefined' &&
          input[a].controlName !== 'Header'
        ) {
          input[a].userValue = null;
        }
      }

      for (var i = 0; i < input.length; i++) {
        var currentTotal = occurrences[input[i].displaySequence];

        switch (currentTotal) {
          case 1:
            input[i].screenColumns = 12;
            break;
          case 2:
            input[i].screenColumns = 6;
            break;
          case 3:
            input[i].screenColumns = 4;
            break;
          case 4:
            input[i].screenColumns = 3;
            break;
          default:
            input[i].screenColumns = 1;
            break;
        }

        // adds a value if this is the last element of the row
        // this way we can display a CLEARFIX or stfff like that
        if (typeof input[i + 1] !== 'undefined') {
          if (input[i].displaySequence != input[i + 1].displaySequence) {
            input[i].hasRowBreak = true;
          } else {
            input[i].hasRowBreak = false;
          }
        }
      }

      // make field Editable or not
      for (var el = 0; el < input.length; el++) {
        if (
          input[el].editable != null &&
          input[el].editable != undefined &&
          input[el].editable == 'N'
        ) {
          input[el].nonEditableFeild = true;
        } else {
          input[el].nonEditableFeild = false;
        }
      }

      for (var l = 0; l < input.length; l++) {
        if (typeof input[l].validationName !== 'undefined') {
          var currEntVal = input[l].validationName;
          var isRequired = false;
          var inputType = 'text';
          for (var ak = 0; ak < currEntVal.length; ak++) {
            if (currEntVal[ak] == 'Required') {
              isRequired = true;
            }
            if (currEntVal[ak] == 'Numeric') {
              inputType = 'number';
            }
            if (currEntVal[ak] == 'Currency') {
              input[l].controlName = 'Currency';
              inputType = 'text';
            }
          }
          input[l].isRequired = isRequired;
          input[l].inputType = inputType;
        } else {
          input[l].isRequired = false;
          input[l].inputType = 'text';
        }
      }
    }
    return response;
  } // End of displaySeq Function

  /**
	 * desc buildingParse
	 * 
	 * @param {*}
	 *            response
	 * @param {*}
	 *            formModel
	 * @param {*}
	 *            maskAttrs
	 */
  function buildingParse(response, formModel, maskAttrs) {
    for (var j = 0; j < response.length; j++) {
      var input = response[j].BuildingQuestions;
      input.sort(fieldSorter(['displaySequence', 'id']));
      var occurrences = {};

      for (var ak = 0; ak < input.length; ak++) {
        // checks for duplicates in valueName arrays
        if (input[ak].valueName) {
          if (input[ak].valueName instanceof Array) {
            input[ak].valueName = uniq(input[ak].valueName);
          }
        }

        // adds UI-MASK
        if (input[ak].datasetName === 'ANNUAL SALES') {
          input[ak].stringMask = '999-999-9999';
        }

        if (
          typeof input[ak].uuid === 'undefined' &&
          input[ak].controlName !== 'Header'
        ) {
          input[ak].uuid = generateUUID();
        }

        if (
          typeof input[ak].userValue !== 'undefined' &&
          input[ak].controlName !== 'Header'
        ) {
          if (input[ak].controlName == 'Date') {
            formModel[input[ak].uuid] = stringToDate(
              input[ak].userValue,
              'mm/dd/yyyy',
              '/'
            );
          } else {
            formModel[input[ak].uuid] = input[ak].userValue;
          }
        } else {
          if (input[ak].controlName !== 'Header') {
            input[ak].userValue = null;
            formModel[input[ak].uuid] = null;
          }
        }
      }

      for (var a = 0; a < input.length; a++) {
        if (!occurrences[input[a].displaySequence]) {
          occurrences[input[a].displaySequence] = 1;
        } else {
          occurrences[input[a].displaySequence]++;
        }
      }

      for (var i = 0; i < input.length; i++) {
        var currentTotal = occurrences[input[i].displaySequence];

        switch (currentTotal) {
          case 1:
            input[i].screenColumns = 12;
            break;
          case 2:
            input[i].screenColumns = 6;
            break;
          case 3:
            input[i].screenColumns = 4;
            break;
          case 4:
            input[i].screenColumns = 3;
            break;
          default:
            input[i].screenColumns = 1;
            break;
        }
        // adds a value if this is the last element of the row
        // this way we can display a CLEARFIX or stfff like that
        if (typeof input[i + 1] !== 'undefined') {
          if (input[i].displaySequence != input[i + 1].displaySequence) {
            input[i].hasRowBreak = true;
          } else {
            input[i].hasRowBreak = false;
          }
        }
      }

      // make field Editable or not
      for (var ebl = 0; ebl < input.length; ebl++) {
        if (input[ebl].editable && input[ebl].editable == 'N') {
          input[ebl].nonEditableFeild = true;
        } else {
          input[ebl].nonEditableFeild = false;
        }
      }

      for (var l = 0; l < input.length; l++) {
        if (typeof input[l].validationName !== 'undefined') {
          var currEntVal = input[l].validationName;
          var isRequired = false;
          var inputType = 'text';

          for (var az = 0; az < currEntVal.length; az++) {
            if (currEntVal[az] == 'Required') {
              isRequired = true;
            }
            if (currEntVal[az] == 'Numeric') {
              inputType = 'number';
            }
            if (currEntVal[az] == 'Currency') {
              input[l].controlName = 'Currency';
              inputType = 'text';
            }
          }
          input[l].isRequired = isRequired;
          input[l].inputType = inputType;
        } else {
          input[l].isRequired = false;
          input[l].inputType = 'text';
        }
        if (typeof input[l].maskOpts === 'undefined') {
          input[l].maskOpts = {
            mask: '*',
            restrict: 'accept',
            validate: 'false',
            limit: 'false',
          };
          input[l].stringMask = '@';
        }
        if (typeof maskAttrs !== 'undefined') {
          maskAttrs[input[l].uuid] = angular.copy(input[l].maskOpts);
        }
      }
    }
    return response;
  }

  /**
	 * desc
	 * 
	 * @param {*}
	 *            response
	 * @param {*}
	 *            formModel
	 * @param {*}
	 *            maskAttrs
	 */
  function parse(response, formModel, maskAttrs) {
    var input = response;
    // sort the fields
    if (input != undefined) {
      input.sort(fieldSorter(['displaySequence', 'id']));
      // count the occurrences of each element
      // if multiple elements share the same displaySequence they are in the
		// same row of the form
      var occurrences = {};
      for (var a = 0; a < input.length; a++) {
        // checks for duplicates in valueName arrays
        if (input[a].valueName) {
          if (input[a].valueName instanceof Array) {
            input[a].valueName = uniq(input[a].valueName);
          }
        }

        // adds UI-MASK
        if (input[a].datasetName === 'ANNUAL SALES') {
          input[a].stringMask = '999-999-9999';
        }

        if (
          typeof input[a].uuid === 'undefined' &&
          input[a].controlName !== 'Header'
        ) {
          input[a].uuid = generateUUID();
        }

        if (typeof input[a].userValue !== 'undefined') {
          if (input[a].controlName == 'Date') {
            formModel[input[a].uuid] = stringToDate(
              input[a].userValue,
              'mm/dd/yyyy',
              '/'
            );
          } else {
            formModel[input[a].uuid] = input[a].userValue;
          }
        } else {
          if (input[a].controlName !== 'Header') {
            input[a].userValue = '';
            formModel[input[a].uuid] = '';
          }
        }
      }
      for (var aj = 0; aj < input.length; aj++) {
        if (!occurrences[input[aj].displaySequence]) {
          occurrences[input[aj].displaySequence] = 1;
        } else {
          occurrences[input[aj].displaySequence]++;
        }
      }
      for (var i = 0; i < input.length; i++) {
        var currentTotal = occurrences[input[i].displaySequence];
        // console.log("currentTotal = ",input);
        switch (currentTotal) {
          case 1:
            input[i].screenColumns = 12;
            break;
          case 2:
            input[i].screenColumns = 6;
            break;
          case 3:
            input[i].screenColumns = 4;
            break;
          case 4:
            input[i].screenColumns = 3;
            break;
          default:
            input[i].screenColumns = 1;
            break;
        }
        // adds a value if this is the last element of the row
        // this way we can display a CLEARFIX or stfff like that
        if (typeof input[i + 1] !== 'undefined') {
          if (input[i].displaySequence != input[i + 1].displaySequence) {
            input[i].hasRowBreak = true;
          } else {
            input[i].hasRowBreak = false;
          }
        }
      }

      // make field Editable or not
      for (var el = 0; el < input.length; el++) {
        if (input[el].editable && input[el].editable == 'N') {
          input[el].nonEditableFeild = true;
        } else {
          input[el].nonEditableFeild = false;
        }
      }

      // TODO
      // checks for validation and add more Angular Friendly elements
      for (var ii = 0; ii < input.length; ii++) {
        if (typeof input[ii].validationName !== 'undefined') {
          var currEntVal = input[ii].validationName;
          var isRequired = false;
          var inputType = 'text';

          for (var ai = 0; ai < currEntVal.length; ai++) {
            if (currEntVal[ai] == 'Required') {
              isRequired = true;
            }
            if (currEntVal[ai] == 'Numeric') {
              inputType = 'number';
            }
            if (currEntVal[ai] == 'Currency') {
              input[ii].controlName = 'Currency';
              inputType = 'text';
            }
            // if (currEntVal[ai] == 'Email') {
            // input[ii].controlName='Text_Box';
            // inputType = 'email';
            // }
            if (currEntVal[ai] == 'PhoneNumber') {
              inputType = 'text';
              input[ai].maskOpts = {
                mask: '999-999-9999',
                restrict: 'reject',
                validate: 'true',
                limit: 'true',
              };
            }
          }
          input[ii].isRequired = isRequired;
          input[ii].inputType = inputType;
        } else {
          input[ii].isRequired = false;
          input[ii].inputType = 'text';
        }

        if (typeof input[ii].maskOpts === 'undefined') {
          input[ii].maskOpts = {
            mask: '*',
            restrict: 'accept',
            validate: 'false',
            limit: 'false',
          };
          input[ii].stringMask = '@';
        }
        if (typeof maskAttrs !== 'undefined') {
          maskAttrs[input[ii].uuid] = angular.copy(input[ii].maskOpts);
        }
      }
    }
    return input;
  }

  // /**
  // * desc
  // * @param {*} inputFormat
  // */
  // function parseMask(inputFormat) {
  // if (inputFormat) {
  // var newStr = inputFormat.replace(new RegExp('0', 'g'), '9');
  // return newStr;
  // } else {
  // throw new Error('JSON ERROR: missing field additionalInfo');
  // }
  // }

  /**
	 * desc
	 * 
	 * @param {*}
	 *            value
	 */
  function dateToString(value) {
    return (
      lpad(value.getMonth() + 1) +
      '/' +
      lpad(value.getDate()) +
      '/' +
      value.getFullYear()
    );
  }

  /**
	 * desc
	 * 
	 * @param {*}
	 *            str
	 */
  function lpad(str) {
    var value = str.toString();
    if (value.length < 2) {
      return '0' + value;
    } else {
      return value;
    }
  }
  // <<<<<<< HEAD

  /**
	 * desc
	 * 
	 * @param {*}
	 *            _date
	 * @param {*}
	 *            _format
	 * @param {*}
	 *            _delimiter
	 */
  function stringToDate(_date, _format, _delimiter) {
    var formatLowerCase = _format.toLowerCase();
    var formatItems = formatLowerCase.split(_delimiter);
    var dateItems = _date.split(_delimiter);
    var monthIndex = formatItems.indexOf('mm');
    var dayIndex = formatItems.indexOf('dd');
    var yearIndex = formatItems.indexOf('yyyy');
    var month = parseInt(dateItems[monthIndex]);
    month -= 1;
    var formatedDate = new Date(
      dateItems[yearIndex],
      month,
      dateItems[dayIndex]
    );
    return formatedDate;
  }

  /**
	 * desc =======
	 * 
	 * /** desc
	 * 
	 * @param {*}
	 *            _date
	 * @param {*}
	 *            _format
	 * @param {*}
	 *            _delimiter
	 */
  function stringToDate(_date, _format, _delimiter) {
    var formatLowerCase = _format.toLowerCase();
    var formatItems = formatLowerCase.split(_delimiter);
    var dateItems = _date.split(_delimiter);
    var monthIndex = formatItems.indexOf('mm');
    var dayIndex = formatItems.indexOf('dd');
    var yearIndex = formatItems.indexOf('yyyy');
    var month = parseInt(dateItems[monthIndex]);
    month -= 1;
    var formatedDate = new Date(
      dateItems[yearIndex],
      month,
      dateItems[dayIndex]
    );
    return formatedDate;
  }

  /**
	 * desc >>>>>>> UAT-Tranche2-P2
	 */
  function generateUUID() {
    // Public Domain/MIT
    var d = new Date().getTime();
    if (
      typeof performance !== 'undefined' &&
      typeof performance.now === 'function'
    ) {
      d += performance.now(); // use high-precision timer if available
    }
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = (d + Math.random() * 16) % 16 | 0;
      d = Math.floor(d / 16);
      return (c === 'x' ? r : (r & 0x3) | 0x8).toString(16);
    });
  }

  /**
	 * desc
	 * 
	 * @param {*}
	 *            fields
	 */
  function fieldSorter(fields) {
    return function(a, b) {
      return fields
        .map(function(o) {
          var dir = 1;
          if (o[0] === '-') {
            dir = -1;
            o = o.substring(1);
          }
          if (a[o] > b[o]) return dir;
          if (a[o] < b[o]) return -dir;
          return 0;
        })
        .reduce(function firstNonZeroValue(p, n) {
          return p ? p : n;
        }, 0);
    };
  }

  /**
	 * removes duplicates from ARRAY
	 * 
	 * @param {*}
	 *            a
	 */
  function uniq(a) {
    var prims = {boolean: {}, number: {}, string: {}},
      objs = [];
    return a.filter(function(item) {
      var type = typeof item;
      if (type in prims)
        return prims[type].hasOwnProperty(item)
          ? false
          : (prims[type][item] = true);
      else return objs.indexOf(item) >= 0 ? false : objs.push(item);
    });
  }
}
// })();

(function() {
  'use strict';

  angular
    // underwriting service
    .module('quoteUi')
    .factory('mainService', MainService);

    MainService.$inject = ['util'];

  function MainService(util) {
    var _post = util.requestPOST;
    // var _get = util.requestGET;
    // var env = util.getEnv();
    var serviceURL = util.getServiceURL();

    return {
     
      logout : logout
    
    };

    function logout(data) {
      return _post(serviceURL + 'create', data);
    }

  }
})();

(function() {
  'use strict';

  angular
    // location service
    .module('quoteUi')
    .factory('locationService', LocationService);

  LocationService.$inject = ['util'];

  function LocationService(util) {
    var _post = util.requestPOST;
    var _get = util.requestGET;
    var env = util.getEnv();
    var serviceURL = util.getServiceURL();

    return {
      fetchLocationData: fetchLocationData,
      fetchBuildingData: fetchBuildingData,
      fetchBusinessClass: fetchBusinessClass,
      saveLocations: saveLocations,
      saveOccupants: saveOccupants,
      removeOccupants: removeOccupants,
      editOccupants: editOccupants,
    // fetchLocationMeta: fetchLocationMeta,
      fetchLocationmaster: fetchLocationmaster,
      validateBuildingAppianRule: validateBuildingAppianRule,
      saveBuildingApplicantDC: saveBuildingApplicantDC,
      deleteLocations: deleteLocations,
      validateStateAppianRule: validateStateAppianRule,
      duplicateBuilding: duplicateBuilding,
      fetchOccupantMaster: fetchOccupantMaster,
      locationSeviceCall: locationSeviceCall,
      zipCodeLookUP: zipCodeLookUP,
      deletebuilding: deletebuilding,
      saveBuildings: saveBuildings,
      editLocation: editLocation,
      editBuilding: editBuilding,
      fetchBuildingUnderData:fetchBuildingUnderData,
      protectionClassLookup:protectionClassLookup,
      "veriskCall": veriskCall

    };

    /**
	 * fetch locations
	 * 
	 * @param {*}
	 *            data
	 */
    function fetchLocationData(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'view', data);
      } else {
        return _get(util.getAssetPath() + 'dataset/Location.json', null);
      }
    }

    /**
	 * fetch buildings
	 * 
	 * @param {*}
	 *            data
	 */
    function fetchBuildingData(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'view', data);
      } else {
        return _get(util.getAssetPath() + 'dataset/building.json', null);
      }
    }
    /**
	 * fetchBuildingUnderData
	 * 
	 * @param {*}
	 *            data
	 */
    function fetchBuildingUnderData(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'view', data);
      } else {
        return _get(util.getAssetPath() + 'dataset/Building_underwriting.json', null);
      }
    }


 /**
	 * Fetch Business Class
	 * 
	 * @param {*}
	 *            data
	 */
    function fetchBusinessClass(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'view', data);
      } else {
       // return _get(util.getAssetPath() + 'dataset/building.json', null);
      }
    }

    /**
	 * Submit Applicant and Multiple Occupancy
	 * 
	 * @param {*}
	 *            data
	 */
    function saveBuildingApplicantDC(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'create', data);
      } else {
        return _get(util.getAssetPath() + 'dataset/locationAppian.json', null);
      }
    }

     /**
		 * Submit business appian rule validation
		 * 
		 * @param {*}
		 *            data
		 */
    function validateBuildingAppianRule(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'create', data);
      } else {
        return _get(util.getAssetPath() + 'dataset/locationAppian.json', null);
      }
    }

    /**
	 * Submit zipCodeLookUP
	 * 
	 * @param {*}
	 *            data
	 */
    function zipCodeLookUP(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'create', data);


      } else {
        return _get(util.getAssetPath() + 'dataset/zipcodelookup.json', null);
      }


    }
     /**
		 * Submit protectionClassLookup
		 * 
		 * @param {*}
		 *            data
		 */
    function protectionClassLookup(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'create', data);


      } else {
        return _get(util.getAssetPath() + 'dataset/protectionClassLookup.json', null);
      }


    }


    /**
	 * Submit state appian rule validation
	 * 
	 * @param {*}
	 *            data
	 */
    function validateStateAppianRule(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'create', data);
      } else {
        return _get(util.getAssetPath() + 'dataset/location.json', null);
      }
    }

    /**
	 * editLocation
	 * 
	 * @param {*}
	 *            data
	 */
    function editLocation(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'create', data);
        // return _get('assets/dataset/Location.json', null);
      } else {
        return _get(util.getAssetPath() + 'dataset/Location.json', null);
      }
    }

    /**
	 * editBuilding
	 * 
	 * @param {*}
	 *            data
	 */
    function editBuilding(data) {
      // if (env !== 'proto') {
      return _post(serviceURL + 'create', data);
      // return _get('assets/dataset/Location.json', null);
      // } else {
      // return _get('assets/dataset/Location.json', null);
      // }
    }

    /**
	 * Save location
	 * 
	 * @param {*}
	 *            data
	 */
    function saveLocations(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'create', data);
        // return _get('assets/dataset/Location.json', null);
      } else {
        return _get(util.getAssetPath() + 'dataset/Location.json', null);
      }
    }

    /**
	 * saveBuildings
	 * 
	 * @param {*}
	 *            data
	 */
    function saveBuildings(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'create', data);
        // return _get('assets/dataset/Location.json', null);
      } else {
        return _get(
          util.getAssetPath() + 'dataset/buildingAfterSave.json',
          null
        );
      }
    }


    /**
	 * Fetch location master data (add location)
	 * 
	 * @param {*}
	 *            data
	 */
    function fetchLocationmaster(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'view', data);
        // return _get('assets/dataset/Location.json', null);
      } else {
        return _get(util.getAssetPath() + 'dataset/LocationAdd.json', null);
      }
    }
    /**
	 * Fetch fetchOccupantMaster (add occupant)
	 * 
	 * @param {*}
	 *            data
	 */
    function fetchOccupantMaster(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'view', data);
       // return _get(util.getAssetPath() + 'dataset/occupant.json', null);

      } else {
        return _get(util.getAssetPath() + 'dataset/occupant.json', null);
      }
    }


 /**
	 * Save Occupants
	 * 
	 * @param {*}
	 *            data
	 */
       function saveOccupants(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'create', data);
     // return _get(util.getAssetPath() + 'dataset/occupant.json', null);

      } else {
        return _get(util.getAssetPath() + 'dataset/occupant.json', null);
      }
    }

    

     /**
		 * Remove Occupants
		 * 
		 * @param {*}
		 *            data
		 */
       function removeOccupants(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'create', data);
        } else {
        return _get(util.getAssetPath() + 'dataset/occupant.json', null);
      }
    }


      /**
		 * Edit Occupants
		 * 
		 * @param {*}
		 *            data
		 */
       function editOccupants(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'create', data);
        } else {
        return _get(util.getAssetPath() + 'dataset/occupant.json', null);
      }
    }


    /**
	 * Fetch duplicate building (duplicate building)
	 * 
	 * @param {*}
	 *            data
	 */
    function duplicateBuilding(data) {
      return _post(serviceURL + 'create', data);
    }

    /**
	 * delete location
	 * 
	 * @param {*}
	 *            data
	 */
    function deleteLocations(data) {
      return _post(serviceURL + 'create', data);
    }
    /**
	 * deletebuilding
	 * 
	 * @param {*}
	 *            data
	 */
    function deletebuilding(data) {
      return _post(serviceURL + 'create', data);
    }

    /**
	 * veriskCall
	 * 
	 * @param {*}
	 *            data
	 */
    function veriskCall(data) {

      return _post(serviceURL + 'create', data);
    }

    /**
	 * Fetch location master data (add location)
	 * 
	 * @param {*}
	 *            data
	 */
    function locationSeviceCall(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'create', data);
        // return _get('assets/dataset/Location.json', null);
      } else {
        return _get(util.getAssetPath() + 'dataset/locationServiceCall.json', null);
      }

    }
  }
})();

(function() {
  'use strict';

  angular
    // LOB service
    .module('quoteUi')
    .factory('lobService', LobService);

  LobService.$inject = ['util'];

  function LobService(util) {
    var _post = util.requestPOST;
    var _get = util.requestGET;
    var env = util.getEnv();
    var serviceURL = util.getServiceURL();

    return {
      getMeta: getMeta,
      saveLob: saveLob,
      validateLob: validateLob
    };

    /**
	 * 
	 * @param {*}
	 *            data
	 */
    function getMeta(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'view', data);
      } else {
        return _get(util.getAssetPath() + 'dataset/lob.json', null);
      }
    }

    /**
	 * Save LOBs
	 * 
	 * @param {*}
	 *            data
	 */
    function saveLob(data) {
      return _post(serviceURL + 'create', data);
    }

    /**
	 * Validate with appian
	 * 
	 * @param {*}
	 *            data
	 */
    function validateLob(data) {
      return _post(serviceURL + 'create', data);
    }
  }
})();

(function () {
  'use strict';
  angular
    //
    .module ('quoteUi')
    .service ('LoadingInterceptor', [
      '$q',
      '$rootScope',
      '$log',
      function ($q, $rootScope) {
        'use strict';
        return {
          request: function (config) {
            if (config.method == 'POST') {
              $rootScope.showSpinners = true;
              document.onkeydown = function () {
                return false;
              }; // disable all key in keyboard
            }
            return config;
          },
          requestError: function (rejection) {
            $rootScope.showSpinners = false;
            document.onkeydown = function () {
              return true;
            }; // enable all key in keyboard
            return $q.reject (rejection);
          },
          response: function (response) {
            if (response.config.method == 'POST') {
              $rootScope.showSpinners = false;
              document.onkeydown = function () {
                return true;
              }; // enable all key in keyboard
            }
            return response;
          },
          responseError: function (rejection) {
            $rootScope.showSpinners = false;
            document.onkeydown = function () {
              return true;
            }; // enable all key in keyboard
            return $q.reject (rejection);
          },
        };
      },
    ]);
}) ();

(function() {
  'use strict';

  angular
    //
    .module('quoteUi')
    .factory('ErrorToast', ErrorToast);

  ErrorToast.$inject = ['$log', 'toastr'];

  function ErrorToast($log, toastr) {
    return {
      showError: showError
    };

    function showError(response, extra) {
      $log.error('showError', response);
      
      var errorMSG = '';

      if (typeof response.status !== 'undefined') {
        errorMSG += 'Error Status ' + response.status;
      }
      if (response.data && (response.data.error || response.data.message)) {
        if (typeof response.data.error !== 'undefined') {
          errorMSG += ' - ' + response.data.error;
        }
        if (typeof response.data.message !== 'undefined') {
          errorMSG += ' - ' + response.data.message;
        }
      }

      if (typeof extra !== 'undefined') {
        errorMSG += ' - ' + extra;
      }
      // toastr.error(errorMSG, 'Error');
      toastr.error(errorMSG, response);
    }
  }
})();

(function () {
  'use strict';

  angular
    // eligibility service
    .module('quoteUi')
    .factory('eligibilityService', EligibilityService);

  EligibilityService.$inject = ['util'];

  function EligibilityService(util) {
    var _post = util.requestPOST;
    var _get = util.requestGET;
    // var env = util.getEnv();
    var serviceURL = util.getServiceURL();

    return {
      fetchEligibilityMeta: fetchEligibilityMeta,
      submitEligibilityForm: submitEligibilityForm
    };

    /**
	 * fetch eligibility metadata
	 * 
	 * @param {*}
	 *            data
	 */
    function fetchEligibilityMeta(data) {
      return _get(util.getAssetPath() + 'dataset/poc.json', data);
    }

    /**
	 * Submit eligibility form
	 * 
	 * @param {*}
	 *            data
	 */
    function submitEligibilityForm(data) {
      return _post(serviceURL + 'create', data);
    }
  }
})();

(function() {
  'use strict';

  angular
    // list service
    .module('quoteUi')
    .factory('dashboardService', DashboardService);

  DashboardService.$inject = ['util'];

  function DashboardService() {
    // var _post = util.requestPOST;
    // var _get = util.requestGET;
    // var env = util.getEnv();
    // var serviceURL = util.getServiceURL();

    return {};
  }
})();

(function () {
  'use strict';

  angular
    // business service
    .module('quoteUi')
    .factory('businessService', BusinessService);

  BusinessService.$inject = ['util'];

  function BusinessService(util) {
    var _post = util.requestPOST;
    var _get = util.requestGET;
    var env = util.getEnv();
    var serviceURL = util.getServiceURL();



    return {
      fetchBusinessForm: fetchBusinessForm,
      validateBusinessAppianRule: validateBusinessAppianRule,
      submitBusinessForm: submitBusinessForm,
      yearStartedRule: yearStartedRule,
      zipCodeLookUP: zipCodeLookUP,
      USPSandExperian: USPSandExperian,
      experianClientID: experianClientID,
      productIDforVerisk: productIDforVerisk,
      veriskCall: veriskCall
    };

    /**
	 * Fetch business form data/meta
	 * 
	 * @param {*}
	 *            data
	 */
    function fetchBusinessForm(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'view', data);
      } else {
        return _get(
          util.getAssetPath() +
          'dataset/business-details.json',
          null
        );
      }
    }

    /**
	 * Submit business appian rule validation
	 * 
	 * @param {*}
	 *            data
	 */
    function validateBusinessAppianRule(data) {
      return _post(serviceURL + 'create', data);
    }

    /**
	 * Submit business form
	 * 
	 * @param {*}
	 *            data
	 */
    function submitBusinessForm(data) {
      return _post(serviceURL + 'create', data);
    }

    /**
	 * year started validation
	 * 
	 * @param {*}
	 *            data
	 */
    function yearStartedRule(data) {
      return _post(serviceURL + 'create', data);
    }

    function zipCodeLookUP(data) {
      return _post(serviceURL + 'create', data);
    }

    function USPSandExperian(data) {
      return _post(serviceURL + 'create', data);
    }

    function experianClientID(data) {
      return _post(serviceURL + 'create', data);
    }

    function productIDforVerisk(data) {
      return _post(serviceURL + 'create', data);
    }

    function veriskCall(data) {
      return _post(serviceURL + 'create', data);
    }
  }
})();

(function () {
  'use strict';

  angular
    // building service
    .module('quoteUi')
    .factory('buildingService', BuildingService);

  BuildingService.$inject = ['util'];

  function BuildingService(util) {
    var _post = util.requestPOST;
    var _get = util.requestGET;
    var env = util.getEnv();
    var serviceURL = util.getServiceURL();

    return {
      getLocationMeta: getLocationMeta,
      viewService: viewService,
      createService: createService
    };

    function getLocationMeta(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'view', data);
      } else {
        return _get(util.getAssetPath() + 'dataset/locations.json', null);
      }
    }

    function viewService(data) {
      return _post(serviceURL + 'view', data);
    }

    function createService(data) {
      return _post(serviceURL + 'create', data);
    }
  }
})();

(function() {
  'use strict';

  angular
    // underwriting service
    .module('quoteUi')
    .factory('beforeyouissueService', BeforeyouissueService);

    BeforeyouissueService.$inject = ['util'];

  function BeforeyouissueService(util) {
    var _post = util.requestPOST;
    var _get = util.requestGET;
    var env = util.getEnv();
    var serviceURL = util.getServiceURL();

    return {     
      submitBeforeyouissue: submitBeforeyouissue,
      fetchBeforeyouissueForm : fetchBeforeyouissueForm,
      appianRule : appianRule,
      zipCodeLookUP : zipCodeLookUP
    };


    function fetchBeforeyouissueForm(data) {
      
      if (env !== 'proto') {
       
        return _post(serviceURL + 'view', data);
      } else {
       
        return _get(
          '/assets/dataset/beforeyouissue.json',
          null
        );
      }
    }
    /**
	 * Get form metadata // *
	 * 
	 * @param {*}
	 *            data //
	 */
    // function getFormMeta(data) {
    // return _get('assets/dataset/beforeyouissue.json', data);
    // }
    function appianRule(data) {
      return _post(serviceURL + 'view', data);
    }     

    function submitBeforeyouissue(data) {
      return _post(serviceURL + 'view', data);
    }  

    function zipCodeLookUP(data) {
      return _post(serviceURL + 'view', data);
    }
  }
})();

(function() {
  'use strict';

  angular
    // business service
    .module('quoteUi')
    .factory('BeforeYouBeginService', BeforeYouBeginService);

  BeforeYouBeginService.$inject = ['util'];

  function BeforeYouBeginService(util) {
    var _post = util.requestPOST;
    var _get = util.requestGET;
    var env = util.getEnv();
    var serviceURL = util.getServiceURL();

    return {
      fetchBusinessForm: fetchBusinessForm,
      validateBusinessAppianRule: validateBusinessAppianRule,
      submitForm: submitForm,
      yearStartedRule: yearStartedRule
    };

    /**
	 * Fetch business form data/meta
	 * 
	 * @param {*}
	 *            data
	 */
    function fetchBusinessForm(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'view', data);
      } else {
        return _get(util.getAssetPath() + 'dataset/beforeubegin.json', null);
      }
    }

    /**
	 * Submit business appian rule validation
	 * 
	 * @param {*}
	 *            data
	 */
    function validateBusinessAppianRule(data) {
      return _post(serviceURL + 'create', data);
    }

    /**
	 * Submit business form
	 * 
	 * @param {*}
	 *            data
	 */
    function submitForm(data) {
      return _post(serviceURL + 'create', data);
    }

    /**
	 * year started validation
	 * 
	 * @param {*}
	 *            data
	 */
    function yearStartedRule(data) {
      return _post(serviceURL + 'create', data);
    }
  }
})();

(function() {
  'use strict';

  angular
    // AsyncValidator factory
    // Analize response from $http and generates error message accordingly
    .module('quoteUi')
    .factory('AsyncValidator', AsyncValidator);

  AsyncValidator.$inject = ['$log'];

  function AsyncValidator($log) {
    return {
      test: test
    };

    function test(response) {
      $log.error('test', response);
    }
  }
})();

(function() {
  'use strict';

  angular
    // Additional InterestService service
    .module('quoteUi')
    .factory('additionalinterestService', AdditionalInterestService);

  AdditionalInterestService.$inject = ['util'];

  function AdditionalInterestService(util) {
    var _post = util.requestPOST;
    var _get = util.requestGET;
    var env = util.getEnv();
    var serviceURL = util.getServiceURL();

    return {
      submitAddInterest: submitAddInterest,
      fetchAddInterestForm: fetchAddInterestForm,
      fetchEditableAddInterestForm: fetchEditableAddInterestForm,
      zipcodeLookup: zipcodeLookup,
      fetchBuilding: fetchBuilding
    };

    function fetchAddInterestForm(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'view', data);
      } else {
        return _get(util.getAssetPath() + 'dataset/interest.json', null);
      }
    }

    function fetchEditableAddInterestForm(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'view', data);
      } else {
        return _get(
          util.getAssetPath() + 'dataset/interest_transaction.json',
          null
        );
      }
    }

    function submitAddInterest(data) {
      return _post(serviceURL + 'create', data);
    }

    function zipcodeLookup(data) {
      return _post(serviceURL + 'create', data);
    }

    function fetchBuilding(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'view', data);
      } else {
        return _get(
          util.getAssetPath() + 'dataset/interest_building.json',
          null
        );
      }
    }
  }
})();

(function() {
  'use strict';

  angular
    // accounts service
    .module('quoteUi')
    .factory('accountsService', AccountsService);

  AccountsService.$inject = ['util'];

  function AccountsService(util) {
    var _post = util.requestPOST;
    var _get = util.requestGET;
    var env = util.getEnv();
    var serviceURL = util.getServiceURL();

    return {
      fetchAccountData: fetchAccountData,
      getAction: getAction,
      fetchAccountDet: fetchAccountDet
    };

    function fetchAccountData(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'view', data);
      } else {
        return _get(util.getAssetPath() + 'dataset/account.json', null);
      }
    }

    function fetchAccountDet(data) {
      if (env !== 'proto') {
        return _post(serviceURL + 'view', data);
      } else {
        return _get(
          util.getAssetPath() + 'dataset/editclientresponse.json',
          null
        );
      }
    }

    function getAction(data) {
      // console.log("in service")
      if (env !== 'proto') {
        return _post(serviceURL + 'view', data);
      } else {
        return _get(util.getAssetPath() + 'dataset/accountdd.json', null);
      }
    }
  }
})();

(function () {
  'use strict';
  angular
    // main
    .module ('quoteUi')
    .controller ('MainController', MainController);

  /** @ngInject */

  function MainController (
    util,
    $location,
    $rootScope,
    $scope,
    $interval,
    $state,
    saveandexitService,
    Idle,
    Keepalive,
    $uibModal,
    $timeout
  ) {
    var vm = this;
    vm.exitApp = exitApp;
    vm.triggerExit = triggerExit;
    vm.appPath = $location.path ();

    vm.enableExit = false;

    start ();

    // Timeout Starts

    $scope.started = false;

    function closeModals () {
      if ($scope.warning) {
        $scope.warning.close ();
        $scope.warning = null;
      }

      if ($scope.timedout) {
        $scope.timedout.close ();
        $scope.timedout = null;
      }
    }

    $scope.$on ('IdleStart', function () {
      closeModals ();

      $scope.warning = $uibModal.open ({
        templateUrl: 'warning-dialog.html',
        windowClass: 'modal-warning',
      });
    });

    $scope.$on ('IdleEnd', function () {
      closeModals ();
      stop ();
    });

    $scope.$on ('IdleTimeout', function () {
      stop ();
      closeModals ();
      exitApp ();
    });

    function start () {
      closeModals ();
      Idle.watch ();
      $scope.started = true;
    }

    function stop () {
      closeModals ();
      Idle.unwatch ();
      $scope.started = false;
    }

    // Timeout Ends

    vm.resolution = window.innerWidth + 'X' + window.innerHeight;

    $interval (function () {
      $rootScope.quoteBindable = $rootScope.appQuoteBindable;

      $scope.$watch ('$rootScope.pageInfo', function () {
        vm.pageInfo = $rootScope.pageInfo[0].currentPage;
        vm.pageInfoData = $rootScope.pageInfo[0].currentPageData;
        vm.element = $rootScope.pageInfo[0].element;
        vm.operation = $rootScope.pageInfo[0].operation;

        if (
          vm.pageInfo == '/beforeyoubegin' ||
          vm.pageInfo == '/businessdetails' ||
          vm.pageInfo == '/lob' ||
          vm.pageInfo == '/quote' ||
          vm.pageInfo == '/qualification'
        ) {
          vm.enableExit = true;
        } else {
          vm.enableExit = false;
        }
      });
    }, 1000);

    function exitApp () {
      vm.loggedIn = false;

      $ ('#id1').remove ();
      $ ('.OmniaheaderText').css ('display', 'none');
      $ ('#main-wrapper').attr ('class', 'col-md-12');
      $ ('#main-wrapper')
        .text ('You have successfully logged off.')
        .css ('text-align', 'center');

      // var data = {
      // session: ["12345"],
      // elementType: ['GLOBALUI'],
      // operationType: ['LOGOUT']
      // }

      $state.go ('app.logout');
    }

    function triggerExit (val) {
      $rootScope.$broadcast ('onsave');
      if (vm.element[0] == 'RISK' && vm.operation[0] == 'SAVELOCATION') {
        console.log (
          "vm.element[0] == 'RISK' && vm.operation[0]",
          vm.element[0],
          vm.operation[0]
        );
        $rootScope.$broadcast ('exitLocationName', vm.pageInfoData);
        console.log ('vm.pageInfoDat', vm.pageInfoData);
        $rootScope.pageInfo[0].currentPageData[0].editableLeft = true;
        $rootScope.pageInfo[0].currentPageData[0].locationName[0] =
          $rootScope.locationNameSE;
      }
      if (vm.element[0] == 'RISK' && vm.operation[0] == 'SAVEBUILDING') {
        $rootScope.$broadcast ('exitbuildingres', vm.pageInfoData);

        $rootScope.pageInfo[0].currentPageData = [
          $rootScope.exitBuildingDataResSE,
        ];
        $rootScope.pageInfo[0].currentPageData[0].buildingLeft = true;
      }
      if (vm.element[0] == 'RISK' && vm.operation[0] == 'EDITBUILDING') {
        $rootScope.$broadcast ('exitbuildingres', vm.pageInfoData);

        $rootScope.pageInfo[0].currentPageData = [
          $rootScope.exitBuildingDataResSE,
        ];
        $rootScope.pageInfo[0].currentPageData[0].editableBuildingLeft = true;
      }
      $timeout (function () {
        if (val === 'save') {
          var data = {
            elementType: $rootScope.pageInfo[0].element,
            operationType: $rootScope.pageInfo[0].operation,
            saveAndExit: ['true'],
            data: $rootScope.pageInfo[0].currentPageData,
          };
          console.log ('ghfsghdf', data);
          saveandexitService.submitTransactionData (data).then (function () {
            exitApp ();
          });
        } else {
          exitApp ();
        }
      }, 20);
    }
    vm.assetPath = util.getImagePath ();
  }
}) ();

(function() {
  'use strict';

  angular
    // Zip code validation directive
    .module('quoteUi')
    .directive('zipCode', ZipCode);

  ZipCode.$inject = [];

  function ZipCode() {
    return {
      restrict: 'A',
      require: ['?ngModel', '^form'],
      link: function(scope, elem, attr, ctrl) {
        var ngModel = ctrl[0];
        // var form = ctrl[1];
        if (ngModel && attr.zipCode == 'true') {
          // var getter = $parse(attr.ngModel);
          // var setter = getter.assign;

          var ctrlDown = false, // check ctrl key (window) or cmd key (Mac) is
								// press for copy/paste
              ctrlKey = 17,     // for ctrl key (window)
              cmdKey = 91,      // for cmd key (mac)
              vKey = 86,        // check 'c' key is press for copy
              cKey = 67,        // check 'v' key is press for paste
              xKey = 88,        // check 'x' key is press for cut
              aKey = 65;        // check 'a' key is press to select the content

          var controlKeys = [
            8, // backspace
            9, // tab
            13, // enter
            37, // left arrow
            38, // up arrow
            39, // right arrow
            40 // down arrow
          ];

          // key up
          elem.bind('keyup', function ($event) {

            /**
			 * If the ctr key in window or cmd key in mac is release then make
			 * ctrlDown = false which when control key or cmd key is released on
			 * key up
			 */
            if ($event.keyCode == ctrlKey ||   // check ctl key press in window
                $event.keyCode == cmdKey) {    // check cmd key press in mac

                ctrlDown = false;
            }

          });

          // key down
          elem.on('keydown', function(e) {

            /**
			 * If the ctr key in window or cmd key in mac is press then make
			 * ctrlDown = true
			 */
            if (e.keyCode == ctrlKey ||  // check ctl key press in window
                e.keyCode == cmdKey) {   // check cmd key press in mac

                ctrlDown = true;
            }
            /**
			 * If ctrlDown = true which mean ctl/cmd key is already pressed then
			 * check if 'c' key is press for copy check if 'v' key is press for
			 * paste check if 'x' key is press for cut check if 'a' key is press
			 * to highlight content
			 */
            if (ctrlDown &&             // ctl/cmd key press
                (e.keyCode == vKey ||   // 'v' key press for paste
                 e.keyCode == cKey ||   // 'c' key press for copy
                 e.keyCode == aKey ||   // 'a' key press for selecting content
                 e.keyCode == xKey)) {  // 'x' key press for cut

                 return false;
            }
            // allow only digits
            if (
              (e.keyCode < 96 && e.keyCode > 105) ||
              (e.keyCode >= 65 && e.keyCode <= 90) ||
              e.keyCode > 105
            ) {
              e.preventDefault();
            }
            switch (e.key) {
              case '!':
              case '@':
              case '#':
              case '$':
              case '%':
              case '^':
              case '&':
              case '*':
              case '(':
              case ')':
                e.preventDefault();
                break;
            }



            /**
			 * This is for Chrome Browser. When user type zipcode and reach the
			 * max character limit, at that time if user select the complete
			 * text and then if user type again zipcode then
			 * selected/highlighted zipcode must be removed with the new one
			 * that user entered.
			 */
           if (window.getSelection()) {
            var selecttxt = window.getSelection().toString();
            if (selecttxt) {
              return;
            }
          }
          /**
			 * This is for other Browser. When user type zipcode and reach the
			 * max character limit, at that time if user select the complete
			 * text and then if user type again zipcode then
			 * selected/highlighted zipcode must be removed with the new one
			 * that user entered.
			 */
          var text = attr.uservalue;
          var selectText = text.valueOf().substr(text.selectionStart,text.selectionEnd - text.selectionStart);
          if (selectText) {
            return;
          }


          });

          // key press
          elem.on('keypress', function(e) {
            /**
			 * This is for Chrome Browser. When user type zipcode and reach the
			 * max character limit, at that time if user select the complete
			 * text and then if user type again zipcode then
			 * selected/highlighted zipcode must be removed with the new one
			 * that user entered.
			 */
            if (window.getSelection()) {
              var selecttxt = window.getSelection().toString();
              if (selecttxt) {
                return;
              }
            }

            /**
			 * This is for other Browser. When user type zipcode and reach the
			 * max character limit, at that time if user select the complete
			 * text and then if user type again zipcode then
			 * selected/highlighted zipcode must be removed with the new one
			 * that user entered.
			 */
           var text = attr.uservalue;
           var selectText = text.valueOf().substr(text.selectionStart,text.selectionEnd - text.selectionStart);
           if (selectText) {
             return;
           }

            // max length 5
            var val = elem.val();
            if (val.length != null && val.length >= 5) {
              if (controlKeys.indexOf(e.keyCode) == -1 || e.shiftKey) {
                e.preventDefault();
              }
            }
          });

          // blur
          elem.on('blur', function() {
            // var val = e.target.value;
            // var ctName = attr.name;
            // form[ctName];
          });
        }
      }
    };
  }
})();

(function () {
    'use strict';
    angular
        .module('quoteUi')
        .directive('widget', function () {
            var template = '<div ng-switch="type">'
               // +'<span class="{{$parent.classname}}"
				// ng-switch-when="checkbox" ng-repeat="i in values track by
				// $index"><input
				// ng-disabled="checkEditable(editable.toLowerCase())"
				// required="required" name="{{name}}" type="checkbox"
				// id="{{name}}_{{$index}}"
				// ng-model="$parent.$parent.modelValue[$index]" value="{{i}}"
				// ><label for="{{name}}_{{$index}}"
				// >{{labels[$index]}}</label></span>'
                +'<span class="{{$parent.classname}}" ng-switch-when="checkbox" ng-repeat="i in values track by $index"><input  ng-disabled="checkEditable(editable.toLowerCase())"  ng-required="checkEditable(editable.toLowerCase())===false" name="{{name}}" type="checkbox" id="{{name}}_{{$index}}" ng-checked="checkboxChecked($index)" ng-click="oncheckBoxChange($event,$index)" value="{{i}}"  ><label for="{{name}}_{{$index}}" >{{labels[$index]}}</label></span>'
               
                + '<span  ng-switch-when="text"><input ng-disabled="checkEditable(editable.toLowerCase())"  ng-required="checkEditable(editable.toLowerCase())===false" name="{{name}}" type="text" ng-model="$parent.modelValue" class="{{$parent.classname}}" ng-change="onChange()"  ></span>'
                + '<span class="{{$parent.classname}}" ng-switch-when="radio" ng-repeat="i in values track by $index"><input ng-disabled="checkEditable(editable.toLowerCase())"  ng-disabled="checkEditable(editable.toLowerCase())" ng-required="checkEditable(editable.toLowerCase())===false" name="{{name}}" type="radio" id="{{name}}_{{$index}}" ng-model="$parent.$parent.modelValue" ng-change="onChange()" value="{{i}}"  ><label for="{{name}}_{{$index}}" >{{labels[$index]}}</label></span>'
                + '<span  ng-switch-when="dropdown" >'
                + '<select  ng-disabled="checkEditable(editable.toLowerCase())"  name="{{name}}" ng-change="onChange()" ng-model="$parent.modelValue" class="{{$parent.classname}}" >'
                + '<option ng-repeat="i in values track by $index" value="{{i}}">{{labels[$index]}}</option>'
                + '</select>'
                + '</span>'
                + '<span class="{{$parent.classname}}" ng-switch-default ng-bind-html="modelValue"></span>'
                + '</div>';
            var controller = ['$scope', function ($scope) {
                function init() {
                   // $scope.widgetVal = $scope.modelValue;
                }
                $scope.checkEditable = function(aString){
                    if(aString != "y"){return true;}
                    return false;
                };
                $scope.checkboxChecked= function(aIndex) {
                    if(Array.isArray($scope.modelValue)){
                        return Boolean($scope.modelValue[aIndex]);
                    }
                    return Boolean($scope.modelValue);    
                    
                };
                $scope.oncheckBoxChange = function(aEvent,aIndex){
                    if(Array.isArray($scope.modelValue)){
                        $scope.modelValue[aIndex] = Number(angular.element(aEvent.srcElement)[0].checked);
                    }else{
                        $scope.modelValue = Number(angular.element(aEvent.srcElement)[0].checked);
                    }
                };
                $scope.onChange = function () {
// console.log($scope.modelValue)
                    $scope.widgetChange();
                };
                init();
            }];
            return {
                restrict: 'EA',
                scope: {
                    editable: '@',
                    name: '@',
                    classname: '@',
                    type: '@',
                    widgetChange:'&',
                    values:"=",
                    labels:'=',
                    modelValue: '=ngModel'
                },
                controller: controller,
                template: template
            };
        });
}());

// ErrorToast factory
// Analize response from $http and generates error message accordingly
angular.module('quoteUi').directive('white-box', function() {
  return {
    restrict: 'E',
    transclude: true,
    scope: {
      title: '@',
      subtitle: '@'
    },
    template:
      "<div class='white-pannel'>" +
      '<h5>{{title}}</h5>' +
      "<div ng-if='{{subtitle}}'>{{subtitle}}</div>" +
      '<div ng-transclude></div></div>'
  };
});

(function() {
  'use strict';

  angular
    //
    .module('quoteUi')
    .directive('smartFormField', function($http, $compile) {
      var linker = function(scope, element) {
        // var renderNull = false;
        // var formElementHtml = '';
        // var messagesHtml = '';
        // GET template content from path
        // var theHtml = "<p>"+attrs.field+"</p>";
        // switch (scope.dataset.controlName) {
        // case 'Text_Box':
        // //formElementHtml = renderText(scope.dataset);
        // break;
        // default:
        // //renderNull = true;
        // return;
        // break;
        // }
        scope.clikko = function() {
          //
        };
        var theHtml = renderText(scope.dataset);
        element.html(theHtml);
        $compile(element.contents())(scope);
      };

      function renderText(item) {
        var html =
          '<input type="' +
          item.inputType +
          '" data-uuid="' +
          item.uuid +
          '" id="' +
          item.systemRefId +
          '" name="' +
          item.systemRefId +
          '"';
        html +=
          'ng-change="vm.validateBusinessInfo($index)" class="form-control"';
        html += 'ng-model="ngModel" placeholder="{{item.additionalInfo}}"';
        html +=
          'ui-mask="{{item.maskOpts.mask}}" ng-required="item.isRequired" ng-click="clikko()"/>';
        return html;
      }

      // function createInput() {
      // return '<input type="text" ng-model="role" ng-currency
		// ng-required=true />';
      // }

      return {
        template: '<div ng-bind="field"></div>',
        restrict: 'E',
        // require: '?ngModel',
        scope: {
          field: '=',
          dataset: '=dataset',
          ngModel: '=ngModel'
        },
        link: linker
      };
    });
})();

(function () {
  'use strict';
  angular.module ('quoteUi').directive ('pasteNumberOnly', function () {
    /**
	 * This Directive is used to paste only number till its length limit in
	 * particular input tag. For example if length on zip-code field is 5 then
	 * it will restrict user to paste till 5 characters.
	 * 
	 * HOW TO USE IT? just paste the directive name (paste-number-only) in your
	 * input tag then Add conditions in that input tag with the 'key' as length
	 * of input field and 'value' as systemRefId of particular field in that
	 * page paste-number-only="{5:{{item.systemRefId == 'mailingAddress.ZIP'}} }"
	 * 
	 * NOTE : i am assuming that length for particular field will remain
	 * constant across all pages. This need to confirm with BA.
	 * 
	 * ONE MORE THING : we can make it more dynamic once we get length of
	 * particular field in JSON from back-end Team.
	 * 
	 * ALERT: if you have more to add, to make it more easy,efficient and
	 * dynamic, plz come forward and suggest.
	 * 
	 * Thank You.
	 * 
	 * Creator/Developer :- AnshulJS
	 */

    return {
      require: 'ngModel',
      restrict: 'A',

      link: function (scope, element, attrs, modelCtrl) {
        var modelLimit, inputLimit = undefined;

        function CharLimit (input, maxChar) {
          var len = input.length;
          if (len > maxChar) {
            input = input.substring (0, maxChar);
          }
          return input;
        }
        modelCtrl.$parsers.push (function (inputValue) {
          /**
			 * attrs.pasteNumberOnly is returning a json which contain key as
			 * length and value as boolean value. so below _.each() is an
			 * function from underscore.js library that check for a value which
			 * contain 'true' then it will take corresponding 'key' which is a
			 * length and we assign that length to 'inputLimit' variable.
			 */
          var pasteOBJ = eval ('(' + attrs.pasteNumberOnly + ')');

          _.each (pasteOBJ, function (val, key) {
            if (val) {
              inputLimit = key;
            }
          });

          if (inputValue === undefined) {
            return '';
          }
          if (inputLimit == undefined) {
            return inputValue;
          }
          /**
			 * This below condition is use for making restriction on some input
			 * tag For Ex. if input field is business Phone then with only digit
			 * it also contain hyphen '-'.
			 * 
			 */
          // console.log(attrs.pasteboolean);
          if (attrs.pasteboolean == 'false') {
            // console.log('paste inside false directive',inputValue);
            modelCtrl.$setViewValue (inputValue);
            modelCtrl.$render ();
            return inputValue;
          }

          var cleanInputValue = inputValue.replace (/[^\d]/g, ''); // keep
																	// numbers
																	// only

          if (cleanInputValue != inputValue) {
            modelLimit = CharLimit (cleanInputValue, inputLimit);
            modelCtrl.$setViewValue (modelLimit);
            modelCtrl.$render ();
            return modelLimit;
          } else {
            inputValue = CharLimit (inputValue, inputLimit);
            modelCtrl.$setViewValue (inputValue);
            modelCtrl.$render ();
            return inputValue;
          }
        }); // End of parser function
      }, // End of link function
    }; // End of return
  }); // End of directive
}) ();

(function() {
    'use strict';

    angular
      .module('quoteUi')
      .directive('pasteAlphaNumericSymbol', function() {


   /**
	 * This Directive is used to paste Alphabet + Number + Symbol till its
	 * length limit in particular input tag. For example if length on street
	 * Address field is 40 then it will restrict user to paste till 40
	 * characters.
	 * 
	 * HOW TO USE IT? just paste the directive name (paste-alpha-numeric-symbol)
	 * in your input tag then Add conditions in that input tag with the 'key' as
	 * length of input field and 'value' as systemRefId of particular field in
	 * that page
	 * 
	 * paste-alpha-numeric-symbol="{ 40:{{item.systemRefId ==
	 * 'mailingAddress.address1'}} }"
	 * 
	 * NOTE : i am assuming that length for particular field will remain
	 * constant across all pages. This need to confirm with BA.
	 * 
	 * ONE MORE THING : we can make it more dynamic once we get length of
	 * particular field in JSON from back-end Team.
	 * 
	 * ALERT: if you have more to add, to make it more easy,efficient and
	 * dynamic, plz come forward and suggest.
	 * 
	 * Thank You.
	 * 
	 * Creator/Developer :- AnshulJS
	 */

        return {
                require: 'ngModel',
                restrict: 'A',

                link: function(scope, element, attrs, modelCtrl) {


                  var inputLimit = undefined;

                    function CharLimit(input, maxChar) {

                        var len = input.length;
                        if (len > maxChar) {
                           input =  input.substring(0, maxChar);
                        }
                        return input;
                    }
                    modelCtrl.$parsers.push(function(inputValue) {

                        /**
						 * attrs.pasteAlphaNumericSymbol is returning a json
						 * which contain key as length and value as boolean
						 * value. so below _.each() is an function from
						 * underscore.js library that check for a value which
						 * contain 'true' then it will take corresponding 'key'
						 * which is a length and we assign that length to
						 * 'inputLimit' variable.
						 */
                      var pasteOBJ = eval('(' + attrs.pasteAlphaNumericSymbol + ')');


                      _.each(pasteOBJ, function( val, key ) {

                            if ( val ) {
                              inputLimit = key;
                            }
                        });

                        if (inputValue === undefined){
                              return '';
                        }
                        if (inputLimit == undefined || inputLimit == '') {
                          return inputValue;
                        }

                        if(attrs.pasteboolean == "false"){

                            modelCtrl.$setViewValue(inputValue);
                            modelCtrl.$render();
                            return inputValue;
                        }
                            inputValue = CharLimit(inputValue, inputLimit);
                            modelCtrl.$setViewValue(inputValue);
                            modelCtrl.$render();
                            return inputValue;


                    }); // End of parser function
                }  // End of link function
            };  // End of return
        });  // End of directive
})();

/** @const */

// custom ng-if directive to create optional elements wihtin directives

var NAME = 'customNgIf';

angular.module('quoteUi').directive(NAME, function(ngIfDirective) {
  var ngIf = ngIfDirective[0];

  return {
    transclude: ngIf.transclude,
    priority: ngIf.priority,
    terminal: ngIf.terminal,
    restrict: ngIf.restrict,
    link: function($scope, $element, $attr) {
      var value = $attr[NAME];
      var yourCustomValue = $scope.$eval(value);

      $attr.ngIf = function() {
        return yourCustomValue;
      };
      ngIf.link.apply(ngIf, arguments);
    }
  };
});

(function() {
  'use strict';

  angular
    // currency directive
    .module('quoteUi')
    .directive('myCurrency', MyCurrency);

  MyCurrency.$inject = ['$filter'];

  function MyCurrency() {
    return function(input) {
      input = parseFloat(input);
      input = input.toFixed(input % 1 === 0 ? 0 : 2);
      return '$' + input.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    };
  }
})();

(function () {
  'use strict';
  angular.module ('quoteUi').directive ('inputLengthLimit', inputLengthLimit);

  /**
	 * This Directive is used to restrict the user character input to particular
	 * given length. For example if length on zipcode field is 5 then it will
	 * restrict user input to 5 character.
	 * 
	 * HOW TO USE IT? just paste the directive name (input-length-limit) in your
	 * input tag then Add below in-built angular directive in that input tag
	 * with the 'key' as length of input field and 'value' as systemRefId of
	 * particular field in that page ng-maxlength="{ 5:item.systemRefId ==
	 * 'mailingAddress.ZIP', 40:item.systemRefId == 'mailingAddress.address1' ||
	 * item.systemRefId == 'mailingAddress.address2', }" Also Add this attribute
	 * uservalue="{{modelName}}" in that input field. For eg: In business detail
	 * page :- uservalue="{{vm.businessDetailsForm.uuid[item.uuid]}}" where
	 * vm.businessDetailsForm.uuid[item.uuid] is the ng-model
	 * 
	 * NOTE : i am assuming that length for particular field will remain
	 * constant across all pages. This need to confirm with BA.
	 * 
	 * ONE MORE THING : we can make it more dynamic once we get length of
	 * particular field in JSON from backend Team.
	 * 
	 * ALERT: if you have more to add, to make it more easy,efficient and
	 * dynamic, plz come forward and suggest.
	 * 
	 * Thank You.
	 * 
	 * Creator/Developer :- AnshulJS
	 */

  function inputLengthLimit () {
    return {
      require: 'ngModel',
      link: function (scope, el, attrs, modelCtrl) {
        var currency;
        var ctrlDown = false, // check ctrl key (window) or cmd key (Mac) is
								// press for copy/paste
          ctrlKey = 17, // for ctrl key (window)
          cmdKey = 91, // for cmd key (mac)
          vKey = 86, // check 'c' key is press for copy
          cKey = 67, // check 'v' key is press for paste
          xKey = 88, // check 'x' key is press for cut
          aKey = 65; // check 'a' key is press to select the content

        el.bind ('blur', function () {
          currency = attrs.uservalue;
          // console.log((currency.match(/0/g) || []).length ==
			// currency.length);
          if (
            isNaN (currency) == false &&
            currency &&
            (currency.match (/0/g) || []).length == currency.length
          ) {
            currency = parseFloat (currency);
            currency = currency.toFixed (currency % 1 === 0 ? 0 : 2);
            modelCtrl.$setViewValue (currency);
            modelCtrl.$render ();
            return;
          }
        });

        el.bind ('keyup', function ($event) {
          /**
			 * If the ctr key in window or cmd key in mac is release then make
			 * ctrlDown = false which when control key or cmd key is released on
			 * key up
			 */
          if (
            $event.keyCode == ctrlKey || // check ctl key press in window
            $event.keyCode == cmdKey
          ) {
            // check cmd key press in mac

            ctrlDown = false;
          }
        });

        el.bind ('keypress', function ($event) {
          // console.log($event.key);
          // console.log(attrs.ngMaxlength);
          // console.log($event.ctrlKey);

          var length;

          /**
			 * If the ctr key in window or cmd key in mac is press then make
			 * ctrlDown = true
			 */
          if (
            $event.keyCode == ctrlKey || // check ctl key press in window
            $event.keyCode == cmdKey
          ) {
            // check cmd key press in mac

            ctrlDown = true;
          }
          /**
			 * If ctrlDown = true which mean ctl/cmd key is already pressed then
			 * check if 'c' key is press for copy check if 'v' key is press for
			 * paste check if 'x' key is press for cut check if 'a' key is press
			 * to highlight content
			 */
          if (
            ctrlDown && // ctl/cmd key press
            ($event.keyCode == vKey || // 'v' key press for paste
            $event.keyCode == cKey || // 'c' key press for copy
            $event.keyCode == aKey || // 'a' key press for selecting content
              $event.keyCode == xKey)
          ) {
            // 'x' key press for cut

            return false;
          }
          /**
			 * --> attrs.ngMaxlength is returning a json which contain key as
			 * length and value as boolean value. so below _.each() is an
			 * function from underscore.js library that check for a value which
			 * contain 'true' then it will take corresponding 'key' which is a
			 * length and we assign that length to 'inputLimit' variable.
			 */
          _.each (attrs.ngMaxlength, function (val, key) {
            if (val) {
              length = key;
            }
          });

          /**
			 * This is for Chrome Browser. When user type zipcode and reach the
			 * max character limit, at that time if user select the complete
			 * text and then if user type again zipcode then
			 * selected/highlighted zipcode must be removed with the new one
			 * that user entered.
			 */
          if (window.getSelection ()) {
            var selecttxt = window.getSelection ().toString ();
            if (selecttxt) {
              return;
            }
          }

          /**
			 * This is for other Browser. When user type zipcode and reach the
			 * max character limit, at that time if user select the complete
			 * text and then if user type again zipcode then
			 * selected/highlighted zipcode must be removed with the new one
			 * that user entered.
			 */
          var text = $event.target;
          var selectText = text.value.substr (
            text.selectionStart,
            text.selectionEnd - text.selectionStart
          );
          if (selectText) {
            return;
          }

          /**
			 * This will check the length and restrict it to its limit
			 */

          if (attrs.uservalue.length > length - 1) {
            if (
              $event.keyCode == 8 || // backspace
              $event.keyCode == 46 || // delete
              $event.keyCode == 37 || // left arrow
              $event.keyCode == 39 || // right arrow
              $event.keyCode == 9 || // tab
              $event.keyCode == 35 || // end
              $event.keyCode == 36 // home
            ) {
              return;
            }

            $event.preventDefault ();
          }
        }); // End of KeyPress and KeyDown
      }, // End of link function
    }; // End of return
  } // End of inputLengthLimit function
}) ();


(function() {
    'use strict';
  
    angular
      .module('quoteUi')
      .directive('inputCharOnly', function() { 
            
        
        return {
                require: 'ngModel',
                restrict: 'A',
                
                link: function(scope, element, attrs, modelCtrl) { 
                    // console.log("attrs..." + JSON.stringify(attrs));

                    var modelLimit;
                    function CharLimit(input, maxChar) {
             
                        var len = input.length;
                        if (len > maxChar) {
                           input =  input.substring(0, maxChar);
                        }
                        return input;
                    }
                    modelCtrl.$parsers.push(function(inputValue) {
                        // console.log('jkjk',inputValue);
                        // console.log('jkjk',attrs);
                        // console.log('jkjk',modelCtrl);
                        if(inputValue === undefined) return '';

       

                         if(attrs.systemrefid == "NAME_OF_THE_PERSON_OR_ORG" || attrs.systemrefid == "STREET_ADDRESS" || attrs.systemrefid == "APT_SUITE"|| attrs.systemrefid == "CITY"){ 
                            // var cleanInputValue =
							// inputValue.replace(/[^\d.]/g, ''); // keep
							// numbers and dots
                           // var length = attrs.id.replace(/[^\d]/g, ''); //
							// keep numbers only
                              var length = attrs.id;
                                                                                  
                            var cleanInputValue = inputValue.replace(/[^ a-zA-Z0-9]/g, '');  //
                            if(cleanInputValue != inputValue) {
                                modelLimit = CharLimit(cleanInputValue, length);
                                modelCtrl.$setViewValue(modelLimit);
                                modelCtrl.$render();
                            }else{
                                return inputValue;
                            }
                            return modelLimit;
                        }

                        return inputValue;

                        
                    });

                    
                }
            };
});


})();

(function() {
    'use strict';
  
    angular
      // currency directive
      .module('quoteUi')
      .directive('infinateScroll', InfinateScroll);

        function InfinateScroll($rootScope) {
            var link = function (scope, element) {
                var raw = element[0];
    
                element.bind('scroll', function(e) {
                    var height = raw.scrollTop + raw.offsetHeight;
    
                    height = Math.round(height);
    
                    if (height >= raw.scrollHeight && !$rootScope.busy) {
                        var evt = window.event || e; // equalize event object
                        evt = evt.originalEvent ? evt.originalEvent : evt; // convert
																			// to
																			// originalEvent
																			// if
																			// possible
                        var delta = evt.detail ? evt.detail*(-40) : evt.wheelDelta; // check
																					// for
																					// detail
																					// first,
																					// because
																					// it
																					// is
																					// used
																					// by
																					// Opera
																					// and
																					// FF
    
                        if(delta > 0) { // jshint ignore:line
                            // scroll up
                        } else {
                            // scope[attrs.function](true); for avoiding
							// conflicts with ng scrollbars
                            scope.method()(true);
                            scope.$apply();
                        }
                    }
                });
            };
            return { 
                restrict: 'A',
                scope: {
                    'method': '&infinateScroll'
                },
                link: link
            };
        }

})();

/*
 * angular-google-places-autocomplete
 * 
 * Copyright (c) 2014 "kuhnza" David Kuhn Licensed under the MIT license.
 * https://github.com/kuhnza/angular-google-places-autocomplete/blob/master/LICENSE
 */

 'use strict';

angular.module('google.places', [])
	/**
	 * DI wrapper around global google places library.
	 * 
	 * Note: requires the Google Places API to already be loaded on the page.
	 */
	.factory('googlePlacesApi', ['$window', function ($window) {
        if (!$window.google) throw 'Global `google` var missing. Did you forget to include the places API script?';

		return $window.google;
	}])

	/**
	 * Autocomplete directive. Use like this:
	 * 
	 * <input type="text" g-places-autocomplete ng-model="myScopeVar" />
	 */
	.directive('gPlacesAutocomplete',
        [ '$parse', '$compile', '$timeout', '$document', 'googlePlacesApi',
        function ($parse, $compile, $timeout, $document, google) {

            return {
                restrict: 'A',
                require: '^ngModel',
                scope: {
                    model: '=ngModel',
                    options: '=?',
                    forceSelection: '=?',
                    customPlaces: '=?'
                },
                controller: ['$scope', function () {}],
                link: function ($scope, element, attrs, controller) {


                    if(attrs.systemrefid == "clientDetails.name" || attrs.systemrefid == "mailingAddress.address1"|| attrs.systemrefid == "LocationInput.Address1"){
                        // Just checking

                    }else{

                        return;
                    }

                    var keymap = {
                            tab: 9,
                            enter: 13,
                            esc: 27,
                            up: 38,
                            down: 40
                        },
                        hotkeys = [keymap.tab, keymap.enter, keymap.esc, keymap.up, keymap.down],
                        autocompleteService = new google.maps.places.AutocompleteService(),
                        placesService = new google.maps.places.PlacesService(element[0]);

                    (function init() {
                        $scope.query = '';
                        $scope.predictions = [];
                        $scope.input = element;
                        $scope.options = $scope.options || {};

                        initAutocompleteDrawer();
                        initEvents();
                        initNgModelController();
                    }());

                    function initEvents() {
                        element.bind('keydown', onKeydown);
                        element.bind('blur', onBlur);
                        element.bind('submit', onBlur);

                        $scope.$watch('selected', select);
                    }

                    function initAutocompleteDrawer() {
                        // Drawer element used to display predictions
                        var drawerElement = angular.element('<div g-places-autocomplete-drawer></div>'),
                            body = angular.element($document[0].body),
                            $drawer;

                        drawerElement.attr({
                            input: 'input',
                            query: 'query',
                            predictions: 'predictions',
                            active: 'active',
                            selected: 'selected'
                        });

                        $drawer = $compile(drawerElement)($scope);
                        body.append($drawer);  // Append to DOM
                    }

                    function initNgModelController() {
                        controller.$parsers.push(parse);
                        controller.$formatters.push(format);
                        controller.$render = render;
                    }

                    function onKeydown(event) {

                        if ($scope.predictions.length === 0 || indexOf(hotkeys, event.which) === -1) {
                            return;
                        }

                        event.preventDefault();

                        if (event.which === keymap.down) {
                            $scope.active = ($scope.active + 1) % $scope.predictions.length;
                            $scope.$digest();
                        } else if (event.which === keymap.up) {
                            $scope.active = ($scope.active ? $scope.active : $scope.predictions.length) - 1;
                            $scope.$digest();
                        } else if (event.which === 13 || event.which === 9) {
                            if ($scope.forceSelection) {
                                $scope.active = ($scope.active === -1) ? 0 : $scope.active;
                            }

                            $scope.$apply(function () {
                                $scope.selected = $scope.active;

                                if ($scope.selected === -1) {
                                    clearPredictions();
                                }
                            });
                        } else if (event.which === 27) {
                            $scope.$apply(function () {
                                event.stopPropagation();
                                clearPredictions();
                            });
                        }
                    }

                    function onBlur() {
                        if ($scope.predictions.length === 0) {
                            return;
                        }

                        if ($scope.forceSelection) {
                            $scope.selected = ($scope.selected === -1) ? 0 : $scope.selected;
                        }

                        $scope.$digest();

                        $scope.$apply(function () {
                            if ($scope.selected === -1) {
                                clearPredictions();
                            }
                        });
                    }

                    function select() {
                        var prediction;

                        prediction = $scope.predictions[$scope.selected];
                        if (!prediction) return;

                        if (prediction.is_custom) {
                            $scope.$apply(function () {
                                $scope.model = prediction.place;
                                $scope.$emit('g-places-autocomplete:select', prediction.place);
                                $timeout(function () {
                                    controller.$viewChangeListeners.forEach(function (fn) {fn();});
                                });
                            });
                        } else {
                            placesService.getDetails({ placeId: prediction.place_id }, function (place, status) {
                                if (status == google.maps.places.PlacesServiceStatus.OK) {
                                    $scope.$apply(function () {
                                        $scope.model = place;
                                        $scope.$emit('g-places-autocomplete:select', place);
                                        $timeout(function () {
                                            controller.$viewChangeListeners.forEach(function (fn) {fn();});
                                        });
                                    });
                                }
                            });
                        }

                        clearPredictions();
                    }

                    function parse(viewValue) {
                        var request;

                        if (!(viewValue && isString(viewValue))) return viewValue;

                        $scope.query = viewValue;

                        request = angular.extend({ input: viewValue }, $scope.options);
                        autocompleteService.getPlacePredictions(request, function (predictions, status) {
                            $scope.$apply(function () {
                                var customPlacePredictions;

                                clearPredictions();

                                if ($scope.customPlaces) {
                                    customPlacePredictions = getCustomPlacePredictions($scope.query);
                                    $scope.predictions.push.apply($scope.predictions, customPlacePredictions);
                                }

                                if (status == google.maps.places.PlacesServiceStatus.OK) {
                                    $scope.predictions.push.apply($scope.predictions, predictions);
                                }

                                if ($scope.predictions.length > 5) {
                                    $scope.predictions.length = 5;  // trim
																	// predictions
																	// down to
																	// size
                                }
                            });
                        });

                        if ($scope.forceSelection) {
                            return controller.$modelValue;
                        } else {
                            return viewValue;
                        }
                    }

                    function format(modelValue) {
                        var viewValue = "";

                        if (isString(modelValue)) {
                            viewValue = modelValue;
                        } else if (isObject(modelValue)) {
                            viewValue = modelValue.formatted_address;
                        }

                        return viewValue;
                    }

                    function render() {
                        return element.val(controller.$viewValue);
                    }

                    function clearPredictions() {
                        $scope.active = -1;
                        $scope.selected = -1;
                        $scope.predictions = [];
                    }

                    function getCustomPlacePredictions(query) {
                        var predictions = [],
                            place, match, i;

                        for (i = 0; i < $scope.customPlaces.length; i++) {
                            place = $scope.customPlaces[i];

                            match = getCustomPlaceMatches(query, place);
                            if (match.matched_substrings.length > 0) {
                                predictions.push({
                                    is_custom: true,
                                    custom_prediction_label: place.custom_prediction_label || '(Custom Non-Google Result)',  // required
																																// by
																																// https://developers.google.com/maps/terms
																																// §
																																// 10.1.1
																																// (d)
                                    description: place.formatted_address,
                                    place: place,
                                    matched_substrings: match.matched_substrings,
                                    terms: match.terms
                                });
                            }
                        }

                        return predictions;
                    }

                    function getCustomPlaceMatches(query, place) {
                        var q = query + '',  // make a copy so we don't
												// interfere with subsequent
												// matches
                            terms = [],
                            matched_substrings = [],
                            fragment,
                            termFragments,
                            i;

                        termFragments = place.formatted_address.split(',');
                        for (i = 0; i < termFragments.length; i++) {
                            fragment = termFragments[i].trim();

                            if (q.length > 0) {
                                if (fragment.length >= q.length) {
                                    if (startsWith(fragment, q)) {
                                        matched_substrings.push({ length: q.length, offset: i });
                                    }
                                    q = '';  // no more matching to do
                                } else {
                                    if (startsWith(q, fragment)) {
                                        matched_substrings.push({ length: fragment.length, offset: i });
                                        q = q.replace(fragment, '').trim();
                                    } else {
                                        q = '';  // no more matching to do
                                    }
                                }
                            }

                            terms.push({
                                value: fragment,
                                offset: place.formatted_address.indexOf(fragment)
                            });
                        }

                        return {
                            matched_substrings: matched_substrings,
                            terms: terms
                        };
                    }

                    function isString(val) {
                        return Object.prototype.toString.call(val) == '[object String]';
                    }

                    function isObject(val) {
                        return Object.prototype.toString.call(val) == '[object Object]';
                    }

                    function indexOf(array, item) {
                        var i, length;

                        if (array == null) return -1;

                        length = array.length;
                        for (i = 0; i < length; i++) {
                            if (array[i] === item) return i;
                        }
                        return -1;
                    }

                    function startsWith(string1, string2) {
                        return toLower(string1).lastIndexOf(toLower(string2), 0) === 0;
                    }

                    function toLower(string) {
                        return (string == null) ? "" : string.toLowerCase();
                    }

                }

            };


        }
    ])


    .directive('gPlacesAutocompleteDrawer', ['$window', '$document', function ($window, $document) {
        var TEMPLATE = [
            '<div class="pac-container" ng-if="isOpen()" ng-style="{top: position.top+\'px\', left: position.left+\'px\', width: position.width+\'px\'}" style="display: block;" role="listbox" aria-hidden="{{!isOpen()}}">',
            '  <div class="pac-item" g-places-autocomplete-prediction index="$index" prediction="prediction" query="query"',
            '       ng-repeat="prediction in predictions track by $index" ng-class="{\'pac-item-selected\': isActive($index) }"',
            '       ng-mouseenter="selectActive($index)" ng-click="selectPrediction($index)" role="option" id="{{prediction.id}}">',
            '  </div>',
            '</div>'
        ];

        return {
            restrict: 'A',
            scope:{
                input: '=',
                query: '=',
                predictions: '=',
                active: '=',
                selected: '='
            },
            template: TEMPLATE.join(''),
            link: function ($scope, element) {
                element.bind('mousedown', function (event) {
                    event.preventDefault();  // prevent blur event from
												// firing when clicking
												// selection
                });

                $window.onresize = function () {
                    $scope.$apply(function () {
                        $scope.position = getDrawerPosition($scope.input);
                    });
                };

                $scope.isOpen = function () {
                    return $scope.predictions.length > 0;
                };

                $scope.isActive = function (index) {
                    return $scope.active === index;
                };

                $scope.selectActive = function (index) {
                    $scope.active = index;
                };

                $scope.selectPrediction = function (index) {
                    $scope.selected = index;
                };

                $scope.$watch('predictions', function () {
                    $scope.position = getDrawerPosition($scope.input);
                }, true);

                function getDrawerPosition(element) {
                    var domEl = element[0],
                        rect = domEl.getBoundingClientRect(),
                        docEl = $document[0].documentElement,
                        body = $document[0].body,
                        scrollTop = $window.pageYOffset || docEl.scrollTop || body.scrollTop,
                        scrollLeft = $window.pageXOffset || docEl.scrollLeft || body.scrollLeft;

                    return {
                        width: rect.width,
                        height: rect.height,
                        top: rect.top + rect.height + scrollTop,
                        left: rect.left + scrollLeft
                    };
                }
            }
        };
    }])

    .directive('gPlacesAutocompletePrediction', [function () {
        var TEMPLATE = [
            '<span class="pac-icon pac-icon-marker"></span>',
            '<span class="pac-item-query" ng-bind-html="prediction | highlightMatched"></span>',
            '<span ng-repeat="term in prediction.terms | unmatchedTermsOnly:prediction">{{term.value | trailingComma:!$last}}&nbsp;</span>',
            '<span class="custom-prediction-label" ng-if="prediction.is_custom">&nbsp;{{prediction.custom_prediction_label}}</span>'
        ];

        return {
            restrict: 'A',
            scope:{
                index:'=',
                prediction:'=',
                query:'='
            },
            template: TEMPLATE.join('')
        };
    }])

    .filter('highlightMatched', ['$sce', function ($sce) {
        return function (prediction) {
            var matchedPortion = '',
                unmatchedPortion = '',
                matched;

            if (prediction.matched_substrings.length > 0 && prediction.terms.length > 0) {
                matched = prediction.matched_substrings[0];
                matchedPortion = prediction.terms[0].value.substr(matched.offset, matched.length);
                unmatchedPortion = prediction.terms[0].value.substr(matched.offset + matched.length);
            }

            return $sce.trustAsHtml('<span class="pac-matched">' + matchedPortion + '</span>' + unmatchedPortion);
        };
    }])

    .filter('unmatchedTermsOnly', [function () {
        return function (terms, prediction) {
            var i, term, filtered = [];

            for (i = 0; i < terms.length; i++) {
                term = terms[i];
                if (prediction.matched_substrings.length > 0 && term.offset > prediction.matched_substrings[0].length) {
                    filtered.push(term);
                }
            }

            return filtered;
        };
    }])

    .filter('trailingComma', [function () {
        return function (input, condition) {
            return (condition) ? input + ',' : input;
        };
    }]);



(function() {
    'use strict';

    angular
      .module('quoteUi')
      .directive('formatPhoneNumber', formatPhoneNumber);

    /**
	 * This Directive is used to restrict the symbol and alphabet + it formats
	 * the Business Phone in this format xxx-xxx-xxxx.
	 * 
	 * HOW TO USE IT? just paste the directive name in your input tag that
	 * contain phone number field ng-attr-format-phone-number="{{
	 * item.systemRefId == 'BusinessContact.BusinessPhone' ? true : false }}"
	 * and also this uservalue="{{vm.businessDetailsForm.uuid[item.uuid]}}" user
	 * value contain ng-model
	 * 
	 * Also use below attribute (if you are using paste directive)
	 * ng-paste="vm.pasteData = true" (make it false in ng-focus: plz check
	 * businessDetail page how to use it) pasteBoolean="{{vm.pasteData}}" (if
	 * you are using paste directive)
	 * 
	 * ALERT: if you have more to add, to make it more easy,efficient and
	 * dynamic, plz come forward and suggest.
	 * 
	 * Thank You.
	 * 
	 * Creator/Developer :- AnshulJS
	 */

    function formatPhoneNumber() {
      return {
        require: 'ngModel',
        link: function(scope, el, attrs,modelCtrl) {
            var phoneData;


          function modelToViewChangeOnPageLoad(phoneData) {
            if (phoneData && phoneData.length == 10 && phoneData.indexOf('-') == -1) {
              phoneData = phoneData.substr(0, 3) + '-' + phoneData.substr(3, 3) + '-' + phoneData.substr(6);
            }
            return phoneData;
          }
          modelCtrl.$formatters.push(modelToViewChangeOnPageLoad);

          function viewToModelChangeOnUserInput(phoneData) {

            return phoneData.replace(/-/g,'');
          }
          modelCtrl.$parsers.push(viewToModelChangeOnUserInput);



            if (attrs.formatPhoneNumber == 'true') {

              var ctrlDown = false, // check ctrl key (window) or cmd key (Mac)
									// is press for copy/paste
                  ctrlKey = 17,     // for ctrl key (window)
                  cmdKey = 91,      // for cmd key (mac)
                  vKey = 86,        // check 'c' key is press for copy
                  cKey = 67,        // check 'v' key is press for paste
                  xKey = 88,        // check 'x' key is press for cut
                  aKey = 65;        // check 'a' key is press to select the
									// content

              /**
				 * 
				 * This is for that case if suppoose user paste 10 digit phone
				 * number like this 1534689855 then it will automatically add
				 * '-' in it like this 153-468-9855 on ng-blur (when user loose
				 * focus from input)
				 */
            el.bind('blur', function() {
                phoneData = attrs.uservalue;
                // console.log('blur',attrs.uservalue);

              if(phoneData && phoneData.length == 10 && phoneData.indexOf('-') == -1){
                modelCtrl.$setViewValue(phoneData.substr(0,3) + '-' + phoneData.substr(3,3) + '-' +phoneData.substr(6));
                modelCtrl.$render();
              }

            });// End of Blue Event

            /**
			 * This function format the business phone in this xxx-xxx-xxxx User
			 * can add '-' hyphen manually OR it will add automatically
			 */
            el.bind('keyup', function($event) {
            // console.log('keyup',$event.key);
            // console.log('keyup',attrs.uservalue);
            phoneData = attrs.uservalue;

              /**
				 * If the ctr key in window or cmd key in mac is release then
				 * make ctrlDown = false which when control key or cmd key is
				 * released on key up
				 */
              if ($event.keyCode == ctrlKey ||   // check ctl key press in
													// window
                  $event.keyCode == cmdKey) {    // check cmd key press in
													// mac

                  ctrlDown = false;
              }

              if($event.keyCode != 8 && phoneData && (phoneData.replace(/[^\d]/g, '').length >3 && phoneData.replace(/[^\d]/g, '').length <= 5) && phoneData.indexOf('-') == -1){

                modelCtrl.$setViewValue(phoneData.substr(0,3) + '-' + phoneData.substr(3));
                modelCtrl.$render();
                return;
              }
              if($event.keyCode != 8 && phoneData && phoneData.replace(/[^\d]/g, '').length > 5 && phoneData.replace(/[^\d]/g, '').length < 11 && phoneData.indexOf('-') == -1){

                modelCtrl.$setViewValue(phoneData.substr(0,3) + '-' + phoneData.substr(3,3) + '-' +phoneData.substr(6));
                modelCtrl.$render();
                return;
              }

              if(phoneData && (phoneData.replace(/[^\d]/g, '').length == 3 ||
                              phoneData.replace(/[^\d]/g, '').length == 4 ||
                              phoneData.replace(/[^\d]/g, '').length == 6 ||
                              phoneData.replace(/[^\d]/g, '').length == 7) && $event && $event.keyCode != 8){

                if(phoneData.replace(/[^\d]/g, '').length == 3){

                    modelCtrl.$setViewValue(phoneData.substr(0,3) + '-');
                    modelCtrl.$render();
                    return;
                }

                if(phoneData.replace(/[^\d]/g, '').length == 4 && phoneData.replace(/[^-]/g, '').length == 0){
                    modelCtrl.$setViewValue(phoneData.substr(0,3) + '-' + phoneData.substr(3));
                    modelCtrl.$render();
                    return;
                }

                if(phoneData.replace(/[^\d]/g, '').length == 6 && phoneData.replace(/[^-]/g, '').length == 1){
                    modelCtrl.$setViewValue(phoneData + '-');
                    modelCtrl.$render();
                  return;
                }
                if(phoneData.replace(/[^\d]/g, '').length == 7 && phoneData.replace(/[^-]/g, '').length == 1){
                    modelCtrl.$setViewValue(phoneData.substr(0,7) + '-' + phoneData.substr(7));
                    modelCtrl.$render();
                  return;
                }

              }

            }); // End of Keyup

            /**
			 * Since Business Phone field have hyphen symbol '-' so to restrict
			 * character and symbol we made condition in keydown function
			 */
          el.bind('keydown', function($event) {
            // console.log('keydown',$event.key);

            /**
			 * If the ctr key in window or cmd key in mac is press then make
			 * ctrlDown = true
			 */
            if ($event.keyCode == ctrlKey ||  // check ctl key press in window
                $event.keyCode == cmdKey) {   // check cmd key press in mac

                ctrlDown = true;
            }
            /**
			 * If ctrlDown = true which mean ctl/cmd key is already pressed then
			 * check if 'c' key is press for copy check if 'v' key is press for
			 * paste check if 'x' key is press for cut check if 'a' key is press
			 * to highlight content
			 */
            if (ctrlDown &&                 // ctl/cmd key press
               ($event.keyCode == vKey ||   // 'v' key press for paste
                $event.keyCode == cKey ||   // 'c' key press for copy
                $event.keyCode == aKey ||   // 'a' key press for selecting
											// content
                $event.keyCode == xKey)) {  // 'x' key press for cut

              return false;
            }

          // allow only digits
          if (
            ($event.keyCode < 96 && $event.keyCode > 105) ||
            ($event.keyCode >= 65 && $event.keyCode <= 90) ||
            $event.keyCode > 105
          ) {
            $event.preventDefault();
          }
          switch ($event.key) {
            case '!':
            case '@':
            case '#':
            case '$':
            case '%':
            case '^':
            case '&':
            case '*':
            case '(':
            case ')':
              $event.preventDefault();
              break;
          }

          }); // End of KeyDown
           /**
			 * This is for Chrome Browser. When user type zipcode and reach the
			 * max character limit, at that time if user select the complete
			 * text and then if user type again zipcode then
			 * selected/highlighted zipcode must be removed with the new one
			 * that user entered.
			 */
           if (window.getSelection()) {
            var selecttxt = window.getSelection().toString();
            if (selecttxt) {
              return;
            }
          }
          /**
			 * This is for other Browser. When user type zipcode and reach the
			 * max character limit, at that time if user select the complete
			 * text and then if user type again zipcode then
			 * selected/highlighted zipcode must be removed with the new one
			 * that user entered.
			 */
          var text = attrs.uservalue;
          var selectText = text.valueOf().substr(text.selectionStart,text.selectionEnd - text.selectionStart);
          if (selectText) {
            return;
          }

          }// End of if TRUE
        } // End of link function

      };  // End of return
    }// End of formatPhoneNumber function
  })();

(function () {
  'use strict';
  angular.module ('quoteUi').directive ('formatCurrency', formatCurrency);

  /**
	 * 
	 * This Directive is used to restrict the symbol and alphabet + it change
	 * the annual sales in this format $100,000,000.
	 * 
	 * HOW TO USE IT? just paste the directive name in your currency tag as
	 * shown below ng-attr-format-currency="{{ item.systemRefId ==
	 * 'misc.TotalAnnualSales' ? true : false }}" and also this
	 * uservalue="{{vm.businessDetailsForm.uuid[item.uuid]}}" user value contain
	 * ng-model
	 * 
	 * Also use below attribute (if you are using paste directive)
	 * ng-paste="vm.pastecurrency = true" (make it false in ng-focus: plz check
	 * businessDetail page how to use it) pasteBoolean="{{vm.pastecurrency}}"
	 * (if you are using paste directive)
	 * 
	 * ALERT: if you have more to add, to make it more easy,efficient and
	 * dynamic, plz come forward and suggest.
	 * 
	 * Thank You.
	 * 
	 * Creator/Developer :- AnshulJS
	 */

  function formatCurrency () {
    return {
      require: 'ngModel',
      link: function (scope, el, attrs, modelCtrl) {
        var currency;

        if (attrs.formatCurrency == 'true') {
          /**
			 * 
			 * when user lose focus from annual sale currency field then it
			 * convert the annual sale in this format $100,000,000;
			 */
          el.bind ('blur', function () {
            currency = attrs.uservalue;
            if (currency.replace (/[^\d]/g, '').length == 0) {
              modelCtrl.$setViewValue (null);
              modelCtrl.$render ();
              return;
            }
            if (currency.indexOf ('-') == 0) {
              currency = currency.replace (/[^\d.]/g, ''); // Keep Number Only
              currency = parseFloat (currency);
              currency = currency.toFixed (currency % 1 === 0 ? 0 : 2);
              modelCtrl.$setViewValue (
                '-$' +
                  currency.toString ().replace (/\B(?=(\d{3})+(?!\d))/g, ',')
              );
              modelCtrl.$render ();
              return;
            }
            currency = currency.replace (/[^\d.]/g, ''); // Keep Number Only
            currency = parseFloat (currency);
            currency = currency.toFixed (currency % 1 === 0 ? 0 : 2);
            modelCtrl.$setViewValue (
              '$' + currency.toString ().replace (/\B(?=(\d{3})+(?!\d))/g, ',')
            );
            modelCtrl.$render ();
            return;
          }); // End of Blue Event

          /**
			 * when user focus on the annual sale field it will remove $ and ,
			 * symbol from it.
			 */
          el.bind ('focus', function () {
            currency = attrs.uservalue;
            if (currency) {
              currency = currency.toString ().replace ('$', '');
              currency = currency.toString ().replace (/,/g, '');
              modelCtrl.$setViewValue (currency);
              modelCtrl.$render ();
              return;
            }
          });
          el.bind ('keydown', function () {}); // End of Keyup

          /**
			 * Restrict user to enter alphabet and symbol
			 */
          el.bind ('keypress', function ($event) {
            // console.log('keydown',$event.key);
            currency = attrs.uservalue;
            // console.log($event.keyCode);

            if ($event.key == '-') {
              if ($event.target.selectionStart == 0) {
                // allow minus sumbol (-) to be only in 1 position
                return;
              } else {
                $event.preventDefault ();
              }
            }

            // exception characters that are allowed in annual sales
            if (
              $event.key == ',' || // allow any number of comma
              ($event.key == '.' && currency.replace (/[^/.]/g, '').length == 0) // allow
																					// only
																					// one
																					// dot
																					// (.)
																					// for
																					// decimal
																					// number
            ) {
              return;
            }
            // allow only digits
            if (
              ($event.keyCode > 96 && $event.keyCode < 123) || // for small
																// alphabet
              ($event.keyCode < 96 && $event.keyCode > 105) ||
              ($event.keyCode >= 65 && $event.keyCode <= 90) || // for capital
																// alphabet
              $event.keyCode > 105
            ) {
              $event.preventDefault ();
            }
            switch ($event.key) {
              case '!':
              case '@':
              case '#':
              case '$':
              case '%':
              case '^':
              case '&':
              case '*':
              case '(':
              case ')':
              case '=':
              case '+':
              case '[':
              case ']':
              case '\\':
              case '"':
              case "'":
              case ';':
              case ':':
              case '?':
              case '/':
              case '>':
              case '<':
              case '_':
              case '.': // after one dot (.) is entered it block other dot (.)to
						// be enter
                $event.preventDefault ();
                break;
            }
          }); // End of KeyDown
          /**
			 * This is for Chrome Browser. When user type zipcode and reach the
			 * max character limit, at that time if user select the complete
			 * text and then if user type again zipcode then
			 * selected/highlighted zipcode must be removed with the new one
			 * that user entered.
			 */
          if (window.getSelection ()) {
            var selecttxt = window.getSelection ().toString ();
            if (selecttxt) {
              return;
            }
          }
          /**
			 * This is for other Browser. When user type zipcode and reach the
			 * max character limit, at that time if user select the complete
			 * text and then if user type again zipcode then
			 * selected/highlighted zipcode must be removed with the new one
			 * that user entered.
			 */
          var text = attrs.uservalue;
          var selectText = text
            .valueOf ()
            .substr (
              text.selectionStart,
              text.selectionEnd - text.selectionStart
            );
          if (selectText) {
            return;
          }
        } // End of if TRUE
      }, // End of link function
    }; // End of return
  } // End of formatPhoneNumber function
}) ();

(function () {
  'use strict';
  angular
    .module ('quoteUi')
    .directive ('formatCurrencyLocation', formatCurrencyLocation);

  /**
	 * 
	 * This Directive is used to restrict the symbol and alphabet + it change
	 * the annual sales in this format $100,000,000.
	 * 
	 * HOW TO USE IT? just paste the directive name in your currency tag as
	 * shown below ng-attr-format-currency="{{ item.systemRefId ==
	 * 'misc.TotalAnnualSales' ? true : false }}" and also this
	 * uservalue="{{vm.businessDetailsForm.uuid[item.uuid]}}" user value contain
	 * ng-model
	 * 
	 * Also use below attribute (if you are using paste directive)
	 * ng-paste="vm.pastecurrency = true" (make it false in ng-focus: plz check
	 * businessDetail page how to use it) pasteBoolean="{{vm.pastecurrency}}"
	 * (if you are using paste directive)
	 * 
	 * ALERT: if you have more to add, to make it more easy,efficient and
	 * dynamic, plz come forward and suggest.
	 * 
	 * Thank You.
	 * 
	 * Creator/Developer :- AnshulJS
	 */

  function formatCurrencyLocation () {
    return {
      require: 'ngModel',
      link: function (scope, el, attrs, modelCtrl) {
        var currency;

        if (attrs.formatCurrencyLocation == 'true') {
          /**
			 * 
			 * when user lose focus from annual sale currency field then it
			 * convert the annual sale in this format $100,000,000;
			 */
          el.bind ('blur', function () {
            currency = attrs.uservalue;
            if (currency.replace (/[^\d]/g, '').length == 0) {
              modelCtrl.$setViewValue (null);
              modelCtrl.$render ();
              return;
            }
            if (currency.indexOf ('-') == 0) {
              currency = currency.replace (/[^\d]/g, ''); // Keep Number Only
              currency = parseFloat (currency);
              currency = currency.toFixed (currency % 1 === 0 ? 0 : 2);
              modelCtrl.$setViewValue (
                '-$' +
                  currency.toString ().replace (/\B(?=(\d{3})+(?!\d))/g, ',')
              );
              modelCtrl.$render ();
              return;
            }
            currency = currency.replace (/[^\d]/g, ''); // Keep Number Only
            currency = parseFloat (currency);
            currency = currency.toFixed (currency % 1 === 0 ? 0 : 2);
            modelCtrl.$setViewValue (
              '$' + currency.toString ().replace (/\B(?=(\d{3})+(?!\d))/g, ',')
            );
            modelCtrl.$render ();
            return;
          }); // End of Blue Event

          /**
			 * when user focus on the annual sale field it will remove $ and ,
			 * symbol from it.
			 */
          el.bind ('focus', function () {
            currency = attrs.uservalue;
            if (currency) {
              currency = currency.toString ().replace ('$', '');
              currency = currency.toString ().replace (/,/g, '');
              modelCtrl.$setViewValue (currency);
              modelCtrl.$render ();
              return;
            }
          });
          el.bind ('keydown', function () {}); // End of Keyup

          /**
			 * Restrict user to enter alphabet and symbol
			 */
          el.bind ('keypress', function ($event) {
            // console.log('keydown',$event.key);
            currency = attrs.uservalue;
            // console.log($event.keyCode);

            if ($event.key == '-') {
              if ($event.target.selectionStart == 0) {
                // allow minus sumbol (-) to be only in 1 position
                return;
              } else {
                $event.preventDefault ();
              }
            }

            // exception characters that are allowed in annual sales
            if (
              $event.key == ',' // allow any number of comma
            ) {
              return;
            }
            // allow only digits
            if (
              ($event.keyCode > 96 && $event.keyCode < 123) || // for small
																// alphabet
              ($event.keyCode < 96 && $event.keyCode > 105) ||
              ($event.keyCode >= 65 && $event.keyCode <= 90) || // for capital
																// alphabet
              $event.keyCode > 105
            ) {
              $event.preventDefault ();
            }
            switch ($event.key) {
              case '!':
              case '@':
              case '#':
              case '$':
              case '%':
              case '^':
              case '&':
              case '*':
              case '(':
              case ')':
              case '=':
              case '+':
              case '[':
              case ']':
              case '\\':
              case '"':
              case "'":
              case ';':
              case ':':
              case '?':
              case '/':
              case '>':
              case '<':
              case '_':
              case '.': // after one dot (.) is entered it block other dot (.)to
						// be enter
                $event.preventDefault ();
                break;
            }
          }); // End of KeyDown
          /**
			 * This is for Chrome Browser. When user type zipcode and reach the
			 * max character limit, at that time if user select the complete
			 * text and then if user type again zipcode then
			 * selected/highlighted zipcode must be removed with the new one
			 * that user entered.
			 */
          if (window.getSelection ()) {
            var selecttxt = window.getSelection ().toString ();
            if (selecttxt) {
              return;
            }
          }
          /**
			 * This is for other Browser. When user type zipcode and reach the
			 * max character limit, at that time if user select the complete
			 * text and then if user type again zipcode then
			 * selected/highlighted zipcode must be removed with the new one
			 * that user entered.
			 */
          var text = attrs.uservalue;
          var selectText = text
            .valueOf ()
            .substr (
              text.selectionStart,
              text.selectionEnd - text.selectionStart
            );
          if (selectText) {
            return;
          }
        } // End of if TRUE
      }, // End of link function
    }; // End of return
  } // End of formatPhoneNumber function
}) ();

(function() {
  'use strict';

  angular
    //
    .module('quoteUi')
    .directive('formSubmitValidation', function() {
      function fixVm(form) {
        angular.forEach(form, function(control, name) {
          // Excludes internal angular properties
          if (typeof name === 'string' && name.charAt(0) !== '$') {
            // To display ngMessages
            if (control.$setTouched) {
              control.$setTouched();
            }
            // Runs each of the registered validators
            if (control.$validate) {
              control.$validate();
            }
          }
        });
      }

      function setAllInputsDirty(scope) {
        angular.forEach(scope, function(value) {
          // We skip non-form and non-inputs
          if (!value || value.$dirty === undefined) {
            return;
          }
          // Recursively applying same method on all forms included in the
			// form
          if (value.$addControl) {
            return setAllInputsDirty(value);
          }
          if (value.$setTouched) {
            value.$setTouched();
          }
          // Runs each of the registered validators
          if (value.$validate) {
            value.$validate();
          }
          // Setting inputs to $dirty, but re-applying its content in itself
          if (value.$setViewValue) {
            return value.$setViewValue(value.$viewValue);
          }
        });
      }

      return {
        require: 'form',
        compile: function(tElem) {
          tElem.data('augmented', true);

          return function(scope, elem, attr, form) {
            elem.on('submit', function() {
              scope.$broadcast('form:submit', form);
              if (!form.$valid) {
                // $event.preventDefault();
              }
              // force all the forms element to validate

              var formName = '';
              if (form.$name.indexOf('vm.') > -1) {
                fixVm(form);
                // vm.formName -> $scope.formName
                formName = form.$name.substr(3);
              } else {
                setAllInputsDirty(scope);
                formName = form.$name;
              }
              scope.$apply(function() {
                // $scope.formName
                if (scope[formName]) {
                  scope[formName].submitted = true;
                } else {
                  // vm.formName -> $scope.formName
                  scope[formName] = {};
                  scope[formName].submitted = true;
                }
              });
            });
          };
        }
      };
    });
})();

(function () {
  'use strict';

  angular
    // date parse directive
    .module('quoteUi')
    .directive('dateVal', function ($parse, $log) {
      return {
        require: '?ngModel',
        link: function (scope, elm, attrs, ngModel) {
          var getter = $parse(attrs.ngModel);
          var setter = getter.assign;
          if (ngModel) {
            var val = getter(scope);
            var paramsData = val;
            $log.info('d1', val);
            var newVal = new Date(newVal);
            $log.info('d2', newVal);
            if (newVal == 'Invalid Date') {
              val = moment(val, 'MM/DD/YYYY').format('YYYY-MM-DD');
              if (val == 'Invalid date') {
                val = moment(paramsData, 'YYYY-MM-DD').format('YYYY-MM-DD');;
              }
              $log.info('d3', val);
            }
            // val = moment(val, 'MM/DD/YYYY').format('MM/DD/YYYY');
            if (val != 'Invalid date') {
              $log.info('d4', new Date(moment.parseZone(val).format('LLL')));
              setter(scope, new Date(moment.parseZone(val).format('LLL')));
            }
            // setter(scope, val);

            // prefix with zero
            elm.on('blur', function () {
              val = elm.val();
              $log.info('d5', val);
              // say nothing if format not with mm/dd/yyyy
              // if (!/^\d[1-12]\/\d[1-31]\/\d{4}$/.test(val)) {
              // return false;
              // }
              val = moment(val, 'MM/DD/YYYY').format('MM/DD/YYYY');
              $log.info('d6', val);
              if (!moment(val, 'MM/DD/YYYY').isValid()) {
                return false;
              }

              // prefix and parse to date
              if (val.length >= 8) {
                val = val.replace(/(^|\D)(\d)(?!\d)/g, '$10$2');
                $log.info('d7', val);
                if (val instanceof Date == false) {
                  val = moment(val, 'MM/DD/YYYY').format('YYYY-MM-DD');
                  $log.info('d8', val);
                }
                if (val != 'Invalid date') {
                  $log.info('d9', new Date(moment.parseZone(val).format('LLL')));
                  setter(scope, new Date(moment.parseZone(val).format('LLL')));
                }
                // setter(scope, val);
              }
            });
            // allow only digits, tab, enter, forward slash, delete, backspace
			// and arrow keys only
            elm.on('keydown', function (event) {
              var allowedKeys = [
                8, // backspace
                9, // tab
                13, // enter
                37, // left arrow
                38, // up arrow
                39, // right arrow
                40, // down arrow
                48, // 0
                49, // 1
                50, // 2
                51, // 3
                52, // 4
                53, // 5
                54, // 6
                55, // 7
                56, // 8
                57, // 9
                96, // 0
                97, // 1
                98, // 2
                99, // 3
                100, // 4
                101, // 5
                102, // 6
                103, // 7
                104, // 8
                105, // 9
                111, // back slash
                191 // back slash
              ];
              if (allowedKeys.indexOf(event.keyCode) == -1 || event.shiftKey) {
                event.preventDefault();
                return false;
              }
            });
          }
        }
      };
    });
})();

(function() {
  'use strict';

  angular
    //
    .module('quoteUi')
    .directive('myDirective', function() {
      return {
        link: function() {
          // convert the attributes to object and get its properties
          // var attributes = scope.$eval(attrs.myDirective);
        }
      };
    });
})();

(function () {
  'use strict';

  angular
    .module('quoteUi')
    .directive('changeViewModel', function ($parse) {


      /**
		 * 
		 * NOTE - IN PROGESS - DONT'T USE IT.
		 * 
		 * This Directive is used to change view based on Model in particular
		 * input tag. For example for Business Phone field, format we have for
		 * view is like this xxx-xxx-xxxx but in Duckcreek it should be like
		 * this xxxxxxxxxx. For ex. In UI Business Phone look like: 123-123-1233
		 * But in DB it goes like this: 1231231233 (only digit without '-')
		 * 
		 * HOW TO USE IT? just paste the directive name (change-view-model) in
		 * your input tag
		 * 
		 * NOTE : Here we have 2 length for business Phone 1) for view (UI), we
		 * have 12 character which include dash symbol '-' and digits. 2) for
		 * model (DB), we have 10 character which only have digits.
		 * 
		 * ng-maxlength="{ 10:item.systemRefId ==
		 * 'BusinessContact.BusinessPhone' }" SO, Instead of making Business
		 * Phone to 12 character in ng-maxlength attribute, make it 10
		 * character.
		 * 
		 * ONE MORE THING :IN PROGRESS
		 * 
		 * ALERT: if you have more to add, to make it more easy,efficient and
		 * dynamic, plz come forward and suggest.
		 * 
		 * Thank You.
		 * 
		 * Creator/Developer :- AnshulJS
		 */

      return {
        require: 'ngModel',
        restrict: 'A',

        link: function (scope, element, attrs, modelCtrl) {

          var tempViewValue;

          function howViewLook(viewValue) {


              tempViewValue = viewValue.replace(/[^\d]/g, '');

              modelCtrl.$modelValue = tempViewValue;

              modelCtrl.$viewValue = viewValue;

              return viewValue;
          }

          modelCtrl.$viewChangeListeners.push(function () {
            /* Set model value differently based on the viewvalue entered */
            if (tempViewValue){
              modelCtrl.$modelValue = tempViewValue;
            }
            $parse(attrs.ngModel).assign(scope, modelCtrl.$modelValue);
          });

          modelCtrl.$parsers.push(howViewLook);


        }  // End of link function
      };  // End of return
    });  // End of directive
})();

(function() {
  'use strict';

  angular
    // number format directive - usage: amount="true"
    .module('quoteUi')
    .directive('amount', Amount);

  Amount.$inject = ['$filter'];

  function Amount($filter) {
    return {
      require: '^ngModel',
      link: function(scope, el, attrs, ngModel) {
        function formatter(value) {
          value = value
            ? parseFloat(value.toString().replace(/[^0-9._-]/g, '')) || 0
            : 0;
          if (value > 0) {
            var formattedValue = $filter('number')(value);
            el.val(formattedValue);
            return formattedValue;
          }
        }

        if (ngModel && attrs.amount == 'true') {
          ngModel.$formatters.push(formatter);

          el.bind('blur', function() {
            formatter(el.val());
          });
        }
      }
    };
  }
})();


(function() {
  'use strict';

  angular
    .module('quoteUi')
    // veriskPhoneFormat filter - Whatever phone number we get from verisk
	// should be changed into phone format
    .filter('veriskPhoneFormat', function() {
      return function(input) {
        // console.log(input.toString().substring(0, 3) + '-' +
		// input.substring(3, 6) )
        if (input && input != 'No data available'){
          return input.toString().substring(0, 3) + '-' + input.toString().substring(3, 6) + '-' + input.toString().substring( 6);
        }
        return input;
      };
    });
})();

(function () {
  'use strict';

  angular
    .module('quoteUi')
    // veriskPhoneFormat filter - Whatever Square Footage we get from verisk
	// should be comma separated
    .filter('squareFootageCommaSeparator', function () {
      return function (input) {

        if (input && input != 'No data available') {
          return input.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');

        }
        return input;
      };
    });
})();


(function() {
    'use strict';
  
    angular
      .module('quoteUi').filter('dotWithUnderscore', function () {
    return function (input) {
        return input.replace(/[.-]/g, "_");
    };
  });
 })();
(function() {
  'use strict';

  angular
    //
    .module('quoteUi')
    .filter('removeColon', function() {
      return function(input) {
        var result = '';
        result = input.split(':') || '';
        if (result.length == 2) {
          return result[1];
        } else {
          return result;
        }
      };
    })
    .filter('removeQuote', function() {
      return function(input) {
        var result = '';
        result = input.split(':') || '';
        if (result.length == 1) {
          return result[0];
        } else {
          return result;
        }
      };
    });
})();


(function() {
  'use strict';

  angular
    .module('quoteUi')
    // camelcase filter (Error message label should be camelCase format)
    .filter('camelCase', function() {
      return function(input) {
        input = input || '';
        return input.replace(/\w\S*/g, function(txt) {

          
          if(txt.indexOf('/') != -1){

            // console.log(txt.indexOf('/'));
            return (txt.charAt(0).toUpperCase() + txt.substr(1,txt.indexOf('/')).toLowerCase()+ txt.charAt(txt.indexOf('/')+1).toUpperCase() +txt.substr(txt.indexOf('/')+2).toLowerCase());
            
          }
          return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
      };
    });
})();

(function() {
  'use strict';
  angular.module('ngMask', []);
})();
(function() {
  'use strict';
  angular.module('ngMask').directive('mask', [
    '$log',
    '$timeout',
    'MaskService',
    function($log, $timeout, MaskService) {
      return {
        restrict: 'A',
        require: 'ngModel',
        compile: function($element, $attrs) {
          if (!$attrs.mask || !$attrs.ngModel) {
            $log.info('Mask and ng-model attributes are required!');
            return;
          }
          var maskService = MaskService.create();
          var timeout;
          var promise;

          function setSelectionRange(selectionStart) {
            if (typeof selectionStart !== 'number') {
              return;
            }

            // using $timeout:
            // it should run after the DOM has been manipulated by Angular
            // and after the browser renders (which may cause flicker in some
			// cases)
            $timeout.cancel(timeout);
            timeout = $timeout(function() {
              var selectionEnd = selectionStart + 1;
              var input = $element[0];

              if (input.setSelectionRange) {
                input.focus();
                input.setSelectionRange(selectionStart, selectionEnd);
              } else if (input.createTextRange) {
                var range = input.createTextRange();

                range.collapse(true);
                range.moveEnd('character', selectionEnd);
                range.moveStart('character', selectionStart);
                range.select();
              }
            });
          }

          return {
            pre: function($scope, $element, $attrs) {
              promise = maskService.generateRegex({
                mask: $attrs.mask,
                // repeat mask expression n times
                repeat: $attrs.repeat || $attrs.maskRepeat,
                // clean model value - without divisors
                clean: ($attrs.clean || $attrs.maskClean) === 'true',
                // limit length based on mask length
                limit: ($attrs.limit || $attrs.maskLimit || 'true') === 'true',
                // how to act with a wrong value
                restrict: $attrs.restrict || $attrs.maskRestrict || 'select', // select,
																				// reject,
																				// accept
                // set validity mask
                validate:
                  ($attrs.validate || $attrs.maskValidate || 'true') === 'true',
                // default model value
                model: $attrs.ngModel,
                // default input value
                value: $attrs.ngValue
              });
            },
            post: function($scope, $element, $attrs, controller) {
              var timeout;
              var options = maskService.getOptions();

              function parseViewValue(value) {
                var untouchedValue = value;
                options = maskService.getOptions();
                // set default value equal 0
                value = value || '';

                // get view value object
                var viewValue = maskService.getViewValue(value);

                // get mask without question marks
                var maskWithoutOptionals =
                  options['maskWithoutOptionals'] || '';

                // get view values capped
                // used on view
                var viewValueWithDivisors = viewValue.withDivisors(true);
                // used on model
                var viewValueWithoutDivisors = viewValue.withoutDivisors(true);

                try {
                  // get current regex
                  var regex = maskService.getRegex(
                    viewValueWithDivisors.length - 1
                  );
                  var fullRegex = maskService.getRegex(
                    maskWithoutOptionals.length - 1
                  );

                  // current position is valid
                  var validCurrentPosition =
                    regex.test(viewValueWithDivisors) ||
                    fullRegex.test(viewValueWithDivisors);

                  // difference means for select option
                  var diffValueAndViewValueLengthIsOne =
                    value.length - viewValueWithDivisors.length === 1;
                  var diffMaskAndViewValueIsGreaterThanZero =
                    maskWithoutOptionals.length - viewValueWithDivisors.length >
                    0;

                  if (options.restrict !== 'accept') {
                    if (
                      options.restrict === 'select' &&
                      (!validCurrentPosition ||
                        diffValueAndViewValueLengthIsOne)
                    ) {
                      var lastCharInputed = value[value.length - 1];
                      var lastCharGenerated =
                        viewValueWithDivisors[viewValueWithDivisors.length - 1];

                      if (
                        lastCharInputed !== lastCharGenerated &&
                        diffMaskAndViewValueIsGreaterThanZero
                      ) {
                        viewValueWithDivisors =
                          viewValueWithDivisors + lastCharInputed;
                      }

                      var wrongPosition = maskService.getFirstWrongPosition(
                        viewValueWithDivisors
                      );
                      if (angular.isDefined(wrongPosition)) {
                        setSelectionRange(wrongPosition);
                      }
                    } else if (
                      options.restrict === 'reject' &&
                      !validCurrentPosition
                    ) {
                      viewValue = maskService.removeWrongPositions(
                        viewValueWithDivisors
                      );
                      viewValueWithDivisors = viewValue.withDivisors(true);
                      viewValueWithoutDivisors = viewValue.withoutDivisors(
                        true
                      );

                      // setSelectionRange(viewValueWithDivisors.length);
                    }
                  }

                  if (!options.limit) {
                    viewValueWithDivisors = viewValue.withDivisors(false);
                    viewValueWithoutDivisors = viewValue.withoutDivisors(false);
                  }

                  // Set validity
                  if (options.validate && controller.$dirty) {
                    if (
                      fullRegex.test(viewValueWithDivisors) ||
                      controller.$isEmpty(untouchedValue)
                    ) {
                      controller.$setValidity('mask', true);
                    } else {
                      controller.$setValidity('mask', false);
                    }
                  }

                  // Update view and model values
                  if (value !== viewValueWithDivisors) {
                    controller.$setViewValue(
                      angular.copy(viewValueWithDivisors),
                      'input'
                    );
                    controller.$render();
                  }
                } catch (e) {
                  $log.error('[mask - parseViewValue]');
                  throw e;
                }

                // Update model, can be different of view value
                if (options.clean) {
                  return viewValueWithoutDivisors;
                } else {
                  return viewValueWithDivisors;
                }
              }

              var callParseViewValue = function() {
                parseViewValue();

                controller.$parsers.push(parseViewValue);

                // $evalAsync from a directive
                // it should run after the DOM has been manipulated by Angular
                // but before the browser renders
                if (options.value) {
                  $scope.$evalAsync(function() {
                    controller.$setViewValue(
                      angular.copy(options.value),
                      'input'
                    );
                    controller.$render();
                  });
                }
              };

              $element.on('click input paste keyup', function() {
                timeout = $timeout(function() {
                  // Manual debounce to prevent multiple execution
                  $timeout.cancel(timeout);

                  parseViewValue($element.val());
                  $scope.$apply();
                }, 100);
              });

              // Register the watch to observe remote loading or promised data
              // Deregister calling returned function
              var watcher = $scope.$watch($attrs.ngModel, function(newValue) {
                if (angular.isDefined(newValue)) {
                  parseViewValue(newValue);
                  watcher();
                }
              });

              $scope.$watch(
                function() {
                  return [$attrs.mask];
                },
                function() {
                  promise = maskService
                    .generateRegex({
                      mask: $attrs.mask,
                      // repeat mask expression n times
                      repeat: $attrs.repeat || $attrs.maskRepeat,
                      // clean model value - without divisors
                      clean: ($attrs.clean || $attrs.maskClean) === 'true',
                      // limit length based on mask length
                      limit:
                        ($attrs.limit || $attrs.maskLimit || 'true') === 'true',
                      // how to act with a wrong value
                      restrict:
                        $attrs.restrict || $attrs.maskRestrict || 'select', // select,
																			// reject,
																			// accept
                      // set validity mask
                      validate:
                        ($attrs.validate || $attrs.maskValidate || 'true') ===
                        'true',
                      // default model value
                      model: $attrs.ngModel,
                      // default input value
                      value: $attrs.ngValue
                    })
                    .then(function() {
                      $element.triggerHandler('click');
                    });

                  promise.then(callParseViewValue);
                },
                true
              );

              promise.then(callParseViewValue);
            }
          };
        }
      };
    }
  ]);
})();
(function() {
  'use strict';
  angular.module('ngMask').factory('MaskService', [
    '$q',
    'OptionalService',
    'UtilService',
    function($q, OptionalService, UtilService) {
      function create() {
        var options;
        var maskWithoutOptionals;
        var maskWithoutOptionalsLength = 0;
        var maskWithoutOptionalsAndDivisorsLength = 0;
        var optionalIndexes = [];
        var optionalDivisors = {};
        var optionalDivisorsCombinations = [];
        var divisors = [];
        var divisorElements = {};
        var regex = [];
        var patterns = {
          '9': /[0-9]/,
          '8': /[0-8]/,
          '7': /[0-7]/,
          '6': /[0-6]/,
          '5': /[0-5]/,
          '4': /[0-4]/,
          '3': /[0-3]/,
          '2': /[0-2]/,
          '1': /[0-1]/,
          '0': /[0]/,
          '*': /./,
          w: /\w/,
          W: /\W/,
          d: /\d/,
          D: /\D/,
          s: /\s/,
          S: /\S/,
          b: /\b/,
          A: /[A-Z]/,
          a: /[a-z]/,
          Z: /[A-ZÇÀÁÂÃÈÉÊẼÌÍÎĨÒÓÔÕÙÚÛŨ]/,
          z: /[a-zçáàãâéèêẽíìĩîóòôõúùũüû]/,
          '@': /[a-zA-Z]/,
          '#': /[a-zA-ZçáàãâéèêẽíìĩîóòôõúùũüûÇÀÁÂÃÈÉÊẼÌÍÎĨÒÓÔÕÙÚÛŨ]/,
          '%': /[0-9a-zA-ZçáàãâéèêẽíìĩîóòôõúùũüûÇÀÁÂÃÈÉÊẼÌÍÎĨÒÓÔÕÙÚÛŨ]/
        };

        // REGEX

        function generateIntermetiateElementRegex(i, forceOptional) {
          var charRegex;
          try {
            var element = maskWithoutOptionals[i];
            var elementRegex = patterns[element];
            var hasOptional = isOptional(i);

            if (elementRegex) {
              charRegex = '(' + elementRegex.source + ')';
            } else {
              // is a divisor
              if (!isDivisor(i)) {
                divisors.push(i);
                divisorElements[i] = element;
              }

              charRegex = '(' + '\\' + element + ')';
            }
          } catch (e) {
            throw e;
          }

          if (hasOptional || forceOptional) {
            charRegex += '?';
          }

          return new RegExp(charRegex);
        }

        function generateIntermetiateRegex(i, forceOptional) {
          var elementRegex;
          var elementOptionalRegex;
          try {
            var intermetiateElementRegex = generateIntermetiateElementRegex(
              i,
              forceOptional
            );
            elementRegex = intermetiateElementRegex;

            var hasOptional = isOptional(i);
            var currentRegex = intermetiateElementRegex.source;

            if (hasOptional && i + 1 < maskWithoutOptionalsLength) {
              var intermetiateRegex = generateIntermetiateRegex(
                i + 1,
                true
              ).elementOptionalRegex();
              currentRegex += intermetiateRegex.source;
            }

            elementOptionalRegex = new RegExp(currentRegex);
          } catch (e) {
            throw e;
          }
          return {
            elementRegex: function() {
              return elementRegex;
            },
            elementOptionalRegex: function() {
              // from element regex, gets the flow of regex until first not
				// optional
              return elementOptionalRegex;
            }
          };
        }

        function generateRegex(opts) {
          var deferred = $q.defer();
          maskWithoutOptionals = null;
          maskWithoutOptionalsLength = 0;
          maskWithoutOptionalsAndDivisorsLength = 0;
          optionalIndexes = [];
          optionalDivisors = {};
          optionalDivisorsCombinations = [];
          divisors = [];
          divisorElements = {};
          regex = [];
          options = opts;

          try {
            var mask = opts['mask'];
            var repeat = opts['repeat'];

            if (!mask) return;

            if (repeat) {
              mask = Array(parseInt(repeat) + 1).join(mask);
            }

            optionalIndexes = OptionalService.getOptionals(
              mask
            ).fromMaskWithoutOptionals();
            options[
              'maskWithoutOptionals'
            ] = maskWithoutOptionals = OptionalService.removeOptionals(mask);
            maskWithoutOptionalsLength = maskWithoutOptionals.length;

            var cumulativeRegex;
            for (var i = 0; i < maskWithoutOptionalsLength; i++) {
              var charRegex = generateIntermetiateRegex(i);
              var elementRegex = charRegex.elementRegex();
              var elementOptionalRegex = charRegex.elementOptionalRegex();

              var newRegex = cumulativeRegex
                ? cumulativeRegex.source + elementOptionalRegex.source
                : elementOptionalRegex.source;
              newRegex = new RegExp(newRegex);
              cumulativeRegex = cumulativeRegex
                ? cumulativeRegex.source + elementRegex.source
                : elementRegex.source;
              cumulativeRegex = new RegExp(cumulativeRegex);

              regex.push(newRegex);
            }

            generateOptionalDivisors();
            maskWithoutOptionalsAndDivisorsLength = removeDivisors(
              maskWithoutOptionals
            ).length;

            deferred.resolve({
              options: options,
              divisors: divisors,
              divisorElements: divisorElements,
              optionalIndexes: optionalIndexes,
              optionalDivisors: optionalDivisors,
              optionalDivisorsCombinations: optionalDivisorsCombinations
            });
          } catch (e) {
            deferred.reject(e);
            throw e;
          }

          return deferred.promise;
        }

        function getRegex(index) {
          var currentRegex;

          try {
            currentRegex = regex[index] ? regex[index].source : '';
          } catch (e) {
            throw e;
          }

          return new RegExp('^' + currentRegex + '$');
        }

        // DIVISOR

        function isOptional(currentPos) {
          return UtilService.inArray(currentPos, optionalIndexes);
        }

        function isDivisor(currentPos) {
          return UtilService.inArray(currentPos, divisors);
        }

        function generateOptionalDivisors() {
          function sortNumber(a, b) {
            return a - b;
          }

          var sortedDivisors = divisors.sort(sortNumber);
          var sortedOptionals = optionalIndexes.sort(sortNumber);
          for (var i = 0; i < sortedDivisors.length; i++) {
            var divisor = sortedDivisors[i];
            for (var j = 1; j <= sortedOptionals.length; j++) {
              var optional = sortedOptionals[j - 1];
              if (optional >= divisor) {
                break;
              }

              if (optionalDivisors[divisor]) {
                optionalDivisors[divisor] = optionalDivisors[divisor].concat(
                  divisor - j
                );
              } else {
                optionalDivisors[divisor] = [divisor - j];
              }

              // get the original divisor for alternative divisor
              divisorElements[divisor - j] = divisorElements[divisor];
            }
          }
        }

        function removeDivisors(value) {
          value = value.toString();
          try {
            if (divisors.length > 0 && value) {
              var keys = Object.keys(divisorElements);
              var elments = [];

              for (var i = keys.length - 1; i >= 0; i--) {
                var divisor = divisorElements[keys[i]];
                if (divisor) {
                  elments.push(divisor);
                }
              }

              elments = UtilService.uniqueArray(elments);

              // remove if it is not pattern
              var regex = new RegExp(
                '[' + '\\' + elments.join('\\') + ']',
                'g'
              );
              return value.replace(regex, '');
            } else {
              return value;
            }
          } catch (e) {
            throw e;
          }
        }

        function insertDivisors(array, combination) {
          function insert(array, output) {
            var out = output;
            for (var i = 0; i < array.length; i++) {
              var divisor = array[i];
              if (divisor < out.length) {
                out.splice(divisor, 0, divisorElements[divisor]);
              }
            }
            return out;
          }

          var output = array;
          var divs = divisors.filter(function(it) {
            var optionalDivisorsKeys = Object.keys(optionalDivisors).map(
              function(it) {
                return parseInt(it);
              }
            );

            return (
              !UtilService.inArray(it, combination) &&
              !UtilService.inArray(it, optionalDivisorsKeys)
            );
          });

          if (!angular.isArray(array) || !angular.isArray(combination)) {
            return output;
          }

          // insert not optional divisors
          output = insert(divs, output);

          // insert optional divisors
          output = insert(combination, output);

          return output;
        }

        function tryDivisorConfiguration(value) {
          var output = value.split('');
          var defaultDivisors = true;

          // has optional?
          if (optionalIndexes.length > 0) {
            var lazyArguments = [];
            var optionalDivisorsKeys = Object.keys(optionalDivisors);

            // get all optional divisors as array of arrays [[], [], []...]
            for (var i = 0; i < optionalDivisorsKeys.length; i++) {
              var val = optionalDivisors[optionalDivisorsKeys[i]];
              lazyArguments.push(val);
            }

            // generate all possible configurations
            if (optionalDivisorsCombinations.length === 0) {
              UtilService.lazyProduct(lazyArguments, function() {
                // convert arguments to array
                optionalDivisorsCombinations.push(
                  Array.prototype.slice.call(arguments)
                );
              });
            }

            for (var j = optionalDivisorsCombinations.length - 1; j >= 0; j--) {
              var outputClone = angular.copy(output);
              outputClone = insertDivisors(
                outputClone,
                optionalDivisorsCombinations[j]
              );

              // try validation
              var viewValueWithDivisors = outputClone.join('');
              var regex = getRegex(maskWithoutOptionals.length - 1);

              if (regex.test(viewValueWithDivisors)) {
                defaultDivisors = false;
                output = outputClone;
                break;
              }
            }
          }

          if (defaultDivisors) {
            output = insertDivisors(output, divisors);
          }

          return output.join('');
        }

        // MASK

        function getOptions() {
          return options;
        }

        function getViewValue(value) {
          try {
            var outputWithoutDivisors = removeDivisors(value);
            var output = tryDivisorConfiguration(outputWithoutDivisors);

            return {
              withDivisors: function(capped) {
                if (capped) {
                  return output.substr(0, maskWithoutOptionalsLength);
                } else {
                  return output;
                }
              },
              withoutDivisors: function(capped) {
                if (capped) {
                  return outputWithoutDivisors.substr(
                    0,
                    maskWithoutOptionalsAndDivisorsLength
                  );
                } else {
                  return outputWithoutDivisors;
                }
              }
            };
          } catch (e) {
            throw e;
          }
        }

        // SELECTOR

        function getWrongPositions(viewValueWithDivisors, onlyFirst) {
          var pos = [];

          if (!viewValueWithDivisors) {
            return 0;
          }

          for (var i = 0; i < viewValueWithDivisors.length; i++) {
            var pattern = getRegex(i);
            var value = viewValueWithDivisors.substr(0, i + 1);

            if (pattern && !pattern.test(value)) {
              pos.push(i);

              if (onlyFirst) {
                break;
              }
            }
          }

          return pos;
        }

        function getFirstWrongPosition(viewValueWithDivisors) {
          return getWrongPositions(viewValueWithDivisors, true)[0];
        }

        function removeWrongPositions(viewValueWithDivisors) {
          var wrongPositions = getWrongPositions(viewValueWithDivisors, false);
          var newViewValue = viewValueWithDivisors;

          for (var i = 0; i < wrongPositions.length; i++) {
            var wrongPosition = wrongPositions[i];
            var viewValueArray = viewValueWithDivisors.split('');
            viewValueArray.splice(wrongPosition, 1);
            newViewValue = viewValueArray.join('');
          }

          return getViewValue(newViewValue);
        }

        return {
          getViewValue: getViewValue,
          generateRegex: generateRegex,
          getRegex: getRegex,
          getOptions: getOptions,
          removeDivisors: removeDivisors,
          getFirstWrongPosition: getFirstWrongPosition,
          removeWrongPositions: removeWrongPositions
        };
      }

      return {
        create: create
      };
    }
  ]);
})();
(function() {
  'use strict';
  angular.module('ngMask').factory('OptionalService', [
    function() {
      function getOptionalsIndexes(mask) {
        var indexes = [];

        try {
          var regexp = /\?/g;
          var match = [];

          while ((match = regexp.exec(mask)) != null) {
            // Save the optional char
            indexes.push(match.index - 1);
          }
        } catch (e) {
          throw e;
        }

        return {
          fromMask: function() {
            return indexes;
          },
          fromMaskWithoutOptionals: function() {
            return getOptionalsRelativeMaskWithoutOptionals(indexes);
          }
        };
      }

      function getOptionalsRelativeMaskWithoutOptionals(optionals) {
        var indexes = [];
        for (var i = 0; i < optionals.length; i++) {
          indexes.push(optionals[i] - i);
        }
        return indexes;
      }

      function removeOptionals(mask) {
        var newMask;

        try {
          newMask = mask.replace(/\?/g, '');
        } catch (e) {
          throw e;
        }

        return newMask;
      }

      return {
        removeOptionals: removeOptionals,
        getOptionals: getOptionalsIndexes
      };
    }
  ]);
})();
(function() {
  'use strict';
  angular.module('ngMask').factory('UtilService', [
    function() {
      // sets: an array of arrays
      // f: your callback function
      // context: [optional] the `this` to use for your callback
      // http://phrogz.net/lazy-cartesian-product
      function lazyProduct(sets, f, context) {
        if (!context) {
          context = this;
        }

        var p = [];
        var max = sets.length - 1;
        var lens = [];

        for (var i = sets.length; i--; ) {
          lens[i] = sets[i].length;
        }

        function dive(d) {
          var a = sets[d];
          var len = lens[d];

          if (d === max) {
            for (var i = 0; i < len; ++i) {
              p[d] = a[i];
              f.apply(context, p);
            }
          } else {
            for (var k = 0; k < len; ++k) {
              p[d] = a[k];
              dive(d + 1);
            }
          }

          p.pop();
        }

        dive(0);
      }

      function inArray(i, array) {
        var output;

        try {
          output = array.indexOf(i) > -1;
        } catch (e) {
          throw e;
        }

        return output;
      }

      function uniqueArray(array) {
        var u = {};
        var a = [];

        for (var i = 0, l = array.length; i < l; ++i) {
          if (u.hasOwnProperty(array[i])) {
            continue;
          }

          a.push(array[i]);
          u[array[i]] = 1;
        }

        return a;
      }

      return {
        lazyProduct: lazyProduct,
        inArray: inArray,
        uniqueArray: uniqueArray
      };
    }
  ]);
})();

/*
 * Attaches input mask onto input element
 */
angular
  .module('ui.mask', [])
  .value('uiMaskConfig', {
    maskDefinitions: {
      '9': /\d/,
      A: /[a-zA-Z]/,
      '*': /[a-zA-Z0-9]/
    },
    clearOnBlur: true,
    clearOnBlurPlaceholder: false,
    escChar: '\\',
    eventsToHandle: ['input', 'keyup', 'click', 'focus'],
    addDefaultPlaceholder: true,
    allowInvalidValue: false
  })
  .provider('uiMask.Config', function() {
    var options = {};

    this.maskDefinitions = function(maskDefinitions) {
      return (options.maskDefinitions = maskDefinitions);
    };
    this.clearOnBlur = function(clearOnBlur) {
      return (options.clearOnBlur = clearOnBlur);
    };
    this.clearOnBlurPlaceholder = function(clearOnBlurPlaceholder) {
      return (options.clearOnBlurPlaceholder = clearOnBlurPlaceholder);
    };
    this.eventsToHandle = function(eventsToHandle) {
      return (options.eventsToHandle = eventsToHandle);
    };
    this.addDefaultPlaceholder = function(addDefaultPlaceholder) {
      return (options.addDefaultPlaceholder = addDefaultPlaceholder);
    };
    this.allowInvalidValue = function(allowInvalidValue) {
      return (options.allowInvalidValue = allowInvalidValue);
    };
    this.$get = [
      'uiMaskConfig',
      function(uiMaskConfig) {
        var tempOptions = uiMaskConfig;
        for (var prop in options) {
          if (
            angular.isObject(options[prop]) &&
            !angular.isArray(options[prop])
          ) {
            angular.extend(tempOptions[prop], options[prop]);
          } else {
            tempOptions[prop] = options[prop];
          }
        }

        return tempOptions;
      }
    ];
  })
  .directive('uiMask', [
    'uiMask.Config',
    function(maskConfig) {
      function isFocused(elem) {
        return (
          elem === document.activeElement &&
          (!document.hasFocus || document.hasFocus()) &&
          !!(elem.type || elem.href || ~elem.tabIndex)
        );
      }

      return {
        priority: 100,
        require: 'ngModel',
        restrict: 'A',
        compile: function uiMaskCompilingFunction() {
          var options = angular.copy(maskConfig);

          return function uiMaskLinkingFunction(
            scope,
            iElement,
            iAttrs,
            controller
          ) {
            var maskProcessed = false,
              eventsBound = false,
              maskCaretMap,
              maskPatterns,
              maskPlaceholder,
              maskComponents,
              // Minimum required length of the value to be considered valid
              minRequiredLength,
              value,
              isValid,
              // Vars for initializing/uninitializing
              originalPlaceholder = iAttrs.placeholder,
              originalMaxlength = iAttrs.maxlength,
              // Vars used exclusively in eventHandler()
              oldValue,
              oldValueUnmasked,
              oldCaretPosition,
              oldSelectionLength,
              // Used for communicating if a backspace operation should be
				// allowed between
              // keydownHandler and eventHandler
              preventBackspace;

            var originalIsEmpty = controller.$isEmpty;
            controller.$isEmpty = function(value) {
              if (maskProcessed) {
                return originalIsEmpty(unmaskValue(value || ''));
              } else {
                return originalIsEmpty(value);
              }
            };

            function initialize(maskAttr) {
              if (!angular.isDefined(maskAttr)) {
                return uninitialize();
              }
              processRawMask(maskAttr);
              if (!maskProcessed) {
                return uninitialize();
              }
              initializeElement();
              bindEventListeners();
              return true;
            }

            function initPlaceholder(placeholderAttr) {
              if (!placeholderAttr) {
                return;
              }

              maskPlaceholder = placeholderAttr;

              // If the mask is processed, then we need to update the value
              // but don't set the value if there is nothing entered into the
				// element
              // and there is a placeholder attribute on the element because
				// that
              // will only set the value as the blank maskPlaceholder
              // and override the placeholder on the element
              if (
                maskProcessed &&
                !(
                  iElement.val().length === 0 &&
                  angular.isDefined(iAttrs.placeholder)
                )
              ) {
                iElement.val(maskValue(unmaskValue(iElement.val())));
              }
            }

            function initPlaceholderChar() {
              return initialize(iAttrs.uiMask);
            }

            var modelViewValue = false;
            iAttrs.$observe('modelViewValue', function(val) {
              if (val === 'true') {
                modelViewValue = true;
              }
            });

            iAttrs.$observe('allowInvalidValue', function(val) {
              linkOptions.allowInvalidValue = val === '' ? true : !!val;
              formatter(controller.$modelValue);
            });

            function formatter(fromModelValue) {
              if (!maskProcessed) {
                return fromModelValue;
              }
              value = unmaskValue(fromModelValue || '');
              isValid = validateValue(value);
              controller.$setValidity('mask', isValid);

              if (!value.length) return undefined;
              if (isValid || linkOptions.allowInvalidValue) {
                return maskValue(value);
              } else {
                return undefined;
              }
            }

            function parser(fromViewValue) {
              if (!maskProcessed) {
                return fromViewValue;
              }
              value = unmaskValue(fromViewValue || '');
              isValid = validateValue(value);
              // We have to set viewValue manually as the reformatting of the
				// input
              // value performed by eventHandler() doesn't happen until after
              // this parser is called, which causes what the user sees in the
				// input
              // to be out-of-sync with what the controller's $viewValue is
				// set to.
              controller.$viewValue = value.length ? maskValue(value) : '';
              controller.$setValidity('mask', isValid);

              if (isValid || linkOptions.allowInvalidValue) {
                return modelViewValue ? controller.$viewValue : value;
              }
            }

            var linkOptions = {};

            if (iAttrs.uiOptions) {
              linkOptions = scope.$eval('[' + iAttrs.uiOptions + ']');
              if (angular.isObject(linkOptions[0])) {
                // we can't use angular.copy nor angular.extend, they lack the
				// power to do a deep merge
                linkOptions = (function(original, current) {
                  for (var i in original) {
                    if (Object.prototype.hasOwnProperty.call(original, i)) {
                      if (current[i] === undefined) {
                        current[i] = angular.copy(original[i]);
                      } else {
                        if (
                          angular.isObject(current[i]) &&
                          !angular.isArray(current[i])
                        ) {
                          current[i] = angular.extend(
                            {},
                            original[i],
                            current[i]
                          );
                        }
                      }
                    }
                  }
                  return current;
                })(options, linkOptions[0]);
              } else {
                linkOptions = options; // gotta be a better way to do this..
              }
            } else {
              linkOptions = options;
            }

            iAttrs.$observe('uiMask', initialize);
            if (angular.isDefined(iAttrs.uiMaskPlaceholder)) {
              iAttrs.$observe('uiMaskPlaceholder', initPlaceholder);
            } else {
              iAttrs.$observe('placeholder', initPlaceholder);
            }
            if (angular.isDefined(iAttrs.uiMaskPlaceholderChar)) {
              iAttrs.$observe('uiMaskPlaceholderChar', initPlaceholderChar);
            }

            controller.$formatters.unshift(formatter);
            controller.$parsers.unshift(parser);

            function uninitialize() {
              maskProcessed = false;
              unbindEventListeners();

              if (angular.isDefined(originalPlaceholder)) {
                iElement.attr('placeholder', originalPlaceholder);
              } else {
                iElement.removeAttr('placeholder');
              }

              if (angular.isDefined(originalMaxlength)) {
                iElement.attr('maxlength', originalMaxlength);
              } else {
                iElement.removeAttr('maxlength');
              }

              iElement.val(controller.$modelValue);
              controller.$viewValue = controller.$modelValue;
              return false;
            }

            function initializeElement() {
              value = oldValueUnmasked = unmaskValue(
                controller.$modelValue || ''
              );
              // var valueMasked = (oldValue = maskValue(value));
              isValid = validateValue(value);
              if (iAttrs.maxlength) {
                // Double maxlength to allow pasting new val at end of mask
                iElement.attr(
                  'maxlength',
                  maskCaretMap[maskCaretMap.length - 1] * 2
                );
              }
              if (!originalPlaceholder && linkOptions.addDefaultPlaceholder) {
                iElement.attr('placeholder', maskPlaceholder);
              }
              var viewValue = controller.$modelValue;
              var idx = controller.$formatters.length;
              while (idx--) {
                viewValue = controller.$formatters[idx](viewValue);
              }
              controller.$viewValue = viewValue || '';
              controller.$render();
              // Not using $setViewValue so we don't clobber the model value
				// and dirty the form
              // without any kind of user interaction.
            }

            function bindEventListeners() {
              if (eventsBound) {
                return;
              }
              iElement.bind('blur', blurHandler);
              iElement.bind('mousedown mouseup', mouseDownUpHandler);
              iElement.bind('keydown', keydownHandler);
              iElement.bind(linkOptions.eventsToHandle.join(' '), eventHandler);
              eventsBound = true;
            }

            function unbindEventListeners() {
              if (!eventsBound) {
                return;
              }
              iElement.unbind('blur', blurHandler);
              iElement.unbind('mousedown', mouseDownUpHandler);
              iElement.unbind('mouseup', mouseDownUpHandler);
              iElement.unbind('keydown', keydownHandler);
              iElement.unbind('input', eventHandler);
              iElement.unbind('keyup', eventHandler);
              iElement.unbind('click', eventHandler);
              iElement.unbind('focus', eventHandler);
              eventsBound = false;
            }

            function validateValue(value) {
              // Zero-length value validity is ngRequired's determination
              return value.length ? value.length >= minRequiredLength : true;
            }

            function unmaskValue(value) {
              var valueUnmasked = '',
                input = iElement[0],
                maskPatternsCopy = maskPatterns.slice(),
                selectionStart = oldCaretPosition,
                selectionEnd = selectionStart + getSelectionLength(input),
                valueOffset,
                valueDelta,
                tempValue = '';
              // Preprocess by stripping mask components from value
              value = value.toString();
              valueOffset = 0;
              valueDelta = value.length - maskPlaceholder.length;
              angular.forEach(maskComponents, function(component) {
                var position = component.position;
                // Only try and replace the component if the component position
				// is not within the selected range
                // If component was in selected range then it was removed with
				// the user input so no need to try and remove that component
                if (!(position >= selectionStart && position < selectionEnd)) {
                  if (position >= selectionStart) {
                    position += valueDelta;
                  }
                  if (
                    value.substring(
                      position,
                      position + component.value.length
                    ) === component.value
                  ) {
                    tempValue += value.slice(valueOffset, position); // +
																		// value.slice(position
																		// +
																		// component.value.length);
                    valueOffset = position + component.value.length;
                  }
                }
              });
              value = tempValue + value.slice(valueOffset);
              angular.forEach(value.split(''), function(chr) {
                if (maskPatternsCopy.length && maskPatternsCopy[0].test(chr)) {
                  valueUnmasked += chr;
                  maskPatternsCopy.shift();
                }
              });

              return valueUnmasked;
            }

            function maskValue(unmaskedValue) {
              var valueMasked = '',
                maskCaretMapCopy = maskCaretMap.slice();

              angular.forEach(maskPlaceholder.split(''), function(chr, i) {
                if (unmaskedValue.length && i === maskCaretMapCopy[0]) {
                  valueMasked += unmaskedValue.charAt(0) || '_';
                  unmaskedValue = unmaskedValue.substr(1);
                  maskCaretMapCopy.shift();
                } else {
                  valueMasked += chr;
                }
              });
              return valueMasked;
            }

            function getPlaceholderChar(i) {
              var placeholder = angular.isDefined(iAttrs.uiMaskPlaceholder)
                  ? iAttrs.uiMaskPlaceholder
                  : iAttrs.placeholder,
                defaultPlaceholderChar;

              if (angular.isDefined(placeholder) && placeholder[i]) {
                return placeholder[i];
              } else {
                defaultPlaceholderChar =
                  angular.isDefined(iAttrs.uiMaskPlaceholderChar) &&
                  iAttrs.uiMaskPlaceholderChar
                    ? iAttrs.uiMaskPlaceholderChar
                    : '_';
                return defaultPlaceholderChar.toLowerCase() === 'space'
                  ? ' '
                  : defaultPlaceholderChar[0];
              }
            }

            // Generate array of mask components that will be stripped from a
			// masked value
            // before processing to prevent mask components from being added to
			// the unmasked value.
            // E.g., a mask pattern of '+7 9999' won't have the 7 bleed into the
			// unmasked value.
            function getMaskComponents() {
              var maskPlaceholderChars = maskPlaceholder.split(''),
                maskPlaceholderCopy,
                components;

              // maskCaretMap can have bad values if the input has the ui-mask
				// attribute implemented as an obversable property, e.g. the
				// demo page
              if (maskCaretMap && !isNaN(maskCaretMap[0])) {
                // Instead of trying to manipulate the RegEx based on the
				// placeholder characters
                // we can simply replace the placeholder characters based on the
				// already built
                // maskCaretMap to underscores and leave the original working
				// RegEx to get the proper
                // mask components
                angular.forEach(maskCaretMap, function(value) {
                  maskPlaceholderChars[value] = '_';
                });
              }
              maskPlaceholderCopy = maskPlaceholderChars.join('');
              components = maskPlaceholderCopy.replace(/[_]+/g, '_').split('_');
              components = components.filter(function(s) {
                return s !== '';
              });

              // need a string search offset in cases where the mask contains
				// multiple identical components
              // E.g., a mask of 99.99.99-999.99
              var offset = 0;
              return components.map(function(c) {
                var componentPosition = maskPlaceholderCopy.indexOf(c, offset);
                offset = componentPosition + 1;
                return {
                  value: c,
                  position: componentPosition
                };
              });
            }

            function processRawMask(mask) {
              var characterCount = 0;

              maskCaretMap = [];
              maskPatterns = [];
              maskPlaceholder = '';

              if (angular.isString(mask)) {
                minRequiredLength = 0;

                var isOptional = false,
                  numberOfOptionalCharacters = 0,
                  splitMask = mask.split('');

                var inEscape = false;
                angular.forEach(splitMask, function(chr, i) {
                  if (inEscape) {
                    inEscape = false;
                    maskPlaceholder += chr;
                    characterCount++;
                  } else if (linkOptions.escChar === chr) {
                    inEscape = true;
                  } else if (linkOptions.maskDefinitions[chr]) {
                    maskCaretMap.push(characterCount);

                    maskPlaceholder += getPlaceholderChar(
                      i - numberOfOptionalCharacters
                    );
                    maskPatterns.push(linkOptions.maskDefinitions[chr]);

                    characterCount++;
                    if (!isOptional) {
                      minRequiredLength++;
                    }

                    isOptional = false;
                  } else if (chr === '?') {
                    isOptional = true;
                    numberOfOptionalCharacters++;
                  } else {
                    maskPlaceholder += chr;
                    characterCount++;
                  }
                });
              }
              // Caret position immediately following last position is valid.
              maskCaretMap.push(maskCaretMap.slice().pop() + 1);

              maskComponents = getMaskComponents();
              maskProcessed = maskCaretMap.length > 1 ? true : false;
            }

            var prevValue = iElement.val();
            function blurHandler() {
              if (
                linkOptions.clearOnBlur ||
                (linkOptions.clearOnBlurPlaceholder &&
                  value.length === 0 &&
                  iAttrs.placeholder)
              ) {
                oldCaretPosition = 0;
                oldSelectionLength = 0;
                if (!isValid || value.length === 0) {
                  // var valueMasked = '';
                  iElement.val('');
                  scope.$apply(function() {
                    // only $setViewValue when not $pristine to avoid changing
					// $pristine state.
                    if (!controller.$pristine) {
                      controller.$setViewValue('');
                    }
                  });
                }
              }
              // Check for different value and trigger change.
              // Check for different value and trigger change.
              if (value !== prevValue) {
                // #157 Fix the bug from the trigger when backspacing exactly on
				// the first letter (emptying the field)
                // and then blurring out.
                // Angular uses html element and calls
				// setViewValue(element.value.trim()), setting it to the trimmed
				// mask
                // when it should be empty
                var currentVal = iElement.val();
                var isTemporarilyEmpty =
                  value === '' &&
                  currentVal &&
                  angular.isDefined(iAttrs.uiMaskPlaceholderChar) &&
                  iAttrs.uiMaskPlaceholderChar === 'space';
                if (isTemporarilyEmpty) {
                  iElement.val('');
                }
                triggerChangeEvent(iElement[0]);
                if (isTemporarilyEmpty) {
                  iElement.val(currentVal);
                }
              }
              prevValue = value;
            }

            function triggerChangeEvent(element) {
              var change;
              if (angular.isFunction(window.Event) && !element.fireEvent) {
                // modern browsers and Edge
                try {
                  change = new Event('change', {
                    view: window,
                    bubbles: true,
                    cancelable: false
                  });
                } catch (ex) {
                  // this is for certain mobile browsers that have the Event
					// object
                  // but don't support the Event constructor #168
                  change = document.createEvent('HTMLEvents');
                  change.initEvent('change', false, true);
                } finally {
                  element.dispatchEvent(change);
                }
              } else if ('createEvent' in document) {
                // older browsers
                change = document.createEvent('HTMLEvents');
                change.initEvent('change', false, true);
                element.dispatchEvent(change);
              } else if (element.fireEvent) {
                // IE <= 11
                element.fireEvent('onchange');
              }
            }

            function mouseDownUpHandler(e) {
              if (e.type === 'mousedown') {
                iElement.bind('mouseout', mouseoutHandler);
              } else {
                iElement.unbind('mouseout', mouseoutHandler);
              }
            }

            iElement.bind('mousedown mouseup', mouseDownUpHandler);

            function mouseoutHandler() {
              /* jshint validthis: true */
              oldSelectionLength = getSelectionLength(this);
              iElement.unbind('mouseout', mouseoutHandler);
            }

            function keydownHandler(e) {
              /* jshint validthis: true */
              var isKeyBackspace = e.which === 8,
                caretPos = getCaretPosition(this) - 1 || 0, // value in keydown
															// is pre change so
															// bump caret
															// position back to
															// simulate post
															// change
                isCtrlZ = e.which === 90 && e.ctrlKey; // ctrl+z pressed

              if (isKeyBackspace) {
                while (caretPos >= 0) {
                  if (isValidCaretPosition(caretPos)) {
                    // re-adjust the caret position.
                    // Increment to account for the initial decrement to
					// simulate post change caret position
                    setCaretPosition(this, caretPos + 1);
                    break;
                  }
                  caretPos--;
                }
                preventBackspace = caretPos === -1;
              }

              if (isCtrlZ) {
                // prevent IE bug - value should be returned to initial state
                iElement.val('');
                e.preventDefault();
              }
            }

            function eventHandler(e) {
              /* jshint validthis: true */
              e = e || {};
              // Allows more efficient minification
              var eventWhich = e.which,
                eventType = e.type;

              // Prevent shift and ctrl from mucking with old values
              if (eventWhich === 16 || eventWhich === 91) {
                return;
              }

              var val = iElement.val(),
                valOld = oldValue,
                valMasked,
                valAltered = false,
                valUnmasked = unmaskValue(val),
                valUnmaskedOld = oldValueUnmasked,
                caretPos = getCaretPosition(this) || 0,
                caretPosOld = oldCaretPosition || 0,
                caretPosDelta = caretPos - caretPosOld,
                caretPosMin = maskCaretMap[0],
                caretPosMax =
                  maskCaretMap[valUnmasked.length] ||
                  maskCaretMap.slice().shift(),
                selectionLenOld = oldSelectionLength || 0,
                isSelected = getSelectionLength(this) > 0,
                wasSelected = selectionLenOld > 0,
                // Case: Typing a character to overwrite a selection
                isAddition =
                  val.length > valOld.length ||
                  (selectionLenOld &&
                    val.length > valOld.length - selectionLenOld),
                // Case: Delete and backspace behave identically on a selection
                isDeletion =
                  val.length < valOld.length ||
                  (selectionLenOld &&
                    val.length === valOld.length - selectionLenOld),
                isSelection =
                  eventWhich >= 37 && eventWhich <= 40 && e.shiftKey, // Arrow
																		// key
																		// codes
                isKeyLeftArrow = eventWhich === 37,
                // Necessary due to "input" event not providing a key code
                isKeyBackspace =
                  eventWhich === 8 ||
                  (eventType !== 'keyup' && isDeletion && caretPosDelta === -1),
                isKeyDelete =
                  eventWhich === 46 ||
                  (eventType !== 'keyup' &&
                    isDeletion &&
                    caretPosDelta === 0 &&
                    !wasSelected),
                // Handles cases where caret is moved and placed in front of
				// invalid maskCaretMap position. Logic below
                // ensures that, on click or leftward caret placement, caret is
				// moved leftward until directly right of
                // non-mask character. Also applied to click since users are
				// (arguably) more likely to backspace
                // a character when clicking within a filled input.
                caretBumpBack =
                  (isKeyLeftArrow || isKeyBackspace || eventType === 'click') &&
                  caretPos > caretPosMin;

              oldSelectionLength = getSelectionLength(this);

              // These events don't require any action
              if (
                isSelection ||
                (isSelected &&
                  (eventType === 'click' ||
                    eventType === 'keyup' ||
                    eventType === 'focus'))
              ) {
                return;
              }

              if (isKeyBackspace && preventBackspace) {
                iElement.val(maskPlaceholder);
                // This shouldn't be needed but for some reason after aggressive
				// backspacing the controller $viewValue is incorrect.
                // This keeps the $viewValue updated and correct.
                scope.$apply(function() {
                  controller.$setViewValue(''); // $setViewValue should be run
												// in angular context, otherwise
												// the changes will be invisible
												// to angular and user code.
                });
                setCaretPosition(this, caretPosOld);
                return;
              }

              // Value Handling
              // ==============

              // User attempted to delete but raw value was
				// unaffected--correct this grievous offense
              if (
                eventType === 'input' &&
                isDeletion &&
                !wasSelected &&
                valUnmasked === valUnmaskedOld
              ) {
                while (
                  isKeyBackspace &&
                  caretPos > caretPosMin &&
                  !isValidCaretPosition(caretPos)
                ) {
                  caretPos--;
                }
                while (
                  isKeyDelete &&
                  caretPos < caretPosMax &&
                  maskCaretMap.indexOf(caretPos) === -1
                ) {
                  caretPos++;
                }
                var charIndex = maskCaretMap.indexOf(caretPos);
                // Strip out non-mask character that user would have deleted if
				// mask hadn't been in the way.
                valUnmasked =
                  valUnmasked.substring(0, charIndex) +
                  valUnmasked.substring(charIndex + 1);

                // If value has not changed, don't want to call $setViewValue,
				// may be caused by IE raising input event due to placeholder
                if (valUnmasked !== valUnmaskedOld) valAltered = true;
              }

              // Update values
              valMasked = maskValue(valUnmasked);

              oldValue = valMasked;
              oldValueUnmasked = valUnmasked;

              // additional check to fix the problem where the viewValue is
				// out of sync with the value of the element.
              // better fix for commit
				// 2a83b5fb8312e71d220a497545f999fc82503bd9 (I think)
              if (!valAltered && val.length > valMasked.length)
                valAltered = true;

              iElement.val(valMasked);

              // we need this check. What could happen if you don't have it is
				// that you'll set the model value without the user
              // actually doing anything. Meaning, things like pristine and
				// touched will be set.
              if (valAltered) {
                scope.$apply(function() {
                  controller.$setViewValue(valMasked); // $setViewValue should
														// be run in angular
														// context, otherwise
														// the changes will be
														// invisible to angular
														// and user code.
                });
              }

              // Caret Repositioning
              // ===================

              // Ensure that typing always places caret ahead of typed
				// character in cases where the first char of
              // the input is a mask char and the caret is placed at the 0
				// position.
              if (isAddition && caretPos <= caretPosMin) {
                caretPos = caretPosMin + 1;
              }

              if (caretBumpBack) {
                caretPos--;
              }

              // Make sure caret is within min and max position limits
              caretPos =
                caretPos > caretPosMax
                  ? caretPosMax
                  : caretPos < caretPosMin
                    ? caretPosMin
                    : caretPos;

              // Scoot the caret back or forth until it's in a non-mask
				// position and within min/max position limits
              while (
                !isValidCaretPosition(caretPos) &&
                caretPos > caretPosMin &&
                caretPos < caretPosMax
              ) {
                caretPos += caretBumpBack ? -1 : 1;
              }

              if (
                (caretBumpBack && caretPos < caretPosMax) ||
                (isAddition && !isValidCaretPosition(caretPosOld))
              ) {
                caretPos++;
              }
              oldCaretPosition = caretPos;
              setCaretPosition(this, caretPos);
            }

            function isValidCaretPosition(pos) {
              return maskCaretMap.indexOf(pos) > -1;
            }

            function getCaretPosition(input) {
              if (!input) return 0;
              if (input.selectionStart !== undefined) {
                return input.selectionStart;
              } else if (document.selection) {
                if (isFocused(iElement[0])) {
                  // Curse you IE
                  input.focus();
                  var selection = document.selection.createRange();
                  selection.moveStart(
                    'character',
                    input.value ? -input.value.length : 0
                  );
                  return selection.text.length;
                }
              }
              return 0;
            }

            function setCaretPosition(input, pos) {
              if (!input) return 0;
              if (input.offsetWidth === 0 || input.offsetHeight === 0) {
                return; // Input's hidden
              }
              if (input.setSelectionRange) {
                if (isFocused(iElement[0])) {
                  input.focus();
                  input.setSelectionRange(pos, pos);
                }
              } else if (input.createTextRange) {
                // Curse you IE
                var range = input.createTextRange();
                range.collapse(true);
                range.moveEnd('character', pos);
                range.moveStart('character', pos);
                range.select();
              }
            }

            function getSelectionLength(input) {
              if (!input) return 0;
              if (input.selectionStart !== undefined) {
                return input.selectionEnd - input.selectionStart;
              }
              if (window.getSelection) {
                return window.getSelection().toString().length;
              }
              if (document.selection) {
                return document.selection.createRange().text.length;
              }
              return 0;
            }

            // https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Array/indexOf
            if (!Array.prototype.indexOf) {
              Array.prototype.indexOf = function(
                searchElement /* , fromIndex */
              ) {
                if (this === null) {
                  throw new TypeError();
                }
                var t = Object(this);
                var len = t.length >>> 0;
                if (len === 0) {
                  return -1;
                }
                var n = 0;
                if (arguments.length > 1) {
                  n = Number(arguments[1]);
                  if (n !== n) {
                    // shortcut for verifying if it's NaN
                    n = 0;
                  } else if (n !== 0 && n !== Infinity && n !== -Infinity) {
                    n = (n > 0 || -1) * Math.floor(Math.abs(n));
                  }
                }
                if (n >= len) {
                  return -1;
                }
                var k = n >= 0 ? n : Math.max(len - Math.abs(n), 0);
                for (; k < len; k++) {
                  if (k in t && t[k] === searchElement) {
                    return k;
                  }
                }
                return -1;
              };
            }
          };
        }
      };
    }
  ]);

(function () {
  'use strict';

  angular
    // run config
    .module('quoteUi')
    .run(runBlock);

  /** @ngInject */
  function runBlock($transitions, $window, $rootScope, util) {
    $transitions.onStart({
        to: '**'
      },
      function (trans) {
        var accountPages = ['app.businessdetails', 'app.lob', 'app.qualification', 'app.quote'];
        if (trans.from().name == "" && accountPages.indexOf(trans.to().name) != -1) {
          return util.handlePageAccess();
        }
        // if(trans.from().name == "") // refresh/first load
        // var sessionIndex = $window.sessionStorage.getItem('pageIndex');
        // var currentIndex;
        // if (sessionIndex == null) {
        // currentIndex = 0;
        // } else {
        // currentIndex = sessionIndex >= 0 ? sessionIndex : 0;
        // }
        // var actualIndex = util.getPageIndex(trans.to().name);
        // if (trans.to().name != 'app.logout' && trans.to().name !=
		// 'accountview' && trans.to().name != 'details' && trans.to().name !=
		// 'refer_redirect') {
        // if (trans.to().name == 'app.beforeyoubegin' && actualIndex <
		// currentIndex && trans.from().name != 'app.businessdetails' &&
		// trans.from().name != 'accountview' && trans.from().name != 'details')
		// {
        // actualIndex = parseInt(currentIndex) + 1;
        // }
        // return util.handlePageAccess(currentIndex, actualIndex);
        // }
      }
    );

    // $transitions.onSuccess({
    // to: '**'
    // },
    // function (trans) {
    // if (trans.to().name != 'app.refer' && trans.to().name != 'app.logout' &&
	// trans.to().name != 'accountview' && trans.to().name != 'details' &&
	// trans.to().name != 'refer_redirect') {
    // util.setPageIndex(trans.to().name);
    // }
    // });
  }
})();

(function () {
  'use strict';
  angular
    // route config
    .module ('quoteUi')
    .config (routerConfig);

  /** @ngInject */
  function routerConfig ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise ('/accountview');

    $stateProvider
      // common
      .state ('app', {
        abstract: true,
        url: '',
        views: {
          '@': {
            templateUrl: 'app/partials/structure.html',
            controller: 'MainController',
            controllerAs: 'main',
          },
          'top@app': {
            templateUrl: 'app/partials/tpl.header.html',
            controller: 'MainController',
            controllerAs: 'vm',
          },
          'left@app': {
            templateUrl: 'app/partials/tpl.navigation.html',
          },
          'main@app': {
            templateUrl: 'app/partials/tpl.main.html',
          },
        },
      })
      // account
      .state ('accountview', {
        url: '/accountview',
        views: {
          '@': {
            templateUrl: 'app/views/account/account.html',
            controller: 'AccountController',
            controllerAs: 'vm',
          },
        },
      })
      // details
      .state ('details', {
        url: '/details:accountId/:adderss',
        views: {
          '@': {
            templateUrl: 'app/views/details/details.html',
            controller: 'DetailsController',
            controllerAs: 'vm',
          },
        },
      })
      // begin
      .state ('app.beforeyoubegin', {
        url: '/beforeyoubegin',
        views: {
          'main@app': {
            templateUrl: 'app/views/beforeyoubegin/beforeyoubegin.html',
            controller: 'BeforeYouBeginController',
            controllerAs: 'vm',
          },
          'left@app': {
            templateUrl: 'app/views/beforeyoubegin/nav.beforeyoubegin.html',
          },
        },
      })
      // business details
      .state ('app.businessdetails', {
        url: '/businessdetails',
        views: {
          'main@app': {
            templateUrl: 'app/views/businessdetails/businessdetails.html',
            controller: 'BusinessDetailsController',
            controllerAs: 'vm',
          },
          'left@app': {
            templateUrl: 'app/views/businessdetails/nav.businessdetails.html',
            // controller: 'BusinessDetailsController',
            // controllerAs: 'vm'
          },
        },
      })
      // quote
      .state ('app.quote', {
        url: '/quote',
        views: {
          'main@app': {
            templateUrl: 'app/views/quote/quote.html',
            controller: 'QuoteController',
            controllerAs: 'vm',
          },
          'left@app': {
            templateUrl: 'app/views/quote/nav.quote.html',
          },
        },
      })
      // before you issue
      .state ('app.beforeyouissue', {
        url: '/beforeyouissue',
        views: {
          'main@app': {
            templateUrl: 'app/views/beforeyouissue/beforeyouissue.html',
            controller: 'BeforeYouIssueController',
            controllerAs: 'vm',
          },
          'left@app': {
            templateUrl: 'app/views/beforeyouissue/nav.beforeyouissue.html',
            // controller: 'BeforeYouIssueController',
            // controllerAs: 'vm'
          },
        },
      })
      // logout
      .state ('app.logout', {
        url: '/logout',
        views: {
          'main@app': {
            templateUrl: 'app/partials/logout.html',
            controller: 'MainController',
            controllerAs: 'vm',
          },
        },
      })
      // dashboard logout
      .state ('loggedout', {
        url: '/loggedout',
        views: {
          '@': {
            templateUrl: 'app/partials/loggedout1.html',
            controller: 'AccountController',
            controllerAs: 'vm',
          },
        },
      })
      // lob
      .state ('app.lob', {
        url: '/lob',
        views: {
          'main@app': {
            templateUrl: 'app/views/lob/lob.html',
            controller: 'LobController',
            controllerAs: 'vm',
          },
          'left@app': {
            templateUrl: 'app/views/lob/nav.lob.html',
          },
        },
      })
      .state ('app.qualification', {
        url: '/qualification',
        views: {
          'main@app': {
            templateUrl: 'app/views/qualification/qualification.html',
            controller: 'QualificationController',
            controllerAs: 'vm',
          },
          'left@app': {
            templateUrl: 'app/views/qualification/nav.qualification.html',
          },
        },
      })
      // location
      .state ('app.location', {
        url: '/location',
        views: {
          'main@app': {
            templateUrl: 'app/views/location/location.html',
            controller: 'LocationController',
            controllerAs: 'vm',
          },
          'left@app': {
            templateUrl: 'app/views/location/nav.location.html',
          },
        },
      })
      .state ('app.building', {
        url: '/building',
        views: {
          'main@app': {
            templateUrl: 'app/views/building/building.html',
            controller: 'BuildingController',
            controllerAs: 'vm',
          },
          'left@app': {
            templateUrl: 'app/views/building/nav.building.html',
          },
        },
      })
      // underwriting
      .state ('app.underwriting', {
        url: '/underwriting',
        views: {
          'main@app': {
            templateUrl: 'app/views/underwriting/underwriting.html',
            controller: 'UnderwritingController',
            controllerAs: 'vm',
          },
          'left@app': {
            templateUrl: 'app/views/underwriting/nav.underwriting.html',
          },
        },
      })
      // additional ineterests
      .state ('app.additionalinterest', {
        url: '/additionalinterest',
        views: {
          'main@app': {
            templateUrl: 'app/views/interest/additionalinterest.html',
            controller: 'additionalInterestController',
            controllerAs: 'vm',
          },
          'left@app': {
            templateUrl: 'app/views/interest/nav.additionalinterest.html',
          },
        },
      })
      // pricing and coverage
      .state ('app.pricingandcoverage', {
        url: '/pricingandcoverage',
        views: {
          'main@app': {
            templateUrl: 'app/views/pricingandcoverage/pricingandcoverage.html',
            controller: 'PricingAndCoverageController',
            controllerAs: 'vm',
          },
          'left@app': {
            templateUrl: 'app/views/pricingandcoverage/nav.pricingandcoverage.html',
          },
        },
      })
      // premium summary

      // before issue policy

      // refer
      .state ('app.refer', {
        url: '/refer',
        views: {
          'main@app': {
            templateUrl: 'app/views/refer/refer.html',
            controller: 'ReferController',
            controllerAs: 'vm',
          },
          'left@app': {
            templateUrl: 'app/views/refer/nav.refer.html',
          },
        },
      })
      // redirect page after refer to an underwriter - DS-1667
      .state ('refer_redirect', {
        url: '/refer_redirect',
        views: {
          '@': {
            templateUrl: 'app/views/refer/refer_redirect.html',
            controller: 'ReferRedirectController',
            controllerAs: 'vm',
          },
        },
      })
      // summary
      .state ('app.premiumsummary', {
        url: '/premiumsummary',
        views: {
          'main@app': {
            templateUrl: 'app/views/premiumsummary/premiumsummary.html',
            controller: 'PremiumSummaryController',
            controllerAs: 'vm',
          },
          'left@app': {
            templateUrl: 'app/views/premiumsummary/nav.premiumsummary.html',
          },
        },
      });
  }
}) ();

(function() {
  'use strict';

  angular
    // constants
    .module('quoteUi')
    .constant(
      'Conf',
      (function() {
        var globals = {};

        var myUrlPattern = '.local';

        globals.cache = true;

        globals.commonURI = 'DSCIQuote/';
        if (
          window.location.hostname === 'localhost' ||
          location.hostname === '127.0.0.1' ||
          window.location.hostname.indexOf(myUrlPattern) >= 0
        ) {
          globals.host = 'http://localhost:8080/';
          globals.staticRoot = '';
        } else {
          globals.staticRoot = '/jsp/';
          globals.host =
            'http://openappkubernetes.eastus.cloudapp.azure.com:30022/';
        }
        globals.serverAction = function(action) {
          return 'here' + action;
        };
        return globals;
      })()
    );
})();

(function () {
  'use strict';
  angular
    // config
    .module('quoteUi')
    .config(config)
    .config(function (toastrConfig, $httpProvider) {
      $httpProvider.defaults.useXDomain = true;
      angular.extend(toastrConfig, {
        autoDismiss: false,
        containerId: 'toast-container',
        maxOpened: 0,
        newestOnTop: true,
        positionClass: 'toast-top-right',
        preventDuplicates: false,
        preventOpenDuplicates: false,
        target: 'body'
      });
    });

  /** @ngInject */
  function config($httpProvider) {
    // Loading Spinner service
    $httpProvider.interceptors.push('LoadingInterceptor');
  }
})();

angular.module('quoteUi').run(['$templateCache', function($templateCache) {$templateCache.put('app/partials/app-header.html','<header class=header><div class=navbar-header style="width: 40%"><button class="navbar-toggle collapsed" type=button data-toggle=collapse data-target=.navbar-collapse><i class=icon-reorder></i></button> <a class="navbar-brand dashboard-brand" href=javascript:; data-ui-sref=accountview title="{{ main.resolution }}" style="width: 100%;"><img class="OmniaheaderLogo omnia-logo" ng-src="{{vm.assetPath+\'Omnia-Header-Logo.svg\'}}" alt=Omnia> <img class=utica-logo ng-src="{{vm.assetPath+\'logo.png\'}}" alt="Utica National Insurance Group"></a></div><nav class="nav clearfix OmniaheaderContainer" style="width: 60%;float:right;"><!--<a href="javascript:;" data-ui- sref="accountview">Dashboard</a>--> <a href={{vm.handofurl}}>Go to Commercial Edge </a><a href="https://secure.uticanational.com/@ysLogin.asp?from=v3&redirpath=%2fays" target=_blank>Go to @your.service</a> <a ng-click=vm.exitApp();>Logout</a></nav></header>');
$templateCache.put('app/partials/form-widget.html','<div><!-- Textbox --><div class=col-md-{{item.screenColumns}} ng-if="item.controlName == \'Text_Box\' && item.appearUI == \'Y\'"><div class=form-group ng-class="{\'has-error\': referForm.$submitted && referForm.{{item.systemRefId | dotWithUnderscore}}.$invalid}"><label for="{{item.systemRefId | dotWithUnderscore}}">{{item.datasetName}}</label> <input type={{item.inputType}} name="{{item.systemRefId | dotWithUnderscore}}" class=form-control ng-model=item.userValue placeholder={{item.additionalInfo}} ng-required=item.isRequired id="{{item.systemRefId | dotWithUnderscore}}" ng-blur="item.systemRefId == \'UNIG_PolicyUndQuestionsInput.ContactPhoneNumber\' ? vm.validatePhone(item.userValue) : \'\';item.systemRefId == \'UNIG_PolicyUndQuestionsInput.ContactEmail\' ? vm.validateEmail(item.userValue) : \'\';" ng-attr-format-phone-number="{{ item.systemRefId == \'UNIG_PolicyUndQuestionsInput.ContactPhoneNumber\' ? true : false }}" uservalue={{item.userValue}} ng-maxlength="{\n          12:item.systemRefId == \'UNIG_PolicyUndQuestionsInput.ContactPhoneNumber\'\n        }" input-length-limit><!-- Required Validation --><p ng-show="(referForm.$submitted && referForm.{{item.systemRefId | dotWithUnderscore}}.$invalid)" class="help-block error build22">{{item.datasetName | camelCase}} is required</p><p class="help-block error build22" ng-if="vm.invalidPhone && item.systemRefId == \'UNIG_PolicyUndQuestionsInput.ContactPhoneNumber\'">This field should be in the format of xxx-xxx-xxxx</p><p ng-if="vm.emailErr && item.systemRefId == \'UNIG_PolicyUndQuestionsInput.ContactEmail\'" class="help-block error build22">This field should be an email address in the format "user@example.com"</p></div></div><!-- end Textbox --><!-- Text Area --><div class=col-md-{{item.screenColumns}} ng-if="item.controlName == \'Text_Area\' && item.appearUI == \'Y\'"><div class=form-group ng-class="{\'has-error\': referForm.$submitted && referForm.{{item.systemRefId | dotWithUnderscore}}.$invalid}"><label for="{{item.systemRefId | dotWithUnderscore}}">{{item.datasetName}}</label> <textarea name="{{item.systemRefId | dotWithUnderscore}}" class=form-control ng-model=item.userValue placeholder={{item.additionalInfo}} ng-required=item.isRequired id="{{item.systemRefId | dotWithUnderscore}}" rows=5></textarea><!-- Required Validation --><p ng-show="(referForm.$submitted && referForm.{{item.systemRefId | dotWithUnderscore}}.$invalid)" class="help-block error build22">{{item.datasetName | camelCase}} is required</p></div></div><!-- end Text Area --><!-- Radio --><div class="col-md-{{item.screenColumns}} checkradio-wrap" ng-if="item.controlName == \'Radio_Button\' && item.appearUI == \'Y\'"><div class=row><div class=col-md-12><p><strong>{{item.datasetName}}</strong></p></div></div><div class=row><div class="col-md-3 form-group" ng-repeat="value in item.valueName" ng-class="{ \'has-error\': referForm.$submitted && referForm.{{item.systemRefId | dotWithUnderscore}}.$invalid}"><input type=radio name="{{item.systemRefId | dotWithUnderscore}}" value={{value}} id="{{item.systemRefId | dotWithUnderscore}}-{{$index}}" ng-model=item.userValue ng-required=item.isRequired> <label for="{{item.systemRefId | dotWithUnderscore}}-{{$index}}">{{value}}</label></div></div><div class=col-md-12><!-- Required Validation --><p ng-show="referForm.$submitted && referForm.{{item.systemRefId | dotWithUnderscore}}.$invalid" class="help-block error build22">This field is required</p></div></div><!-- End Radio --><!-- Dropdown --><div class=col-md-{{item.screenColumns}} ng-if="item.controlName == \'Drop_Down\' && item.appearUI == \'Y\'"><div class=form-group ng-class="{\'has-error\': (referForm.$submitted && referForm.{{item.systemRefId | dotWithUnderscore}}.$invalid) }"><label>{{item.datasetName}}</label> <select name="{{item.systemRefId | dotWithUnderscore}}" ng-model=item.userValue class="custom_select form-control" ng-required=item.isRequired><option value="">Please Select</option><option ng-repeat="option in item.valueName" value={{option}}>{{option}}</option></select><!-- Required Validation --><p ng-show="referForm.$submitted && referForm.{{item.systemRefId | dotWithUnderscore}}.$invalid" class="help-block error build22">{{item.datasetName | camelCase}} is required</p></div></div><!-- End Dropdown --><!-- Checkbox --><div class=col-md-{{item.screenColumns}} ng-if="item.controlName == \'Check_Box\' && item.appearUI == \'Y\'"><div class=checkbox-wrap><input type=checkbox class=checkbox id="{{item.systemRefId | dotWithUnderscore}}" name="{{item.systemRefId | dotWithUnderscore}}" ng-model=item.userValue> <label for="{{item.systemRefId | dotWithUnderscore}}" class=checkbox-label>{{item.datasetName}}</label></div></div><!-- End Checkbox --><div class=clearfix ng-if=item.hasRowBreak></div></div>');
$templateCache.put('app/partials/loggedout.html','<section class=main><div ui-view=""></div></section><!-- <div id="wrapper">\n  <div id="sidebar-wrapper" class="col-md-2">\n    <div id="sidebar" ui-view="left"></div>\n  </div>\n  <div id="main-wrapper" class="col-md-10 pull-right-wrapper">\n    <div ui-view="main" id="main"></div>\n  </div>\n</div> -->');
$templateCache.put('app/partials/logout.html','<section class=main><div ui-view=""></div></section><!-- <div id="wrapper">\n  <div id="sidebar-wrapper" class="col-md-2">\n    <div id="sidebar" ui-view="left"></div>\n  </div>\n  <div id="main-wrapper" class="col-md-10 pull-right-wrapper">\n    <div ui-view="main" id="main"></div>\n  </div>\n</div> -->');
$templateCache.put('app/partials/structure.html','<section class=top><div ui-view=top></div></section><div id=wrapper><div id=sidebar-wrapper class=col-md-2><div id=sidebar ui-view=left></div></div><div id=main-wrapper class="col-md-10 pull-right-wrapper"><div ui-view=main id=main style="clear: both;display: block;position: relative;"></div><!-- <div style="position: relative;clear: both;display: block;text-align:center;padding-bottom: 10px; margin-top: 10px;color: #909090;">\n      Copyright \xA9 2018 Utica Mutual Insurance Company, New Hartford, NY 13413 | All Rights Reserved \n    </div> --></div></div>');
$templateCache.put('app/partials/tpl.datepickerpopup.html','<ul role=presentation class="uib-datepicker-popup dropdown-menu uib-position-measure" dropdown-nested ng-if=isOpen ng-keydown=keydown($event) ng-click=$event.stopPropagation()><li ng-transclude></li><li ng-if=showButtonBar class=uib-button-bar><span class="btn-group pull-left"><button type=button class="btn btn-sm btn-info gino uib-datepicker-current" ng-click="select(\'today\', $event)" ng-disabled="isDisabled(\'today\')">{{ getText(\'current\') }}</button> <button type=button class="btn btn-sm btn-danger uib-clear" ng-click="select(null, $event)">{{ getText(\'clear\') }}</button> </span><button type=button class="btn btn-sm btn-success pull-right uib-close" ng-click=close($event)>{{ getText(\'close\') }}</button></li></ul>');
$templateCache.put('app/partials/tpl.footer.html','');
$templateCache.put('app/partials/tpl.header.html','<div id=header class="navbar navbar-default navbar-fixed-top"><div class=navbar-header style="width: 40%"><button class="navbar-toggle collapsed" type=button data-toggle=collapse data-target=.navbar-collapse><i class=icon-reorder></i></button> <a class=navbar-brand href=# title="{{ main.resolution }}" style="width: 100%;"><img class="OmniaheaderLogo omnia-logo" ng-src="{{vm.assetPath+\'Omnia-Header-Logo.svg\'}}" alt=Omnia> <img class=utica-logo ng-src="{{vm.assetPath+\'logo.png\'}}" alt="Utica National Insurance Group"></a></div><nav class="collapse navbar-collapse" style="width: 60%;float:right;"><ul class="nav navbar-nav pull-right OmniaheaderText"><li><p style="font-size:13px;font-family: Open sans-serif;">To speak with an Underwriter<br>Call 866-557-1914 Press 4</p></li><li><noscript><div style=display:inline><a href="http://www.providesupport.com?messenger=1can3j12idg1u11q88et4ph2ni">Live Chat</a></div></noscript><button type=button class="btn btn-primary btn-lg-c" onclick="psY4uUow(); return false;">Click to chat with an Underwriter</button><!-- <p style="font-size: 12px;padding-left: 25%;font-family: Open sans-serif;">Click to Chat</p> --></li><li><button type=button class="btn btn-default btn-big exit-btn" ng-click="vm.triggerExit(\'exit\');" ng-show=vm.enableExit>EXIT</button> <button type=button class="btn btn-default btn-big save-ext-btn" ng-click="vm.triggerExit(\'save\');" ng-show=!vm.enableExit>SAVE &amp; EXIT</button></li></ul></nav></div><script type=text/ng-template id=warning-dialog.html><div idle-countdown="countdown" class="modal-body text-center">\n    Your session will end in {{countdown}} seconds\n  </div></script>');
$templateCache.put('app/partials/tpl.main.html','<div>MAIN</div>');
$templateCache.put('app/partials/tpl.navigation.html','<ul class="nav list-group" ng-if=main.loggedIn><li><a class=list-group-item href=#><i class="icon-home icon-1x"></i>Sidebar Item 1</a></li><li><a class=list-group-item href=#><i class="icon-home icon-1x"></i>Sidebar Item 2</a></li><li><a class=list-group-item href=#><i class="icon-home icon-1x"></i>Sidebar Item 9</a></li><li><a class=list-group-item href=#><i class="icon-home icon-1x"></i>Sidebar Item 10</a></li><li><a class=list-group-item href=#><i class="icon-home icon-1x"></i>Sidebar Item 11</a></li></ul>');
$templateCache.put('app/views/account/account.html','<div class=account-page><div data-ng-include="\'app/partials/app-header.html\'"></div><div data-ng-include="\'app/partials/loggedout.html\'"></div><div class=dashboard-wrapper style=overflow:auto><div class=contents><div class=blue-banner><!-- <div class="left-light-blue "></div>\n        <div class="left-top-blue"></div>\n        <div class="right-light-blue"></div> --><div class=mains><h1 class=logo-center><img ng-src="{{vm.assetPath+\'omnia-logo.svg\'}}"></h1><h2 class=sub-title>Welcome to Omnia, a breakthrough in how small commercial business is done.</h2><p><a data-ng-href=#!/beforeyoubegin class="btn btn-white">Create new account</a></p></div></div><!-- end .blue-banner --><div class=container><div class=main-dashboard><div class="top-bar row"><h4 class=title>Recent Accounts</h4><div class=right-area><div class="control-group clearfix search-box"><div class="item width36"><select ng-model=vm.searchType style="padding-left: 9px;opacity: 1 !important; height:39px" class="custom_select form-control"><option selected value="Account Name">Account Name</option><option value="Account ID">Account ID</option><option value=Address>Address</option></select><!-- end .dropdown-default --></div><!-- end .item --><div class="item width64"><div class=search><span class=inputs><a href=javascript:; class=icon-search></a> <span ng-if="vm.searchInput != \'\'" ng-click=vm.clearsrch() style="position:absolute; right:12px;top:10px; color:#ccc">x</span> <input style=padding-right:20px ng-keyup="$event.keyCode == 13 ? vm.searchTable() : null" type=search data-ng-model=vm.searchInput placeholder=Search></span></div><!-- end .search --></div><!-- end .item --></div><!-- end .control-group --> <a href=javascript:; class="btn btn-success" data-ng-click=vm.searchTable()>Search</a></div></div><!-- end .top-bar --><div class=search-result><div class="row module-data" data-ng-if="vm.tableData.length > 0"><div class=table-data data-ng-class="{\'not-find\': noSearchResult}"><div class="row-tr row-th"><div class="col-th width27" data-ng-click="clickSortArrow(\'accountName\')" data-ng-class="{\'desc\': sortedAccountNameDesc}"><div class=spacing>Account Name<!--<span class="caret"></span>--></div></div><div class="col-th width22" data-ng-click="clickSortArrow(\'accountID\')" data-ng-class="{\'desc\': sortedAccountIDDesc}"><div class=spacing>Account ID<!--<span class="caret"></span>--></div></div><div class=col-th data-ng-click="clickSortArrow(\'address\')" data-ng-class="{\'desc\': sortedAddressDesc}"><div class=spacing>Address<!--<span class="caret"></span>--></div></div></div><!-- end .row-tr --><div class=table-body infinate-scroll=loadMore style=height:395px;overflow-y:scroll><div class=no-txt>Sorry, we could not find any results that match that search. Try creating an account</div><div class="row-tr row-td" data-ng-repeat="item in vm.bindData | filter:f.ClientName"><div class="col-td width27"><a data-ng-href="#!/details:{{item.ClientId}}/:{{item.Address1}}, {{item.City}}, {{item.State}}, {{item.Zip}}" class=spacing>{{item.ClientName}}</a></div><div class="col-td width22"><a data-ng-href="#!/details:{{item.ClientId}}/:{{item.Address1}}, {{item.City}}, {{item.State}}, {{item.Zip}}" class="spacing sec-col">{{item.ClientId}}</a></div><div class=col-td><a data-ng-href="#!/details:{{item.ClientId}}/:{{item.Address1}}, {{item.City}}, {{item.State}}, {{item.Zip}}" class="spacing sec-col"><span ng-show=item.Address1>{{item.Address1}}, </span><span ng-show=item.Address2>{{item.Address2}}, </span><span ng-show=item.City>{{item.City}}, </span><span ng-show=item.State>{{item.State}}, </span><span ng-show=item.Zip>{{item.Zip}}</span></a></div></div><div ng-show=vm.noresult><p style="text-align:center; padding:10px 10px">No results found</p></div><!-- end .table-data --></div><!-- end .table-data --><div class="row module-nodata" data-ng-if="pageContent.tableData.length == 0"><p class=bold-txt>You have no accounts SCI yet</p><div class=create-box><a data-ng-href=#!/accountDetails class="btn btn-light-blue">Create New Account</a></div></div><!-- end .row --></div></div><!-- end .main-dashboard --></div></div><!-- end .contents --><footer class="bottom-gray-txt container" style=padding-bottom:70px><!-- <p>\n         Please provide feedback at feedback@dsci.com, to help make this even better!\n       </p>--><p>Copyright \xA9 2018 Utica Mutual Insurance Company, New Hartford, NY 13413 | All Rights Reserved</p></footer></div><!-- end .dashboard-wrapper --></div><script type=text/ng-template id=warning-dialog.html><div idle-countdown="countdown" class="modal-body text-center">\n    Your session will end in {{countdown}} seconds\n  </div></script></div></div>');
$templateCache.put('app/views/account/nav.account.html','<nav>cio</nav>');
$templateCache.put('app/views/beforeyoubegin/beforeyoubegin.html','<div class="beforebegin-page clearfix"><div class=row><div class="col col-md-12"><h2>Before You Begin</h2></div></div><form data-toggle=validator class=formClass form-submit-validation="" name=bybForm ng-submit=vm.triggerSubmit(bybForm.$valid) novalidate autocomplete=off><div class=row></div><!-- Agent Information --><div class=row><div class=white-pannel><h4 class=pannel-title>{{vm.agentInfoHdr.datasetName}}</h4><div class=row><!-- Radio Control --><div class="col-md-12 checkradio-wrap" ng-repeat="question in vm.bybMeta.agent_Info" ng-if="question.systemRefId == \'Do_you_intend_to_quote_a_Busine\'"><div class=row><div class=col-md-6><p><strong>{{question.datasetName}}</strong></p></div><div class="col-md-1 form-group" ng-repeat="value in question.valueName" ng-class="{ \'has-error\': bybForm.submitted && bybForm.doyouintend.$invalid && bybForm.doyouintend.$touched }"><input style=position:absolute type=radio name=doyouintend data-uuid={{question.uuid}} value={{value}} id="{{$index + 2}}" ng-model=question.userValue ng-required=true> <label for="{{$index + 2}}">{{value}}</label></div><!-- Required Validation --><p ng-show="bybForm.doyouintend.$invalid && bybForm.doyouintend.$touched && bybForm.submitted" class="col-md-12 help-block error build22" style=padding-left:52%;margin-top:-12px>This field is required</p></div></div><!--  End Radio Control --><!-- Other Form Controls --><div class=row><div class=col-md-6><div class=form-group ng-repeat="item in vm.bybMeta.agent_Info" ng-if="item.systemRefId == \'NewManuScript.AgencyID\'" ng-class="{\'has-error\': (bybForm.submitted && bybForm.aid.$touched &&\n              bybForm.aid.$invalid)}"><label for={{item.systemRefId}}>{{item.datasetName}}</label> <input type=text name=aid disabled class=form-control ng-model=item.userValue placeholder={{item.additionalInfo}} ng-required=true id=aid><p class="help-block error build22" ng-if="(bybForm.submitted && bybForm.aid.$touched &&\n                    bybForm.aid.$invalid)">{{item.datasetName | camelCase}} is Required</p></div></div><div class=col-md-6><div class=form-group ng-class="{\'has-error\': bybForm.submitted && bybForm.ps.$touched &&\n              bybForm.ps.$invalid}" ng-repeat="item in vm.bybMeta.agent_Info" ng-if="item.systemRefId == \'NewManuScript.ProducerIDDropDown\'"><label for={{item.systemRefId}}>{{item.datasetName}}</label> <select name=ps ng-model=item.userValue class="custom_select form-control" ng-required=true><option value="">Please Select</option><option ng-repeat="option in item.valueName" value={{option.value}}>{{option.caption}}</option></select><p class="help-block error build22" ng-if="bybForm.submitted && bybForm.ps.$touched &&\n                  bybForm.ps.$invalid">{{item.datasetName | camelCase}} is Required</p></div></div></div><div class=row><div class=col-md-6><div class=form-group ng-repeat="item in vm.bybMeta.agent_Info" ng-class="{\'has-error\': bybForm.submitted && bybForm.cname.$touched &&\n              bybForm.cname.$invalid}" ng-if="item.systemRefId == \'NewManuScript.AgencyContactNameAcct\'"><label for={{item.systemRefId}}>{{item.datasetName}}</label> <input type=text name=cname class=form-control ng-model=item.userValue placeholder={{item.additionalInfo}} ng-required=true id=cname><p class="help-block error build22" ng-if="bybForm.submitted && bybForm.cname.$touched &&\n                  bybForm.cname.$invalid">{{item.datasetName | camelCase}} is Required</p></div></div><div class=col-md-6><div class=form-group><div class=form-group ng-repeat="item in vm.bybMeta.agent_Info" ng-class="{\'has-error\': bybForm.submitted && bybForm.cphone.$touched &&\n                  (bybForm.cphone.$invalid || vm.invalidPhone)}" ng-if="item.systemRefId == \'NewManuScript.AgencyContactPhoneNumberAcct\'"><label for={{item.systemRefId}}>{{item.datasetName}}</label> <input type=text name=cphone class=form-control ng-model=item.userValue placeholder={{item.additionalInfo}} ng-required=true ng-keydown=vm.checkLength($event,item.userValue,item.systemRefId) ng-blur=vm.checkvalid(item.userValue) id=cphone ng-attr-format-phone-number=true uservalue={{item.userValue}}><p class="help-block error build22" ng-if="bybForm.submitted && bybForm.cphone.$touched &&\n                        bybForm.cphone.$invalid && !vm.invalidPhone">{{item.datasetName | camelCase}} is Required</p><p class="help-block error build22" ng-if="vm.invalidPhone && bybForm.submitted">This field should be in the format of xxx-xxx-xxxx</p><!--\n                    </p>--></div></div></div></div><div class=row><div class=col-md-6><div class=form-group><div class=form-group ng-repeat="item in vm.bybMeta.agent_Info" ng-class="{\'has-error\': bybForm.submitted && bybForm.cemail.$touched &&\n                  (bybForm.cemail.$invalid || vm.emailErr)}" ng-if="item.systemRefId == \'NewManuScript.AgencyContactEmailAcct\'"><label for={{item.systemRefId}}>{{item.datasetName}}</label> <input type=email name=cemail class=form-control ng-model=item.userValue placeholder={{item.additionalInfo}} ng-required=true ng-keydown=vm.checkLength($event,item.userValue,item.systemRefId) ng-blur=vm.checkvalidemail(item.userValue) id=cemail><p class="help-block error build22" ng-if="bybForm.submitted && \n                        bybForm.cemail.$invalid && !vm.emailErr">{{item.datasetName | camelCase}} is Required</p><p ng-if="vm.emailErr && bybForm.submitted" class="help-block error build22">This field should be an email address in the format "user@example.com"</p></div></div></div><!--<div ng-repeat="item in vm.bybMeta.agent_Info" ng-if="question.systemRefId != \'Do_you_intend_to_quote_a_Busine\'">\n\n            <div class="col-md-{{item.screenColumns}}" ng-if="item.controlName == \'Text_Box\'">\n              <div class="form-group" ng-class="{ \'has-error\': (bybForm.submitted && bybForm.{{item.systemRefId | dotWithUnderscore}}.$touched &&\n              bybForm.{{item.systemRefId | dotWithUnderscore}}.$invalid) ||\n              (vm.emailError == 1 && item.systemRefId == \'NewManuScript.AgencyContactEmailAcct\')||\n              (vm.invalidPhoneMsg == 1 && item.systemRefId == \'NewManuScript.AgencyContactPhoneNumberAcct\')}">\n                <label for="{{item.systemRefId}}">{{item.datasetName}}</label>\n                <input type="{{item.inputType}}" name="{{item.systemRefId | dotWithUnderscore}}" class="form-control" ng-model="item.userValue"\n                  placeholder="{{item.additionalInfo}}" ng-required="item.isRequired" ng-keydown="vm.checkLength($event,item.userValue,item.systemRefId)"\n                  ng-click="vm.emailError = 0; vm.invalidPhoneMsg=0 "\n                  ng-blur="item.userValue = vm.changePhoneFormat(item.userValue, item.systemRefId)"\n                  id="{{item.systemRefId}}" />\n\n                <p ng-show="vm.tempZip = (bybForm.submitted && bybForm.{{item.systemRefId | dotWithUnderscore}}.$invalid &&\n                bybForm.{{item.systemRefId | dotWithUnderscore}}.$touched ) && !vm.invalidPhone"\n                  class="help-block error build22">\n                  {{item.datasetName | camelCase}} is required\n                </p>\n\n                <p class="help-block error build22" ng-if="vm.invalidPhone && item.systemRefId == \'NewManuScript.AgencyContactPhoneNumberAcct\'"\n                  ng-show="vm.invalidPhoneMsg==1">\n                  This field should be in the format of xxx-xxx-xxxx\n                </p>\n\n                <p ng-if="vm.emailErr && item.systemRefId == \'NewManuScript.AgencyContactEmailAcct\' && item.userValue" ng-show="vm.emailError == 1"\n                  class="help-block error build22">\n                  This field should be an email address in the format "user@example.com"\n                </p>\n              </div>\n            </div>\n            <div class="col-md-{{item.screenColumns}}" ng-if="item.controlName == \'Drop_Down\'">\n              <div class="form-group" ng-class="{ \'has-error\': bybForm.submitted && bybForm.{{item.systemRefId | dotWithUnderscore}}.$touched && bybForm.{{item.systemRefId | dotWithUnderscore}}.$invalid }">\n                <label for="{{item.systemRefId}}">{{item.datasetName}}</label>\n                <select name="{{item.systemRefId | dotWithUnderscore}}" ng-model="item.userValue" class="custom_select form-control" ng-required="item.isRequired"\n                  id="{{item.systemRefId}}">\n                  <option value="">Please Select</option>\n                  <option ng-repeat="option in item.valueName" value="{{option.value}}">{{option.caption}}</option>\n                </select>\n\n                <p ng-show="bybForm.submitted && bybForm.{{item.systemRefId | dotWithUnderscore}}.$invalid && bybForm.{{item.systemRefId | dotWithUnderscore}}.$touched"\n                  class="help-block error build22">\n                  {{item.datasetName | camelCase}} is required\n                </p>\n              </div>\n            </div>\n\n            <div class="clearfix" ng-if="item.hasRowBreak"></div>\n          </div>--><!-- End Other controls --><!-- Checkbox --><div class="col-md-6 checkradio-wrap" ng-repeat="question in vm.bybMeta.agent_Info" ng-if="question.systemRefId == \'NewManuScript.HowShouldWeContactYouAcct\'"><div class=col-md-6 style="padding:20px 0"><p><strong>{{question.datasetName}}</strong></p></div><div class="col-md-3 form-group" style="padding:20px 0" ng-repeat="value in question.valueName" ng-class="{ \'has-error\': bybForm.submitted && bybForm.{{question.systemRefId | dotWithUnderscore}}.$invalid && bybForm.{{question.systemRefId | dotWithUnderscore}}.$touched}"><input type=radio name=howshouldwe data-uuid={{question.uuid}} value={{value}} id={{$index+5}} ng-model=question.userValue ng-change=vm.lobNewValue(value) ng-required=true> <label ng-if="value == 1" for={{$index+5}}>Phone</label> <label ng-if="value == 2" for={{$index+5}}>Email</label></div><div class=col-md-12><!-- Required Validation --><p ng-show="bybForm.submitted && bybForm.howshouldwe.$invalid && bybForm.howshouldwe.$touched" class="help-block error build22" style=padding-left:53%;margin-top:-12px>This field is required</p></div></div><!-- End Checkbox --></div></div></div></div><!-- Insured Information Block --><div class=row><div class=white-pannel><h4 class=pannel-title>{{ vm.binfoHdr.datasetName }}</h4><div class=row><div ng-repeat="item in vm.bybMeta.insured_Info"><!-- Dropdown --><div class=col-md-{{item.screenColumns}} ng-if="item.controlName == \'Drop_Down\'"><div class=form-group ng-class="{\'has-error\': bybForm.submitted && bybForm.{{item.systemRefId | dotWithUnderscore}}.$touched && bybForm.{{item.systemRefId | dotWithUnderscore}}.$invalid }"><label>{{item.datasetName}}</label> <select name="{{item.systemRefId | dotWithUnderscore}}" ng-model=item.userValue class="custom_select form-control" ng-required=item.isRequired><option value="">Please Select</option><option ng-repeat="option in item.valueName" value={{option}}>{{option}}</option></select><!-- Required Validation --><p ng-show="bybForm.submitted && bybForm.{{item.systemRefId | dotWithUnderscore}}.$invalid && bybForm.{{item.systemRefId | dotWithUnderscore}}.$touched" class="help-block error build22">{{item.datasetName | camelCase}} is required</p></div></div><!-- end Dropdown --><!-- Textbox/Date --><div class=col-md-{{item.screenColumns}} ng-if="item.controlName == \'Date\'"><div class=form-group ng-class="{ \'has-error\': bybForm.submitted && bybForm.{{item.systemRefId | dotWithUnderscore}}.$touched && bybForm.{{item.systemRefId | dotWithUnderscore}}.$invalid ||\n                (vm.lengthOfZip && item.systemRefId == \'US_Zip_Code\') || (vm.dateFormatError[$index])}"><label for={{item.systemRefId}}>{{item.datasetName}}</label> <input type={{item.inputType}} name="{{item.systemRefId | dotWithUnderscore}}" class="form-control dtpicker lob" ng-model=item.userValue id={{item.systemRefId}} show-button-bar=false ng-click="vm.open($index ,$event)" placeholder={{item.additionalInfo}} ng-required=item.isRequired uib-datepicker-popup={{vm.format}} maxlength=10 is-open=vm.opened[$index] datepicker-options=vm.dateOptions close-text=Close datepicker-popup-template-url=app/partials/tpl.datepickerpopup.html ng-keydown="item.datasetName == \'ZIP CODE\' ? vm.ZIPCodeValidation($event) : \'\'" ng-keypress="vm.checkLength($event, item.userValue,item.systemRefId);" ng-keyup="vm.keyupevt($index, $event)" ng-blur="vm.keyupevt($index, $event)" date-val><!-- Required Validation --><p ng-if=!vm.dateFormatError[$index] ng-show="vm.tempZip = (bybForm.submitted && bybForm.{{item.systemRefId | dotWithUnderscore}}.$invalid && bybForm.{{item.systemRefId | dotWithUnderscore}}.$touched )" class="help-block error build22">{{item.datasetName | camelCase}} is required</p><!-- Date Error --><p ng-if=vm.dateFormatError[$index] class="help-block error build22">{{ vm.message[$index] }}</p></div></div><!-- end Textbox --><div class=clearfix ng-if=item.hasRowBreak></div></div></div></div></div><!-- End Insured Information Block --><!-- button --><div class=row><div class=empty-panel-formatter><div class=col-md-4><button type=submit class="btn btn-blue">Continue</button></div></div></div></form></div>');
$templateCache.put('app/views/beforeyoubegin/nav.beforeyoubegin.html','<ul class="nav list-group business-lockedleftnav"><li class="locked current">Business Details</li><li class="locked current">Select lines of business</li></ul><div style="position: absolute;bottom:0px;color: #909090;padding-left: 5px;">Copyright \xA9 2018 Utica Mutual Insurance Company, New Hartford, NY 13413 | All Rights Reserved</div>');
$templateCache.put('app/views/businessdetails/businessdetails.html','<div class="right-contents business-details-page"><div style="padding-left: 6px;" class="col col-md-12" id=Business_Detail><div class="col col-md-12"><h2>Business Details</h2></div></div><div style="padding-left: 6px;" ng-if=vm.showUSPSErrorMessage class="col col-md-12" id=Business_Detail><div class="col col-md-12"><div class="alert alert-danger">{{vm.USPSFailureErrorMessage}}</div></div></div><form data-toggle=validator class=formClass form-submit-validation="" name=businessDetailsForm ng-submit=vm.triggerSubmit(businessDetailsForm.$valid,businessDetailsForm) id=businessDetailsFormID novalidate autocomplete=off ng-show=vm.pageLoadShowWidget><div class="white-pannel col-md-5 businessNameCSS"><h4 class=pannel-title ng-repeat="header in vm.businessName" ng-if="header.controlName == \'Header\'">{{header.datasetName}}</h4><div class=form-group ng-class="{ \'has-error\':  businessDetailsForm.submitted &&\n                                          businessDetailsForm.businessName.$touched &&\n                                          businessDetailsForm.businessName.$invalid }" ng-repeat="header in vm.businessName" ng-if="header.controlName == \'Text_Box\'"><input type=text id=businessName_ID name=businessName class=form-control placeholder={{header.additionalInfo}} systemrefid={{header.systemRefId}} autocomplete=on|off ng-disabled=vm.disableCompletePage data-uuid={{header.uuid}} ng-model=vm.businessDetailsForm.uuid[header.uuid] ng-change="vm.updateLoc(vm.businessDetailsForm.uuid[header.uuid], header.uuid)" ng-blur=vm.USPSandEXPERIAN(false); required g-places-autocomplete options=vm.autocompleteOptions><div class=help-block ng-messages="businessDetailsForm.submitted && businessDetailsForm.businessName.$error" ng-if=businessDetailsForm.businessName.$touched><p ng-message=required id={{header.systemRefId}}>{{header.datasetName | camelCase}} is required</p></div></div></div><div class="col col-md-9"><!-- business mail address block --><div class=row id=Business_Location><div class="white-pannel businessLocationCSS"><a class="pull-right businessMagicFill" href="" ng-show=vm.googleData ng-class="{ disableMagicFill: vm.disableCompletePage}" ng-click="vm.disableCompletePage ? \'\' : vm.googleAPIAddress()"><img ng-src="{{main.assetPath+\'magic_button.png\'}}"><p>Sources: Google</p></a><div ng-repeat="header in vm.businessAddress" ng-if="header.controlName == \'Header\'"><h4 class=pannel-title>{{header.datasetName}}</h4><h5 class=panel-title>{{header.additionalInfo}}</h5></div><div class=row><div ng-repeat="item in vm.businessAddress"><!-- input box --><div class=col-md-{{item.screenColumns}} ng-if="item.controlName==\'Text_Box\'"><div class=form-group ng-class="{ \'has-error\':  businessDetailsForm.submitted &&\n                                              businessDetailsForm.{{item.systemRefId | dotWithUnderscore}}.$touched &&\n                                              businessDetailsForm.{{item.systemRefId | dotWithUnderscore}}.$invalid ||\n                                              (vm.lengthOfZip && item.systemRefId == \'mailingAddress.ZIP\')}"><label>{{item.datasetName}}</label> <input type={{item.inputType}} name="{{item.systemRefId | dotWithUnderscore}}" class=form-control id="{{item.systemRefId | dotWithUnderscore}}" placeholder={{item.additionalInfo}} ng-model=vm.businessDetailsForm.uuid[item.uuid] autocomplete=on|off ng-required=item.isRequired ng-disabled="vm.disableCompletePage ? true : item.nonEditableFeild;" ng-focus="vm.lengthOfZip = false;\n                                    vm.pasteData = false;" ng-maxlength="{\n                                         5:item.systemRefId == \'mailingAddress.ZIP\',\n                                        40:item.systemRefId == \'mailingAddress.address1\' ||\n                                           item.systemRefId == \'mailingAddress.address2\',\n                                        30:item.systemRefId == \'mailingAddress.city\',\n                                      }" ng-change="vm.gAutofill(vm.businessDetailsForm.uuid[item.uuid], item.uuid)" ng-blur="vm.lengthOfZip = item.systemRefId == \'mailingAddress.ZIP\' ? vm.checkZipLessThan5(vm.businessDetailsForm.uuid[item.uuid]) : false;\n                                  vm.zipCodeLookUP(item.systemRefId,item.eventName,vm.lengthOfZip,vm.businessDetailsForm.uuid[item.uuid]);\n                                  item.systemRefId == \'mailingAddress.ZIP\'? vm.USPSandEXPERIAN(true) : vm.USPSandEXPERIAN(false);" ng-paste="vm.pasteData = true" ng-keypress="vm.pasteData = false;" pasteboolean={{vm.pasteData}} g-places-autocomplete options=vm.streetAutocompleteOptions ng-attr-zip-code="{{ item.systemRefId == \'mailingAddress.ZIP\' ? true : false }}" input-length-limit paste-number-only="{\n                                              5:{{item.systemRefId == \'mailingAddress.ZIP\'}}\n                                          }" paste-alpha-numeric-symbol="{\n                                                      40:{{item.systemRefId == \'mailingAddress.address1\' ||\n                                                         item.systemRefId == \'mailingAddress.address2\'}},\n                                                      30:{{item.systemRefId == \'mailingAddress.city\'}},\n                                                    }" systemrefid={{item.systemRefId}} uservalue={{vm.businessDetailsForm.uuid[item.uuid]}}><p id={{item.systemRefId}} ng-show="vm.tempZip = (businessDetailsForm.submitted &&\n                            businessDetailsForm.{{item.systemRefId | dotWithUnderscore}}.$invalid &&\n                            businessDetailsForm.{{item.systemRefId | dotWithUnderscore}}.$touched )" class="help-block error build22">{{item.datasetName | camelCase}} is required</p><p id={{item.systemRefId}} ng-if="vm.lengthOfZip && item.systemRefId == \'mailingAddress.ZIP\'" ng-hide="(businessDetailsForm.submitted &&\n                            businessDetailsForm.{{item.systemRefId | dotWithUnderscore}}.$invalid &&\n                            businessDetailsForm.{{item.systemRefId | dotWithUnderscore}}.$touched)" class="help-block error build22">Invalid Zip Code</p></div></div><!-- end of input box --><!-- list menu (Drop_Down)--><div class=col-md-{{item.screenColumns}} ng-if="item.controlName==\'Drop_Down\'"><div class=form-group ng-class="{ \'has-error\':  businessDetailsForm.submitted &&\n                                              businessDetailsForm.{{item.systemRefId | dotWithUnderscore}}.$touched &&\n                                              businessDetailsForm.{{item.systemRefId | dotWithUnderscore}}.$invalid }"><label>{{item.datasetName}}</label> <select name="{{item.systemRefId | dotWithUnderscore}}" ng-model=vm.businessDetailsForm.uuid[item.uuid] class="custom_select form-control" ng-required=item.isRequired><!-- keep the first one always empty this helps with validation and to cancel the selection --><option value="">Select a State</option><option ng-repeat="option in item.valueName" value={{option}}>{{option}}</option></select><p id={{item.systemRefId}} ng-show="businessDetailsForm.submitted && businessDetailsForm.{{item.systemRefId | dotWithUnderscore}}.$invalid && businessDetailsForm.{{item.systemRefId | dotWithUnderscore}}.$touched" class="help-block error build22">{{item.datasetName | camelCase}} is required</p></div></div><!-- end of list menu (Drop_Down)--><div class=clearfix ng-if=item.hasRowBreak></div></div></div></div></div><!-- end of business mail address block --><!-- Start of Verify Business Widget  --><div class=row id=verifyBusinessID><div class="white-pannel verifyBusinessCSS"><h4 class=pannel-title ng-repeat="header in vm.verifyBusiness" ng-if="header.controlName == \'Header\'">{{header.datasetName}}</h4><div class=form-group id=scrollable-dropdown-menu ng-class="{ \'has-error\': businessDetailsForm.submitted &&\n                                        businessDetailsForm.verifyBusiness_name.$touched &&\n                                        (vm.businessDetailsForm.uuid[verifyBusiness.uuid] == \' \' ||\n                                        businessDetailsForm.verifyBusiness_name.$invalid) &&\n                                       verifyBusiness.editable.toLowerCase() == \'y\' }" ng-repeat="verifyBusiness in vm.verifyBusiness" ng-if="verifyBusiness.controlName == \'Drop_Down\'"><label>{{verifyBusiness.datasetName}}</label><!-- <input  type="text"\n                  id="verifyBusiness_ID"\n                  name="verifyBusiness_name"\n                  class="form-control custom_select"\n                  placeholder="{{verifyBusiness.additionalInfo}}"\n                  options="vm.autocompleteOptions"\n                  systemrefid="{{verifyBusiness.systemRefId}}"\n                  data-uuid="{{verifyBusiness.uuid}}"\n                  ng-model="vm.businessDetailsForm.uuid[verifyBusiness.uuid]"\n                  ng-disabled="verifyBusiness.editable.toLowerCase() == \'n\'? true : False"\n                  ng-required="verifyBusiness.isRequired"\n                  readonly\n                  uib-typeahead="value.data for value in vm.verifyBusinessDropDown"\n                  typeahead-focus\n                   typeahead-on-select="vm.sendDataToExperian($item.id)" > --><div class=dropdown uib-dropdown><!-- <button id="button-template-url" ng-model="vm.businessDetailsForm.uuid[verifyBusiness.uuid]" type="button"\n                          class="verifyDropDownClass btn btn-default"\n                          uib-dropdown-toggle\n                           ng-required="verifyBusiness.isRequired"\n                           ng-disabled="verifyBusiness.editable.toLowerCase() == \'n\'? true : False">\n                       {{ (vm.businessDetailsForm.uuid[verifyBusiness.uuid] = vm.selectedVerifyBusiness) || verifyBusiness.datasetName}}\n                  </button> --> <input id=button-template-url ng-model=vm.businessDetailsForm.uuid[verifyBusiness.uuid] type=text id=verifyBusiness_ID name=verifyBusiness_name class="form-control custom_select_verify verifyDropDownClass btn btn-default" uib-dropdown-toggle ng-required=verifyBusiness.isRequired placeholder={{verifyBusiness.additionalInfo}} readonly ng-init="vm.selectedVerifyBusiness   = verifyBusiness.userValue" ng-change="{{vm.businessDetailsForm.uuid[verifyBusiness.uuid] = vm.selectedVerifyBusiness}};" ng-disabled=" vm.disableCompletePage ? true : verifyBusiness.editable.toLowerCase() == \'n\'? true : false;"><ul class=dropdown-menu uib-dropdown-menu template-url=app/views/businessdetails/verifyBusinessValue.html aria-labelledby=button-template-url></ul></div><p id={{verifyBusiness.systemRefId}} ng-show="businessDetailsForm.submitted && businessDetailsForm.verifyBusiness_name.$touched &&\n                      (vm.businessDetailsForm.uuid[verifyBusiness.uuid] == \' \' || businessDetailsForm.verifyBusiness_name.$invalid)  &&\n                       verifyBusiness.editable.toLowerCase() == \'y\'" class="help-block error build22">Select Business is required</p></div></div></div><!-- End of Verify Business Widget --><!-- Business class autocomplete block --><div class=row id=Business_Class><div class="white-pannel businessClassCSS"><h4 class=pannel-title ng-repeat="header in vm.businessClassAndUserValue" ng-if="header.controlName == \'Header\'">{{header.datasetName}}</h4><!-- we use position 1 because the fields get sorted by ParseJSON --><div class=form-group id=scrollable-dropdown-menu ng-class="{ \'has-error\': businessDetailsForm.submitted &&\n                                        businessDetailsForm.businessClassAndCode.$touched &&\n                                        businessDetailsForm.businessClassAndCode.$invalid &&\n                                        businessClassName.editable.toLowerCase() == \'y\' ||\n                                        vm.businessClassError == 1}" ng-repeat="businessClassName in vm.businessClassAndUserValue" ng-if="businessClassName.controlName == \'Drop_Down\'"><input type=text ng-model=vm.businessDetailsForm.uuid[vm.businessClassAndUserValue[1].uuid] name=businessClassAndCode uib-typeahead="value.value as value.caption for value in vm.businessClassAndCode | filter:$viewValue" typeahead-min-length=3 class=form-control placeholder={{businessClassName.additionalInfo}} required ng-disabled="vm.disableCompletePage ? true : businessClassName.editable.toLowerCase() == \'n\'? true : false;" typeahead-on-select="vm.businessDetailsForm.uuid[vm.businessClassAndUserValue[1].uuid].length > 0 ?\n                                       vm.businessClassValidation(vm.businessDetailsForm.uuid[vm.businessClassAndUserValue[1].uuid]) : vm.noResults = false;\n                                       vm.verisk();" typeahead-select-on-exact=true typeahead-input-formatter=vm.businessClassCaptionValue($model,vm.businessClassAndCode) ng-focus="vm.businessClassError = 0;" ng-change="vm.businessDetailsForm.uuid[vm.businessClassAndUserValue[1].uuid].length > 0 ?\n                             vm.businessClassValidation(vm.businessDetailsForm.uuid[vm.businessClassAndUserValue[1].uuid]) : vm.noResults = false;"><p id={{businessClassName.systemRefId}} ng-show="businessDetailsForm.submitted && businessDetailsForm.businessClassAndCode.$invalid &&\n                      businessDetailsForm.businessClassAndCode.$touched &&\n                      businessClassName.editable.toLowerCase() == \'y\'" class="help-block error build22">Business Class is required</p><p id={{businessClassName.systemRefId}} ng-if="vm.noResults && vm.businessClassError == 1" ng-hide="(businessDetailsForm.businessClassAndCode.$invalid && businessDetailsForm.businessClassAndCode.$touched)" class="help-block error build22">No Business Class Found</p></div></div></div><!-- end of business class autocomplete --><!-- business info block --><div class=row><div class="white-pannel businessInfoCSS"><a class="pull-right businessMagicFill businessInfoMagicFill" href="" ng-if=vm.businessInfoMagicButton ng-class="{ disableMagicFill: vm.disableCompletePage}" ng-click="vm.disableCompletePage ? \'\' : vm.businessInfoMagicFill(vm.businessInfo)"><img ng-src="{{main.assetPath+\'magic_button.png\'}}"><p>Sources: Verisk ISO</p></a><h4 class=pannel-title ng-repeat="header in vm.businessInfo" ng-if="header.controlName == \'Header\'">{{header.datasetName}}</h4><div class=row name=businessInfoTag><div ng-repeat="item in vm.businessInfo" id=Business_Info><!-- start of year and phone input box --><div class=col-md-{{item.screenColumns}} ng-if="item.controlName==\'Text_Box\'" ng-show="item.appearUI == \'Y\' || vm.detailExplanation"><div class=form-group ng-class="{ \'has-error\': businessDetailsForm.submitted &&\n                                                    businessDetailsForm.{{item.systemRefId | dotWithUnderscore}}.$touched &&\n                                                    businessDetailsForm.{{item.systemRefId | dotWithUnderscore}}.$invalid ||\n                                                    businessDetailsForm.{{item.systemRefId | dotWithUnderscore}}.$error.number ||\n                                                    (item.systemRefId  ==  \'BusinessContact.BusinessPhone\' && vm.invalidPhoneFlag == 1)||\n                                                    (item.systemRefId  ==  \'BusinessContact.BusinessPhone\' && vm.invalidPhoneFormatFlag == 1),\n                                                    \'has-error1\': vm.flag == \'1\' &&  item.systemRefId  ==  \'misc.YearBusinessStarted\'}"><label>{{item.datasetName}}</label> <input type={{item.inputType}} data-uuid={{item.uuid}} id="{{item.systemRefId | dotWithUnderscore}}" name="{{item.systemRefId | dotWithUnderscore}}" class=form-control ng-model=vm.businessDetailsForm.uuid[item.uuid] placeholder={{item.additionalInfo}} ng-focus="vm.invalidPhoneFlag = 0;\n                                  vm.invalidPhoneFormatFlag = 0;\n                                  vm.invalidPhoneFormat = false;\n                                  vm.flag = 0;\n                                  vm.pasteData = false;" autocomplete=on|off ng-disabled=vm.disableCompletePage ng-required="vm.requireddetailExplanation = (vm.detailExplanation == false || vm.detailExplanation == undefined ) &&\n                                                                    (item.systemRefId == \'Details_Explanation\') ?  false : item.isRequired " ng-maxlength="{\n                                        4:item.systemRefId == \'misc.YearBusinessStarted\',\n                                        10:item.systemRefId == \'BusinessContact.BusinessPhone\'\n                                      }" ng-keydown="vm.hide2FieldWhenYearNULL(item.systemRefId,vm.businessDetailsForm.uuid[item.uuid]);\n                                    vm.makeNULLfor2HiddenField(item.systemRefId,item.uuid);\n                                    vm.pasteData = false;" ng-blur="vm.businessDetailsForm.uuid[item.uuid].length < 4 ? vm.checkTheAppianRule(vm.businessDetailsForm.uuid[item.uuid], item.systemRefId) : \'\';\n                                  vm.phoneLengthErrorMessage(vm.businessDetailsForm.uuid[item.uuid], item.systemRefId);" ng-change="vm.businessDetailsForm.uuid[item.uuid].length == 4 ? vm.checkTheAppianRule(vm.businessDetailsForm.uuid[item.uuid], item.systemRefId) : \'\'" ng-attr-format-phone-number="{{ item.systemRefId == \'BusinessContact.BusinessPhone\' ? true : false }}" ng-attr-zip-code="{{ item.systemRefId == \'misc.YearBusinessStarted\' ? true : false }}" input-length-limit paste-number-only="{\n                                              4:{{item.systemRefId == \'misc.YearBusinessStarted\'}},\n                                              10:{{item.systemRefId == \'BusinessContact.BusinessPhone\'}}\n                                            }" ng-paste="vm.pasteData = true" pasteboolean={{vm.pasteData}} systemrefid={{item.systemRefId}} uservalue={{vm.businessDetailsForm.uuid[item.uuid]}}><p id={{item.systemRefId}} ng-show="businessDetailsForm.submitted && businessDetailsForm.{{item.systemRefId | dotWithUnderscore}}.$invalid && businessDetailsForm.{{item.systemRefId | dotWithUnderscore}}.$touched" class="help-block error build22">{{item.datasetName | camelCase}} is required</p><p id={{item.systemRefId}} ng-if="vm.checkInvalidYear == true && item.systemRefId  ==  \'misc.YearBusinessStarted\' && vm.businessDetailsForm.uuid[item.uuid]" class="help-block error build22" ng-show=" vm.flag == \'1\'">{{vm.invalidYear}}</p><p id={{item.systemRefId}} ng-if="item.systemRefId  ==  \'BusinessContact.BusinessPhone\' && vm.invalidPhoneFormat == true && vm.businessDetailsForm.uuid[item.uuid]" class="help-block error build22" ng-show=" vm.invalidPhoneFormatFlag == \'1\'">This field should be in the format of xxx-xxx-xxxx</p></div></div><!-- end of year and phone input box --><!-- currency --><div class=col-md-{{item.screenColumns}} ng-if="item.controlName==\'Currency\'"><div class=form-group ng-class="{ \'has-error\': (businessDetailsForm.submitted &&\n                                                                businessDetailsForm.{{item.systemRefId | dotWithUnderscore}}.$touched &&\n                                                                businessDetailsForm.{{item.systemRefId | dotWithUnderscore}}.$invalid) ||\n                                                                (vm.saleFlag == 1 && item.systemRefId == \'misc.TotalAnnualSales\') ||\n                                                                (vm.invalidAnnualMessage == 1 && item.systemRefId  ==  \'misc.TotalAnnualSales\') }"><label>{{item.datasetName}}</label> <input type={{item.inputType}} data-uuid={{item.uuid}} id="{{item.systemRefId | dotWithUnderscore}}" name="{{item.systemRefId | dotWithUnderscore}}" class=form-control ng-model=vm.businessDetailsForm.uuid[item.uuid] placeholder={{item.additionalInfo}} autocomplete=on|off ng-disabled=vm.disableCompletePage ng-focus="vm.errorMessageAnnualSale = false;\n                                  vm.salesError = false;\n                                  vm.invalidAnnualMessage = 0;\n                                  vm.saleFlag = 0;\n                                   vm.pasteData = false;" ng-required=item.isRequired ng-maxlength="{\n                                        12:item.systemRefId == \'misc.TotalAnnualSales\'\n                                      }" ng-blur=vm.checkTheAppianRule(vm.businessDetailsForm.uuid[item.uuid],item.systemRefId); ng-attr-format-currency="{{ item.systemRefId == \'misc.TotalAnnualSales\' ? true : false }}" paste-number-only="{\n                                               12:{{item.systemRefId == \'misc.TotalAnnualSales\'}}\n                                            }" input-length-limit ng-paste="vm.pasteData = true" pasteboolean={{vm.pasteData}} systemrefid={{item.systemRefId}} uservalue={{vm.businessDetailsForm.uuid[item.uuid]}}><p id={{item.systemRefId}} ng-show="businessDetailsForm.submitted && businessDetailsForm.{{item.systemRefId | dotWithUnderscore}}.$invalid && businessDetailsForm.{{item.systemRefId | dotWithUnderscore}}.$touched" class="help-block error build22">{{item.datasetName | camelCase}} is required</p><p id={{item.systemRefId}} ng-if="vm.invalidAnnualMessage == 1 && item.systemRefId  ==  \'misc.TotalAnnualSales\'" ng-show="vm.errorMessageAnnualSale == true" ng-hide="businessDetailsForm.submitted && businessDetailsForm.{{item.systemRefId | dotWithUnderscore}}.$invalid && businessDetailsForm.{{item.systemRefId | dotWithUnderscore}}.$touched" class="help-block error build22">Invalid Annual Sales</p><p id={{item.systemRefId}} class="help-block error build22" ng-if="vm.saleFlag == 1  && item.systemRefId  ==  \'misc.TotalAnnualSales\'" ng-show="vm.salesError == true " ng-hide="businessDetailsForm.submitted && businessDetailsForm.{{item.systemRefId | dotWithUnderscore}}.$invalid && businessDetailsForm.{{item.systemRefId | dotWithUnderscore}}.$touched">{{vm.salesErrorMsg}}</p></div></div><!-- end of currency --><!-- list menu (Drop_Down)--><div class=col-md-6 ng-if="item.controlName == \'Drop_Down\' && item.appearUI == \'Y\'"><!-- {{item.screenColumns}} --><div class=form-group ng-class="{ \'has-error\': businessDetailsForm.submitted &&\n                                                                businessDetailsForm.{{item.systemRefId | dotWithUnderscore}}.$touched &&\n                                                                businessDetailsForm.{{item.systemRefId | dotWithUnderscore}}.$invalid }"><label>{{item.datasetName}}</label> <select name="{{item.systemRefId | dotWithUnderscore}}" ng-model=vm.businessDetailsForm.uuid[item.uuid] class="custom_select form-control" ng-required=item.isRequired ng-blur="vm.businessStatus == true ? vm.businessDetailsForm.uuid[item.uuid] : null;" ng-change=vm.makeNULLfor2HiddenField(item.systemRefId,item.uuid) ng-disabled=vm.disableCompletePage><!-- keep the first one always empty this helps with validation and to cancel the selection --><option value="">Select Status</option><option ng-repeat="option in item.valueName" value={{option}}>{{option}}</option></select><p id={{item.systemRefId}} ng-show="businessDetailsForm.submitted && businessDetailsForm.{{item.systemRefId | dotWithUnderscore}}.$invalid && businessDetailsForm.{{item.systemRefId | dotWithUnderscore}}.$touched" class="help-block error build22">{{item.datasetName | camelCase}} is required</p></div></div><!-- end of list menu (Drop_Down)--><!-- <div class="clearfix" ng-if="item.hasRowBreak"></div> --></div></div><!--\n        <div class="row" ng-if="vm.businessDetailsForm.dataset.businessinfo[\'Text_Box122\']>=vm.currentYear">\n          <div class="form-group"\n               ng-class="{ \'has-error\': businessDetailsForm.extrafield.$touched && businessDetailsForm.extrafield.$invalid }">\n            <div class="col-md-8">\n              Pleae select the current business status\n              <p\n                ng-show="businessDetailsForm.extrafield.$invalid && businessDetailsForm.extrafield.$touched"\n                class="help-block error build22">This field is required</p>\n            </div>\n            <div class="col-md-4">\n              <select name="extrafield" ng-model="vm.state" class="custom_select form-control" placeholder="Please Select" required>\n                <option value="" selected>Please select</option>\n                <option value="Open and Operating" selected>Open and Operating</option>\n                <option value="Opening in the Future" selected>Opening in the Future</option>\n              </select>\n            </div>\n          </div>\n        </div>\n      --></div></div><!-- end of business info block --><div class="row marginLeft"><div class=empty-panel-formatter><div class=col-md-4><button type=submit class="btn btn-blue businessDetailSaveButton">Save &amp; Continue</button></div></div></div></div><div class="col-md-3 right-col"><div class="card right-rail boxLeft" ng-show=vm.googleData style="background-color: #F5F7F8; margin-top:20px"><div class="panel panel-default googleImageDIV"><div class="panel-heading clearfix"><h3 style="float:left; color: gray;" class=panel-title>Google Maps</h3><span style=float:right><img style=padding:12px ng-src="{{main.assetPath+\'google.png\'}}"></span></div><div class=panel-body><p><strong>ADDRESS</strong></p><!--{{vm.data}}--><div class="panel-sec googleFormatAdd" id=googleFormatAddID1 ng-bind-html=vm.streetAPTSuite></div><div class="panel-sec googleFormatAdd" id=googleFormatAddID2 ng-bind-html=vm.cityStateZipcode></div><div class="panel-sec googleFormatAdd" id=googleFormatAddID3 ng-bind-html=vm.country></div></div><img class=googleImage style="width:100%; height:110px" style=padding:12px ng-src="https://maps.googleapis.com/maps/api/streetview?size=400x400&location={{vm.lat}},{{vm.lng}}&key=AIzaSyCirM4vfDlez6kIT6TiVD8nS9Veti4iyPg"></div></div><!-- <div class="card right-rail verisk" style="background-color: #F5F7F8;">\n      <div class="panel panel-default">\n        <div class="panel-heading clearfix">\n          <h3 style="float:left; color: gray;" class="panel-title">Verisk</h3>\n        </div>\n\n        <div class="panel-body">\n          <div class="panel-sec">\n            <h5>\n              <b>SQUARE FOOTAGE</b>\n            </h5>\n            <p class="grayedcolor">2600</p>\n          </div>\n          <div class="panel-sec">\n            <h5>\n              <b>NUMBER OF STORIES</b>\n            </h5>\n            <p class="grayedcolor">1</p>\n          </div>\n          <div class="panel-sec">\n            <h5>\n              <b>CONSTRUCTION TYPE</b>\n            </h5>\n            <p class="grayedcolor">Mansory Non-Combination</p>\n          </div>\n        </div>\n        <a class="ext-link" href="">\n          <b>View in verisk</b>\n        </a>\n      </div>\n    </div> --><!-- Verisk Business Class Suggestion --><div class="card right-rail marginTop boxLeft" ng-show=vm.veriskData style="background-color: #F5F7F8;"><div class="panel panel-default"><div class="panel-heading clearfix"><h3 style="float:left; color: black;" class=panel-title><strong>Verisk ISO</strong></h3></div><div class=panel-body><p><strong>SUGGESTED CLASSES</strong></p><div class=panel-sec ng-repeat="veriskBC in vm.veriskBusinessName">{{veriskBC}}</div></div></div></div><!-- Verisk Year Started --><div class="card right-rail marginTop boxLeft" ng-show=vm.veriskData style="background-color: #F5F7F8;"><div class="panel panel-default"><div class="panel-heading clearfix"><h3 style="float:left; color: black;" class=panel-title><strong>Verisk ISO</strong></h3></div><div class=panel-body><p><strong>YEAR STARTED</strong></p><div class=panel-sec ng-bind-html=vm.veriskYearStarted></div></div><div class=panel-body><p><strong>ANNUAL SALES</strong></p><div class=panel-sec ng-bind-html=vm.veriskAnnualSales></div></div><div class=panel-body><p><strong>PHONE NUMBER</strong></p><div class=panel-sec ng-bind-html="vm.veriskPhoneNumber | veriskPhoneFormat"></div></div></div></div><!-- verisk Annual Sales --><!-- <div class="card right-rail marginTop" ng-show="vm.veriskData" style="background-color: #F5F7F8;">\n        <div class="panel panel-default">\n\n          <div class="panel-heading clearfix">\n            <h3 style="float:left; color: gray;" class="panel-title">Google</h3>\n            <span style="float:right">\n              <img style="padding:12px" ng-src="{{main.assetPath+\'google.png\'}}" />\n            </span>\n          </div>\n\n          <div class="panel-body">\n            <p>\n              <strong>ANNUAL SALES</strong>\n            </p>\n            <div class="panel-sec" ng-bind-html="vm.veriskAnnualSales">\n            </div>\n          </div>\n\n        </div>\n      </div> --><!-- Verisk Phone Number --><!-- <div class="card right-rail marginTop" ng-show="vm.veriskData" style="background-color: #F5F7F8;">\n        <div class="panel panel-default">\n\n          <div class="panel-heading clearfix">\n            <h3 style="float:left; color: gray;" class="panel-title">Google</h3>\n            <span style="float:right">\n              <img style="padding:12px" ng-src="{{main.assetPath+\'google.png\'}}" />\n            </span>\n          </div>\n\n          <div class="panel-body">\n            <p>\n              <strong>PHONE NUMBER</strong>\n            </p>\n            <div class="panel-sec" ng-bind-html="vm.veriskPhoneNumber | veriskPhoneFormat">\n            </div>\n          </div>\n\n        </div>\n      </div> --></div></form></div>');
$templateCache.put('app/views/businessdetails/nav.businessdetails.html','<div style="min-height: 460px;"><ul class="nav list-group business-leftnav"><li><a class="list-group-item active" href="" ng-click="vm.scrollTo(\'Business_Detail\')"><i class="icon-home icon-1x"></i>Business Details</a></li><li><a class=list-group-item2 href="" ng-click="vm.scrollTo(\'Business_Name\')"><i class="icon-home icon-1x"></i>Business Name</a></li><li><a class=list-group-item2 href="" ng-click="vm.scrollTo(\'Business_Location\')"><i class="icon-home icon-1x"></i>Business Location</a></li><li><a class=list-group-item2 href="" ng-click="vm.scrollTo(\'Verify_Business\')"><i class="icon-home icon-1x"></i>Verify Business</a></li><li><a class=list-group-item2 href="" ng-click="vm.scrollTo(\'Business_Class\')"><i class="icon-home icon-1x"></i>Business Class</a></li><li><a class=list-group-item2 href="" ng-click="vm.scrollTo(\'Business_Info\')"><i class="icon-home icon-1x"></i>Business Info</a></li></ul><ul class="nav list-group business-lockedleftnav"><li class="locked current">Select lines of business</li></ul></div><div style="position: relative;bottom:0px;color: #909090;padding-left: 5px;">Copyright \xA9 2018 Utica Mutual Insurance Company, New Hartford, NY 13413 | All Rights Reserved</div>');
$templateCache.put('app/views/businessdetails/verifyBusinessValue.html','<ul class="customDropDownClass dropdown-menu dropdown-menu-businessClass multi-column columns-3"><div class="row verifyDropRepeatClass" ng-click="vm.assignVerifyBusinessNameTOngModal(item.BusinessName,item.id);\n                  vm.openPOPUP(item.BusinessName,item.completeAddress,item.defaultValue);" data-toggle=tooltip title={{item.completeAddress}} ng-repeat="item in vm.verifyBusinessDropDown"><div class="col-sm-4 verifyDropClass"><ul class=multi-column-dropdown><li class=verifyDropValueClass><a href="">{{item.BusinessName}}</a></li></ul></div><div class="col-sm-4 verifyDropClass"><ul class=multi-column-dropdown><li class=verifyDropValueClass><a href="">{{item.City}}</a></li></ul></div><div class="col-sm-4 verifyDropClass"><ul class=multi-column-dropdown><li class=verifyDropValueClass><a href="">{{item.streetAddress}}</a></li></ul></div></div></ul><!-- POP UP window for Verify Business --><script type=text/ng-template id=blocks/modal/verifyPOPUP.html><!-- header -->\n      <div class="modal-header" style="border-bottom: 0px;">\n        <h4 class="modal-title" id="myModalLabel">Verify Business</h4>\n      </div>\n\n      <!-- body -->\n      <div class="modal-body" ng-if="!defaultValue">\n        You have selected <strong>{{verifyBusinessNamePOPUP}}</strong> located at <strong>{{verifyBusinessCompanyAddressPOPUP}}</strong>\n      </div>\n      <div class="modal-body" ng-if="defaultValue">\n        You have selected <strong>{{verifyBusinessNamePOPUP}}</strong>\n      </div>\n\n      <!-- footer -->\n      <div class="modal-footer">\n        <button type="button" class="btn btn-default" ng-click="closePOPUP()">Cancel</button>\n        <button type="button" class="btn btn-primary" ng-click="sendExperianID()">OK</button>\n      </div></script>');
$templateCache.put('app/views/details/details.html','<div class="account-page details-page"><div data-ng-include="\'app/partials/app-header.html\'"></div><div class=account-details-wrapper><div class=contents><div class=account-detail-content><div class="top clearfix"><span class=left>{{vm.pageContent.Main.MainBusinessName}}</span> <span class=right>Client ID #: {{vm.pageContent.Main.MainClientID}}</span></div><!--end .top--><div class="main clearfix"><div class=left><div class=title>Business Details</div><ul class=items><li><span>ACCOUNT ID</span> <input type=text data-ng-model=vm.pageContent.Main.MainClientID readonly data-ng-change=changeForm()></li><li><span>BUSINESS NAME</span> <input type=text data-ng-model=vm.pageContent.Main.MainBusinessName readonly data-ng-change=changeForm()></li><li><span>BUSINESS ADDRESS</span> <textarea style=width:70% data-ng-model=vm.addr readonly data-ng-change=changeForm()>, {{vm.addr}}</textarea></li><li><span>BUSINESS CLASS</span> <input type=text data-ng-model=vm.pageContent.Main.MainBusinessClass readonly data-ng-change=changeForm()></li><li><span>PRIMARY RISK STATE</span> <input type=text data-ng-model=vm.pageContent.Main.MainPrimaryRiskState readonly data-ng-change=changeForm()></li></ul><!--end .items--></div><!--end .left--><!--<div class="right clearfix pb82">\n            <a href="javascript:;" class="btn-blue">Resume Application</a>\n            <div class="right-main">\n              <div class="title">Lines of Business</div>\n              <p>More Information is needed to determine the eligible lines of business.</p>\n            </div>\n          </div>--><!--end .right--><div class="right filled-table clearfix"><!-- <a href="javascript:;" class="btn-blue">Resume Application</a> --><div class=right-main><div class=title>Lines of Business</div></div><div class="table-thead clearfix"><span style=width:28%>Line of Business</span> <span style=width:12%>Quote ID / Policy ID</span> <span style=width:12%>Status</span> <span style=width:12%>Effective Date</span> <span style=width:12%>Last Opened</span> <span style=width:12%>Premium</span> <span style=width:12%>Action</span></div><!--end .table-thead--><ul class=table-items><li><span class=color-black>{{vm.pageContent.BOP.BOPFullName}}</span> <span>{{vm.pageContent.BOP.BOPPolicyId}}</span> <span>{{vm.pageContent.BOP.BOPStatus}}</span> <span>{{vm.pageContent.BOP.BOPEffectiveDate | date: \'MM-dd-yyyy\'}}</span> <span>{{vm.pageContent.BOP.BOPLastAccessedDate.split(\' \')[0] | date: \'MM-dd-yyyy\'}}</span> <span>{{vm.pageContent.BOP.BOPCurrentPremium}}</span> <span><a style="color:#0078a8 !important" href="" ng-click="vm.getAction(vm.pageContent.BOP.BOPPolicyId, vm.pageContent.BOP.BOPAction.toString().replace(\'Continue\', \'Resume quote\'), vm.pageContent.BOP.BOPStatus)">{{vm.pageContent.BOP.BOPAction.toString().replace(\'Continue\', \'Resume quote\')}}</a></span><!--<div>\n                <a data-ng-href="#!/{{item[\'Action\'] === \'View Policy\' ? \'premiumSummary\' : \'beforeyoubegin\'}}" class="color-blue">{{item["Action"]}}</a>\n              </div>--></li><li><span class=width-100>The following lines of business are quoted in Commercial Edge Interface.We will direct you to Commercial Edge to complete.</span></li><li ng-if="vm.pageContent.Umbrella.UmbrellaFullName !=\'\'"><span class=color-black>{{vm.pageContent.Umbrella.UmbrellaFullName}}</span> <span ng-class="{ \'hidecell\' : vm.pageContent.Umbrella.UmbrellaPolicyId == \'\'}">{{vm.pageContent.Umbrella.UmbrellaPolicyId}}</span> <span ng-class="{ \'hidecell\' : vm.pageContent.Umbrella.UmbrellaPolicyId == \'\'}">{{vm.pageContent.Umbrella.UmbrellaIsEligible}}</span> <span ng-class="{ \'hidecell\' : vm.pageContent.Umbrella.UmbrellaPolicyId == \'\'}">{{vm.pageContent.Umbrella.UmbrellaStatus}}</span> <span ng-class="{ \'hidecell\' : vm.pageContent.Umbrella.UmbrellaPolicyId == \'\'}"></span> <span ng-class="{ \'hidecell\' : vm.pageContent.Umbrella.UmbrellaPolicyId == \'\'}">{{vm.pageContent.Umbrella.UmbrellaCurrentPremium}}</span> <span><a style="color:#0078a8 !important" href="/DSCIQuote/outbound?landingPageInCE=Select_Line_of_Business&lob=CarrierBusinessOwners">{{vm.pageContent.Umbrella.UmbrellaAction.toString().replace(\'Continue\', \'Resume quote\')}}</a> </span><!--<div>\n                <a data-ng-href="#!/{{item[\'Action\'] === \'View Policy\' ? \'premiumSummary\' : \'beforeyoubegin\'}}" class="color-blue">{{item["Action"]}}</a>\n              </div>--></li><li ng-if="vm.pageContent.CAuto.CAutoFullName !=\'\'"><span class=color-black>{{vm.pageContent.CAuto.CAutoFullName}}</span> <span ng-class="{ \'hidecell\' : vm.pageContent.CAuto.CAutoPolicyId == \'\'}">{{vm.pageContent.CAuto.CAutoPolicyId}}</span> <span ng-class="{ \'hidecell\' : vm.pageContent.CAuto.CAutoPolicyId == \'\'}">{{vm.pageContent.CAuto.CAutoIsEligible}}</span> <span ng-class="{ \'hidecell\' : vm.pageContent.CAuto.CAutoPolicyId == \'\'}">{{vm.pageContent.CAuto.CAutoStatus}}</span> <span ng-class="{ \'hidecell\' : vm.pageContent.CAuto.CAutoPolicyId == \'\'}">&nbsp;</span> <span ng-class="{ \'hidecell\' : vm.pageContent.CAuto.CAutoPolicyId == \'\'}">{{vm.pageContent.CAuto.CAutoCurrentPremium}}</span> <span><a style="color:#0078a8 !important" href="/DSCIQuote/outbound?landingPageInCE=Select_Line_of_Business&lob=CarrierBusinessOwners">{{vm.pageContent.CAuto.CAutoAction.toString().replace(\'Continue\', \'Resume quote\')}}</a> </span><!--<div>\n                  <a data-ng-href="#!/{{item[\'Action\'] === \'View Policy\' ? \'premiumSummary\' : \'beforeyoubegin\'}}" class="color-blue">{{item["Action"]}}</a>\n                </div>--></li><li ng-if="vm.pageContent.CPP.CPPFullName !=\'\'"><span class=color-black>{{vm.pageContent.CPP.CPPFullName}}</span> <span ng-class="{ \'hidecell\' : vm.pageContent.CPP.CPPPolicyId == \'\'}">{{vm.pageContent.CPP.CPPPolicyId}}</span> <span ng-class="{ \'hidecell\' : vm.pageContent.CPP.CPPPolicyId == \'\'}">{{vm.pageContent.CPP.CPPIsEligible}}</span> <span ng-class="{ \'hidecell\' : vm.pageContent.CPP.CPPPolicyId == \'\'}">{{vm.pageContent.CPP.CPPStatus}}</span> <span ng-class="{ \'hidecell\' : vm.pageContent.CPP.CPPPolicyId == \'\'}">&nbsp;</span> <span ng-class="{ \'hidecell\' : vm.pageContent.CPP.CPPPolicyId == \'\'}">{{vm.pageContent.CPP.CPPCurrentPremium}}</span> <span><a style="color:#0078a8 !important" href="/DSCIQuote/outbound?landingPageInCE=Select_Line_of_Business&lob=CarrierBusinessOwners">{{vm.pageContent.CPP.CPPAction.toString().replace(\'Continue\', \'Resume quote\')}}</a> </span><!--<div>\n                  <a data-ng-href="#!/{{item[\'Action\'] === \'View Policy\' ? \'premiumSummary\' : \'beforeyoubegin\'}}" class="color-blue">{{item["Action"]}}</a>\n                </div>--></li><li ng-if="vm.pageContent.WC.WCFullName !=\'\'"><span class=color-black>{{vm.pageContent.WC.WCFullName}}</span> <span ng-class="{ \'hidecell\' : vm.pageContent.WC.WCPolicyId == \'\'}">{{vm.pageContent.WC.WCPolicyId}}</span> <span ng-class="{ \'hidecell\' : vm.pageContent.WC.WCPolicyId == \'\'}">{{vm.pageContent.WC.WCIsEligible}}</span> <span ng-class="{ \'hidecell\' : vm.pageContent.WC.WCPolicyId == \'\'}">{{vm.pageContent.WC.WCStatus}}</span> <span ng-class="{ \'hidecell\' : vm.pageContent.WC.WCPolicyId == \'\'}">&nbsp;</span> <span ng-class="{ \'hidecell\' : vm.pageContent.WC.WCPolicyId == \'\'}">{{vm.pageContent.WC.WCCurrentPremium}}</span> <span><a style="color:#0078a8 !important" href="/DSCIQuote/outbound?landingPageInCE=Select_Line_of_Business&lob=CarrierBusinessOwners">{{vm.pageContent.WC.WCAction.toString().replace(\'Continue\', \'Resume quote\')}}</a> </span><!--<div>\n                  <a data-ng-href="#!/{{item[\'Action\'] === \'View Policy\' ? \'premiumSummary\' : \'beforeyoubegin\'}}" class="color-blue">{{item["Action"]}}</a>\n                </div>--></li><li ng-if="vm.pageContent.MAAuto.MAAutoFullName !=\'\'"><span class=color-black>{{vm.pageContent.MAAuto.MAAutoFullName}}</span> <span ng-class="{ \'hidecell\' : vm.pageContent.MAAuto.MAAutoPolicyId == \'\'}">{{vm.pageContent.MAAuto.MAAutoPolicyId}}</span> <span ng-class="{ \'hidecell\' : vm.pageContent.MAAuto.MAAutoPolicyId == \'\'}">{{vm.pageContent.MAAuto.MAAutoIsEligible}}</span> <span ng-class="{ \'hidecell\' : vm.pageContent.MAAuto.MAAutoPolicyId == \'\'}">{{vm.pageContent.MAAuto.MAAutoStatus}}</span> <span ng-class="{ \'hidecell\' : vm.pageContent.MAAuto.MAAutoPolicyId == \'\'}">&nbsp;</span> <span ng-class="{ \'hidecell\' : vm.pageContent.MAAuto.MAAutoPolicyId == \'\'}">{{vm.pageContent.MAAuto.MAAutoCurrentPremium}}</span> <span><a style="color:#0078a8 !important" href="/DSCIQuote/outbound?landingPageInCE=Select_Line_of_Business&lob=CarrierBusinessOwners">{{vm.pageContent.MAAuto.MAAutoAction.toString().replace(\'Continue\', \'Resume quote\')}}</a> </span><!--<div>\n                  <a data-ng-href="#!/{{item[\'Action\'] === \'View Policy\' ? \'premiumSummary\' : \'beforeyoubegin\'}}" class="color-blue">{{item["Action"]}}</a>\n                </div>--></li></ul><!--end table-items--></div><!--end .right--><footer><p>Copyright \xA9 2018 Utica Mutual Insurance Company, New Hartford, NY 13413 | All Rights Reserved</p></footer></div><!--end .main--></div><!--end .account-detail-content--></div><!-- end .contents --></div></div>');
$templateCache.put('app/views/details/nav.details.html','<div data-ng-include="\'templates/header.html\'"></div><!-- end .header --><div class=account-details-wrapper><div class=contents><div class=account-detail-content><div class="top clearfix"><span class=left>{{accountItem.accountName}}</span> <span class=right>Account #: {{accountItem.accountID}}</span></div><!--end .top--><div class="main clearfix"><div class=left><div class=title>Business Details</div><ul class=items><li><span>ACCOUNT ID</span> <input type=text data-ng-model=accountItem.accountID readonly data-ng-change=changeForm()></li><li><span>BUSINESS NAME</span> <input type=text data-ng-model=accountItem.accountName data-ng-change=changeForm()></li><li><span>BUSINESS ADDRESS</span> <textarea data-ng-model=accountItem.address data-ng-change=changeForm()></textarea></li><li><span>BUSINESS CLASS</span> <input type=text data-ng-model=accountItem.details.businessClass data-ng-change=changeForm()></li><li><span>PRIMARY RISK STATE</span> <input type=text data-ng-model=accountItem.details.primaryRiskState data-ng-change=changeForm()></li></ul><!--end .items--></div><!--end .left--><div class="right clearfix pb82" data-ng-if="accountItem.details.tableDataTop.length === 0 && accountItem.details.tableDataBottom.length === 0"><a href=javascript:; class=btn-blue>Resume Application</a><div class=right-main><div class=title>Lines of Business</div><p>More Information is needed to determine the eligible lines of business.</p></div></div><!--end .right--><div class="right filled-table clearfix" data-ng-if="accountItem.details.tableDataTop.length > 0 || accountItem.details.tableDataBottom.length > 0"><a href=javascript:; class=btn-blue>Resume Application</a><div class=right-main><div class=title>Lines of Business</div></div><div class="table-thead clearfix"><span>Line of Business</span> <span>Quote ID</span> <span>Status</span> <span>Effective Date</span> <span>Last Opened</span> <span>Premium</span> <span>Action</span></div><!--end .table-thead--><ul class=table-items><li data-ng-repeat="item in accountItem.details.tableDataTop"><span class=color-black>{{item["Line of Business"]}}</span> <span>{{item["Quote ID"]}}</span> <span>{{item["Status"]}}</span> <span>{{item["Effective Date"]}}</span> <span>{{item["Last Opened"]}}</span> <span>{{item["Premium"]}}</span><div><a data-ng-href="#!/{{item[\'Action\'] === \'View Policy\' ? \'premiumSummary\' : \'submitAndIssue\'}}" class=color-blue>{{item["Action"]}}</a></div></li><li data-ng-if="accountItem.details.tableDataBottom.length > 0"><span class=width-100>The following lines of business are quoted in the Worker\'s Compensation interface. We will direct you to contact to complete.</span></li><li data-ng-repeat="item in accountItem.details.tableDataBottom"><span class=color-black>{{item["Line of Business"]}}</span> <span data-ng-class="{\'width-26\': item[\'Quote ID\'] === \'Eligible - no quotes started yet\'}">{{item["Quote ID"]}} </span><span data-ng-class="{\'hide\': item[\'Quote ID\'] === \'Eligible - no quotes started yet\'}">{{item["Status"]}}</span> <span>{{item["Effective Date"]}}</span> <span>{{item["Last Opened"]}}</span> <span>{{item["Premium"]}}</span><div><a data-ng-href="#!/{{item[\'Action\'] === \'View Policy\' ? \'premiumSummary\' : \'submitAndIssue\'}}" class=color-blue>{{item["Action"]}}</a></div></li></ul><!--end table-items--></div><!--end .right--></div><!--end .main--></div><!--end .account-detail-content--></div><!-- end .contents --></div><!-- end .account-details-wrapper --><div style="position: absolute;bottom:0px;color: #909090;padding-left: 5px;">Copyright \xA9 2018 Utica Mutual Insurance Company, New Hartford, NY 13413 | All Rights Reserved</div>');
$templateCache.put('app/views/beforeyouissue/beforeyouissue.html','<!-- modalwindow script for Issuepolicy--><script type=text/ng-template id=issuepolicy.html><div class="row" style="background-color:#171d26;">\n    <div class="modal-body" >\n      <div class="row" style="background-color:#fff;margin-right:10px;margin-left:10px;border-radius:5px;padding-left:20px;">\n        <h2>Billing information</h2>\n        <h6 style="margin-top:-15px;">In order to complete policy issuance please click below and complete billing information in CommercialEdge</h6><br><br>\n        <a href="/DSCIQuote/outbound?landingPageInCE=Issue_Policy_page&lob=CarrierBusinessOwners"><img ng-src="{{$ctrl.imagePath+\'commercial-edge-logo.png\'}}"/></a><br><br>\n      </div>\n    </div>\n    <div style="text-align:right;margin-right:25px;margin-bottom:15px;">\n      <button class="btn btn-blue cancelbtn" ng-click="$ctrl.completepolicy()">Close</button>\n    </div>\n  </div></script><!-- modalwindow script for quoteanotherline--><script type=text/ng-template id=quoteanotherline.html><div class="row" style="background-color:#171d26;">\n    <div class="modal-body" >\n      <div class="row" style="background-color:#fff;margin-right:10px;margin-left:10px;border-radius:5px;padding-left:20px;margin-top:10px;margin-bottom:30px;">\n        <h3><b>Leaving to go to Commercial Edge</b></h3>\n        <h6 style="margin-top:-10px;">Quotes for Worker\'s Compensation and Commercial Auto can be completed in Commercial Edge. Continuing will open Commercial Edge in a new tab.</h6>\n        <div style="text-align:right;margin-right:25px;margin-bottom:15px;">\n          <button style="background:#fff;border:none;padding-right:20px;" ng-click="$ctrl.completepolicy()">Cancel</button>\n          <a class="btn btn-blue cancelbtn" href="/DSCIQuote/outbound?landingPageInCE=My_Dashboard_page&lob=CarrierBusinessOwners">Continue</a>\n        </div>\n      </div>\n    </div>\n  </div></script><div class=beforeyouissue-page><div id=main-wrapper1><!--form 1--><form class=formClass name=beforeYouIssueFormData id=beforeYouIssueFormDataID form-submit-validation="" data-toggle=validator ng-submit=vm.triggerSubmit(beforeYouIssueFormData.$valid,beforeYouIssueFormData) novalidate autocomplete=off><div><h1>Before you issue policy</h1><div class=col-md-8><!-- Start of Select Type Section Row --><!-- <ng-form class="formClass" name="nameInsuredData" id="nameInsuredDataID" form-submit-validation="" data-toggle="validator"\n      ng-submit="vm.savePrimary(nameInsuredData.$valid,nameInsuredData)"  autocomplete="off"> --><div ng-repeat="insuredItemARRAY in vm.addNewInsureditems track by $index" ng-init="sectionIndex = $index"><div id=bname class="card no-margin"><div class=card-block><div class=row><div class="col-md-12 beforeYouIssueDIV"><div class="col-md-8 insuredHeader" ng-if="$index == 0"><p><strong>Insured Information</strong></p></div><div class="col-md-8 insuredHeader" ng-if="$index != 0"><p class=additionalHeader><strong>Additional Insured Information</strong></p><p class=removeClass ng-click=vm.removeNewInsuredFunction(insuredItemARRAY,$index)>Remove</p></div><div class="form-group row ques" ng-class="{ \'has-error\': (beforeYouIssueFormData.submitted &&\n                          beforeYouIssueFormData.{{radioButtonValue.systemRefId+sectionIndex|dotWithUnderscore}}.$invalid &&\n                          beforeYouIssueFormData.{{radioButtonValue.systemRefId+sectionIndex|dotWithUnderscore}}.$touched)||(vm.errPrimary[$index]  && vm.saveClicked[sectionIndex] && insuredItemARRAY[8].DuckId==vm.ducid) }" ng-repeat="radioButtonValue in insuredItemARRAY" ng-if="radioButtonValue.appearUI.toLowerCase() == \'y\' &&\n                                  radioButtonValue.controlName.toLowerCase() == \'radio_button\'"><!-- {{radioButtonValue.systemRefId}} --><div><p class="insuredHeaderSection row" style="font-weight: bold;">{{radioButtonValue.datasetName}}</p></div><span class="col-md-4 checkradio-wrap" ng-repeat="radioButton in radioButtonValue.valueName"><!-- *********{{$parent.$parent.$parent.$index}} --><!-- +++{{$index}} --> <input type=radio name={{radioButtonValue.systemRefId+sectionIndex|dotWithUnderscore}} value={{radioButton}} id={{radioButtonValue.systemRefId|dotWithUnderscore}}_{{insuredItemARRAY[8].DuckId|dotWithUnderscore}}_{{$index}}_{{$parent.$parent.$parent.$index}} ng-required={{radioButtonValue.isRequired}} ng-model=radioButtonValue.userValue ng-change=vm.onRadioBtnChange(insuredItemARRAY) ng-click=vm.validateNameInsured(insuredItemARRAY)><!-- {{radioButtonValue.systemRefId+insuredItemARRAY[8].DuckId}} --> <label for={{radioButtonValue.systemRefId|dotWithUnderscore}}_{{insuredItemARRAY[8].DuckId|dotWithUnderscore}}_{{$index}}_{{$parent.$parent.$parent.$index}}>{{radioButton}}</label> </span><!-- required= {{radioButtonValue.isRequired}}    ++++\n                      {{radioButtonValue.systemRefId}}_{{insuredItemARRAY[8].DuckId|dotWithUnderscore}}------------\n                      touch = {{nameInsuredData.Select_Type_d7fbc10e_32b1_4108_89f9_d4ebd5caf362.$touched}}\n                      invalid = {{nameInsuredData.Select_Type_d7fbc10e_32b1_4108_89f9_d4ebd5caf362.$touched}} --><p ng-show="(beforeYouIssueFormData.submitted &&\n                      beforeYouIssueFormData.{{radioButtonValue.systemRefId+sectionIndex|dotWithUnderscore}}.$invalid &&\n                      beforeYouIssueFormData.{{radioButtonValue.systemRefId+sectionIndex|dotWithUnderscore}}.$touched) ||\n                     (vm.errPrimary[$index]  && vm.saveClicked[sectionIndex] && insuredItemARRAY[8].DuckId==vm.ducid)" class="error err col-md-12 help-block">This field is required</p></div></div><div class=msg>{{vm.showMsg}}</div><div class="col-md-12 nameClass" ng-repeat="nameData in insuredItemARRAY" ng-if="nameData.controlName.toLowerCase()==\'text_box\' && nameData.appearUI.toLowerCase() == \'y\'"><div class=form-group ng-class="{ \'has-error\': (beforeYouIssueFormData.submitted &&\n                      beforeYouIssueFormData.{{nameData.systemRefId|dotWithUnderscore}}.$touched &&\n                      beforeYouIssueFormData.{{nameData.systemRefId|dotWithUnderscore}}.$invalid)||(vm.errPrimary[$index] && vm.saveClicked[sectionIndex] && insuredItemARRAY[8].DuckId==vm.ducid)}"><strong><label for={{nameData.systemRefId|dotWithUnderscore}}>{{nameData.datasetName}}</label> </strong><input type={{nameData.inputType}} name={{nameData.systemRefId|dotWithUnderscore}} id={{nameData.systemRefId|dotWithUnderscore}} class=form-control ng-focus="" ng-model=nameData.userValue ng-required={{nameData.isRequired}} placeholder={{nameData.additionalInfo}} ng-maxlength="{  50:nameData.systemRefId == \'AdditionalOtherInterestInput.UNIG_FirstName\'||\n                                nameData.systemRefId == \'AdditionalOtherInterestInput.UNIG_MiddleName\' ||\n                                nameData.systemRefId == \'AdditionalOtherInterestInput.UNIG_LastName\'}" input-length-limit uservalue={{nameData.userValue}} ng-keydown=vm.validateNameInsured(insuredItemARRAY) ng-change=vm.validateNameInsured(insuredItemARRAY)><p ng-show="((beforeYouIssueFormData.submitted &&\n                            beforeYouIssueFormData.{{nameData.systemRefId|dotWithUnderscore}}.$touched &&\n                            beforeYouIssueFormData.{{nameData.systemRefId|dotWithUnderscore}}.$invalid))|| (vm.errPrimary[$index] && vm.saveClicked[sectionIndex] && insuredItemARRAY[8].DuckId==vm.ducid)" class="help-block error build22">{{nameData.datasetName | camelCase}} is required</p></div></div></div><div class="input-group col-md-4"><button id=saveInsured type=button class="btn btn-block btn-sm btn-primary save-insured" ng-click="vm.savePrimary(insuredItemARRAY, $index, sectionIndex)">Save</button><br></div></div></div><!-- End of Select Type Section Row --></div><!-- </ng-form> --><!-- End of Add a named insured Widget--><!-- Start of Add a named insured Widget--><div class="card addNewInsuredClass"><div class=card-block><h4 class="card-title paddingClass"><button type=button class="expand-roundicon savebtn addNewInsuredTextClass" ng-click=vm.addNewInsuredFunction(); ng-class="{\'disabled\' : vm.addNewInsureditems[vm.addNewInsureditems.length-1][8].status==\'save\'}" ng-disabled="vm.addNewInsureditems[vm.addNewInsureditems.length-1][8].status==\'save\'">Add a named insured</button></h4></div></div><div class=card ng-if="vm.producerdata.length!=0"><div class=col-md-12><div class="alert alertmsg" ng-if=vm.produceralert><p>Our records indicate your agency and producer may not have the proper licensing / appointment for the risk state(s). All proper state licensing and / or appointments are required prior to policy issuance. Please contact our Licensing Department by email at</p><p>HO.Marketing.LicenseControls@uticanational.com</p><p>You may also contact us by phone between 7:30 AM and 5:00 PM EST at 800-274-1914 extensions 2318, 2057 or 2511</p></div><!--for Producer dropdown--><div class="row producercard"><!-- <form class="formClass" name="vm.premiumForm" id="psform" novalidate autocomplete="off" form-submit-validation="" ng-submit="vm.checkproducervalue(vm.producerdata)"> --><div class="row producercarddata" ng-repeat="item in vm.producerdata"><h4 ng-if="item.controlName == \'Header\'">{{item.datasetName}}</h4><div class=col-md-11 ng-if="item.controlName==\'Drop_Down\'" style=padding-left:0px;><select name="{{item.systemRefId | dotWithUnderscore}}" ng-model=item.userValue class="custom_select form-control" ng-disabled="item.editable==\'Y\'" ng-required=true ng-change=vm.selectproductvalue()><option value="Not on list">Not on list</option><option ng-repeat="val in item.valueName track by $index" value={{val}}>{{val}}</option></select><p class="help-block error" ng-show="beforeYouIssueFormData.submitted && beforeYouIssueFormData.{{item.systemRefId| dotWithUnderscore}}.$invalid && beforeYouIssueFormData.{{item.systemRefId | dotWithUnderscore}}.$touched" class="help-block error"><span>{{item.datasetName | camelCase}} is required</span></p></div></div><!-- </form> --></div></div></div><!--Policy , Business , Loss Info and accounts section --><div class="card no-margin" ng-repeat="item in vm.addNewInsuredArray"><div class=card-block><div class=row><div ng-repeat="policyInfo in item track by $index"><div class=insuredHeaderClass ng-if="policyInfo.controlName.toLowerCase()==\'header\'"><p><strong>{{policyInfo.datasetName}}</strong></p></div><div class="checkboxClass checkbox-wrap" ng-if="policyInfo.controlName.toLowerCase() == \'check_box\'"><input type=checkbox id={{$parent.$parent.$index}} name=policyInfo.systemRefId ng-checked={{policyInfo.defaultValue}} ng-model=policyInfo.userValue ng-click="vm.defaultInfo(item, policyInfo.userValue);"> <label for={{$parent.$parent.$index}} class=checkbox-label>{{policyInfo.datasetName}}</label><!-- ng-init="policyInfo.userValue = policyInfo.defaultValue"   --></div><div class="col-md-6 form-group" ng-class="{ \'has-error\': (beforeYouIssueFormData.submitted &&\n                                                                            beforeYouIssueFormData.{{policyInfo.systemRefId}}.$touched &&\n                                                                            beforeYouIssueFormData.{{policyInfo.systemRefId}}.$invalid ||\n                                                                            (vm.lengthOfZip && policyInfo.systemRefId == \'ZIP_CODE\')||\n                                                                             ((vm.showPhoneErrorOnSave == true &&\n                                                                             vm.invalidPhoneFormat[$parent.$parent.$index + $index] == true &&\n                                                                             policyInfo.userValue)&&(policyInfo.systemRefId == \'BUSINESS_PHONE\' ||policyInfo.systemRefId == \'LOSS_CONTACT_PHONE\' ||policyInfo.systemRefId == \'ACCOUNTING_RECORDS_PHONE\'))\n                                                                             )}" ng-if="policyInfo.controlName.toLowerCase() == \'text_box\'"><label>{{policyInfo.datasetName}}</label> <input type={{policyInfo.inputType}} name={{policyInfo.systemRefId}} id={{policyInfo.systemRefId}} class="form-control text-field" ng-model=policyInfo.userValue ng-required={{policyInfo.isRequired}} placeholder="" ng-focus="vm.lengthOfZip = false;vm.submittedbtn=false" ng-disabled="policyInfo.editable.toLowerCase() == \'n\' ? true : false" ng-keypress=vm.checkLength($event,policyInfo.userValue) ng-blur="vm.lengthOfZip = policyInfo.systemRefId == \'ZIP_CODE\' ? vm.checkZipLessThan5(policyInfo.userValue) : false;\n                                              vm.zipCodeLookUP(item, policyInfo.systemRefId, vm.lengthOfZip, policyInfo.userValue);\n                                              vm.emailValidation (policyInfo.systemRefId, policyInfo.userValue);\n                                               vm.phoneLengthErrorMessage(policyInfo.userValue, policyInfo.systemRefId,$parent.$parent.$index,$index);" ng-attr-zip-code="{{ policyInfo.systemRefId === \'ZIP_CODE\' ? true : false }}" ng-attr-format-phone-number="{{ (policyInfo.systemRefId == \'BUSINESS_PHONE\'||policyInfo.systemRefId == \'LOSS_CONTACT_PHONE\' ||policyInfo.systemRefId == \'ACCOUNTING_RECORDS_PHONE\') ? true : false }}" uservalue={{policyInfo.userValue}} input-length-limit ng-maxlength="{  5:policyInfo.systemRefId == \'ZIP_CODE\',\n                                        40:policyInfo.systemRefId == \'APT_SUITE\' ||\n                                        policyInfo.systemRefId == \'STREET_ADDRESS\',\n                                        10:policyInfo.systemRefId == \'BUSINESS_PHONE\'||\n                                        policyInfo.systemRefId == \'LOSS_CONTACT_PHONE\' ||\n                                        policyInfo.systemRefId == \'ACCOUNTING_RECORDS_PHONE\'\n                                      }"><!-- ng-attr-zip-code="{{policyInfo.systemRefId == \'mailingAddress.ZIP\' ? true : false}}"   --><p ng-show="(beforeYouIssueFormData.submitted &&\n                                  beforeYouIssueFormData.{{policyInfo.systemRefId}}.$touched &&\n                                  beforeYouIssueFormData.{{policyInfo.systemRefId}}.$invalid)" class="help-block error build22">{{policyInfo.datasetName | camelCase}} is required</p><p ng-if="vm.lengthOfZip && policyInfo.systemRefId == \'ZIP_CODE\'" ng-hide="(beforeYouIssueFormData.submitted &&\n                                      beforeYouIssueFormData.{{policyInfo.systemRefId | dotWithUnderscore}}.$invalid &&\n                                      beforeYouIssueFormData.{{policyInfo.systemRefId | dotWithUnderscore}}.$touched)" class="help-block error build22">Invalid Zip Code</p><!-- Error Message for incorrect phone format --><p ng-if="(policyInfo.systemRefId  ==  \'BUSINESS_PHONE\' ||\n                                                                                         policyInfo.systemRefId == \'LOSS_CONTACT_PHONE\' ||\n                                                                                         policyInfo.systemRefId == \'ACCOUNTING_RECORDS_PHONE\'  ) &&\n                                                                                      vm.invalidPhoneFormat[$parent.$parent.$index + $index] == true && policyInfo.userValue && vm.submittedbtn" ng-show=vm.showPhoneErrorOnSave class="help-block error build22">This field should be in the format of xxx-xxx-xxxx</p><p ng-if="((vm.busemailErr && policyInfo.systemRefId==\'BUSINESS_EMAIL\')||(vm.accemailErr && policyInfo.systemRefId==\'ACCOUNTING_RECORDS_EMAIL\')||(vm.lossemailErr && policyInfo.systemRefId==\'LOSS_CONTACT_EMAIL\')) && policyInfo.userValue && vm.submittedbtn" class="help-block error build22">This field should be an email address in the format "user@example.com"</p></div></div></div></div></div><!-- Mailing Address Section END --><!-- Loss Control Contact Information START --><!-- Account Recording Ends --></div><div class=col-md-3><div class="summary-quote quo" style="padding:10px 20px;"><div class="quote-close pull-right"><!-- <span class="glyphicon glyphicon-ok-circle" style="font-size: 22px;">\n            </span> --> <img ng-src="{{main.assetPath+\'checkmark.png\'}}"></div><br><br><div><p>Your Quote</p></div><div class=quote-value><p class="price premiumValue">{{vm.premiumValue}}</p><!-- <p class="quote-annually">annually</p> --></div><br><div><p class=coverage-style>POLICY PERIOD</p></div><div><div><p class=period-style>{{vm.beforeYouIssueData.effectiveStartDate|date}} to {{vm.beforeYouIssueData.effectiveEndDate|date}}</p><!-- <p class="quote-annually">annually</p> --></div></div><!-- <div class="quote-duration">\n            <p><b class="quote-coverageperiod">Coverage Period</b></p>\n            <p class="quote-coverage-date">{{vm.period.datasetName}}</p>\n          </div> --></div><div class=summary-buttons><button id=issuePolicy type=submit class="btn btn-success btn-block policy-button">Issue Policy</button><br><a class="btn btn-block quote-btn" href="/DSCIQuote/outbound?landingPageInCE=My_Dashboard_page&lob=CarrierBusinessOwners">Quote Another Line</a><br></div></div></div><!-- End of Container Class --><!-- <div class="col-md-9 ">\n          <div class="col-md-12 ">\n            <div class="card plan-bg ">\n              <div class="input-group col-md-4 " style="width: 20% ">\n                <button type="submit " class="btn btn-lg btn-primary ">Save &amp; Continue</button>\n              </div>\n            </div>\n          </div>\n        </div> --></form></div></div>');
$templateCache.put('app/views/beforeyouissue/nav.beforeyouissue.html','<span class=summary-leftnav><br><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.businessdetails>Business Details</a></span> <span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.lob>Select lines of business</a></span> <span class=summary-leftnav class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.qualification>Qualification Questions</a></span><hr><h4 class=eligibility-panels>Business Owners Policy</h4><span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.quote>Start your quote</a></span> <span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.location>Locations and Buildings</a></span> <span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.additionalinterest>Additional Interests</a></span> <span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.underwriting>Underwriting Questions</a></span> <span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.pricingandcoverage>Pricing and Coverage</a></span> <span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.premiumsummary>Premium Summary</a></span><ul class="nav list-group eligibility-panel"><li><a class="list-group-item active" href=""><i class="icon-home icon-1x"></i>Before you issue policy</a></li></ul><div style="position: relative;bottom:10px;color: #909090;padding-left: 5px;">Copyright \xA9 2018 Utica Mutual Insurance Company, New Hartford, NY 13413 | All Rights Reserved</div>');
$templateCache.put('app/views/interest/additionalinterest.html','<div class="right-contents additional-interest-page"><div class=row><div class="col col-md-12"><h2 class=ai-heading>Additional Interests</h2></div></div><div><div id=bnames ng-show=vm.addInterestEditable class="card interest-card"><div class=card-block><div class="row editing-AI" ng-repeat="(parentIndex, items) in vm.editableAI track by $index"><div class="col-md-12 additional-interest-page add-aditional-interest" ng-repeat="(key, item) in items track by $index" ng-if="[key] == \'0\'"><div class=col-md-10>{{item.correspondingEntTyp}} - {{item.userValue}}</div><div class=col-md-1><span class=edit-delete-btn ng-click=vm.editAI(items,item.uuid)>Edit</span></div><div class=col-md-1><span class=edit-delete-btn ng-click=vm.deleteAI(items,parentIndex,item.uuid)>Delete</span></div></div></div></div></div></div><div id=bname class="card interest-card"><div class=card-block><b class=add-aditional-interest>To add additional interest please select from the list below</b><br><br><div class=AI-widgets ng-repeat="(key, addInt) in vm.additionalinterestsection track by $index"><div class="additional-container additional-interest-page checkbox-wrap" ng-hide="vm.addInterestEditable && [key] == \'0\'"><input type=checkbox id={{$index}} name=addInt.systemRefId ng-model=addInt.displaySequenc ng-change="vm.addInterest(addInt.datasetName, addInt.systemRefId)" ng-disabled="[key] == \'0\' && vm.interestselection" ng-checked="addInt.displaySequence == 0 && vm.selectedInterest.length <= 0 || addInt.displaySequence == 0 && vm.selectedInterest.length <= 0 && vm.editableAI.length <= 0 "> <label for={{$index}} class="additional-interest-page checkbox-label">{{addInt.datasetName}}</label></div></div></div></div><form class=formClass name=vm.additionalinterestForm id=additionalinterestFormID form-submit-validation="" data-toggle=validator ng-submit=vm.addinterestSubmit(vm.additionalinterestForm,vm.selectedInterest) novalidate autocomplete=off><div id=bname class="card interest-card" ng-repeat="(parentIndex,items) in vm.selectedInterest track by $index" ng-init="v1=$index" ng-show=vm.addInterestSection><div class=card-block><b>{{items[0].correspondingEntTyp}}</b><br><br><div ng-repeat="(index,item) in items track by $index" ng-init="v2=$index"><div class=col-md-12 ng-if="item.controlName == \'Text_Box\' && item.datasetName == \'NAME OF THE PERSON OR ORGANISATION\'"><div class=form-group ng-class="{ \'has-error\' : vm.additionalinterestForm.{{item.systemRefId+v1+v2}}.$invalid && vm.additionalinterestForm.{{item.systemRefId+v1+v2}}.$touched }"><label>{{item.datasetName}}</label> <input type={{item.inputType}} name={{item.systemRefId+v1+v2}} ng-model=item.userValue placeholder={{item.additionalInfo}} class=form-control ng-required=item.isRequired ng-keypress=vm.lengthValidation(item.systemRefId,item.userValue,$event,item.validationName) input-char-only systemrefid={{item.systemRefId}} id={{item.validationLength}}><p ng-show="vm.additionalinterestForm.{{item.systemRefId+v1+v2}}.$invalid && vm.additionalinterestForm.{{item.systemRefId+v1+v2}}.$touched" class="help-block error">{{item.datasetName | camelCase}} is required</p></div></div><div class=col-md-{{item.screenColumns}} ng-if="item.controlName==\'Text_Box\' && item.datasetName !== \'NAME OF THE PERSON OR ORGANISATION\' && item.systemRefId != \'ZIPCODE\'"><div class=form-group ng-class="{ \'has-error\' : vm.additionalinterestForm.{{item.systemRefId+v1+v2}}.$invalid && vm.additionalinterestForm.{{item.systemRefId+v1+v2}}.$touched }"><label>{{item.datasetName}}</label> <input type={{item.inputType}} name={{item.systemRefId+v1+v2}} ng-model=item.userValue placeholder={{item.additionalInfo}} class=form-control ng-required=item.isRequired ng-attr-zip-code="{{ item.datasetName === \'ZIP CODE\' ? true : false }}" paste-alpha-numeric-symbol="{\'{{item.validationLength}}\':\'{{item.systemRefId}}\'}" ng-keypress=vm.lengthValidation(item.systemRefId,item.userValue,$event,item.validationName) ng-disabled="item.systemRefId==\'STATE\'?true:false" input-char-only systemrefid={{item.systemRefId}} id={{item.validationLength}}><p ng-show="vm.additionalinterestForm.{{item.systemRefId+v1+v2}}.$invalid && vm.additionalinterestForm.{{item.systemRefId+v1+v2}}.$touched" class="help-block error">{{item.datasetName | camelCase}} is required</p></div></div><div class=col-md-{{item.screenColumns}} ng-if="item.controlName==\'Text_Box\' && item.datasetName !== \'NAME OF THE PERSON OR ORGANISATION\' && item.systemRefId==\'ZIPCODE\'"><div class=form-group ng-class="{ \'has-error\' : vm.additionalinterestForm.{{item.systemRefId+v1+v2}}.$invalid && vm.additionalinterestForm.{{item.systemRefId+v1+v2}}.$touched }"><label>{{item.datasetName}}</label> <input type={{item.inputType}} name={{item.systemRefId+v1+v2}} ng-model=item.userValue ng-pattern=/^[0-9]{5}$/ placeholder={{item.additionalInfo}} class=form-control ng-required=item.isRequired ng-attr-zip-code="{{ item.datasetName === \'ZIP CODE\' ? true : false }}" uservalue=item.userValue ng-keypress=vm.lengthValidation(item.systemRefId,item.userValue,$event,item.validationName) ng-blur=vm.ziplengthmin($event,item.userValue,item.systemRefId);vm.zipcodelookup(item.userValue,items,item.systemRefId) input-char-only systemrefid={{item.systemRefId}} id={{item.validationLength}}><p ng-show="vm.additionalinterestForm.{{item.systemRefId+v1+v2}}.$error.required && vm.additionalinterestForm.{{item.systemRefId+v1+v2}}.$touched" class="help-block error">{{item.datasetName | camelCase}} is required</p><p ng-show="vm.additionalinterestForm.{{item.systemRefId+v1+v2}}.$error.pattern && vm.additionalinterestForm.{{item.systemRefId+v1+v2}}.$touched && (item.systemRefId==\'ZIPCODE\')" class="help-block error">Invalid Zip Code</p></div></div><div class=col-md-{{item.screenColumns}} ng-if="item.controlName==\'Drop_Down\'"><div class=form-group ng-class="{ \'has-error\' : vm.additionalinterestForm.{{item.systemRefId+v1+v2}}.$invalid && vm.additionalinterestForm.{{item.systemRefId+v1+v2}}.$touched }"><label>{{item.datasetName}}</label> <select style="padding-right: 35px;" name={{item.systemRefId+v1+v2}} ng-model=item.userValue class="custom_select form-control" placeholder=teritory ng-required=item.isRequired ng-init="item.userValue = (item.defaultValue != null)?item.defaultValue:item.userValue" ng-change="vm.selectedLocation(item.userValue, index, parentIndex, item.uuid, item.datasetName)"><option value="">Select a {{item.datasetName | camelCase}}</option><option ng-repeat="(SubIndex, val) in item.valueName track by $index" value={{val}}>{{val}}</option></select><p class="help-block error" ng-show="vm.additionalinterestForm.{{item.systemRefId+v1+v2}}.$invalid && vm.additionalinterestForm.{{item.systemRefId+v1+v2}}.$touched" class="help-block error"><span ng-show="item.datasetName !== \'LOCATION\' && item.datasetName !== \'BUILDING\'">{{item.datasetName | camelCase}} is required </span><span ng-show="item.datasetName === \'LOCATION\'">To assign a building ,choose a location</span></p></div><p style="font-size: 11px;" class="help-block error" ng-show="item.datasetName === \'BUILDING\' && item.duplicate">A building may only be assigned to an Additional Interest once</p><!-- <span class="location-added" ng-show="vm.addLocationInfo && item.datasetName === \'BUILDING\' && $index + 2 > items.length"> --> <span class=location-added ng-show="vm.addLocationInfo && item.datasetName === \'BUILDING\' && $index + 3 > items.length"><img src="{{main.assetPath+\'plus.png\'}}"> <label ng-click="vm.addLocation(items, parentIndex)">Add Location</label> </span><span class=cancel-building ng-click="vm.cancelAddedLocation(index, parentIndex)" ng-show="item.datasetName === \'BUILDING\' && item.AICount > 0">Cancel</span></div><div class=clearfix ng-if=item.hasRowBreak></div></div><!-- <div class="col-md-12 location-added" ng-show="vm.addLocationInfo">\n             <img src="{{main.assetPath+\'plus.png\'}}" />\n              <label ng-click="vm.addLocation(items, parentIndex)">Add Location </label>\n            </div> --></div></div><div class=col-md-12 style="padding-left: 0px;"><div class="card plan-bg" style="margin-left: 0px;"><div class="input-group col-md-4" style="width: 20%;margin-left: 0px;"><button type=submit class="btn btn-primary" style=opacity:1>Save &amp; Continue</button></div></div></div></form></div>');
$templateCache.put('app/views/interest/nav.additionalinterest.html','<div style="min-height: 460px;"><span class=summary-leftnav><br><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.businessdetails>Business Details</a></span> <span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.lob>Select lines of business</a></span> <span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.qualification>Qualification Questions</a></span><hr><h4 class=eligibility-panels>Business Owners Policy</h4><span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.quote>Start your quote</a></span> <span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.location>Locations and Buildings</a></span><ul class="nav list-group eligibility-panel"><li><a class="list-group-item active" href=""><i class="icon-home icon-1x"></i>Additional Interests</a></li><li><a class="list-group-item locked" href=""><i class="icon-home icon-1x"></i>Underwriting Questions</a></li><li><a class="list-group-item locked" href=""><i class="icon-home icon-1x"></i>Pricing & Coverages</a></li></ul></div><div style="position: relative;bottom:0px;color: #909090;padding-left: 5px;">Copyright \xA9 2018 Utica Mutual Insurance Company, New Hartford, NY 13413 | All Rights Reserved</div>');
$templateCache.put('app/views/location/edit-location.html','<form name=vm.locationForm data-toggle=validator class=formClass form-submit-validation="" autocomplete=off novalidate><div class=card-block ng-repeat="currLocItem in vm.explocData track by $index"><!-- location name --><div class=form-group><h4>Location {{vm.locID}}</h4><!-- info --><h6 class="card-subtitle mb-2 text-muted">All addresses must be in the United States of America</h6></div><div class=row><div ng-repeat="item in currLocItem.LocationAddress track by $index"><!-- text box widget --><div class=col-md-{{item.screenColumns}} ng-if="item.controlName==\'Text_Box\'"><div class=form-group ng-class="{ \n            \'has-error\' : vm.locationForm.{{item.systemRefId | dotWithUnderscore}}.$invalid && vm.locationForm.{{item.systemRefId | dotWithUnderscore}}.$touched ||\n            ((item.systemRefId == \'LocationInput.ZipCode\') &&\n            (vm.lengthOfZip)) ||(item.systemRefId == \'LocationInput.ZipCode\' && vm.stateErrMsg != null) }"><!-- label --> <label>{{item.datasetName}}</label><!-- control --> <input type=text name="{{item.systemRefId | dotWithUnderscore}}" ng-model=item.userValue placeholder={{item.additionalInfo}} class=form-control ng-maxlength="{  5:item.systemRefId == \'LocationInput.ZipCode\',\n            40:item.systemRefId == \'LocationInput.Address1\' ||\n            item.systemRefId == \'LocationInput.Address2\',\n            30:item.systemRefId == \'LocationInput.City\'}" input-length-limit uservalue={{item.userValue}} options=vm.streetAutocompleteOptions ng-disabled=item.nonEditableFeild ng-required=item.isRequired ng-attr-zip-code="{{ item.systemRefId == \'LocationInput.ZipCode\' ? true : false }}" ng-focus="vm.pasteData = false;vm.locationSerChecking = false;vm.locationSerChecking1 = false;" ng-keypress="vm.pasteData = false;vm.locationSerChecking = false;vm.locationSerChecking1 = false;" ng-change=" item.systemRefId == \'LocationInput.State\' ? \n             vm.stateValidation(item.userValue,item.systemRefId,item.eventName,vm.locID):\'\';\n             item.systemRefId == \'LocationInput.Address1\' ||\n             item.systemRefId == \'LocationInput.Address2\' ||\n              item.systemRefId == \'LocationInput.City\' ||\n               item.systemRefId == \'LocationInput.ZipCode\' ||\n                item.systemRefId == \'LocationInput.State\' ? vm.locationCallSystemRefId(currLocItem.LocationAddress,currLocItem.locationId):\'\';\n                vm.updateLoc(item,item.userValue,item.systemRefId,currLocItem,vm.locID);" pasteboolean={{vm.pasteData}} ng-blur="\n             vm.lengthOfZip = item.systemRefId == \'LocationInput.ZipCode\' ?\n             vm.ziplengthmin($event,item.userValue,item.systemRefId) : false;\n             vm.zipCodeLookUP(item.systemRefId,item.eventName,vm.lengthOfZip,item.userValue,vm.locID);\n           \n             item.systemRefId == \'LocationInput.Address1\' ||\n             item.systemRefId == \'LocationInput.Address2\' ||\n             item.systemRefId == \'LocationInput.City\' ? vm.locationSeviceCallBlur(locItem.LocationAddress,currLocItem.locationId[0],vm.explocData,vm.locationForm,vm.eventMode,vm.locID):\'\'; " ng-attr-zip-code="{{ item.systemRefId == \'LocationInput.ZipCode\' ? true : false }}" restrict-character-input paste-number-only="{\n               5:{{item.systemRefId == \'LocationInput.ZipCode\'}}\n                  }" paste-alpha-numeric-symbol="{\n                       40:{{item.systemRefId == \'LocationInput.Address1\' ||\n                          item.systemRefId == \'LocationInput.Address2\'}},\n                       30:{{item.systemRefId == \'LocationInput.City\'}},\n                     }" systemrefid={{item.systemRefId}} g-places-autocomplete><!-- message --><p ng-show="vm.locationForm.{{item.systemRefId | dotWithUnderscore}}.$invalid && vm.locationForm.{{item.systemRefId | dotWithUnderscore}}.$touched" class="help-block error">{{item.datasetName | camelCase}} is required</p><p ng-if="(item.systemRefId == \'LocationInput.ZipCode\')&&\n            (vm.lengthOfZip)" class="help-block error">Invalid Zip Code</p><p ng-if="item.systemRefId== \'LocationInput.ZipCode\' && vm.stateErrMsg != null && item.userValue" class="help-block error build22">{{vm.stateErrMsg}}</p></div></div><!-- dropdown widget --><div class=col-md-{{item.screenColumns}} ng-if="item.controlName==\'Drop_Down\'"><div class=form-group ng-class="{ \'has-error\' : vm.locationForm.{{item.systemRefId | dotWithUnderscore}}.$invalid && vm.locationForm.{{item.systemRefId | dotWithUnderscore}}.$touched }"><!-- label --> <label>{{item.datasetName}}</label><!-- select control --> <select name="{{item.systemRefId | dotWithUnderscore}}" ng-model=item.userValue class="custom_select form-control" ng-required=item.isRequired ng-focus="vm.protErrMsg = null;" ng-change="item.userValue != null && item.systemRefId == \'LocationBusinessOwnersInput.UNIG_Territory\' ? vm.protectionClassLookup(item.systemRefId,item.eventName,item.userValue,currLocItem.LocationAddress,currLocItem.locationId,vm.locID):\'\';"><option value="" selected>Select a {{item.datasetName | camelCase}}</option><option ng-repeat="items in item.valueName track by $index" value={{items.value}}>{{items.caption}}</option></select><!-- message --><p class="help-block error" ng-show="vm.locationForm.{{item.systemRefId | dotWithUnderscore}}.$invalid && vm.locationForm.{{item.systemRefId | dotWithUnderscore}}.$touched" class="help-block error">{{item.datasetName}} is required</p><p ng-if="item.systemRefId==\'LocationBusinessOwnersInput.ProtectionClass\' && vm.protErrMsg != null && item.userValue" ng-show=vm.showState class="help-block error build22">{{vm.protErrMsg}}</p></div></div><div class=clearfix ng-if=item.hasRowBreak></div></div><!-- buttons --><div style=text-align:right><button type=button ng-click=vm.locationCancel(currLocItem,vm.locID,vm.eventMode) class="btn btn-sm btn-default buttonloc">Cancel</button> <button type=submit ng-mousedown="vm.appianBefSaveLoc1 = true" ng-click=vm.saveLocation(currLocItem.locationId[0],vm.explocData,vm.locationForm,vm.eventMode,vm.locID) class="btn btn-sm btn-primary buttonloc1">Save</button></div><span class=clearfix></span></div></div></form>');
$templateCache.put('app/views/location/location.html','<div class=location-page><!-- heading --><div class=col-md-12><div class=col-md-6><h1>Locations and Buildings</h1></div></div><!-- end heading --><!-- ****************************************************************LOCATION form page ***********************************************************--><div ng-hide=vm.defaultLocPage><div class=col-md-9><!-- card --><div style="margin-left: 32px;" class="locCard alert alert-danger" ng-show=vm.locservicerror>Geocoding is currently unavailable</div><div class=card><!-- location form --><form name=vm.defaultlocationForm data-toggle=validator class=formClass form-submit-validation="" autocomplete=off novalidate><div class=card-block ng-repeat="locItem in vm.explocData track by $index" ng-init="outerFirstIndex=$index"><!-- location name --><div class=form-group><h4>Location 1</h4></div><div class=row><!-- form control widgets --><div ng-repeat="item in locItem.LocationAddress track by $index" ng-init="innerFirstIndex=$index"><!-- text box --><div class=col-md-{{item.screenColumns}} ng-if="item.controlName==\'Text_Box\'"><div class=form-group ng-class="{ \'has-error\' :\n                   vm.defaultlocationForm.{{item.systemRefId | dotWithUnderscore}}.$invalid && vm.defaultlocationForm.{{item.systemRefId | dotWithUnderscore}}.$touched ||\n                   ((item.systemRefId == \'LocationInput.ZipCode\')&&\n                   (vm.lengthOfZip)) || (item.systemRefId== \'LocationInput.ZipCode\' && vm.stateErrMsg != null) }"><!-- label --> <label>{{item.datasetName}}</label><!-- input control\n                    {{item.systemRefId}}--> <input type=text name="{{item.systemRefId  | dotWithUnderscore}}" ng-model=item.userValue placeholder={{item.additionalInfo}} class=form-control ng-disabled=item.nonEditableFeild ng-required=item.isRequired ng-maxlength="{  5:item.systemRefId == \'LocationInput.ZipCode\',\n                         40:item.systemRefId == \'LocationInput.Address1\' ||\n                         item.systemRefId == \'LocationInput.Address2\',\n                         30:item.systemRefId == \'LocationInput.City\' }" ng-paste="vm.pasteData = true" input-length-limit options=vm.streetAutocompleteOptions uservalue={{item.userValue}} ng-change=" item.systemRefId == \'LocationInput.State\' ?\n                      vm.stateValidation(item.userValue,item.systemRefId,item.eventName,outerFirstIndex+1):\'\';\n                      item.systemRefId == \'LocationInput.Address1\' ||\n                      item.systemRefId == \'LocationInput.Address2\' ||\n                       item.systemRefId == \'LocationInput.City\' ||\n                        item.systemRefId == \'LocationInput.ZipCode\' ||\n                         item.systemRefId == \'LocationInput.State\' ? vm.locationCallSystemRefId(locItem.LocationAddress,locItem.locationId):\'\';\n                         vm.updateLoc(item,item.userValue,item.systemRefId,locItem,outerFirstIndex+1);" ng-focus="vm.pasteData = false;vm.locationSerChecking = false;vm.locationSerChecking1 = false;" ng-keypress="vm.pasteData = false;vm.locationSerChecking = false; vm.locationSerChecking1 = false;" pasteboolean={{vm.pasteData}} ng-blur="\n                         vm.lengthOfZip = item.systemRefId == \'LocationInput.ZipCode\' ?\n                         vm.ziplengthmin($event,item.userValue,item.systemRefId) : false;\n                         vm.zipCodeLookUP(item.systemRefId,item.eventName,vm.lengthOfZip,item.userValue,outerFirstIndex+1);\n                         item.systemRefId == \'LocationInput.Address1\' ||\n                         item.systemRefId == \'LocationInput.Address2\' ||\n                         item.systemRefId == \'LocationInput.City\' ? vm.locationSeviceCallBlur(locItem.LocationAddress,locItem.locationId,vm.explocData,vm.defaultlocationForm,null,outerFirstIndex+1):\'\'; " ng-attr-zip-code="{{ item.systemRefId == \'LocationInput.ZipCode\' ? true : false }}" paste-number-only="{\n                        5:{{item.systemRefId == \'LocationInput.ZipCode\'}}\n                           }" paste-alpha-numeric-symbol="{\n                                40:{{item.systemRefId == \'LocationInput.Address1\' ||\n                                   item.systemRefId == \'LocationInput.Address2\'}},\n                                30:{{item.systemRefId == \'LocationInput.City\'}},\n                              }" systemrefid={{item.systemRefId}} g-places-autocomplete><!-- error message --><p ng-show="vm.defaultlocationForm.{{item.systemRefId  | dotWithUnderscore }}.$invalid && vm.defaultlocationForm.{{item.systemRefId  | dotWithUnderscore}}.$touched" class="help-block error">{{item.datasetName | camelCase}} is required</p><!-- error message --><p ng-if="(item.systemRefId == \'LocationInput.ZipCode\')&&\n                     (vm.lengthOfZip)" class="help-block error">Invalid Zip Code</p><p ng-if="item.systemRefId==\'LocationInput.ZipCode\' && vm.stateErrMsg != null && item.userValue" class="help-block error build22">{{vm.stateErrMsg}}</p></div></div><!-- end text box --><!-- dropdown --><div class=col-md-{{item.screenColumns}} ng-if="item.controlName==\'Drop_Down\'"><div class=form-group ng-class="{ \'has-error\' : vm.defaultlocationForm.{{item.systemRefId| dotWithUnderscore}}.$invalid && vm.defaultlocationForm.{{item.systemRefId | dotWithUnderscore}}.$touched }"><!-- label --> <label>{{item.datasetName}}</label><!-- select control --> <select name="{{item.systemRefId | dotWithUnderscore }}" ng-model=item.userValue class="custom_select form-control" ng-required=item.isRequired ng-focus="vm.protErrMsg = null;" ng-change="item.userValue != null && item.systemRefId == \'LocationBusinessOwnersInput.UNIG_Territory\' ? vm.protectionClassLookup(item.systemRefId,item.eventName,item.userValue,locItem.LocationAddress,locItem.locationId,outerFirstIndex+1):\'\';"><option value="">Select a {{item.datasetName | camelCase}}</option><option ng-repeat="items in item.valueName track by $index" value={{items.value}}>{{items.caption}}</option></select><!-- error message --><p class="help-block error" ng-show="vm.defaultlocationForm.{{item.systemRefId | dotWithUnderscore}}.$invalid && vm.defaultlocationForm.{{item.systemRefId | dotWithUnderscore}}.$touched" class="help-block error">{{item.datasetName | camelCase}} is required</p><p ng-if="item.systemRefId==\'LocationBusinessOwnersInput.ProtectionClass\' && vm.protErrMsg != null && item.userValue" ng-show=vm.showState class="help-block error build22">{{vm.protErrMsg}}</p></div></div><!-- end dropdown --><div class=clearfix ng-if=item.hasRowBreak></div></div><!-- end form control widgets --><!-- save button --><div style=text-align:right><button type=submit class="btn btn-sm btn-primary buttonloc1" ng-mousedown="vm.appianBefSaveLoc = true" ng-click=vm.saveExpandedLocation(locItem.locationId,$index,vm.explocData,vm.defaultlocationForm)>Save</button></div><!-- end button --> <span class=clearfix></span></div></div></form><!-- end location form --></div><!-- end card --></div></div><!-- *******************************************************************End LOCATION form page *******************************************************************--><!-- **********************************************************Summary of ADD Location and Building  page ***********************************************************--><div ng-show=vm.defaultLocPage><div class=col-md-9><div class=col-md-12><div style="margin-left: 32px;" class="locCard alert alert-danger" ng-if="vm.annualSaleCountErr != null">{{vm.annualSaleCountErr}}</div><!-- location list --><div ng-repeat="loc in vm.locData track by $index" ng-init="outerIndex=$index"><div ng-hide=vm.viewMode class=card><div class=card-block><p style=margin-bottom:0></p><div><table><tr class=tablehead><!-- location name --><td style=width:900px><h4>Location {{$index+1}}: {{loc.locationName[0]}}</h4></td><!-- remove Location--><td ng-show="vm.locData.length > 1 && loc.BuildingData.length >=1 " style="width:100px;   text-align:right; font-size:32px"><a href="" ng-click=vm.removeLocation(loc.locationId[0],outerIndex);$event.stopPropagation();><b style=font-size:18px>Remove</b></a></td><!-- edit location control --><td style="width:100px; text-align:right; font-size:32px"><a href="" ng-click=vm.editLocation(loc.locationId[0],outerIndex)><b style=font-size:18px>Edit</b></a></td></tr></table><!-- building list --><table class=building_table><tr ng-repeat="building in loc.BuildingData track by $index" ng-init="innerIndex=$index"><td style=width:420px;padding-left:25px;><h5 style="margin:0 0 0px 0">Building {{$index + 1}}</h5><div ng-repeat="buildingQuestion in building.BuildingQuestions"><!-- {{buildingQuestion.datasetName == \'Building Description\'}} --><!-- <p "buildingQuestion.datasetName == \'Building Description\'">{{ buildingQuestion.datasetName}}</p> --><p style=font-size:12px ng-show="buildingQuestion.systemRefId == \'Building_Description\'">{{buildingQuestion.userValue}}</p></div><!-- <span class="clearfix" ng-show="(vm.Emptybuilding == true && loc.BuildingData.length == 1 && vm.locCheckIndex == outerIndex)||(vm.buildUnderCancel== true && loc.BuildingData.length == 1 && vm.locUnderIndex == outerIndex)">\n                        <span style="margin-right: -55px;" class="error">Building 1 is incomplete. Complete this building to Proceed.</span>\n                      </span> --><!-- occupant list --><table class=occupant_table><tr ng-repeat="occupant in building.occupantData track by $index" ng-init="innerIndex=$index"><td style=width:200px;padding-left:30px;><h5 style="margin:0 0 0px 0">Occupant {{$index + 1}}</h5><div ng-repeat="occupantQuestion in occupant.occupantQuestions"><p style=font-size:12px ng-show="occupantQuestion.systemRefId == \'Occupant_Description\'">{{occupantQuestion.displayValue}}</p></div></td></tr></table><!-- ng-show="vm.Emptybuilding && loc.BuildingData.length == 1" --></td><!-- remove building   --><td valign=top ng-show="loc.BuildingData.length > 1 " style=width:85px;><a href="" ng-click=vm.removeBuilding(loc.locationId[0],building.buildingId,outerIndex,innerIndex);$event.stopPropagation();><b style="font-size:14px; font-weight:normal">Remove</b></a></td><!-- duplicate building --><td valign=top style=width:150px ng-hide="(vm.Emptybuilding == true && loc.BuildingData.length == 1 && vm.locCheckIndex == outerIndex)"><a href="" ng-click=vm.duplicateBuilding(loc.locationId[0],building.buildingId,building,outerIndex,innerIndex)><b style="font-size:14px; font-weight:normal">Duplicate</b></a></td><!-- edit building --><td valign=top style="width:350px; text-align:right"><a href="" ng-click=vm.editBuilding(loc.locationId[0],building.buildingId,loc.locationName,outerIndex,innerIndex)><b style=font-size:18px>Edit</b></a></td></tr></table><p style=margin-bottom:0></p><!-- add building --><div><span class=clearfix><h2><label class=expand-icon2><span ng-class="{locDisabled: (vm.Emptybuilding  && loc.BuildingData.length == 1 && vm.locCheckIndex == outerIndex) }"><a href="" ng-click=vm.addNewBuilding(outerIndex,loc.BuildingData.length,loc.locationId[0],loc.locationName,loc.BuildingData) style="font-size: 18px;"><b>Add a building</b></a></span></label></h2></span></div><!-- ***************************************************END of Locaation summary *************************************--><!-- info --><div><span class=clearfix ng-if="vm.stateSummErrMsg  != null &&  vm.locCheckSumIndex == outerIndex"><span class=error>Primary rating state does not match location state. Please verify.{{outerIndex}}{{vm.locCheckSumIndex}}</span> </span><!-- <span class="clearfix" ng-show="vm.saveDisable && loc.BuildingData.length == 0">\n                    <span class="error">Each location must have at least one building</span>\n                    </span> --></div></div></div></div></div><!-- *************************************************** ADD Location *************************************--><div ng-hide=vm.viewMode class=card><div class=card-block><h4 class=card-title><label><span ng-class="{locDisabled: vm.Emptybuilding}"><a class="loc expand-roundicon" ng-click=vm.addNewLocation() style="font-size: 18px;">Add a location</a></span></label></h4><div class=row><span class=clearfix></span></div></div></div><!-- ***************************************************END of ADD location *************************************--><!-- begin location edit form --><div ng-hide=vm.showLocationForm class=card ng-include="\'app/views/location/edit-location.html\'"></div><!-- end location edit form --><!-- ***************************************************BEGIN BUILDING FORM *************************************--><!-- BEGIN BUILDING FORM --><!-- NEW building FORM --><div class=row ng-show=vm.showBuildingForm><form data-toggle=validator class=formClass form-submit-validation="" name=vm.buildingFormElement autocomplete=off novalidate><!-- build form white panel begin --><div class=white-pannel ng-repeat="building in vm.buildingData track by $index"><div class=form-group style="padding: 1px 2px 3px 0px"><h4>Location {{vm.locID}}: {{building.locationName[0]}}</h4></div><a class="pull-right businessMagicFill businessInfoMagicFill" href="" ng-if=vm.veriskSuggestionBox ng-click="vm.locationMagicFill(building);vm.disableunderSubmit = true;  vm.disableSubmit= false;"><img ng-src="{{main.assetPath+\'magic_button.png\'}}"><p style=padding-left:10%;>Sources: Verisk ISO</p></a><div class=form-group style=padding-right:2px><h4 class=pannel-title>Building {{vm.buildingID+1}}</h4></div><div class=row style="margin-top: -39px;"><!-- BEGIN main loop --><div ng-repeat="item in building.BuildingQuestions"><!--\n                      LIST OF CONTROLS\n                      --><!-- vm.buildingDataUUID -> vm.buildingDataUUID --><!-- description text box--><div class="col-md-{{item.screenColumns}} buildingDESC" ng-if="(item.controlName==\'Placeholder_Text\') && (item.appearUI==\'Y\')"><div><input type=text style=border:none;width:100%; name="{{item.systemRefId | dotWithUnderscore }}" id={{item.systemRefId}} ng-model=vm.buildingDataUUID[item.uuid] ng-disabled=item.nonEditableFeild ng-maxlength="{30:item.systemRefId == \'Building_Description\'}" ng-keypress="vm.pasteData = false;" systemrefid={{item.systemRefId}} ng-focus="vm.pasteData = false;" paste-alpha-numeric-symbol="{\n                          30:{{item.systemRefId == \'Building_Description\'}},\n                        }" input-length-limit uservalue={{vm.buildingDataUUID[item.uuid]}} restrict-character-input placeholder={{item.additionalInfo}}></div></div><!-- end of description text box--><!-- help text --><div class=col-md-{{item.screenColumns}} ng-if="(item.controlName==\'Help_Text\') && (item.appearUI==\'Y\')"><div><p id={{item.systemRefId}} ng-model=vm.buildingDataUUID[item.uuid] ng-disabled=item.nonEditableFeild>{{item.datasetName}}</p></div></div><!-- end of help text --><!-- input box --><div class=col-md-{{item.screenColumns}} ng-if="(item.controlName==\'Text_Box\') && (item.appearUI==\'Y\')"><div class=form-group ng-class="{ \'has-error\': (vm.buildingFormElement.{{item.systemRefId | dotWithUnderscore}}.$touched &&\n                    vm.buildingFormElement.{{item.systemRefId | dotWithUnderscore}}.$invalid) ||\n                                                                      ((item.systemRefId==\'BuildingInput.YearBuilt\' && vm.yearerrmsg != null) && vm.showyear) ||\n                                                                      ((item.systemRefId==\'BuildingInput.UNIG_TotalSquareFeet\' && vm.squareFootageMsg != null) && vm.showyear) ||\n                                                                      ((item.systemRefId==\'OccupancyOutput.BOP_SquareFootage\' && vm.OccSquareFootageMsg != null) && vm.showyear) ||\n                                                                      ((item.systemRefId==\'CovBuildingInput.Limit\' && vm.buildingLimitMsg != null) && vm.showyear) ||\n                                                                      ((item.systemRefId==\'CovPersonalPropertyInput.Limit\' && vm.buildingPLimitMsg != null) && vm.showyear) ||\n                                                                      ((item.systemRefId==\'BuildingInput.NumberOfStories\' && vm.numberofstorieserrmsg != null) && vm.showyear)\n                                                                                                                                            }"><label style=height:35px;>{{item.datasetName}}</label> <input type=text name="{{item.systemRefId | dotWithUnderscore}}" id={{item.systemRefId}} class=form-control ng-model=vm.buildingDataUUID[item.uuid] placeholder={{item.additionalInfo}} ng-init="vm.buildingDataUUID[item.uuid]=(item.defaultValue !=null && item.userValue == null && vm.buildingDataUUID[item.uuid] == null) ?\n                      item.defaultValue : item.userValue" ng-required=item.isRequired ng-keypress="vm.pasteData = false;" ng-maxlength="{\n                      10:item.systemRefId == \'BuildingInput.YearBuilt\'||\n                      item.systemRefId ==\'OccupancyOutput.BOP_SquareFootage\'||\n                      item.systemRefId ==\'BuildingInput.UNIG_TotalSquareFeet\'||\n                      item.systemRefId ==\'CovBuildingInput.Limit\'||\n                      item.systemRefId ==\'CovPersonalPropertyInput.Limit\',\n                      100000000:item.systemRefId == \'BuildingInput.NumberOfStories\'\n                    }" input-length-limit uservalue={{vm.buildingDataUUID[item.uuid]}} ng-focus="vm.showyear === false; vm.disableunderSubmit = true;  vm.disableSubmit= false; vm.pasteData = false;" paste-number-only="{\n                      10:{{item.systemRefId == \'BuildingInput.YearBuilt\' ||\n                      item.systemRefId ==\'OccupancyOutput.BOP_SquareFootage\'||\n                      item.systemRefId ==\'BuildingInput.UNIG_TotalSquareFeet\'||\n                      item.systemRefId ==\'CovBuildingInput.Limit\'||\n                      item.systemRefId ==\'CovPersonalPropertyInput.Limit\'\n                    }}, 100000000:{{item.systemRefId == \'BuildingInput.NumberOfStories\'}} }" systemrefid={{item.systemRefId}} restrict-character-input ng-blur="vm.buildingDataUUID[item.uuid] != null ? vm.buildingDataUUID[item.uuid] = vm.validationTextfields(item.systemRefId,vm.buildingDataUUID[item.uuid],vm.buildingFormElement,vm.buildingData,vm.buildingEventMode):null;\n                                  vm.buildingDataUUID[item.uuid] != null ? vm.validInputfeilds(vm.buildingDataUUID[item.uuid],building.BuildingQuestions,item.eventName,item.systemRefId,vm.buildingFormElement,vm.buildingData,vm.buildingEventMode):\'\';\n                                  vm.buildingDataUUID[item.uuid] != null ? vm.addAditional(item.systemRefId,building.BuildingQuestions,vm.buildingDataUUID[item.uuid],vm.buildingFormElement,vm.buildingData,vm.buildingEventMode):\'\';\n                                  vm.buildingDataUUID[item.uuid] != null ? vm.yearBuiltAddField(vm.buildingDataUUID[item.uuid],building.BuildingQuestions,item.systemRefId,item.eventName,vm.buildingFormElement,vm.buildingData,vm.buildingEventMode):\'\';"><p ng-show="vm.buildingFormElement.{{item.systemRefId | dotWithUnderscore}}.$invalid && vm.buildingFormElement.{{item.systemRefId | dotWithUnderscore}}.$touched" class="help-block error build22">{{item.datasetName | camelCase}} is required</p><p ng-if="item.systemRefId==\'OccupancyOutput.BOP_SquareFootage\' && vm.OccSquareFootageMsg != null && vm.buildingDataUUID[item.uuid]" ng-show=vm.showyear class="help-block error build22">{{vm.OccSquareFootageMsg}}</p><p ng-if="item.systemRefId==\'BuildingInput.UNIG_TotalSquareFeet\' && vm.squareFootageMsg != null && vm.buildingDataUUID[item.uuid].length != 0" ng-show=vm.showyear class="help-block error build22">{{vm.squareFootageMsg}}</p><p ng-if="item.systemRefId==\'CovBuildingInput.Limit\' && vm.buildingLimitMsg != null && vm.buildingDataUUID[item.uuid]" ng-show=vm.showyear class="help-block error build22">{{vm.buildingLimitMsg}}</p><p ng-if="item.systemRefId==\'CovPersonalPropertyInput.Limit\' && vm.buildingPLimitMsg != null && vm.buildingDataUUID[item.uuid]" ng-show=vm.showyear class="help-block error build22">{{vm.buildingPLimitMsg}}</p><p ng-if="item.systemRefId==\'BuildingInput.YearBuilt\' && vm.yearerrmsg != null && vm.buildingDataUUID[item.uuid]" ng-show=vm.showyear class="help-block error build22">{{vm.yearerrmsg}}</p><p ng-if="item.systemRefId==\'BuildingInput.NumberOfStories\' && vm.numberofstorieserrmsg!=null && vm.buildingDataUUID[item.uuid]" ng-show=vm.showyear class="help-block error build22">{{vm.numberofstorieserrmsg}}</p></div></div><!-- end of input box --><!-- inline text box--><div class=col-md-{{item.screenColumns}} ng-if="(item.controlName==\'Inline_Text\') && (item.appearUI==\'Y\')" style=padding-bottom:15px;><div class=form-group ng-class="{ \'has-error\': (vm.buildingFormElement.{{item.systemRefId | dotWithUnderscore}}.$touched && vm.buildingFormElement.{{item.systemRefId | dotWithUnderscore}}.$invalid)\n\n                          ||((item.systemRefId ==\'RiskInput.PermanentlyInstalledEquipmentLimit\' && vm.perielerrmsg != null) && vm.showyear)\n                         \n                           }"><p style=height:35px;padding-left:0px; class=col-md-6>{{item.datasetName}}</p><div class=col-md-6><input type=text data-uuid={{item.uuid}} name="{{item.systemRefId | dotWithUnderscore}}" id={{item.systemRefId}} class=form-control ng-model=vm.buildingDataUUID[item.uuid] placeholder={{item.additionalInfo}} ng-init="vm.buildingDataUUID[item.uuid]=(item.defaultValue !=null && item.userValue == null && vm.buildingDataUUID[item.uuid] == null) ?\n                                  item.defaultValue : vm.buildingDataUUID[item.uuid]" ng-required=item.isRequired ng-maxlength="{10:item.systemRefId == \'RiskInput.PermanentlyInstalledEquipmentLimit\'\n                                                          }" ng-paste="vm.pasteData = true" pasteboolean={{vm.pasteData}} ng-focus="vm.pasteData = false;" input-length-limit uservalue={{vm.buildingDataUUID[item.uuid]}} ng-keypress="vm.pasteData = false;" ng-attr-format-currency-location="{{ item.systemRefId == \'RiskInput.PermanentlyInstalledEquipmentLimit\' ? true : false }}" ng-focus="\n                        vm.disableunderSubmit = true; vm.showyear = false; vm.disableSubmit= false; vm.pasteData = false;" paste-number-only="{\n                          10:{{item.systemRefId == \'RiskInput.PermanentlyInstalledEquipmentLimit\'\n                        }}\n                             }" systemrefid={{item.systemRefId}} ng-blur=" \n                             vm.buildingDataUUID[item.uuid] != null ? vm.buildingDataUUID[item.uuid]=vm.validationTextfields(item.systemRefId,vm.buildingDataUUID[item.uuid],vm.buildingFormElement,vm.buildingData,vm.buildingEventMode):\'\';\n                                    vm.buildingDataUUID[item.uuid] != null ? vm.validInputfeilds(vm.buildingDataUUID[item.uuid],building.BuildingQuestions,item.eventName,item.systemRefId,vm.buildingFormElement,vm.buildingData,vm.buildingEventMode):\'\';\n                                    vm.buildingDataUUID[item.uuid] != null ? vm.addAditional(item.systemRefId,building.BuildingQuestions,vm.buildingDataUUID[item.uuid],vm.buildingFormElement,vm.buildingData,vm.buildingEventMode):\'\';"><p ng-show="vm.buildingFormElement.{{item.systemRefId | dotWithUnderscore}}.$invalid && vm.buildingFormElement.{{item.systemRefId | dotWithUnderscore}}.$touched" class="help-block error build22" ng-if="(item.systemRefId==\'RiskInput.PermanentlyInstalledEquipmentLimit\')">This field is required</p><p ng-if="item.systemRefId==\'RiskInput.PermanentlyInstalledEquipmentLimit\' && vm.perielerrmsg != null" ng-show=vm.showyear class="help-block error build22">{{vm.perielerrmsg}}</p></div></div></div><!-- end of inline text box--><!-- input Checkbox --><div class=col-md-{{item.screenColumns}} ng-if="(item.controlName==\'Check_Box\') && (item.appearUI==\'Y\')"><div class=form-group ng-class="{ \'has-error\': vm.buildingFormElement.{{item.systemRefId | dotWithUnderscore}}.$touched && vm.buildingFormElement.{{item.systemRefId | dotWithUnderscore}}.$invalid }"><table><tr><td><input class=dsci-checkbox type=checkbox ng-model=vm.buildingDataUUID[item.uuid] ng-change="  vm.buildingDataUUID[item.uuid] != null ? vm.validInputfeilds(vm.buildingDataUUID[item.uuid],building.BuildingQuestions,item.eventName,item.systemRefId,vm.buildingFormElement,vm.buildingData,vm.buildingEventMode):\'\';\n                            vm.yearBuiltAddField(vm.buildingDataUUID[item.uuid],building.BuildingQuestions,item.systemRefId,item.eventName,vm.buildingFormElement,vm.buildingData,vm.buildingEventMode);\n                            vm.disableunderSubmit = true; vm.disableSubmit= false;" ng-init="vm.buildingDataUUID[item.uuid]=(item.defaultValue !=null && item.userValue == null && vm.buildingDataUUID[item.uuid] == null) ?\n                              item.defaultValue : vm.buildingDataUUID[item.uuid]" name="{{item.systemRefId | dotWithUnderscore}}" id={{item.systemRefId}} value={{item.systemRefId}} class=buildcheckbox> <label for={{item.systemRefId}} name="{{item.systemRefId | dotWithUnderscore}}">{{item.datasetName}}</label></td><td style=padding-left:15px;></td></tr></table><p ng-show="vm.buildingFormElement.{{item.systemRefId | dotWithUnderscore}}.$invalid && vm.buildingFormElement.{{item.systemRefId | dotWithUnderscore}}.$touched" class="help-block error build22">{{item.datasetName | camelCase}} is required</p></div></div><!-- end of input checkbox --><!-- list menu (Drop_Down)--><div class=col-md-{{item.screenColumns}} ng-if="item.controlName==\'Drop_Down\' && (item.appearUI==\'Y\')"><div class=form-group ng-class="{ \'has-error\': vm.buildingFormElement.{{item.systemRefId | dotWithUnderscore}}.$touched && vm.buildingFormElement.{{item.systemRefId | dotWithUnderscore}}.$invalid }"><label style=height:35px;>{{item.datasetName}}</label> <select name="{{item.systemRefId | dotWithUnderscore}}" ng-init="vm.buildingDataUUID[item.uuid]=(item.defaultValue !=null)?item.defaultValue:vm.buildingDataUUID[item.uuid]" ng-change="vm.buildingDataUUID[item.uuid] != null ? vm.addAditional(item.systemRefId,building.BuildingQuestions,vm.buildingDataUUID[item.uuid],vm.buildingFormElement,vm.buildingData,vm.buildingEventMode):\'\';" ng-focus="vm.disableunderSubmit = true; vm.disableSubmit= false;" ng-model=vm.buildingDataUUID[item.uuid] ng-init="vm.buildingDataUUID[item.uuid]=(item.defaultValue !=null && item.userValue == null && vm.buildingDataUUID[item.uuid] == null) ?\n                      item.defaultValue : vm.buildingDataUUID[item.uuid]" ng-blur=" vm.buildingDataUUID[item.uuid] != null ? vm.validInputfeilds(vm.buildingDataUUID[item.uuid], building.BuildingQuestions,item.eventName,item.systemRefId,vm.buildingFormElement,vm.buildingData,vm.buildingEventMode):\'\';" class="custom_select form-control" ng-required=item.isRequired><!-- keep the first one always empty this helps with validation and to cancel the selection --><option value="">(select)</option><option ng-repeat="option in item.valueName" value={{option}}>{{option}}</option></select> {{option}}<p ng-show="vm.buildingFormElement.{{item.systemRefId | dotWithUnderscore}}.$invalid && vm.buildingFormElement.{{item.systemRefId | dotWithUnderscore}}.$touched" class="help-block error build22">{{item.datasetName | camelCase}} is required</p></div></div><!-- end of list menu (Drop_Down)--><!-- RADIO BUTTON--><div class=form-group ng-if="(item.controlName==\'Radio_Button\') && (item.appearUI==\'Y\')"><div class=col-md-5><p>{{item.datasetName}}</p></div><div ng-repeat="option in item.valueName" class="col-md-2 checkradio-wrap"><input type=radio ng-model=vm.buildingDataUUID[item.uuid] name="{{item.systemRefId | dotWithUnderscore}}" id={{item.systemRefId+$index}} value={{option}} ng-required=item.isRequired ng-focus="vm.showyear = false; vm.disableunderSubmit = true;\n                      vm.disableSubmit= false;" ng-init="vm.buildingDataUUID[item.uuid]=(item.defaultValue !=null && item.userValue == null && vm.buildingDataUUID[item.uuid] == null) ?\n                      item.defaultValue : vm.buildingDataUUID[item.uuid]" ng-change="vm.buildingDataUUID[item.uuid] != null ? vm.addAditionaldcCall(item.systemRefId,building.BuildingQuestions,vm.buildingDataUUID[item.uuid]):\'\';\n                        vm.buildingDataUUID[item.uuid] != null ? vm.addAditional(item.systemRefId,building.BuildingQuestions,vm.buildingDataUUID[item.uuid],vm.buildingFormElement,vm.buildingData,vm.buildingEventMode):\'\';\n                        vm.buildingDataUUID[item.uuid] != null ? vm.validInputfeilds(vm.buildingDataUUID[item.uuid], building.BuildingQuestions,item.eventName,item.systemRefId,vm.buildingFormElement,vm.buildingData,vm.buildingEventMode):\'\';\n                        vm.buildingDataUUID[item.uuid] != null ? vm.yearBuiltAddField(vm.buildingDataUUID[item.uuid],building.BuildingQuestions,item.systemRefId,item.eventName,vm.buildingFormElement,vm.buildingData,vm.buildingEventMode):\'\';"> <label for={{item.systemRefId+$index}}>{{option}}<!--  <span class="label-text">{{option}}</span>--></label></div><p ng-class="{ \'has-error\': vm.buildingFormElement.{{item.systemRefId | dotWithUnderscore}}.$touched &&\n                    vm.buildingFormElement.{{item.systemRefId | dotWithUnderscore}}.$invalid }" ng-show="vm.buildingFormElement.{{item.systemRefId | dotWithUnderscore}}.$invalid && vm.buildingFormElement.{{item.systemRefId | dotWithUnderscore}}.$touched" class="help-block error build22 ierrormsg">This field is required</p></div><div ng-if="item.appearUI==\'Y\'" style=margin-bottom:10px;></div><!-- END OF RADIO BUTTON--><!--\n                      END LIST OF CONTROLS\n                      --><div class=clearfix ng-if=item.hasRowBreak></div></div><!-- END main loop --></div><!-- ***************************************************END of ADDing building *************************************--><!-- *************************************************** ADD occupant *************************************--><!-- *************************************************** ADD occupant *************************************--><div ng-show="vm.showOccupantForm && vm.showBuildingForm"><form name=vm.occupantDetailsForm novalidate><fieldset class=occupantFeildset><!-- <div ng-repeat="occupantss in vm.addOcuData track by $index" ng-show="!$first"> --><div ng-repeat="occupantss in vm.addOcuData track by $index"><div class=form-group><h4>Occupant {{$index + 2}}</h4><!--  <a href="" ng-click="vm.editOccupant($index,occupants);$event.stopPropagation();">\n                  <b class="pull-right" style="font-size:14px; font-weight:normal">Edit Occupant</b>\n                </a>\n                <a href="" ng-click="vm.removeOccupant($index,occupants);$event.stopPropagation();">\n                  <b class="pull-right" style="font-size:14px; font-weight:normal">Remove</b>\n                </a> --></div><!-- ***************************************************END of ADDing building *************************************--><!-- BEGIN submit button --><div class=row><div class=col-md-12><!--   <button ng-show="vm.cancelHide" type="button" class="btn btn-sm btn-default " ng-click="vm.buildingCancel(vm.buildingID,vm.locID,building,vm.buildingEventMode)"\n                    ng-if="vm.buildingID != 0 && vm.buildingEventMode != \'edit\'">Cancel</button> --><!-- <button type="submit" ng-click="vm.saveBuilding(vm.buildingFormElement,vm.buildingData,vm.buildingEventMode);" class="btn btn-sm btn-primary"\n                    ng-disabled="vm.disableSubmit" ng-class="{\'disableButton\': vm.disableSubmit}">Continue</button> --><div ng-repeat="occupants in occupantss.occupantQuestions"><div class=form-group id=scrollable-dropdown-menu ng-class="{ \'has-error\': vm.occupantDetailsForm.{{occupants.systemRefId}}_{{$parent.$parent.$index + 1}}.$touched && \n                                    vm.occupantDetailsForm.{{occupants.systemRefId}}_{{$parent.$parent.$index + 1}}.$invalid \n                                   }" ng-if="occupants.controlName == \'Drop_Down\'"><label>{{occupants.datasetName}}</label><div style="float: right;"><a ng-show="occupants.userValue && occupants.occInfo" href="" ng-click="vm.editOccupant($parent.$parent.$index,occupants,occupants.occupantId, $index);$event.stopPropagation();"><b style="font-size:14px; font-weight:normal; margin-right: 20px;">Edit</b> </a><a href="" ng-click="vm.removeOccupant($parent.$parent.$index,occupants, occupants.occupantId);$event.stopPropagation();"><b style="font-size:14px; font-weight:normal">Remove</b></a></div><!-- <input type="text" ng-model="occupants.userValue" ng-disabled="occupants.disableStatus" name="{{occupants.systemRefId}}_{{$parent.$parent.$index + 1}}" uib-typeahead="value.Caption for value in vm.startsWith(vm.businessClassValues,occupants.userValue);" \n                    typeahead-min-length="3" typeahead-editable="true" class="form-control" placeholder="{{occupants.additionalInfo}}"\n                    required="required" typeahead-on-select="occupants.userValue.length > 0 ? \n              vm.businessClassValidation(vm.businessClassValues,occupants.userValue,$parent.$parent.$index + 1, occupants) : vm.noResults[$parent.$parent.$index + 1] = false;"\n                    ng-focus="vm.businessClassError = 0; vm.disableunderSubmit = true;" ng-change="occupants.userValue.length > 0 ? \n            vm.businessClassValidation(vm.businessClassValues,occupants.userValue,$parent.$parent.$index + 1,occupants) : vm.noResults[$parent.$parent.$index + 1] = false;"> --> <input type=text ng-model=occupants.userValue ng-disabled=occupants.disableStatus name="{{occupants.systemRefId}}_{{$parent.$parent.$index + 1}}" uib-typeahead="value.Caption for value in vm.businessClassValues | filter:$viewValue" typeahead-min-length=3 typeahead-editable=true class=form-control placeholder={{occupants.additionalInfo}} required typeahead-on-select="occupants.userValue != null ? vm.saveOccupant(occupants,$parent.$parent.$index,occupantss):\'\';\n              occupants.userValue.length > 0 ? \n              vm.businessClassValidate(occupants.userValue,$parent.$parent.$index) : occupants.occSearch = false;\'\'" ng-focus="occupants.userValue.length > 3 ? vm.businessClassValidate(occupants.userValue,$parent.$parent.$index) : occupants.occSearch = false; vm.disableunderSubmit = true;" ng-change="occupants.userValue.length > 3 ? \n            vm.businessClassValidate(occupants.userValue,$parent.$parent.$index) : occupants.occSearch = false;" ng-blur="occupants.userValue.length > 3 ? vm.businessClassValidate(occupants.userValue,$parent.$parent.$index) : occupants.occSearch = false;"> <!--   <input type="text" ng-model="occupants.userValue" ng-disabled="occupants.disableStatus" name="{{occupants.systemRefId}}_{{$parent.$parent.$index + 1}}"\n                                uib-typeahead="value.Caption for value in vm.startsWith(vm.businessClassValues,occupants.userValue);"\n                                typeahead-min-length="3" typeahead-editable="true" class="form-control" placeholder="{{occupants.additionalInfo}}"\n                                required="required" typeahead-on-select="occupants.userValue != null ? vm.saveOccupant(occupants,$parent.$parent.$index,occupantss):\'\';\n              occupants.userValue.length > 0 ? \n              vm.businessClassValidate(occupants.userValue,$parent.$parent.$index) : occupants.occSearch = false;\'\'" ng-focus="occupants.userValue.length> 3 ? vm.businessClassValidate(occupants.userValue,$parent.$parent.$index) : occupants.occSearch\n                              = false; vm.disableunderSubmit = true;" ng-change="occupants.userValue.length > 3 ? vm.businessClassValidate(occupants.userValue,$parent.$parent.$index)\n                              : occupants.occSearch = false;" ng-blur="occupants.userValue.length > 3 ? vm.businessClassValidate(occupants.userValue,$parent.$parent.$index)\n                              : occupants.occSearch = false;">\n\n                              <h5 ng-show="!occupants.occSaved && occupants.occSaved !== undefined && !occupants.occSearch" style="color: red;"> This field is required </h5>\n\n                              <!-- <p id="{{businessClassName.systemRefId}}" ng-if="vm.noResults && vm.businessClassError == 1" ng-hide="(businessDetailsForm.businessClassAndCode.$invalid && businessDetailsForm.businessClassAndCode.$touched)"\n              class="help-block error build22">No Business Class Found</p> --><h5 ng-show="!occupants.occSaved && occupants.occSaved !== undefined && !occupants.occSearch" style="color: red;">This field is required</h5><p id={{businessClassName.systemRefId}} ng-show=occupants.occSearch class="help-block error build22">No Business Class Found</p><!-- \n            ng-blur="occupants.userValue != null ? vm.saveOccupant(occupants,occupantss.occupantId,$parent.$parent.$index):\'\';" --><!--  <p ng-show=" vm.occupantDetailsForm.{{occupants.systemRefId}}_{{$parent.$parent.$index + 1}}.$invalid && vm.occupantDetailsForm.{{occupants.systemRefId}}_{{$parent.$parent.$index + 1}}.$touched"\n                    class="help-block error build22">Occupant is required</p>\n                  <p ng-if="vm.noResults[$parent.$parent.$index + 1]" ng-hide="(vm.occupantDetailsForm.{{occupants.systemRefId}}_{{$parent.$parent.$index + 1}}.$invalid && vm.occupantDetailsForm.{{occupants.systemRefId}}_{{$parent.$parent.$index + 1}}.$touched) && occupants.userValue != null"\n                    class="help-block error build22">No Occupant Found</p> --></div><div class=clearfix ng-if=item.hasRowBreak></div></div></div></div></div></fieldset></form><h4 class="card-title addOccupantLink" ng-show=vm.addAnotherOccupant><a ng-click="vm.addNewOccupant(vm.addOcuData,vm.buildingData, occupantss.occupantQuestions)" style="font-size: 18px;"><label class="loc expand-roundicon">Add Another Occupant</label></a></h4><h5 ng-show=vm.showOccupantError style="color: red;">Multiple Occupancy building with Tenant as Applicant. Only one occupancy allowed. Please delete non-applicant occupancies</h5></div><div class="row pull-right"><div class=col-md-12><button ng-show=vm.cancelHide type=button class="btn btn-sm btn-default" ng-click=vm.buildingCancel(vm.buildingID,vm.locID,building,vm.buildingEventMode) ng-if="vm.buildingID != 0 && vm.buildingEventMode != \'edit\'">Cancel</button> <button type=submit ng-mousedown="vm.appianBefSave = true" ng-click=vm.saveBuilding(vm.buildingFormElement,vm.buildingData,vm.buildingEventMode); class="btn btn-sm btn-primary" ng-disabled="vm.disableSubmit || vm.showOccupantError" ng-class="{\'disableButton\': vm.disableSubmit}">Continue</button></div></div><div class=clearfix></div><!-- building form white panel end --></div></form><!--   <h4 class="card-title addOccupantLink" ng-show="vm.addAnotherOccupant">\n        <label class=" loc expand-roundicon">\n          <a ng-click="vm.addNewOccupant(vm.addOcuData,vm.buildingData, occupantss.occupantQuestions)" style="font-size: 18px;">Add Another Occupant</a>\n        </label>\n      </h4> --><!-- BEGIN submit button Buil --><!--  <div class="buildingSave">\n        \n        <button ng-show="vm.cancelHide" type="button" class="btn btn-sm btn-default " ng-click="vm.buildingCancel(vm.buildingID,vm.locID,building,vm.buildingEventMode)">Cancel</button>\n        <button type="button" class="btn btn-sm btn-primary" form="buildingForms" ng-disabled="vm.disableSubmit" ng-class="{\'disableButton\': vm.disableSubmit}"\n          ng-click="vm.saveBuilding(vm.occupantDetailsForm,vm.addOcuData,vm.buildingFormElement,vm.buildingData,vm.buildingEventMode);">Continue</button>\n        \n      </div> --><!-- END submit button --></div><!-- ***************************************************Building UnderWriting *************************************--><div class=row ng-show="vm.buildunderqa && vm.showBuildingForm"><form data-toggle=validator class=formClass form-submit-validation="" name=vm.buildingUnderFormElement id=buildingUnderWrite autocomplete=off novalidate><fieldset class=buildingFeildset><!-- build form white panel begin --><div class=white-pannel ng-class="{\'disableunderButton\': vm.disableunderSubmit}" ng-repeat="buildingUnder in vm.buildingUnderData track by $index"><div class=row style="margin-top: 0px;"><!-- BEGIN main loop --><div ng-repeat="underitem in buildingUnder.BuildingQuestions track by $index"><!--\n                                    LIST OF CONTROLS\n                                    --><!-- vm.buildingDataUUID -> vm.buildingDataUUID --><!-- help text --><div class=col-md-{{underitem.screenColumns}} ng-if="(underitem.controlName==\'Help_Text\') && (underitem.appearUI==\'Y\')"><div><p style="height:25px; padding-left:0px;" id={{underitem.systemRefId}} ng-model=vm.buildingUnderDataUUID[underitem.uuid] ng-disabled=underitem.nonEditableFeild>{{underitem.datasetName}}</p></div></div><!-- end of help text --><!-- input box --><div class=col-md-{{underitem.screenColumns}} ng-if="(underitem.controlName==\'Text_Box\') && (underitem.appearUI==\'Y\')"><div class=form-group ng-class="{ \'has-error\':(vm.buildingUnderFormElement.{{underitem.systemRefId | dotWithUnderscore}}.$touched && vm.buildingUnderFormElement.{{underitem.systemRefId | dotWithUnderscore}}.$invalid)                                            || ((underitem.systemRefId == \'BuildingInput.UNIG_BuildingAnnualSales\' && vm.buildingUnderDataUUID[underitem.uuid] && vm.errorMessageAnnualSale == true) && vm.showAnnualSaleErrorMesg == 1)\n                                                                                    \n                                                                                                                            ||((underitem.systemRefId==\'BuildingInput.UNIG_BuildingAnnualSales\' && vm.annualsaleserrmsg != null && vm.buildingUnderDataUUID[underitem.uuid]))\n                                                                                                                                                          }"><label style=height:35px;>{{underitem.datasetName}}</label> <input type=text data-uuid={{underitem.uuid}} name="{{underitem.systemRefId | dotWithUnderscore}}" id="{{underitem.systemRefId | dotWithUnderscore}}" class=form-control ng-model=vm.buildingUnderDataUUID[underitem.uuid] placeholder={{underitem.additionalInfo}} ng-attr-format-currency-location="{{ underitem.systemRefId == \'BuildingInput.UNIG_BuildingAnnualSales\' ? true : false }}" autocomplete=on|off ng-init="vm.underappainCheck = false ; vm.buildingUnderDataUUID[underitem.uuid]=(underitem.defaultValue !=null && underitem.userValue == null && vm.buildingUnderDataUUID[underitem.uuid] == null) ?\n                                      underitem.defaultValue : vm.buildingUnderDataUUID[underitem.uuid]" ng-required=underitem.isRequired ng-keypress="vm.pasteData = false" ng-maxlength="{\n                              14:underitem.systemRefId == \'BuildingInput.UNIG_BuildingAnnualSales\',\n                                10:underitem.systemRefId ==\'UNIG_BuildingUndQuestionsInput.ResidentialSquareFootage\' ,\n                                4:underitem.systemRefId == \'UNIG_BuildingUndQuestionsInput.ResidentialUnitsNumber\'}" input-length-limit paste-number-only="{\n                                  4:{{underitem.systemRefId == \'UNIG_BuildingUndQuestionsInput.ResidentialUnitsNumber\'}},\n                                  14:{{underitem.systemRefId == \'BuildingInput.UNIG_BuildingAnnualSales\'}},\n                                  10:{{underitem.systemRefId ==\'UNIG_BuildingUndQuestionsInput.ResidentialSquareFootage\'}}\n                                    }" ng-disabled=vm.disableunderSubmit uservalue={{vm.buildingUnderDataUUID[underitem.uuid]}} ng-paste="vm.pasteData = true" pasteboolean={{vm.pasteData}} ng-focus="vm.pasteData = false;\n                                vm.showAnnualSaleErrorMesg = 0;\n                                vm.errorMessageAnnualSale = false;" ng-blur=" vm.buildingUnderDataUUID[underitem.uuid] != null && underitem.eventAssociatedID == 1 ? vm.getChildUnderwriting(buildingUnder,underitem.systemRefId,buildingUnder.BuildingQuestions,vm.buildingUnderDataUUID[underitem.uuid],underitem.eventName):\'\';\n                                    vm.buildingUnderDataUUID[underitem.uuid] != null && underitem.eventName != null && vm.addAditionalCall == false ? vm.addAditionalUnder(underitem.systemRefId,buildingUnder.BuildingQuestions,vm.buildingUnderDataUUID[underitem.uuid],underitem.eventName,vm.buildingUnderFormElement,vm.buildingUnderData,vm.buildingEventMode):\'\';\n                                  "><p ng-show="vm.buildingUnderFormElement.{{underitem.systemRefId | dotWithUnderscore}}.$invalid && vm.buildingUnderFormElement.{{underitem.systemRefId | dotWithUnderscore}}.$touched" class="help-block error build22" ng-if="(underitem.systemRefId != \'UNIG_BuildingUndQuestionsInput.ResidentialUnitsNumber\')&&\n                                             (underitem.systemRefId != \'UNIG_BuildingUndQuestionsInput.ResidentialSquareFootage\') &&  (underitem.systemRefId !=\'BuildingInput.UNIG_BuildingAnnualSales\') ">{{underitem.datasetName | camelCase}} is required</p><p ng-show="vm.buildingUnderFormElement.{{underitem.systemRefId | dotWithUnderscore}}.$invalid && vm.buildingUnderFormElement.{{underitem.systemRefId | dotWithUnderscore}}.$touched" class="help-block error build22" ng-if="(underitem.systemRefId==\'UNIG_BuildingUndQuestionsInput.ResidentialUnitsNumber\')||\n                                              (underitem.systemRefId==\'UNIG_BuildingUndQuestionsInput.ResidentialSquareFootage\')||\n                                              (underitem.systemRefId==\'BuildingInput.UNIG_BuildingAnnualSales\')\n                                              ">This field is required</p><p ng-if="underitem.systemRefId == \'BuildingInput.UNIG_BuildingAnnualSales\' && vm.buildingUnderDataUUID[underitem.uuid] && vm.errorMessageAnnualSale == true" ng-show="vm.showAnnualSaleErrorMesg == 1" class="help-block error build22">Invalid Annual Sales</p><p ng-if="underitem.systemRefId==\'BuildingInput.UNIG_BuildingAnnualSales\' && vm.annualsaleserrmsg != null && vm.buildingUnderDataUUID[underitem.uuid]" ng-show="vm.annualsaleserrmsg != null" class="help-block error build22">{{vm.annualsaleserrmsg}}</p></div></div><!-- end of input box --><!-- list menu (Drop_Down)--><div class=col-md-{{underitem.screenColumns}} ng-if="underitem.controlName==\'Drop_Down\' && (underitem.appearUI==\'Y\')"><div class=form-group ng-class="{ \'has-error\': vm.buildingUnderFormElement.{{underitem.systemRefId | dotWithUnderscore}}.$touched && vm.buildingUnderFormElement.{{underitem.systemRefId | dotWithUnderscore}}.$invalid }"><label style=height:35px;>{{underitem.datasetName}}</label> <select name="{{underitem.systemRefId | dotWithUnderscore}}" ng-init="vm.buildingUnderDataUUID[underitem.uuid]=(underitem.defaultValue !=null)?underitem.defaultValue:vm.buildingUnderDataUUID[underitem.uuid]" ng-disabled=vm.disableunderSubmit ng-model=vm.buildingUnderDataUUID[underitem.uuid] class="custom_select form-control" ng-required=underitem.isRequired><!-- keep the first one always empty this helps with validation and to cancel the selection --><option value="">(select)</option><option ng-repeat="option in underitem.valueName" value={{option.value}}>{{option.caption}}</option></select> {{option}}<p ng-show="vm.buildingUnderFormElement.{{underitem.systemRefId | dotWithUnderscore}}.$invalid && vm.buildingUnderFormElement.{{underitem.systemRefId | dotWithUnderscore}}.$touched" class="help-block error build22">{{underitem.datasetName | camelCase}} is required</p></div></div><!-- end of list menu (Drop_Down)--><!-- input Checkbox --><div class=col-md-{{underitem.screenColumns}} ng-if="(underitem.controlName==\'Check_Box\') && (underitem.appearUI==\'Y\')"><div class=form-group ng-class="{ \'has-error\': vm.buildingUnderFormElement.{{underitem.systemRefId | dotWithUnderscore}}.$touched && vm.buildingUnderFormElement.{{underitem.systemRefId | dotWithUnderscore}}.$invalid }"><table><tr><td><input class=dsci-checkbox type=checkbox ng-model=vm.buildingUnderDataUUID[underitem.uuid] ng-change=" \n                              vm.buildingUnderDataUUID[underitem.uuid] != null  && underitem.eventAssociatedID == 1 ? vm.getChildUnderwriting(buildingUnder,underitem.systemRefId,buildingUnder.BuildingQuestions,vm.buildingUnderDataUUID[underitem.uuid],underitem.eventName):\'\';\n                              vm.buildingUnderDataUUID[underitem.uuid] != null && underitem.eventName != null && underitem.systemRefId == \'Have_all_of_the_following_been\' ? vm.yearUnder(underitem.systemRefId,buildingUnder.BuildingQuestions,vm.buildingUnderDataUUID[underitem.uuid],underitem.eventName):\'\';\n                              vm.buildingUnderDataUUID[underitem.uuid] != null && underitem.eventName != null && underitem.systemRefId != \'Have_all_of_the_following_been\'  ? vm.yearBuiltAddFieldUnder(vm.buildingUnderDataUUID[underitem.uuid],buildingUnder.BuildingQuestions,underitem.systemRefId,underitem.eventName):\'\'; " ng-disabled=vm.disableunderSubmit ng-init="vm.buildingUnderDataUUID[underitem.uuid]=(underitem.defaultValue !=null && underitem.userValue == null && vm.buildingUnderDataUUID[underitem.uuid] == null) ?\n                                           underitem.defaultValue : vm.buildingUnderDataUUID[underitem.uuid]" name="{{underitem.systemRefId | dotWithUnderscore}}" id={{underitem.systemRefId}} value={{underitem.systemRefId}} class=buildcheckbox> <label for={{underitem.systemRefId}} name="{{underitem.systemRefId | dotWithUnderscore}}">{{underitem.datasetName}}</label></td><td style=padding-left:15px;></td></tr></table><p ng-show="vm.buildingUnderFormElement.{{underitem.systemRefId | dotWithUnderscore}}.$invalid && vm.buildingUnderFormElement.{{underitem.systemRefId | dotWithUnderscore}}.$touched" class="help-block error build22">{{underitem.datasetName | camelCase}} is required</p></div></div><!-- end of input checkbox --><!-- RADIO BUTTON--><div class=form-group ng-if="(underitem.controlName==\'Radio_Button\') && (underitem.appearUI==\'Y\')"><div class=col-md-5><p>{{underitem.datasetName}}</p></div><div ng-repeat="option in underitem.valueName" class="col-md-2 checkradio-wrap"><input type=radio ng-model=vm.buildingUnderDataUUID[underitem.uuid] name="{{underitem.systemRefId | dotWithUnderscore}}" id={{underitem.systemRefId+$index}} ng-disabled=vm.disableunderSubmit value={{option.value}} ng-required=underitem.isRequired ng-focus="vm.showyearUnder = false;" ng-init=" vm.buildingUnderDataUUID[underitem.uuid]=(underitem.defaultValue !=null && underitem.userValue == null && vm.buildingUnderDataUUID[underitem.uuid] == null) ?\n                                      underitem.defaultValue : vm.buildingUnderDataUUID[underitem.uuid]" ng-change="vm.buildingUnderDataUUID[underitem.uuid] != null && underitem.eventAssociatedID == 1 ? vm.getChildUnderwriting(buildingUnder,underitem.systemRefId,buildingUnder.BuildingQuestions,vm.buildingUnderDataUUID[underitem.uuid],underitem.eventName):\'\';\n                          vm.buildingUnderDataUUID[underitem.uuid] != null && underitem.eventName != null && underitem.systemRefId != \'Have_all_of_the_following_been\'&& vm.addAditionalCall == false ? vm.addAditionalUnder(underitem.systemRefId,buildingUnder.BuildingQuestions,vm.buildingUnderDataUUID[underitem.uuid],underitem.eventName,vm.buildingUnderFormElement,vm.buildingUnderData,vm.buildingEventMode):\'\';\n                         vm.buildingUnderDataUUID[underitem.uuid] != null && underitem.eventName != null && underitem.systemRefId == \'Have_all_of_the_following_been\' ? vm.yearUnder(underitem.systemRefId,buildingUnder.BuildingQuestions,vm.buildingUnderDataUUID[underitem.uuid],underitem.eventName):\'\'; "> <label ng-class="{\'radiochange\' : vm.disableunderSubmit}" for={{underitem.systemRefId+$index}}>{{option.caption}}<!--  <span class="label-text">{{option}}</span>--></label></div><div ng-if="underitem.toolTip != null" class=col-md-5 style="margin-right: 200px;"><h6 class="card-subtitle mb-2 text-muted" style="height:17px; padding-left:0px;" ng-disabled=true>{{underitem.toolTip}}</h6></div><p ng-class="{ \'has-error\': vm.buildingUnderFormElement.{{underitem.systemRefId | dotWithUnderscore}}.$touched &&\n                                                                                                  vm.buildingUnderFormElement.{{underitem.systemRefId | dotWithUnderscore}}.$invalid }" ng-show="vm.buildingUnderFormElement.{{underitem.systemRefId | dotWithUnderscore}}.$invalid && vm.buildingUnderFormElement.{{underitem.systemRefId | dotWithUnderscore}}.$touched" class="help-block error build22 ierrormsg">This field is required</p></div><div ng-if="underitem.appearUI==\'Y\'" style=margin-bottom:10px;></div><div class=clearfix ng-if=underitem.hasRowBreak></div></div></div><!-- BEGIN submit button --></div><div class=clearfix></div></fieldset></form><div class="row pull-right" style="margin-right: 5px;" ng-show="vm.buildunderqa && vm.showBuildingForm"><div class=col-md-12><button type=button class="btn btn-sm btn-default" form=buildingUnderWrite ng-click=vm.buildingUnderCancel(vm.buildingID,vm.locID,vm.buildingUnderData[0],vm.buildingUnderEventMode) ng-if="vm.buildingID != 0 && vm.buildingEventMode != \'edit\'">Cancel</button> <button type=submit ng-mousedown="vm.appianBefSaveUnder = true" ng-click=vm.saveBuildingUnderwriting(vm.buildingUnderFormElement,vm.buildingUnderData,vm.buildingEventMode) ng-disabled="vm.disableunderSubmit || vm.showOccupantError" class="btn btn-sm btn-primary" form=buildingUnderWrite>Save</button></div></div></div></div><!-- end class="col-md-12"--></div><!-- end class="col-md-9"--><!---***********************************************  Google API *********************************************--><!-- Verisk Response --><div class="col-md-3 right-col" ng-show="vm.veriskSuggestionBox && vm.showBuildingForm"><div class="card right-rail" style="background-color: #F5F7F8;"><div class="panel panel-default"><div class="panel-heading clearfix"><h3 style="float:left; color: gray;" class=panel-title>Verisk ISO</h3><!-- <span style="float:right">\n              <img style="padding:12px" ng-src="{{main.assetPath+\'google.png\'}}" />\n            </span> --></div><!-- Year Built Verisk Response  --><div class=panel-body><p><strong>Year Built</strong></p><div class=panel-sec ng-bind-html=vm.yearBuilt></div></div><!--  Construction verisk response--><div class=panel-body><p><strong>Construction</strong></p><div class=panel-sec ng-bind-html=vm.constructionVeriskBox></div></div><!--  Square Footage verisk response--><div class=panel-body><p><strong>Square Footage</strong></p><div class=panel-sec ng-bind-html="vm.squareFootage | squareFootageCommaSeparator"></div></div></div></div></div><!-- Google Box --><div class="col-md-3 right-col" ng-show="vm.googleData && vm.showBuildingForm"><div class="card right-rail" style="background-color: #F5F7F8;"><div class="panel panel-default"><div class="panel-heading clearfix"><h3 style="float:left; color: gray;" class=panel-title>Google Maps</h3><span style=float:right><img style=padding:12px ng-src="{{main.assetPath+\'google.png\'}}"></span></div><div class=panel-body><p><strong>ADDRESS</strong></p><div class=panel-sec ng-bind-html=vm.gAddr></div></div><img style="width:100%; height:150px" style=padding:12px ng-src="https://maps.googleapis.com/maps/api/streetview?size=400x400&location={{vm.lat}},{{vm.lng}}&key=AIzaSyCirM4vfDlez6kIT6TiVD8nS9Veti4iyPg"></div></div><!-- <div class="card right-rail" style="background-color: #F5F7F8;">\n        <div class="panel panel-default">\n          <div class="panel-heading clearfix">\n            <h3 style="float:left; color: gray;" class="panel-title">Google</h3>\n            <span style="float:right">\n              <img style="padding:12px" ng-src="{{main.assetPath+\'google.png\'}}" />\n            </span>\n          </div>\n          <div class="panel-body">\n            <p>\n              <strong>PHONE NUMBER</strong>\n            </p>\n            <div class="panel-sec" ng-bind-html="vm.gPhone">\n            </div>\n          </div>\n\n        </div>\n      </div> --></div><!---***********************************************  Google API END *********************************************--><div class=col-md-9><div class=col-md-12><div class="card plan-bg"><div class="input-group col-md-4" style="width: 20%"><button type=submit ng-hide=vm.viewMode ng-click=vm.saveLoc() style="background-color: red" ui-sref=home.lob class="btn btn-lg btn-primary">Save &amp; Continue</button></div></div></div></div></div><!-- begin Building page google map row ng-show="vm.googleData" --><!--<div class="col-md-3" ng-show="vm.googleData">\n      <div class="card right-rail" style="background-color: #F5F7F8;">\n        <div class="panel panel-default">\n          <div class="panel-heading clearfix">\n            <h3 style="float:left; color: gray;" class="panel-title">Google Maps</h3>\n            <span style="float:right">\n              <img style="padding:12px" src="./jsp/assets/images/google.png" />\n            </span>\n          </div>\n          <div class="panel-body">\n            <div class="panel-sec">\n              <h5>\n                <b>YEAR BUILT</b>\n              </h5>\n              <p class="grayedcolor">1961</p>\n            </div>\n            <div class="panel-sec">\n              <h5>\n                <b>BUILDING LIMIT</b>\n              </h5>\n              <p class="grayedcolor">2,100,000</p>\n            </div>\n          </div>\n          <a class="ext-link" href="">\n            <b>View in Google Maps</b>\n          </a>\n        </div>\n      </div>\n      <div class="card right-rail" style="background-color: #F5F7F8;">\n        <div class="panel panel-default">\n          <div class="panel-heading clearfix">\n            <h3 style="float:left; color: gray;" class="panel-title">Verisk</h3>\n          </div>\n          <div class="panel-body">\n            <div class="panel-sec">\n              <h5>\n                <b>SQUARE FOOTAGE</b>\n              </h5>\n              <p class="grayedcolor">2600</p>\n            </div>\n            <div class="panel-sec">\n              <h5>\n                <b>NUMBER OF STORIES</b>\n              </h5>\n              <p class="grayedcolor">1</p>\n            </div>\n            <div class="panel-sec">\n              <h5>\n                <b>CONSTRUCTION TYPE</b>\n              </h5>\n              <p class="grayedcolor">Mansory Non-Combination</p>\n            </div>\n          </div>\n          <a class="ext-link" href="">\n            <b>View in verisk</b>\n          </a>\n        </div>\n      </div>\n      <div class="card right-rail" style="background-color: #F5F7F8;">\n        <div class="panel panel-default">\n          <div class="panel-heading clearfix">\n            <h4 style="float:left; color: gray;" class="panel-title">Google</h4>\n            <span style="float:right">\n              <img style="padding:12px" src="./jsp/assets/images/google.png" />\n            </span>\n          </div>\n          <div class="panel-body">\n            <div class="bgk-image-panel">\n            </div>\n          </div>\n          <a class="ext-link" href="">\n            <b>View in Google Streetview</b>\n          </a>\n        </div>\n      </div>\n    </div>--><!-- end Building page google map row --></div><!-- **********************************************************END - list of ADD Location and Building  page ***********************************************************--><!-- modal template --><script type=text/ng-template id=customTemplate.html><a>\n      <span ng-bind-html="match.label | uibTypeaheadHighlight:query"></span>\n    </a></script><!-- modal template --><script type=text/ng-template id=customTemplate.html><a>\n      <span ng-bind-html="match.label | uibTypeaheadHighlight:query"></span>\n    </a></script><!-- modal template --><script type=text/ng-template id=customPopupTemplate.html><div class="custom-popup-wrapper" ng-style="{top: position().top+\'px\', left: position().left+\'px\'}" style="display: block;"\n      ng-show="isOpen() && !moveInProgress" aria-hidden="{{!isOpen()}}">\n      <ul class="dropdown-menu" role="listbox">\n        <li class="uib-typeahead-match" ng-repeat="match in matches track by $index" ng-class="{active: isActive($index) }" ng-mouseenter="selectActive($index)"\n          ng-click="selectMatch($index)" role="option" id="{{::match.model.sicCode}}">\n          <div uib-typeahead-match index="$index" match="match" query="query" template-url="customTemplate.html"></div>\n        </li>\n      </ul>\n    </div></script><!-- modal template --><!-- <script type="text/ng-template" id="blocks/modal/dialog.html">\n    <div class="modal-body gradientContent">\n      <div class="row">\n        <div class="col-md-8">\n          <h2 style="margin-top: 0;padding-top: 0;">Are you sure?</h2>\n        </div>\n        <div class="col-md-2">\n          <button class="btn btn-lg btn-primary" type="button" ng-click="vm.yes()">Yes</button>\n        </div>\n        <div class="col-md-2">\n          <button class="btn btn-lg btn-primary" type="button" ng-click="vm.no()">No</button>\n        </div>\n      </div>\n    </div>\n  </script>\n --><!--location modal template --><script type=text/ng-template id=blocks/modal/locDialog.html><!-- header -->\n    <div class="modal-header" style="border-bottom: 0px;">\n      <h4 class="modal-title" id="myModalLabel">Remove Location</h4>\n    </div>\n    <!-- body -->\n    <div class="modal-body">\n      Are you sure you want to remove this location?\n    </div>\n       <!-- footer -->\n     <div class="modal-footer">\n      <button type="button" class="btn btn-default" ng-click="$locRemCtrl.locCancel()">Cancel</button>\n     <button type="button" class="btn btn-primary" ng-click="$locRemCtrl.locRemoveConfirm()">Remove</button>\n\n    </div></script><!-- build modal template --><script type=text/ng-template id=blocks/modal/buildDialog.html><!-- header -->\n      <div class="modal-header" style="border-bottom: 0px;">\n        <h4 class="modal-title" id="myModalLabel">Remove Building</h4>\n      </div>\n      <!-- body -->\n      <div class="modal-body">\n        Are you sure you want to remove this Building?\n      </div>\n         <!-- footer -->\n       <div class="modal-footer">\n        <button type="button" class="btn btn-default" ng-click="$buildRemCtrl.buildCancel()">Cancel</button>\n        <button type="button" class="btn btn-primary" ng-click="$buildRemCtrl.buildRemoveConfirm($buildRemCtrl.locationId,$buildRemCtrl.buildingId)">Remove</button>\n\n      </div></script><!-- edit modal template --><script type=text/ng-template id=blocks/modal/editDialog.html><!-- header -->\n      <div class="modal-header" style="border-bottom: 0px;">\n        <h4 class="modal-title" id="myModalLabel">Edit Location</h4>\n      </div>\n      <!-- body -->\n      <div class="modal-body">\n        Making changes to the location address will not update the "Magic Fill" detailed data. Please verify the location details for this location.\n      </div>\n         <!-- footer -->\n       <div class="modal-footer">\n        <button type="button" class="btn btn-default" ng-click="$editConfirmCtrl.editCancel()">Cancel</button>\n        <button type="button" class="btn btn-primary" ng-click="$editConfirmCtrl.editConfirm()">OK</button>\n\n      </div></script>');
$templateCache.put('app/views/location/nav.location.html','<div style="min-height: 460px;"><span class=summary-leftnav><br><span class=selected-plan style="display: inline-block;"></span> <a class=list-group-item4 ui-sref=app.businessdetails>Business Details</a> </span><span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span> <a class=list-group-item4 ui-sref=app.lob>Select lines of business</a> </span><span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span> <a class=list-group-item4 ui-sref=app.qualification>Qualification Questions</a></span><hr><h4 class=eligibility-panels>Business Owners Policy</h4><span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span> <a class=list-group-item4 ui-sref=app.quote>Start your quote</a></span><ul class="nav list-group eligibility-panel"><li><a class="list-group-item active" href=""><i class="icon-home icon-1x"></i>Locations and Buildings</a></li><li><a class="list-group-item locked" href=""><i class="icon-home icon-1x"></i>Additional Interests</a></li><li><a class="list-group-item locked" href=""><i class="icon-home icon-1x"></i>Underwriting Questions</a></li><li><a class="list-group-item locked" href=""><i class="icon-home icon-1x"></i>Pricing & Coverages</a></li></ul></div><div style="position: relative;bottom:0px;color: #909090;padding-left: 5px;">Copyright \xA9 2018 Utica Mutual Insurance Company, New Hartford, NY 13413 | All Rights Reserved</div>');
$templateCache.put('app/views/lob/lob.html','<div class=new-lob-page><form class=formClass name=vm.lobForm form-submit-validation="" ng-submit=vm.submitForm(vm.lobForm.$valid,vm.lobForm) novalidate><div class=col-md-12><div class=col-md-10><h1 class=b16>Lines of Business</h1><div id=bname class="card no-margin"><div class=card-block><h4 class="card-title mb-10">Select Lines of Business</h4><div ng-if="vm.message != null" class="alert border alert-danger alert-color"><span><img ng-src="{{main.assetPath+\'icon-more-info.svg\'}}"> </span><span>{{vm.message}}</span></div><p ng-if="vm.lobs.length > 0">Based on information provided, available lines of business are shown below</p><div class=row><div class=col-md-12><!-- --><p class=title-question ng-if="vm.lobs.length > 0">What lines of business would you like to quote?</p><div class=checkbox-containers><!-- start --><div class=row data-ng-repeat="lob in vm.lobs" data-ng-if="lob.controlName == \'Check_Box\' && lob.appearUI == \'Y\'"><!-- checkbox container --><div class="col-md-9 checkbox-wrap"><input type=checkbox name="{{ lob.systemRefId }}" id="{{ lob.systemRefId }}" class=checkbox data-ng-model=lob.userValue ng-disabled="lob.isRequiredToQuote || lob.editable == \'N\'" ng-checked="lob.userValue == 1" ng-true-value=1 ng-false-value=0> <label for="{{ lob.systemRefId }}" class=checkbox-label>{{ lob.datasetName }}</label></div><div class=col-md-3><h4 class=eligible><img ng-src="{{main.assetPath+\'finished.png\'}}"> Eligible</h4></div><!-- additional info --><div class=row data-ng-if="lob.additionalInfo != null"><div class=col-md-12><p data-ng-bind-html=lob.additionalInfo class=additional-info-text></p></div></div></div><!-- end --></div></div></div></div></div></div></div><!-- continue button --><div class=col-md-9><div class=col-md-12><div class="card plan-bg"><div class="input-group col-md-4" style="width: 20%"><button type=submit class="btn btn-lg btn-primary" ng-disabled=vm.disableSubmit>Continue</button></div></div></div></div></form></div>');
$templateCache.put('app/views/lob/nav.lob.html','<span class=summary-leftnav><br><span class=selected-plan style="display: inline-block;"></span> <a class=list-group-item4 ui-sref=app.businessdetails>Business Details</a></span><ul class="nav list-group"><li class=lob-leftnav><a class="list-group-item active" ui-sref=app href=""><i class="icon-home icon-1x"></i>Select lines of business</a></li></ul><div style="position: absolute;bottom:0px;color: #909090;padding-left: 5px;">Copyright \xA9 2018 Utica Mutual Insurance Company, New Hartford, NY 13413 | All Rights Reserved</div>');
$templateCache.put('app/views/premiumsummary/nav.premiumsummary.html','<span class=summary-leftnav><br><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.businessdetails>Business Details</a></span> <span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.lob>Select lines of business</a></span> <span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.qualification>Qualification Questions</a></span><hr><h4 class=eligibility-panels>Business Owners Policy</h4><span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.quote>Start your quote</a></span> <span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.location>Locations and Buildings</a></span> <span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.additionalinterest>Additional Interests</a></span> <span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.underwriting>Underwriting Questions</a></span> <span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.pricingandcoverage>Pricing and Coverage</a></span><ul class="nav list-group"><li style="padding-left: 5px;"><a class="list-group-item active" href=""><i class="icon-home icon-1x"></i>Premium Summary</a></li><li style="padding-left: 5px;"><a class="list-group-item locked" href=""><i class="icon-home icon-1x"></i>Before you issue policy</a></li></ul><div style="position: relative;bottom:0px;color: #909090;padding-left: 5px;">Copyright \xA9 2018 Utica Mutual Insurance Company, New Hartford, NY 13413 | All Rights Reserved</div>');
$templateCache.put('app/views/premiumsummary/premiumsummary.html','<div class="col-md-12 premium-summary-page"><!--start modal for create proposal--><script type=text/ng-template id=Createproposal.html><div class="modal-header premium-summary-modal">\n      <button type="button" class="close premium-summary-modal popup-close" ng-click="$ctrl.cancel()">&times;</button>\n      <h1 class="premium-summary-modal  modal-title header-title">Create Proposal</h1>\n    </div>\n    <div class="modal-body premium-summary-modal body-bg">\n      <div class="row">\n        <div class="col-md-6">\n          <h4>Terms & Conditions</h4>\n          <li>Subject to favorable loss control survey</li>\n          <li>Subject to loss or claim information verification</li>\n          <li>Subject to financial stability review</li>\n          <li>Subject to general risk information verification</li>\n        </div>\n        <div class="col-md-6">\n          <h6>\n            <b>OTHER TERMS & CONDITIONS</b>\n          </h6>\n          <textarea class="premium-summary-modal modaltextarea" rows="5" ng-model="othertc" placeholder="Other Terms & Conditions"></textarea>\n        </div>\n      </div>\n      <div>\n        <h4>Proposal Description</h4>\n        <textarea class="premium-summary-modal modaltextarea" rows="5" ng-model="proposalDesc" placeholder="Proposal description"></textarea>\n      </div>\n    </div>\n    <div class="modal-footer premium-summary-modal footer-bg">\n      <button class="btn btn-blue cancelbtn" ng-click="$ctrl.cancel()">Cancel</button>\n      <button type="button" class="btn btn-blue nextbtn" ng-click="$ctrl.generatepdf(othertc,proposalDesc)">Next</button>\n    </div></script><!--end modal window for create proposal--><h2 style=padding-left:20px;>Premium Summary</h2><br><div class="col-md-8 summary-details" style=padding-left:30px;><!--For all coverages--><div class="row summary-page"><div class="row summary-page-content" ng-repeat="component in vm.premiumsummaryData.groups" ng-init="parentIndex = $index"><table><tbody><tr><td class=cov-head><div class=coverage-head><span class=mainindexnumber>{{$index+1}}</span> <span class=mainindexname>{{component.datasetName}}&nbsp; <a ng-if="component.toolTip !== null && component.toolTip !== undefined" data-toggle=tooltip data-placement=right title={{component.datasetName}}><img class=infoicon ng-src="{{main.assetPath+\'icon-more-info.png\'}}"></a></span></div></td><td class=cov-head-val><div class=coverage-value><p ng-repeat="componentDisplayValue in component.components track by $index">{{componentDisplayValue.compDisplayValue[0]}}</p></div></td></tr><tr ng-repeat="subGroup in component.subGroups| filter: {appearUI: \'Y\'} |filter: {appearUI: \'y\'}" style=height:60px;><td class=cov-subhead><div class=mainvalue><span class=col-md-1>{{parentIndex+1}}.{{$index+1}}</span> <span class=col-md-11 ng-bind-html=subGroup.datasetName></span><p class=col-md-11 style=padding-left:50px;opacity:0.5; ng-repeat="componentDisplayValue in subGroup.components track by $index" ng-if="componentDisplayValue.compShowAsHelp.toLowerCase()!=\'n\' && componentDisplayValue.compAppearUI.toLowerCase()!=\'n\'" ng-bind-html=componentDisplayValue.compDisplayValue[0]></p></div></td><td class=cov-subhead-val><p style=padding-left:10px; ng-repeat="componentDisplayValue in subGroup.components track by $index" ng-if="componentDisplayValue.compShowAsHelp.toLowerCase()!=\'y\' && componentDisplayValue.compAppearUI.toLowerCase()!=\'n\'" ng-bind-html=componentDisplayValue.compDisplayValue[0]></p></td></tr><tr></tr></tbody></table></div></div></div><div class=col-md-4 style=padding-left:45px;><div class=summary-quote style="padding:10px 20px;"><div class="quote-close pull-right"><!-- <span class="glyphicon glyphicon-ok-circle" style="font-size: 22px;">\n        </span> --> <img src="{{main.assetPath+\'checkmark.png\'}}"></div><br><br><p class=quote-title>Your Quote</p><div class=quote-value><p class=price>{{vm.quoteAmount}}</p><!-- <p class="quote-annually">annually</p> --></div><br><br><br><div class=quote-duration><p><b class=quote-coverageperiod>Policy Period</b></p><p class=quote-coverage-date>{{vm.startDate|date}} to {{vm.endDate|date}}</p></div></div><div class=summary-buttons><button type=submit class="btn btn-success btn-block policy-button" ng-click=vm.goToIssuePolicy()>Issue Policy</button><br><button type=button class="btn btn-block quote-btn" ui-sref=app.pricingandcoverage>Edit Quote</button><br><button type=button class="btn btn-block quote-btn" ng-click=vm.openComponentModal(othertc,proposalDesc)>Create Proposal</button></div></div></div>');
$templateCache.put('app/views/pricingandcoverage/nav.pricingandcoverage.html','<div style="min-height: 700px;"><!-- <span class="summary-leftnav" ng-if="quoteBindable"><br/><span class="selected-plan" style="display: inline-block;"></span><a class="list-group-item4" ui-sref="app.businessdetails">Business Details</a></span> --> <span class=summary-leftnav><br><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.businessdetails>Business Details</a></span> <span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.lob>Select lines of business</a></span> <span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.qualification>Qualification Questions</a></span><hr><h4 class=eligibility-panels>Business Owners Policy</h4><span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.quote>Start your quote</a></span> <span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.location>Locations and Buildings</a></span> <span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.additionalinterest>Additional Interests</a></span> <span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.underwriting>Underwriting Questions</a></span><ul class="nav list-group"><li style="padding-left: 5px;"><a class="list-group-item active" ui-sref=app.pricingandcoverage><i class="icon-home icon-1x"></i>Pricing and Coverage</a></li><li style="padding-left: 5px;"><a class="list-group-item locked" href=""><i class="icon-home icon-1x"></i>Premium Summary</a></li><li style="padding-left: 5px;"><a class="list-group-item locked" href=""><i class="icon-home icon-1x"></i>Before you issue policy</a></li></ul><div style="position: relative;bottom:0px;color: #909090;padding-left: 5px;">Copyright \xA9 2018 Utica Mutual Insurance Company, New Hartford, NY 13413 | All Rights Reserved</div></div>');
$templateCache.put('app/views/pricingandcoverage/pricingandcoverage.html','<div class="pricing-page pageBG"><script type=text/ng-template id=AddCoverageConfirm.html><!-- header -->\n      <div class="modal-header pricing-coveragepopup body-bg" style="border-bottom: 0px;padding-top: 7px;">\n        <button type="button" class="close pricing-coveragepopup popup-close" ng-click="$ctrl.onCancel()">&times;</button>\n        <p class="modal-title pricing-coveragepopup header-title" id="myModalLabel"><br/>Are you sure you want to remove this Coverage?<br/><br/></p>\n      </div>\n  \n        <!-- body -->\n        <!-- footer -->\n        <div class="modal-footer pricing-coveragepopup footer-bg">\n          <!--  <button type="button" class="btn btn-blue" ng-click="$ctrl.cancel()">Back</button>\n          <button type="button" class="btn btn-blue pricing-coveragepopup addCoverages-text" ng-click="$ctrl.onSubmit()">Next</button>\n        -->\n          <button class="btn btn-blue" ng-click="$ctrl.cancel()">Cancel</button>\n          <button type="submit" class="btn btn-blue" ng-disabled="pcQuestForm.$invalid" ng-click="$ctrl.ok()">Remove</button>\n        </div></script><script type=text/ng-template id=AddUWContent.html><!-- header -->\n    <div class="modal-header pricing-coveragepopup body-bg" style="border-bottom: 0px;padding-top: 7px;">\n      <button type="button" class="close pricing-coveragepopup popup-close" ng-click="$ctrl.onCancel()">&times;</button>\n      <p class="modal-title pricing-coveragepopup header-title" id="myModalLabel">{{$ctrl.coverageName}}\n        <br ng-if="$ctrl.followQuestObj.headers.header != null" />{{$ctrl.followQuestObj.headers.header}}</p>\n      <p class="pricing-coveragepopup header-content" ng-bind="$ctrl.followQuestObj.headers.instructionText"></p>\n    </div>\n    <form data-toggle="validator" class="formClass" form-submit-validation="" name="pcQuestForm" ng-submit="$ctrl.onSubmit()"\n      novalidate autocomplete="off">\n\n      <!-- body -->\n      <div class="modal-body pricing-coveragepopup body-bg">\n\n        <div class="row pricing-coveragepopup white-pannel" ng-init="parentIndex = $index">\n\n          <div ng-repeat="component in $ctrl.value.CoverageUWQuestions" class="col-md-12 pricing-coveragepopup popup-content" style="padding-left: 0px;padding-right: 0px;margin-left:-15px;margin-right:-15px;"\n            ng-if="component.appearUI.toLowerCase()!=\'n\' && component.appearUI!=null">\n            <div class="col-md-6" style="margin-left: 10px;margin-right: -10px;">{{component.datasetName}}</div>\n            <div class="col-md-6 text-center">\n              <widget editable="{{component.editable}}" ng-model="component.userValue" name="{{component.systemRefID}}" type="{{$ctrl.getWidgetType(component.controlName)}}"\n                classname="{{$ctrl.getWidgetClass(component.controlName)}}" labels="component.widgetCaptions" values="component.widgetValues"\n                widget-change="$ctrl.widgetChange(component)"></widget>\n\n\n              <!-- Required Validation -->\n              <p ng-show="pcQuestForm[{{component.systemRefID+\'_1_\'+parentIndex+\'_\'+$index}}].$invalid && pcQuestForm[{{component.systemRefID+\'_1_\'+parentIndex+\'_\'+$index}}].$touched && pcQuestForm.submitted"\n                class="col-md-12 help-block error build22" style="padding-left:39%;margin-top:-12px">\n                This field is required\n              </p>\n              <!--End Required Validation -->\n\n\n\n            </div>\n\n          </div>\n\n\n        </div>\n\n      </div>\n      <!-- footer -->\n      <div class="modal-footer pricing-coveragepopup footer-bg">\n        <!--  <button type="button" class="btn btn-blue" ng-click="$ctrl.cancel()">Back</button>\n          <button type="button" class="btn btn-blue pricing-coveragepopup addCoverages-text" ng-click="$ctrl.onSubmit()">Next</button>\n        -->\n        <button class="btn btn-blue" ng-click="$ctrl.onCancel()">Back</button>\n        <button type="submit" class="btn btn-blue" ng-disabled="pcQuestForm.$invalid">Continue</button>\n      </div>\n\n    </form></script><script type=text/ng-template id=AddFrequently.html><div class="modal-header" style="padding-bottom: 0px;">\n      <button type="button" class="close pricing-coveragepopup popup-close" ng-click="$ctrl.cancel()">&times;</button>\n      <h1 class="pricing-page modal-title header-title">Add a coverage</h1><br/>\n      <div class="pull-left" style="margin-left: 25px;display:none;">\n        <span class="pricing-coveragepopup freqCheckbox-wrap">\n          <input ng-model="$ctrl.selectedAll" ng-change="$ctrl.onSelectAll()" name="select_all_btn" id="select_all_btn" type="checkbox"\n            indeterminate>\n          <label style="font-size: 14px;" for="select_all_btn">Select All</label>\n        </span>\n      </div>\n\n    <!--  <div class="pricing-coveragepopup control-group clearfix search-box pull-right"> -->\n        <!--<div class="item">-->\n         <!-- <div class="pricing-coveragepopup search">\n            <p class="inputs">\n              <a href="javascript:;" class="icon-search"></a>\n              <input type="search" ng-model="$ctrl.searchValue" ng-change="$ctrl.onSearchChange()" placeholder="Search for a coverage">\n            </p>\n          </div>-->\n          <!-- end .search -->\n       <!-- </div>-->\n        <!-- end .item -->\n     <!-- </div> -->\n\n\n    </div>\n    <div class="modal-body pricing-coveragepopup body-bg">\n      <div>\n        <div ng-repeat="component in $ctrl.value | filter : $ctrl.searchValue" ng-init="parentIndex = $index">\n          <div class="row pricing-coveragepopup freqPopup-Item $ctrl.accordion" ng-class="{active:$ctrl.accordion==parentIndex}" style="padding-bottom: 23px;" >\n            <!--<span class="pricing-coveragepopup freqCheckbox-wrap">-->\n            <span class="pricing-coveragepopup" style="margin-left: 20px;" >  \n              <input ng-model="component.userValue" style="display:none;" name="freq_item_btn_{{parentIndex}}" ng-change="$ctrl.onMainChange(parentIndex)" id="freq_item_btn_{{parentIndex}}"\n                type="checkbox" value="{{component.datasetName}}">\n             <!-- <label style="font-size: 14px;" for="freq_item_btn_{{parentIndex}}" ng-bind="component.datasetName"></label>-->\n              <label style="font-size: 14px;"  ng-bind="component.datasetName"></label>\n\n            </span>\n            <span class="pricing-coveragepopup freqOpen-icon pull-right" style="margin-right: 30px;"   ng-click="($ctrl.accordion === parentIndex)? $ctrl.accordion = null : $ctrl.accordion = parentIndex"></span>\n          </div>\n          <div class="row pricing-coveragepopup freqPopup-subItem accordion-content" ng-repeat="subGroup in component.subGroups" ng-init="subIndex = $index"\n            ng-show="$ctrl.accordion==parentIndex">\n            <span class="pricing-coveragepopup freqCheckbox-wrap">\n              <input ng-model="subGroup.userValue" name="freq_item_btn_{{parentIndex}}_{{subIndex}}" ng-change="$ctrl.onSubChange(parentIndex,subGroup)"\n                id="freq_item_btn_{{parentIndex}}_{{subIndex}}" type="checkbox" value="{{subGroup.coverageName}}">\n              <label style="font-size: 14px;" for="freq_item_btn_{{parentIndex}}_{{subIndex}}" ng-bind="subGroup.coverageName"></label>\n            </span>\n          </div>\n        </div>\n      </div>\n\n\n    </div>\n    <div class="modal-footer pricing-coveragepopup footer-bg">\n      <button class="btn btn-blue" ng-click="$ctrl.cancel()">Back</button>\n      <button type="button" class="btn btn-blue" ng-click="$ctrl.submit()">Continue</button>\n    </div></script><!--start modal for create proposal--><script type=text/ng-template id=Createproposal.html><div class="modal-header premium-summary-modal">\n      <button type="button" class="close premium-summary-modal popup-close" ng-click="$ctrl.cancel()">&times;</button>\n      <h1 class="premium-summary-modal  modal-title header-title">Create Proposal</h1>\n    </div>\n    <div class="modal-body premium-summary-modal body-bg">\n      <div class="row">\n        <div class="col-md-6">\n          <h4>Terms & Conditions</h4>\n          <li>Subject to favorable loss control survey</li>\n          <li>Subject to loss or claim information verification</li>\n          <li>Subject to financial stability review</li>\n          <li>Subject to general risk information verification</li>\n        </div>\n        <div class="col-md-6">\n          <h6>\n            <b>OTHER TERMS & CONDITIONS</b>\n          </h6>\n          <textarea class="premium-summary-modal modaltextarea" rows="5" ng-model="othertc" placeholder="Other Terms & Conditions"></textarea>\n        </div>\n      </div>\n      <div>\n        <h4>Proposal Description</h4>\n        <textarea class="premium-summary-modal modaltextarea" rows="5" ng-model="proposalDesc" placeholder="Proposal description"></textarea>\n      </div>\n    </div>\n    <div class="modal-footer premium-summary-modal footer-bg">\n      <button class="btn btn-blue cancelbtn" ng-click="$ctrl.cancel()">Cancel</button>\n      <button type="button" class="btn btn-blue nextbtn" ng-click="$ctrl.generatepdf(othertc,proposalDesc)">Continue</button>\n    </div></script><!--end modal window for create proposal--><script type=text/ng-template id=AddCoverageContent.html><!-- header -->\n    <div class="modal-header pricing-coveragepopup body-bg" style="border-bottom: 0px;padding-top: 7px;">\n      <button type="button" class="close pricing-coveragepopup popup-close" ng-click="$ctrl.onCancel()">&times;</button>\n      <p class="modal-title pricing-coveragepopup header-title" id="myModalLabel">{{$ctrl.coverageName}}\n        <br ng-if="$ctrl.value.headers.header != null" />{{$ctrl.value.headers.header}}</p>\n      <p class="pricing-coveragepopup header-content" ng-bind="$ctrl.value.headers.instructionText"></p>\n    </div>\n    <form data-toggle="validator" class="formClass" form-submit-validation="" name="pcQuestForm" ng-submit="$ctrl.onSubmit()" novalidate autocomplete="off">\n\n      <!-- body -->\n      <div class="modal-body pricing-coveragepopup body-bg">\n\n        <div class="row pricing-coveragepopup white-pannel" ng-repeat="component in $ctrl.value.Policy.components" ng-init="parentIndex = $index">\n\n\n          <div class="col-md-12 pricing-coveragepopup popup-content" style="padding-left: 0px;padding-right: 0px;margin-left:-15px;margin-right:-15px;"\n            ng-if="component.appearUI.toLowerCase()!=\'n\' && component.appearUI!=null">\n            <div class="col-md-12 pricing-coveragepopup popup-smalltitle" ng-if="component.datasetName!=null && component.controlName.toLowerCase() ==\'header\'"\n              ng-bind="component.datasetName"></div>\n          </div>\n\n          <div class="col-md-{{component.screenColumns}} pricing-coveragepopup popup-content" style="padding-left: 0px;padding-right: 0px;margin-left:-15px;margin-right:-15px;"\n            ng-if="component.subComponents===null && component.appearUI.toLowerCase()!=\'n\' && component.appearUI!=null">\n            <div class="col-md-6" style="margin-left: 10px;margin-right: -10px;" ng-if="component.controlName != \'Check_Box\' && component.controlName != \'Date\'">{{component.datasetName}}</div>\n            <div class="col-md-6 text-center" ng-if="component.controlName != \'Check_Box\' && component.controlName != \'Date\'">\n              <widget editable="{{component.editable}}" ng-model="component.userValue" name="{{component.systemRefID+\'_1_\'+parentIndex+\'_\'+$index}}"\n                type="{{$ctrl.getWidgetType(component.controlName)}}" classname="{{$ctrl.getWidgetClass(component.controlName)}}"\n                labels="component.widgetCaptions" values="component.widgetValues" widget-change="$ctrl.widgetChange(component)"></widget>\n\n              <!-- Appian Validation-1 --\n                  <p ng-if="Object.keys(component).indexOf(\'validationError\') !== -1" ng-bind="component.validationError" class="col-md-12 help-block error build22" style="padding-left:39%;margin-top:-12px"></p>\n                  --End Appian Validation -->\n\n\n              <!-- Required Validation -->\n              <p ng-show="pcQuestForm[{{component.systemRefID+\'_1_\'+parentIndex+\'_\'+$index}}].$invalid && pcQuestForm[{{component.systemRefID+\'_1_\'+parentIndex+\'_\'+$index}}].$touched && pcQuestForm.submitted"\n                class="col-md-12 help-block error build22" style="padding-left:39%;margin-top:-12px">\n                This field is required\n              </p>\n              <!--End Required Validation -->\n\n            </div>\n\n            <div class="col-md-12" ng-if="component.controlName != \'Check_Box\' && component.controlName == \'Date\'">\n              <div class="col-md-6" style="margin-left: 10px;margin-right: -10px;"> {{component.datasetName}}</div>\n              <div class="col-md-6" style="margin-left: 0px;margin-top: -5px;">\n\n                <input placeholder="Please Select" id="{{component.systemRefID}}" show-button-bar="false" type="text" ng-disabled="component.editable.toLowerCase()===\'n\'"\n                  name="{{component.systemRefID}}" class="lob form-control dtpicker" uib-datepicker-popup="{{$ctrl.format}}"\n                  ng-model="component.userValue" ng-required="component.editable.toLowerCase()!==\'n\'" close-text="Close" datepicker-options="$ctrl.dateOptions"  datepicker-popup-template-url="app/partials/tpl.datepickerpopup.html"\n                  maxlength="10" date-val is-open="$ctrl.opened[component.systemRefID]" ng-click="$ctrl.open(component.systemRefID)"\n                />\n                <!-- Required Validation -->\n              <!--  <p ng-show="pcQuestForm[{{component.systemRefID+\'_1_\'+parentIndex+\'_\'+$index}}].$invalid && pcQuestForm[{{component.systemRefID+\'_1_\'+parentIndex+\'_\'+$index}}].$touched && pcQuestForm.submitted"\n                  class="col-md-12 help-block error build22" style="padding-left:39%;margin-top:-12px">\n                  This field is required\n                </p>-->\n                <!--End Required Validation -->\n                <!-- Appian Validation-2 -->\n               <!-- <p ng-if="Object.keys(component).indexOf(\'validationError\') !== -1" ng-bind="component.validationError" class="col-md-12 help-block error build22"\n                  style="padding-left:39%;margin-top:-12px"></p>-->\n                <!--End Appian Validation -->\n\n\n\n              </div>\n            </div>\n\n\n\n\n\n            <div class="col-md-12" ng-if="component.controlName == \'Check_Box\' && component.valueName== null && component.controlName != \'Date\'">\n              <div class="col-md-6" style="margin-left: 10px;margin-right: -10px;"> {{component.datasetName}}</div>\n              <div class="col-md-6" style="margin-left: 0px;margin-top: -28px;">\n                <widget editable="{{component.editable}}" ng-if="component.controlName == \'Check_Box\'&& component.controlName != \'Date\'"\n                  ng-model="component.userValue" name="{{component.systemRefID+\'_1_\'+parentIndex+\'_\'+$index}}" type="{{$ctrl.getWidgetType(component.controlName)}}"\n                  classname="{{$ctrl.getWidgetClass(component.controlName)}}" labels="[\'\']" values="[\'\']" widget-change="$ctrl.widgetChange(component)"></widget>\n\n                <!-- Required Validation -->\n                <p ng-show="pcQuestForm[{{component.systemRefID+\'_1_\'+parentIndex+\'_\'+$index}}].$invalid && pcQuestForm[{{component.systemRefID+\'_1_\'+parentIndex+\'_\'+$index}}].$touched && pcQuestForm.submitted"\n                  class="col-md-12 help-block error build22" style="padding-left:39%;margin-top:-12px">\n                  This field is required\n                </p>\n                <!--End Required Validation -->\n                <!-- Appian Validation-3 -->\n                <p ng-if="Object.keys(component).indexOf(\'validationError\') !== -1" ng-bind="component.validationError" class="col-md-12 help-block error build22"\n                  style="padding-left:39%;margin-top:-12px"></p>\n                <!--End Appian Validation -->\n\n\n              </div>\n            </div>\n\n\n\n            <div class="col-md-12" ng-if="component.controlName == \'Check_Box\' && component.valueName!= null && component.controlName != \'Date\'">{{component.datasetName}}\n              <br/>\n              <br/>\n            </div>\n            <widget editable="{{component.editable}}" ng-if="component.controlName == \'Check_Box\' && component.controlName != \'Date\'"\n              ng-model="component.userValue" name="{{component.systemRefID+\'_1_\'+parentIndex+\'_\'+$index}}" type="{{$ctrl.getWidgetType(component.controlName)}}"\n              classname="{{$ctrl.getWidgetClass(component.controlName)}}" labels="component.widgetCaptions" values="component.widgetValues"\n              widget-change="$ctrl.widgetChange(component)"></widget>\n\n            <!-- Required Validation -->\n            <p ng-show="pcQuestForm[{{component.systemRefID+\'_1_\'+parentIndex+\'_\'+$index}}].$invalid && pcQuestForm[{{component.systemRefID+\'_1_\'+parentIndex+\'_\'+$index}}].$touched && pcQuestForm.submitted"\n              class="col-md-12 help-block error build22" style="padding-left:39%;margin-top:-12px">\n              This field is required\n            </p>\n            <!--End Required Validation -->\n            <!-- Appian Validation-4 -->\n            <p ng-if="Object.keys(component).indexOf(\'validationError\') !== -1" ng-bind="component.validationError" class="col-md-12 help-block error build22"\n              style="padding-left:39%;margin-top:10px"></p>\n            <!--End Appian Validation -->\n\n\n          </div>\n\n\n\n          <div class="col-md-{{subComponent.screenColumns}} pricing-coveragepopup popup-content" style="padding-left: 0px;padding-right: 0px;margin-left:-15px;margin-right:-15px;"\n            ng-repeat="subComponent in component.subComponents track by $index" ng-if="subComponent.appearUI.toLowerCase()!=\'n\' && subComponent.appearUI!=null">\n            <div class="col-md-6" style="margin-left: 10px;margin-right: -10px;" ng-if="subComponent.controlName != \'Check_Box\' && component.controlName != \'Date\'">{{subComponent.datasetName}}</div>\n            <div class="col-md-6 text-center" ng-if="subComponent.controlName != \'Check_Box\' && component.controlName != \'Date\'">\n              <widget editable="{{subComponent.editable}}" ng-model="subComponent.userValue" name="{{subComponent.systemRefID+\'_1_\'+parentIndex+\'_\'+$index}}"\n                type="{{$ctrl.getWidgetType(subComponent.controlName)}}" classname="{{$ctrl.getWidgetClass(subComponent.controlName)}}"\n                labels="subComponent.widgetCaptions" values="subComponent.widgetValues" widget-change="$ctrl.widgetChange(subComponent)"></widget>\n\n              <!-- Required Validation -->\n              <p ng-show="pcQuestForm[{{subComponent.systemRefID+\'_1_\'+parentIndex+\'_\'+$index}}].$invalid && pcQuestForm[{{subComponent.systemRefID+\'_1_\'+parentIndex+\'_\'+$index}}].$touched && pcQuestForm.submitted"\n                class="col-md-12 help-block error build22" style="padding-left:39%;margin-top:-12px">\n                This field is required\n              </p>\n              <!--End Required Validation -->\n\n              <!-- Appian Validation-5 -->\n              <p ng-if="Object.keys(subComponent).indexOf(\'validationError\') !== -1" ng-bind="subComponent.validationError" class="col-md-12 help-block error build22"\n                style="padding-left:39%;margin-top:-12px"></p>\n              <!--End Appian Validation -->\n\n\n            </div>\n\n            <div class="col-md-12" ng-if="subComponent.controlName != \'Check_Box\' && subComponent.controlName == \'Date\'">\n              <div class="col-md-6" style="margin-left: 10px;margin-right: -10px;"> {{subComponent.datasetName}}</div>\n              <div class="col-md-6" style="margin-left: 0px;margin-top: -5px;">\n\n                <input placeholder="Please Select" id="{{subComponent.systemRefID}}" show-button-bar="false" type="text" ng-disabled="component.editable.toLowerCase()===\'n\'"\n                  name="{{component.systemRefID}}" class="lob form-control dtpicker" uib-datepicker-popup="{{$ctrl.format}}"\n                  ng-model="subComponent.userValue" ng-required="true" close-text="Close" datepicker-popup-template-url="app/partials/tpl.datepickerpopup.html"\n                  maxlength="10" date-val is-open="$ctrl.opened[subComponent.systemRefID]" ng-click="$ctrl.open(subComponent.systemRefID)"\n                />\n                <!-- Required Validation -->\n                <p ng-show="pcQuestForm[{{subComponent.systemRefID+\'_1_\'+parentIndex+\'_\'+$index}}].$invalid && pcQuestForm[{{subComponent.systemRefID+\'_1_\'+parentIndex+\'_\'+$index}}].$touched && pcQuestForm.submitted"\n                  class="col-md-12 help-block error build22" style="padding-left:39%;margin-top:-12px">\n                  This field is required\n                </p>\n                <!--End Required Validation -->\n\n                <!-- Appian Validation-6 -->\n                <p ng-if="Object.keys(subComponent).indexOf(\'validationError\') !== -1" ng-bind="subComponent.validationError" class="col-md-12 help-block error build22"\n                  style="padding-left:39%;margin-top:-12px"></p>\n                <!--End Appian Validation -->\n\n              </div>\n            </div>\n\n\n\n            <div class="col-md-12" ng-if="subComponent.controlName == \'Check_Box\' && subComponent.valueName== null && subComponent.controlName != \'Date\'">\n              <div class="col-md-6" style="margin-left: 10px;margin-right: -10px;"> {{component.datasetName}}</div>\n              <div class="col-md-6" style="margin-left: 0px;margin-top: -28px;">\n                <widget editable="{{subComponent.editable}}" ng-if="subComponent.controlName == \'Check_Box\'&& subComponent.controlName != \'Date\'"\n                  ng-model="subComponent.userValue" name="{{subComponent.systemRefID+\'_1_\'+parentIndex+\'_\'+$index}}" type="{{$ctrl.getWidgetType(subComponent.controlName)}}"\n                  classname="{{$ctrl.getWidgetClass(subComponent.controlName)}}" labels="[\'\']" values="[\'\']" widget-change="$ctrl.widgetChange(subComponent)"></widget>\n\n                <!-- Required Validation -->\n                <p ng-show="pcQuestForm[{{subComponent.systemRefID+\'_1_\'+parentIndex+\'_\'+$index}}].$invalid && pcQuestForm[{{subComponent.systemRefID+\'_1_\'+parentIndex+\'_\'+$index}}].$touched && pcQuestForm.submitted"\n                  class="col-md-12 help-block error build22" style="padding-left:39%;margin-top:-12px">\n                  This field is required\n                </p>\n                <!--End Required Validation -->\n\n                <!-- Appian Validation-7 -->\n                <p ng-if="Object.keys(subComponent).indexOf(\'validationError\') !== -1" ng-bind="subComponent.validationError" class="col-md-12 help-block error build22"\n                  style="padding-left:39%;margin-top:-12px"></p>\n                <!--End Appian Validation -->\n              </div>\n            </div>\n\n\n\n\n            <div class="col-md-12" ng-if="subComponent.controlName == \'Check_Box\' && component.controlName != \'Date\'">{{subComponent.datasetName}}\n              <br/>\n              <br/>\n            </div>\n            <widget editable="{{subComponent.editable}}" ng-if="subComponent.controlName == \'Check_Box\' && component.controlName != \'Date\'"\n              ng-model="subComponent.userValue" name="{{subComponent.systemRefID+\'_1_\'+parentIndex+\'_\'+$index}}" type="{{$ctrl.getWidgetType(subComponent.controlName)}}"\n              classname="{{$ctrl.getWidgetClass(subComponent.controlName)}}" labels="subComponent.widgetCaptions" values="subComponent.widgetValues"\n              widget-change="$ctrl.widgetChange(subComponent)"></widget>\n\n            <!-- Required Validation -->\n            <p ng-show="pcQuestForm[{{subComponent.systemRefID+\'_1_\'+parentIndex+\'_\'+$index}}].$invalid && pcQuestForm[{{subComponent.systemRefID+\'_1_\'+parentIndex+\'_\'+$index}}].$touched && pcQuestForm.submitted"\n              class="col-md-12 help-block error build22" style="padding-left:39%;margin-top:-12px">\n              This field is required\n            </p>\n            <!--End Required Validation -->\n            <!-- Appian Validation-8 -->\n            <p ng-if="Object.keys(subComponent).indexOf(\'validationError\') !== -1" ng-bind="subComponent.validationError" class="col-md-12 help-block error build22"\n              style="padding-left:39%;margin-top:-12px"></p>\n            <!--End Appian Validation -->\n\n\n          </div>\n\n        </div>\n\n\n\n\n\n\n\n\n\n        <div ng-repeat="location in $ctrl.value.Policy.locations" ng-if="$ctrl.value.Policy.locations!=null" ng-init="locationIndex = $index" >\n          <div class="row pricing-coveragepopup white-pannel" ng-repeat="component in location.components" ng-init="parentIndex = $index">\n\n\n            <div ng-if="parentIndex===0" class="col-md-12 pricing-coveragepopup popup-content" style="padding-left: 0px;padding-right: 0px;margin-left:-15px;margin-right:-15px;">\n              <div class="col-md-6" style="margin-left: 10px;margin-right: -10px;">Location</div>\n              <div class="col-md-6" ng-bind="location.locationName"></div>\n            </div>\n\n\n\n            <div class="col-md-12 pricing-coveragepopup popup-content" style="padding-left: 0px;padding-right: 0px;margin-left:-15px;margin-right:-15px;"\n              ng-if="component.appearUI.toLowerCase()!=\'n\' && component.appearUI!=null">\n              <div class="col-md-12 pricing-coveragepopup popup-smalltitle" ng-if="component.datasetName!=null && component.controlName.toLowerCase() ==\'header\'"\n                ng-bind="component.datasetName"></div>\n            </div>\n\n            <div class="col-md-{{component.screenColumns}} pricing-coveragepopup popup-content" style="padding-left: 0px;padding-right: 0px;margin-left:-15px;margin-right:-15px;"\n              ng-if="component.subComponents===null && component.appearUI.toLowerCase()!=\'n\' && component.appearUI!=null">\n              <div class="col-md-6" style="margin-left: 10px;margin-right: -10px;" ng-if="component.controlName != \'Check_Box\'&& component.controlName != \'Date\'">{{component.datasetName}}</div>\n              <div class="col-md-6 text-center" ng-if="component.controlName != \'Check_Box\' && component.controlName != \'Date\'">\n                <widget editable="{{component.editable}}" ng-model="component.userValue" name="{{component.systemRefID+\'_2_\'+parentIndex+\'_\'+$index}}"\n                  type="{{$ctrl.getWidgetType(component.controlName)}}" classname="{{$ctrl.getWidgetClass(component.controlName)}}"\n                  labels="component.widgetCaptions" values="component.widgetValues" widget-change="$ctrl.widgetChange(component)"></widget>\n\n                <!-- Required Validation -->\n                <p ng-show="pcQuestForm[{{component.systemRefID+\'_2_\'+parentIndex+\'_\'+$index}}].$invalid && pcQuestForm[{{component.systemRefID+\'_2_\'+parentIndex+\'_\'+$index}}].$touched && pcQuestForm.submitted"\n                  class="col-md-12 help-block error build22" style="padding-left:39%;margin-top:-12px">\n                  This field is required\n                </p>\n                <!--End Required Validation -->\n                <!-- Appian Validation-9 -->\n                <p ng-if="Object.keys(component).indexOf(\'validationError\') !== -1" ng-bind="component.validationError" class="col-md-12 help-block error build22"\n                  style="padding-left:39%;margin-top:-12px"></p>\n                <!--End Appian Validation -->\n\n\n              </div>\n              <div class="col-md-12" ng-if="component.controlName == \'Check_Box\' && component.controlName != \'Date\'">{{component.datasetName}}\n                <br/>\n                <br/>\n              </div>\n              <widget editable="{{component.editable}}" ng-if="component.controlName == \'Check_Box\' && component.controlName != \'Date\'"\n                ng-model="component.userValue" name="{{component.systemRefID+\'_2_\'+parentIndex+\'_\'+$index}}" type="{{$ctrl.getWidgetType(component.controlName)}}"\n                classname="{{$ctrl.getWidgetClass(component.controlName)}}" labels="component.widgetCaptions" values="component.widgetValues"\n                widget-change="$ctrl.widgetChange(component)"></widget>\n\n              <!-- Required Validation -->\n              <p ng-show="pcQuestForm[{{component.systemRefID+\'_2_\'+parentIndex+\'_\'+$index}}].$invalid && pcQuestForm[{{component.systemRefID+\'_2_\'+parentIndex+\'_\'+$index}}].$touched && pcQuestForm.submitted"\n                class="col-md-12 help-block error build22" style="padding-left:39%;margin-top:-12px">\n                This field is required\n              </p>\n              <!--End Required Validation -->\n\n              <!-- Appian Validation-10 -->\n              <p ng-if="Object.keys(component).indexOf(\'validationError\') !== -1" ng-bind="component.validationError" class="col-md-12 help-block error build22"\n                style="padding-left:39%;margin-top:-12px"></p>\n              <!--End Appian Validation -->\n\n            </div>\n\n\n\n            <div class="col-md-{{subComponent.screenColumns}} pricing-coveragepopup popup-content" style="padding-left: 0px;padding-right: 0px;margin-left:-15px;margin-right:-15px;"\n              ng-repeat="subComponent in component.subComponents track by $index" ng-if="subComponent.appearUI.toLowerCase()!=\'n\' && subComponent.appearUI!=null">\n              <div class="col-md-6" style="margin-left: 10px;margin-right: -10px;" ng-if="subComponent.controlName != \'Check_Box\' && component.controlName != \'Date\'">{{subComponent.datasetName}}</div>\n              <div class="col-md-6 text-center" ng-if="subComponent.controlName != \'Check_Box\' && component.controlName != \'Date\'">\n                <widget editable="{{subComponent.editable}}" ng-model="subComponent.userValue" name="{{subComponent.systemRefID+\'_2_\'+parentIndex+\'_\'+$index}}"\n                  type="{{$ctrl.getWidgetType(subComponent.controlName)}}" classname="{{$ctrl.getWidgetClass(subComponent.controlName)}}"\n                  labels="subComponent.widgetCaptions" values="subComponent.widgetValues" widget-change="$ctrl.widgetChange(subComponent)"></widget>\n\n                <!-- Required Validation -->\n                <p ng-show="pcQuestForm[{{subComponent.systemRefID+\'_2_\'+parentIndex+\'_\'+$index}}].$invalid && pcQuestForm[{{subComponent.systemRefID+\'_2_\'+parentIndex+\'_\'+$index}}].$touched && pcQuestForm.submitted"\n                  class="col-md-12 help-block error build22" style="padding-left:39%;margin-top:-12px">\n                  This field is required\n                </p>\n                <!--End Required Validation -->\n\n                <!-- Appian Validation-11 -->\n                <p ng-if="Object.keys(subComponent).indexOf(\'validationError\') !== -1" ng-bind="subComponent.validationError" class="col-md-12 help-block error build22"\n                  style="padding-left:39%;margin-top:-12px"></p>\n                <!--End Appian Validation -->\n\n              </div>\n              <div class="col-md-12" ng-if="subComponent.controlName == \'Check_Box\' && component.controlName != \'Date\'">{{subComponent.datasetName}}\n                <br/>\n                <br/>\n              </div>\n              <widget editable="{{subComponent.editable}}" ng-if="subComponent.controlName == \'Check_Box\' && component.controlName != \'Date\'"\n                ng-model="subComponent.userValue" name="{{subComponent.systemRefID+\'_2_\'+parentIndex+\'_\'+$index}}" type="{{$ctrl.getWidgetType(subComponent.controlName)}}"\n                classname="{{$ctrl.getWidgetClass(subComponent.controlName)}}" labels="subComponent.widgetCaptions" values="subComponent.widgetValues"\n                widget-change="$ctrl.widgetChange(subComponent)"></widget>\n\n              <!-- Required Validation -->\n              <p ng-show="pcQuestForm[{{subComponent.systemRefID+\'_2_\'+parentIndex+\'_\'+$index}}].$invalid && pcQuestForm[{{subComponent.systemRefID+\'_2_\'+parentIndex+\'_\'+$index}}].$touched && pcQuestForm.submitted"\n                class="col-md-12 help-block error build22" style="padding-left:39%;margin-top:-12px">\n                This field is required\n              </p>\n              <!--End Required Validation -->\n\n              <!-- Appian Validation-12 -->\n              <p ng-if="Object.keys(subComponent).indexOf(\'validationError\') !== -1" ng-bind="subComponent.validationError" class="col-md-12 help-block error build22"\n                style="padding-left:39%;margin-top:-12px"></p>\n              <!--End Appian Validation -->\n\n            </div>\n\n          </div>\n          <div class="pricing-coveragepopup white-pannel">\n\n            <div ng-repeat="building in location.Building" ng-init="buildingIndex = $index">\n              <div class="row pricing-coveragepopup popup-content" style="padding-left: 0px;padding-right: 0px;margin-left:-15px;margin-right:-15px;">\n\n                <div ng-if="buildingIndex===0 && location.components===null" class="col-md-12 pricing-coveragepopup popup-content" style="padding-left: 0px;padding-right: 0px;margin-left:-15px;margin-right:-15px;">\n                  <div class="col-md-6" style="margin-left: 10px;margin-right: -10px;">Location</div>\n                  <div class="col-md-6" ng-bind="location.locationName"></div>\n                </div>\n\n                <!--<div class="col-md-6" style="margin-left: 10px;margin-right: -10px;" ng-bind="location.Building[0].buildingName" ></div>-->\n                <div class="col-md-6" style="margin-left: 10px;margin-right: -10px;">Building {{buildingIndex+1}}</div>\n                <div class="col-md-1 pricing-coveragepopup checkbox-wrap" style="margin-top: -30px;">\n                  <input ng-model="building.userValue" required="required" name="building_{{locationIndex}}_{{buildingIndex}}" type="checkbox"\n                    id="building_{{locationIndex}}_{{buildingIndex}}">\n                  <label for="building_{{locationIndex}}_{{buildingIndex}}"></label>\n                </div>\n              </div>\n\n              <div class="row" style="padding-left: 0px;padding-right: 0px;margin-left:0px;margin-right:0px;" ng-if="building.userValue===true"\n                ng-repeat="component in building.components" ng-init="parentIndex = $index">\n\n\n\n\n\n                <div class="col-md-12 pricing-coveragepopup popup-content" style="padding-left: 0px;padding-right: 0px;margin-left:-15px;margin-right:-15px;"\n                  ng-if="component.appearUI.toLowerCase()!=\'n\' && component.appearUI!=null">\n                  <div class="col-md-12 pricing-coveragepopup popup-smalltitle" ng-if="component.datasetName!=null && component.controlName.toLowerCase() ==\'header\'"\n                    ng-bind="component.datasetName"></div>\n                </div>\n\n                <div class="col-md-{{component.screenColumns}} pricing-coveragepopup popup-content" style="padding-left: 0px;padding-right: 0px;margin-left:-15px;margin-right:-15px;"\n                  ng-if="component.subComponents===null && component.appearUI.toLowerCase()!=\'n\' && component.appearUI!=null">\n                  <div class="col-md-6" style="margin-left: 10px;margin-right: -10px;" ng-if="component.controlName != \'Check_Box\'&& component.controlName != \'Date\'">{{component.datasetName}}</div>\n                  <div class="col-md-6 text-center" ng-if="component.controlName != \'Check_Box\' && component.controlName != \'Date\'">\n                    <widget editable="{{component.editable}}" ng-model="component.userValue" name="{{\'building_\'+buildingIndex+component.systemRefID+\'_2_\'+parentIndex+\'_\'+$index}}"\n                      type="{{$ctrl.getWidgetType(component.controlName)}}" classname="{{$ctrl.getWidgetClass(component.controlName)}}"\n                      labels="component.widgetCaptions" values="component.widgetValues" widget-change="$ctrl.widgetChange(component)"></widget>\n\n                    <!-- Required Validation -->\n                    <p ng-show="pcQuestForm[{{component.systemRefID+\'_2_\'+parentIndex+\'_\'+$index}}].$invalid && pcQuestForm[{{component.systemRefID+\'_2_\'+parentIndex+\'_\'+$index}}].$touched && pcQuestForm.submitted"\n                      class="col-md-12 help-block error build22" style="padding-left:39%;margin-top:-12px">\n                      This field is required\n                    </p>\n                    <!--End Required Validation -->\n\n                    <!-- Appian Validation-13 -->\n                    <p ng-if="Object.keys(component).indexOf(\'validationError\') !== -1" ng-bind="component.validationError" class="col-md-12 help-block error build22"\n                      style="padding-left:39%;margin-top:-12px"></p>\n                    <!--End Appian Validation -->\n\n                  </div>\n                  <div class="col-md-12" ng-if="component.controlName == \'Check_Box\' && component.controlName != \'Date\'">{{component.datasetName}}\n                    <br/>\n                    <br/>\n                  </div>\n                  <widget editable="{{component.editable}}" ng-if="component.controlName == \'Check_Box\' && component.controlName != \'Date\'"\n                    ng-model="component.userValue" name="{{\'building_\'+buildingIndex+component.systemRefID+\'_2_\'+parentIndex+\'_\'+$index}}"\n                    type="{{$ctrl.getWidgetType(component.controlName)}}" classname="{{$ctrl.getWidgetClass(component.controlName)}}"\n                    labels="component.widgetCaptions" values="component.widgetValues" widget-change="$ctrl.widgetChange(component)"></widget>\n\n                  <!-- Required Validation -->\n                  <p ng-show="pcQuestForm[{{component.systemRefID+\'_2_\'+parentIndex+\'_\'+$index}}].$invalid && pcQuestForm[{{component.systemRefID+\'_2_\'+parentIndex+\'_\'+$index}}].$touched && pcQuestForm.submitted"\n                    class="col-md-12 help-block error build22" style="padding-left:39%;margin-top:-12px">\n                    This field is required\n                  </p>\n                  <!--End Required Validation -->\n\n                  <!-- Appian Validation-14 -->\n                  <p ng-if="Object.keys(component).indexOf(\'validationError\') !== -1" ng-bind="component.validationError" class="col-md-12 help-block error build22"\n                    style="padding-left:39%;margin-top:-12px"></p>\n                  <!--End Appian Validation -->\n\n                </div>\n\n\n\n                <div class="col-md-{{subComponent.screenColumns}} pricing-coveragepopup popup-content" style="padding-left: 0px;padding-right: 0px;margin-left:-15px;margin-right:-15px;"\n                  ng-repeat="subComponent in component.subComponents track by $index" ng-if="subComponent.appearUI.toLowerCase()!=\'n\' && subComponent.appearUI!=null">\n                  <div class="col-md-6" style="margin-left: 10px;margin-right: -10px;" ng-if="subComponent.controlName != \'Check_Box\' && component.controlName != \'Date\'">{{subComponent.datasetName}}</div>\n                  <div class="col-md-6 text-center" ng-if="subComponent.controlName != \'Check_Box\' && component.controlName != \'Date\'">\n                    <widget editable="{{subComponent.editable}}" ng-model="subComponent.userValue" name="{{\'building_\'+buildingIndex+subComponent.systemRefID+\'_2_\'+parentIndex+\'_\'+$index}}"\n                      type="{{$ctrl.getWidgetType(subComponent.controlName)}}" classname="{{$ctrl.getWidgetClass(subComponent.controlName)}}"\n                      labels="subComponent.widgetCaptions" values="subComponent.widgetValues" widget-change="$ctrl.widgetChange(subComponent)"></widget>\n\n                    <!-- Required Validation -->\n                    <p ng-show="pcQuestForm[{{subComponent.systemRefID+\'_2_\'+parentIndex+\'_\'+$index}}].$invalid && pcQuestForm[{{subComponent.systemRefID+\'_2_\'+parentIndex+\'_\'+$index}}].$touched && pcQuestForm.submitted"\n                      class="col-md-12 help-block error build22" style="padding-left:39%;margin-top:-12px">\n                      This field is required\n                    </p>\n                    <!--End Required Validation -->\n\n                    <!-- Appian Validation-15 -->\n                    <p ng-if="Object.keys(subComponent).indexOf(\'validationError\') !== -1" ng-bind="subComponent.validationError" class="col-md-12 help-block error build22"\n                      style="padding-left:39%;margin-top:-12px"></p>\n                    <!--End Appian Validation -->\n\n                  </div>\n                  <div class="col-md-12" ng-if="subComponent.controlName == \'Check_Box\' && component.controlName != \'Date\'">{{subComponent.datasetName}}\n                    <br/>\n                    <br/>\n                  </div>\n                  <widget editable="{{subComponent.editable}}" ng-if="subComponent.controlName == \'Check_Box\' && component.controlName != \'Date\'"\n                    ng-model="subComponent.userValue" name="{{\'building_\'+buildingIndex+subComponent.systemRefID+\'_2_\'+parentIndex+\'_\'+$index}}"\n                    type="{{$ctrl.getWidgetType(subComponent.controlName)}}" classname="{{$ctrl.getWidgetClass(subComponent.controlName)}}"\n                    labels="subComponent.widgetCaptions" values="subComponent.widgetValues" widget-change="$ctrl.widgetChange(subComponent)"></widget>\n\n                  <!-- Required Validation -->\n                  <p ng-show="pcQuestForm[{{subComponent.systemRefID+\'_2_\'+parentIndex+\'_\'+$index}}].$invalid && pcQuestForm[{{subComponent.systemRefID+\'_2_\'+parentIndex+\'_\'+$index}}].$touched && pcQuestForm.submitted"\n                    class="col-md-12 help-block error build22" style="padding-left:39%;margin-top:-12px">\n                    This field is required\n                  </p>\n                  <!--End Required Validation -->\n\n                  <!-- Appian Validation-16 -->\n                  <p ng-if="Object.keys(subComponent).indexOf(\'validationError\') !== -1" ng-bind="subComponent.validationError" class="col-md-12 help-block error build22"\n                    style="padding-left:39%;margin-top:-12px"></p>\n                  <!--End Appian Validation -->\n\n                </div>\n\n              </div>\n            </div>\n          </div>\n\n        </div>\n      </div>\n      <!-- footer -->\n      <div class="modal-footer pricing-coveragepopup footer-bg">\n        <!--  <button type="button" class="btn btn-blue" ng-click="$ctrl.cancel()">Back</button>\n        <button type="button" class="btn btn-blue pricing-coveragepopup addCoverages-text" ng-click="$ctrl.onSubmit()">Next</button>\n      -->\n        <button class="btn btn-blue" ng-click="$ctrl.onCancel()">Back</button>\n        <button type="submit" class="btn btn-blue" ng-disabled="pcQuestForm.$invalid" >Continue</button>\n      </div>\n\n    </form></script><form class="formClass pricing-page user-formClass" ng-if="vm.pricingData.length != 0" name=userForm ng-submit=submitForm() novalidate><div class=row style=background-color:#fff><br><br><p class="pricing-page heading">Pricing &amp; Coverages</p><span ng-if=!vm.serverError><div class="col-md-11 error card" ng-if="vm.pricingData[vm.planIndex].AgencyValid.toLowerCase() === \'false\'" style="left: 2%;width: 93%;"><ul class=list-inline style="margin-top: 10px;"><li class=col-md-1 style="padding: 2%;"><img width=34px height=28px ng-src="{{main.assetPath+\'yellow_warning.png\'}}"></li><li style="padding: 0;" class=col-md-11>Our records indicate your agency and producer may not have the proper licensing / appointment for the risk state(s). All proper state licensing and / or appointments are required prior to policy issuance. Please contact our Licensing Department by email at HO.Marketing.LicenseControls@uticanational.com<br>You may also contact us by phone between 7:30 AM and 5:00 PM EST</li></ul></div><div class="col-md-11 error card" ng-if="vm.pricingData[vm.planIndex].messages.length > 1" style="left: 2%;width: 93%;"><ul class=list-inline style="margin-top: 10px;"><li class=col-md-1 style="padding: 2%;"><img width=34px height=28px ng-src="{{main.assetPath+\'yellow_warning.png\'}}"></li><li class=col-md-11 style="padding: 0;"><p style="margin-bottom: 0px;" ng-repeat="message in vm.pricingData[vm.planIndex].messages" ng-if="$index > 0" ng-bind=message.value></p></li></ul></div><!-- <div class="col-md-11 error card" ng-if="vm.pricingData[vm.planIndex].messages.length > 1" style="left: 2%;width: 93%;">\n          <img width="34px" height="28px" ng-src="{{main.assetPath+\'yellow_warning.png\'}}">\n          <p ng-repeat="message in vm.pricingData[vm.planIndex].messages" ng-bind="message.value"></p>\n        </div>\n      --><div class=col-md-5><div class="row pricing-page components-heading"></div><div class="row pricing-page component-container" ng-repeat="component in vm.pricingData[vm.planIndex].groups" ng-init="parentIndex = $index"><div class="col-md-12 pricing-page group-container"><span class="blue-link pricing-page group-number">{{$index+1}}</span> <span class="pricing-page group-name">{{component.datasetName}}&nbsp; <a ng-if="component.toolTip !== null && component.toolTip !== undefined" data-toggle=tooltip data-placement=right title={{component.datasetName}}><img width=15px height=15px ng-src="{{main.assetPath+\'icon-more-info.png\'}}"></a></span></div><div class="row pricing-page subGroup-container" ng-if="subGroup.appearUI.toLowerCase()!=\'n\' && subGroup.appearUI!=null" ng-repeat="subGroup in component.subGroups | filter: {appearUI: \'Y\'} |filter: {appearUI: \'y\'}" ng-init="subIndex = $index" ng-if="component.subGroups !== null && component.subGroups.length !== 0"><div class="col-md-12 pricing-page subGroup-name" ng-style=vm.getGroupRowHeight(parentIndex,subIndex)><span class=col-md-1 style=padding-left:13px>{{parentIndex+1}}.{{$index+1}}</span> <span class=col-md-10 style=padding-left:10px ng-bind-html=subGroup.coverageName></span><!-- <ul class="list-inline">\n                      <li class="list-inline-item" style="padding-left:13px">{{parentIndex+1}}.{{$index+1}}</li>\n                      <li class="list-inline-item" style="padding-left:10px;padding-right: 25px;" ng-bind-html="subGroup.coverageName"></li>\n                    </ul>\n                    <br/>--><p class=col-md-10 style="padding-left:45px;opacity: 0.6;" ng-if="componentDisplayValue.compShowAsHelp.toLowerCase()!=\'n\' && componentDisplayValue.compAppearUI.toLowerCase()!=\'n\'" ng-repeat="componentDisplayValue in subGroup.components track by $index" ng-bind-html=componentDisplayValue.compDisplayValue[0]></p></div></div></div></div><div class="col-md-2 pricing-page" ng-class=vm.stdPlanClass><div class="row pricing-page checkradio-wrap pricing-page package-heading"><input id=0 type=radio ng-model=vm.stdPlanSelected ng-change=vm.onStdPlan() value=true ng-disabled=vm.stdRadioDisabled checked> <label for=0 class="pricing-page package-name" ng-class=vm.stdPlanSelectedTxt>{{vm.pricingData[0].packageName}}</label></div><div class=row style=margin-top:10px ng-repeat="component in vm.pricingData[0].groups" ng-init="parentIndex = $index"><div class=col-md-12 ng-if="component.subGroups === null"><div class="row pricing-page components-value" ng-repeat="componentDisplayValue in component.components track by $index">{{componentDisplayValue.compDisplayValue[0]}}<br></div></div><div class="col-md-12 pricing-page components-value" ng-if="component.subGroups !== null"><br></div><div class="row pricing-page subGroup-value-cont" ng-if="subGroup.appearUI.toLowerCase()!=\'n\' && subGroup.appearUI!=null" ng-repeat="subGroup in component.subGroups | filter: {appearUI: \'Y\'} |filter: {appearUI: \'y\'}" ng-init="subIndex = $index" ng-if="component.subGroups !== null && component.subGroups.length !== 0"><div class="col-md-12 pricing-page subGroup-value" ng-style=vm.getGroupRowHeight(parentIndex,subIndex)><p style=word-wrap:break-word; ng-if="componentDisplayValue.compShowAsHelp.toLowerCase()!=\'y\' && componentDisplayValue.compAppearUI.toLowerCase()!=\'n\'" ng-repeat="componentDisplayValue in subGroup.components track by $index" ng-bind-html=componentDisplayValue.compDisplayValue[0]></p></div></div></div></div><div class="col-md-2 pricing-page" ng-class=vm.tldPlanClass><div class="row pricing-page checkradio-wrap-disabled pricing-page package-heading"><input id=1 type=radio ng-model=vm.tldPlanSelected ng-change=vm.onTldPlan() value=true> <label for=1 class="pricing-page package-name" ng-class=vm.tldPlanSelectedTxt>{{vm.pricingData[1].packageName}}</label></div><div class=row style=margin-top:10px ng-repeat="component in vm.pricingData[1].groups | limitTo : vm.pricingData[vm.planIndex].groups.length" ng-init="parentIndex = $index"><div class=col-md-12 ng-if="component.subGroups === null"><div ng-if="vm.planIndex === 1" class="row pricing-page components-value" ng-repeat="componentDisplayValue in component.components track by $index">{{componentDisplayValue.compDisplayValue[0]}}<div class=pull-right style="margin-right: 15px;"><span ng-click="(component.editable.toLowerCase() ===\'y\') ? vm.openComponentModal(component) : null" class="pricing-page coverageGrpBtns coverageGrpEdit fa fa-pencil" ng-style="{{component.editable.toLowerCase()}}===\'y\' ? {\'opacity\':\'1\'} : {\'opacity\':\'0.5\'}"></span> <span ng-click=vm.onFreqDelete(parentIndex,null) ng-if="component.frequentlyAddedCov.toLowerCase()===\'y\'" class="pricing-page coverageGrpBtns coverageGrpDelete fa fa-trash"></span> <span ng-if="component.mandatoryCoverage.toLowerCase()===\'y\'" class="pricing-coveragepopup tailCheckbox-wrap" ng-style="{{component.editable.toLowerCase()}}===\'y\' ? {\'opacity\':\'1\'} : {\'opacity\':\'0.5\'}" style="margin-top: 0px;"><input ng-model=component.coverageIncluded ng-change=vm.onCoverageCheck() name="\'includeCHK\'_{{parentIndex}}_{{$index}}" ng-disabled="component.editable.toLowerCase()===\'n\'" type=checkbox id="\'includeCHK\'_{{parentIndex}}_{{$index}}"> <label for="\'includeCHK\'_{{parentIndex}}_{{$index}}"></label></span></div><br></div><div ng-if="vm.planIndex !== 1" class="row pricing-page components-value" ng-repeat="componentDisplayValue in component.components track by $index">{{componentDisplayValue.compDisplayValue[0]}}<br></div></div><div class="col-md-12 pricing-page components-value" ng-if="component.subGroups !== null && vm.planIndex === 1"><br><!--  <div class="pull-right"  > \n                <span class="pricing-page coverageGrpBtns coverageGrpEdit fa fa-pencil" ></span>\n                <span ng-if="component.frequentlyAddedCov.toLowerCase()===\'y\'" class="pricing-page coverageGrpBtns coverageGrpDelete fa fa-trash" ></span>\n                <span ng-if="component.mandatoryCoverage.toLowerCase()===\'y\'" class="pricing-coveragepopup tailCheckbox-wrap" style="margin-top: 0px;">\n                    <input   name="\'includeCHK\'_{{parentIndex}}_{{$index}}" type="checkbox" id="\'includeCHK\'_{{parentIndex}}_{{$index}}">\n                    <label for="\'includeCHK\'_{{parentIndex}}_{{$index}}"></label>\n                </span>\n\n              </div> --></div><div class="col-md-12 pricing-page components-value" ng-if="component.subGroups !== null && vm.planIndex !== 1"><br></div><div class="row pricing-page subGroup-value-cont" ng-if="subGroup.appearUI.toLowerCase()!=\'n\' && subGroup.appearUI!=null" ng-repeat="subGroup in component.subGroups | filter: {appearUI: \'Y\'} |filter: {appearUI: \'y\'}" ng-init="subIndex = $index" ng-if="component.subGroups !== null && component.subGroups.length !== 0"><div class="col-md-12 pricing-page subGroup-value" ng-if="vm.planIndex === 1" ng-style=vm.getGroupRowHeight(parentIndex,subIndex)><p style=word-wrap:break-word; ng-if="componentDisplayValue.compShowAsHelp.toLowerCase()!=\'y\' && componentDisplayValue.compAppearUI.toLowerCase()!=\'n\' && subGroup.components.length === 1" ng-repeat="componentDisplayValue in subGroup.components track by $index"><widget editable={{componentDisplayValue.compEditable}} ng-model=componentDisplayValue.userValue name="{{component.compControlName+\'_\'+parentIndex+\'_\'+subIndex+\'_\'+$index}}" type={{vm.getWidgetType(componentDisplayValue.compControlName)}} classname={{vm.getWidgetClass(componentDisplayValue.compControlName)}} labels=componentDisplayValue.compValueName values=componentDisplayValue.compValueName widget-change=vm.widgetChange(componentDisplayValue)></widget></p><p style=word-wrap:break-word; ng-if="componentDisplayValue.compShowAsHelp.toLowerCase()!=\'y\' && componentDisplayValue.compAppearUI.toLowerCase()!=\'n\' && subGroup.components.length > 1">Included <span ng-click=vm.onFreqDelete(parentIndex,subIndex) ng-if="subGroup.frequentlyAddedCov.toLowerCase()===\'y\'" class="pull-right pricing-page coverageGrpBtns coverageGrpDelete fa fa-trash"></span> <span ng-if="subGroup.mandatoryCoverage.toLowerCase()===\'y\'" class="pull-right pricing-coveragepopup tailCheckbox-wrap" style=margin-top:-5px;><input ng-model=subGroup.coverageIncluded ng-change=vm.onCoverageCheck() name="\'includeCHK\'_{{parentIndex}}_{{$index}}" type=checkbox id="\'includeCHK\'_{{parentIndex}}_{{$index}}"> <label for="\'includeCHK\'_{{parentIndex}}_{{$index}}"></label> </span><span ng-click=vm.openComponentModal(subGroup) class="pull-right pricing-page coverageGrpBtns coverageGrpEdit fa fa-pencil"></span></p><!-- <p style="word-wrap:break-word;" ng-if="componentDisplayValue.compShowAsHelp.toLowerCase()!=\'y\' && componentDisplayValue.compAppearUI.toLowerCase()!=\'n\'"\n                  ng-repeat="componentDisplayValue in subGroup.components track by $index" ng-bind-html="componentDisplayValue.compDisplayValue[0]">  </p> --></div><div class="col-md-12 pricing-page subGroup-value" ng-if="vm.planIndex !== 1" ng-style=vm.getGroupRowHeight(parentIndex,subIndex)><p style=word-wrap:break-word; ng-if="componentDisplayValue.compShowAsHelp.toLowerCase()!=\'y\' && componentDisplayValue.compAppearUI.toLowerCase()!=\'n\'" ng-repeat="componentDisplayValue in subGroup.components track by $index" ng-bind-html=componentDisplayValue.compDisplayValue[0]></p></div></div></div></div><div class=col-md-3><div class="row text-center pricing-page frequently-added-title">Frequently added coverages<br>for this business class:</div><div class="row text-center" ng-repeat="component in vm.pricingData[2].groups"><div class=col-centered style="width: 177px;height: 155px;border-radius: 6px;box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.2);"><div class=row style="margin: 0px;position:  relative;"><img ng-src="{{main.assetPath+\'pic.png\'}}"> <span class="pricing-page frequently-btn-title">{{component.coverageName}}</span></div><div class=clearfix></div><div class="row text-left" ng-click=vm.openComponentModal(component) style="position:  relative;bottom: 0px;margin-left: 4px;margin-top: 4px;"><img ng-src="{{main.assetPath+\'dot-add.png\'}}"> <span class="pricing-page tile-img-label">Add Coverage</span></div></div></div><!--  <div class="row text-center" >\n                <div class="col-centered"style="width: 177px;height: 155px;border-radius: 6px;box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.2);">\n                  <div class="row" style="margin: 0px;">\n                    <img ng-src="{{main.assetPath+\'pic-1.png\'}}" >\n                    <span class="pricing-page frequently-btn-title">Employee Benefits Liability</span>\n                  </div>\n                  <div class="clearfix"></div>\n\n                  <div class="row text-left"  ng-click="vm.openComponentModal()"  style="position:  relative;top: -40px;margin-left: 4px;margin-top: 4px;">\n                    <img ng-src="{{main.assetPath+\'dot-add.png\'}}" >\n                    <span class="pricing-page tile-img-label">Add Coverage</span>\n                  </div>\n\n              </div>\n            </div>\n            <div class="row text-center" >\n                <div class="col-centered"style="width: 177px;height: 155px;border-radius: 6px;box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.2);">\n                  <div class="row" style="margin: 0px;">\n                    <img ng-src="{{main.assetPath+\'pic-2.png\'}}" >\n                    <span class="pricing-page frequently-btn-title">Crisis Event Expense</span>\n                  </div>\n                  <div class="clearfix"></div>\n\n                  <div class="row text-left"  ng-click="vm.openComponentModal()"  style="position:  relative;top: -20px;margin-left: 4px;margin-top: 4px;">\n                    <img ng-src="{{main.assetPath+\'dot-add.png\'}}" >\n                    <span class="pricing-page tile-img-label">Add Coverage</span>\n                  </div>\n\n              </div>\n            </div>--><div class="row text-center" style="margin-top: 21px;"><span ng-click=vm.openAddFreqModal() class="btn btn-blue pricing-page addCoverages-text">Add Coverages</span></div></div></span></div><div class=row style="height: 160px;width: 100%;"></div><div class="col-md-10 pricing-page pricing-footer" ng-if="vm.pricingData.length != 0 && !vm.serverError"><div class=row><div class=col-md-5 style="padding-left: 70px;padding-top: 60px;"><p ng-if="vm.pricingData[vm.planIndex].quoteBindable.toLowerCase() === \'true\' && vm.pricingData[vm.planIndex].AgencyValid.toLowerCase() === \'true\'" class="pricing-page bindable"><img ng-src="{{main.assetPath+\'bindable.png\'}}"> <span style="padding-left: 10px;">Your quote is bindable</span></p><p ng-if="vm.pricingData[vm.planIndex].quoteBindable.toLowerCase() !== \'true\' || vm.pricingData[vm.planIndex].AgencyValid.toLowerCase() !== \'true\'" class="pricing-page bindable"><img ng-src="{{main.assetPath+\'non-bindable.png\'}}"> <span style="padding-left: 10px;">Your quote is not bindable</span></p></div><div class="col-md-2 pricing-page" ng-class=vm.stdPlanFooterClass style="padding-left: 22px;margin-left: -16px;padding-top: 30px;"><p class="pricing-page footer-package-name pricing-page" ng-class=vm.stdPlanSelectedTxt>{{vm.pricingData[0].packageName}}</p><p class="pricing-page total-price">{{(vm.pricingData[0].price!=null)?vm.currencyType+vm.pricingData[0].price:vm.pricingData[0].price}}</p><p class="pricing-page footer-small-text">annually</p></div><div class="col-md-2 pricing-page" ng-class=vm.tldPlanFooterClass style="padding-left: 22px;padding-top: 30px;"><p class="pricing-page footer-package-name" ng-class=vm.tldPlanSelectedTxt>{{vm.pricingData[1].packageName}}</p><p ng-if="vm.tailerValueModified!==true" class="pricing-page total-price">{{(vm.pricingData[1].price!=null)?vm.currencyType+vm.pricingData[1].price:vm.pricingData[1].price}}</p><p ng-if="vm.tailerValueModified!==true" class="pricing-page footer-small-text">annually</p><p ng-if="vm.tailerValueModified===true" style="margin-left: 0px;" class="pricing-page apply-price"><span class="col-md-8 pricing-page apply-price-text" style="padding: 0;padding-top:15px;padding-left: 5px;">Apply changes for updated premium</span> <span class=col-md-4 style="padding: 0;padding-top: 10px;"><button class="btn btn-blue btn-xs" ng-click=vm.onApplyButton() style="width:100%;margin-left:-5px;padding-left: 5px;padding-right: 5px;">Apply</button></span></p></div><!-- <div class="col-md-1"></div>--><div class="col-md-3 text-center" style="padding-top: 11px;padding-left: 0px;padding-right:0px;margin-left: 0px;"><div class=row><a href=javascript:; ng-click=vm.goToPremiumSummary() ng-class="(vm.pricingData[vm.planIndex].quoteBindable.toLowerCase() !== \'true\' || vm.pricingData[vm.planIndex].AgencyValid.toLowerCase() === \'false\' || vm.pricingData[vm.planIndex].price===null)? \'btn pricing-page btn-green btn-disabled\' : \'btn pricing-page btn-green\'" style="width: 177px;height: 46px;line-height: 37px;"><span ng-class="(vm.pricingData[vm.planIndex].quoteBindable.toLowerCase() !== \'true\'  || vm.pricingData[vm.planIndex].AgencyValid.toLowerCase() === \'false\' || vm.pricingData[vm.planIndex].price===null)? \'pricing-page button-label-black\' : \'pricing-page button-label\'">Proceed to Issue</span></a></div><!--  <div class="row" style="margin-top: 15px;">\n            <a ng-click="vm.onCreateProposal()" ng-class="(vm.pricingData[vm.planIndex].quoteBindable.toLowerCase() !== \'true\' || vm.pricingData[vm.planIndex].price===null || vm.pricingData[vm.planIndex].price===null)? \'btn btn-blue btn-disabled\' : \'btn btn-blue\'"\n              style="width: 177px;height: 46px;line-height: 46px;padding: 0 20px;">\n              <span ng-class="(vm.pricingData[vm.planIndex].quoteBindable.toLowerCase() !== \'true\' || vm.pricingData[vm.planIndex].price===null )? \'pricing-page button-label2-black\' : \'pricing-page button-label2\'">Create Proposal</span>\n            </a>\n          </div>--><div class=row style="margin-top: 15px;"><a ng-click=vm.onCreateProposal() ng-class="(vm.pricingData[vm.planIndex].price===null)? \'btn btn-blue btn-disabled\' : \'btn btn-blue\'" style="width: 177px;height: 46px;line-height: 46px;padding: 0 20px;"><span ng-class="(vm.pricingData[vm.planIndex].price===null )? \'pricing-page button-label2-black\' : \'pricing-page button-label2\'">Create Proposal</span></a></div><div class=clearfix></div><div class=row style="margin-top: 15px;"><span ng-if="vm.pricingData[vm.planIndex].price!=null"><!-- <a href="#">Create Proposal</a>\n                  <br/>--> <a class="pricing-page blueLink footer-content" href=javascript:; ng-click=vm.goToRefer()>Refer to underwriter</a> </span><span ng-if="vm.pricingData[vm.planIndex].price==null" class="pricing-page footer-content">Refer to underwriter</span></div></div></div></div></form></div>');
$templateCache.put('app/views/qualification/nav.qualification.html','<span class=summary-leftnav><br><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.businessdetails>Business Details</a></span> <span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.lob>Select lines of business</a></span><ul class="nav list-group eligibility-panel"><li><a class="list-group-item active" href=""><i class="icon-home icon-1x"></i>Qualification Questions</a></li></ul><div style="position: absolute;bottom:0px;color: #909090;padding-left: 5px;">Copyright \xA9 2018 Utica Mutual Insurance Company, New Hartford, NY 13413 | All Rights Reserved</div>');
$templateCache.put('app/views/qualification/qualification.html','<div class="modal fade" id=myModal tabindex=-1 role=dialog aria-labelledby=myModalLabel><div class=modal-dialog role=document><div class=modal-content><div class=modal-header style="border-bottom: 0px;"><h4 class=modal-title id=myModalLabel>Leaving to go to Commercial Edge</h4></div><div class=modal-body>Quotes for Worker\'s Compensation and Commercial Auto can be Completed in Commercial Edge. Contuining will open Commercial Edge in new tab.</div><div class=modal-footer style="border-top: 0px;"><button type=button class="btn btn-default" data-dismiss=modal>Cancel</button> <button type=button class="btn btn-primary">Continue</button></div></div></div></div><script type=text/ng-template id=customTemplate.html><a>\n    <span ng-bind-html="match.label | uibTypeaheadHighlight:query"></span>\n  </a></script><div class="right-contents eligibility-page"><div class=row><div class="col col-md-12"><h2>Qualification Questions</h2><h4 class=error ng-if="vm.formValidated && vm.appianEligibility==false"><img ng-src="{{main.assetPath+\'non-bindable.png\'}}">Not Eligible</h4><h4 style=color:green ng-if="vm.formValidated && vm.appianEligibility==true"><img ng-src="{{main.assetPath+\'finished.png\'}}">Eligible</h4></div></div><!-- form --><form class=formClass form-submit-validation="" name=vm.eligibilityForm id=vm.eligibilityForm ng-submit="vm.triggerSubmit(vm.eligibilityForm.$valid, vm.eligibilityForm)" novalidate><!-- widgets --><div class=row ng-repeat="lob in vm.questions"><div class=white-pannel><h4 class=pannel-title>{{ lob.lob }}</h4><span class="row eligible eligiblehieght" ng-repeat="question in lob.qualificationQuestion" ng-if="question.appearUI.toLowerCase()!=\'n\' && question.appearUI!=null "><fieldset style="padding-left:  0px;margin-left:  0px; color:black;"><span class=col-md-6 style="padding-left:  0px;"><p ng-bind-html=question.datasetName>{{question.datasetName}}</p><p ng-if="question.aditionalInfo !=null"><u ng-if="Object.keys(question).indexOf(\'url\')==-1 || question.url ==null" ng-bind-html=question.aditionalInfo>{{question.aditionalInfo}}</u> <u ng-if="question.url !=null"><a ng-click="vm.openLink(question.url, $event)" href=javascript:;>{{question.aditionalInfo}}</a></u></p><p ng-if="question.additionalInfo !=null"><u ng-if="Object.keys(question).indexOf(\'url\')==-1 || question.url ==null" ng-bind-html=question.additionalInfo>{{question.additionalInfo}}</u> <u ng-if="question.url !=null"><a href=javascript:; ng-click="vm.openLink(question.url, $event)">{{question.additionalInfo}}</a></u></p><ul ng-if="question.fieldText != null && question.fieldText != undefined"><li ng-repeat="list in question.fieldText">{{list.caption}}</li></ul></span><span ng-if="question.controlName == \'Radio_Button\'"><span class="col-md-2 checkradio-wrap no-padding" ng-repeat="q in question.valueName"><input type=radio name={{question.sequenceQQ}} value={{q.value}} id={{question.sequenceQQ}}{{$index*2}} ng-required=question.isRequired ng-model=question.userValue data-systemrefid={{question.systemRefID}} ng-change=vm.loadChildQuestion(question)> <label for={{question.sequenceQQ}}{{$index*2}}>{{q.caption | removeQuote}}<!-- {{q.caption | removeQuote}} {{$parent.$parent.$parent.$parent.$index}}  --></label> </span><!-- {{question.systemRefID}}-{{$parent.$parent.$parent.$parent.$index}} --><div class=clearfix></div><div class="col-md-offset-6 col-md-6"><p class="help-block error radio-error" ng-if="eligibilityForm.submitted && (question.userValue == \'\' || question.userValue == null)">This field is required</p></div></span></fieldset></span><span class=row ng-if="lob.qualificationQuestion.length == 0"><p>No applicable questions</p></span></div></div><div class=row><div class=empty-panel-formatter><div class=col-md-4><button type=submit class="btn btn-blue">Save &amp; Continue</button></div></div></div></form></div>');
$templateCache.put('app/views/quote/nav.quote.html','<div style="min-height: 460px;"><span class=summary-leftnav><br><span class=selected-plan style="display: inline-block;"></span> <a class=list-group-item4 ui-sref=app.businessdetails>Business Details</a> </span><span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span> <a class=list-group-item4 ui-sref=app.lob>Select lines of business</a> </span><span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span> <a class=list-group-item4 ui-sref=app.qualification>Qualification Questions</a></span><hr><h4 class=eligibility-panels>Business Owners Policy</h4><ul class="nav list-group"><li class=lob-leftnav><a class="list-group-item active" ui-sref=app href=""><i class="icon-home icon-1x"></i> Start your quote</a></li><li class=lob-leftnav><a class="list-group-item locked" ui-sref=app href=""><i class="icon-home icon-1x"></i> Locations and Buildings</a></li><li class=lob-leftnav><a class="list-group-item locked" ui-sref=app href=""><i class="icon-home icon-1x"></i> Additional Interests</a></li><li class=lob-leftnav><a class="list-group-item locked" ui-sref=app href=""><i class="icon-home icon-1x"></i> Underwriting Questions</a></li><li class=lob-leftnav><a class="list-group-item locked" ui-sref=app href=""><i class="icon-home icon-1x"></i> Pricing & Coverages</a></li></ul></div><div style="position: relative;bottom:0px;color: #909090;padding-left: 5px;">Copyright \xA9 2018 Utica Mutual Insurance Company, New Hartford, NY 13413 | All Rights Reserved</div>');
$templateCache.put('app/views/quote/quote.html','<div class=lob-page><!-- modal template  --><script type=text/ng-template id=LOBModalContent.html><!-- header -->\n    <div class="modal-header" style="border-bottom: 0px;">\n      <h4 class="modal-title" id="myModalLabel">Leaving to go to Commercial Edge</h4>\n    </div>\n    <!-- body -->\n    <div class="modal-body">\n      Quotes for {{ $ctrl.lobLabel }} can be completed in Commercial Edge. Continuing will open Commercial Edge in a new tab.\n    </div>\n    <!-- footer -->\n    <div class="modal-footer">\n      <button type="button" class="btn btn-default" ng-click="$ctrl.cancel()">Cancel</button>\n      <button type="button" class="btn btn-primary" ng-click="$ctrl.ok()">Continue</button>\n    </div></script><!-- LOB form --><form class=formClass name=vm.lobForm form-submit-validation="" ng-submit=vm.submitForm(vm.lobForm.$valid,vm.lobForm) novalidate><div class=col-md-12><div class=col-md-10><h1 class=b16>Start Your Quote</h1><!-- widget card --><div id=bname class="card no-margin"><div class=card-block><h4 class="card-title mb-10">Select Lines of Business</h4><div class=col-md-12><div class="row checkdiv"><!-- LOB IS eligible --><div class="col-md-12 checkradio-wrap" ng-repeat="question in vm.lobMeta.lob"><!-- LOB values - why only first one? --><div class=row ng-repeat="value in question.valueName" ng-if="value.eligibility == true && value.lobName == \'Business Owners\'"><div class=col-md-8><input type=radio name=lobitem data-uuid={{question.uuid}} value=Yes id=lob-{{$index}} ng-model=value.userValue ng-change=vm.lobNewValue(value.userValue)> <label for=lob-{{$index}}>{{value.lobName}}</label></div></div><!-- End LOB values --><p style=color:#666;font-size:12px ng-if="vm.eligibleLobs != undefined">An umbrella quote can be added after completion of business owners policy</p><!-- end of LOB IS eligible --><div ng-if="vm.mvpLob != null && vm.mvpLob != undefined" class="alert border alert-warning alert-color"><span><img ng-src="{{main.assetPath+\'icon-more-info.svg\'}}"> </span><span>This business class is not yet available in omnia. <a href="/DSCIQuote/outbound?landingPageInCE=Select_Line_of_Business&lob=CarrierBusinessOwners">Start in Commercial Edge</a></span></div><!-- LOB not eligible --><div class=row ng-repeat="value in question.valueName" ng-if="value.lobName == \'Business Owners\' && value.eligibility == false" style=padding-bottom:15px;><div class=col-md-1 style=width:0px;><img ng-if="value.isQuoteEligible == false" ng-src="{{main.assetPath+\'tick_red.png\'}}"> <img ng-if="value.isQuoteEligible == true" ng-src="{{main.assetPath+\'tick_green.png\'}}"></div><div class=col-md-5><strong>{{value.lobName}}</strong></div><div class=col-md-6><span ng-if="value.isQuoteEligible == false">Not eligible</span><!-- Sorry, but you are not eligible to quote this line of business --> <a ng-if="value.isQuoteEligible == true" style="font-size: 14px;font-weight:bold;" href="/DSCIQuote/outbound?landingPageInCE=First_page_of_the_quote_flow&lob={{value.handOff}}">Start in Commercial Edge</a></div></div><!-- END OF LOB not eligible  value.isQuoteEligible == false--></div></div><fieldset><!-- we need to use ng-show because the form elements must be present. --><div class=row ng-repeat="question in vm.lobMeta.lob" ng-show=vm.lobSelected><div class="col-md-4 col-sm-12" ng-repeat="policy in vm.lobMeta.policyType" ng-if="policy.controlName == \'Date\'"><label for={{policy.systemRefId}}>{{policy.datasetName}}</label><div class=form-group ng-class="{ \'has-error\' : vm.lobForm.submitted && vm.lobForm.{{policy.systemRefId}}.$invalid && vm.lobForm.{{policy.systemRefId}}.$touched }"><!-- date picker --> <input placeholder="Please Select" ng-click="vm.open($index ,$event)" id={{policy.systemRefId}} show-button-bar=false type=text name={{policy.systemRefId}} class="lob form-control dtpicker" uib-datepicker-popup={{vm.format}} ng-model=policy.userValue is-open=vm.opened[$index] datepicker-options=vm.dateOptions[policy.systemRefId] ng-required=true close-text=Close datepicker-popup-template-url=app/partials/tpl.datepickerpopup.html maxlength=10 date-val ng-change="vm.changed($index, policy.userValue, policy.systemRefId)" ng-keyup="vm.keyupevt($index, $event, policy.systemRefId)" ng-blur="vm.keyupevt($index, $event, policy.systemRefId)"><!--  alt-input-formats="altInputFormats" --><!--\n                          ng-disabled="vm.dateDisabled[$index]" data-uuid="{{policy.uuid}}"\n                      --><!-- required validation --><p ng-show="policy.userValue == undefined || (vm.lobForm.{{policy.systemRefId}}.$invalid && vm.lobForm.{{policy.systemRefId}}.$touched)" class="help-block error build22">{{policy.datasetName | camelCase}} is required</p><!-- end required validation  ng-if="policy.isRequired && !vm.dateFormatError[$index]"--><!-- date format validation --><p ng-if=vm.dateFormatError[$index] class=error>{{vm.message[$index]}}</p></div></div><!-- state select --><div class="col-md-4 col-sm-12" ng-repeat="policy in vm.lobMeta.policyType" ng-if="policy.controlName ==\'Drop_Down\'"><label for={{policy.systemRefId}}>{{policy.datasetName}}</label><div class=form-group ng-class="{ \'has-error\' : vm.lobForm.submitted && vm.lobForm.{{policy.systemRefId}}.$invalid && vm.lobForm.{{policy.systemRefId}}.$touched }"><!-- control --> <select name={{policy.systemRefId}} ng-model=policy.userValue class="custom_select form-control" placeholder="Please Select" required ng-change="vm.showMessage(policy.userValue, vm.policyType[2].eventName[0])" id={{policy.systemRefId}} data-uuid={{policy.uuid}}><option value="" selected>Please Select</option><option ng-repeat="option in policy.valueName" value={{option}}>{{option}}</option></select><!-- validation --><p ng-show="policy.userValue == undefined || (vm.lobForm.{{policy.systemRefId}}.$invalid && vm.lobForm.{{policy.systemRefId}}.$touched)" class="help-block error build22">{{policy.datasetName | camelCase}} is required</p></div></div><!-- end state select --><!-- validation errors --><div class="col-md-12 col-sm-12"><!-- datediff --><div ng-if="vm.endDateDiff || vm.lobError && (vm.appianError != null || vm.appianWarning != null)" class="alert border" ng-class="{\'alert-danger alert-color\':vm.isError, \'alert-warning border-warning\':!vm.isError}"><span><img ng-src="{{main.assetPath+\'icon-more-info.svg\'}}"> </span><span ng-if=vm.isError>{{vm.appianError}}. <a href="/DSCIQuote/outbound?landingPageInCE=First_page_of_the_quote_flow&lob=CarrierBusinessOwners">Start in Commercial Edge</a></span> <span ng-if=!vm.isError>{{vm.appianWarning}}.</span></div><!-- loberr --><!-- <div ng-if="vm.lobError" class="alert alert-warning border border-warning">\n                      <span>\n                        <img ng-src="{{main.assetPath+\'icon-more-info.svg\'}}" />\n                      </span>\n                      You have picked a non standard effective date.This quote will be referred to an Underwriter after pricing.\n                    </div> --><!-- effectivedatediff --><!-- <div ng-if="vm.effectiveDateDiff" class="alert alert-warning border border-warning">\n                      <span>\n                        <img ng-src="{{main.assetPath+\'icon-more-info.svg\'}}" />\n                      </span>\n                      You have picked a non standard effective date.This quote will be referred to an Underwriter after pricing.\n                    </div> --><!-- direct to commercial edge --><p class=primary_risk_message ng-show="vm.showMsg && vm.optionData">Quote for {{vm.optionData}} can be completed in Commercial Edge. <a style="font-size: 14px;" href="/DSCIQuote/outbound?landingPageInCE=First_page_of_the_quote_flow&lob=CarrierBusinessOwners">Start in Commercial Edge</a></p></div><!-- end validation block --><!-- past policy info --><div class="col-md-12 col-sm-12" ng-if=vm.lobMeta.pastPolicyInfo><label>PAST POLICY INFO</label><div class=row><div class=col-md-8><p class=small-p>{{vm.lobMeta.pastPolicyInfo.datasetName}}</p></div><div class=col-md-4><select name={{vm.lobMeta.pastPolicyInfo.systemRefId}} ng-model=vm.lobMeta.pastPolicyInfo.userValue class="custom_select form-control" placeholder="Please Select" required id={{vm.lobMeta.pastPolicyInfo.systemRefId}}><option value="" selected>Please Select</option><option ng-repeat="option in vm.lobMeta.pastPolicyInfo.valueName" value={{option}}>{{option}}</option></select><p ng-show="vm.lobForm.{{vm.lobMeta.pastPolicyInfo.systemRefId}}.$invalid && vm.lobForm.{{vm.lobMeta.pastPolicyInfo.systemRefId}}.$touched" class="help-block error build22">Past Policy Info is required</p></div></div></div><!-- past policy info --></div><!-- lob menu --> <!-- <div class="row" ng-if="vm.notEligibleLobs != undefined && vm.eligibleLobs != undefined">\n                  <div class="col-md-12">\n                    <hr/>\n                  </div>\n                </div> --><div class=row></div><hr ng-if="vm.notEligibleLobs != undefined && vm.eligibleLobs != undefined"><p style=font-size:12px ng-if="vm.notEligibleLobs != undefined">The following lines of business are quoted in the Commercial Edge interface. We will direct you to Commercial Edge to complete.</p><br><div ng-repeat="questions in vm.lobMeta.lob" style=margin-left:10px;><div ng-repeat="theLob in questions.valueName" ng-if="theLob.productId == null && theLob.lobName != \'Business Owners\'"><!-- displays if eligible = TRUE --><div class=row ng-if="theLob.eligibility == false && theLob.isQuoteEligible == true"><div class=col-md-1 style=width:0px;><img ng-src="{{main.assetPath+\'tick_green.png\'}}"></div><div class=col-md-5><label style="font-size: 14px;" for={{$index*2}}>{{theLob.lobName}}</label></div><div class=col-md-6><label for={{$index*2}}><a style="font-size: 14px;" href="/DSCIQuote/outbound?landingPageInCE=First_page_of_the_quote_flow&lob={{theLob.handOff}}">Start in Commercial Edge</a></label></div></div><!-- END displays if eligible = TRUE --><!-- displays if eligible = FALSE --><div class=row ng-if="theLob.eligibility == false && theLob.isQuoteEligible == false"><div class=col-md-1 style=width:0px;><img ng-src="{{main.assetPath+\'tick_red.png\'}}"></div><div class=col-md-5><label style="font-size: 14px;" for={{$index*2}}>{{theLob.lobName}}</label></div><div class=col-md-6><!-- Sorry, but you are not eligible to quote this line of business --> Not eligible</div></div><!-- END displays if eligible = FALSE --></div></div></fieldset></div></div></div></div></div><!-- continue button --><div class=col-md-9><div class=col-md-12><div class="card plan-bg"><div class="input-group col-md-4" style="width: 20%"><button type=submit class="btn btn-lg btn-primary" ng-disabled="!vm.selectedValue || vm.isError || vm.mvpLob != null">Continue</button></div></div></div></div><!-- end button block --></form><div class=col-md-3></div></div>');
$templateCache.put('app/views/refer/nav.refer.html','<span class=summary-leftnav><br><span class=selected-plan style="display: inline-block;"></span> <a class=list-group-item4 ui-sref=app.businessdetails>Business Details</a> </span><span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span> <a class=list-group-item4 ui-sref=app.lob>Select lines of business</a> </span><span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span> <a class=list-group-item4 ui-sref=app.qualification>Qualification Questions</a></span><hr><h4 class=eligibility-panels>Business Owners Policy</h4><span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span> <a class=list-group-item4 ui-sref=app.quote>Start your quote</a> </span><span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span> <a class=list-group-item4 ui-sref=app.location>Locations and Buildings</a> </span><span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span> <a class=list-group-item4 ui-sref=app.additionalinterest>Additional Interests</a> </span><span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span> <a class=list-group-item4 ui-sref=app.underwriting>Underwriting Questions</a></span><ul class="nav list-group"><li style="padding-left: 5px;"><a class="list-group-item active" ui-sref=app.pricingandcoverage><i class="icon-home icon-1x"></i>Pricing and Coverage</a></li></ul><div style="position: absolute;bottom:0px;color: #909090;padding-left: 5px;">Copyright \xA9 2018 Utica Mutual Insurance Company, New Hartford, NY 13413 | All Rights Reserved</div>');
$templateCache.put('app/views/refer/refer.html','<div class=refer-page><!-- modals and stuff go here --><!-- <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">\n    <div class="modal-dialog" role="document">\n      <div class="modal-content">\n        <div class="modal-header" style="border-bottom: 0px;">\n          <h4 class="modal-title" id="myModalLabel">Leaving to go to Commercial Edge </h4>\n        </div>\n        <div class="modal-body">\n          Quotes for Worker\'s Compensation and Commercial Auto can be Completed in Commercial Edge. Contuining will open\n          Commercial\n          Edge in new tab.\n        </div>\n        <div class="modal-footer" style="border-top: 0px;">\n          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>\n          <button type="button" class="btn btn-primary">Continue</button>\n        </div>\n      </div>\n    </div>\n  </div>\n  <script type="text/ng-template" id="customTemplate.html">\n    <a>\n      <span ng-bind-html="match.label | uibTypeaheadHighlight:query"></span>\n    </a>\n  </script>\n  <script type="text/ng-template" id="customPopupTemplate.html">\n    <div class="custom-popup-wrapper" ng-style="{top: position().top+\'px\', left: position().left+\'px\'}"\n         style="display: block;"\n         ng-show="isOpen() && !moveInProgress" aria-hidden="{{!isOpen()}}">\n      <ul class="dropdown-menu" role="listbox">\n        <li class="uib-typeahead-match" ng-repeat="match in matches track by $index"\n            ng-class="{active: isActive($index) }" ng-mouseenter="selectActive($index)"\n            ng-click="selectMatch($index)" role="option" id="{{::match.model.sicCode}}">\n          <div uib-typeahead-match index="$index" match="match" query="query" template-url="customTemplate.html"></div>\n        </li>\n      </ul>\n    </div>\n  </script> --><!-- end of modals --><div class=row><div class=col-md-12><h1>Refer to Underwriter</h1></div></div><div class=row><div class=col-md-9><!-- Insured Info --><form data-toggle=validator class=formClass form-submit-validation="" name=namedForm id=namedForm autocomplete=off novalidate><div class=row><div class="white-pannel insured-panel"><h4 class=pannel-title>{{ vm.insuredInfoHdr.datasetName }}</h4><div class=row><div ng-repeat="item in vm.formMeta.insuredInformationMetadata[0].insuredInformation"><div><!-- Textbox --><div class=col-md-{{item.screenColumns}} ng-if="item.controlName == \'Text_Box\' && item.appearUI == \'Y\'"><div class=form-group ng-class="{\'has-error\': vm.forms[0] == true && namedForm.{{item.systemRefId | dotWithUnderscore}}.$invalid}"><label for="{{item.systemRefId | dotWithUnderscore}}">{{item.datasetName}}</label> <input type={{item.inputType}} name="{{item.systemRefId | dotWithUnderscore}}" class=form-control ng-model=item.userValue placeholder={{item.additionalInfo}} ng-required=item.isRequired id="{{item.systemRefId | dotWithUnderscore}}" ng-change=vm.valueUpdated(vm.formMeta.insuredInformationMetadata[0])><!-- Required Validation --><p ng-show="(vm.forms[0] == true && namedForm.{{item.systemRefId | dotWithUnderscore}}.$invalid)" class="help-block error build22">{{item.datasetName | camelCase}} is required</p></div></div><!-- end Textbox --><!-- Text Area --><div class=col-md-{{item.screenColumns}} ng-if="item.controlName == \'Text_Area\' && item.appearUI == \'Y\'"><div class=form-group ng-class="{\'has-error\': vm.forms[0] == true && namedForm.{{item.systemRefId | dotWithUnderscore}}.$invalid}"><label for="{{item.systemRefId | dotWithUnderscore}}">{{item.datasetName}}</label> <textarea name="{{item.systemRefId | dotWithUnderscore}}" class=form-control ng-model=item.userValue placeholder={{item.additionalInfo}} ng-required=item.isRequired id="{{item.systemRefId | dotWithUnderscore}}" rows=5 ng-change=vm.valueUpdated(vm.formMeta.insuredInformationMetadata[0])></textarea><!-- Required Validation --><p ng-show="(vm.forms[0] == true && namedForm.{{item.systemRefId | dotWithUnderscore}}.$invalid)" class="help-block error build22">{{item.datasetName | camelCase}} is required</p></div></div><!-- end Text Area --><!-- Radio --><div class="col-md-{{item.screenColumns}} checkradio-wrap" ng-if="item.controlName == \'Radio_Button\' && item.appearUI == \'Y\'"><div class=row><div class=col-md-12><p><strong>{{item.datasetName}}</strong></p></div></div><div class=row><div class="col-md-3 form-group" ng-repeat="value in item.valueName" ng-class="{ \'has-error\': vm.forms[0] == true && namedForm.{{item.systemRefId | dotWithUnderscore}}.$invalid}"><input type=radio name="{{item.systemRefId | dotWithUnderscore}}" value={{value}} id="{{item.systemRefId | dotWithUnderscore}}-{{$index}}" ng-model=item.userValue ng-required=item.isRequired ng-change=vm.changeType(vm.formMeta.insuredInformationMetadata[0].insuredInformation);vm.valueUpdated(vm.formMeta.insuredInformationMetadata[0]);> <label for="{{item.systemRefId | dotWithUnderscore}}-{{$index}}">{{value}}</label></div></div><div class=col-md-12><!-- Required Validation --><p ng-show="vm.forms[0] == true && namedForm.{{item.systemRefId | dotWithUnderscore}}.$invalid" class="help-block error build22">This field is required</p></div></div><!-- End Radio --><!-- Dropdown --><div class=col-md-{{item.screenColumns}} ng-if="item.controlName == \'Drop_Down\' && item.appearUI == \'Y\'"><div class=form-group ng-class="{\'has-error\': (vm.forms[0] == true && namedForm.{{item.systemRefId | dotWithUnderscore}}.$invalid) }"><label>{{item.datasetName}}</label> <select name="{{item.systemRefId | dotWithUnderscore}}" ng-model=item.userValue class="custom_select form-control" ng-required=item.isRequired ng-change=vm.valueUpdated(vm.formMeta.insuredInformationMetadata[0])><option value="">Please Select</option><option ng-repeat="option in item.valueName" value={{option}}>{{option}}</option></select><!-- Required Validation --><p ng-show="vm.forms[0] == true && namedForm.{{item.systemRefId | dotWithUnderscore}}.$invalid" class="help-block error build22">{{item.datasetName | camelCase}} is required</p></div></div><!-- End Dropdown --><!-- Checkbox --><div class=col-md-{{item.screenColumns}} ng-if="item.controlName == \'Check_Box\' && item.appearUI == \'Y\'"><div class=checkbox-wrap><input type=checkbox class=checkbox id="{{item.systemRefId | dotWithUnderscore}}" name="{{item.systemRefId | dotWithUnderscore}}" ng-model=item.userValue ng-change=vm.valueUpdated(vm.formMeta.insuredInformationMetadata[0])> <label for="{{item.systemRefId | dotWithUnderscore}}" class=checkbox-label>{{item.datasetName}}</label></div></div><!-- End Checkbox --><div class=clearfix ng-if=item.hasRowBreak></div></div></div></div><div class=row><button type=button class="btn btn-primary pull-right" ng-click="vm.submitNamedForm(0, vm.formMeta.insuredInformationMetadata[0], namedForm.$valid, true)" ng-if=vm.formMeta.insuredInformationMetadata[0].insuredInformation>Save</button></div></div></div></form><!-- dynamic --><div class=row ng-repeat="widget in vm.formMeta.insuredInformationMetadata" ng-if="$index != 0"><div class="white-pannel insured-panel"><h4 class=pannel-title>Additional Insured Information <a class=pull-right href=javascript:; ng-click=vm.removeNamedInsured($index)>Remove</a></h4><form data-toggle=validator class=formClass form-submit-validation="" name=namedForm_{{$index}} id=namedForm_{{$index}} ng-submit="vm.submitNamedForm($index, widget, this[\'namedForm_\'+$index].$valid, false)" autocomplete=off novalidate><div class=row><div ng-repeat="item in widget.insuredInformation"><div><!-- Textbox --><div class=col-md-{{item.screenColumns}} ng-if="item.controlName == \'Text_Box\' && item.appearUI == \'Y\'"><div class=form-group ng-class="{\'has-error\': vm.forms[$parent.$parent.$index] == true && namedForm_{{$parent.$parent.$index}}.{{item.systemRefId | dotWithUnderscore}}_{{$parent.$parent.$index}}.$invalid}"><label for="{{item.systemRefId | dotWithUnderscore}}_{{$parent.$parent.$index}}">{{item.datasetName}}</label> <input type=text name="{{item.systemRefId | dotWithUnderscore}}_{{$parent.$parent.$index}}" class=form-control ng-model=item.userValue placeholder={{item.additionalInfo}} required id="{{item.systemRefId | dotWithUnderscore}}_{{$parent.$parent.$index}}" ng-change=vm.valueUpdated(widget)><!-- Required Validation --><p ng-show="(vm.forms[$parent.$parent.$index] == true && namedForm_{{$parent.$parent.$index}}.{{item.systemRefId | dotWithUnderscore}}_{{$parent.$parent.$index}}.$invalid)" class="help-block error build22">{{item.datasetName | camelCase}} is required</p></div></div><!-- end Textbox --><!-- Text Area --><div class=col-md-{{item.screenColumns}} ng-if="item.controlName == \'Text_Area\' && item.appearUI == \'Y\'"><div class=form-group ng-class="{\'has-error\': vm.forms[$parent.$parent.$index] == true && namedForm_{{$parent.$parent.$index}}.{{item.systemRefId | dotWithUnderscore}}.$invalid}"><label for="{{item.systemRefId | dotWithUnderscore}}">{{item.datasetName}}</label> <textarea name="{{item.systemRefId | dotWithUnderscore}}" class=form-control ng-model=item.userValue placeholder={{item.additionalInfo}} ng-required=item.isRequired id="{{item.systemRefId | dotWithUnderscore}}" rows=5 ng-change=vm.valueUpdated(widget)></textarea><!-- Required Validation --><p ng-show="(vm.forms[$parent.$parent.$index] == true && namedForm_{{$parent.$parent.$index}}.{{item.systemRefId | dotWithUnderscore}}.$invalid)" class="help-block error build22">{{item.datasetName | camelCase}} is required</p></div></div><!-- end Text Area --><!-- Radio --><div class="col-md-{{item.screenColumns}} checkradio-wrap" ng-if="item.controlName == \'Radio_Button\' && item.appearUI == \'Y\'"><div class=row><div class=col-md-12><p><strong>{{item.datasetName}}</strong></p></div></div><div class=row><div class="col-md-3 form-group" ng-repeat="value in item.valueName" ng-class="{ \'has-error\': vm.forms[$parent.$parent.$index] == true && namedForm_{{$parent.$parent.$parent.$index}}.{{item.systemRefId | dotWithUnderscore}}.$invalid}"><input type=radio name="{{item.systemRefId | dotWithUnderscore}}" value={{value}} id="{{item.systemRefId | dotWithUnderscore}}-{{$parent.$parent.$parent.$index}}-{{$index}}" ng-model=item.userValue ng-required=item.isRequired ng-change=vm.changeType(widget.insuredInformation);vm.valueUpdated(widget);> <label for="{{item.systemRefId | dotWithUnderscore}}-{{$parent.$parent.$parent.$index}}-{{$index}}">{{value}}</label></div></div><div class=col-md-12><!-- Required Validation --><p ng-show="vm.forms[$parent.$parent.$index] == true && namedForm_{{$parent.$parent.$index}}.{{item.systemRefId | dotWithUnderscore}}.$invalid" class="help-block error build22">This field is required</p></div></div><!-- End Radio --><!-- Dropdown --><div class=col-md-{{item.screenColumns}} ng-if="item.controlName == \'Drop_Down\' && item.appearUI == \'Y\'"><div class=form-group ng-class="{\'has-error\': (vm.forms[$parent.$parent.$index] == true && namedForm_{{$parent.$parent.$index}}.{{item.systemRefId | dotWithUnderscore}}.$invalid) }"><label>{{item.datasetName}}</label> <select name="{{item.systemRefId | dotWithUnderscore}}" ng-model=item.userValue class="custom_select form-control" ng-required=item.isRequired ng-change=vm.valueUpdated(widget)><option value="">Please Select</option><option ng-repeat="option in item.valueName" value={{option}}>{{option}}</option></select><!-- Required Validation --><p ng-show="vm.forms[$parent.$parent.$index] == true && namedForm_{{$parent.$parent.$index}}.{{item.systemRefId | dotWithUnderscore}}.$invalid" class="help-block error build22">{{item.datasetName | camelCase}} is required</p></div></div><!-- End Dropdown --><!-- Checkbox --><div class=col-md-{{item.screenColumns}} ng-if="item.controlName == \'Check_Box\' && item.appearUI == \'Y\'"><div class=checkbox-wrap><input type=checkbox class=checkbox id="{{item.systemRefId | dotWithUnderscore}}" name="{{item.systemRefId | dotWithUnderscore}}" ng-model=item.userValue ng-change=vm.valueUpdated(widget)> <label for="{{item.systemRefId | dotWithUnderscore}}" class=checkbox-label>{{item.datasetName}}</label></div></div><!-- End Checkbox --><div class=clearfix ng-if=item.hasRowBreak></div></div></div></div><div class=row><button type=submit class="btn btn-primary pull-right" id=sbmt_{$parent.$parent.$index}}>Save</button></div></form></div></div><!-- add button --><div class=row><div class=white-pannel><div class=row><button class="btn-link expand-roundicon" type=button ng-click=vm.addNewInsured(); ng-class="{\'disabled\' : vm.saveDisabled}" ng-disabled=vm.saveDisabled><!-- ng-class="{\'disabled\' : vm.saveDisabled}" ng-disabled="{{vm.saveDisabled}}" --> Add a named insured</button></div></div></div><form data-toggle=validator class=formClass form-submit-validation="" name=referForm autocomplete=off novalidate><!-- Agent Contact Info --><div class=row><div class=white-pannel><h4 class=pannel-title>{{ vm.agentInfoHdr.datasetName }}</h4><div class=row><div ng-repeat="item in vm.formMeta.agentContactInfo" data-ng-include="\'app/partials/form-widget.html\'"></div></div></div></div><!-- Licensed --><div class=row><div class=white-pannel><h4 class=pannel-title>{{ vm.licInfoHdr.datasetName }}</h4><div class=row><div ng-repeat="item in vm.formMeta.licensedProducerSigningApplication" data-ng-include="\'app/partials/form-widget.html\'"></div></div></div></div><!-- Explanation of Application --><div class=row><div class=white-pannel><h4 class=pannel-title>{{ vm.explanationInfoHdr.datasetName }}</h4><div class=row><div ng-repeat="item in vm.formMeta.explanationOfApplication" data-ng-include="\'app/partials/form-widget.html\'"></div></div></div></div></form></div><!-- right panel --><div class=col-md-3><!-- box one --><div class="grad_col shadowy" style="margin-bottom: 30px;"><header><div><img ng-src="{{main.assetPath+\'icon-alert-blue.svg\'}}"></div><h1>Sorry, the quote cannot be completed in omnia</h1><svg xmlns=http://www.w3.org/2000/svg viewBox="0 0 100 100" preserveAspectRatio=none><polygon fill=#f7f7f7 points="0,100 100,0 100,100"/></svg></header><section><h4><!-- <b>REASON</b> --></h4><!-- <p class="grayedcolor">[ Reason for referrring to underwriter ]</p> --></section></div><!-- box two --><!-- <div class="boxed_reason shadowy">\n        <div class="alertBlock">\n          <span>[Reason]</span>\n          <img style="float:right;" ng-src="{{main.assetPath+\'icon-alert-blue.svg\'}}" />\n        </div>\n        <span>\n          An underwriter can help understand and complete this application due to the risk associated with it\n        </span>\n      </div> --><!-- button box --><div class=right-rail><div class="panel panel-default" style="background-color: transparent"><!-- <button type="button" ng-hide="vm.viewMode" class="btn btn-lg btn-primary btn-block">\n            Chat with Underwriter\n          </button> --><!-- ng-disabled="userForm.$invalid" --> <button type=button class="btn btn-lg btn-success btn-block" ng-click=vm.triggerSubmit(referForm.$valid,referForm)>Refer to Underwriter</button></div></div></div></div></div>');
$templateCache.put('app/views/refer/refer_redirect.html','<section class=top><div><div data-ng-include="\'app/partials/loggedout.html\'"></div><div id=header class="navbar navbar-default navbar-fixed-top"><div class=navbar-header><button class="navbar-toggle collapsed" type=button data-toggle=collapse data-target=.navbar-collapse><i class=icon-reorder></i></button> <a class=navbar-brand href=#><img ng-src="{{vm.assetPath+\'logo.png\'}}" alt="Utica National Insurance Group"></a></div></div></div></section><div id=wrapper><div id=main-wrapper class="col-md-12 text-center"><div id=main><p>&nbsp;</p><p>&nbsp;</p><h4>You have successfully referred your application to an underwriter.</h4><div class=text-center style=margin-top:30px;><button class="btn btn-lg btn-primary" type=button ng-click=vm.redirectToDashboard()>Return to dashboard</button></div></div></div></div><script type=text/ng-template id=warning-dialog.html><div idle-countdown="countdown" class="modal-body text-center">\n    Your session will end in {{countdown}} seconds\n  </div></script>');
$templateCache.put('app/views/underwriting/nav.underwriting.html','<span class=summary-leftnav><br><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.businessdetails>Business Details</a></span> <span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.lob>Select lines of business</a></span> <span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.qualification>Qualification Questions</a></span><hr><h4 class=eligibility-panels>Business Owners Policy</h4><span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.quote>Start your quote</a></span> <span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.location>Locations and Buildings</a></span> <span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.additionalinterest>Additional Interests</a></span><ul class="nav list-group eligibility-panel"><li><a class="list-group-item active" href=""><i class="icon-home icon-1x"></i>Underwriting Questions</a></li><li><a class="list-group-item locked" href=""><i class="icon-home icon-1x"></i>Pricing & Coverages</a></li></ul><div style="position: absolute;bottom:0px;color: #909090;padding-left: 5px;">Copyright \xA9 2018 Utica Mutual Insurance Company, New Hartford, NY 13413 | All Rights Reserved</div>');
$templateCache.put('app/views/underwriting/underwriting.html','<div class=underwriting-page><div id=main-wrapper1><form class=formClass name=underwritingForm id=underwritingFormID form-submit-validation="" data-toggle=validator ng-submit=vm.triggerSubmit(underwritingForm.$valid,underwritingForm) novalidate autocomplete=off><div class=row><div class=col-md-9><h1 class=headUnd>Underwriting Questions</h1><div><div id=bname class="card no-margin"><div class=card-block><div class=row><!-- Question Section --><div class="col-md-12 underwritingQuestion" ng-repeat="question in vm.underwritingQuestions" ng-init="sectionIndex = $index" ng-if="question.appearUI.toLowerCase() == \'y\'"><!-- <div class="row" > --><div class="col-md-8 ques"><p>{{question.datasetName}}</p></div><!-- <div class="col-md-6" > --><div class=form-group ng-class="{ \'has-error\': underwritingForm.submitted &&\n                                                                   vm.underwritingForm.userValue[question.systemRefId] == \'\' } "><span class="col-md-2 checkradio-wrap" ng-repeat="option in question.valueName" ng-if=" question.appearUI.toLowerCase() == \'y\' && question.controlName.toLowerCase() == \'radio\'"><input type=radio name={{question.systemRefId|dotWithUnderscore}} value={{option.value}} id={{sectionIndex*2+$index}} ng-required={{question.isRequired}} ng-model=vm.underwritingForm.userValue[question.systemRefId] ng-change=vm.onRadioBtnChange($index)> <label for={{sectionIndex*2+$index}}>{{option.caption}}</label></span></div><p ng-show="underwritingForm.submitted &&\n                              underwritingForm.{{question.systemRefId|dotWithUnderscore}}.$invalid &&\n                              underwritingForm.{{question.systemRefId|dotWithUnderscore}}.$touched" class="underwritingError error err help-block">This field is required</p></div></div><!-- Question section ends--><!-- End of 3rd Row --><!--Prior policy and carrier info section--><div class="row pol"><div class=col-md-6 ng-repeat="policy in vm.policy " ng-if="policy.controlName.toLowerCase()==\'header\' "><div class=form-group><p><strong>{{policy.datasetName}}</strong></p></div></div></div><!--From and to date--><div class=row><div class=col-md-6 ng-repeat="policy in vm.policy " ng-if="policy.controlName.toLowerCase()==\'date\' "><div class=form-group ng-class="{ \'has-error\' : ( underwritingForm.submitted &&\n                                                                      underwritingForm.{{policy.systemRefId|dotWithUnderscore}}.$invalid &&\n                                                                      underwritingForm.{{policy.systemRefId|dotWithUnderscore}}.$touched) ||\n                                                                      (vm.dateFormatError[$index])} "><label>{{policy.datasetName}}</label><!-- <input placeholder="Please Select"\n                              ng-click="vm.open($index ,$event)"\n                              ng-disabled="vm.dateDisabled[$index]"\n                              data-uuid="{{policy.systemRefId}}"\n                              id="cal{{$index}}"\n                              show-button-bar="false"\n                              type="text"\n                              name="{{policy.systemRefId|dotWithUnderscore}}"\n                              ng-value = "{{policy.userValue}}"\n                              class="lob form-control dtpicker"\n                              uib-datepicker-popup={{vm.format}}\n                              ng-model="vm.underwritingForm.userValue[policy.systemRefId]"\n                              is-open="vm.opened[$index]"\n                              datepicker-options="vm.dateOptions[$index]"\n                              ng-required="{{policy.isRequired}}"\n                              close-text="Close"\n                              datepicker-popup-template-url="app/partials/tpl.datepickerpopup.html"\n                              maxlength="10"\n                              ng-keyup="vm.keyupevt($index, $event)"\n                              ng-blur="vm.keyupevt($index, $event); vm.changeFormat($event); vm.setDefaultDate($event, sysRefId, userValue);"\n                              as-date={{policy.userValue}} /> --> <input placeholder="Please Select" ng-click="vm.open($index ,$event)" id={{policy.systemRefId}} show-button-bar=false type=text name={{policy.systemRefId|dotWithUnderscore}} class="lob form-control dtpicker" uib-datepicker-popup={{vm.format}} ng-model=vm.underwritingForm.userValue[policy.systemRefId] is-open=vm.opened[$index] datepicker-options=vm.dateOptions[$index] ng-required=true close-text=Close datepicker-popup-template-url=app/partials/tpl.datepickerpopup.html ng-change="vm.changeDate(policy.systemRefId, vm.underwritingForm.userValue[policy.systemRefId],$index)" maxlength=10 date-val><p id=" hideOnKeyEvt" ng-show="( underwritingForm.submitted &&\n                                      underwritingForm.{{policy.systemRefId|dotWithUnderscore}}.$invalid &&\n                                      underwritingForm.{{policy.systemRefId|dotWithUnderscore}}.$touched)" class="help-block error build22 err">{{policy.datasetName | camelCase}} is required</p><p ng-if=vm.dateFormatError[$index] class="help-block error err build22">{{vm.message[$index]}}</p></div></div></div><!--Policy premium and carrier name --><div class=row><div class=col-md-6 ng-repeat="policy1 in vm.policy" ng-if="policy1.controlName.toLowerCase()==\'text\'"><div class=form-group ng-class="{ \'has-error\': (underwritingForm.submitted &&\n                                                                    underwritingForm.{{policy1.systemRefId|dotWithUnderscore}}.$touched &&\n                                                                    underwritingForm.{{policy1.systemRefId|dotWithUnderscore}}.$invalid) ||\n                                                                    (vm.policyPremiumSave == 1 &&\n                                                                     policy1.systemRefId == \'UNIG_PriorCarrierInformationInput.PolicyPremium\')}"><label>{{policy1.datasetName}}</label> <input type={{policy1.inputType}} name={{policy1.systemRefId|dotWithUnderscore}} class=form-control ng-focus="vm.policyPremiumSave = 0;\n                                      vm.underwritingForm.userValue[policy1.systemRefId] = vm.replaceCommaDollar(vm.underwritingForm.userValue[policy1.systemRefId])" ng-model=vm.underwritingForm.userValue[policy1.systemRefId] ng-required={{policy1.isRequired}} ng-keydown="vm.setLength($event, policy1.systemRefId, vm.underwritingForm.userValue[policy1.systemRefId])" placeholder={{policy1.additionalInfo}} ng-blur="vm.underwritingForm.userValue[policy1.systemRefId] = vm.maskCurrency(policy1.systemRefId ,vm.underwritingForm.userValue[policy1.systemRefId]);" ng-maxlength="{\n                                            40:policy1.systemRefId == \'UNIG_PriorCarrierInformationInput.CarrierName\',\n                                            14:policy1.systemRefId == \'UNIG_PriorCarrierInformationInput.PolicyPremium\'\n                              }" paste-number-only="{\n                                                     14:{{policy1.systemRefId == \'UNIG_PriorCarrierInformationInput.PolicyPremium\'}}\n                              }" paste-alpha-numeric-symbol="{\n                                                    40:{{policy1.systemRefId == \'UNIG_PriorCarrierInformationInput.CarrierName\'}}\n                              }" pasteboolean={{vm.pasteData}} ng-paste="vm.pasteData = true" input-length-limit ng-attr-format-currency="{{ policy1.systemRefId == \'UNIG_PriorCarrierInformationInput.PolicyPremium\' ? true : false }}" systemrefid={{policy1.systemRefId}} uservalue={{vm.underwritingForm.userValue[policy1.systemRefId]}}><p ng-show="(underwritingForm.submitted &&\n                                      underwritingForm.{{policy1.systemRefId|dotWithUnderscore}}.$touched &&\n                                      underwritingForm.{{policy1.systemRefId|dotWithUnderscore}}.$invalid)" class="help-block error build22 err"><!-- {{policy1.systemRefId}} --> {{policy1.datasetName | camelCase}} is required</p><p ng-if="policy1.systemRefId  == \'UNIG_PriorCarrierInformationInput.PolicyPremium\' &&\n                                   vm.policyPremiumBool == true &&\n                                  vm.underwritingForm.userValue[policy1.systemRefId] " ng-show="vm.policyPremiumSave == 1" class="help-block error build22 err">{{vm.policyPremiumErrorMesg}}</p></div></div></div></div></div></div></div></div><!-- End of Container Class --><div class=row><div class=col-md-9><div class=button-container><button type=submit class="btn btn-blue" style=opacity:1>Save &amp; Continue</button></div></div></div></form></div></div>');
$templateCache.put('app/views/building/building.html','<div class=location-page><!-- heading --><div class=row><div class=col-md-12><div class=col-md-6><h1>Locations and Buildings</h1></div></div></div><div class=row><div class=col-md-9><!-- error --><div class=card ng-show=vm.geoError><div class=card-block><p class="help-block error">Geocoding is currently unavailable</p></div></div><!-- card--><div class=card><!-- location form --><form name=locationForm autocomplete=off novalidate><div class=card-block><!-- location name --><div class=form-group><h4>Location 1</h4></div><div class=row><!-- form control widgets --><div ng-repeat="item in vm.formMeta.LocationAddress track by $index"><div><!-- Textbox --><div class=col-md-{{item.screenColumns}} ng-if="item.controlName == \'Text_Box\' && item.appearUI == \'Y\'"><div class=form-group ng-class="{\'has-error\': locationForm.$submitted && locationForm.{{item.systemRefId | dotWithUnderscore}}.$invalid || (item.systemRefId== \'LocationInput.State\' && vm.stateErrMsg != null)}"><label for="{{item.systemRefId | dotWithUnderscore}}">{{item.datasetName}}</label> <input type={{item.inputType}} name="{{item.systemRefId | dotWithUnderscore}}" class=form-control ng-model=item.userValue ng-readonly="item.editable == \'NNN\'" ng-maxlength="{  5:item.systemRefId == \'LocationInput.ZipCode\', 40:item.systemRefId == \'LocationInput.Address1\' || item.systemRefId == \'LocationInput.Address2\', 30:item.systemRefId == \'LocationInput.City\'}" input-length-limit placeholder={{item.additionalInfo}} ng-required=item.isRequired id="{{item.systemRefId | dotWithUnderscore}}" uservalue={{item.userValue}} ng-change="item.systemRefId == \'LocationInput.State\' ?\n                        vm.stateValidation(item.userValue, item.systemRefId) : \'\';" ng-blur="item.systemRefId == \'LocationInput.ZipCode\' ? vm.zipCodeLookUp(item.userValue, locItem.locationId) : \'\';"><!-- Required Validation --><p ng-show="(locationForm.$submitted && locationForm.{{item.systemRefId | dotWithUnderscore}}.$invalid)" class="help-block error build22">{{item.datasetName | camelCase}} is required</p><p ng-if="item.systemRefId==\'LocationInput.State\' && vm.stateErrMsg != null && item.userValue" class="help-block error build22">{{vm.stateErrMsg}}</p></div></div><!-- end Textbox --><!-- Text Area --><div class=col-md-{{item.screenColumns}} ng-if="item.controlName == \'Text_Area\' && item.appearUI == \'Y\'"><div class=form-group ng-class="{\'has-error\': locationForm.$submitted && locationForm.{{item.systemRefId | dotWithUnderscore}}.$invalid}"><label for="{{item.systemRefId | dotWithUnderscore}}">{{item.datasetName}}</label> <textarea name="{{item.systemRefId | dotWithUnderscore}}" class=form-control ng-model=item.userValue placeholder={{item.additionalInfo}} ng-required=item.isRequired id="{{item.systemRefId | dotWithUnderscore}}" rows=5></textarea><!-- Required Validation --><p ng-show="(locationForm.$submitted && locationForm.{{item.systemRefId | dotWithUnderscore}}.$invalid)" class="help-block error build22">{{item.datasetName | camelCase}} is required</p></div></div><!-- end Text Area --><!-- Radio --><div class="col-md-{{item.screenColumns}} checkradio-wrap" ng-if="item.controlName == \'Radio_Button\' && item.appearUI == \'Y\'"><div class=row><div class=col-md-12><p><strong>{{item.datasetName}}</strong></p></div></div><div class=row><div class="col-md-4 form-group" ng-repeat="value in item.valueName" ng-class="{ \'has-error\': locationForm.$submitted && locationForm.{{item.systemRefId | dotWithUnderscore}}.$invalid}"><input type=radio name="{{item.systemRefId | dotWithUnderscore}}" value={{value}} id="{{item.systemRefId | dotWithUnderscore}}-{{$index}}" ng-model=item.userValue ng-required=item.isRequired> <label for="{{item.systemRefId | dotWithUnderscore}}-{{$index}}">{{value}}</label></div></div><div class=col-md-12><!-- Required Validation --><p ng-show="locationForm.$submitted && locationForm.{{item.systemRefId | dotWithUnderscore}}.$invalid" class="help-block error build22">This field is required</p></div></div><!-- End Radio --><!-- Dropdown --><div class=col-md-{{item.screenColumns}} ng-if="item.controlName == \'Drop_Down\' && item.appearUI == \'Y\'"><div class=form-group ng-class="{\'has-error\': (locationForm.$submitted && locationForm.{{item.systemRefId | dotWithUnderscore}}.$invalid) }"><label>{{item.datasetName}}</label> <select name="{{item.systemRefId | dotWithUnderscore}}" ng-model=item.userValue class="custom_select form-control" ng-required=item.isRequired ng-blur="item.systemRefId == \'LocationBusinessOwnersInput.UNIG_Territory\' ? vm.protectionClassLookup(item.userValue, locItem.locationId) : \'\';"><option value="">Please Select</option><option ng-repeat="option in item.valueName track by $index" value={{option.value}}>{{option.caption}}</option></select><!-- Required Validation --><p ng-show="locationForm.$submitted && locationForm.{{item.systemRefId | dotWithUnderscore}}.$invalid" class="help-block error build22">{{item.datasetName | camelCase}} is required</p></div></div><!-- End Dropdown --><!-- Checkbox --><div class=col-md-{{item.screenColumns}} ng-if="item.controlName == \'Check_Box\' && item.appearUI == \'Y\'"><div class=checkbox-wrap><input type=checkbox class=checkbox id="{{item.systemRefId | dotWithUnderscore}}" name="{{item.systemRefId | dotWithUnderscore}}" ng-model=item.userValue> <label for="{{item.systemRefId | dotWithUnderscore}}" class=checkbox-label>{{item.datasetName}}</label></div></div><!-- End Checkbox --><div class=clearfix ng-if=item.hasRowBreak></div></div></div><!-- end form control widgets --><!-- save button --><div style=text-align:right><button type=submit class="btn btn-sm btn-primary buttonloc1">Save</button></div><!-- end button --> <span class=clearfix></span></div></div></form><!-- end location form --></div><!-- end card --></div></div></div>');
$templateCache.put('app/views/building/edit-building.html','<form name=vm.locationForm autocomplete=off novalidate><div class=card-block ng-repeat="currLocItem in vm.explocData track by $index"><!-- location name --><div class=form-group><h4>Location {{vm.locID}}</h4><!-- info --><h6 class="card-subtitle mb-2 text-muted">All addresses must be in the United States of America</h6></div><div class=row><div ng-repeat="item in currLocItem.LocationAddress track by $index"><!-- text box widget --><div class=col-md-{{item.screenColumns}} ng-if="item.controlName==\'Text_Box\'"><div class=form-group ng-class="{ \n            \'has-error\' : vm.locationForm.{{item.systemRefId | dotWithUnderscore}}.$invalid && vm.locationForm.{{item.systemRefId | dotWithUnderscore}}.$touched ||\n            ((item.systemRefId == \'LocationInput.ZipCode\') &&\n            (vm.lengthOfZip)) ||(item.systemRefId == \'LocationInput.ZipCode\' && vm.stateErrMsg != null) }"><!-- label --> <label>{{item.datasetName}}</label><!-- control --> <input type=text name="{{item.systemRefId | dotWithUnderscore}}" ng-model=item.userValue placeholder={{item.additionalInfo}} class=form-control ng-maxlength="{  5:item.systemRefId == \'LocationInput.ZipCode\',\n            40:item.systemRefId == \'LocationInput.Address1\' ||\n            item.systemRefId == \'LocationInput.Address2\',\n            30:item.systemRefId == \'LocationInput.City\'}" input-length-limit uservalue={{item.userValue}} options=vm.streetAutocompleteOptions ng-required=item.isRequired ng-attr-zip-code="{{ item.systemRefId == \'LocationInput.ZipCode\' ? true : false }}" ng-change=" item.systemRefId == \'LocationInput.State\' ? \n             vm.stateValidation(item.userValue,item.systemRefId,item.eventName):\'\';\n             item.systemRefId == \'LocationInput.Address1\' ||\n             item.systemRefId == \'LocationInput.Address2\' ||\n              item.systemRefId == \'LocationInput.City\' ||\n               item.systemRefId == \'LocationInput.ZipCode\' ||\n                item.systemRefId == \'LocationInput.State\' ? vm.locationCallSystemRefId(currLocItem.LocationAddress,currLocItem.locationId):\'\';\n                vm.updateLoc(item,item.userValue,item.systemRefId,currLocItem);" ng-blur="\n             vm.lengthOfZip = item.systemRefId == \'LocationInput.ZipCode\' ?\n             vm.ziplengthmin($event,item.userValue,item.systemRefId) : false;\n             vm.zipCodeLookUP(item.systemRefId,item.eventName,vm.lengthOfZip,item.userValue,currLocItem.LocationAddress,currLocItem.locationId);\n             item.systemRefId == \'LocationInput.Address1\' ||\n             item.systemRefId == \'LocationInput.Address2\' ||\n              item.systemRefId == \'LocationInput.City\' ? vm.locationInputCall(item.userValue,item.systemRefId,currLocItem.locationId,currLocItem.LocationAddress):\'\';" restrict-character-input systemrefid={{item.systemRefId}} g-places-autocomplete><!-- message --><p ng-show="vm.locationForm.{{item.systemRefId | dotWithUnderscore}}.$invalid && vm.locationForm.{{item.systemRefId | dotWithUnderscore}}.$touched" class="help-block error">{{item.datasetName | camelCase}} is required</p><p ng-if="(item.systemRefId == \'LocationInput.ZipCode\')&&\n            (vm.lengthOfZip)" class="help-block error">Invalid Zip Code</p><p ng-if="item.systemRefId== \'LocationInput.ZipCode\' && vm.stateErrMsg != null && item.userValue" class="help-block error build22">{{vm.stateErrMsg}}</p></div></div><!-- dropdown widget --><div class=col-md-{{item.screenColumns}} ng-if="item.controlName==\'Drop_Down\'"><div class=form-group ng-class="{ \'has-error\' : vm.locationForm.{{item.systemRefId | dotWithUnderscore}}.$invalid && vm.locationForm.{{item.systemRefId | dotWithUnderscore}}.$touched }"><!-- label --> <label>{{item.datasetName}}</label><!-- select control --> <select name="{{item.systemRefId | dotWithUnderscore}}" ng-model=item.userValue class="custom_select form-control" ng-required=item.isRequired><option value="" selected>Select a {{item.datasetName | camelCase}}</option><option ng-repeat="items in item.valueName track by $index" value={{items.value}}>{{items.caption}}</option></select><!-- message --><p class="help-block error" ng-show="vm.locationForm.{{item.systemRefId | dotWithUnderscore}}.$invalid && vm.locationForm.{{item.systemRefId | dotWithUnderscore}}.$touched" class="help-block error">{{item.datasetName}} is required</p></div></div><div class=clearfix ng-if=item.hasRowBreak></div></div><!-- buttons --><div style=text-align:right><button type=button ng-click=vm.locationCancel(currLocItem,vm.locID,vm.eventMode) class="btn btn-sm btn-default buttonloc">Cancel</button> <button type=button ng-click=vm.saveLocation(currLocItem.locationId[0],vm.explocData,vm.locationForm,vm.eventMode,vm.locID) class="btn btn-sm btn-primary buttonloc1">Save</button></div><span class=clearfix></span></div></div></form>');
$templateCache.put('app/views/building/nav.building.html','<div><span class=summary-leftnav><br><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.businessdetails>Business Details</a></span> <span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.lob>Select lines of business</a></span> <span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.qualification>Qualification Questions</a></span><hr><h4 class=eligibility-panels>Business Owners Policy</h4><span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span><a class=list-group-item4 ui-sref=app.quote>Start your quote</a></span><ul class="nav list-group eligibility-panel"><!-- <li><a class="list-group-item" href=""><i class="icon-home icon-1x"></i>Eligibility</a></li> --><li><a class="list-group-item active" href=""><i class="icon-home icon-1x"></i>Locations and Buildings</a></li><li><a class="list-group-item locked" href=""><i class="icon-home icon-1x"></i>Additional Interests</a></li><li><a class="list-group-item locked" href=""><i class="icon-home icon-1x"></i>Underwriting Questions</a></li><li><a class="list-group-item locked" href=""><i class="icon-home icon-1x"></i>Pricing & Coverages</a></li></ul></div><div style="position: relative;bottom:0px;color: #909090;padding-left: 5px;">Copyright \xA9 2018 Utica Mutual Insurance Company, New Hartford, NY 13413 | All Rights Reserved</div>');
$templateCache.put('app/views/eligibility/eligibility.html','<div class=eligibility-page><form class=formClass name=namedForm form-submit-validation="" ng-submit="vm.submitForm(namedForm.$valid, namedForm)" novalidate><div class=row><div class=white-pannel><h4 class=pannel-title>Insured Info</h4><div class=row><div ng-repeat="item in vm.formMeta.insuredInformation track by $index"><!-- Textbox --><div class=col-md-{{item.screenColumns}} ng-if="item.controlName == \'Text_Box\' && item.appearUI == \'Y\'"><div class=form-group ng-class="{\'has-error\': namedForm.submitted && namedForm.{{item.systemRefId}}.$touched && namedForm.{{item.systemRefId}}.$invalid}"><label for={{item.systemRefId}}>{{item.datasetName}}</label> <input type={{item.inputType}} name={{item.systemRefId}} class=form-control ng-model=item.userValue placeholder={{item.additionalInfo}} ng-required=item.isRequired id={{item.systemRefId}}><!-- Required Validation --><p ng-show="(namedForm.submitted && namedForm.{{item.systemRefId}}.$invalid && namedForm.{{item.systemRefId}}.$touched)" class="help-block error build22">{{item.datasetName | camelCase}} is required</p></div></div><!-- end Textbox --></div></div><div class=row><div class=empty-panel-formatter><div class="col-md-12 text-right"><button type=submit class="btn btn-blue">Save</button></div></div></div></div></div></form><div class=row><div class=col-md-12><button type=button class="btn btn-blue" ng-click=vm.addWidget()>Add more Widgets</button></div></div><form class=formClass name=eligibilityForm form-submit-validation="" ng-submit="vm.submitForm(eligibilityForm.$valid, eligibilityForm)" novalidate><!----><div class=row ng-repeat="widget in vm.formMeta.additionalInsuredInformation track by $index"><div class=white-pannel><h4 class=pannel-title>Insured Info</h4><div class=row><div ng-repeat="item in widget track by $index"><!-- Textbox --><div class=col-md-{{item.screenColumns}} ng-if="item.controlName == \'Text_Box\' && item.appearUI == \'Y\'"><div class=form-group ng-class="{\'has-error\': eligibilityForm.submitted && eligibilityForm.{{item.systemRefId}}.$touched && eligibilityForm.{{item.systemRefId}}.$invalid}"><label for={{item.systemRefId}}>{{item.datasetName}}</label> <input type=text name={{item.systemRefId}} class=form-control ng-model=item.userValue placeholder={{item.additionalInfo}} ng-required=item.isRequired id={{item.systemRefId}}><!-- Required Validation --><p ng-show="(eligibilityForm.submitted && eligibilityForm.{{item.systemRefId}}.$invalid && eligibilityForm.{{item.systemRefId}}.$touched)" class="help-block error build22">{{item.datasetName | camelCase}} is required</p></div></div><!-- end Textbox --></div></div></div></div><!----><div class=row><div class=white-pannel><h4 class=pannel-title>{{vm.agentInfoHdr.datasetName}}</h4><div class=row><div ng-repeat="item in vm.formMeta.agentContactInfo"><!-- Textbox --><div class=col-md-{{item.screenColumns}} ng-if="item.controlName == \'Text_Box\' && item.appearUI == \'Y\'"><div class=form-group ng-class="{\'has-error\': eligibilityForm.submitted && eligibilityForm.{{item.systemRefId}}.$touched && eligibilityForm.{{item.systemRefId}}.$invalid}"><label for={{item.systemRefId}}>{{item.datasetName}}</label> <input type={{item.inputType}} name={{item.systemRefId}} class=form-control ng-model=item.userValue placeholder={{item.additionalInfo}} ng-required=item.isRequired id={{item.systemRefId}}><!-- Required Validation --><p ng-show="(eligibilityForm.submitted && eligibilityForm.{{item.systemRefId}}.$invalid && eligibilityForm.{{item.systemRefId}}.$touched)" class="help-block error build22">{{item.datasetName | camelCase}} is required</p></div></div><!-- end Textbox --></div></div></div></div><div class=row><div class=empty-panel-formatter><div class=col-md-4><button type=submit class="btn btn-blue">Save</button></div></div></div></form></div>');
$templateCache.put('app/views/eligibility/nav.eligibility.html','<span class=summary-leftnav><br><span class=selected-plan style="display: inline-block;"></span> <a class=list-group-item4 ui-sref=app.businessdetails>Business Details</a> </span><span class=summary-leftnav><span class=selected-plan style="display: inline-block;"></span> <a class=list-group-item4 ui-sref=app.lob>Select lines of business</a></span><ul class="nav list-group eligibility-panel"><li><a class="list-group-item active" href=""><i class="icon-home icon-1x"></i>Qualification Questions</a></li></ul>');}]);