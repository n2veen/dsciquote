package com.wipro.dsci.quote.exception;

public class DSCIQuoteException extends RuntimeException {

		private static final long serialVersionUID = 1L;
		private String message;
		
		public DSCIQuoteException() {
			super();
		}
		
	    public DSCIQuoteException(Throwable cause) {
	        super(cause);
	    }
	    
		public DSCIQuoteException(String message) {
			super(message);
			this.message = message;
		}
		
	    public DSCIQuoteException(String message, Throwable cause) {
	        super(message, cause);
	    }
		 
	    @Override
	    public String toString() {
	        return message;
	    }
	 
	    @Override
	    public String getMessage() {
	        return message;
	    }

}

