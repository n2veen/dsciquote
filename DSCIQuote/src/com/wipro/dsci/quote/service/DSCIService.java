package com.wipro.dsci.quote.service;

import java.net.URI;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wipro.dsci.quote.enums.Actions;
import com.wipro.dsci.quote.exception.DSCIQuoteException;

@FunctionalInterface

public interface DSCIService<R, E extends Throwable> {

	// <T, U> R create(T a, U b);
	/**
	 * @param Actions
	 *            - ENUM containing operation types.
	 * @param requestData
	 * @return R
	 * @throws E
	 *             of type Exception
	 */
	R delegate(Actions actions, LinkedMultiValueMap<String, Object> requestData) throws E;

	/**
	 * 
	 * converting object to JSONString
	 * 
	 * @param object
	 * @return String
	 */
	public static String toJson(Object object) {

		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			throw new DSCIQuoteException(e);
		}
	}

	/**
	 * 
	 * method for convert url to uri
	 * 
	 * @param saveURL
	 * @return URI
	 */
	public static URI toURI(String saveURL) {

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(saveURL);
		return builder.build().toUri();

	}

	/**
	 * method to return JSON as string for APPIAN calls
	 * 
	 * @param responseFromAppian
	 * @return
	 * @throws JsonProcessingException
	 */

	public static String appianStringtoJson(ResponseEntity<String> responseFromAppian) throws JsonProcessingException {
		LinkedMultiValueMap<String, String> obj;

		obj = new LinkedMultiValueMap<String, String>();
		String response = responseFromAppian.getBody().toString();
		String[] index = response.split(";");
		System.out.println("index" + index.length);
		for (int i = 0; i < index.length; i++) {
			System.out.println(index[i]);
			String[] str = index[i].split(":");
			System.out.println("str" + str.length);
			obj.add(str[0], str.length > 1 ? str[1] : null);

		}

		return (new ObjectMapper().writeValueAsString(obj));
	}

	// log implementation
	public static void debug(String format, Object... args) {
		Logger logger = LoggerFactory.getLogger("DSCIQuote_dblogger");
		if (logger.isDebugEnabled()) {
			logger.debug(format, args);
		}
	}

	/**
	 * creating HttpEntity with appian credential while fetching data
	 * 
	 * @return HttpEntity
	 */
	@SuppressWarnings("rawtypes")
	public static HttpEntity getViewHttpEntity() {
		String credential = CreateElementService.getEnvironment().getProperty("appian.credentials");
		HttpHeaders httpHeaders = new HttpHeaders() {
			private static final long serialVersionUID = 1L;
			{
				set("Authorization", credential);
			}
		};
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON_UTF8));
		return new HttpEntity<>(httpHeaders);
	}

	/**
	 * creating HttpEntity with appian credential while fetching data
	 * 
	 * @return HttpEntity
	 */
	@SuppressWarnings("rawtypes")
	public static HttpEntity getSaveHttpEntity(Object data) {
		String credential = CreateElementService.getEnvironment().getProperty("appian.credentials");
		HttpHeaders httpHeaders = new HttpHeaders() {
			private static final long serialVersionUID = 1L;
			{
				set("Authorization", credential);
			}
		};
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON_UTF8));
		return new HttpEntity<>(data, httpHeaders);
	}

	/**
	 * Method to get uservalue by comparing system reference id from request
	 * data while saving
	 * 
	 * @param list
	 * @param value
	 * @return String
	 */
	public static String userValue(List<LinkedHashMap<String, Object>> list, String value) {
		List<LinkedHashMap<String, Object>> response = list.stream().filter(p -> p.get("systemRefId").equals(value))
				.collect(Collectors.toList());
		if (response.isEmpty()) {
			String message = CreateElementService.getEnvironment().getProperty("response.null");
			throw new DSCIQuoteException(message + " for " + value);
		} else {
			String userValue = response.get(0).get("userValue")!= null?response.get(0).get("userValue").toString():"";
			return userValue;
		}
	}
	
	/** To get the userValue from SessionData and add in the requestData 
	 * (if prefill values required in UI)
	 * 
	 * @param requestData
	 * @param systemRefId
	 * @param value
	 * @return response
	 */
	public static List<Map<String, String>> prefill(List<Map<String, String>> requestData, String systemRefId ,String value) {
		
		List<Map<String, String>> response = requestData.stream()
				.filter(list -> list.get("systemRefId").equals(systemRefId))
				.map(p -> {
			p.put("userValue", value);
			
			return p;
		}).collect(Collectors.toList());
		
		return response;
	}
	
	/** To get the data of particular systemRefID without userValue
	 * @param requestData
	 * @param systemRefId
	 * @return
	 */
	public static List<Map<String, String>> withoutPrefill(List<Map<String, String>> requestData, String systemRefId) {
		
		List<Map<String, String>> response = requestData.stream()
				.filter(list -> list.get("systemRefId") != null && list.get("systemRefId").equals(systemRefId))
				.map(p -> {
			
			return p;
		}).collect(Collectors.toList());

		return response;
	}

}
