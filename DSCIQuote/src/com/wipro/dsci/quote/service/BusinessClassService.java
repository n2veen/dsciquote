package com.wipro.dsci.quote.service;

import java.io.IOException;
import java.net.URI;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiFunction;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wipro.dsci.quote.enums.Actions;
import com.wipro.dsci.quote.exception.DSCIQuoteException;
import com.wipro.dsci.quote.model.BusinessAddress;
import com.wipro.dsci.quote.model.BusinessInfo;
import com.wipro.dsci.quote.model.SessionData;
import com.wipro.dsci.quote.util.DSCIUtility;

import static com.wipro.dsci.quote.service.DSCIService.debug;

import static com.wipro.dsci.quote.service.DSCIService.getSaveHttpEntity;
import static com.wipro.dsci.quote.service.DSCIService.toJson;
import static com.wipro.dsci.quote.service.DSCIService.userValue;

@org.springframework.stereotype.Service("BusinessClassService")
@SuppressWarnings({ "unchecked", "rawtypes" })
public class BusinessClassService<R, E> implements DSCIService<R, DSCIQuoteException> {

	@Autowired
	RestTemplate restTemplate;

	private Map<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>> opTypeMap = new HashMap<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>>() {

		private static final long serialVersionUID = 1L;
		{
			put(Actions.CREATECALL, (a, b) -> {
				return createCall(a, b);
			});
			put(Actions.CREATEBUSINESSDETAILS, (a, b) -> {
				return createCall(a, b);
			});

			put(Actions.SAVE_BDETAILS_IN_APPIAN, (a, b) -> {
				return saveBusinessDetailsInAppian(a, b);
			});

			put(Actions.EXPERIAN, (a, b) -> {
				return saveBusinessDetails(a, b);
			});

			put(Actions.SELECTEXPERIAN, (a, b) -> {
				return selectBusinessFromExperian(a, b);
			});

			put(Actions.VIEWCALL, (a, b) -> {
				return viewCall(a, b);
			});
			put(Actions.VIEWDISTINCTNAME, (a, b) -> {
				return viewCall(a, b);
			});
			put(Actions.VIEWBUSINESSDETAILSBYWIDGETID, (a, b) -> {
				return viewCall(a, b);
			});
			put(Actions.VIEWBUSINESSDETAILSBYENTITYTYPE, (a, b) -> {
				return viewCall(a, b);
			});
			put(Actions.VIEWBUSINESSDETAILS, (a, b) -> {
				return viewBusinessDetails(a, b);
			});
			put(Actions.FETCHPRODUCTID, (a, b) -> {
				return fetchProductId(a, b);
			});
			put(Actions.GETVERISK, (a, b) -> {
				return veriskCall(a, b);
			});
		}
	};

	@Override
	public R delegate(Actions actions, LinkedMultiValueMap<String, Object> requestData) throws RuntimeException {

		// null check and getting operationType

		String operationType = requestData.containsKey("operationType")
				? (String) requestData.get("operationType").get(0) : "";

		if (!opTypeMap.containsKey(Actions.valueOf(operationType.toUpperCase()))) {
			throw new IllegalArgumentException(String.format("%s is not supported", actions));
		}
		return opTypeMap.get(actions).apply(restTemplate, requestData);
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.wipro.dsci.quote.service.Service#createCall(org.springframework.web.
	 * client.RestTemplate, org.springframework.util.MultiValueMap)
	 */

	public R createCall(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {

		debug("Request:in createCall method:" + requestData);

		// setting uri
		String url = CreateElementService.getEnvironment().getProperty("url.create");

		URI uri = DSCIService.toURI(url);

		String request = DSCIService.toJson(requestData);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> entity = new HttpEntity<String>(request, headers);
		// sending data
		// ResponseEntity<String> response = restTemplate.postForEntity(uri,
		// entity,
		// String.class);
		System.out.println("Inside createcall");
		ResponseEntity<String> response = null;
		response = restTemplate.postForEntity(uri, entity, String.class);
		System.out.println("Response:-" + response);
		if (response.getBody().isEmpty()) {
			debug("Error: response is empty in createCall method.");
			String message = CreateElementService.getEnvironment().getProperty("response.null");
			throw new DSCIQuoteException(message);
		}
		debug("Response:in createCall method:" + response);
		return (R) response;
	}

	public R saveBusinessDetails(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {

		debug("Request:in saveBusinessDetailsInAppian method:" + requestData);
		// removing control data
		requestData.remove("elementType");
		requestData.remove("operationType");

		String businessDetailsUrl = CreateElementService.getEnvironment().getProperty("url.businessDetails");

		HttpSession session = (HttpSession) requestData.get("session").get(0);
		Object data = requestData.containsKey("data") ? requestData.get("data").get(0) : "";

		LinkedMultiValueMap<String, Object> requestMap = new LinkedMultiValueMap<>();

		requestMap.add("clientId", session.getAttribute("clientId"));
		// System.out.println("id from request"+requestMap.get("clientId"));

		requestMap.add("operationType", "EXPERIAN");
		requestMap.add("data", data);
		requestMap = DSCIUtility.updateRequestDataWithJWTToken(requestMap);
		// String dataToSave = toJson(requestMap) + "##SessionDetails##" + "";

		// preparing header for rest call

		// HttpEntity saveEntity = getSaveHttpEntity(dataToSave);
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON_UTF8));

		// rest call
		ResponseEntity<String> response = restTemplate.postForEntity(businessDetailsUrl,
				new HttpEntity<>(requestMap, httpHeaders), String.class);
		if (response.getBody().isEmpty()) {
			debug("Error: response is empty in saveBusinessDetailsInAppian method.");
			String message = CreateElementService.getEnvironment().getProperty("response.null");
			throw new DSCIQuoteException(message);
		}

		debug("Response:in saveBusinessDetailsInAppian method:" + response);
		return (R) response;
	}

	public R selectBusinessFromExperian(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {

		debug("Request:in saveBusinessDetailsInAppian method:" + requestData);
		// removing control data
		requestData.remove("elementType");
		requestData.remove("operationType");

		String businessDetailsUrl = CreateElementService.getEnvironment().getProperty("url.businessDetails");

		Object data = requestData.containsKey("data") ? requestData.get("data").get(0) : "";

		LinkedMultiValueMap<String, Object> requestMap = new LinkedMultiValueMap<>();

		requestMap.add("operationType", "SELECTBUSINESS");
		requestMap.add("data", data);
		requestMap = DSCIUtility.updateRequestDataWithJWTToken(requestMap);
		// String dataToSave = toJson(requestMap) + "##SessionDetails##" + "";

		// preparing header for rest call

		// HttpEntity saveEntity = getSaveHttpEntity(dataToSave);
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON_UTF8));

		ParameterizedTypeReference<Map<String, Object>> parameterizedTypeReference = new ParameterizedTypeReference<Map<String, Object>>() {
		};

		// rest call
		ResponseEntity<Map<String, Object>> responseEntity = restTemplate.exchange(businessDetailsUrl, HttpMethod.POST,
				new HttpEntity<>(requestMap, httpHeaders), parameterizedTypeReference);

		Map<String, Object> response = responseEntity.getBody();

		((HttpSession) requestData.get("session").get(0)).setAttribute("clientId", response.get("clientId"));

		debug("Response:in saveBusinessDetailsInAppian method:" + response);
		return (R) responseEntity;
	}

	public R fetchProductId(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {

		debug("Request:in saveBusinessDetailsInAppian method:" + requestData);
		// removing control data
		requestData.remove("elementType");
		requestData.remove("operationType");

		String businessDetailsUrl = CreateElementService.getEnvironment().getProperty("url.businessDetails");

		Map<String, String> data = requestData.containsKey("data")
				? (HashMap<String, String>) requestData.get("data").get(0) : new HashMap<String, String>();

		String businessClassName = data.get("businessClass");
		if (businessClassName.contains("]")) {
			data.put("businessClass", businessClassName.split("] ")[1]);
		} else {
			data.put("businessClass", businessClassName);
		}

		LinkedMultiValueMap<String, Object> requestMap = new LinkedMultiValueMap<>();

		requestMap.add("operationType", "FETCHPRODUCTID");
		requestMap.add("data", data);

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON_UTF8));

		ParameterizedTypeReference<Map<String, String>> parameterizedTypeReference = new ParameterizedTypeReference<Map<String, String>>() {
		};

		// rest call
		ResponseEntity<Map<String, String>> responseEntity = restTemplate.exchange(businessDetailsUrl, HttpMethod.POST,
				new HttpEntity<>(requestMap, httpHeaders), parameterizedTypeReference);

		return (R) responseEntity;
	}

	public R saveBusinessDetailsInAppian(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {

		debug("Request:in saveBusinessDetailsInAppian method:" + requestData);
		// removing control data
		requestData.remove("elementType");
		requestData.remove("operationType");
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();
		Object data = requestData.containsKey("data") ? requestData.get("data").get(0) : "";
		String saveURL = CreateElementService.getEnvironment().getProperty("url.saveQuoteDetails");
		String policyId = session.getAttribute("Id").toString();
		String QuoteSection = "2";
		saveURL += "?&policy_id=" + policyId + "&quote_section=" + QuoteSection;
		String dataToSave = toJson(data) + "##SessionDetails##" + toJson(sessionData);
		HttpEntity saveEntity = getSaveHttpEntity(dataToSave);
		session.setAttribute("businessDetailsJson", dataToSave);
		
		LinkedHashMap<String, Object> extractData = (LinkedHashMap<String, Object>) requestData.get("data").get(0);
		// Session Data
		List<LinkedHashMap<String, Object>> listOfBC = (List<LinkedHashMap<String, Object>>) extractData
				.get("Business_Class");
		String businessclassname = listOfBC.get(1).get("userValue").toString();

		List<LinkedHashMap<String, Object>> listOfBA = (List<LinkedHashMap<String, Object>>) extractData
				.get("Business_Address");

		if (listOfBA.get(0) == null)
			throw new DSCIQuoteException("No data found inside list!");

		String street = userValue(listOfBA, "mailingAddress.address1");
		String suite = userValue(listOfBA, "mailingAddress.address2");
		String city = userValue(listOfBA, "mailingAddress.city");
		String state = userValue(listOfBA, "mailingAddress.state");
		String zip = userValue(listOfBA, "mailingAddress.ZIP");
		List<LinkedHashMap<String, Object>> listOfBI = (List<LinkedHashMap<String, Object>>) extractData
				.get("Business_Info");
		String year = userValue(listOfBI, "misc.YearBusinessStarted");
		String sales = userValue(listOfBI, "misc.TotalAnnualSales");
		String phone = userValue(listOfBI, "BusinessContact.BusinessPhone");
		List<LinkedHashMap<String, Object>> businessNameWidgets = (List<LinkedHashMap<String, Object>>) extractData
				.get("Business_Name");
		String businessName = userValue(businessNameWidgets, "clientDetails.name");

		String newBusinessclassname;
		if (businessclassname.contains("]")) {
			newBusinessclassname = businessclassname.split("] ")[1];
		} else {
			newBusinessclassname = businessclassname;
		}

		// setting business class name
		sessionData.setBusinessClass(newBusinessclassname);

		// setting business address
		BusinessAddress businessAddress = new BusinessAddress();
		businessAddress.setStreetAddress(street);
		businessAddress.setSuite(suite);
		businessAddress.setCity(city);
		businessAddress.setState(state);
		businessAddress.setZip(zip);
		businessAddress.setBusinessName(businessName);
		sessionData.setBusinessAddress(businessAddress);

		// setting business info
		BusinessInfo businessInfo = new BusinessInfo();
		businessInfo.setYear(year);
		businessInfo.setSales(sales);
		businessInfo.setPhone(phone);
		sessionData.setBusinessInfo(businessInfo);

		// setting channel
		sessionData.setChannel("Independent_Agent");
		session.setAttribute("sessionData", sessionData);

		URI uri = DSCIService.toURI(saveURL);

		CompletableFuture.runAsync(()->{restTemplate.exchange(uri, HttpMethod.POST, saveEntity, String.class);});
		return (R) new ResponseEntity(HttpStatus.OK);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.wipro.dsci.quote.service.Service#viewCall(org.springframework.web.
	 * client. RestTemplate, java.util.Map)
	 */
	public R viewCall(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {

		debug("Request:in viewCall method:" + requestData);
		String url = CreateElementService.getEnvironment().getProperty("url.view");
		ResponseEntity<List> response = null;
		response = restTemplate.postForEntity(url, requestData, List.class);
		if (response.getBody().isEmpty()) {
			debug("Error: response is empty in viewCall method.");
			String message = CreateElementService.getEnvironment().getProperty("response.null");
			throw new DSCIQuoteException(message);
		}
		debug("Response:in viewCall method:" + response);
		return (R) response;
	}

	/**
	 * @param restTemplate
	 * @param requestData
	 * @return
	 * @throws RuntimeException
	 */
	public R viewBusinessDetails(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws RuntimeException {
		debug("Request:in viewBusinessDetails method:" + requestData);

		HttpSession session = (HttpSession) requestData.get("session").get(0);
		String policyId = (String) session.getAttribute("Id");
		String clientId = (String) session.getAttribute("clientId");
		SessionData sessionData = (SessionData) session.getAttribute("sessionData");
		String url = CreateElementService.getEnvironment().getProperty("url.getQuestion");
		String QuoteSection = "2";
		url += "?quote_section=" + QuoteSection + "&policy_id=" + policyId + "&fetch_master=No";

		if (clientId == null) {
			// transaction data
			url = CreateElementService.getEnvironment().getProperty("url.businessDetails");
		}

		URI uri = DSCIService.toURI(url);

		// creating HttpEntity
		LinkedMultiValueMap<String, Object> data = new LinkedMultiValueMap<>();
		data.add("operationType", "VIEWBUSINESSDETAILS");
		data.add("clientId", session.getAttribute("clientId"));
		HttpEntity entity = getSaveHttpEntity(data);
		debug("Connecting: viewBusinessDetails : to APPIAN with HttpEntity: " + entity);
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

		ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
		String[] separatedResponse = null;

		debug("Exit: viewBusinessDetails method of BusinessClassService in DSCIQuote with reponse: " + response);
		if (response.getBody().isEmpty()) {
			debug("Error: response is empty in viewBusinessDetails method.");
			String message = CreateElementService.getEnvironment().getProperty("response.null");
			throw new DSCIQuoteException(message);
		} else {
			separatedResponse = response.getBody().split("##SessionData##");
		}

		ResponseEntity<String> responseString;

		String[] separatedSessionAndTransactionalResponse = separatedResponse[0].split("--MidPayload--");
		if (separatedSessionAndTransactionalResponse.length > 1) {
			debug("sending transactional data");

			responseString = new ResponseEntity<String>(separatedSessionAndTransactionalResponse[1], HttpStatus.OK);
		} else {
			debug("sending master data");
			responseString = new ResponseEntity<String>(separatedSessionAndTransactionalResponse[0], HttpStatus.OK);
		}
		debug("Response:in viewBusinessDetails method:" + responseString);

		Map<String, Object> responseMap = null;

		try {
			responseMap = new ObjectMapper().readValue(responseString.getBody(),
					new TypeReference<Map<String, Object>>() {
					});
		} catch (IOException exception) {
			throw new DSCIQuoteException("Json Conversion failed");
		}
		responseMap.put("beforeYouBeginData", sessionData.getBeforeYouBeginData());
		responseMap.put("quoteCreated",
				session.getAttribute("quoteCreated") != null ? session.getAttribute("quoteCreated") : false);
		return (R) new ResponseEntity<Object>(responseMap, HttpStatus.OK);
	}

	/**
	 * @param restTemplate
	 * @param requestData
	 * @return
	 * @throws RuntimeException
	 *             for Verisk call
	 */
	public R veriskCall(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws RuntimeException {
		long startTime = System.currentTimeMillis();
		debug("Request:in veriskCall method:" + requestData);
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		SessionData sessionData = (SessionData) session.getAttribute("sessionData");
		ParameterizedTypeReference<Map<String, Object>> parameterizedTypeReference = new ParameterizedTypeReference<Map<String, Object>>() {
		};
		requestData.remove("elementType");
		requestData.remove("session");
		String url = CreateElementService.getEnvironment().getProperty("verisk.url");
		HttpEntity entity = getSaveHttpEntity(requestData);
		ResponseEntity<Map<String, Object>> response = restTemplate.exchange(url, HttpMethod.POST, entity,
				parameterizedTypeReference);

		// update liens count and bankruptcy count in session
		if (!response.getBody().isEmpty()) {
			Map<String, Object> veriskData = (Map<String, Object>) response.getBody().get("veriskData");
			if (veriskData != null) {
				List<Map<String, Object>> locations = (List<Map<String, Object>>) veriskData.get("locations");
				if (locations != null && !locations.isEmpty()) {
					Map<String, Object> location = locations.get(0);
					if (location != null) {
						List<Map<String, Object>> buildings = (List<Map<String, Object>>) location.get("buildings");
						if (buildings != null && !buildings.isEmpty()) {
							Map<String, Object> building = buildings.get(0);
							sessionData.setBankruptcyCount((String) building.get("bankruptcyCount"));
							sessionData.setLiensCount((String) building.get("liensCount"));
						}
					}
				}
			}
		}
		debug("Response:in veriskCall method:" + requestData + " executionTime:" + (System.currentTimeMillis() - startTime));
		return (R) response;
	}

}
