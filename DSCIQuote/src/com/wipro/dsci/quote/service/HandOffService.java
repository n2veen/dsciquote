package com.wipro.dsci.quote.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wipro.dsci.quote.enums.Actions;
import com.wipro.dsci.quote.exception.DSCIQuoteException;
import com.wipro.dsci.quote.model.SessionData;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.net.URI;
import java.util.*;
import java.util.function.BiFunction;

@Service("HandOffService")
@SuppressWarnings({ "unchecked", "rawtypes" })

@Component
public class HandOffService<R, E> implements DSCIService<R, DSCIQuoteException> {

	static Logger log = LoggerFactory.getLogger(HandOffService.class.getName());

	public HandOffService() {
	}

	private static Environment environment;

	@Autowired
	public HandOffService(Environment environment) {
		HandOffService.environment = environment;
	}

	@Autowired
	RestTemplate restTemplate;

	private Map<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>> opTypeMap = new HashMap<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>>() {

		private static final long serialVersionUID = 1L;
		{
			put(Actions.CLIENTDATA, (a, b) -> {
				return fetchClientDetails(a, b);
			});
		}
	};

	@Override
	public R delegate(Actions actions, LinkedMultiValueMap<String, Object> requestData) throws RuntimeException {

		// null check and getting operationType
		String operationType = requestData.containsKey("operationType")
				? (String) requestData.get("operationType").get(0)
				: "";
		if (!opTypeMap.containsKey(Actions.valueOf(operationType.toUpperCase()))) {
			throw new IllegalArgumentException(String.format("%s is not supported", actions));
		}
		return opTypeMap.get(actions).apply(restTemplate, requestData);
	}

	/**
	 * To validate incoming token through DC wrapper integration
	 * 
	 * @param jwtToken
	 * @return
	 * @throws RuntimeException
	 */
	public String validateToken(String jwtToken) throws RuntimeException {
		log.info("Enter into validateToken HandoffService : " + jwtToken);

		String validateUrl = CreateElementService.getEnvironment().getProperty("url.dct.auth");
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.set("Authorization", jwtToken);

		HttpEntity httpEntity = new HttpEntity<>(httpHeaders);
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> validateResponse = null;
		HashMap<String, String> validValue = null;
		try {
			validateResponse = restTemplate.exchange(validateUrl, HttpMethod.GET, httpEntity, String.class);
			log.info(" validateResponse : " + validateResponse.getBody());
			validValue = mapper.readValue(validateResponse.getBody(), HashMap.class);
		} catch (Exception e1) {
			log.info("error from DuckCreek validate token service call");
		}
		if (validValue != null && Boolean.TRUE.equals(validValue.get("IsValid"))) {
			return "success";
		} else
			return "failure";
	}

	/**
	 * For user authentication through DC wrapper integration
	 * 
	 * @param userName
	 * @param password
	 * @return
	 * @throws RuntimeException
	 */
	@SuppressWarnings("restriction")
	public HashMap<String, String> dcAuthentication(String userName, String password) throws RuntimeException {
		log.info("Enter into dcAuthentication HandoffService : " + userName + " " + password);
		HashMap<String, String> responseMap = new LinkedHashMap<String, String>();
		HashMap<String, String> requestPageSet = new HashMap<String, String>();
		requestPageSet.put("Username", userName);
		requestPageSet.put("Password", password);
		String authenticateUrl = CreateElementService.getEnvironment().getProperty("url.dct.auth");
		String authString = userName + ":" + password;
		final byte[] encodedBytes = Base64.encodeBase64(authString.getBytes());
		String authStringEnc = new String(encodedBytes);
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON_UTF8));
		httpHeaders.set(HttpHeaders.AUTHORIZATION, "Basic " + authStringEnc);
		HttpEntity httpEntity = new HttpEntity<>(DSCIService.toJson(requestPageSet), httpHeaders);
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> authenticationResponse = null;
		HashMap<String, String> responseValue = null;
		try {
			authenticationResponse = restTemplate.exchange(authenticateUrl, HttpMethod.POST, httpEntity, String.class);
			log.info(" Response body : " + authenticationResponse.getBody());
			responseValue = mapper.readValue(authenticationResponse.getBody(), HashMap.class);
			responseMap.put("JsonWebToken", responseValue.get("JsonWebToken"));
			responseMap.put("Status", responseValue.get("Status"));
		} catch (Exception e1) {
			log.info("Error from DuckCreek authentication service call");
			responseMap.put("JsonWebToken", null);
			responseMap.put("Status", "failure");
		}
		log.info("Exit from dcAuthentication HandoffService : " + responseMap.size());
		return responseMap;
	}

	/**
	 * To navigate thru DSCI pages
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return
	 * @throws RuntimeException
	 */
	public String dsciLandingPage(RestTemplate restTemplate, HashMap<String, String> requestData)
			throws RuntimeException {
		log.info("Request:in dsciLandingPage  method:" + requestData);

		String policy_id = requestData.get("policy_id");
		String action = requestData.get("action");

		String accountURL = environment.getProperty("url.resume") +"?policy_id="+ policy_id + "&action=" + action.replace(" ","_");

		URI uri = DSCIService.toURI(accountURL);
		HttpEntity entity = DSCIService.getSaveHttpEntity(new HashMap());

		String appState;
		String sessionData = null;

		ResponseEntity<String> accountResponse = restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);

		String appianResponse = accountResponse.getBody();
		String[] splitResponse = appianResponse.split("#####");

		if (splitResponse.length >= 2) {
			appState = splitResponse[0];
			sessionData = splitResponse[1];
		} else {
			appState = splitResponse[0];
		}
		SessionData sessionDataNew = new SessionData();
		HashMap<String, String> sessionDataMap = new LinkedHashMap<String, String>();
		ArrayList lobs=new ArrayList<String>();
		Map<String, Object> beforeYouBeginData =new LinkedHashMap<String, Object>();
		 List<HashMap<String, Object>> locationData=new ArrayList<HashMap<String, Object>>();
		ObjectMapper mapper1 = new ObjectMapper();
		try {
			sessionDataNew = mapper1.readValue(sessionData, SessionData.class);
			sessionDataMap = mapper1.readValue(sessionData, HashMap.class);
			Object lobls=(Object)sessionDataMap.get("lobs");
			lobs=(ArrayList<String>)lobls;
			Object beforeYouBeginDataStr= (Object)sessionDataMap.get("beforeYouBeginData");
			beforeYouBeginData= (LinkedHashMap<String, Object>)beforeYouBeginDataStr;
			sessionDataNew.setLobs(lobs);
			sessionDataNew.setBeforeYouBeginData(beforeYouBeginData);
		
		} catch (Exception e) {
			log.info("Exception from HandOffController#dsciLandingPage :" + e.getMessage());
		}
		ServletRequestAttributes requestAttribute = (ServletRequestAttributes) RequestContextHolder
				.currentRequestAttributes();
		
		HttpSession session = requestAttribute.getRequest().getSession();
		session.setAttribute("sessionData", sessionDataNew);

		log.info("dsciLandingPage method:" + appState);

		HashMap<String, String> appStateMap = new LinkedHashMap<String, String>();
		ObjectMapper mapper = new ObjectMapper();
		try {
			appStateMap = mapper.readValue(appState, HashMap.class);
		} catch (Exception e) {
			log.info("Exception from HandOffController#dsciLandingPage :" + e.getMessage());
		}
		log.info("Exit from HandOffController#dsciLandingPage :" + appStateMap.size() + "appState"
				+ appStateMap.get("state"));

		return appStateMap.get("state");
	}


	/**
	 * To fetch Client Details for SYQ
	 * @param clientId
	 * @return
	 * @throws RuntimeException
	 */
	public R fetchClientDetails(RestTemplate a, LinkedMultiValueMap<String, Object> requestData)
			throws RuntimeException {
		log.info("Request:in fetchClientDetails  method:");

		String clientId = (String) requestData.get("clientId").get(0);
		String jwtToken = (String) requestData.get("jwtToken").get(0);

		String clientDataURL = environment.getProperty("url.clientData");

		LinkedMultiValueMap<String, Object> requestValue = new LinkedMultiValueMap<String, Object>();
		List<Object> valueList = new ArrayList<Object>();
		valueList.add("CLIENTDATA");
		List<Object> inputList = new ArrayList<Object>();
		inputList.add(clientId);
		List<Object> tokenList = new ArrayList<Object>();
		tokenList.add(jwtToken);
		requestValue.put("operationType", valueList);
		requestValue.put("clientId", inputList);
		requestValue.put("jwtToken", tokenList);

		URI uri = DSCIService.toURI(clientDataURL);

		HttpEntity entity = DSCIService.getSaveHttpEntity(requestValue);

		ResponseEntity<String> clientDetailsResponse = restTemplate.exchange(uri, HttpMethod.POST, entity,
				String.class);
		log.info("Response:in fetchClientDetails method:" + clientDetailsResponse.hasBody());
		return (R) new ResponseEntity<Object>(clientDetailsResponse.getBody(), HttpStatus.OK);
	}

	/**
	 * To get outbound url
	 * @return
	 * @throws RuntimeException
	 */
	@SuppressWarnings("restriction")
	public String getOutBoundURL() throws RuntimeException {
		String outboundCEURL = environment.getProperty("url.outboundDataCE");
		return outboundCEURL;
	}

	/**
	 * To get outbound data
	 * @param landingPageInCE
	 * @param lob
	 * @return
	 * @throws RuntimeException
	 */
	@SuppressWarnings("restriction")
	public Map<String, Object> getOutBoundData(String landingPageInCE, String lob) throws RuntimeException {
		log.info("Request:in getOutBoundData  method:");
		String outBoundAppianURL = environment.getProperty("url.outboundDataAppian");
		HashMap<String, String> requestValue = new HashMap<String, String>();

		requestValue.put("var1", landingPageInCE);
		requestValue.put("var2", lob);

		URI uri = DSCIService.toURI(outBoundAppianURL + "?var1=" + landingPageInCE + "&var2=" + lob);

		HttpEntity entity = DSCIService.getSaveHttpEntity(requestValue);

		ResponseEntity<String> outBoundResponse = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
		Map<String, Object> responseMap = null;

		try {
			responseMap = new ObjectMapper().readValue(outBoundResponse.getBody(),
					new TypeReference<Map<String, Object>>() {
					});
		} catch (IOException exception) {
			throw new DSCIQuoteException("Json Conversion failed");
		}
		log.info("Response:in getOutBoundData method:" + outBoundResponse.hasBody());
		return responseMap;
	}

	/**
	 * To get environment key for Outbound Token Creation
	 * @return
	 * @throws RuntimeException
	 */
	@SuppressWarnings("restriction")
	public String getEnvironmentKey() throws RuntimeException {
		String evnKey = environment.getProperty("environment.key");
		return evnKey;
	}
	
	/**
	 * To update CE Landing Page for hand-off scenario with quoteId
	 * @param quoteId
	 * @param jwtToken
	 * @return
	 * @throws RuntimeException
	 */
	public String updateLandingPageInCE(String jwtToken, String quoteId) throws RuntimeException {
		log.info("Enter into updateLandingPageInCE HandoffService : " + quoteId + " jwtToken " + jwtToken );

		String baseUrl = CreateElementService.getEnvironment().getProperty("url.dct.auth");
		String authUrl = baseUrl + "?policyId=" + quoteId;
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.set("Authorization", jwtToken);

		HttpEntity httpEntity = new HttpEntity<>(httpHeaders);
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> updateResponse = null;
		HashMap<String, String> responseValue = null;
		try {
			updateResponse = restTemplate.exchange(authUrl, HttpMethod.PUT, httpEntity, String.class);
			log.info(" updateResponse : " + updateResponse.getBody());
			responseValue = mapper.readValue(updateResponse.getBody(), HashMap.class);
		} catch (Exception e1) {
			log.info("error from DuckCreek auth with policyId service call");
		}
		if (responseValue != null && responseValue.get("response").equals("success")) {
			return "success";
		} else
			return "failure";
	}
	
}
