package com.wipro.dsci.quote.service;

import static com.wipro.dsci.quote.service.DSCIService.debug;
import static com.wipro.dsci.quote.service.DSCIService.getSaveHttpEntity;
import static com.wipro.dsci.quote.service.DSCIService.toJson;
import static com.wipro.dsci.quote.util.DSCIUtility.updateRequestMapDataWithJWTToken;
import static com.wipro.dsci.quote.util.DSCIUtility.updateRequestStringMapDataWithJWTToken;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiFunction;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wipro.dsci.quote.enums.Actions;
import com.wipro.dsci.quote.exception.DSCIQuoteException;
import com.wipro.dsci.quote.model.SessionData;
import com.wipro.dsci.quote.util.DSCIUtility;

@org.springframework.stereotype.Service("RiskService")
public class RiskService<R, E> implements DSCIService<R, DSCIQuoteException> {

	@Autowired
	RestTemplate restTemplate;

	private Map<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>> opTypeMap = new HashMap<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>>() {

		private static final long serialVersionUID = 1L;
		{
			put(Actions.VIEWLOCATION, (a, b) -> {
				return viewLocation(a, b);
			});
			put(Actions.CANCELLOCATION, (a, b) -> {
				return cancelLocation(a, b);
			});
			put(Actions.SAVELOCATION, (a, b) -> {
				return saveLocation(a, b);
			});
			put(Actions.DELETELOCATION, (a, b) -> {
				return deleteLocation(a, b);
			});

			put(Actions.VIEW_EDIT_LOCATION, (a, b) -> {
				return viewEditLocation(a, b);
			});
			put(Actions.CANCEL_EDIT_LOCATION, (a, b) -> {
				return cancelEditLocation(a, b);
			});

			put(Actions.VIEW_BUILDING, (a, b) -> {
				return viewBuilding(a, b);
			});
			put(Actions.SAVEBUILDING, (a, b) -> {
				return saveBuilding(a, b);
			});
			put(Actions.UPDATEBUILDING, (a, b) -> {
				return updateBuilding(a, b);
			});
			put(Actions.CANCEL_NEW_BUILDING, (a, b) -> {
				return cancelNewBuilding(a, b);
			});
			put(Actions.EDITBUILDING, (a, b) -> {
				return editBuilding(a, b);
			});
			put(Actions.DELETEBUILDING, (a, b) -> {
				return deleteBuilding(a, b);
			});
			put(Actions.DUPLICATEBUILDING, (a, b) -> {
				return duplicateBuilding(a, b);
			});

			put(Actions.LOCATIONSERVICE, (a, b) -> {
				return validateAddress(a, b);
			});
			put(Actions.ZIPCODELOOKUP, (a, b) -> {
				return zipcodeLookup(a, b);
			});
			put(Actions.TERRITORY_LOOKUP, (a, b) -> {
				return territoryLookup(a, b);
			});
			put(Actions.SUMMARY_PAGE_SAVE_IN_APPIAN, (a, b) -> {
				return saveSummaryPageinAppian(a, b);
			});

		}
	};

	@Override
	public R delegate(Actions actions, LinkedMultiValueMap<String, Object> requestData) {

		// null check and getting operationType
		String operationType = requestData.containsKey("operationType")
				? (String) requestData.get("operationType").get(0)
				: "";

		if (!opTypeMap.containsKey(Actions.valueOf(operationType.toUpperCase()))) {
			throw new IllegalArgumentException(String.format("%s is not supported", actions));
		}
		return opTypeMap.get(actions).apply(restTemplate, requestData);
	}

	/**
	 * To get the data from DB for location page and Summary page
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return responseString
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R viewLocation(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in viewLocation  method:" + requestData);

		HttpSession session = (HttpSession) requestData.get("session").get(0);

		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData")
				: new SessionData();
				
		String policyId = (String) session.getAttribute("Id");
		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}

		String dataType = requestData.containsKey("dataType") ? requestData.get("dataType").get(0).toString() : "";
		String locQuoteSection = "7";

		// setting Appian URL
		String getURL = CreateElementService.getEnvironment().getProperty("url.getQuestion") + "?policy_id=" + policyId;
		String getLocURL = getURL + "&quote_section=" + locQuoteSection;

		// converting URL to URI
		URI getLocURI = DSCIService.toURI(getLocURL);

		// preparing header for rest call
		HttpEntity entity = getSaveHttpEntity(new HashMap());

		String[] separatedResponse = null;

		// rest call
		ResponseEntity<String> response = restTemplate.exchange(getLocURI, HttpMethod.POST, entity, String.class);
		if (response.getBody().isEmpty()) {
			debug("Error: response is empty in viewLocation method.");
			String message = CreateElementService.getEnvironment().getProperty("response.null");
			throw new DSCIQuoteException(message);
		} else {
			debug("Data from appian" + response);
			separatedResponse = response.getBody().split("##SessionData##");
		}
		ResponseEntity<String> responseString = null;
		String[] separatedMasterAndTransactionalResponse = separatedResponse[0].split("--MidPayload--");

		ObjectMapper mapper = new ObjectMapper();

		HashMap<String, Object> resMap = null;

		/*
		 * if (separatedResponse.length == 1) {
		 * 
		 * throw new DSCIQuoteException("Session Data not available"); } else {
		 */
		if (separatedMasterAndTransactionalResponse.length <= 1 || dataType.equalsIgnoreCase("Master")) {
			String firstLocation = separatedMasterAndTransactionalResponse.length <= 1 ? "true" : "false";

			HashMap<String, Object> req = new HashMap<>();
			req.put("elementType", "LOCATION");
			req.put("operationType", "VIEW_LOCATION");
			req.put("policyId", policyId);
			req.put("firstLocation", firstLocation);

			ResponseEntity<String> response1 = restTemplate.postForEntity(
					CreateElementService.getEnvironment().getProperty("url.location"),
					updateRequestMapDataWithJWTToken(req), String.class);

			try {
				resMap = mapper.readValue(response1.getBody(), HashMap.class);
			} catch (IOException e) {
				throw new DSCIQuoteException();
			}
		} else {

			try {
				resMap = mapper.readValue(separatedMasterAndTransactionalResponse[1], HashMap.class);
			} catch (IOException e) {
				throw new DSCIQuoteException();
			}
		}
		/*HashMap<String,String> resMap1 = new HashMap();

		resMap1.put("streetAddress","49 KEARNY AVE");
		resMap1.put("suite","KEARNY");
		resMap1.put("city","NJ");
		resMap1.put("state","");
		resMap1.put("zip","07032");
		resMap1.put("businessName","BELLA RE'S PIZZA");
				resMap.put("SYQ", Arrays.asList("NJ"));
				resMap.put("businessAddress",  Arrays.asList(resMap1));
				resMap.put("businessClass", Arrays.asList("203660"));*/
		resMap.put("SYQ", Arrays.asList(sessionData.getState()));
		resMap.put("businessAddress", sessionData.getBusinessAddress());
		resMap.put("businessClass", Arrays.asList(sessionData.getOperationCode()));
		responseString = new ResponseEntity<String>(toJson(resMap), HttpStatus.OK);
		return (R) responseString;
	}

	/**
	 * To cancel a new location (To delete the location id created in duckcreek)
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return
	 * @throws DSCIQuoteException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R cancelLocation(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws DSCIQuoteException {
		debug("Request:in cancelLocation method:" + requestData);

		HttpSession session = (HttpSession) requestData.get("session").get(0);
		String policyId = (String) session.getAttribute("Id");
		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}

		// null check and getting data for saving
		Object data = requestData.containsKey("data") ? requestData.get("data").get(0) : "";

		ObjectMapper mapper = new ObjectMapper();

		HashMap<String, Object> buildingData = null;
		try {
			buildingData = mapper.readValue(toJson(data), HashMap.class);
		} catch (IOException e1) {
			throw new DSCIQuoteException(e1);
		}

		// To save the data in db and dc
		HashMap<String, Object> req = new HashMap<>();
		req.put("elementType", "LOCATION");
		req.put("operationType", "DELETE_LOCATION");
		req.put("policyId", policyId);
		req.put("locationId", buildingData.get("locationId") != null ? buildingData.get("locationId").toString() : "");
		ResponseEntity<String> dcResponse = restTemplate.postForEntity(
				CreateElementService.getEnvironment().getProperty("url.location"),
				updateRequestMapDataWithJWTToken(req), String.class);

		if (dcResponse.getStatusCodeValue() == 200) {
			debug("Response:in cancelLocationInDC method:" + dcResponse);
			return (R) new ResponseEntity<>(toJson(dcResponse), HttpStatus.OK);
		} else {
			throw new DSCIQuoteException("Location is not canceled properly");
		}

	}

	/**
	 * To save the location transactional data in db and duckcreek (for both new
	 * location and existion location)
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return response
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R saveLocation(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws DSCIQuoteException {
		debug("Request:in saveLocation method:" + requestData);

		HttpSession session = (HttpSession) requestData.get("session").get(0);
		String policyId = (String) session.getAttribute("Id");
		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}

		String locQuoteSection = "7";

		// setting Appian URL
		String saveURL = CreateElementService.getEnvironment().getProperty("url.saveQuoteDetails") + "?policy_id="
				+ policyId;
		String getURL = CreateElementService.getEnvironment().getProperty("url.getQuestion") + "?policy_id=" + policyId;

		// to fetch the transaction data
		String getLocURL = getURL + "&quote_section=" + locQuoteSection;
		// to add the new loc data in transaction data
		String saveLocURL = saveURL + "&quote_section=" + locQuoteSection;

		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData")
				: new SessionData();

		// null check and getting data for saving
		Object data = requestData.containsKey("data") ? requestData.get("data").get(0) : "";

		HttpEntity getEntity = getSaveHttpEntity(new HashMap());

		ObjectMapper mapper = new ObjectMapper();

		HashMap<String, Object> LocationsData = new HashMap<>();
		HashMap<String, Object> LocationAddress = null;
		try {
			LocationAddress = mapper.readValue(toJson(data), HashMap.class);

		} catch (IOException e1) {
			throw new DSCIQuoteException(e1);
		}

		String locationId = ((List) LocationAddress.get("locationId")).get(0).toString();

		// Master and Transaction data from Appian
		String[] separatedMasterAndTransactionalResponse = servicecall(restTemplate, DSCIService.toURI(getLocURL),
				getEntity);

		LocationsData.put("LocationsData", LocationAddress);

		HashMap<String, Object> LocData = new HashMap<>();
		if (LocationAddress.get("subjectType") != null
				&& ((List) LocationAddress.get("subjectType")).get(0).toString().equalsIgnoreCase("edit")
				&& separatedMasterAndTransactionalResponse.length > 1) {

			try {
				System.out.println("-----------------------------" + separatedMasterAndTransactionalResponse[1]);
				LocData = mapper.readValue(separatedMasterAndTransactionalResponse[1], HashMap.class);
				System.out.println("LocData" + LocData);
			} catch (IOException e) {
				throw new DSCIQuoteException(e);
			}

			for (int i = 0; i < ((List) LocData.get("LocationsData")).size(); i++) {
				System.out.println("locId------"
						+ ((List) ((HashMap) ((List) LocData.get("LocationsData")).get(i)).get("locationId")).get(0));
				if (((List) ((HashMap) ((List) LocData.get("LocationsData")).get(i)).get("locationId")).get(0)
						.toString().equalsIgnoreCase(locationId)) {
					((HashMap) ((List) LocData.get("LocationsData")).get(i)).replace("LocationAddress",
							(List) LocationAddress.get("LocationAddress"));
					System.out.println("Loop : LocData----" + LocData);
					((HashMap) ((List) LocData.get("LocationsData")).get(i)).replace("locationName",
							(List) LocationAddress.get("locationName"));
				}
				break;
			}

			sessionData.setSubjectType("");
		} else {
			List<String> locIds = new ArrayList<>();
			if (separatedMasterAndTransactionalResponse.length > 1) {
				try {
					LocData = mapper.readValue(separatedMasterAndTransactionalResponse[1], HashMap.class);
				} catch (IOException e) {
					throw new DSCIQuoteException(e);
				}
				for (int i = 0; i < ((List) LocData.get("LocationsData")).size(); i++) {
					System.out.println("locId------"
							+ ((List) ((HashMap) ((List) LocData.get("LocationsData")).get(i)).get("locationId")).get(0));
					if (((List) ((HashMap) ((List) LocData.get("LocationsData")).get(i)).get("locationId")).get(0)
							.toString().equalsIgnoreCase(locationId)) {
						((HashMap) ((List) LocData.get("LocationsData")).get(i)).replace("LocationAddress",
								(List) LocationAddress.get("LocationAddress"));
						System.out.println("Loop : LocData----" + LocData);
						((HashMap) ((List) LocData.get("LocationsData")).get(i)).replace("locationName",
								(List) LocationAddress.get("locationName"));
					}else{
						((List<Object>) LocData.get("LocationsData")).add(LocationAddress);
					}
					break;
				}		
				locIds = sessionData.getLocationIds();
				locIds.add(locationId);
				sessionData.setLocationIds(locIds);
				

			} else {
				LocData.put("LocationsData", Arrays.asList(LocationAddress));
				locIds.add(locationId);
				sessionData.setLocationIds(locIds);

			}
		}

		/*
		 * HashMap<String, Object> id = new HashMap<>();
		 * System.out.println("LocationAddress----" + LocationAddress);
		 * id.put("locationId", LocationAddress.get("locationId"));
		 * id.put("BuildingIds", Arrays.asList()); HashMap<String, Object> idList = new
		 * HashMap<>(); idList.put("idList", Arrays.asList(id));
		 * session.setAttribute("ListOfIds", idList);
		 */

		String dataToSave = toJson(LocData) + "##SessionDetails##" + toJson(sessionData);
		System.out.println("session saved " + dataToSave);
		HttpEntity entity = getSaveHttpEntity(dataToSave);
		debug("saveLocationAndBuildingInAppian " + entity);

		if (requestData.containsKey("saveAndExit")) {
			session.invalidate();
			saveLocURL += "&action=SE";
			restTemplate.exchange(DSCIService.toURI(saveLocURL), HttpMethod.POST, entity, String.class);
			return (R) new ResponseEntity<String>("{\"status\":\"Success\"}", HttpStatus.OK);
		} else {
			saveLocURL += "&action=SC";
		}

		// To save the data in db and dc
		HashMap<String, Object> req = new HashMap<>();
		req.put("elementType", "LOCATION");
		req.put("operationType", "SAVE_LOCATION");
		req.put("subjectType",
				LocationAddress.get("subjectType") != null
						? ((List) LocationAddress.get("subjectType")).get(0).toString()
						: "");
		req.put("locationIndex",
				requestData.get("locationIndex") != null ? requestData.get("locationIndex").get(0) : "");
		req.put("policyId", policyId);
		req.put("data", Arrays.asList(LocationsData));
		ResponseEntity<String> saveResponseInDb = restTemplate.postForEntity(
				CreateElementService.getEnvironment().getProperty("url.location"),
				updateRequestMapDataWithJWTToken(req), String.class);

		if (saveResponseInDb.getStatusCodeValue() == 200) {
			debug("Response:in saveLocation method:" + saveResponseInDb);
			restTemplate.exchange(DSCIService.toURI(saveLocURL), HttpMethod.POST, entity, String.class);
			return (R) new ResponseEntity<>(toJson(LocData), HttpStatus.OK);
		} else {
			throw new DSCIQuoteException("Location is not saved properly");
		}
	}

	/**
	 * To delete a existing location and it's corresponding buildings
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return
	 * @throws DSCIQuoteException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R deleteLocation(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws DSCIQuoteException {
		debug("Request:in deleteLocation method:" + requestData);

		HttpSession session = (HttpSession) requestData.get("session").get(0);
		String policyId = (String) session.getAttribute("Id");
		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}

		String locQuoteSection = "7";

		// setting Appian URL
		String saveURL = CreateElementService.getEnvironment().getProperty("url.saveQuoteDetails") + "?policy_id="
				+ policyId;
		String getURL = CreateElementService.getEnvironment().getProperty("url.getQuestion") + "?policy_id=" + policyId;

		String getLocURL = getURL + "&quote_section=" + locQuoteSection;
		String saveLocURL = saveURL + "&quote_section=" + locQuoteSection;

		if (requestData.containsKey("saveAndExit")) {
			session.invalidate();
			saveLocURL += "&action=SE";
		} else {
			saveLocURL += "&action=SC";
		}

		// converting URL to URI
		URI getLocURI = DSCIService.toURI(getLocURL);
		URI saveLocURI = DSCIService.toURI(saveLocURL);

		// null check and getting data for saving
		Object data = requestData.containsKey("data") ? requestData.get("data").get(0) : "";
		HttpEntity getEntity = getSaveHttpEntity(new HashMap());

		ObjectMapper mapper = new ObjectMapper();

		HashMap<String, List<HashMap<String, List>>> LocationsData = null;
		HashMap<String, Object> buildingData = null;
		try {
			buildingData = mapper.readValue(toJson(data), HashMap.class);
		} catch (IOException e1) {
			throw new DSCIQuoteException(e1);
		}
		String locationId = null;

		locationId = (String) buildingData.get("locationId");

		if (locationId == null) {
			throw new DSCIQuoteException("id not found");
		}

		// Master and Transaction data from Appian
		String[] separatedMasterAndTransactionalResponse = servicecall(restTemplate, getLocURI, getEntity);

		HashMap<String, Object> LocData = new HashMap<>();
		List<HashMap<String, Object>> locDataList = null;
		if (separatedMasterAndTransactionalResponse.length > 1) {
			try {
				LocData = mapper.readValue(separatedMasterAndTransactionalResponse[1], HashMap.class);
			} catch (IOException e) {
				throw new DSCIQuoteException(e);
			}
			for (int i = 0; i < ((List) LocData.get("LocationsData")).size(); i++) {
				if (((List) ((HashMap) ((List) LocData.get("LocationsData")).get(i)).get("locationId")).get(0)
						.toString().equalsIgnoreCase(locationId)) {
					try {
						locDataList = mapper.readValue(toJson(LocData.get("LocationsData")), List.class);
						locDataList.remove(i);
						System.out.println("oldLocAddress" + locDataList);
					} catch (IOException e) {
						throw new DSCIQuoteException();
					}
					LocData.put("LocationsData", locDataList);
					System.out.println("Loop : LocData----" + LocData);
				}
			}
		}

		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData")
				: new SessionData();

		String dataToSave = toJson(LocData) + "##SessionDetails##" + toJson(sessionData);
		System.out.println("session saved " + toJson(LocData));
		HttpEntity entity = getSaveHttpEntity(dataToSave);
		debug("deleteLocationInAppian " + entity);

		// rest call
		ResponseEntity<String> saveResponse = restTemplate.exchange(saveLocURI, HttpMethod.POST, entity, String.class);

		// To save the data in db and dc
		HashMap<String, Object> req = new HashMap<>();
		req.put("elementType", "LOCATION");
		req.put("operationType", "DELETE_LOCATION");
		req.put("policyId", policyId);
		req.put("locationId", locationId);
		req.put("deleteFromDb", "true");
		req.put("locationIndex", buildingData.get("locIndex") != null ? buildingData.get("locIndex").toString() : "");
		ResponseEntity<String> dcResponse = restTemplate.postForEntity(
				CreateElementService.getEnvironment().getProperty("url.location"),
				updateRequestMapDataWithJWTToken(req), String.class);

		if (dcResponse.getStatusCodeValue() == 200) {
			debug("Response:in deleteLocationInDC method:" + dcResponse);
			return (R) new ResponseEntity<>(toJson(LocData), HttpStatus.OK);
		} else {
			throw new DSCIQuoteException("Location is not deleted properly");
		}

	}

	/**
	 * To enable the edit of location in duckcreek
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return responseString
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R viewEditLocation(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in enableEditLocation  method:" + requestData);

		HttpSession session = (HttpSession) requestData.get("session").get(0);

		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData")
				: new SessionData();

		String dataType = requestData.containsKey("dataType") ? requestData.get("dataType").get(0).toString() : "";
		String policyId = (String) session.getAttribute("Id");
		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}

		ResponseEntity<String> responseString = null;

		// if(session.getAttribute("LocAtionData")!=null){

		HashMap<String, Object> req = new HashMap<>();
		req.put("elementType", "LOCATION");
		req.put("operationType", "ENABLE_EDIT_LOCATION");
		req.put("policyId", policyId);
		req.put("locationId", requestData.get("locationId").get(0).toString());
		req.put("locationIndex", requestData.get("locationIndex").get(0).toString());
		ResponseEntity<String> response1 = restTemplate.postForEntity(
				CreateElementService.getEnvironment().getProperty("url.location"),
				updateRequestMapDataWithJWTToken(req), String.class);

		ObjectMapper mapper = new ObjectMapper();

		HashMap<String, Object> resMap = null;
		try {
			resMap = mapper.readValue(response1.getBody(), HashMap.class);
		} catch (IOException e) {
			throw new DSCIQuoteException();
		}
		resMap.put("SYQ", Arrays.asList(sessionData.getState()));
		//resMap.put("SYQ",Arrays.asList("NJ"));
		responseString = new ResponseEntity<String>(toJson(resMap), HttpStatus.OK);
		sessionData.setSubjectType("edit");
		// }

		debug("Response:in viewEditLocation method:" + responseString);
		return (R) responseString;
	}

	/**
	 * Method to cancel editing of location (It takes the old data from appian and
	 * again save the same location data in duckcreek)
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return
	 * @throws DSCIQuoteException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R cancelEditLocation(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws DSCIQuoteException {
		debug("Request:in cancelEditLocation method:" + requestData);

		HttpSession session = (HttpSession) requestData.get("session").get(0);

		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData")
				: new SessionData();

		String policyId = (String) session.getAttribute("Id");
		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}

		String locQuoteSection = "7";

		// setting Appian URL
		String getURL = CreateElementService.getEnvironment().getProperty("url.getQuestion") + "?policy_id=" + policyId;
		// String getURL =
		// "https://wiprocasemgmt.appiancloud.com/suite/webapi/getquotesectiondetails?policy_id="
		// + policyId;
		String getLocURL = getURL + "&quote_section=" + locQuoteSection;

		// converting URL to URI
		URI getLocURI = DSCIService.toURI(getLocURL);

		// null check and getting data for saving
		Object data = requestData.containsKey("data") ? requestData.get("data").get(0) : "";

		HttpEntity getEntity = getSaveHttpEntity(new HashMap());

		ObjectMapper mapper = new ObjectMapper();

		HashMap<String, Object> LocationsData = new HashMap<>();
		HashMap<String, Object> LocationAddress = null;
		try {
			LocationAddress = mapper.readValue(toJson(data), HashMap.class);

		} catch (IOException e1) {
			throw new DSCIQuoteException(e1);
		}

		// String locationId = ((List)
		// LocationAddress.get("locationId")).get(0).toString();
		String locationId = LocationAddress.get("locationId").toString();

		// Master and Transaction data from Appian
		String[] separatedMasterAndTransactionalResponse = servicecall(restTemplate, getLocURI, getEntity);

		HashMap<String, Object> LocData = new HashMap<>();
		HashMap<String, Object> oldLocAddress = new HashMap<>();
		if (separatedMasterAndTransactionalResponse.length > 1) {

			try {
				System.out.println("-----------------------------" + separatedMasterAndTransactionalResponse[1]);
				LocData = mapper.readValue(separatedMasterAndTransactionalResponse[1], HashMap.class);
				System.out.println("LocData" + LocData);
			} catch (IOException e) {
				throw new DSCIQuoteException(e);
			}

			for (int i = 0; i < ((List) LocData.get("LocationsData")).size(); i++) {
				if (((List) ((HashMap) ((List) LocData.get("LocationsData")).get(i)).get("locationId")).get(0)
						.toString().equalsIgnoreCase(locationId)) {
					try {
						// oldLocAddress = mapper.readValue(toJson(((HashMap) (
						// (List)
						// LocData.get("LocationsData")).get(i)).get("LocationAddress")),
						// List.class);
						oldLocAddress = mapper.readValue(toJson(((List) LocData.get("LocationsData")).get(i)),
								HashMap.class);
						oldLocAddress.remove("BuildingData");
						System.out.println("oldLocAddress" + oldLocAddress);
					} catch (IOException e) {
						throw new DSCIQuoteException();
					}
					System.out.println("Loop : LocData----" + LocData);
				}

			}
		}

		LocationsData.put("LocationsData", oldLocAddress);

		/*
		 * HashMap<String, Object> id = new HashMap<>(); id.put("locationId",
		 * locationId); id.put("BuildingIds", Arrays.asList()); HashMap<String, Object>
		 * idList = new HashMap<>(); idList.put("idList", Arrays.asList(id));
		 * session.setAttribute("ListOfIds", idList);
		 */

		HashMap<String, Object> req = new HashMap<>();
		req.put("elementType", "LOCATION");
		req.put("operationType", "SAVE_LOCATION");
		req.put("subjectType", "edit");
		req.put("policyId", policyId);
		req.put("locationIndex", LocationAddress.get("locIndex") != null ? LocationAddress.get("locIndex") : "");
		req.put("data", Arrays.asList(LocationsData));
		ResponseEntity<String> saveResponse = restTemplate.postForEntity(
				CreateElementService.getEnvironment().getProperty("url.location"),
				updateRequestMapDataWithJWTToken(req), String.class);

		if (saveResponse.getStatusCodeValue() == 200) {
			debug("Response:in saving location with old data method:" + saveResponse);
			sessionData.setSubjectType("");
			return (R) new ResponseEntity<>(toJson(LocData), HttpStatus.OK);
		} else {
			throw new DSCIQuoteException("Location is not saved properly");
		}
	}

	/**
	 * To get the building page master data (It creates a buildingId in duckcreek)
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return responseString
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R viewBuilding(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in viewBuilding  method:" + requestData);

		HttpSession session = (HttpSession) requestData.get("session").get(0);

		String policyId = (String) session.getAttribute("Id");
		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}
		
		String buiQuoteSection = "8";
		
		// setting URL
		String saveURL = CreateElementService.getEnvironment().getProperty("url.saveQuoteDetails") + "?policy_id="
				+ policyId + "&quote_section=" + buiQuoteSection;
		String getURL = CreateElementService.getEnvironment().getProperty("url.getQuestion") + "?policy_id=" 
				+ policyId + "&quote_section=" + buiQuoteSection;

		String dataType = requestData.containsKey("dataType") ? requestData.get("dataType").get(0).toString() : "";

		HashMap<String, String> requestData1 = new HashMap<>();
		requestData1.put("elementType", "BUILDING");
		requestData1.put("operationType", "VIEW_BUILDING");
		requestData1.put("firstBuilding", requestData.get("firstBuilding").get(0).toString());
		requestData1.put("policyId", policyId);
		requestData1.put("locationId", requestData.get("locationId").get(0).toString());
		System.out.println("request data " + requestData1);

		ResponseEntity<String> response = restTemplate.postForEntity(
				CreateElementService.getEnvironment().getProperty("url.location"),
				updateRequestStringMapDataWithJWTToken(requestData1), String.class);

		HttpEntity getEntity = getSaveHttpEntity(new HashMap());
		ObjectMapper mapper = new ObjectMapper();
		HashMap<String, List<HashMap<String, List>>> LocationsData = null;
		HashMap<String, Object> buildingData = null;
		HashMap<String, Object> buildingMap = null;
		try {
			buildingData = mapper.readValue(response.getBody(), HashMap.class);
			buildingMap = (HashMap) ((List) buildingData.get("BuildingData")).get(0);
		} catch (IOException e) {
			throw new DSCIQuoteException();
		}
		String locationId = (String) requestData.get("locationId").get(0).toString();

		if (locationId == null) {
			throw new DSCIQuoteException("id not found");
		}
		
		// rest call
		String[] separatedMasterAndTransactionalResponse = servicecall(restTemplate, DSCIService.toURI(getURL),
				getEntity);
		
		if (separatedMasterAndTransactionalResponse.length > 1) {
			try {
				LocationsData = mapper.readValue(separatedMasterAndTransactionalResponse[1], HashMap.class);
			} catch (IOException e) {
				throw new DSCIQuoteException(e);
			}

			for (int i = 0; i < LocationsData.get("LocationsData").size(); i++) {

				if (LocationsData.get("LocationsData").get(i).get("locationId").get(0).equals(locationId)) {
					System.out.println("empty build data ---"
							+ LocationsData.get("LocationsData").get(i).get("BuildingData").get(0).toString());
					HashMap<String, Object> buildingData1 = (HashMap<String, Object>) LocationsData.get("LocationsData")
							.get(i).get("BuildingData").get(0);
					if (buildingData1.isEmpty()) {
						LocationsData.get("LocationsData").get(i).get("BuildingData").remove(0);
						System.out.println("LocationsData1:B1---" + LocationsData);
						LocationsData.get("LocationsData").get(i).get("BuildingData").add(buildingMap);
					} else {
						LocationsData.get("LocationsData").get(i).get("BuildingData").add(buildingMap);
					}
					System.out.println("LocationsData2:B1---" + LocationsData);
					break;
				}
			}

		} else {
			throw new DSCIQuoteException("Location doesnot exist");
		}
		
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData")
				: new SessionData();

		String dataToSave = toJson(LocationsData) + "##SessionDetails##" + toJson(sessionData);
		HttpEntity entity = getSaveHttpEntity(dataToSave);
		ResponseEntity<String> saveResponseInAppian = restTemplate.exchange(DSCIService.toURI(saveURL),
				HttpMethod.POST, entity, String.class);

		debug("Response:in viewBuilding method:" + toJson(buildingData));
		return (R) new ResponseEntity<String>(toJson(buildingData), HttpStatus.OK);
	}

	/**
	 * To save the building data in db and duckcreek
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return
	 * @throws DSCIQuoteException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R saveBuilding(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws DSCIQuoteException {
		debug("Request:in saveBuilding method:" + requestData);

		HttpSession session = (HttpSession) requestData.get("session").get(0);

		String policyId = (String) session.getAttribute("Id");
		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}

		String buiQuoteSection = "8";

		// setting URL
		String saveURL = CreateElementService.getEnvironment().getProperty("url.saveQuoteDetails") + "?policy_id="
				+ policyId;
		String getURL = CreateElementService.getEnvironment().getProperty("url.getQuestion") + "?policy_id=" + policyId;

		String getBuiURL = getURL + "&quote_section=" + buiQuoteSection;
		String saveBuiURL = saveURL + "&quote_section=" + buiQuoteSection;

		// null check and getting data for saving
		Object data = requestData.containsKey("data") ? requestData.get("data").get(0) : "";

		HttpEntity getEntity = getSaveHttpEntity(new HashMap());

		ObjectMapper mapper = new ObjectMapper();

		HashMap<String, List<HashMap<String, List>>> LocationsData = null;
		HashMap<String, Object> buildingData = null;

		try {
			buildingData = mapper.readValue(toJson(data), HashMap.class);
		} catch (IOException e1) {
			throw new DSCIQuoteException(e1);
		}
		String locationId = (String) buildingData.get("locationId");
		String buildingId = (String) buildingData.get("buildingId");

		/*
		 * buildingData.put("buildingId", Arrays.asList(generateId()));
		 * buildingData.put("buildingId",
		 * Arrays.asList(buildingData.get("buildingId").toString()));
		 * buildingData.put("occupantId",
		 * Arrays.asList(buildingData.get("occupantId").toString()));
		 * buildingData.put("riskId",
		 * Arrays.asList(buildingData.get("riskId").toString()));
		 */

		if (locationId == null) {
			throw new DSCIQuoteException("id not found");
		}

		// rest call
		String[] separatedMasterAndTransactionalResponse = servicecall(restTemplate, DSCIService.toURI(getBuiURL),
				getEntity);

		if (separatedMasterAndTransactionalResponse.length > 1) {
			try {
				LocationsData = mapper.readValue(separatedMasterAndTransactionalResponse[1], HashMap.class);
			} catch (IOException e) {
				throw new DSCIQuoteException(e);
			}

			for (int i = 0; i < LocationsData.get("LocationsData").size(); i++) {

				if (LocationsData.get("LocationsData").get(i).get("locationId").get(0).equals(locationId)) {
					System.out.println("empty build data ---"
							+ LocationsData.get("LocationsData").get(i).get("BuildingData").get(0).toString());
					
					for (int j = 0; j < LocationsData.get("LocationsData").get(i).get("BuildingData").size(); j++) {
						if(((HashMap) LocationsData.get("LocationsData").get(i).get("BuildingData").get(j)).get("buildingId")
								.toString().equals(buildingId)){
							LocationsData.get("LocationsData").get(i).get("BuildingData").set(j, buildingData);
							break;
						}	
					}
					System.out.println("LocationsData2:B1---" + LocationsData);
					break;
				}
			}

		} else {
			throw new DSCIQuoteException("Location doesnot exist");
		}
		
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData")
				: new SessionData();

		String dataToSave = toJson(LocationsData) + "##SessionDetails##" + toJson(sessionData);
		HttpEntity entity = getSaveHttpEntity(dataToSave);

		if (requestData.containsKey("saveAndExit")) {
			session.invalidate();

			saveBuiURL += "&action=SE";
			restTemplate.exchange(DSCIService.toURI(saveBuiURL), HttpMethod.POST, entity, String.class);
			return (R) new ResponseEntity<String>("{\"status\":\"Success\"}", HttpStatus.OK);
		} else {
			saveBuiURL += "&action=SC";
		}

		HashMap<String, Object> req = new HashMap<>();
		req.put("elementType", "BUILDING");
		req.put("operationType", "SAVE_BUILDING");
		req.put("policyId", policyId);
		req.put("BuildingData", Arrays.asList(buildingData));
		ResponseEntity<String> saveResponseInDb = restTemplate.postForEntity(
				CreateElementService.getEnvironment().getProperty("url.location"),
				updateRequestMapDataWithJWTToken(req), String.class);

		if (saveResponseInDb.getStatusCodeValue() == 200) {
			// debug("Response:in saveBuilding method:" + saveResponse);
			restTemplate.exchange(DSCIService.toURI(saveBuiURL), HttpMethod.POST, entity, String.class);
			return (R) new ResponseEntity<>(toJson(buildingData), HttpStatus.OK);
		} else {
			throw new DSCIQuoteException("Building is not saved properly");
		}

	}

	/**
	 * To save the building data in db and duckcreek
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return
	 * @throws DSCIQuoteException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R updateBuilding(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws DSCIQuoteException {

		debug("Request:in saveBuilding method:" + requestData);

		HttpSession session = (HttpSession) requestData.get("session").get(0);

		String policyId = (String) session.getAttribute("Id");
		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}

		Object data = requestData.containsKey("data") ? requestData.get("data").get(0) : "";
		HashMap<String, Object> buildingData = null;
		ObjectMapper mapper = new ObjectMapper();

		try {
			buildingData = mapper.readValue(toJson(data), HashMap.class);
		} catch (IOException e1) {
			throw new DSCIQuoteException(e1);
		}

		HashMap<String, Object> req = new HashMap<>();
		req.put("elementType", "BUILDING");
		req.put("operationType", "UPDATE_BUILDING");
		req.put("policyId", policyId);
		req.put("BuildingData", Arrays.asList(buildingData));
		
		ResponseEntity<String> saveResponseInDb = restTemplate.postForEntity(
				CreateElementService.getEnvironment().getProperty("url.location"),
				updateRequestMapDataWithJWTToken(req), String.class);

		if (saveResponseInDb.getStatusCodeValue() == 200) {
			// debug("Response:in saveBuilding method:" + saveResponse);
			return (R) new ResponseEntity<>(toJson(buildingData), HttpStatus.OK);
		} else {
			throw new DSCIQuoteException("Building is not saved properly");
		}

	}

	/**
	 * To cancel a new building This method deletes the building id generated on
	 * click of add a new building link (This method will be called on click of
	 * cancel button in building section of building page)
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R cancelNewBuilding(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in cancelNewBuilding  method:" + requestData);

		HttpSession session = (HttpSession) requestData.get("session").get(0);

		String policyId = (String) session.getAttribute("Id");
		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}

		String buiQuoteSection = "8";

		// setting Appian URL
		String saveURL = CreateElementService.getEnvironment().getProperty("url.saveQuoteDetails") + "?policy_id="
				+ policyId;
		String getURL = CreateElementService.getEnvironment().getProperty("url.getQuestion") + "?policy_id=" + policyId;

		String getBuiURL = getURL + "&quote_section=" + buiQuoteSection;
		String saveBuiURL = saveURL + "&quote_section=" + buiQuoteSection;
		
		// converting URL to URI
		URI getBuiURI = DSCIService.toURI(getBuiURL);
		URI saveBuiURI = DSCIService.toURI(saveBuiURL);
		
		// null check and getting data for saving
				Object data = requestData.containsKey("data") ? requestData.get("data").get(0) : "";
				HttpEntity getEntity = getSaveHttpEntity(new HashMap());

				ObjectMapper mapper = new ObjectMapper();

				HashMap<String, List<HashMap<String, List>>> LocationsData = null;
				HashMap<String, Object> buildingData = null;
				try {
					buildingData = mapper.readValue(toJson(data), HashMap.class);
				} catch (IOException e1) {
					throw new DSCIQuoteException(e1);
				}

				String locationId = (String) buildingData.get("locationId");
				String buildingId = (String) buildingData.get("buildingId");
		
				// Master and Transaction data from Appian
				String[] separatedMasterAndTransactionalResponse = servicecall(restTemplate, getBuiURI, getEntity);

				HashMap<String, Object> LocData = new HashMap<>();
				List<HashMap<String, Object>> locDataList = null;
				if (separatedMasterAndTransactionalResponse.length > 1) {
					try {
						LocData = mapper.readValue(separatedMasterAndTransactionalResponse[1], HashMap.class);
					} catch (IOException e) {
						throw new DSCIQuoteException(e);
					}
					for (int i = 0; i < ((List) LocData.get("LocationsData")).size(); i++) {
						if (((List) ((HashMap) ((List) LocData.get("LocationsData")).get(i)).get("locationId")).get(0)
								.toString().equalsIgnoreCase(locationId)) {
							for (int j = 0; j < ((List) ((HashMap) ((List) LocData.get("LocationsData")).get(i))
									.get("BuildingData")).size(); j++) {
								if (((HashMap) ((List) ((HashMap) ((List) LocData.get("LocationsData")).get(i))
										.get("BuildingData")).get(j)).get("buildingId").toString().equals(buildingId)) {
									((List) ((HashMap) ((List) LocData.get("LocationsData")).get(i)).get("BuildingData"))
											.remove(j);
									break;
								}
							}
							break;
						}
					}
				}

				SessionData sessionData = session.getAttribute("sessionData") != null
						? (SessionData) session.getAttribute("sessionData") : new SessionData();

				String dataToSave = toJson(LocData) + "##SessionDetails##" + toJson(sessionData);
				HttpEntity saveEntity = getSaveHttpEntity(dataToSave);
				debug("deleteBuildingInAppian " + saveEntity);
				
		HashMap<String, String> dcRequestData = new HashMap<>();
		dcRequestData.put("elementType", "BUILDING");
		dcRequestData.put("operationType", "DELETE_BUILDING");
		dcRequestData.put("policyId", policyId);
		dcRequestData.put("buildingId",
				((HashMap) ((List) requestData.get("data")).get(0)).get("buildingId").toString());
		System.out.println("request data " + dcRequestData);

		ResponseEntity<String> response = restTemplate.postForEntity(
				CreateElementService.getEnvironment().getProperty("url.location"),
				updateRequestStringMapDataWithJWTToken(dcRequestData), String.class);

		if (response.getBody().isEmpty()) {
			debug("Error: response is empty in cancelNewBuilding method.");
			String message = CreateElementService.getEnvironment().getProperty("response.null");
			throw new DSCIQuoteException(message);
		} else {
			debug("Response:in cancelNewBuilding method:");
			ResponseEntity<String> saveResponse = restTemplate.exchange(saveBuiURI, HttpMethod.POST, saveEntity,
					String.class);
			return (R) new ResponseEntity<>(response.getBody(), HttpStatus.OK);
		}
	}

	/**
	 * To edit a building (to save the new building data in db and duckcreek)
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return
	 * @throws DSCIQuoteException
	 */
	/*@SuppressWarnings({ "unchecked", "rawtypes" })
	public R editBuilding(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws DSCIQuoteException {
		debug("Request:in editBuilding method:" + requestData);

		HttpSession session = (HttpSession) requestData.get("session").get(0);
		String policyId = (String) session.getAttribute("Id");
		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}

		String buiQuoteSection = "8";

		// setting Appian URL
		String getURL = CreateElementService.getEnvironment().getProperty("url.getQuestion") + "?policy_id=" + policyId;
		String saveURL = CreateElementService.getEnvironment().getProperty("url.saveQuoteDetails") + "?policy_id="
				+ policyId;
		String getBuiURL = getURL + "&quote_section=" + buiQuoteSection;
		String saveBuiURL = saveURL + "&quote_section=" + buiQuoteSection;

		// null check and getting data for saving
		Object data = requestData.containsKey("data") ? requestData.get("data").get(0) : "";
		HttpEntity getEntity = getSaveHttpEntity(new HashMap());

		ObjectMapper mapper = new ObjectMapper();

		HashMap<String, List<HashMap<String, List>>> LocationsData = null;
		HashMap<String, Object> buildingData = null;
		try {
			buildingData = mapper.readValue(toJson(data), HashMap.class);
		} catch (IOException e1) {
			throw new DSCIQuoteException(e1);
		}

		String locationId = (String) buildingData.get("locationId");
		String buildingId = (String) buildingData.get("buildingId");

		if (locationId == null) {
			throw new DSCIQuoteException("id not found");
		}

		// rest call
		String[] separatedMasterAndTransactionalResponse = servicecall(restTemplate, DSCIService.toURI(getBuiURL),
				getEntity);

		try {
			// LocationsData =
			// mapper.readValue(separatedMasterAndTransactionalResponse[1],
			// HashMap.class);
			System.out.println("-----------------------------" + separatedMasterAndTransactionalResponse[1]);
			LocationsData = mapper.readValue(separatedMasterAndTransactionalResponse[1], HashMap.class);
		} catch (IOException e) {
			throw new DSCIQuoteException(e);
		}

		for (int i = 0; i < ((List) LocationsData.get("LocationsData")).size(); i++) {
			if (((List) ((HashMap) ((List) LocationsData.get("LocationsData")).get(i)).get("locationId")).get(0)
					.toString().equalsIgnoreCase(locationId)) {
				for (int j = 0; j < ((List) ((HashMap) ((List) LocationsData.get("LocationsData")).get(i))
						.get("BuildingData")).size(); j++) {
					if (((HashMap) ((List) ((HashMap) ((List) LocationsData.get("LocationsData")).get(i))
							.get("BuildingData")).get(j)).get("buildingId").toString().equals(buildingId)) {
						((List) ((HashMap) ((List) LocationsData.get("LocationsData")).get(i)).get("BuildingData"))
								.set(j, buildingData);
						break;
					}
				}
				break;
			}

		}

		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData")
				: new SessionData();

		String dataToSave = toJson(LocationsData) + "##SessionDetails##" + toJson(sessionData);
		HttpEntity saveEntity = getSaveHttpEntity(dataToSave);

		if (requestData.containsKey("saveAndExit")) {
			session.invalidate();

			// save the building in dc
			
			 * HashMap<String, Object> req = new HashMap<>(); req.put("elementType",
			 * "BUILDING"); req.put("operationType", "FINAL_BUILDING_SAVE");
			 * req.put("policyId", policyId); ResponseEntity<String> saveResponseInDc =
			 * restTemplate.postForEntity(
			 * CreateElementService.getEnvironment().getProperty("url.location"),
			 * updateRequestMapDataWithJWTToken(req), String.class);
			 

			saveBuiURL += "&action=SE";
			restTemplate.exchange(DSCIService.toURI(saveURL), HttpMethod.POST, saveEntity, String.class);
			return (R) new ResponseEntity<String>("{\"status\":\"Success\"}", HttpStatus.OK);
		} else {
			saveBuiURL += "&action=SC";
		}

		// rest call
		ResponseEntity<String> saveResponseInAppian = restTemplate.exchange(DSCIService.toURI(saveBuiURL),
				HttpMethod.POST, saveEntity, String.class);
		debug("saveLocationAndBuildingInAppian " + saveEntity);

		// To save the data in db and dc
		HashMap<String, Object> req = new HashMap<>();
		req.put("elementType", "BUILDING");
		req.put("operationType", "EDIT_BUILDING");
		req.put("policyId", policyId);
		req.put("buildingId", buildingId);
		req.put("BuildingData", Arrays.asList(buildingData));
		ResponseEntity<String> dcResponse = restTemplate.postForEntity(
				CreateElementService.getEnvironment().getProperty("url.location"),
				updateRequestMapDataWithJWTToken(req), String.class);

		if (dcResponse.getStatusCodeValue() == 200) {
			debug("Response:in editBuilding method:" + dcResponse);
			return (R) new ResponseEntity<>(toJson(LocationsData), HttpStatus.OK);
		} else {
			throw new DSCIQuoteException("Building is not edited properly");
		}

	}*/
	
	/**
	 * To open a building in edit mode
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return
	 * @throws DSCIQuoteException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R editBuilding(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws DSCIQuoteException {
		debug("Request:in editBuilding method:" + requestData);

		HttpSession session = (HttpSession) requestData.get("session").get(0);
		String policyId = (String) session.getAttribute("Id");
		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}

		String buildingId = requestData.get("buildingId").get(0).toString();
		
		// To save the data in db and dc
		HashMap<String, Object> req = new HashMap<>();
		req.put("elementType", "BUILDING");
		req.put("operationType", "EDIT_BUILDING");
		req.put("policyId", policyId);
		req.put("buildingId", buildingId);
		ResponseEntity<String> dcResponse = restTemplate.postForEntity(
				CreateElementService.getEnvironment().getProperty("url.location"),
				updateRequestMapDataWithJWTToken(req), String.class);

		if (dcResponse.getStatusCodeValue() == 200) {
			debug("Response:in editBuilding method:" + dcResponse);
			return (R) new ResponseEntity<String>("{\"status\":\"Success\"}", HttpStatus.OK);
		} else {
			throw new DSCIQuoteException("Building is not edited properly");
		}

	}

	/**
	 * To delete a exiting building 
	 * (This method will be triggered :
	 * 1. on click of delete button on summary page 
	 * 2. on click of cancel button (page level cancel) of building page )
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return
	 * @throws DSCIQuoteException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R deleteBuilding(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws DSCIQuoteException {
		debug("Request:in deleteBuilding method:" + requestData);

		HttpSession session = (HttpSession) requestData.get("session").get(0);
		String policyId = (String) session.getAttribute("Id");
		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}
		String locQuoteSection = "8";

		// setting Appian URL
		String saveURL = CreateElementService.getEnvironment().getProperty("url.saveQuoteDetails") + "?policy_id="
				+ policyId;
		String getURL = CreateElementService.getEnvironment().getProperty("url.getQuestion") + "?policy_id=" + policyId;

		String getBuiURL = getURL + "&quote_section=" + locQuoteSection;
		String saveBuiURL = saveURL + "&quote_section=" + locQuoteSection;

		if (requestData.containsKey("saveAndExit")) {
			session.invalidate();
			saveBuiURL += "&action=SE";
		} else {
			saveBuiURL += "&action=SC";
		}

		// converting URL to URI
		URI getBuiURI = DSCIService.toURI(getBuiURL);
		URI saveBuiURI = DSCIService.toURI(saveBuiURL);

		// null check and getting data for saving
		Object data = requestData.containsKey("data") ? requestData.get("data").get(0) : "";
		HttpEntity getEntity = getSaveHttpEntity(new HashMap());

		ObjectMapper mapper = new ObjectMapper();

		HashMap<String, List<HashMap<String, List>>> LocationsData = null;
		HashMap<String, Object> buildingData = null;
		try {
			buildingData = mapper.readValue(toJson(data), HashMap.class);
		} catch (IOException e1) {
			throw new DSCIQuoteException(e1);
		}
		String locationId = null;
		String buildingId = null;

		locationId = (String) buildingData.get("locationId");
		buildingId = (String) buildingData.get("buildingId");

		if (locationId == null) {
			throw new DSCIQuoteException("id not found");
		}

		// Master and Transaction data from Appian
		String[] separatedMasterAndTransactionalResponse = servicecall(restTemplate, getBuiURI, getEntity);

		HashMap<String, Object> LocData = new HashMap<>();
		List<HashMap<String, Object>> locDataList = null;
		if (separatedMasterAndTransactionalResponse.length > 1) {
			try {
				LocData = mapper.readValue(separatedMasterAndTransactionalResponse[1], HashMap.class);
			} catch (IOException e) {
				throw new DSCIQuoteException(e);
			}
			for (int i = 0; i < ((List) LocData.get("LocationsData")).size(); i++) {
				if (((List) ((HashMap) ((List) LocData.get("LocationsData")).get(i)).get("locationId")).get(0)
						.toString().equalsIgnoreCase(locationId)) {
					for (int j = 0; j < ((List) ((HashMap) ((List) LocData.get("LocationsData")).get(i))
							.get("BuildingData")).size(); j++) {
						if (((HashMap) ((List) ((HashMap) ((List) LocData.get("LocationsData")).get(i))
								.get("BuildingData")).get(j)).get("buildingId").toString().equals(buildingId)) {
							((List) ((HashMap) ((List) LocData.get("LocationsData")).get(i)).get("BuildingData"))
									.remove(j);
							break;
						}
					}
					break;
				}
			}
		}

		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData")
				: new SessionData();

		String dataToSave = toJson(LocData) + "##SessionDetails##" + toJson(sessionData);
		HttpEntity saveEntity = getSaveHttpEntity(dataToSave);
		debug("deleteBuildingInAppian " + saveEntity);

		// rest call
		ResponseEntity<String> Saveresponse = restTemplate.exchange(saveBuiURI, HttpMethod.POST, saveEntity,
				String.class);

		HashMap<String, Object> req = new HashMap<>();
		req.put("elementType", "BUILDING");
		req.put("operationType", "DELETE_BUILDING");
		req.put("deleteFromDb", "true");
		req.put("policyId", policyId);
		req.put("buildingId", buildingId);
		ResponseEntity<String> dcResponse = restTemplate.postForEntity(
				CreateElementService.getEnvironment().getProperty("url.location"),
				updateRequestMapDataWithJWTToken(req), String.class);

		if (dcResponse.getStatusCodeValue() == 200) {
			debug("Response:in deleteBuildingInDC method:" + dcResponse);
			return (R) new ResponseEntity<>(toJson(LocData), HttpStatus.OK);
		} else {
			throw new DSCIQuoteException("Building is not deleted properly");
		}
	}

	/**
	 * To duplicate the building data in db and duckcreek
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return
	 * @throws DSCIQuoteException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R duplicateBuilding(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws DSCIQuoteException {
		debug("Request:in duplicateBuilding method:" + requestData);

		HttpSession session = (HttpSession) requestData.get("session").get(0);

		String policyId = (String) session.getAttribute("Id");
		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}

		String buiQuoteSection = "8";

		// setting URL
		String saveURL = CreateElementService.getEnvironment().getProperty("url.saveQuoteDetails") + "?policy_id="
				+ policyId;
		String getURL = CreateElementService.getEnvironment().getProperty("url.getQuestion") + "?policy_id=" + policyId;

		String getBuiURL = getURL + "&quote_section=" + buiQuoteSection;
		String saveBuiURL = saveURL + "&quote_section=" + buiQuoteSection;

		if (requestData.containsKey("saveAndExit")) {
			session.invalidate();
			saveBuiURL += "&action=SE";
		} else {
			saveBuiURL += "&action=SC";
		}

		// converting URL to URI
		URI getBuiURI = DSCIService.toURI(getBuiURL);
		URI saveBuiURI = DSCIService.toURI(saveBuiURL);

		// null check and getting data for saving
		Object data = requestData.containsKey("data") ? requestData.get("data").get(0) : "";

		HttpEntity getEntity = getSaveHttpEntity(new HashMap());

		ObjectMapper mapper = new ObjectMapper();

		HashMap<String, List<HashMap<String, List>>> LocationsData = null;
		HashMap<String, Object> buildingData = null;

		try {
			buildingData = mapper.readValue(toJson(data), HashMap.class);
		} catch (IOException e1) {
			throw new DSCIQuoteException(e1);
		}
		String locationId = null;

		locationId = (String) buildingData.get("locationId");

		if (locationId == null) {
			throw new DSCIQuoteException("id not found");
		}

		HashMap<String, Object> req = new HashMap<>();
		req.put("elementType", "BUILDING");
		req.put("operationType", "DUPLICATE_BUILDING");
		req.put("policyId", policyId);
		req.put("BuildingData", Arrays.asList(buildingData));
		ResponseEntity<String> dcResponse = restTemplate.postForEntity(
				CreateElementService.getEnvironment().getProperty("url.location"),
				updateRequestMapDataWithJWTToken(req), String.class);

		HashMap<String, Object> duplicateBuildingData = null;

		try {
			duplicateBuildingData = mapper.readValue(dcResponse.getBody(), HashMap.class);
		} catch (IOException e) {
			throw new DSCIQuoteException(e);
		}

		// rest call
		String[] separatedMasterAndTransactionalResponse = servicecall(restTemplate, getBuiURI, getEntity);

		if (separatedMasterAndTransactionalResponse.length > 1) {
			try {
				LocationsData = mapper.readValue(separatedMasterAndTransactionalResponse[1], HashMap.class);
			} catch (IOException e) {
				throw new DSCIQuoteException(e);
			}

			for (int i = 0; i < LocationsData.get("LocationsData").size(); i++) {

				if (LocationsData.get("LocationsData").get(i).get("locationId").get(0).equals(locationId)) {
					LocationsData.get("LocationsData").get(i).get("BuildingData")
							.add(((List) duplicateBuildingData.get("BuildingData")).get(0));
					System.out.println("LocationsData2:B1---" + LocationsData);
					break;
				}
			}

		} else {
			throw new DSCIQuoteException("Location doesnot exist");
		}

		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData")
				: new SessionData();

		String dataToSave = toJson(LocationsData) + "##SessionDetails##" + toJson(sessionData);
		HttpEntity entity = getSaveHttpEntity(dataToSave);
		ResponseEntity<String> saveResponseInAppian = restTemplate.exchange(saveBuiURI, HttpMethod.POST, entity,
				String.class);

		if (dcResponse.getStatusCodeValue() == 200) {
			debug("Response:in duplicateBuilding method:" + dcResponse);
			return (R) new ResponseEntity<>(toJson(LocationsData), HttpStatus.OK);
		} else {
			throw new DSCIQuoteException("Building is not duplicated properly");
		}

	}

	/**
	 * To call zipcode lookup on location page
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return
	 * @throws DSCIQuoteException
	 */
	@SuppressWarnings("rawtypes")
	private R zipcodeLookup(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws DSCIQuoteException {
		debug("Request:in zipcodeLookup method:" + requestData);
		String zipCode = requestData.containsKey("data") ? (String) requestData.get("data").get(0) : "";

		HttpSession session = (HttpSession) requestData.get("session").get(0);
		String policyId = (String) session.getAttribute("Id");
		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}

		HashMap<String, Object> request = new HashMap<>();
		request.put("zipCode", zipCode);
		request.put("locationId",
				requestData.containsKey("locationId") ? (String) requestData.get("locationId").get(0) : "");
		request.put("policyId", policyId);
		request.put("elementType", "LOCATION");
		request.put("operationType", "ZIPCODE_LOOKUP");
		HttpEntity entity = getSaveHttpEntity(updateRequestMapDataWithJWTToken(request));

		ResponseEntity<String> response = restTemplate.exchange(
				CreateElementService.getEnvironment().getProperty("url.location"), HttpMethod.POST, entity,
				String.class);

		if (response.getBody().isEmpty()) {
			debug("Error: response is empty in territoryLookup method.");
			String message = CreateElementService.getEnvironment().getProperty("response.null");
			throw new DSCIQuoteException(message);
		} else {
			debug("Response:in zipCodeLooUp method:" + response);
			return (R) new ResponseEntity<>(response.getBody(), HttpStatus.OK);
		}
	}

	/**
	 * To validate the address on location page It is a combination of location
	 * service and terror target. Also, have different Urls in dc for new location
	 * and existing location
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return
	 * @throws DSCIQuoteException
	 */
	@SuppressWarnings("rawtypes")
	public R validateAddress(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws DSCIQuoteException {
		System.out.println("Request:in validateAddress  method:" + requestData);

		HttpSession session = (HttpSession) requestData.get("session").get(0);

		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData")
				: new SessionData();

		String policyId = (String) session.getAttribute("Id");
		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}

		HashMap<String, Object> req = new HashMap<>();
		req.put("elementType", "LOCATION");
		req.put("operationType", "VALIDATE_LOCATION");
		req.put("subjectType", sessionData.getSubjectType() != null ? sessionData.getSubjectType().toString() : "");
		req.put("policyId", policyId);
		req.put("data", requestData.get("data").get(0));
		System.out.println("data-----------" + requestData.get("data").get(0));
		ResponseEntity<String> response = restTemplate.postForEntity(
				CreateElementService.getEnvironment().getProperty("url.location"),
				updateRequestMapDataWithJWTToken(req), String.class);

		debug("Response:in validateAddress method:" + response.getBody());

		return (R) new ResponseEntity<>(response.getBody(), HttpStatus.OK);
	}

	/**
	 * To get protection class dropdown values based on territory
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return
	 * @throws DSCIQuoteException
	 */
	@SuppressWarnings("rawtypes")
	private R territoryLookup(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws DSCIQuoteException {
		debug("Request:in territoryLookup method:" + requestData);
		String territory = requestData.containsKey("data") ? (String) requestData.get("data").get(0) : "";

		HttpSession session = (HttpSession) requestData.get("session").get(0);
		String policyId = (String) session.getAttribute("Id");
		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}

		HashMap<String, Object> request = new HashMap<>();
		request.put("territory", territory);
		request.put("locationId",
				requestData.containsKey("locationId") ? (String) requestData.get("locationId").get(0) : "");
		request.put("policyId", policyId);
		request.put("elementType", "LOCATION");
		request.put("operationType", "TERRITORY_LOOKUP");
		HttpEntity entity = getSaveHttpEntity(updateRequestMapDataWithJWTToken(request));

		ResponseEntity<String> response = restTemplate.exchange(
				CreateElementService.getEnvironment().getProperty("url.location"), HttpMethod.POST, entity,
				String.class);

		if (response.getBody().isEmpty()) {
			debug("Error: response is empty in territoryLookup method.");
			String message = CreateElementService.getEnvironment().getProperty("response.null");
			throw new DSCIQuoteException(message);
		} else {
			debug("Response:in territoryLookup method:" + response);
			return (R) new ResponseEntity<>(response.getBody(), HttpStatus.OK);
		}
	}

	/**
	 * Function to set location and building data in session for use in Additional
	 * Interest page
	 * 
	 * @param data
	 *            which is being rendered on refresh
	 * @return Object containing the location and building data
	 */

	/*
	 * @SuppressWarnings({ "unchecked", "rawtypes" }) public List<HashMap<String,
	 * Object>> setLocationAndBuildingInSession(String data) {
	 * 
	 * HashMap<String, List<HashMap<String, List>>> locationData; ObjectMapper
	 * mapper = new ObjectMapper();
	 * 
	 * List<HashMap<String, Object>> sessionLocationAndBuilding = new ArrayList<>();
	 * try { locationData = (HashMap<String, List<HashMap<String, List>>>)
	 * mapper.readValue(data, HashMap.class);
	 * 
	 * 
	 * for (int index = 0; index < locationData.get("LocationsData").size();
	 * index++) { HashMap<String, Object> locationTempData = new HashMap<String,
	 * Object>();
	 * 
	 * locationTempData.put("locationName",
	 * locationData.get("LocationsData").get(index).get("locationName").get(0));
	 * locationTempData.put("locationId",
	 * locationData.get("LocationsData").get(index).get("locationId").get(0));
	 * 
	 * if (locationData.get("LocationsData").get(index).get("BuildingData").size() >
	 * 0) { HashMap<String, Object> BuildingTempData = new HashMap<>();
	 * List<HashMap<String, Object>> mapped_buildings = new ArrayList<>();
	 * mapped_buildings = mapper.readValue(
	 * toJson(locationData.get("LocationsData").get(index).get("BuildingData")),
	 * List.class); for (int index_b = 0; index_b < mapped_buildings.size();
	 * index_b++) { BuildingTempData.put("Building " + Integer.toString(index_b +
	 * 1), mapped_buildings.get(index_b).get("buildingId"));
	 * 
	 * } locationTempData.put("Building", BuildingTempData); }
	 * 
	 * sessionLocationAndBuilding.add(locationTempData); }
	 * 
	 * } catch (IOException e) {
	 * debug("Failed to parse location and building data in risk service"); throw
	 * new DSCIQuoteException(); }
	 * 
	 * return sessionLocationAndBuilding; }
	 */

	@SuppressWarnings("rawtypes")
	public String[] servicecall(RestTemplate restTemplate, URI uri, HttpEntity getEntity) {
		ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.POST, getEntity, String.class);
		String[] separatedResponse = null;
		if (response.getBody().isEmpty()) {
			debug("Error: response is empty in viewLocation method.");
			String message = CreateElementService.getEnvironment().getProperty("response.null");
			throw new DSCIQuoteException(message);
		} else {
			debug("Data from appian" + response);
			separatedResponse = response.getBody().split("##SessionData##");
		}

		return separatedResponse[0].split("--MidPayload--");
	}

	/**
	 * To save the summary page data in appian
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return
	 * @throws DSCIQuoteException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R saveSummaryPageinAppian(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws DSCIQuoteException {
		debug("Request:in saveSummaryPageinAppian method:" + requestData);

		HttpSession session = (HttpSession) requestData.get("session").get(0);

		String policyId = (String) session.getAttribute("Id");
		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}

		String summaryQuoteSection = "6";

		// setting URL
		String saveURL = CreateElementService.getEnvironment().getProperty("url.saveQuoteDetails") + "?policy_id="
				+ policyId;

		String saveSummaryURL = saveURL + "&quote_section=" + summaryQuoteSection;

		if (requestData.containsKey("saveAndExit")) {
			session.invalidate();
			saveSummaryURL += "&action=SE";
		} else {
			saveSummaryURL += "&action=SC";
		}

		// converting URL to URI
		URI saveSummaryURI = DSCIService.toURI(saveSummaryURL);

		// null check and getting data for saving
		Object data = requestData.containsKey("data") ? requestData.get("data").get(0) : "";

		ObjectMapper mapper = new ObjectMapper();

		HashMap<String, Object> SummaryData = null;

		try {
			SummaryData = mapper.readValue(toJson(data), HashMap.class);
		} catch (IOException e) {
			throw new DSCIQuoteException(e);
		}

		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData")
				: new SessionData();

		String dataToSave = toJson(SummaryData) + "##SessionDetails##" + toJson(sessionData);
		HttpEntity entity = getSaveHttpEntity(dataToSave);
		restTemplate.exchange(saveSummaryURI, HttpMethod.POST, entity,
				String.class);
        updateApplicantPolicyInformation(requestData);	
		return (R) new ResponseEntity<String>("{\"status\":\"Success\"}",HttpStatus.OK);
	}
	
	private void updateApplicantPolicyInformation(LinkedMultiValueMap<String, Object> requestData) {
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		String url = CreateElementService.getEnvironment().getProperty("url.applicantPolicyInformation.update");
		HashMap<String, Object> data = new HashMap<String, Object>();
		data.put("policyId", (String) session.getAttribute("Id"));
		HashMap<String, Object> request = DSCIUtility.updateRequestMapDataWithJWTToken(data);
	    restTemplate.exchange(url, HttpMethod.PUT, getSaveHttpEntity(request), String.class);
	}

}
