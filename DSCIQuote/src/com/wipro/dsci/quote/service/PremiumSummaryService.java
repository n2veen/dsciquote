package com.wipro.dsci.quote.service;

import static com.wipro.dsci.quote.service.DSCIService.toJson;
import static com.wipro.dsci.quote.service.DSCIService.toURI;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.BiFunction;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wipro.dsci.quote.enums.Actions;
import com.wipro.dsci.quote.exception.DSCIQuoteException;
import com.wipro.dsci.quote.model.SessionData;

import static com.wipro.dsci.quote.service.DSCIService.getViewHttpEntity;
import static com.wipro.dsci.quote.service.DSCIService.getSaveHttpEntity;

@org.springframework.stereotype.Service("PremiumSummaryService")
@SuppressWarnings({ "unchecked", "rawtypes" })
public class PremiumSummaryService<R, E> implements DSCIService<R, DSCIQuoteException> {

	private static void debug(String format, Object... args) {
		Logger logger = LoggerFactory.getLogger("DSCIQuote_dblogger");
		if (logger.isDebugEnabled()) {
			logger.debug(format, args);
		}
	}

	@Autowired
	RestTemplate restTemplate;

	private Map<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>> opTypeMap = new HashMap<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>>() {

		private static final long serialVersionUID = 1L;
		{
			put(Actions.VIEW_PREMIUM_SUMMARY, (a, b) -> {
				return getPremiumSummary(a, b);
			});
			put(Actions.SAVE_AND_EXIT_PREMIUM_SUMMARY, (a, b) -> {
				return saveAndExitPremiumSummary(a, b);
			});
			put(Actions.CREATE_PROPOSAL, (a, b) -> {
				return createProposal(a, b);
			});

		}
	};

	HandOffService handOffService = new HandOffService();
	
	@Override
	public R delegate(Actions actions, LinkedMultiValueMap<String, Object> requestData) throws RuntimeException {

		// null check and getting operationType
		String operationType = requestData.containsKey("operationType")
				? (String) requestData.get("operationType").get(0) : "";
		if (!opTypeMap.containsKey(Actions.valueOf(operationType.toUpperCase()))) {
			throw new IllegalArgumentException(String.format("%s is not supported", actions));
		}
		return opTypeMap.get(actions).apply(restTemplate, requestData);
	}

	/**
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return
	 * @throws RuntimeException
	 */

	// get Premium Summary Data from backend

	public R getPremiumSummary(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws RuntimeException {

		debug("Request: in getPremiumSummary method with requestData: " + requestData);

		
		
		HashMap<String, Object> final_response = new HashMap<String, Object>();
		// String url =
		// CreateElementService.getEnvironment().getProperty("url.getQuestion");
		String url = CreateElementService.getEnvironment().getProperty("url.getQuestion");
		String QuoteSection = "12";
		String QuoteSectionOfPricaingAndCoverage = "11";
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();

		String premiumValue = sessionData.getPremiumValue();
		String effectiveStartDate = sessionData.getEffectiveStartDate();
		String effectiveEndDate = sessionData.getEffectiveEndDate();
		
		
		
		String policyId = (String) session.getAttribute("Id");
		if (policyId == null) {
			String message = "Policy ID not found in Premium Summary Service";
			throw new DSCIQuoteException(message);
		}
		
		String appianData = getDataFromAppian(restTemplate,policyId);
		if(!appianData.equals("noData")){
			return (R) new ResponseEntity<String>(appianData, HttpStatus.OK); 
		}
		
		LinkedMultiValueMap<String, Object> data_get = new LinkedMultiValueMap<>();
		LinkedMultiValueMap<String, Object> data_get_pricing_coverage = new LinkedMultiValueMap<>();

		data_get.add("operationType", "VIEWPREMIUMSUMMARYPAGE");
		// data_get_pricing_coverage.add("isPremiumSummary","true");
		// data_get_pricing_coverage.add("operationType","getcoverages");

		url += "?quote_section=" + QuoteSection + "&policy_id=" + policyId;
		String url_pricing_and_coverage = CreateElementService.getEnvironment().getProperty("url.getQuestion")
				+ "?quote_section=" + QuoteSectionOfPricaingAndCoverage + "&policy_id=" + policyId;

		URI uri = DSCIService.toURI(url);
		URI uri_pricing_coverage = DSCIService.toURI(url_pricing_and_coverage);

		// call to get master data for producer dropdown
		HttpEntity entity = getSaveHttpEntity(data_get);
		// ResponseEntity<ArrayList> response = restTemplate.exchange(uri,
		// HttpMethod.POST, entity, ArrayList.class);

		// rest call to get pricing and coverage transactionsl data
		HttpEntity entity_pricing_coverage = getSaveHttpEntity(data_get_pricing_coverage);
		ResponseEntity<String> response_pricing_coverage = restTemplate.exchange(uri_pricing_coverage, HttpMethod.POST,
				entity_pricing_coverage, String.class);
	

		String[] appianResponse = response_pricing_coverage.getBody().split("--MidPayload--");
		String[] sessionSeperate = appianResponse[1].split("##SessionData##");

		List<HashMap<String, String>> mappedData = new ArrayList<>();

		ObjectMapper mapper = new ObjectMapper();

		try {
			mappedData = mapper.readValue(sessionSeperate[0], List.class);
		} catch (IOException e) {
			debug("error while parsing");
		}

		String quoteAmount = mappedData.get(0).get("price");
		
		session.setAttribute("QuoteAmount", quoteAmount);
		session.setAttribute("startDate", effectiveStartDate);
		session.setAttribute("endDate", effectiveEndDate);
		
		sessionData.setQuoteAmount(quoteAmount);
		// final_response.put("producerdata", response.getBody());
		final_response.put("premiumsummarydata", "");
		final_response.put("QuoteAmount", "$" + quoteAmount);
		final_response.put("startDate", effectiveStartDate);
		final_response.put("endDate", effectiveEndDate);

		return (R) new ResponseEntity<Object>("[" + toJson(final_response) + "," + sessionSeperate[0] + "]",
				HttpStatus.OK);
	}

	public R createProposal(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws RuntimeException {

		debug("Request: in getPremiumSummary method with requestData: " + requestData);
		String url = CreateElementService.getEnvironment().getProperty("url.premium.summary");
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();

		String bearerToken = session.getAttribute("JsonWebToken").toString();
		
		String policy_id = (String) session.getAttribute("Id");
		LinkedMultiValueMap<String, Object> data_get = new LinkedMultiValueMap<>();
		data_get.add("termsandConditions", requestData.get("termsandConditions").get(0));
		data_get.add("ProposalDescription", requestData.get("ProposalDescription").get(0));
		data_get.add("policy_id", policy_id);
		data_get.add("operationType", "CREATE_PROPOSAL");
		data_get.add("bearerToken", bearerToken);

		URI uri = DSCIService.toURI(url);
		HttpEntity entity = getSaveHttpEntity(data_get);
		ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);

		return (R) response;
	}

	public R saveAndExitPremiumSummary(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws RuntimeException {
		debug("Request:in saveAndExitPremiumSummary method with requestData: " + requestData);
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		String QuoteSection = "12";
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();
				
		String policyId = (String) session.getAttribute("Id");		
		String dataToSave = toJson(requestData.get("data").get(0))  +"##SessionDetails##" +toJson(sessionData);	
		String saveURL = CreateElementService.getEnvironment().getProperty("url.saveQuoteDetails");
		saveURL += "?policy_id=" + policyId + "&quote_section=" + QuoteSection + "&action=SE";
		URI uri = toURI(saveURL);
		
		
		try {
			restTemplate.exchange(uri, HttpMethod.POST, getSaveHttpEntity(dataToSave), String.class);
		} catch (RestClientException e) {
			
			throw new DSCIQuoteException("Error while saving premium summary in appian");
		}
		if (requestData.containsKey("saveAndExit")) {
			session.invalidate();
		}

		return (R) new ResponseEntity<Object>(HttpStatus.OK);

	}
	
	public String getDataFromAppian(RestTemplate restTemplate,String policyId){
	
		
		String url = CreateElementService.getEnvironment().getProperty("url.getQuestion");
		String QuoteSection = "12";
		url += "?policy_id=" + policyId + "&quote_section=" + QuoteSection + "&fetch_master=No";
		URI uri = toURI(url);
		
		ResponseEntity<String> resp = restTemplate.exchange(uri, HttpMethod.POST, getSaveHttpEntity(new HashMap<>()), String.class);
		
		String[] newIn = resp.getBody().split("##SessionData##");
                
		
		String responseFromAppian = resp.getBody();
		if(responseFromAppian.toString().equals("##SessionData##")) return "noData";
		String[] withOutSession = responseFromAppian.split("##SessionData##");
		if(withOutSession[0].length()<1) return "noData";
		else{
			
			return withOutSession[0];
		}
	}

	/**
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return
	 * @throws RuntimeException
	 */

	public RestTemplate getTemplate() {
		return restTemplate;
	}

	public UUID generateId() {
		return UUID.randomUUID();
	}

}
