package com.wipro.dsci.quote.service;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.wipro.dsci.quote.enums.Actions;
import com.wipro.dsci.quote.exception.DSCIQuoteException;

@org.springframework.stereotype.Service("GlobalUIService")
public class GlobalUIService<R, E> implements DSCIService<R, DSCIQuoteException> {

	private static void debug(String format, Object... args) {
		Logger logger = LoggerFactory.getLogger("DSCIQuote_dblogger");
		if (logger.isDebugEnabled()) {
			logger.debug(format, args);
		}
	}
	@Autowired
	RestTemplate restTemplate;

	private Map<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>> opTypeMap = new HashMap<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>>() {

		private static final long serialVersionUID = 1L;
		{
			put(Actions.LOGOUT, (a, b) -> {
				return logoutWithExitButton(a, b);
			});

		}
	};

	@Override
	public R delegate(Actions actions, LinkedMultiValueMap<String, Object> requestData) throws RuntimeException {

		// null check and getting operationType
		String operationType = requestData.containsKey("operationType")
				? (String) requestData.get("operationType").get(0) : "";
		if (!opTypeMap.containsKey(Actions.valueOf(operationType.toUpperCase()))) {
			throw new IllegalArgumentException(String.format("%s is not supported", actions));
		}
		return opTypeMap.get(actions).apply(restTemplate, requestData);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R logoutWithExitButton(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in logout method:" + requestData);
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		session.invalidate();
		System.out.println("Inside session invalidate method");
		return (R) new ResponseEntity<>(HttpStatus.NO_CONTENT);

	}

}
