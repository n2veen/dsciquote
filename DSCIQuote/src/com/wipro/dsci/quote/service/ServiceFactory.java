package com.wipro.dsci.quote.service;

import java.util.EnumMap;
import java.util.EnumSet;
import java.util.Objects;
import java.util.function.Supplier;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wipro.dsci.quote.enums.Element;


@Component
public class ServiceFactory {
	
	final static EnumMap<Element, DSCIService<?, ?>> serviceMap = new EnumMap<>(Element.class);
    
	@Autowired
	AccountDashboardService <?, ?> accountDashboardService;
	@Autowired
	BusinessClassService<?, ?> businessClassService;
	@Autowired
	RiskService<?, ?> riskService;
	@Autowired
	AgentService<?, ?> agentService;
	@Autowired
	EligibiltyService<?, ?> eligibiltyService;
	@Autowired
	LobService<?, ?> lobService;
	@Autowired
	UnderwritingQuestionsService<?, ?> underwritingQuestionsService;
	@Autowired
	AppianRulesService<?, ?> appianRulesService;
	@Autowired
	BeforeYouBeginService<?, ?> beforeYouBeginService;
	@Autowired
	PricingAndCoverageService<?, ?> pricingAndCoverageService;
	@Autowired
	AdditionalInterestsService<?, ?> additionalInterestsService;
	@Autowired
	GlobalUIService<?, ?> globalUIService;
	@Autowired
	PremiumSummaryService<?, ?> premiumSummaryService;
	@Autowired
	BeforeYouIssuePolicyService<?, ?> beforeYouIssuePolicyService;
	@Autowired
	StartYourQuoteService<?, ?> startYourQuoteService;
	@Autowired
	ReferUnderwritingService<?, ?> referUnderwritingService;
	@Autowired
	OccupantService<?, ?> occupantService;
	
	@PostConstruct
	public void postContruct() {
		serviceMap.put(Element.ACCOUNT, accountDashboardService);
		serviceMap.put(Element.BUSINESSCLASS, businessClassService);
		serviceMap.put(Element.RISK, riskService);
		serviceMap.put(Element.LOCATION, businessClassService);
		serviceMap.put(Element.ISDSCIAGENT, agentService);
		serviceMap.put(Element.ELIGIBILTY, eligibiltyService);
		serviceMap.put(Element.LOB, lobService);
		serviceMap.put(Element.UNDERWRITINGQUESTIONS, underwritingQuestionsService);
		serviceMap.put(Element.APPIANRULE, appianRulesService);
		serviceMap.put(Element.BEFOREYOUBEGIN, beforeYouBeginService);
		serviceMap.put(Element.PRICINGANDCOVERAGE, pricingAndCoverageService);
		serviceMap.put(Element.ADDITIONALINTERESTS, additionalInterestsService);
		serviceMap.put(Element.GLOBALUI, globalUIService);
		serviceMap.put(Element.PREMIUMSUMMARY, premiumSummaryService);
		serviceMap.put(Element.BEFOREYOUISSUEPOLICY, beforeYouIssuePolicyService);
		serviceMap.put(Element.STARTYOURQUOTE, startYourQuoteService);
		serviceMap.put(Element.REFERUNDERWRITER, referUnderwritingService);
		serviceMap.put(Element.OCCUPANT, occupantService);
	}
/*
	static {
		serviceMap.put(Element.ACCOUNT, AccountDashboardService::new);
		serviceMap.put(Element.BUSINESSCLASS, BusinessClassService::new);
		serviceMap.put(Element.RISK, RiskService::new);
		serviceMap.put(Element.LOCATION, BusinessClassService::new);
		serviceMap.put(Element.ISDSCIAGENT, AgentService::new);
		serviceMap.put(Element.ELIGIBILTY, EligibiltyService::new);
		serviceMap.put(Element.LOB, LobService::new);
		serviceMap.put(Element.UNDERWRITINGQUESTIONS, UnderwritingQuestionsService::new);
		serviceMap.put(Element.APPIANRULE,AppianRulesService::new);
		serviceMap.put(Element.BEFOREYOUBEGIN,BeforeYouBeginService::new);
		serviceMap.put(Element.PRICINGANDCOVERAGE,PricingAndCoverageService::new);
		serviceMap.put(Element.ADDITIONALINTERESTS,AdditionalInterestsService::new);
		serviceMap.put(Element.GLOBALUI,GlobalUIService::new);
		serviceMap.put(Element.PREMIUMSUMMARY,PremiumSummaryService::new);
		serviceMap.put(Element.BEFOREYOUISSUEPOLICY,BeforeYouIssuePolicyService::new);
		serviceMap.put(Element.STARTYOURQUOTE, StartYourQuoteService::new);
		serviceMap.put(Element.REFERUNDERWRITER, ReferUnderwritingService::new);
		serviceMap.put(Element.OCCUPANT, OccupantService::new);
	    assert serviceMap.keySet().containsAll(EnumSet.allOf(Element.class));
	  } 
*/
	
	/**
	 * @param element
	 * @return Service
	 */
	public static DSCIService<?, ?> getService(Element element) {
		return serviceMap.get(element);
/*
        return serviceMap.getOrDefault(
            Objects.requireNonNull(element), ()->{throw new AssertionError();}).get(); */
    }
}
