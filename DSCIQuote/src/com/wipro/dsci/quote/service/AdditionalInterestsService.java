package com.wipro.dsci.quote.service;

import static com.wipro.dsci.quote.service.DSCIService.getSaveHttpEntity;
import static com.wipro.dsci.quote.service.DSCIService.toJson;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.UUID;
import java.util.function.BiFunction;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wipro.dsci.quote.enums.Actions;
import com.wipro.dsci.quote.exception.DSCIQuoteException;
import com.wipro.dsci.quote.model.SessionData;
import com.wipro.dsci.quote.util.DSCIUtility;

@org.springframework.stereotype.Service("AdditionalInterestsService")
@SuppressWarnings({ "unchecked", "rawtypes" })
public class AdditionalInterestsService<R, E> implements DSCIService<R, DSCIQuoteException> {

	private static void debug(String format, Object... args) {
		Logger logger = LoggerFactory.getLogger("DSCIQuote_dblogger");
		if (logger.isDebugEnabled()) {
			logger.debug(format, args);
		}
	}

	@Autowired
	RestTemplate restTemplate;

	private Map<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>> opTypeMap = new HashMap<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>>() {

		private static final long serialVersionUID = 1L;
		{
			put(Actions.VIEW_ADDITIONAL_INTERESTS, (a, b) -> {
				return viewAdditionalInterest(a, b);
			});
			put(Actions.SAVE_ADDITIONAL_INTERESTS_IN_APPIAN, (a, b) -> {
				return saveAdditionalInterestsInAppian(a, b);
			});
			put(Actions.GET_BUILDING_BY_LOCATION, (a, b) -> {
				return getBuildingByLocation(a, b);
			});
			put(Actions.DELETE_ADDITIONAL_INTEREST, (a, b) -> {
				return deleteAdditionalInterests(a, b);
			});

		}
	};

	@Override
	public R delegate(Actions actions, LinkedMultiValueMap<String, Object> requestData) throws RuntimeException {

		// null check and getting operationType
		String operationType = requestData.containsKey("operationType")
				? (String) requestData.get("operationType").get(0) : "";
		if (!opTypeMap.containsKey(Actions.valueOf(operationType.toUpperCase()))) {
			throw new IllegalArgumentException(String.format("%s is not supported", actions));
		}
		return opTypeMap.get(actions).apply(restTemplate, requestData);
	}

	/**
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return
	 * @throws RuntimeException
	 */
	// manas

	// get Additional Interest

	public R viewAdditionalInterest(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws RuntimeException {
		debug("Request:in viewAdditionalInterests method:" + requestData);
		String url = CreateElementService.getEnvironment().getProperty("url.getQuestion");
		String QuoteSection = "9";

		LinkedMultiValueMap<String, Object> data_get = new LinkedMultiValueMap<>();

		HttpSession session = (HttpSession) requestData.get("session").get(0);
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();

		Integer productId = 1563; // Integer.valueOf(sessionData.getProductId());
		data_get.add("productId", productId);

		/*
		 * if (sessionData.getProductId() == null) { productId = 1563;
		 * data_get.add("productId", productId); // throw new
		 * DSCIQuoteException("ProductID is not in session"); } else { productId
		 * = Integer.valueOf(sessionData.getProductId());
		 * data_get.add("productId", productId); }
		 */
		populateSessionLocationData(requestData);
		List<HashMap<String, Object>> locationSessionData = sessionData.getLocationData();
		if (locationSessionData == null) {
			throw new DSCIQuoteException("No location data in session found");
		}

		List<HashMap<String, HashMap<String, List<Object>>>> sessionLocation;
		ObjectMapper mapper_2 = new ObjectMapper();
		List<Object> location_list = new ArrayList<>();
		List<Object> building_list = new ArrayList<>();
		try {
			sessionLocation = mapper_2.readValue(toJson(locationSessionData), List.class);
			for (int index = 0; index < sessionLocation.size(); index++) {

				location_list.add((sessionLocation.get(index).get("locationName")));
				int buildingNumber = sessionLocation.get(index).get("Building").size();
				for (int i = 0; i < buildingNumber; i++) {
					building_list.add("Building " + Integer.toString(i + 1));
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		String policyId = (String) session.getAttribute("Id");
		if (policyId == null) {
			String message = "No policy ID found in Additional Interest Service";
			throw new DSCIQuoteException(message);
		}

		url += "?quote_section=" + QuoteSection + "&policy_id=" + policyId;

		URI uri = DSCIService.toURI(url);

		// creating HttpEntity
		// creating HttpEntity
		Boolean masterData;
		String masterData_string = (String) requestData.get("masterData").get(0);
		if (masterData_string.equals("true")) {
			masterData = true;
		} else {
			masterData = false;
		}

		data_get.add("operationType", "VIEWADDITIONALINTERESTPAGE");
		data_get.add("masterData", masterData);

		HttpEntity entity = getSaveHttpEntity(data_get);
		debug("Connecting: View Additional Interest  : to APPIAN with HttpEntity: " + entity);
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

		// rest call
		ResponseEntity<String> response = null;
		if (masterData) {
			response = restTemplate.exchange(
					CreateElementService.getEnvironment().getProperty("url.additionalInterest"), HttpMethod.POST,
					entity, String.class);
		} else {
			response = restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
		}

		// ResponseEntity<String> response =
		// restTemplate.postForEntity("http://localhost:8080/additionalinterest/",null,String.class);
		String[] separatedResponse = null;

		debug("Exit: viewadditional interest  method of AdditionalInterest in DSCIQuote with reponse: " + response);
		if (response.getBody().isEmpty()) {
			debug("Error: response is empty in Additional Interest service from appian.");
			String message = CreateElementService.getEnvironment().getProperty("response.null");
			throw new DSCIQuoteException(message);
		} else {
			separatedResponse = response.getBody().split("##SessionData##");
		}

		String[] separatedSessionAndTransactionalResponse = separatedResponse[0].split("--MidPayload--");

		ResponseEntity<String> responseString;
		if (masterData) {
			debug("sending only master data");
			HashMap<String, HashMap<String, List<HashMap<String, Object>>>> response_1 = fillLocationAndsBuilding(
					separatedSessionAndTransactionalResponse[0], "LOCATION", location_list);
			HashMap<String, HashMap<String, List<HashMap<String, Object>>>> response_2 = fillLocationAndsBuilding(
					toJson(response_1), "BUILDING", new ArrayList());

			responseString = new ResponseEntity<String>(toJson(response_2), HttpStatus.OK);

		} else {
			debug("sending only transactional data");

			if (separatedSessionAndTransactionalResponse.length > 1) {

				return (R) new ResponseEntity<Object>(
						syncTransactionData(separatedSessionAndTransactionalResponse[1], sessionData.getLocationMap()),
						HttpStatus.OK);
			} else {

				responseString = new ResponseEntity<String>("[]", HttpStatus.OK);
			}
		}

		debug("Response:in viewAdditionalInterests method:" + responseString);
		return (R) responseString;
	}

	public HashMap<String, HashMap<String, List<HashMap<String, Object>>>> fillLocationAndsBuilding(String input,
			String field, Object value) {

		ObjectMapper mapper = new ObjectMapper();
		HashMap<String, HashMap<String, List<HashMap<String, Object>>>> rs = null;
		try {

			rs = (HashMap<String, HashMap<String, List<HashMap<String, Object>>>>) mapper.readValue(input, Map.class);
			rs.get("Widgets").entrySet().stream().forEachOrdered(s -> {
				s.getValue().stream().filter(p -> p.get("datasetName").equals(field))
						.forEach(p -> p.put("valueName", value));
			});

		} catch (IOException e) {
			debug("Error while inserting locaiton/building in JSON");
			e.printStackTrace();
		}

		return rs;
	}

	public R saveAdditionalInterestsInAppian(RestTemplate restTemplate,
			LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in saveAdditionalInterest InAppian method:" + requestData);

		// removing control data
		requestData.remove("elementType");
		requestData.remove("operationType");
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();
		// setting URL
		String saveURL = CreateElementService.getEnvironment().getProperty("url.saveQuoteDetails");
		String policyId = (String) session.getAttribute("Id");

		String QuoteSection = "9";

		Object data = requestData.containsKey("data") ? requestData.get("data") : "";

		List<List<HashMap<String, Object>>> mappedData = new ArrayList<>();
		ObjectMapper mapper = new ObjectMapper();
		try {
			mappedData = mapper.readValue(toJson(data), List.class);
			mappedData.stream().forEach(p -> {
				if (!p.stream().anyMatch(s -> s.get("datasetName").equals("UUID"))) {
					HashMap<String, Object> uuidMap = new HashMap<>();
					uuidMap.put("datasetName", "UUID");
					uuidMap.put("userValue", generateId().toString());
					p.add(uuidMap);
				}
			});
		} catch (IOException e) {
			debug("Error while parsing saving data");
			e.printStackTrace();
		}

		if (policyId == null) {
			throw new DSCIQuoteException("policyId is null in save additional interest function");
		}

		saveURL += "?policy_id=" + policyId + "&quote_section=" + QuoteSection;

		// converting URL to URI
		requestData.add("policyId", policyId);
		requestData.add("operationType", "SAVEADDITIONALINTEREST");
		requestData.put("transactionData", requestData.get("data"));
		requestData.add("locationData", sessionData.getLocationMap());
		requestData.remove("session");
		requestData = DSCIUtility.updateRequestDataWithJWTToken(requestData);
		HttpEntity entity = getSaveHttpEntity(requestData);
		debug("saveAdditionalInterestInAppian " + entity);

		if (requestData.containsKey("saveAndExit")) {
			session.invalidate();
			saveURL += "&action=SE";
			return (R) restTemplate.exchange(DSCIService.toURI(saveURL), HttpMethod.POST,
					getSaveHttpEntity(toJson(requestData.get("data")) + "##SessionDetails##" + toJson(sessionData)),
					String.class);
		} else {
			saveURL += "&action=SC";
		}

		// rest call
		ResponseEntity<LinkedMultiValueMap<String, Object>> response = restTemplate.exchange(
				CreateElementService.getEnvironment().getProperty("url.additionalInterest"), HttpMethod.POST, entity,
				new ParameterizedTypeReference<LinkedMultiValueMap<String, Object>>() {
				});
		String dataToSave = toJson(response.getBody().get("transactionData")) + "##SessionDetails##"
				+ toJson(sessionData);

		restTemplate.exchange(DSCIService.toURI(saveURL), HttpMethod.POST, getSaveHttpEntity(dataToSave), String.class);
		debug("Response:in saveAdditionalInterest method:" + response);
		return (R) response;
	}

	public R getBuildingByLocation(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		// debug("Request:in getBuildingByLocation method:" + requestData);

		// removing control data
		requestData.remove("elementType");
		requestData.remove("operationType");
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();

		Object locationName = requestData.containsKey("locationName") ? requestData.get("locationName").get(0) : "";
		Object sessionLocation = sessionData.getLocationData();
		ObjectMapper mapper = new ObjectMapper();
		ObjectMapper mapper2 = new ObjectMapper();
		List<HashMap<String, String>> listOfBuilding = new ArrayList<>();

		List<HashMap<String, String>> mappedSessionLocation = new ArrayList<>();
		try {
			mappedSessionLocation = mapper2.readValue(toJson(sessionLocation), List.class);
			mappedSessionLocation.stream().filter(p -> p.get("locationName").equals(locationName.toString()))
					.forEach(p -> {
						Object buildingMap = p.get("Building");
						HashMap<String, List> mappedBuilding;
						try {
							mappedBuilding = mapper.readValue(toJson(buildingMap), HashMap.class);
							mappedBuilding.entrySet().forEach(a -> {
								HashMap<String, String> buildingData = new HashMap<>();
								buildingData.put("buildingName", a.getKey().toString());
								buildingData.put("buildinguuid", a.getValue().get(0).toString());
								listOfBuilding.add(buildingData);
							});
						} catch (IOException e) {
							debug("Error while parsing building data");
							e.printStackTrace();
						}
					});

		} catch (IOException e) {
			e.printStackTrace();
		}

		List<HashMap<String, String>> sortedListOfBuilding = sortBuilding(listOfBuilding);

		ResponseEntity<String> response = new ResponseEntity<>(toJson(sortedListOfBuilding), HttpStatus.OK);
		debug("Response:in saveAdditionalInterest method:" + response);
		return (R) response;
	}

	public R deleteAdditionalInterests(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		requestData.add("policyId", (String) session.getAttribute("Id"));
		requestData.remove("session");
		requestData = DSCIUtility.updateRequestDataWithJWTToken(requestData);
		HttpEntity entity = getSaveHttpEntity(requestData);
		// Invoke rest service
		restTemplate.exchange(CreateElementService.getEnvironment().getProperty("url.additionalInterest"),
				HttpMethod.POST, entity, String.class);
		return (R) new ResponseEntity<>(HttpStatus.OK);
	}

	/**
	 * ab387941 function to sort building according to their name
	 */
	public List<HashMap<String, String>> sortBuilding(List<HashMap<String, String>> input) {
		List<HashMap<String, String>> sortedListOfBuilding = new ArrayList<>();
		Map<String, String> sortedMap = new TreeMap<>();
		input.forEach(t -> {
			sortedMap.put(t.get("buildingName"), t.get("buildinguuid"));
		});
		for (String key : sortedMap.keySet()) {
			HashMap<String, String> tempMap = new HashMap<>();
			String value = sortedMap.get(key);
			tempMap.put("buildingName", key);
			tempMap.put("buildinguuid", value);
			sortedListOfBuilding.add(tempMap);
		}
		return sortedListOfBuilding;
	}

	/**
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return
	 * @throws RuntimeException
	 */

	public RestTemplate getTemplate() {
		return restTemplate;
	}

	public UUID generateId() {
		return UUID.randomUUID();
	}

	private void populateSessionLocationData(LinkedMultiValueMap<String, Object> requestData) {
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();
		LinkedMultiValueMap<String, Object> data = new LinkedMultiValueMap<String, Object>();
		data.add("operationType", "FETCHLOCATIONBUILDING");
		data.add("policyId", (String) session.getAttribute("Id"));
		data = DSCIUtility.updateRequestDataWithJWTToken(data);
		ResponseEntity<List<HashMap<String, Object>>> result = restTemplate.exchange(
				CreateElementService.getEnvironment().getProperty("url.additionalInterest"), HttpMethod.POST,
				getSaveHttpEntity(data), new ParameterizedTypeReference<List<HashMap<String, Object>>>() {
				});
		List<HashMap<String, Object>> locationData = result.getBody();
		Map<String, String> locationIdMap = new HashMap<String, String>();
		for (HashMap<String, Object> locationEntry : locationData) {
			locationIdMap.put((String) locationEntry.get("locationName"), (String) locationEntry.get("locationId"));
			Map<String, Object> buildingMap = (Map<String, Object>) locationEntry.get("Building");
			for (Entry<String, Object> entry : buildingMap.entrySet()) {
				locationIdMap.put((String) locationEntry.get("locationName") + entry.getKey(),
						((List<String>) entry.getValue()).get(0));
			}
		}
		sessionData.setLocationMap(locationIdMap);
		sessionData.setLocationData(result.getBody());
		result.getBody();
	}

	private List<List<Map<String, Object>>> syncTransactionData(String transactionData,
			Map<String, String> locationMap) {
		ObjectMapper mapper = new ObjectMapper();
		List<List<Map<String, Object>>> transaction = null;
		List<List<Map<String, Object>>> modifiedTransactionData = new ArrayList<List<Map<String, Object>>>();
		try {
			transaction = mapper.readValue(transactionData, new TypeReference<List<List<Map<String, Object>>>>() {
			});
		} catch (Exception exception) {
			throw new DSCIQuoteException("Json Conversion failed");
		}

		// iterate
		Iterator additionalInterestIterator = transaction.iterator();

		while (additionalInterestIterator.hasNext()) {

			int count = 0;
			List<Map<String, Object>> modifiedData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> additionalInterest = (List<Map<String, Object>>) additionalInterestIterator
					.next();
			Iterator elementIterator = additionalInterest.iterator();

			while (elementIterator.hasNext()) {
				Map<String, Object> entry = (Map<String, Object>) elementIterator.next();
				if (entry.containsKey("systemRefId") && !(entry.get("systemRefId").equals("LOCATION")
						|| entry.get("systemRefId").equals("BUILDING"))) {
					modifiedData.add(entry);
				} else {
					if (entry.get("systemRefId").equals("LOCATION")) {
						String locationName = (String) entry.get("userValue");
						Map<String, Object> buildingEntry = (Map<String, Object>) elementIterator.next();
						String buildingName = locationName + (String) buildingEntry.get("userValue");
						if (locationMap.containsKey(locationName) && locationMap.containsKey(buildingName)) {
							modifiedData.add(entry);
							modifiedData.add(buildingEntry);
							count++;
						}
					}

				}

			}

			// additional Interest with atleast one valid location/Building
			if (count != 0) {
				modifiedTransactionData.add(modifiedData);
			}

		}

		return modifiedTransactionData;
	}

}
