package com.wipro.dsci.quote.service;

import static com.wipro.dsci.quote.service.DSCIService.debug;
import static com.wipro.dsci.quote.service.DSCIService.getSaveHttpEntity;
import static com.wipro.dsci.quote.service.DSCIService.getViewHttpEntity;
import static com.wipro.dsci.quote.service.DSCIService.prefill;
import static com.wipro.dsci.quote.service.DSCIService.withoutPrefill;
import static com.wipro.dsci.quote.service.DSCIService.toJson;
import static com.wipro.dsci.quote.service.DSCIService.toURI;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity; 
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.wipro.dsci.quote.enums.Actions;
import com.wipro.dsci.quote.exception.DSCIQuoteException;
import com.wipro.dsci.quote.model.BusinessAddress;
import com.wipro.dsci.quote.model.SessionData;
import com.wipro.dsci.quote.util.DSCIUtility;

@Component
public class BeforeYouIssuePolicyService<R, E> implements DSCIService<R, DSCIQuoteException> {

	
	@Autowired
	RestTemplate restTemplate;

	private Map<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>> opTypeMap = new HashMap<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>>() {

		private static final long serialVersionUID = 1L;
		{
			put(Actions.VIEW_BEFORE_YOU_ISSUE_POLICY, (a, b) -> {
				return viewBeforeYouIssuePolicyData(a, b);
			});

			put(Actions.VIEW_SECONDARY_NAMED_INSURED, (a, b) -> {
				return viewSecondaryNamedInsuredInfo(a, b);
			});
			
			put(Actions.SAVE_BEFORE_YOU_ISSUE_POLICY, (a, b) -> {
				return saveBeforeYouIssuePolicyData(a, b);
			});
			put(Actions.SEC_SAVE_BEFORE_YOU_ISSUE_POLICY, (a, b) -> {
				return saveSecondaryNamedInsured(a, b);
			});
			put(Actions.DELETE_SECONDARY_NAMED_INSURED, (a, b) -> {
				return deleteSecondaryNamedInsured(a, b);
			});
				
			put(Actions.ZIPCODE_LOOKUP, (a, b) -> {
				return zipcodeLookup(a, b);
			});
			put(Actions.EDIT_SECONDARY_NAMED_INSURED, (a, b) -> {
				return editNamedInsured(a, b);
			});
			put(Actions.TESTBYIP, (a, b) -> {
				return dummy(a, b);
			});
			put(Actions.SAVEANDEXIT, (a, b) -> {
				return saveAndExit(a, b);
			});
		}
	};
	
	HandOffService handOffService = new HandOffService();


	@Override
	public R delegate(Actions actions, LinkedMultiValueMap<String, Object> requestData)  throws DSCIQuoteException {

		// null check and getting operationType

		String operationType = requestData.containsKey("operationType")
				? (String) requestData.get("operationType").get(0) : "";

		if (!opTypeMap.containsKey(Actions.valueOf(operationType.toUpperCase()))) {
			throw new IllegalArgumentException(String.format("%s is not supported", actions));
		}
		return opTypeMap.get(actions).apply(restTemplate, requestData);
	}
	

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R viewBeforeYouIssuePolicyData(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws RuntimeException {
	    
		debug("Request:in viewBeforeYouIssuePolicyData method:" + requestData);
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		
		//uncomment below to check locally
		/*HashMap<String,String> tokens = new HashMap<>();
		tokens = handOffService.dcAuthentication("admin", "admin");
		String bearerToken1 = tokens.get("JsonWebToken");
		session.setAttribute("JsonWebToken", "Bearer " + "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJHQVUwMzY4IiwiU0lEIjoiRkYzQzY4RjU6NTgyQTRFREQ6OTk1OTE4OEQ6RTg4OUFDQTA6N0EzNUQ4Nzk6NDhFOTBDNEIiLCJTeXN0ZW1JRCI6IkRTQ0kiLCJDTElFTlRJRCI6IiIsIlFVT1RFSUQiOiIiLCJ4UGFzcyI6IiIsIlBhZ2UiOiIiLCJQYWdlU2V0IjoiIiwiVGFyZ2V0UGFnZSI6IiIsIk1hbnVzY3JpcHRJRCI6IiIsIm5iZiI6MTUzNDE0OTA0MSwiZXhwIjoxNTM0MjM1NDQxLCJpYXQiOjE1MzQxNDkwNDEsImlzcyI6IkZPQVBJQVMiLCJhdWQiOiJGT0FQSSJ9.rvotlpO_oNp1prAo8wcGjVwnJ-pEszN67OfZun4HqSc");
		
		String policyId1 = "5408105";
		session.setAttribute("Id", policyId1);
		session.setAttribute("QuoteAmount","604");
		session.setAttribute("startDate","12/04/2014");
		session.setAttribute("endDate","12/04/2014");
		session.setAttribute("asdf", tokens);
		
		HashMap<String,Object> pricingMap = new HashMap<>();
		pricingMap.put("stp", "False");
		pricingMap.put("errorCode", null);
		pricingMap.put("message", "Bindable");
		pricingMap.put("status", 0);
		pricingMap.put("lossRatio", null);*/
		
		
		String bearerToken = session.getAttribute("JsonWebToken").toString();
		
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();
		
		String policyId = (String) session.getAttribute("Id");
		
		String appianData = getDataFromAppian(restTemplate, policyId);
		
		if(!appianData.equals("noData")){
			return (R) new ResponseEntity<String>(appianData, HttpStatus.OK); 
		}
		
		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}
		
		LinkedMultiValueMap <String,Object> linkedMultiValueMap=new LinkedMultiValueMap<>();
		linkedMultiValueMap.add("key","1");
		linkedMultiValueMap.add("policyId", policyId);
			
		linkedMultiValueMap=DSCIUtility.updateRequestDataWithJWTToken(linkedMultiValueMap);
		linkedMultiValueMap.add("bearerToken", bearerToken);
		
		HttpEntity entity = getSaveHttpEntity(linkedMultiValueMap);
	
		String[] separatedResponse = null;
		
		//ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
		String url2 = CreateElementService.getEnvironment().getProperty("url.before.you.issue.policy");
			
		ResponseEntity<String> response = restTemplate.exchange(url2, HttpMethod.POST, entity, String.class);
	
		ObjectMapper mapper = new ObjectMapper();
		HashMap<String, Object> responseMap = new HashMap();
		//String sessionData = "{\"businessName\": null,   \"businessClass\": \"Army and Navy Stores\",   \"state\": \"NJ\",   \"channel\": \"Independent_Agent\",   \"lob\": null,   \"productId\": \"703\",   \"businessAddress\": {     \"streetAddress\": \"373 US HIGHWAY 46 STE 250\",     \"suite\": \"\",     \"city\": \"FAIRFIELD\",     \"state\": \"NJ\",     \"zip\": \"07004\",     \"businessName\": \"KEEP THE PIECES MOVING\"   },   \"businessInfo\": {     \"year\": \"1999\",     \"sales\": \"$123,123\",     \"phone\": \"123-123-1231\"   },   \"effectiveStartDate\": \"07/08/2018\",   \"effectiveEndDate\": \"07/08/2019\",   \"lobs\": [     \"Businessowners\"   ],   \"beforeYouBeginData\": {     \"businessOwnersPolicy\": true,     \"state\": \"NJ\",     \"effectiveDate\": \"2018-07-08\"   },   \"locationData\": [     {       \"Building\": {         \"Building #1\": [           \"bC6388EBBE8C4416A971A5674267B5C31\"         ]       },       \"locationName\": \"Location #1:373 US HIGHWAY 46 STE 250, FAIRFIELD, NJ  07004\",       \"locationId\": \"l6FC43115DCE64EE5A11114B4B8BF1FE7\"     }   ],   \"premiumValue\": null,   \"locationMap\": {     \"Location #1:373 US HIGHWAY 46 STE 250, FAIRFIELD, NJ  07004\": \"l6FC43115DCE64EE5A11114B4B8BF1FE7\"   },   \"subjectType\": null }";
		SessionData sessionDataObj = null;
		BusinessAddress businessAddress = null;
		
		try {
			responseMap = mapper.readValue(response.getBody(), HashMap.class);
			
			sessionDataObj = mapper.readValue(toJson(sessionData), SessionData.class);
			//sessionDataObj = mapper.readValue(sessionData, SessionData.class);
			//session.setAttribute("sessionData", sessionDataObj);
			
		} catch (IOException e) {
			throw new DSCIQuoteException("Exception while mapping the response from Monoapp and sesisonData");
		}
		
		businessAddress = sessionDataObj.getBusinessAddress();
		
	
		List<Map<String, String>> policyQuestions = (List<Map<String, String>>) responseMap.get("Policy_Mailing_Address");

		 
		List<Map<String, String>> prefillPolicyQuestion = new ArrayList<>();
		
		
		HashMap<String, String> tempHashMap = new HashMap();
		String tempString=generateId().toString();
		tempHashMap.put("DuckId", tempString);
		
		List<HashMap<String,String>> inputMapPolicy = new ArrayList<>();
		try {
			inputMapPolicy=mapper.readValue(toJson(responseMap.get("Policy_Mailing_Address")), List.class);
		} catch (IOException e) {
			debug("error while mapping policy info");
		}
		
		
		List<HashMap<String,String>> finalPolicy = insertInMasterPolicy(inputMapPolicy,sessionDataObj.getBusinessAddress().getBusinessName(),businessAddress.getStreetAddress(),businessAddress.getSuite(), businessAddress.getCity(), businessAddress.getState(),businessAddress.getZip());
	
		
		
		responseMap.put("Policy_Mailing_Address", finalPolicy);
		
		
		
		List<HashMap<String,String>> inputMapNI = new ArrayList<>();
		try {
			inputMapNI=mapper.readValue(toJson(responseMap.get("Named_Insured_Info")), List.class);
		} catch (IOException e) {
			debug("error while named insured while mapping");
		}
	
		
		List<HashMap<String,String>> finalNI = insertInMasterNamedInsured(inputMapNI,"AdditionalOtherInterestInput.Name",sessionDataObj.getBusinessAddress().getBusinessName());
		responseMap.put("Named_Insured_Info", finalNI);
		
		
		List<HashMap<String,String>> inputMapLoss = new ArrayList<>();
		try {
			inputMapLoss=mapper.readValue(toJson(responseMap.get("LOSS_CONTROL_CONTACT_INFORMATION")), List.class);
		} catch (IOException e) {
			debug("error while mapping loss contact info");
		}
		
		
		List<HashMap<String,String>> finalLoss = insertInMaster(inputMapLoss,"LOSS_CONTACT_NAME",sessionDataObj.getBusinessAddress().getBusinessName(),"LOSS_CONTACT_PHONE",sessionDataObj.getBusinessInfo().getPhone());
		responseMap.put("LOSS_CONTROL_CONTACT_INFORMATION", finalLoss);

		//insert account record contact
		List<HashMap<String,String>> inputMapAccount= new ArrayList<>();
		try {
			inputMapAccount=mapper.readValue(toJson(responseMap.get("ACCOUNTING_RECORDS_CONTACT_INFORMATION")), List.class);
		} catch (IOException e) {
			debug("error while mapping account contact info");
		}

		
		List<HashMap<String,String>> finalAccount = insertInMaster(inputMapAccount,"ACCOUNTING_RECORDS_CONTACT_NAM",sessionDataObj.getBusinessAddress().getBusinessName(),"ACCOUNTING_RECORDS_PHONE",sessionDataObj.getBusinessInfo().getPhone());
		responseMap.put("ACCOUNTING_RECORDS_CONTACT_INFORMATION", finalAccount);
		
		
		List<HashMap<String,String>> inputMapBusiness= new ArrayList<>();
		try {
			inputMapBusiness=mapper.readValue(toJson(responseMap.get("BUSINESS_ACCOUNT_CONTACT_INFORMATION")), List.class);
		} catch (IOException e) {
			debug("error while mapping account contact info");
		}

		List<HashMap<String,String>> finalBusiness = insertInMaster(inputMapBusiness,"BUSINESS_INSURED_CONTACT_NAME",sessionDataObj.getBusinessAddress().getBusinessName().toString(),"BUSINESS_PHONE",sessionDataObj.getBusinessInfo().getPhone().toString());

		responseMap.put("BUSINESS_ACCOUNT_CONTACT_INFORMATION", finalBusiness);
		
		responseMap.put("premiumValue", sessionData.getQuoteAmount());
		responseMap.put("effectiveStartDate", sessionData.getEffectiveStartDate());
		responseMap.put("effectiveEndDate", sessionData.getEffectiveEndDate());
		
/*		responseMap.put("premiumValue", "2345");
		responseMap.put("effectiveStartDate", "11/11/2012");
		responseMap.put("effectiveEndDate","12/12/2012");*/
		
		

		ResponseEntity<String> responseStr;
		
		responseStr = new ResponseEntity<String>(toJson(responseMap), HttpStatus.OK);
		debug("Response:in viewBeforeYouIssuePolicyData method:" + responseStr);
	
		saveDataInAppian(restTemplate,responseStr.getBody()+"##SessionDetails##"+toJson(sessionData),policyId);
		return (R) responseStr;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R viewSecondaryNamedInsuredInfo(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws RuntimeException {
		debug("Request:in viewBeforeYouIssuePolicyData method:" + requestData);

		String url = CreateElementService.getEnvironment().getProperty("url.before.you.issue.policy");
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		String bearerToken= session.getAttribute("JsonWebToken").toString();
		String policyId = (String) session.getAttribute("Id");
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();
		
		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}

		
		// converting URL to URI
		URI uri = DSCIService.toURI(url);
		
		LinkedMultiValueMap <String,Object> linkedMultiValueMap=new LinkedMultiValueMap<>();
		linkedMultiValueMap.add("key","Secondary");
		linkedMultiValueMap.add("policyId",policyId);
		linkedMultiValueMap.add("bearerToken",bearerToken);
		
		
		HttpEntity entity = getSaveHttpEntity(linkedMultiValueMap);
		
		ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
	

		ObjectMapper mapper = new ObjectMapper();
		HashMap<String, Object> responseMap = new HashMap();
		
		//Bookmark
		
		try {
			responseMap = mapper.readValue(response.getBody(), HashMap.class);
		
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		
		List<HashMap<String, String>> namedInsured = (List<HashMap<String, String>>) responseMap.get("Named_Insured_Info");
		List<HashMap<String, String>> finalNI = insertInMasterNamedInsured(namedInsured,"AdditionalOtherInterestInput.Name",sessionData.getBusinessAddress().getBusinessName());

		responseMap.put("Named_Insured_Info", finalNI);
		
		/*LinkedMultiValueMap<String , Object> tempLinkedMultiValueMap=new LinkedMultiValueMap<>();
		tempLinkedMultiValueMap.add("Named_Insured_Info", responseMap.get("Named_Insured_Info"));
		HashMap<String, String> tempHashMap = new HashMap(); 
		String tempString=generateId().toString();
		tempHashMap.put("DuckId", tempString);
		tempLinkedMultiValueMap.add("Named_Insured_Info", tempHashMap);
		
		responseMap.replace("Named_Insured_Info", responseMap.get("Named_Insured_Info"), tempLinkedMultiValueMap.get("Named_Insured_Info"));
		
		System.out.println("data after adding duck creek id into named insured----->"+responseMap.get("Named_Insured_Info"));*/

	
		ResponseEntity<String> responseStr;
		
		responseStr = new ResponseEntity<String>(toJson(responseMap), HttpStatus.OK);
		//saveDataInAppian(restTemplate,toJson(responseMap)+"##SessionDetails##"+toJson(sessionData),policyId);
		debug("Response:in viewBeforeYouIssuePolicyData method:");
		return (R) responseStr;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public R saveBeforeYouIssuePolicyData(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {

		debug("Request:in saveBeforeYouIssuePolicyData method:");
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		
	
		
		String bearerToken = session.getAttribute("JsonWebToken").toString();
		String policyId =(String) session.getAttribute("Id");
		
		String saveComplete = (String) requestData.get("saveComplete").get(0);
		String url2 = CreateElementService.getEnvironment().getProperty("url.before.you.issue.policy");
		
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();
		HashMap<String,Object> stpDetails = (HashMap<String, Object>) sessionData.getStpDetails();
		String premiumValue = sessionData.getQuoteAmount().toString();
		
		//stpDetails.put("lossRatio",12);
		if (premiumValue == null) {
			
			throw new DSCIQuoteException("premiumValue not found");
		}
		if (stpDetails == null) {
			
			throw new DSCIQuoteException("stpDetails not found");
		}
		if(saveComplete.equals("false")){
			// setting URL
		
			if (policyId == null) {
				throw new DSCIQuoteException("ID not found");
			}

			LinkedMultiValueMap requestData1 = new LinkedMultiValueMap<>();
			requestData1.add("key", "saveNamedInsured");		
			requestData1.add("policyId", policyId);
			requestData1.add("data", requestData.get("data").get(0));
			requestData1.add("bearerToken",bearerToken);
			
			
			HttpEntity saveEntity = getSaveHttpEntity(requestData1);
			ResponseEntity<String> response = restTemplate.exchange(url2, HttpMethod.POST, saveEntity, String.class);
					
			if (response.getBody().isEmpty()) {
				debug("Error: response is empty in saveBeforeYouIssuePolicyData method.");
				String message = CreateElementService.getEnvironment().getProperty("response.null");
				throw new DSCIQuoteException(message);
			}
			
			ObjectMapper mapper = new ObjectMapper();
			HashMap<String,List<String>> mappedProducers = new HashMap();
			
			try {
				mappedProducers = mapper.readValue(response.getBody(), HashMap.class);
			} catch (IOException e) {
				throw new DSCIQuoteException("error while parsing producers data");
			}
			String insertedDropdown = insertProducerDropdown(requestData,mappedProducers.get("data"));
			try {
				makeStatusSavedInPrimaryAndSaveInAppian(restTemplate,requestData);
			} catch (Exception e) {
				throw new DSCIQuoteException("error while calling makeStatusSavedInPrimaryAndSaveInAppian method");
			}
			
			debug("Response:in saveBeforeYouIssuePolicyData method:");
			return  (R)response;
		}
		
		String lossRatio;
		if(stpDetails.get("lossRatio")==null) lossRatio= "null";
		else lossRatio = stpDetails.get("lossRatio").toString();
		
		LinkedMultiValueMap requestData2 = new LinkedMultiValueMap();
		requestData2.add("data", requestData.get("data").get(0));
		requestData2.add("key", "issuePolicy");
		requestData2.add("policyId", policyId);
		requestData2.add("bearerToken", bearerToken);
		requestData2.add("premiumValue", premiumValue);
		requestData2.add("lossRatio",lossRatio);
		
		
		HttpEntity saveEntity2 = getSaveHttpEntity(requestData2);

		// rest call
	
		ResponseEntity<String> response = restTemplate.exchange(url2, HttpMethod.POST, saveEntity2, String.class);
		if (response.getBody().isEmpty()) {
			debug("Error: response is empty in saveBeforeYouIssuePolicyData method.");
			String message = CreateElementService.getEnvironment().getProperty("response.null");
			throw new DSCIQuoteException(message);
		}
		
		//if(markCodeStatusInAppian(restTemplate,policyId)) return (R) new ResponseEntity("Success",HttpStatus.OK);
	/*	if(requestData.containsKey("saveAndExit")){
			session.invalidate();
		}*/
		saveDataInAppian(restTemplate,toJson(requestData.get("data").get(0))+"##SessionDetails##"+toJson(sessionData),policyId);
		debug("Response:in saveBeforeYouIssuePolicyData method:");
		return (R) response;
	}
	
	
	/**
	 * make the status of the named insured as save in JSON
	 */
	public String makeStatusSavedInPrimaryAndSaveInAppian(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData){
		
		debug("Request:in saveBeforeYouIssuePolicyData method:");
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		String policyId =  (String) session.getAttribute("Id");
		String bearerToken = session.getAttribute("JsonWebToken").toString();
		
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();
		
		HashMap<String,List<HashMap<String,Object>>> mappedResponse = (HashMap<String, List<HashMap<String, Object>>>) requestData.get("data").get(0);
		/*mappedResponse.entrySet().parallelStream().filter(x->x.getKey().equals("Named_Insured_Info")).forEach(x->{
			
			x.getValue().parallelStream().filter(q->q.containsKey("status")&&q.get("status").equals("save")).forEach(q->q.put("status", "saved"));
		});*/
		mappedResponse.get("Named_Insured_Info").parallelStream().filter(q->q.containsKey("status")&&q.get("status").equals("save")).forEach(q->q.put("status", "saved"));
		mappedResponse.get("producerdata").parallelStream().filter(x->x.get("systemRefId").equals("Producer_Name")).forEach(x->x.put("editable", "N"));
		saveDataInAppian(restTemplate,toJson(mappedResponse)+"##SessionDetails##"+toJson(sessionData),policyId);
		return "Success";
	}
	
	/**
	 * save Secondary named insured in Duckcreek
	 * @param restTemplate
	 * @param requestData
	 * @return
	 */
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public R saveSecondaryNamedInsured(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {

		
		debug("Request:in saveBeforeYouIssuePolicyData method:");
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		String policyId =  (String) session.getAttribute("Id");
		String bearerToken = session.getAttribute("JsonWebToken").toString();
		
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();
		
		String url2 = CreateElementService.getEnvironment().getProperty("url.before.you.issue.policy");

		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}

		LinkedMultiValueMap requestData1 = new LinkedMultiValueMap<>();
		requestData1.add("key", "SaveSecondary");
		requestData1.add("policyId", policyId);
		requestData1.add("data", requestData.get("data").get(0));
		requestData1.add("bearerToken", bearerToken);

		HttpEntity saveEntity = getSaveHttpEntity(requestData1);
		ResponseEntity<String> response = restTemplate.exchange(url2, HttpMethod.POST, saveEntity, String.class);

		if (response.getBody().isEmpty()) {
			debug("Error: response is empty in saveBeforeYouIssuePolicyData method.");
			String message = CreateElementService.getEnvironment().getProperty("response.null");
			throw new DSCIQuoteException(message);
		}
		makeStatusSavedAndSaveInAppian(restTemplate,requestData);
		//saveDataInAppian(restTemplate,toJson(requestData.get("data").get(0))+"##SessionDetails##"+toJson(sessionData),policyId);
		debug("Response:in saveBeforeYouIssuePolicyData method:");
		return (R) response;

	}
	
	/**
	 * make the status of the named insured as save in JSON
	 */
	public String makeStatusSavedAndSaveInAppian(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData){
		
		debug("Request:in saveBeforeYouIssuePolicyData method:");
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		String policyId =  (String) session.getAttribute("Id");
		String bearerToken = session.getAttribute("JsonWebToken").toString();
		
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();
		
		HashMap<String,List<HashMap<String,Object>>> mappedResponse = (HashMap<String, List<HashMap<String, Object>>>) requestData.get("data").get(0);
		mappedResponse.entrySet().parallelStream().filter(x->x.getKey().contains("Named_Insured_Info_")).forEach(x->{
			
			x.getValue().parallelStream().filter(q->q.containsKey("status")&&q.get("status").equals("save")).forEach(q->q.put("status", "saved"));
		});
		saveDataInAppian(restTemplate,toJson(mappedResponse)+"##SessionDetails##"+toJson(sessionData),policyId);
		return "Success";
	}
	
	/**
	 * method to delete named insured in Duck creek
	 * @param restTemplate
	 * @param requestData
	 * @return 200 OK
	 */
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public R deleteSecondaryNamedInsured(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {

		
		debug("Request:in saveBeforeYouIssuePolicyData method:");
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		String duckId = requestData.get("duckId").get(0).toString();
		String policyId = (String) session.getAttribute("Id");
		String bearerToken = session.getAttribute("JsonWebToken").toString();
		
		String url2 = CreateElementService.getEnvironment().getProperty("url.before.you.issue.policy");

		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}

		LinkedMultiValueMap requestData1 = new LinkedMultiValueMap<>();
		requestData1.add("key", "removeNI");
		requestData1.add("policyId", policyId);
		requestData1.add("duckID", duckId);
		requestData1.add("bearerToken", bearerToken);
		
		HttpEntity saveEntity = getSaveHttpEntity(requestData1);
		ResponseEntity<String> response = restTemplate.exchange(url2, HttpMethod.POST, saveEntity, String.class);

		if (response.getBody().isEmpty()) {
			debug("Error: response is empty in deleteSecondaryNamedInsured method.");
			String message = CreateElementService.getEnvironment().getProperty("response.null");
			throw new DSCIQuoteException(message);
		}

		try {
			deleteNamedInsuredInAppian(restTemplate, requestData);
		} catch (Exception e) {
			throw new DSCIQuoteException("Error while delting named insured in Appian");
		}
		
		debug("Response:in saveBeforeYouIssuePolicyData method:");
		return (R) response;

	}

	

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public R editNamedInsured(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {

		
		debug("Request:in editNamedInsured method:");
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		String policyId =  (String) session.getAttribute("Id");	
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();
		String bearerToken = session.getAttribute("JsonWebToken").toString();
		
		String url2 = CreateElementService.getEnvironment().getProperty("url.before.you.issue.policy");
		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}

		LinkedMultiValueMap requestData1 = new LinkedMultiValueMap<>();
		requestData1.add("key", "editNamedInsured");
		requestData1.add("policyId", policyId);
		requestData1.add("data", requestData.get("data").get(0));
		requestData1.add("bearerToken", bearerToken);

		HttpEntity saveEntity = getSaveHttpEntity(requestData1);
		ResponseEntity<String> response = restTemplate.exchange(url2, HttpMethod.POST, saveEntity, String.class);

		if (response.getBody().isEmpty()) {
			debug("Error: response is empty in saveBeforeYouIssuePolicyData method.");
			String message = CreateElementService.getEnvironment().getProperty("response.null");
			throw new DSCIQuoteException(message);
		}

		saveDataInAppian(restTemplate,toJson(requestData.get("data").get(0))+"##SessionDetails##"+toJson(sessionData),policyId);
		debug("Response:in editNamedInsured method:");
		return (R) response;

	}

	
	protected R dummy(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		
		/*HashMap<String,String> map = new HashMap<>();
		map.put("dummy", "dummy2");
		String data = toJson(map) + "##SessionDetails##" + toJson(map);
		String st = saveDataInAppian(restTemplate,data,"5391565");*/
		
		Boolean bool  = markCodeStatusInAppian(restTemplate, requestData.get("policyId").get(0).toString());
		
		return  (R) new ResponseEntity<String>("",HttpStatus.OK);
	}
	
	public boolean markCodeStatusInAppian(RestTemplate restTemplate, String policyId){
		String url = CreateElementService.getEnvironment().getProperty("url.mark.quote.status");
		url+="?policyid="+ policyId+"&quotestatus=Submission Authorized";
		
		ResponseEntity<String> response = restTemplate.exchange(toURI(url), HttpMethod.GET, getViewHttpEntity(), String.class);
		HashMap<String,Object> responseMap = new HashMap<>();
		
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			responseMap = mapper.readValue(response.getBody(), HashMap.class);
		} catch (IOException e) {
			debug("error while parsing appian response");
		}
	
		if(responseMap.get("success").toString().equals("false")) return false;
		
		return true;
	}

	protected R zipcodeLookup(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		
		debug("Request:in zipcodelookup method:");
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		String policyId =(String) session.getAttribute("Id");
		String bearerToken = session.getAttribute("JsonWebToken").toString();
		String url = CreateElementService.getEnvironment().getProperty("url.before.you.issue.policy");
		
		LinkedMultiValueMap requestData1 = new LinkedMultiValueMap<>();
		requestData1.add("key", "Zipcode");		
		requestData1.add("policyId", Integer.parseInt(policyId));
		requestData1.add("zip", requestData.get("data").get(0));
		requestData1.add("bearerToken", bearerToken);
		
		
		HttpEntity saveEntity = getSaveHttpEntity(requestData1);
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, saveEntity, String.class);
		
		
		if (response.getBody().isEmpty()) {
			debug("Error: response is empty in zipcodelookup method.");
			String message = CreateElementService.getEnvironment().getProperty("response.null");
			throw new DSCIQuoteException(message);
		}
		
		debug("Response:in zipcodelookup method:");
		return (R) response;
	}
	

	
	public List<HashMap<String,String>> insertInMasterNamedInsured(List<HashMap<String,String>> input,String sysRefIdName,String valueName){
	
		
		input.parallelStream().filter(x->x.containsKey("systemRefId")).filter(x->x.get("systemRefId").equals(sysRefIdName)).forEach(x->{

			if(x.get("systemRefId").equals(sysRefIdName)) {x.put("userValue", valueName);}
			
			
		});
		
		return input;
	}
	
	public List<HashMap<String,String>> insertInMaster(List<HashMap<String,String>> input,String sysRefIdName,String valueName,String sysRefIdPhone, String ValuePhone){
	
		
		input.parallelStream().filter(x->x.get("systemRefId").equals(sysRefIdName)||x.get("systemRefId").equals(sysRefIdPhone)).forEach(x->{
			if(x.get("systemRefId").equals(sysRefIdName)) {x.put("userValue", valueName);x.put("defaultValue", valueName);}
			if(x.get("systemRefId").equals(sysRefIdPhone)) {x.put("userValue", ValuePhone);x.put("defaultValue", ValuePhone);}
			
		});
		
		return input;
	}
	
	public List<HashMap<String,String>> insertInMasterPolicy(List<HashMap<String,String>> input,String name,String st,String apt, String city, String state,String zip){
	
		
		input.parallelStream().forEach(x->{
			String refId= x.get("systemRefId").toString();
			if(refId.equals("NAME")) {x.put("userValue", name);x.put("defaultValue", name);}
			else if (refId.equals("STREET_ADDRESS")) {x.put("userValue", st);x.put("defaultValue", st);}
			else if (refId.equals("APT_SUITE")) {x.put("userValue", apt);x.put("defaultValue", apt);}
			else if (refId.equals("CITY")) {x.put("userValue", city);x.put("defaultValue", city);}
			else if (refId.equals("STATE")) {x.put("userValue", state);x.put("defaultValue", state);}
			else if (refId.equals("ZIP_CODE")) {x.put("userValue", zip); x.put("defaultValue", zip);}
			
		});
		
		return input;
	}
	
	/**
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return
	 */
	public String saveDataInAppian(RestTemplate restTemplate, String dataToSave,String policyId){
		// removing control data

		String QuoteSection = "13";
		String saveURL = CreateElementService.getEnvironment().getProperty("url.saveQuoteDetails");
		saveURL += "?policy_id=" + policyId + "&quote_section=" + QuoteSection + "&action=SC";
		URI uri = toURI(saveURL);
	
		
		try {
			restTemplate.exchange(uri, HttpMethod.POST, getSaveHttpEntity(dataToSave), String.class);
		} catch (RestClientException e) {
			
			e.printStackTrace();
		}
		return "Success";
	}
	
	public String getDataFromAppian(RestTemplate restTemplate,String policyId){
	
		
		String url = CreateElementService.getEnvironment().getProperty("url.getQuestion");
		String QuoteSection = "13";
		url += "?policy_id=" + policyId + "&quote_section=" + QuoteSection + "&fetch_master=No";
		URI uri = toURI(url);
		
		ResponseEntity<String> resp = restTemplate.exchange(uri, HttpMethod.POST, getSaveHttpEntity(new HashMap<>()), String.class);

		String[] newIn = resp.getBody().split("##SessionData##");

		
		String responseFromAppian = resp.getBody();
		if(responseFromAppian.toString().equals("##SessionData##")) return "noData";
		String[] withOutSession = responseFromAppian.split("##SessionData##");
		if(withOutSession[0].length()<1) return "noData";
		else{
			
			return withOutSession[0];
		}
	}
	
	
	//this is actually BYIP delete
	public R deleteNamedInsuredInAppian(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData){
		// removing control data
		requestData.remove("elementType");
		requestData.remove("operationType");
		
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		String policyId =  (String) session.getAttribute("Id");	
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();
		// setting URL
		Object data = requestData.get("data").get(0);
		String duckId = requestData.get("duckId").get(0).toString();
		ObjectMapper mapper = new ObjectMapper();
		HashMap<String,List<HashMap<String,Object>>> mappedObject = new HashMap<>();
		try {
			mappedObject = mapper.readValue(toJson(data),HashMap.class);
		} catch (IOException e) {
			debug("");
		}
		List<Object> mappedKeyToDelete = mappedObject.entrySet().parallelStream().filter(x->x.getKey().contains("Named_Insured_Info_")).map(x->{
			List<HashMap<String,Object>> object = x.getValue();
			
			List<Object> duckIdList  =object.parallelStream().filter(q->q.containsKey("DuckId")).map(q->{return q.get("DuckId");}).collect(Collectors.toList());
			if(duckIdList.get(0).toString().equals(duckId)) return x.getKey();
				
			return null;
				
		}).filter(r->{if(r==null) return false;else return true;}).collect(Collectors.toList());

		mappedObject.remove(mappedKeyToDelete.get(0).toString());
		saveDataInAppian(restTemplate,toJson(mappedObject)+"##SessionDetails##"+toJson(sessionData),policyId);
		return (R) new ResponseEntity<String>(toJson(mappedObject),HttpStatus.OK);
	}
	
	/**
	 * method to save the data in Appian and kill session
	 * @param restTemplate
	 * @param requestData
	 * @return 200 OK
	 */
	public R saveAndExit(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData){
		
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		
		String policyId = (String) session.getAttribute("Id");
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();
		String dataToSave = toJson(requestData.get("data").get(0))  +"##SessionDetails##" +toJson(sessionData);

		String QuoteSection = "13";
		String saveURL = CreateElementService.getEnvironment().getProperty("url.saveQuoteDetails");
		saveURL += "?policy_id=" + policyId + "&quote_section=" + QuoteSection + "&action=SE";
		URI uri = toURI(saveURL);
			
		try {
			restTemplate.exchange(uri, HttpMethod.POST, getSaveHttpEntity(dataToSave), String.class);
		} catch (RestClientException e) {
			debug("Error while making save and exit call in BYIP saveAndExit function");
		}
		session.invalidate();
		return (R) new ResponseEntity<String>("{\"status\":\"Success\"}",HttpStatus.OK);
	}
	

	
	/**
	 * insert producer dropdown in the save primary named insured data to save it in Appian
	 * @param requestData
	 * @param producersList
	 * @return Json of final data to be saved in appian
	 */

	public String insertProducerDropdown(LinkedMultiValueMap<String, Object> requestData, Object producersList){
		
		HashMap<String,List<HashMap<String,Object>>> mappedData= (HashMap<String, List<HashMap<String, Object>>>) requestData.get("data").get(0); 
		mappedData.get("producerdata").parallelStream().filter(q->q.get("systemRefId").equals("Producer_Name")).forEach(q->{q.put("appearUI", "N");q.put("valueName",producersList);});
		return toJson(mappedData);
	}
	public UUID generateId(){
		return UUID.randomUUID();
	}

}

