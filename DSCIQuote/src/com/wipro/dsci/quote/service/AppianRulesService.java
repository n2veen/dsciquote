package com.wipro.dsci.quote.service;

import java.io.IOException;
import java.net.URI;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wipro.dsci.quote.enums.Actions;
import com.wipro.dsci.quote.exception.DSCIQuoteException;
import com.wipro.dsci.quote.model.SessionData;
import com.wipro.dsci.quote.util.DSCIUtility;

import static com.wipro.dsci.quote.service.DSCIService.getViewHttpEntity;
import static com.wipro.dsci.quote.service.DSCIService.toJson;
import static com.wipro.dsci.quote.service.DSCIService.getSaveHttpEntity;

@Component
@SuppressWarnings({ "unchecked" })
public class AppianRulesService<R, E> implements DSCIService<R, DSCIQuoteException> {

	@Autowired
	RestTemplate restTemplate;
	
	public static void debug(String format, Object... args) {
		Logger logger = LoggerFactory.getLogger(AppianRulesService.class);
		if (logger.isDebugEnabled()) {
			logger.debug(format, args);
		}
	}

	private Map<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>> opTypeMap = new HashMap<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>>() {

		private static final long serialVersionUID = 1L;
		{
			put(Actions.VALIDATEBUILDINGDETAILS, (a, b) -> {
				return saveOccupancyDetailsInAppian(a, b);
			});

			put(Actions.VALIDATE_SIC_CODE, (a, b) -> {
				return validateSICCode(a, b);
			});
			put(Actions.LENGTHVALIDATION, (a, b) -> {
				return BOPPolicyLengthValidation(a, b);
			});

			put(Actions.BRVALIDATECLAIMSORLOSS, (a, b) -> {
				return ClaimorLossUnderwritingQuesDETAILS(a, b);
			});
			put(Actions.VALIDATE_EXTINGUISHING_SYSTEM, (a, b) -> {
				return validateExtinguishingSystem(a, b);
			});

			put(Actions.VALIDATE_HEALTH_DPTMTVIOLATION, (a, b) -> {
				return validateHealthDptmtViolation(a, b);
			});

			put(Actions.VALIDATE_EQUIPMENT_LIMIT, (a, b) -> {
				return validateEquipmentLimit(a, b);
			});

			put(Actions.BRVALIDATEYEARBUILTANDDEPENDENTS, (a, b) -> {
				return validateYearBuilt(a, b);
			});

			put(Actions.VIEW_QUESTION_BUSINESS_STATUS, (a, b) -> {
				return viewQuestionBusinessStatus(a, b);
			});
			put(Actions.BRVALIDATEELIGIBILITYQN1, (a, b) -> {
				return brValidateEligibilityQn1(a, b);
			});
			put(Actions.BRVALIDATEELIGIBILITYQN2, (a, b) -> {
				return brValidateEligibilityQn2(a, b);
			});
			put(Actions.BRVALIDATEELIGIBILITYQN3, (a, b) -> {
				return brValidateEligibilityQn3(a, b);
			});
			put(Actions.BRVALIDATEELIGIBILITYQN4, (a, b) -> {
				return brValidateEligibilityQn4(a, b);
			});
			put(Actions.BRVALIDATEELIGIBILITYQN5, (a, b) -> {
				return brValidateEligibilityQn5(a, b);
			});
			put(Actions.BRVALIDATEELIGIBILITYQN6, (a, b) -> {
				return brValidateEligibilityQn6(a, b);
			});
			put(Actions.BRVALIDATEYEARSTARTED, (a, b) -> {
				return validateYearStarted(a, b);
			});

			put(Actions.BRVALIDATEANNUALSALES, (a, b) -> {
				return brvalidateannualsales(a, b);
			});
			put(Actions.BRVALIDATEELIGIBILITYQN8, (a, b) -> {
				return brValidateEligibilityQn8(a, b);
			});

			put(Actions.BRVALIDATERISKSTATES, (a, b) -> {
				return brValidateRiskStates(a, b);
			});
			put(Actions.BRVALIDATERISKSTATES, (a, b) -> {
				return brValidateRiskStates(a, b);
			});
			put(Actions.BRVALIDATERISKSTATES, (a, b) -> {
				return brValidateRiskStates(a, b);
			});

			put(Actions.BR_VALIDATE_BUILDING_DETAILS, (a, b) -> {
				return brValidateBuildingDetails(a, b);
			});

			put(Actions.BRCALCULATEBUILDINGVACANCY, (a, b) -> {
				return brCalaulateBuildingVacancy(a, b);
			});

			put(Actions.BRVALIDATEBUILDINGDETAILSUWQN, (a, b) -> {
				return brValidatebuildingDetailsSuwqn(a, b);
			});

			put(Actions.BR_VALIDATE_BUSINESS_PERSONAL_PROPERTY_LIMIT, (a, b) -> {
				return brValidateBusinessPersonalPropertyLimit(a, b);
			});
			put(Actions.BRVALIDATENUMBEROFSTORIES, (a, b) -> {
				return brValidateNumberOfStories(a, b);
			});

			put(Actions.BR_VALIDATE_BUILDING_LIMIT, (a, b) -> {
				return brValidatebuildingLimit(a, b);
			});

			put(Actions.BR_OCCUPANCY_VALIDATE_SQUARE_FOOTAGE, (a, b) -> {
				return brOccupancyValidateSquareFootage(a, b);
			});

			put(Actions.BRVALIDATEANNUALRENTALINCOME, (a, b) -> {
				return brValidateAnnualRentalIncome(a, b);
			});

			put(Actions.BRVALIDATEAPPLICANTINTANDDEPENDENTS, (a, b) -> {

				return brValidateApplicantIntandDependents(a, b);

			});
			put(Actions.BRCONSTRTYPEBASEDMIXEDCONSTR, (a, b) -> {
				return brconstrtypebasedmixedconstr(a, b);
			});
			put(Actions.BRVALIDATEMIXEDCONSTRUCTION, (a, b) -> {
				return brvalidatemixedconstruction(a, b);
			});
			put(Actions.PERMANENTLYINSTALLEDEQUIPMENTLMT, (a, b) -> {
				return permanentlyinstalledequipmentlmt(a, b);
			});
			put(Actions.DISPLAY_PERMANENTLY_INSTALLED_EQUIPMENT, (a, b) -> {
				return brdisplaypermanentlyinstldequipment(a, b);
			});
			put(Actions.ZIPCODELOOKUP, (a, b) -> {
				return zipCodeLooUp(a, b);
			});
			put(Actions.ZIPCODELOOKUP1, (a, b) -> {
				return zipCodeLooUp1(a, b);
			});
			put(Actions.BRVALIDATEPOLICYPREMIUM, (a, b) -> {
				return validatePolicyPremium(a, b);
			});
			put(Actions.BRTERRITORYSTATEBASEDPROTECTCLAS, (a, b) -> {
				return validateProtectionClass(a, b);
			});
			put(Actions.BRVALIDATESTATE, (a, b) -> {
				return brValidateState(a, b);
			});
			// PricingAndCoverages
			put(Actions.BRVALIDATECOVERAGE1R1, (a, b) -> {
				return brValidateCoverage1R1(a, b);
			});
			put(Actions.BRVALIDATECOVERAGE1R2, (a, b) -> {
				return brValidateCoverage1R2(a, b);
			});
			put(Actions.BRVALIDATECOVERAGE2R1, (a, b) -> {
				return brValidateCoverage2R1(a, b);
			});
			put(Actions.BRVALIDATECOVERAGE3R1, (a, b) -> {
				return brValidateCoverage3R1(a, b);
			});

			put(Actions.BRVALIDATECOVERAGE4R1, (a, b) -> {
				return brValidateCoverage4R1(a, b);
			});
			put(Actions.BRVALIDATECOVERAGE4R2, (a, b) -> {
				return brValidateCoverage4R2(a, b);
			});
			put(Actions.BRVALIDATECOVERAGE6R1, (a, b) -> {
				return brValidateCoverage6R1(a, b);
			});
			put(Actions.BRVALIDATECOVERAGE9R1, (a, b) -> {
				return brValidateCoverage9R1(a, b);
			});
			put(Actions.BRVALIDATECOVERAGE9R2, (a, b) -> {
				return brValidateCoverage9R2(a, b);
			});
			put(Actions.BRVALIDATECOVERAGE8R1, (a, b) -> {
				return brValidateCoverage8R1(a, b);
			});
			put(Actions.BRVALIDATEPRIORACTSCOVERAGE, (a, b) -> {

				return brValidatePriorActsCoverage(a, b);

			});
			put(Actions.BRVALIDATENOOFEMPLOYEES, (a, b) -> {

				return brValidateNoOfEmployees(a, b);

			});
			put(Actions.BRVALIDATEEACHCLAIMLIMIT, (a, b) -> {

				return brValidateEachClaimLimit(a, b);

			});
			put(Actions.VALIDATELOB, (a, b) -> {
				return validateLOB(a, b);
			});
			// before You Issue Policy
			put(Actions.BRVALIDATESELECTTYPEANDDBA, (a, b) -> {
				return validateNamedInsured(a, b);
			});
			put(Actions.DISPLAYBUSINESSINFORMATION3, (a, b) -> {
				return displaybusinessinformation3(a, b);
			});

			put(Actions.DISPLAYBUSINESSINFORMATION2, (a, b) -> {
				return displaybusinessinformation2(a, b);
			});

			put(Actions.DISPLAYBUSINESSINFORMATION1, (a, b) -> {
				return displaybusinessinformation1(a, b);
			});

			put(Actions.BR_ANNUAL_SALES_TOTAL, (a, b) -> {
				return compareAnnualSalesBusinessClass(a, b);
			});

		}
	};
	
	@Override
	public R delegate(Actions actions, LinkedMultiValueMap<String, Object> requestData) throws DSCIQuoteException {

		// null check and getting operationType

		String operationType = requestData.containsKey("operationType")
				? (String) requestData.get("operationType").get(0) : "";
		if (!opTypeMap.containsKey(Actions.valueOf(operationType.toUpperCase()))) {
			throw new IllegalArgumentException(String.format("%s is not supported", actions));
		}
		return opTypeMap.get(actions).apply(restTemplate, requestData);
	}

	//
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R compareAnnualSalesBusinessClass(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws RuntimeException {

		debug("Request:in VIEW APPIAN  Annaul sales  method:" + requestData);

		// removing control data
		requestData.remove("elementType");
		requestData.remove("operationType");
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();

		System.out.println("Before List");

		Integer TotalAnnual = Integer.parseInt(requestData.get("data").get(0).toString());
		System.out.println("previosAnnualSales" + TotalAnnual);

		String annualSalesB = (sessionData.getBusinessInfo().getSales());
		
		
		String anualSalesBC = (annualSalesB.replaceAll("[$,]|\\s", ""));

	System.out.println("**annualSalesBC business class " + annualSalesB);

		ObjectMapper mapper = new ObjectMapper();
		HashMap<String, Object> final_response = new HashMap<>();

		HttpEntity httpEntity = getViewHttpEntity();

		System.out.println("Apian call started");

		String fetchurl = CreateElementService.getEnvironment().getProperty("url.uw.rule.anualsaleC");

		fetchurl = fetchurl + "?var1=" + anualSalesBC + "&var2=" + TotalAnnual;
		ResponseEntity<String> response = restTemplate.exchange(fetchurl, HttpMethod.GET, httpEntity, String.class);

		System.out.println(response.getBody());

		LinkedMultiValueMap<String, Object> data = new LinkedMultiValueMap<>();

		debug("Connecting: VIEW APPAIN ANNUAL SALES: " + response);

		return (R) response;
	}

	//

	/**
	 * 
	 * appian rule for saveOccupancyDetails
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return R
	 */

	//

	// Card 1019
	public R brValidatePriorActsCoverage(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in brValidatePriorActsCoverage method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.brvalidateprioractscoverage");

		String var = requestData.containsKey("data") ? requestData.get("data").get(0).toString() : "";

		saveURL = saveURL + "?var1=" + var;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> response = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity, String.class);

		debug("Response:in brValidatePriorActsCoverage method:" + response);

		return (R) response;
	}

	// Card 1019
	public R brValidateNoOfEmployees(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in brValidateNoOfEmployees method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.brvalidatenoofemployees");

		String var = requestData.containsKey("data") ? requestData.get("data").get(0).toString() : "";

		saveURL = saveURL + "?var1=" + var;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> response = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity, String.class);

		debug("Response:in brValidateNoOfEmployees method:" + response);

		return (R) response;
	}

	// Card 1019
	public R brValidateEachClaimLimit(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in brValidateEachClaimLimit method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.brvalidateeachclaimlimit");

		String var = requestData.containsKey("data") ? requestData.get("data").get(0).toString() : "";

		saveURL = saveURL + "?var1=" + var;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> response = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity, String.class);

		debug("Response:in brValidateEachClaimLimit method:" + response);

		return (R) response;
	}

	public R saveOccupancyDetailsInAppian(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in saveOccupancyDetailsInAppian method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.saveOccupancyDetails");

		// null check and getting applicantInterest and multipleOccupancy

		String applicantInterest = requestData.containsKey("applicantInterest")
				? requestData.get("applicantInterest").get(0).toString() : "";
		String multipleOccupancy = requestData.containsKey("multipleOccupancy")
				? requestData.get("multipleOccupancy").get(0).toString() : "";
		saveURL = saveURL + "?var1=" + applicantInterest + "&var2=" + multipleOccupancy;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		// System.out.println("URL -------"+saveURL);
		ResponseEntity<String> response = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity, String.class);

		debug("Response:in saveOccupancyDetailsInAppian method:" + response);

		return (R) response;
	}

	/**
	 * appian rule to validate SICCode
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return R
	 */
	public R validateSICCode(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in validateSICCode method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.validatesiccode");

		// null check and getting applicantInterest and multipleOccupancy

		String SICCode = requestData.containsKey("SICCode") ? requestData.get("SICCode").get(0).toString() : "";

		saveURL = saveURL + "?var1=" + SICCode;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> response = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity, String.class);

		debug("Response:in validateSICCode method:" + response);

		return (R) response;
	}

	// Additional Eligibility Questions _ Professional Services
	/**
	 * appian rule for yes rule
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return R
	 */
	public R brValidateEligibilityQn1(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in brValidateEligibilityYQn1 method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.brValidateEligibilityQn1");

		String brValidateEligibilityQn = requestData.containsKey("data") ? requestData.get("data").get(0).toString()
				: "";

		saveURL = saveURL + "?var1=" + brValidateEligibilityQn;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> responseFromAppian = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity,
				String.class);

		debug("Response:in brValidateEligibilityYesQn method:" + responseFromAppian);

		return (R) responseFromAppian;
	}

	// Policy Underwriting Questions for Restaurants Business Class
	/**
	 * appian rule for validateExtinguishingSystem
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return R
	 */
	public R validateExtinguishingSystem(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in validateExtinguishingSystem method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.brvalidateextinguishingsystem");

		String brvalidateextinguishingsystem = requestData.containsKey("Are_all_cooking_equipment_comm")
				? requestData.get("Are_all_cooking_equipment_comm").get(0).toString() : "";

		saveURL = saveURL + "?var1=" + brvalidateextinguishingsystem;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> responseFromAppian = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity,
				String.class);

		debug("Response:in validateExtinguishingSystem method:" + responseFromAppian);

		return (R) responseFromAppian;
	}

	// Policy Underwriting Questions for Restaurants Business Class
	/**
	 * appian rule for validating HealthDptmtViolation
	 * 
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return R
	 */
	public R validateHealthDptmtViolation(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in validateHealthDptmtViolation method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.brvalidatehealthdptmtviolation");

		String brvalidatehealthdptmtviolation = requestData.containsKey("Has_the_risk_had_any_Health_De")
				? requestData.get("Has_the_risk_had_any_Health_De").get(0).toString() : "";

		saveURL = saveURL + "?var1=" + brvalidatehealthdptmtviolation;

		System.out.println(saveURL);
		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> responseFromAppian = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity,
				String.class);

		debug("Response:in validateHealthDptmtViolation method:" + responseFromAppian);
		return (R) responseFromAppian;
	}

	// Display Permanently Installed Equipment Limit
	/**
	 * @param restTemplate
	 * @param requestData
	 * @return R
	 */
	public R validateEquipmentLimit(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in validateEquipmentLimit method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.brvalidateequipmentlimit");

		String brvalidateequipmentlimit = requestData.containsKey("BRVALIDATEEQUIPMENTLIMIT")
				? requestData.get("BRVALIDATEEQUIPMENTLIMIT").get(0).toString() : "";

		saveURL = saveURL + "?var1=" + brvalidateequipmentlimit;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> response = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity, String.class);

		debug("Response:in validateEquipmentLimit method:" + response);

		return (R) response;
	}

	/**
	 * 
	 * appian rule for business owner policy length validation
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return R
	 */
	public R BOPPolicyLengthValidation(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in BOPPolicyLengthValidation method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.lengthValidation");

		// null check and getting applicantInterest and multipleOccupancy

		String date1 = requestData.containsKey("date1") && requestData.get("date1").size() != 0
				&& requestData.get("date1") != null ? requestData.get("date1").get(0).toString() : "";
		String date2 = requestData.containsKey("date2") && requestData.get("date2").size() != 0
				&& requestData.get("date2") != null ? requestData.get("date2").get(0).toString() : "";

		saveURL = saveURL + "?var1=" + date1 + "&var2=" + date2;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> response = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity, String.class);

		debug("Response:in BOPPolicyLengthValidation method:" + response);

		return (R) response;
	}

	/**
	 * 
	 * appian rule for ClaimorLoss
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return R
	 */
	public R ClaimorLossUnderwritingQuesDETAILS(RestTemplate restTemplate,
			LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in ClaimorLossUnderwritingQuesDETAILS method:" + requestData);

		String saveURL = CreateElementService.getEnvironment().getProperty("url.claimorlossunderwriting");

		String response = requestData.containsKey("response") ? requestData.get("response").get(0).toString() : "";

		saveURL = saveURL + "?var1=" + response;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> responseFromAppian = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity,
				String.class);

		debug("Response:in ClaimorLossUnderwritingQuesDETAILS method:" + responseFromAppian);
		return (R) responseFromAppian;
	}

	/**
	 * appian rule for validating built year
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return R
	 */
	public R validateYearBuilt(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in validateYearBuilt method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.validateYearBuilt");

		// null check and getting applicantInterest and multipleOccupancy
		ObjectMapper mapper = new ObjectMapper();
		debug("reached inside 6 var rule" + requestData.get("data"));

		String str = toJson(requestData.get("data").get(0));

		Map<String, String> map = new HashMap<String, String>();
		String year_built = "";
		String have_all_of_the_following_been = "";
		String electrical = "";
		String plumbing = "";
		String roof = "";
		String furnace = "";
		try {
			map = mapper.readValue(str, HashMap.class);
			debug("map" + String.valueOf(map.get("BuildingInput.YearBuilt")));
			year_built = String.valueOf(map.get("BuildingInput.YearBuilt"));
			have_all_of_the_following_been = String.valueOf(map.get("Have_all_of_the_following_been"));
			electrical = String.valueOf(map.get("Electrical"));
			plumbing = String.valueOf(map.get("Plumbing"));
			roof = String.valueOf(map.get("Roof"));
			furnace = String.valueOf(map.get("Furnace"));

		} catch (IOException e) {

			debug("error caught in brValidateApplicantIntandDependents  " + e);
			throw new DSCIQuoteException(e);
		}

		saveURL = saveURL + "?var1=" + year_built + "&var2=" + have_all_of_the_following_been + "&var3=" + electrical
				+ "&var4=" + plumbing + "&var5=" + roof + "&var6=" + furnace;

		debug("final url" + saveURL);

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();
		ResponseEntity<String> response = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity, String.class);

		debug("Response:in validateYearBuilt method:" + response);

		return (R) response;

	}

	/**
	 * appian rule for mixed construction (first drop down)
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return R
	 */
	public R brconstrtypebasedmixedconstr(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in brconstrtypebasedmixedconstr method:" + requestData);
		String brconstrtypebasedmixedconstrURL = CreateElementService.getEnvironment()
				.getProperty("url.brconstrtypebasedmixedconstr");
		brconstrtypebasedmixedconstrURL = brconstrtypebasedmixedconstrURL + "?var1="
				+ requestData.get("data").get(0).toString();

		URI saveURI = DSCIService.toURI(brconstrtypebasedmixedconstrURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> response = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity, String.class);

		debug("Response:in brconstrtypebasedmixedconstr method:" + response);

		return (R) response;
	}

	/**
	 * 
	 * appian rule for mixed construction (second drop down)
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return R
	 */
	public R brvalidatemixedconstruction(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in brvalidatemixedconstruction method:" + requestData);
		String brvalidatemixedconstructionURL = CreateElementService.getEnvironment()
				.getProperty("url.brvalidatemixedconstruction");
		brvalidatemixedconstructionURL = brvalidatemixedconstructionURL + "?var1="
				+ requestData.get("data").get(0).toString();

		URI saveURI = DSCIService.toURI(brvalidatemixedconstructionURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> response = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity, String.class);

		debug("Response:in brvalidatemixedconstruction method:" + response);

		return (R) response;
	}

	/**
	 * 
	 * appian rule for permanently installed equipment
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return R
	 */
	public R permanentlyinstalledequipmentlmt(RestTemplate restTemplate,
			LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in permanentlyinstalledequipmentlmt method:" + requestData);
		String permanentlyinstalledequipmentlmtURL = CreateElementService.getEnvironment()
				.getProperty("url.permanentlyinstalledequipmentlmt");
		permanentlyinstalledequipmentlmtURL = permanentlyinstalledequipmentlmtURL + "?var1="
				+ requestData.get("data").get(0).toString();

		URI saveURI = DSCIService.toURI(permanentlyinstalledequipmentlmtURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> response = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity, String.class);

		debug("Response:in permanentlyinstalledequipmentlmt method:" + response);

		return (R) response;
	}

	/**
	 * appian rule for validating annual sales
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return R
	 */
	public R brvalidateannualsales(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in validate annual sales method:" + requestData);
		String validateanlsalesURL = CreateElementService.getEnvironment().getProperty("url.brvalidateannualsales");

		String data = requestData.containsKey("data") ? requestData.get("data").get(0).toString() : "";
		validateanlsalesURL = validateanlsalesURL + "?var1=" + data;

		URI saveURI = DSCIService.toURI(validateanlsalesURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> responseFromAppian = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity,
				String.class);

		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> response;
		List<Map<String, String>> finalList = Arrays.asList();
		String res = responseFromAppian.getBody();
		try {
			finalList = mapper.readValue(res, List.class);

			for (int i = 0; i < finalList.size(); i++) {
				if (finalList.get(i).get("sysRefId").equals("ANNUAL_SALES")) {
					finalList.get(i).put("sysRefId", "misc.TotalAnnualSales");
				}
			}
			System.out.println("finalList---------" + finalList);

		} catch (IOException e) {
			e.printStackTrace();
		}

		response = new ResponseEntity<String>(toJson(finalList), HttpStatus.OK);

		debug("Response:in validateYearStarted method:" + response);
		return (R) response;
	}

	/**
	 * 
	 * appian rule for business status
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return R
	 */
	public R viewQuestionBusinessStatus(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in viewQuestionBusinessStatus method:" + requestData);
		String CurrentYearUrl = CreateElementService.getEnvironment().getProperty("url.CurrentYearUrl");
		String year = requestData.containsKey("YEAR") ? requestData.get("YEAR").get(0).toString() : "";

		CurrentYearUrl = CurrentYearUrl + "?var1=" + year;

		URI saveURI = DSCIService.toURI(CurrentYearUrl);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> response = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity, String.class);

		debug("Response:in viewQuestionBusinessStatus method:" + response);
		return (R) response;
	}

	// Additional Eligibility Questions _ Professional Services
	/**
	 * appian rule for no rule
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return R
	 */
	public R brValidateEligibilityQn2(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in brValidateEligibilityNoQn method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.brValidateEligibilityQn2");

		String brValidateEligibilityQn = requestData.containsKey("data") ? requestData.get("data").get(0).toString()
				: "";

		saveURL = saveURL + "?var1=" + brValidateEligibilityQn;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> responseFromAppian = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity,
				String.class);

		debug("Response:in brValidateEligibilityNoQn method:" + responseFromAppian);
		return (R) responseFromAppian;
	}

	// Additional Eligibility Questions _ Professional Services
	/**
	 * @param restTemplate
	 * @param requestData
	 * @return R
	 */
	public R brValidateEligibilityQn3(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in brValidateEligibilityNotAppQn method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("brValidateEligibilityQn3");

		String brValidateEligibilityQn = requestData.containsKey("data") ? requestData.get("data").get(0).toString()
				: "";

		saveURL = saveURL + "?var1=" + brValidateEligibilityQn;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> responseFromAppian = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity,
				String.class);

		debug("Response:in brValidateEligibilityNotAppQn method:" + responseFromAppian);
		return (R) responseFromAppian;
	}

	/**
	 * @param restTemplate
	 * @param requestData
	 * @return R
	 */
	public R brValidateEligibilityQn4(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in brValidateEligibilityQ4 method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.brValidateEligibilityQn4");

		String brValidateEligibilityQ4 = requestData.containsKey("data") ? requestData.get("data").get(0).toString()
				: "";

		saveURL = saveURL + "?var1=" + brValidateEligibilityQ4;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> responseFromAppian = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity,
				String.class);

		debug("Response:in brValidateEligibilityQ4 method:" + responseFromAppian);
		return (R) responseFromAppian;
	}

	/**
	 * @param restTemplate
	 * @param requestData
	 * @return R
	 */
	public R brValidateEligibilityQn5(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in brValidateEligibilityQ5 method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.brValidateEligibilityQn5");

		String brValidateEligibilityQ5 = requestData.containsKey("data") ? requestData.get("data").get(0).toString()
				: "";

		saveURL = saveURL + "?var1=" + brValidateEligibilityQ5;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();
		ResponseEntity<String> responseFromAppian = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity,
				String.class);

		debug("Response:in brValidateEligibilityQ5 method:" + responseFromAppian);
		return (R) responseFromAppian;
	}

	/**
	 * @param restTemplate
	 * @param requestData
	 * @return R
	 */
	public R brValidateEligibilityQn6(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in brValidateEligibilityQ6 method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.brValidateEligibilityQn6");

		String brValidateEligibilityQ6 = requestData.containsKey("data") ? requestData.get("data").get(0).toString()
				: "";

		saveURL = saveURL + "?var1=" + brValidateEligibilityQ6;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> responseFromAppian = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity,
				String.class);

		debug("Response:in brValidateEligibilityQ6 method:" + responseFromAppian);
		return (R) responseFromAppian;
	}

	/**
	 * appian rule for year started
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return R
	 */
	public R validateYearStarted(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in validateYearStarted method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.validateYearStarted");

		String yearStarted = requestData.containsKey("year") ? requestData.get("year").get(0).toString() : "";

		saveURL = saveURL + "?var1=" + yearStarted;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> responseFromAppian = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity,
				String.class);

		System.out.println("responseFromAppian---" + responseFromAppian);

		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> response;
		List<Map<String, String>> finalList = Arrays.asList();
		String res = responseFromAppian.getBody();
		try {
			finalList = mapper.readValue(res, List.class);

			for (int i = 0; i < finalList.size(); i++) {

				if (finalList.get(i).get("sysRefId").equals("YEAR_STARTED")) {
					finalList.get(i).put("sysRefId", "misc.YearBusinessStarted");
				} else if (finalList.get(i).get("sysRefId").equals("BUSINESS_STATUS")) {
					finalList.get(i).put("sysRefId", "misc.UNIG_BusinessStatus");
				} else if (finalList.get(i).get("sysRefId").equals("Years_In_Business")) {
					finalList.get(i).put("sysRefId", "misc.YearsInBusiness");
				} else if (finalList.get(i).get("sysRefId").equals("Prior_Experience")) {
					finalList.get(i).put("sysRefId", "misc.UNIG_PriorExperience");
				} else if (finalList.get(i).get("sysRefId").equals("Details_Explanation")) {
					finalList.get(i).put("sysRefId", "Details_Explanation");
				}
			}
			System.out.println("finalList---------" + finalList);

		} catch (IOException e) {
			e.printStackTrace();
		}

		response = new ResponseEntity<String>(toJson(finalList), HttpStatus.OK);
		debug("Response:in validateYearStarted method:" + response);
		return (R) response;
	}

	/**
	 * @param restTemplate
	 * @param requestData
	 * @return R
	 */
	public R brValidateEligibilityQn8(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in brValidateEligibilityQ8 method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.brValidateEligibilityQn8");

		String brValidateEligibilityQ8 = requestData.containsKey("data") ? requestData.get("data").get(0).toString()
				: "";

		saveURL = saveURL + "?var1=" + brValidateEligibilityQ8;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> responseFromAppian = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity,
				String.class);

		debug("Response:in brValidateEligibilityQ8 method:" + responseFromAppian);

		return (R) responseFromAppian;
	}

	// TO validate states on LOB page

	/**
	 * @param restTemplate
	 * @param requestData
	 * @return R
	 */
	public R brValidateRiskStates(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in brValidateRiskStates method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.brValidateRiskStates");

		String brValidateRiskStates = requestData.containsKey("data") ? requestData.get("data").get(0).toString() : "";

		saveURL = saveURL + "?var1=" + brValidateRiskStates;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> responseFromAppian = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity,
				String.class);

		debug("Response:in brValidateRiskStates method:" + responseFromAppian);

		return (R) responseFromAppian;
	}

	/**
	 * @param restTemplate
	 * @param requestData
	 * @return R
	 */
	public R brValidateBuildingDetails(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in brValidateBuildingDetails method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.brValidateBuildingDetails");

		String brValidateBuildingDetails = requestData.containsKey("data") ? requestData.get("data").get(0).toString()
				: "";

		saveURL = saveURL + "?var1=" + brValidateBuildingDetails;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();
		ResponseEntity<String> responseFromAppian = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity,
				String.class);

		debug("Response:in brValidateRiskStates method:" + responseFromAppian);

		return (R) responseFromAppian;
	}

	/**
	 * @param restTemplate
	 * @param requestData
	 * @return R
	 */
	public R brCalaulateBuildingVacancy(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in brCalaulateBuildingVacancy method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.brCalaulateBuildingVacancy");

		String brCalaulateBuildingVacancy = requestData.containsKey("data") ? requestData.get("data").get(0).toString()
				: "";

		saveURL = saveURL + "?var1=" + brCalaulateBuildingVacancy;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();
		ResponseEntity<String> responseFromAppian = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity,
				String.class);

		debug("Response:in brValidateRiskStates method:" + responseFromAppian);

		return (R) responseFromAppian;
	}

	/**
	 * @param restTemplate
	 * @param requestData
	 * @return R
	 */
	public R brValidatebuildingDetailsSuwqn(RestTemplate restTemplate,
			LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in brValidatebuildingDetailsSuwqn method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.brValidatebuildingDetailsSuwqn");

		String brValidatebuildingDetailsSuwqn = requestData.containsKey("data")
				? requestData.get("data").get(0).toString() : "";

		saveURL = saveURL + "?var1=" + brValidatebuildingDetailsSuwqn;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> responseFromAppian = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity,
				String.class);

		debug("Response:in brValidateRiskStates method:" + responseFromAppian);

		return (R) responseFromAppian;
	}

	/**
	 * @param restTemplate
	 * @param requestData
	 * @return R
	 */
	public R brValidateBusinessPersonalPropertyLimit(RestTemplate restTemplate,
			LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in brValidateBusinessPersonalPropertyLimit method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.brValidatebuildingDetailsSuwqn");

		String brValidateBusinessPersonalPropertyLimit = requestData.containsKey("data")
				? requestData.get("data").get(0).toString() : "";

		saveURL = saveURL + "?var1=" + brValidateBusinessPersonalPropertyLimit;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> responseFromAppian = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity,
				String.class);

		debug("Response:in brValidateRiskStates method:" + responseFromAppian);

		return (R) responseFromAppian;
	}

	/**
	 * @param restTemplate
	 * @param requestData
	 * @return R
	 */
	public R brValidatebuildingLimit(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in brValidatebuildingLimit method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.brValidatebuildingLimit");

		String brValidatebuildingLimit = requestData.containsKey("data") ? requestData.get("data").get(0).toString()
				: "";

		saveURL = saveURL + "?var1=" + brValidatebuildingLimit;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> responseFromAppian = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity,
				String.class);

		debug("Response:in brValidateRiskStates method:" + responseFromAppian);

		return (R) responseFromAppian;
	}

	/**
	 * @param restTemplate
	 * @param requestData
	 * @return R
	 */
	public R brValidateNumberOfStories(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in brValidateNumberOfStories method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.brValidateNumberOfStories");

		String brValidateNumberOfStories = requestData.containsKey("data") ? requestData.get("data").get(0).toString()
				: "";

		saveURL = saveURL + "?var1=" + brValidateNumberOfStories;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> responseFromAppian = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity,
				String.class);

		debug("Response:in brValidateRiskStates method:" + responseFromAppian);

		return (R) responseFromAppian;
	}

	/**
	 * @param restTemplate
	 * @param requestData
	 * @return R
	 */
	public R brOccupancyValidateSquareFootage(RestTemplate restTemplate,
			LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in brOccupancyValidateSquareFootage method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.brOccupancyValidateSquareFootage");

		String brOccupancyValidateSquareFootage = requestData.containsKey("data")
				? requestData.get("data").get(0).toString() : "";

		saveURL = saveURL + "?var1=" + brOccupancyValidateSquareFootage;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> responseFromAppian = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity,
				String.class);

		debug("Response:in brValidateRiskStates method:" + responseFromAppian);

		return (R) responseFromAppian;
	}

	/**
	 * @param restTemplate
	 * @param requestData
	 * @return R
	 */
	public R brValidateAnnualRentalIncome(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in brValidateAnnualRentalIncome method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.brValidateAnnualRentalIncome");

		// null check and getting applicantInterest and multipleOccupancy

		String rent = requestData.containsKey("data") ? requestData.get("data").get(0).toString() : "";
		saveURL = saveURL + "?var1=" + rent;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> response = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity, String.class);

		debug("Response:in brValidateAnnualRentalIncome method:" + response);

		return (R) response;
	}

	/**
	 * @param restTemplate
	 * @param requestData
	 * @return R
	 * @throws JsonProcessingException
	 */
	public R brValidateApplicantIntandDependents(RestTemplate restTemplate,
			LinkedMultiValueMap<String, Object> requestData) throws DSCIQuoteException {
		debug("Request:in brValidateAnnualRentalIncome method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.brvalidateapplicantintanddependents");

		// null check and getting applicantInterest and multipleOccupancy

		ObjectMapper mapper = new ObjectMapper();
		debug("reached inside 6 var rule" + requestData.get("data"));

		String str = toJson(requestData.get("data").get(0));

		Map<String, String> map = new HashMap<String, String>();
		String are_there_multiple_occupants_in_building = "";
		String applicant_interest_in_building = "";
		String square_footage = "";
		String building_limit = "";
		String occupancy_square_footage = "";
		String business_personal_property_limit = "";
		String number_of_occupants = "";
		try {
			map = mapper.readValue(str, HashMap.class);
			debug("map" + map.get("BuildingInput.UNIG_MultipleOccupancyBldg"));
			are_there_multiple_occupants_in_building = map.get("BuildingInput.UNIG_MultipleOccupancyBldg") != null ? String.valueOf(map.get("BuildingInput.UNIG_MultipleOccupancyBldg")) : "";
			applicant_interest_in_building = map.get("BuildingInput.UNIG_ApplicantInterest") != null ? String.valueOf(map.get("BuildingInput.UNIG_ApplicantInterest")) : "";
			square_footage = map.get("BuildingInput.UNIG_TotalSquareFeet") != null ? String.valueOf(map.get("BuildingInput.UNIG_TotalSquareFeet")) : "";
			building_limit = map.get("CovBuildingInput.Limit") != null ? String.valueOf(map.get("CovBuildingInput.Limit")) : "";
			occupancy_square_footage = map.get("OccupancyOutput.BOP_SquareFootage") != null ? String.valueOf(map.get("OccupancyOutput.BOP_SquareFootage")) : "";
			business_personal_property_limit = map.get("CovPersonalPropertyInput.Limit") != null ? String.valueOf(map.get("CovPersonalPropertyInput.Limit")) : "";
			number_of_occupants =  map.get("no_of_occupant") != null ?  String.valueOf(map.get("no_of_occupant")) : "";
		} catch (IOException e) {
			
			debug("error caught in brValidateApplicantIntandDependents  "+e);
			throw new DSCIQuoteException(e);
		}

		saveURL = saveURL + "?var1=" + applicant_interest_in_building + "&var2=" + square_footage + "&var3="
				+ occupancy_square_footage + "&var4=" + are_there_multiple_occupants_in_building + "&var5="
				+ building_limit + "&var6=" + business_personal_property_limit + "&var7=" +number_of_occupants;
		debug("final url" + saveURL);

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();
		ResponseEntity<String> response = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity, String.class);
		
		debug("Response:in brValidateAnnualRentalIncome method:" + response);

		return (R) response;
	}

	@SuppressWarnings("rawtypes")
	private R zipCodeLooUp(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws DSCIQuoteException {
		debug("Request:in validateYearBuilt method:" + requestData);
		String businessDetailsUrl = CreateElementService.getEnvironment().getProperty("url.businessDetails");
		String zipCode = requestData.containsKey("data") ? (String) requestData.get("data").get(0) : "";
		String section = requestData.containsKey("section") ? (String) requestData.get("section").get(0) : "";
		String url = CreateElementService.getEnvironment().getProperty("url.getQuestion");
		url += "?quote_section=" + section + "&policy_id=9876543";
		URI uri = DSCIService.toURI(url);

		LinkedMultiValueMap<String, Object> request = new LinkedMultiValueMap<>();
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		request.add("data", zipCode);
		request.add("operationType", "ZIPCODELOOKUP");
		request = DSCIUtility.updateRequestDataWithJWTToken(request);
		HttpEntity entity = getSaveHttpEntity(request);
		ResponseEntity<String> response = restTemplate.postForEntity(businessDetailsUrl, request, String.class);
		String separatedResponse[] = null;
		if (response.getBody().isEmpty()) {
			debug("Error: response is empty in viewBusinessDetails method.");
			String message = CreateElementService.getEnvironment().getProperty("response.null");
			throw new DSCIQuoteException(message);
		} else {
			separatedResponse = response.getBody().split("--MidPayload--");
		}

		debug("Response:in validateYearBuilt method:" + response);

		return (R) new ResponseEntity<>(separatedResponse[0], HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	private R zipCodeLooUp1(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws DSCIQuoteException {
		debug("Request:in zipCodeLooUp method:" + requestData);
		String zipCode = requestData.containsKey("data") ? (String) requestData.get("data").get(0) : "";
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		String policyId = (String) session.getAttribute("Id");

		LinkedMultiValueMap<String, Object> request = new LinkedMultiValueMap<>();
		request.add("data", zipCode);
		request.add("operationType", "ZIPCODELOOKUP");
		request.add("policyId", policyId);
		if (!CollectionUtils.isEmpty(requestData.get("additionalInterestId"))) {
			request.add("additionalInterestId", requestData.containsKey("additionalInterestId")
					? (String) requestData.get("additionalInterestId").get(0) : null);
		}
		if (!CollectionUtils.isEmpty(requestData.get("locationBuildingReferenceId"))) {
			request.add("locationBuildingReferenceId", requestData.containsKey("locationBuildingReferenceId")
					? (String) requestData.get("locationBuildingReferenceId").get(0) : null);
		}
		if (!CollectionUtils.isEmpty(requestData.get("assignedLocation"))) {
			request.add("assignedLocation", requestData.get("assignedLocation").get(0));
		}
		request.add("additionalInterestType",
				requestData.containsKey("type") ? (String) requestData.get("type").get(0) : null);
		request = DSCIUtility.updateRequestDataWithJWTToken(request);
		HttpEntity entity = getSaveHttpEntity(request);
		ResponseEntity<String> response = restTemplate.exchange(
				CreateElementService.getEnvironment().getProperty("url.additionalInterest"), HttpMethod.POST, entity,
				String.class);
		String separatedResponse[] = null;
		if (response.getBody().isEmpty()) {
			debug("Error: response is empty in viewBusinessDetails method.");
			String message = CreateElementService.getEnvironment().getProperty("response.null");
			throw new DSCIQuoteException(message);
		} else {
			separatedResponse = response.getBody().split("--MidPayload--");
		}

		debug("Response:in zipCodeLooUp method:" + response);
		return (R) new ResponseEntity<>(separatedResponse[0], HttpStatus.OK);
	}

	public R validatePolicyPremium(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in brValidateAnnualRentalIncome method:" + requestData);

		String saveURL = CreateElementService.getEnvironment().getProperty("url.brVAlidatePolicyPremium");
		String response = requestData.containsKey("policyPremiumData")
				? requestData.get("policyPremiumData").get(0).toString() : "";
		saveURL = saveURL + "?var1=" + response;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> responseFromAppian = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity,
				String.class);

		return (R) responseFromAppian;
	}

	/**
	 * @param restTemplate
	 * @param requestData
	 * @return
	 * @throws DSCIQuoteException
	 */
	public R validateProtectionClass(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws DSCIQuoteException {
		debug("Request:in validateProtectionClass method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.brterritorystatebasedprotectclas");

		ObjectMapper mapper = new ObjectMapper();
		debug("reached inside protection class based on territory and state rule" + requestData.get("data"));

		String str = toJson(requestData.get("data").get(0));

		Map<String, String> map = new HashMap<String, String>();
		String territory = "";
		String state = "";
		try {
			map = mapper.readValue(str, HashMap.class);
			debug("map" + map.get("Territory"));
			territory = map.get("Territory");
			state = map.get("state");

		} catch (IOException e) {

			debug("error caught in validateProtectionClass  " + e);
			throw new DSCIQuoteException(e);
		}

		saveURL = saveURL + "?var1=" + territory + "&var2=" + state;
		debug("final url" + saveURL);

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();
		ResponseEntity<String> response = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity, String.class);

		debug("Response:in validateProtectionClass method:" + response);

		return (R) response;
	}

	public R brValidateState(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws DSCIQuoteException {
		long startTime = System.currentTimeMillis();
		debug("Request:in brValidateState method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.validateState");

		ObjectMapper mapper = new ObjectMapper();
		debug("reached inside rule for state validation" + requestData.get("data"));

		String str = toJson(requestData.get("data").get(0));

		Map<String, String> map = new HashMap<String, String>();
		String state = "";
		String primaryRiskState = "";
		try {
			map = mapper.readValue(str, HashMap.class);
			debug("map" + map.get("lobstate"));
			state = map.get("lobstate");
			primaryRiskState = map.get("locstate");

		} catch (IOException e) {

			debug("error caught in brValidateState  " + e);
			throw new DSCIQuoteException(e);
		}

		saveURL = saveURL + "?var1=" + state + "&var2=" + primaryRiskState;
		debug("final url" + saveURL);

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();
		ResponseEntity<String> response = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity, String.class);

		debug("Response:in brValidateState method:" + response + " executionTime:" + (System.currentTimeMillis() - startTime));

		return (R) response;
	}

	// For Pricing And Coverages
	public R brValidateCoverage1R1(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in brValidateCoverage1R1 method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.brValidateCoverage1R1");

		String var = requestData.containsKey("data") ? requestData.get("data").get(0).toString() : "";

		saveURL = saveURL + "?var1=" + var;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> response = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity, String.class);

		debug("Response:in brValidateCoverage1R1 method:" + response);

		return (R) response;
	}

	public R brValidateCoverage1R2(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in brValidateCoverage1R2 method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.brValidateCoverage1R2");

		String var1 = requestData.containsKey("data1") ? requestData.get("data1").get(0).toString() : "";
		String var2 = requestData.containsKey("data2") ? requestData.get("data2").get(0).toString() : "";
		String var3 = requestData.containsKey("data3") ? requestData.get("data3").get(0).toString() : "";
		String var4 = requestData.containsKey("data4") ? requestData.get("data4").get(0).toString() : "";

		saveURL = saveURL + "?var1=" + var1 + "&var2=" + var2 + "&var3=" + var3 + "&var4=" + var4;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> response = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity, String.class);

		debug("Response:in brValidateCoverage1R2 method:" + response);

		return (R) response;
	}

	public R brValidateCoverage2R1(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in brValidateCoverage2R1 method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.brValidateCoverage2R1");

		String var = requestData.containsKey("data") ? requestData.get("data").get(0).toString() : "";

		saveURL = saveURL + "?var1=" + var;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> response = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity, String.class);

		debug("Response:in brValidateCoverage2R1 method:" + response);

		return (R) response;
	}

	public R brValidateCoverage3R1(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in brValidateCoverage3R1 method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.brValidateCoverage3R1");

		String var = requestData.containsKey("data") ? requestData.get("data").get(0).toString() : "";

		saveURL = saveURL + "?var1=" + var;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> response = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity, String.class);

		debug("Response:in brValidateCoverage3R1 method:" + response);

		return (R) response;
	}

	public R brValidateCoverage4R1(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in brValidateCoverage4R1 method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.brValidateCoverage4R1");

		String var = requestData.containsKey("data") ? requestData.get("data").get(0).toString() : "";

		saveURL = saveURL + "?var1=" + var;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> response = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity, String.class);

		debug("Response:in brValidateCoverage4R1 method:" + response);

		return (R) response;
	}

	public R brValidateCoverage4R2(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in brValidateCoverage4R2 method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.brValidateCoverage4R2");

		String var1 = requestData.containsKey("data1") ? requestData.get("data1").get(0).toString() : "";
		String var2 = requestData.containsKey("data2") ? requestData.get("data2").get(0).toString() : "";

		saveURL = saveURL + "?var1=" + var1 + "&var2=" + var2;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> response = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity, String.class);

		debug("Response:in brValidateCoverage4R2 method:" + response);

		return (R) response;
	}

	public R brValidateCoverage6R1(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in brValidateCoverage6R1 method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.brValidateCoverage6R1");

		String var = requestData.containsKey("data") ? requestData.get("data").get(0).toString() : "";

		saveURL = saveURL + "?var1=" + var;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> response = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity, String.class);

		debug("Response:in brValidateCoverage6R1 method:" + response);

		return (R) response;
	}

	public R brValidateCoverage9R1(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in brValidateCoverage9R1 method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.brValidateCoverage9R1");

		String var = requestData.containsKey("data") ? requestData.get("data").get(0).toString() : "";

		saveURL = saveURL + "?var1=" + var;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> response = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity, String.class);

		debug("Response:in brValidateCoverage9R1 method:" + response);

		return (R) response;
	}

	public R brValidateCoverage9R2(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in brValidateCoverage9R2 method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.brValidateCoverage9R2");

		String var = requestData.containsKey("data") ? requestData.get("data").get(0).toString() : "";

		saveURL = saveURL + "?var1=" + var;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> response = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity, String.class);

		debug("Response:in brValidateCoverage9R2 method:" + response);

		return (R) response;
	}

	public R brValidateCoverage8R1(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in brValidateCoverage8R1 method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.brValidateCoverage8R1");

		String var = requestData.containsKey("data") ? requestData.get("data").get(0).toString() : "";

		saveURL = saveURL + "?var1=" + var;

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> response = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity, String.class);

		debug("Response:in brValidateCoverage8R1 method:" + response);

		return (R) response;
	}

	public R validateLOB(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		String validateURL = CreateElementService.getEnvironment().getProperty("url.validateLOB");
		List<Object> lobData = requestData.get("data");
		UriComponentsBuilder builder = UriComponentsBuilder.fromUri(DSCIService.toURI(validateURL));
		lobData.forEach(obj -> {
			Map<String, Object> lob = (Map<String, Object>) obj;
			builder.queryParam("var1", (Integer) lob.get("userValue") == 1);
		});

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();

		ResponseEntity<String> response = restTemplate.exchange(builder.buildAndExpand(new HashMap<>()).toUri(),
				HttpMethod.GET, saveEntity, String.class);

		return (R) response;
	}

	public R validateNamedInsured(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in validateNamedInsured method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.validate.byip");

		// null check and getting applicantInterest and multipleOccupancy
		ObjectMapper mapper = new ObjectMapper();

		String var1 = (requestData.get("var1").get(0)).toString();
		String var2 = (requestData.get("var2").get(0)).toString();

		saveURL = saveURL + "?var1=" + var1 + "&var2=" + var2;

		debug("final url" + saveURL);

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();
		ResponseEntity<String> response = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity, String.class);

		debug("Response:in validateNamedInsured method:" + response);

		return (R) response;

	}

	/**
	 * To display the permanently installed equipment question which should come
	 * only for 22 business classes
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return
	 */
	/*public R brdisplaypermanentlyinstldequipment(RestTemplate restTemplate,
			LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in brdisplaypermanentlyinstldequipment method:" + requestData);
		String brdisplaypermanentlyinstldequipmentURL = CreateElementService.getEnvironment()
				.getProperty("url.brdisplaypermanentlyinstldequipment");
		brdisplaypermanentlyinstldequipmentURL += "?var1=" + requestData.get("data").get(0).toString();

		URI saveURI = DSCIService.toURI(brdisplaypermanentlyinstldequipmentURL);

		// creating HttpEntity
		HttpEntity<String> getEntity = getViewHttpEntity();

		ResponseEntity<String> response = restTemplate.exchange(saveURI, HttpMethod.GET, getEntity, String.class);

		debug("Response:in brdisplaypermanentlyinstldequipment method:" + response);

		return (R) response;
	}*/
	
	/**To display the permanently installed equipment question which should come only for 22 business classes
	 * and, to display windstorm protective devices which should appear only for NY state
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return R
	 * @throws JsonProcessingException
	 */
	public R brdisplaypermanentlyinstldequipment(RestTemplate restTemplate,
			LinkedMultiValueMap<String, Object> requestData) throws DSCIQuoteException {
		long startTime = System.currentTimeMillis();
		debug("Request:in brdisplaypermanentlyinstldequipment method:" + requestData);
		String saveURL = CreateElementService.getEnvironment()
				.getProperty("url.brdisplaypermanentlyinstldequipment");

		ObjectMapper mapper = new ObjectMapper();

		String str = toJson(requestData.get("data").get(0));

		Map<String, String> map = new HashMap<String, String>();
		String opCode = "";
		String state = "";
		try {
			map = mapper.readValue(str, HashMap.class);
			debug("map" + map.get("opCode"));
			
			//for permanently_installed_equi_limit
			opCode = map.get("opCode") != null ? String.valueOf(map.get("opCode")) : "";
			//for windstorm_protective_devices
			state = map.get("state") != null ? String.valueOf(map.get("state")) : "";
		} catch (IOException e) {
			
			debug("error caught in brdisplaypermanentlyinstldequipment  "+e);
			throw new DSCIQuoteException(e);
		}

		saveURL = saveURL + "?var1=" + opCode + "&var2=" + state;

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();
		ResponseEntity<String> response = restTemplate.exchange(DSCIService.toURI(saveURL), HttpMethod.GET, saveEntity, String.class);

		debug("Response:in brdisplaypermanentlyinstldequipment method:" + response + " executionTime:" + (System.currentTimeMillis() - startTime));

		return (R) response;
	}
	

	public R displaybusinessinformation3(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in validateNamedInsured method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.displaybusinessinformation3");

		// null check and getting applicantInterest and multipleOccupancy
		ObjectMapper mapper = new ObjectMapper();

		String var1 = requestData.containsKey("var1") && requestData.get("var1").get(0) != null
				? requestData.get("var1").get(0).toString() : "";

		saveURL = saveURL + "?var1=" + var1;

		debug("final url" + saveURL);

		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();
		System.out.println("variable to appain3------>" + var1);
		ResponseEntity<String> response = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity, String.class);
		System.out.println("responce from appain3----->" + response.getBody());
		debug("Response:in validateNamedInsured method:" + response);

		// make quote editable if it is not
		debug("Request:in saveBeforeYouIssuePolicyData method:");
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		String policyId = (String) session.getAttribute("Id");
		String bearerToken = session.getAttribute("JsonWebToken").toString();

		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();

		if (sessionData.getIsQuoteEditable() == null) {
			String url2 = CreateElementService.getEnvironment().getProperty("url.before.you.issue.policy");

			if (policyId == null) {
				throw new DSCIQuoteException("ID not found");
			}

			LinkedMultiValueMap requestData1 = new LinkedMultiValueMap<>();
			requestData1.add("key", "makeQuoteEditable");
			requestData1.add("policyId", policyId);
			requestData1.add("bearerToken", bearerToken);

			HttpEntity saveEntity2 = getSaveHttpEntity(requestData1);
			ResponseEntity<String> response2 = restTemplate.exchange(url2, HttpMethod.POST, saveEntity2, String.class);
			sessionData.setIsQuoteEditable("true");
		}

		return (R) response;

	}

	public R displaybusinessinformation2(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in validateNamedInsured method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.displaybusinessinformation2");

		// null check and getting applicantInterest and multipleOccupancy
		ObjectMapper mapper = new ObjectMapper();

		String var1 = requestData.containsKey("var1") && requestData.get("var1").get(0) != null
				? requestData.get("var1").get(0).toString() : "";

		saveURL = saveURL + "?var1=" + var1;

		debug("final url" + saveURL);

		System.out.println("variable to appain2------>" + var1);
		URI saveURI = DSCIService.toURI(saveURL);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();
		ResponseEntity<String> response = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity, String.class);
		System.out.println("responce from appain2----->" + response.getBody());
		debug("Response:in validateNamedInsured method:" + response);

		return (R) response;

	}

	public R displaybusinessinformation1(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in validateNamedInsured method:" + requestData);
		String saveURL = CreateElementService.getEnvironment().getProperty("url.displaybusinessinformation1");

		// null check and getting applicantInterest and multipleOccupancy
		ObjectMapper mapper = new ObjectMapper();

		String var1 = requestData.containsKey("var1") && requestData.get("var1").get(0) != null
				? requestData.get("var1").get(0).toString() : "";

		saveURL = saveURL + "?var1=" + var1;

		debug("final url" + saveURL);

		URI saveURI = DSCIService.toURI(saveURL);
		System.out.println("variable to appain1------>" + var1);

		// creating HttpEntity
		HttpEntity<String> saveEntity = getViewHttpEntity();
		ResponseEntity<String> response = restTemplate.exchange(saveURI, HttpMethod.GET, saveEntity, String.class);
		System.out.println("responce from appain1----->" + response.getBody());

		debug("Response:in validateNamedInsured method:" + response);

		return (R) response;

	}

}