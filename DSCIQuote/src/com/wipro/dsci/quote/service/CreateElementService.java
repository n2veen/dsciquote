package com.wipro.dsci.quote.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.wipro.dsci.quote.enums.Actions;
import com.wipro.dsci.quote.enums.Element;
import static com.wipro.dsci.quote.service.DSCIService.debug;

/**
 * @author AK328919
 *
 */
@org.springframework.stereotype.Service("CreateElementService")
@Component
@PropertySources({
    @PropertySource("classpath:application.properties"),
    @PropertySource("classpath:statusMessage.properties")
})
public class CreateElementService {


	@Autowired
	RestTemplate restTemplate;

	private static Environment environment;

	public CreateElementService() {
	}

	@Autowired
	public CreateElementService(Environment environment) {
		CreateElementService.environment = environment;
	}

	/**
	 * @param requestData
	 * @return String
	 * @throws Throwable
	 */
	public Object callCreateService(LinkedMultiValueMap<String, Object> requestData) throws Throwable {
		System.out.println("callCreateService " + requestData);
		
		debug("Request:in callCreateService method:"+requestData);
		String elementType = requestData.containsKey("elementType") ? requestData.get("elementType").get(0).toString()
				: "";
		String operationType = requestData.containsKey("operationType")
				? requestData.get("operationType").get(0).toString() : "";

		// restTemplate = new RestTemplate();

		DSCIService<?, ?> service = ServiceFactory.getService(Element.valueOf(elementType.toUpperCase()));

		if (null == service){
			debug("Response:in callCreateService method:Unsupported operation!!");
			throw new Exception("Unsupported operation!!");
		}
		debug("Response:in callCreateService method:"+service.getClass().getName());
		return (Object) service.delegate(Actions.valueOf(operationType.toUpperCase()), requestData);
	}

	

	public static Environment getEnvironment() {
		return environment;
	}

	public static void setEnvironment(Environment environment) {
		CreateElementService.environment = environment;
	}

}
