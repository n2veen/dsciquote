package com.wipro.dsci.quote.service;

import java.io.IOException;
import java.net.URI;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.function.BiFunction;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wipro.dsci.quote.enums.Actions;
import com.wipro.dsci.quote.exception.DSCIQuoteException;
import com.wipro.dsci.quote.model.SessionData;
import com.wipro.dsci.quote.util.DSCIUtility;

import static com.wipro.dsci.quote.service.DSCIService.getViewHttpEntity;
import static com.wipro.dsci.quote.service.DSCIService.debug;

@Service
public class BeforeYouBeginService<R, E> implements DSCIService<R, DSCIQuoteException> {
	
	@Autowired
	RestTemplate restTemplate;

	private Map<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>> opTypeMap = new HashMap<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>>() {

		private static final long serialVersionUID = 1L;
		{
			put(Actions.SAVEBEFOREYOUBEGINSERVICE, (a, b) -> {
				return saveBeforeYouBeginService(a, b);
			});

			put(Actions.VIEWBEFOREYOUBEGIN, (a, b) -> {
				return viewBeforeYouBeginService(a, b);
			});
		}
	};

	@Override
	public R delegate(Actions actions, LinkedMultiValueMap<String, Object> requestData) throws DSCIQuoteException {

		// null check and getting operationType

		String operationType = requestData.containsKey("operationType")
				? (String) requestData.get("operationType").get(0) : "";

		if (!opTypeMap.containsKey(Actions.valueOf(operationType.toUpperCase()))) {
			throw new IllegalArgumentException(String.format("%s is not supported", actions));
		}
		return opTypeMap.get(actions).apply(restTemplate, requestData);
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.wipro.dsci.quote.service.Service#createCall(org.springframework.web.
	 * client.RestTemplate, org.springframework.util.MultiValueMap)
	 */

	@SuppressWarnings("unchecked")
	public R createCall(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {

		debug("Request:in createCall method:" + requestData);
		// setting uri
		String url = CreateElementService.getEnvironment().getProperty("url.create");

		URI uri = DSCIService.toURI(url);

		String request = DSCIService.toJson(requestData);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> entity = new HttpEntity<String>(request, headers);
		// sending data
		// ResponseEntity<String> response = restTemplate.postForEntity(uri,
		// entity,
		// String.class);
		System.out.println("Inside createcall");
		ResponseEntity<String> response = null;
		response = restTemplate.postForEntity(uri, entity, String.class);
		System.out.println("Response:-" + response);
		if (response.getBody().isEmpty()) {
			debug("Error: response is empty in createCall method.");
			String message = CreateElementService.getEnvironment().getProperty("response.null");
			throw new DSCIQuoteException(message);
		}
		debug("Response:in createCall method:" + response);
		return (R) response;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R viewBeforeYouBeginService(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws RuntimeException {
		debug("Request:in viewBusinessDetails method:" + requestData);
		String beforeYouBeginUrl = CreateElementService.getEnvironment().getProperty("url.beforeYouBegin");
		System.out.println("Enter: viewBusinessDetails method of BusinessClassService in DSCIQuoteto");

		String url = CreateElementService.getEnvironment().getProperty("url.getQuestion");
		String QuoteSection = "1";

		url += "?quote_section=" + QuoteSection + "&policy_id=1";

		// converting URL to URI
		URI uri = DSCIService.toURI(url);

		HttpSession session = (HttpSession) requestData.get("session").get(0);
		String agentID = (String) session.getAttribute("agentID");
		// preparing header for rest call

		HttpEntity entity = getViewHttpEntity();
		debug("Connecting: viewBusinessDetails : to APPIAN with entity: " + entity);
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

		HashMap<String, Object> requestMap = new HashMap<>();
		requestMap.put("operationType", "VIEW");
		requestMap.put("agentID",agentID);
		requestMap = DSCIUtility.updateRequestMapDataWithJWTToken(requestMap);

		// rest call
		ResponseEntity<String> response = restTemplate.postForEntity(beforeYouBeginUrl, requestMap, String.class);

		// ResponseEntity<String> response
		// =restTemplate.postForEntity("http://localhost:9066/beforeyoubegin/",null,String.class);
		String[] separatedResponse = null;

		System.out.println(
				"Exit: viewBusinessDetails method of BusinessClassService in DSCIQuote with reponse: " + response);
		if (response.getBody().isEmpty()) {
			debug("Error: response is empty in viewBusinessDetails method.");
			String message = CreateElementService.getEnvironment().getProperty("response.null");
			throw new DSCIQuoteException(message);
		} else {
			separatedResponse = response.getBody().split("##SessionData##");
		}

		ResponseEntity<String> responseString = null;

		String[] separatedSessionAndTransactionalResponse = separatedResponse[0].split("--MidPayload--");

		System.out.println("sending master data");
		ObjectMapper mapper = new ObjectMapper();
		HashMap<String, Object> responseMap = null;
		try {
			responseMap = mapper.readValue(separatedSessionAndTransactionalResponse[0], HashMap.class);
			((HttpSession) requestData.get("session").get(0)).setAttribute("clientId", responseMap.get("id"));
			responseMap.remove("id");
			responseString = new ResponseEntity<String>(mapper.writeValueAsString(responseMap), HttpStatus.OK);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		debug("Response:in viewBusinessDetails method:" + responseString);
		return (R) responseString;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public R saveBeforeYouBeginService(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {

		debug("Request:in saveBusinessDetailsInAppian method:" + requestData);
		String beforeYouBeginUrl = CreateElementService.getEnvironment().getProperty("url.beforeYouBegin");
		// setting URL
		String saveURL = CreateElementService.getEnvironment().getProperty("url.saveQuoteDetails");

		String policyId = generateId().toString();
		String QuoteSection = "1";
		saveURL += "?&policy_id=" + policyId + "&quote_section=" + QuoteSection + "&action=SC";

		HttpSession session = (HttpSession) requestData.get("session").get(0);

		session.setAttribute("Id", policyId);
		// converting URL to URI
		URI uri = DSCIService.toURI(saveURL);

		// null check and getting data for saving
		Object data = requestData.containsKey("data") ? requestData.get("data").get(0) : "";

		ObjectMapper mapper = new ObjectMapper();
		HashMap<String, Object> requestMap = null;
		try {
			requestMap = mapper.readValue(mapper.writeValueAsString(data), HashMap.class);
		} catch (Exception exception) {
			throw new DSCIQuoteException(exception.getMessage());
		}

		requestMap.put("operationType", "SAVE");

		requestMap = DSCIUtility.updateRequestMapDataWithJWTToken(requestMap);
		ResponseEntity<String> response = restTemplate.postForEntity(beforeYouBeginUrl, requestMap, String.class);
		if (response.getBody().isEmpty()) {
			debug("Error: response is empty in saveBusinessDetailsInAppian method.");
			String message = CreateElementService.getEnvironment().getProperty("response.null");
			throw new DSCIQuoteException(message);
		}
		((HttpSession) requestData.get("session").get(0)).setAttribute("BusinessOwners", false);
		List<Map<String, String>> agentInfo = (List<Map<String, String>>) ((HashMap) requestData.get("data").get(0))
				.get("agent_Info");
		Boolean businessOwnersPolicy = false;
		Map<String, Object> beforeYouBeginData = new HashMap<String, Object>();
		if (agentInfo != null) {
			for (Map<String, String> agentData : agentInfo) {
				if (agentData.get("systemRefId").equals("Do_you_intend_to_quote_a_Busine")
						&& agentData.get("userValue").equals("Yes")) {
					// Agent quoting business owners policy
					((HttpSession) requestData.get("session").get(0)).setAttribute("BusinessOwners", true);
					businessOwnersPolicy = true;
				}
				
				if (agentData.get("systemRefId").equals("NewManuScript.HowShouldWeContactYouAcct")) {
					beforeYouBeginData.put("howShouldWeContactYou", agentData.get("userValue"));
				}
				
				if (agentData.get("systemRefId").equals("NewManuScript.AgencyContactEmailAcct")) {
					beforeYouBeginData.put("contactEmail", agentData.get("userValue"));
				}
				
				if (agentData.get("systemRefId").equals("NewManuScript.AgencyContactPhoneNumberAcct")) {
					beforeYouBeginData.put("contactPhoneNumber", agentData.get("userValue"));
				}
				
				if (agentData.get("systemRefId").equals("NewManuScript.AgencyContactNameAcct")) {
					beforeYouBeginData.put("contactName", agentData.get("userValue"));
				}
			}
		}
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();
		List<Map<String, String>> insuredInfo = (List<Map<String, String>>) ((HashMap) requestData.get("data").get(0))
				.get("insured_Info");
		if (insuredInfo != null) {
			for (Map<String, String> insuredData : insuredInfo) {
				if (insuredData.get("systemRefId").equals("misc.PrimaryRiskState")
						&& !insuredData.get("userValue").equals("")) {
					beforeYouBeginData.put("state", insuredData.get("userValue"));
					sessionData.setState(insuredData.get("userValue"));
				}
				if (insuredData.get("systemRefId").equals("misc.EffectiveDate")
						&& !insuredData.get("userValue").equals("")) {
					DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy", Locale.ENGLISH);
					DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);
					LocalDate date = LocalDate.parse(insuredData.get("userValue"), inputFormatter);
					String formattedDate = outputFormatter.format(date);
					beforeYouBeginData.put("effectiveDate", formattedDate);
				}
			}
			beforeYouBeginData.put("businessOwnersPolicy", businessOwnersPolicy);
		}
		sessionData.setBeforeYouBeginData(beforeYouBeginData);
		session.setAttribute("sessionData", sessionData);
		debug("Response:in saveBusinessDetailsInAppian method:" + response);
		return (R) response;
	}

	public RestTemplate getTemplate() {
		return restTemplate;
	}

	public UUID generateId() {
		return UUID.randomUUID();
	}
}
