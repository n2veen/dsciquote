package com.wipro.dsci.quote.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wipro.dsci.quote.enums.Actions;
import com.wipro.dsci.quote.exception.DSCIQuoteException;
import com.wipro.dsci.quote.model.SessionData;
import com.wipro.dsci.quote.util.DSCIUtility;

import static com.wipro.dsci.quote.service.DSCIService.getSaveHttpEntity;
import static com.wipro.dsci.quote.service.DSCIService.toJson;
import static com.wipro.dsci.quote.util.DSCIUtility.updateRequestDataWithJWTToken;
import static com.wipro.dsci.quote.util.DSCIUtility.updateRequestMapDataWithJWTToken;
import static com.wipro.dsci.quote.util.DSCIUtility.updateRequestStringMapDataWithJWTToken;

@Service
public class PricingAndCoverageService<R, E> implements DSCIService<R, DSCIQuoteException> {
	
	@Autowired
	RestTemplate restTemplate;
	
	private static void debug(String format, Object... args) {
		Logger logger = LoggerFactory.getLogger("DSCIQuote_dblogger");
		if (logger.isDebugEnabled()) {
			logger.debug(format, args);
		}
	}


	private Map<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>> opTypeMap = new HashMap<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>>() {

		private static final long serialVersionUID = 1L;
		{
			put(Actions.GETCOVERAGES, (a, b) -> {
				return getCoverages(a, b);
			});
			put(Actions.GETCOMPONENTS, (a, b) -> {
				return getComponents(a, b);
			});
			put(Actions.GETOPTIONALS, (a, b) -> {
				return getOptionals(a, b);
			});
			put(Actions.FETCHUWQUESTIONS, (a, b) -> {
				return fetchUWQuestions(a, b);
			});
			put(Actions.SAVECOVERAGE, (a, b) -> {
				return saveCoverages(a, b);
			});
			put(Actions.SAVECOMPONENTS, (a, b) -> {
				return saveComponents(a, b);
			});
			put(Actions.CHILDUWQUESTIONS, (a, b) -> {
				return childUWQuestions(a, b);
			});
			put(Actions.APPLYPRICE, (a, b) -> {
				return getPrice(a, b);
			});
		}
	};

	//Call method to get the token
	String bearerToken = "";
	@Override
	public R delegate(Actions actions, LinkedMultiValueMap<String, Object> requestData) throws RuntimeException {

		// null check and getting operationType
		String operationType = requestData.containsKey("operationType")
				? (String) requestData.get("operationType").get(0) : "";

		if (!opTypeMap.containsKey(Actions.valueOf(operationType.toUpperCase()))) {
			throw new IllegalArgumentException(String.format("%s is not supported", actions));
		}
		return opTypeMap.get(actions).apply(restTemplate, requestData);
	}

	public R getCoverages(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		requestData.remove("elementType");

		HttpSession session = (HttpSession) requestData.get("session").get(0);
		ObjectMapper objectMapper =new ObjectMapper();
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();

		
		  if (sessionData.getProductId() == null) { throw new
		  DSCIQuoteException("ProductID is not in session"); } else {
		 
	
		requestData.add("productId",  sessionData.getProductId());
		requestData.add("quoteId", session.getAttribute("Id") );
		requestData.remove("session");
	 }

			if(requestData.get("data")!= null){
				List<HashMap> transaction = (List) ((List) requestData.get("data")).get(0);
				int groupsLength = ((List) ((HashMap) transaction.get(1)).get("groups")).size();
				HashMap newCoverage = new HashMap();
				newCoverage.put("datasetName", session.getAttribute("coverageName"));
				newCoverage.put("coverageName", session.getAttribute("coverageName"));
				newCoverage.put("groupDisplaySequence", groupsLength);
				HashMap<String, Object> compDisplayValue = new HashMap();
				compDisplayValue.put("compDisplayValue", Arrays.asList("Included"));
				newCoverage.put("components", Arrays.asList(compDisplayValue));
				newCoverage.put("subGroups", null);
			
				int index = 0;
				for (HashMap temp : transaction) {
					if (temp.get("packageName").toString().equals("Tailored Plan")) {

						((List) ((HashMap) transaction.get(index)).get("groups")).add(newCoverage);
						// reprice here
						LinkedMultiValueMap<String, Object> requestData1 = new LinkedMultiValueMap<String, Object>();
						requestData1.add("operationType", "GETPRICE");
						ResponseEntity<String> res = (ResponseEntity<String>) getPrice(restTemplate, requestData1);
						System.out.println("body" + res.getBody());
						try {
							HashMap price = objectMapper.readValue(res.getBody(), HashMap.class);
							((HashMap) transaction.get(index)).put("message", price.get("message"));
							((HashMap) transaction.get(index)).put("price", price.get("price"));
							((HashMap) transaction.get(index)).put("quoteBindable", price.get("quoteBindable"));
							session.setAttribute("quoteBindable", price.get("quoteBindable"));
						} catch (IOException e) {
							throw new DSCIQuoteException(e);
						}
						break;
					}
					index++;
				}

				String res = "";
				try {
					res = objectMapper.writeValueAsString(transaction);
				} catch (JsonProcessingException e) {
					throw new DSCIQuoteException(e);
				}
				return (R) new ResponseEntity<>(res, HttpStatus.OK);
			}

			/*HashMap<String, String> tokens = new HashMap<>();
			tokens = handOffService.dcAuthentication("admin", "admin");
			String bearerToken = tokens.get("JsonWebToken");
			session.setAttribute("bearerToken", bearerToken);
			requestData.add("bearerToken", bearerToken);*/
			String url = ViewElementService.getEnvironment().getProperty("url.getQuestion");

			String policyId = (String) session.getAttribute("Id");

			if (policyId == null) {
				throw new DSCIQuoteException("ID not found");
			}
			url += "?quote_section=11" + "&policy_id=" + policyId;

			// debug("abccc"+policyId);
			// converting URL to URI
			URI uri = DSCIService.toURI(url);

			requestData.remove("session");
			HttpEntity saveEntity = getSaveHttpEntity(updateRequestDataWithJWTToken(requestData));
			// rest call

			ResponseEntity<String> TransactionData = restTemplate.exchange(url, HttpMethod.POST, saveEntity, String.class);
			System.out.println("TransactionData" + TransactionData);
			String[] sessionSepration = TransactionData.getBody().split("##SessionData##");
			String[] masterTransaction = sessionSepration[0].split("--MidPayload--");
			String response;
			if (masterTransaction.length > 1) {

				List<HashMap> masterData = null;
				HashMap transactionData = null;
				try {
					masterData = objectMapper.readValue(masterTransaction[0], List.class);
					transactionData = objectMapper.readValue(masterTransaction[1], HashMap.class);
					for (HashMap temp : masterData) {
						if (temp.get("packageName").toString().equals("Tailored Plan")) {

							// reprice here
							LinkedMultiValueMap<String, Object> requestData1 = new LinkedMultiValueMap<String, Object>();
							requestData1.add("operationType", "GETPRICE");
							ResponseEntity<String> res = (ResponseEntity<String>) getPrice(restTemplate, requestData1);
							System.out.println("body" + res.getBody());
							try {
								HashMap price = objectMapper.readValue(res.getBody(), HashMap.class);
								transactionData.put("message", price.get("message"));
								transactionData.put("price", price.get("price"));
								transactionData.put("quoteBindable", price.get("quoteBindable"));
								session.setAttribute("quoteBindable", price.get("quoteBindable"));
							} catch (IOException e) {
								throw new DSCIQuoteException(e);
							}
							masterData.remove(temp);
							masterData.add(transactionData);
							break;
						}
					}
					response = toJson(masterData);
					// masterData.get(1).replaceAll( (BiFunction) transactionData);
				} catch (IOException e) {
					throw new DSCIQuoteException(e);
				}

			} else {
				List<HashMap> masterData = null;
				try {
					masterData = objectMapper.readValue(masterTransaction[0], List.class);
					session.setAttribute("quoteBindable", masterData.get(0).get("quoteBindable"));
					session.setAttribute("stpDetails", masterData.get(0).get("stpDetails"));
					sessionData.setStpDetails((HashMap<String, Object>) masterData.get(0).get("stpDetails"));
					sessionData.setQuoteBindable(masterData.get(0).get("quoteBindable").toString());
					masterData.remove("stpDetails");
				} catch (IOException e) {
					throw new DSCIQuoteException(e);
				}
				response = masterTransaction[0];
			}

			return (R) new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	
	// To get Pop-Up
	
		public R getComponents(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
				throws RuntimeException {
			debug("Request:in getComponents method:" + requestData);

			 HttpSession session = (HttpSession) requestData.get("session").get(0);
			
			  SessionData sessionData = session.getAttribute("sessionData") != null
			  ? (SessionData) session.getAttribute("sessionData") : new
			  SessionData();
			  
			  if (sessionData.getProductId() == null) { throw new
			  DSCIQuoteException("ProductID is not in session"); } else {
			  requestData.add("productId", sessionData.getProductId()); }
			 
			String url = ViewElementService.getEnvironment().getProperty("url.pricingandcoverage");
			
			session.setAttribute("coverageName", requestData.get("coverageName").get(0));
			session.setAttribute("coverageId", requestData.get("coverageId").get(0));
			 String policyId =(String) session.getAttribute("Id");
			  if(policyId==null) { throw new DSCIQuoteException("ID not found"); }
			 // url += "?quote_section=11" + "&policy_id=" +policyId;
			 

			//requestData.remove("coverageId");
			//requestData.add("productId",  sessionData.getProductId() );
			requestData.add("quoteId",   session.getAttribute("Id") );
			
			//requestData.add("coverageId", 62 /* sessionData.getProductId() */);
			requestData.remove("session");
			// converting URL to URI
			URI uri = DSCIService.toURI(url);

			HttpEntity saveEntity = getSaveHttpEntity(DSCIUtility.updateRequestDataWithJWTToken(requestData));

			// rest call
			ResponseEntity<String> responseData = restTemplate.exchange(uri, HttpMethod.POST, saveEntity, String.class);

			if (responseData.getBody().isEmpty()) {
				debug("Error: response is empty in getComponents method.");
				String message = CreateElementService.getEnvironment().getProperty("response.null");
				throw new DSCIQuoteException(message);
			}

			String[] sessionSepration = responseData.getBody().split("##SessionData##");
			// Getting master, transactional and sessiondata from appian response
			String[] separatedResponse = sessionSepration[0].split("--MidPayload--");

			String response;
			if (separatedResponse.length == 1) {
				response = separatedResponse[0];
			} else {
				response = separatedResponse[1];
			}

			debug("Response:in getComponents method:" + response);
			return (R) new ResponseEntity<>(response, HttpStatus.OK);
		}

		@SuppressWarnings({ "unchecked", "rawtypes" })
		public R childUWQuestions(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
				throws RuntimeException {
			debug("Request:in getComponents method:" + requestData);

			 HttpSession session = (HttpSession) requestData.get("session").get(0);
			
			  SessionData sessionData = session.getAttribute("sessionData") != null
			  ? (SessionData) session.getAttribute("sessionData") : new
			  SessionData();
			  
			  if (sessionData.getProductId() == null) { throw new
			  DSCIQuoteException("ProductID is not in session"); } else {
			  requestData.add("productId", sessionData.getProductId()); }
			 
			String url = ViewElementService.getEnvironment().getProperty("url.pricingandcoverage");
			;
			
			  String policyId =  (String) session.getAttribute("Id");
			  if(policyId==null) { throw new DSCIQuoteException("ID not found"); }
			  url += "?quote_section=11" + "&policy_id=" +policyId;
			 

			//requestData.remove("coverageId");
			requestData.add("productId",sessionData.getProductId() );
			requestData.add("quoteId",session.getAttribute("Id"));
			//requestData.add("coverageId", 62 /* sessionData.getProductId() */);
			requestData.remove("session");
			// converting URL to URI
			URI uri = DSCIService.toURI(url);

			HttpEntity saveEntity = getSaveHttpEntity(DSCIUtility.updateRequestDataWithJWTToken(requestData));

			// rest call
			ResponseEntity<String> responseData = restTemplate.exchange(uri, HttpMethod.POST, saveEntity, String.class);

			if (responseData.getBody().isEmpty()) {
				debug("Error: response is empty in getComponents method.");
				String message = CreateElementService.getEnvironment().getProperty("response.null");
				throw new DSCIQuoteException(message);
			}

			String[] sessionSepration = responseData.getBody().split("##SessionData##");
			// Getting master, transactional and sessiondata from appian response
			String[] separatedResponse = sessionSepration[0].split("--MidPayload--");

			String response;
			if (separatedResponse.length == 1) {
				response = separatedResponse[0];
			} else {
				response = separatedResponse[1];
			}

			debug("Response:in getComponents method:" + response);
			return (R) new ResponseEntity<>(response, HttpStatus.OK);
		}

		@SuppressWarnings({ "unchecked", "rawtypes" })
		public R fetchUWQuestions(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
				throws RuntimeException {
			debug("Request:in getComponents method:" + requestData);

			 HttpSession session = (HttpSession) requestData.get("session").get(0);
			System.out.println(session.getAttribute("coverageName"));
			  SessionData sessionData = session.getAttribute("sessionData") != null
			  ? (SessionData) session.getAttribute("sessionData") : new
			  SessionData();
			  
			  if (sessionData.getProductId() == null) { throw new
			  DSCIQuoteException("ProductID is not in session"); } else {
			  requestData.add("productId", sessionData.getProductId()); }
			 
			String url = ViewElementService.getEnvironment().getProperty("url.pricingandcoverage");
			
			
			  String policyId =(String) session.getAttribute("Id");
			  if(policyId==null) { throw new DSCIQuoteException("ID not found"); }
			//  url += "?quote_section=11" + "&policy_id=" +policyId;
			 

			//requestData.remove("coverageId");
			requestData.add("productId", sessionData.getProductId() );
			requestData.add("quoteId",session.getAttribute("Id") );
			requestData.add("coverageName", session.getAttribute("coverageName"));
			
			//requestData.add("coverageId",  sessionData.getProductId() );
			requestData.remove("session");
			// converting URL to URI
			URI uri = DSCIService.toURI(url);

			HttpEntity saveEntity = getSaveHttpEntity(DSCIUtility.updateRequestDataWithJWTToken(requestData));

			// rest call
			ResponseEntity<String> responseData = restTemplate.exchange(uri, HttpMethod.POST, saveEntity, String.class);

			if(responseData.getBody().toString().equals("null")){
				url = ViewElementService.getEnvironment().getProperty("url.getQuestion");
				 url += "?quote_section=11" + "&policy_id=" +policyId;
				//restTemplate =new RestTemplate();
				ResponseEntity<String> TransactionData = restTemplate.exchange(url+ "&fetch_master=No",HttpMethod.POST, saveEntity, String.class);
				
				String[] TransactionD = TransactionData.getBody().split("##SessionData##");
				ObjectMapper objectMapper = new ObjectMapper();
				List<HashMap<String,Object>> transaction = new ArrayList();
				
				try {
					transaction = objectMapper.readValue(TransactionD[0], List.class);
					int groupsLength=((List)transaction.get(1).get("groups")).size();
					HashMap newCoverage = new HashMap();
					newCoverage.put("datasetName", session.getAttribute("coverageName"));
					newCoverage.put("coverageName", session.getAttribute("coverageName"));
					newCoverage.put("groupDisplaySequence", groupsLength);
					newCoverage.put("displayValue", "include");
					newCoverage.put("subGroups", null);
					((List)transaction.get(1).get("groups")).add(newCoverage);
					//RestTemplate rest = new RestTemplate();
					requestData.remove("operationType");
					requestData.add("operationType", "applyprice");
					HttpEntity saveEntity1 = getSaveHttpEntity(updateRequestDataWithJWTToken(requestData));
					ResponseEntity<String> responseData1 = restTemplate.exchange(uri, HttpMethod.POST, saveEntity1, String.class);
					HashMap response = new HashMap();
					response = objectMapper.readValue(responseData1.getBody(),HashMap.class);
					transaction.get(1).put("quoteBindable",response.get("quoteBindable"));
					transaction.get(1).put("price",response.get(" price"));
					String saveurl = ViewElementService.getEnvironment().getProperty("url.saveQuoteDetails");
					saveurl += "?quote_section=11" + "&policy_id=" + policyId;

					// requestData.add("productId", "6016");
					// String url =
					// CreateElementService.getEnvironment().getProperty("http://localhost:9067/pricingAndCoverage");
					// converting URL to URI
					URI saveuri = DSCIService.toURI(saveurl);

					HttpEntity saveEntity12 = getSaveHttpEntity(objectMapper.writeValueAsString(transaction) +"##SessionDetails##" +TransactionD[1]);

					// rest call
					ResponseEntity<String> responseData12 = restTemplate.exchange(saveuri, HttpMethod.POST, saveEntity12, String.class);
					
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				System.out.println("TransactionData"+TransactionData);
			}

			debug("Response:in getComponents method:" + responseData);
			return (R) responseData;
		}

		@SuppressWarnings({ "unchecked", "rawtypes" })
		public R getOptionals(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
				throws RuntimeException {
			debug("Request:in getComponents method:" + requestData);

			 HttpSession session = (HttpSession)
			 requestData.get("session").get(0);
			
			  SessionData sessionData = session.getAttribute("sessionData") != null
			  ? (SessionData) session.getAttribute("sessionData") : new
			  SessionData();
			  
			  if (sessionData.getProductId() == null) { throw new
			 DSCIQuoteException("ProductID is not in session"); } else {
			  requestData.add("productId",  sessionData.getProductId()); }
			 
			String url = ViewElementService.getEnvironment().getProperty("url.pricingandcoverage");
			
			
			  String policyId =  (String) session.getAttribute("Id");
			  if(policyId==null) { throw new DSCIQuoteException("ID not found"); }
			 // url += "?quote_section=11" + "&policy_id=" +policyId;
			 

			//requestData.add("productId", sessionData.getProductId() );
			requestData.remove("session");
			// converting URL to URI
			URI uri = DSCIService.toURI(url);

			HttpEntity saveEntity = getSaveHttpEntity(DSCIUtility.updateRequestDataWithJWTToken(requestData));

			System.out.println("k gxzkdfgkhsdgfkhgvikhgfdxsikhtfsg"+uri);
			// rest call
			ResponseEntity<String> responseData = restTemplate.exchange(uri, HttpMethod.POST, saveEntity, String.class);

			if (responseData.getBody().isEmpty()) {
				debug("Error: response is empty in getComponents method.");
				String message = CreateElementService.getEnvironment().getProperty("response.null");
				throw new DSCIQuoteException(message);
			}

			String[] sessionSepration = responseData.getBody().split("##SessionData##");
			// Getting master, transactional and sessiondata from appian response
			String[] separatedResponse = sessionSepration[0].split("--MidPayload--");

			String response;
			if (separatedResponse.length == 1) {
				response = separatedResponse[0];
			} else {
				response = separatedResponse[1];
			}

			debug("Response:in getComponents method:" + response);
			return (R) new ResponseEntity<>(response, HttpStatus.OK);
		}
		
		@SuppressWarnings({ "unchecked", "rawtypes" })
		public R getPrice(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
				throws RuntimeException {
			debug("Request:in getComponents method:" + requestData);

			HttpSession session = (HttpSession) requestData.get("session").get(0);

			 SessionData sessionData = session.getAttribute("sessionData") != null
					  ? (SessionData) session.getAttribute("sessionData") : new
					  SessionData();
			String url = ViewElementService.getEnvironment().getProperty("url.pricingandcoverage");
			String policyId = (String) session.getAttribute("Id") ;
			if (policyId == null) {
				throw new DSCIQuoteException("ID not found");
			}
			// url += "?quote_section=11" + "&policy_id=" +policyId;
			requestData.add("productId",(String)  sessionData.getProductId()  );
			requestData.remove("session");
			// converting URL to URI
			URI uri = DSCIService.toURI(url);
			HttpEntity saveEntity = getSaveHttpEntity(DSCIUtility.updateRequestDataWithJWTToken(requestData));
			System.out.println("k gxzkdfgkhsdgfkhgvikhgfdxsikhtfsg" + uri);
			// rest call
			ResponseEntity<String> responseData = restTemplate.exchange(uri, HttpMethod.POST, saveEntity, String.class);

			if (responseData.getBody().isEmpty()) {
				debug("Error: response is empty in getPrice method.");
				String message = CreateElementService.getEnvironment().getProperty("response.null");
				throw new DSCIQuoteException(message);
			}
			return (R) responseData;
		}

		// to save transactional
		@SuppressWarnings({ "unchecked", "rawtypes" })
		public R saveCoverages(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
				throws RuntimeException {
			debug("Request:in getComponents method:" + requestData);

			HttpSession session = (HttpSession) requestData.get("session").get(0);
			SessionData sessionData = session.getAttribute("sessionData") != null
					? (SessionData) session.getAttribute("sessionData") : new SessionData();

			if (sessionData.getProductId() == null) {
				throw new DSCIQuoteException("ProductID is not in session");
			} else {
				requestData.add("productId",  sessionData.getProductId());
			}
			List<HashMap> mixedData= (List<HashMap>) requestData.get("data").get(0);
			int index = 0;
			System.out.println("mixedData"+mixedData.get(1));
			for(HashMap temp : mixedData){
				 if(temp.get("packageName").toString().equals("Tailored Plan")){
					
					 break;
				 }
				 index ++;
			 }
	
			String policyId = (String) session.getAttribute("Id");
			if (policyId == null) {
				throw new DSCIQuoteException("ID not found");
			}
			String url = ViewElementService.getEnvironment().getProperty("url.saveQuoteDetails");
			url += "?quote_section=11" + "&policy_id=" + policyId;

			// requestData.add("productId", "6016");
			// String url =
			// CreateElementService.getEnvironment().getProperty("http://localhost:9067/pricingAndCoverage");
			// converting URL to URI
			URI uri = DSCIService.toURI(url);

			HttpEntity saveEntity = getSaveHttpEntity(toJson(Arrays.asList(mixedData.get(index)))+"##SessionDetails##"+toJson(sessionData));

			// rest call
			ResponseEntity<String> responseData = restTemplate.exchange(uri, HttpMethod.POST, saveEntity, String.class);

			if (responseData.getBody().isEmpty()) {
				debug("Error: response is empty in getComponents method.");
				String message = CreateElementService.getEnvironment().getProperty("response.null");
				throw new DSCIQuoteException(message);
			}

			/*
			 * String[] sessionSepration =
			 * responseData.getBody().split("##SessionData##"); // Getting master,
			 * transactional and sessiondata from appian response String[]
			 * separatedResponse = sessionSepration[0].split("--MidPayload--");
			 * 
			 * String response; if (separatedResponse.length == 1) { response =
			 * separatedResponse[0]; } else { response = separatedResponse[1]; }
			 */

			debug("Response:in getComponents method:" + responseData);
			return (R) new ResponseEntity<>(responseData, HttpStatus.OK);
		}

		@SuppressWarnings({ "unchecked", "rawtypes" })
		public R saveComponents(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
				throws RuntimeException {
			debug("Request:in getComponents method:" + requestData);

			HttpSession session = (HttpSession) requestData.get("session").get(0);
	
			String policyId = (String) session.getAttribute("Id");
			if (policyId == null) {
				throw new DSCIQuoteException("ID not found");
			}
			String url = ViewElementService.getEnvironment().getProperty("url.pricingandcoverage");
			// url += "?quote_section=11" + "&policy_id=" +policyId;

			// requestData.add("productId", "6016");
			// String url =
			// CreateElementService.getEnvironment().getProperty("http://localhost:9067/pricingAndCoverage");
			// converting URL to URI
			URI uri = DSCIService.toURI(url);

			requestData.add("quoteId", session.getAttribute("Id") );
			requestData.remove("session");
			HttpEntity saveEntity = getSaveHttpEntity(DSCIUtility.updateRequestDataWithJWTToken(requestData));

			// rest call
			ResponseEntity<String> responseData = restTemplate.exchange(uri, HttpMethod.POST, saveEntity, String.class);

			if (responseData.getBody().isEmpty()) {
				debug("Error: response is empty in getComponents method.");
				String message = CreateElementService.getEnvironment().getProperty("response.null");
				throw new DSCIQuoteException(message);
			}

			
			  String[] sessionSepration =
			  responseData.getBody().split("##SessionData##"); 
			  // Getting master,transactional and sessiondata from appian response 
			  String[] separatedResponse = sessionSepration[0].split("--MidPayload--");
			  
			  String response; if (separatedResponse.length == 1) { response =
			  separatedResponse[0]; } else { response = separatedResponse[1]; }
			 

			debug("Response:in getComponents method:" + responseData);
			return (R) new ResponseEntity<>(responseData, HttpStatus.OK);
		}

		public RestTemplate getTemplate() {
			return restTemplate;
		}

}