package com.wipro.dsci.quote.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.wipro.dsci.quote.enums.Actions;
import com.wipro.dsci.quote.enums.Element;
import static com.wipro.dsci.quote.service.DSCIService.debug;

/**
 * @author M0071243
 *
 */
@Service("UpdateElementService")
@Component
@PropertySource("classpath:application.properties")
public class UpdateElementService {

	

	@Autowired
	RestTemplate restTemplate;

	private static Environment environment;

	public UpdateElementService() {
	}

	@Autowired
	public UpdateElementService(Environment environment) {
		UpdateElementService.environment = environment;
	}

	public Object callUpdateService(LinkedMultiValueMap<String, Object> requestData) throws Throwable {

		debug("Request:in callUpdateService method:"+requestData);
		String elementType = requestData.containsKey("elementType") ? requestData.get("elementType").get(0).toString()
				: "";
		String operationType = requestData.containsKey("operationType")
				? requestData.get("operationType").get(0).toString() : "";
		DSCIService<?, ?> service = ServiceFactory.getService(Element.valueOf(elementType.toUpperCase()));
		if (null == service){
			debug("Response:in callUpdateService method:Unsupported operation!!");
			throw new Exception("Unsupported operation!!");
		}
		debug("Response:in callUpdateService method:"+service.getClass().getName());
		return (Object) service.delegate(Actions.valueOf(operationType.toUpperCase()), requestData);
	}

	/*@Bean
	public RestTemplate restTemplateInCreate() {
		return new RestTemplate();
	}*/

	public static Environment getEnvironment() {
		return environment;
	}
}
