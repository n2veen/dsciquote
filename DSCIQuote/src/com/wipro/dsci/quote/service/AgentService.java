package com.wipro.dsci.quote.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.wipro.dsci.quote.enums.Actions;
import com.wipro.dsci.quote.exception.DSCIQuoteException;
import static com.wipro.dsci.quote.service.DSCIService.debug;

@org.springframework.stereotype.Service("AgentService")
public class AgentService<R, E> implements DSCIService<R, DSCIQuoteException> {

	
	@Autowired
	RestTemplate restTemplate;

	private Map<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>> opTypeMap = new HashMap<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>>() {

		private static final long serialVersionUID = 1L;
		{
			put(Actions.VIEWAPCODE, (a, b) -> {
				return viewApCode(a, b);
			});

		}
	};

	@Override
	public R delegate(Actions actions, LinkedMultiValueMap<String, Object> requestData) {

		// null check and getting operationType

		String operationType = requestData.containsKey("operationType")
				? (String) requestData.get("operationType").get(0) : "";
		if (!opTypeMap.containsKey(Actions.valueOf(operationType.toUpperCase()))) {
			throw new IllegalArgumentException(String.format("%s is not supported", actions));
		}
		return opTypeMap.get(actions).apply(restTemplate, requestData);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R viewApCode(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in viewApCode method:"+requestData);
		String url = CreateElementService.getEnvironment().getProperty("isdsciagent.url.view");
		ResponseEntity<List> response = restTemplate.postForEntity(url, requestData, List.class);
		if (response.getBody().isEmpty()){
			debug("Error: response is empty in viewApCode method.");
			String message = CreateElementService.getEnvironment().getProperty("response.null");
			throw new DSCIQuoteException(message);
		}
		debug("Response:in viewApCode method:"+response);
		return (R) response;
	}

	
}
