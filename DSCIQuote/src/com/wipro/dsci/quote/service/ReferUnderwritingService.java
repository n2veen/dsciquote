package com.wipro.dsci.quote.service;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wipro.dsci.quote.enums.Actions;
import com.wipro.dsci.quote.exception.DSCIQuoteException;
import com.wipro.dsci.quote.model.SessionData;

import static com.wipro.dsci.quote.service.DSCIService.*;
import com.wipro.dsci.quote.util.*;
import static com.wipro.dsci.quote.util.DSCIUtility.*;

@org.springframework.stereotype.Service("ReferUnderwritingService")
public class ReferUnderwritingService<R, E> implements DSCIService<R, DSCIQuoteException> {

	@Autowired
	RestTemplate restTemplate;
	
	

	private Map<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>> opTypeMap = new HashMap<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>>() {

		private static final long serialVersionUID = 1L;
		{

			put(Actions.VIEWREFERUNDERWRITER, (a, b) -> {
				return viewReferUW(a, b);
			});
			put(Actions.VIEWNAMEINSURED, (a, b) -> {
				return viewNameInsured(a, b);
			});
			put(Actions.SAVENAMEINSURED, (a, b) -> {
				return savePrimaryNamed(a, b);
			});

			put(Actions.SAVEADDITIONALNAMEINSURED, (a, b) -> {
				return saveAdditionalNamedInsured(a, b);
			});	
			put(Actions.SAVEREFERUNDERWRITER, (a, b) -> {
				return saveReferUnderwriter(a, b);
			});
			put(Actions.EDIT_SECONDARY_NAMED_INSURED, (a, b) -> {
				return editNamedInsured(a, b);
			});
			put(Actions.SAVEINAPPIAN, (a, b) -> {
				return deleteNamedInsuredInAppian(a, b);
			});
			put(Actions.DELETE_NAMED_INSURED, (a, b) -> {
				return deleteSecondaryNamedInsuredRefer(a, b);
			});
			put(Actions.SAVEANDEXIT, (a, b) -> {
				return saveAndExit(a, b);
			});
		}

	};

	HandOffService handOffService = new HandOffService();
	
	@Override
	public R delegate(Actions actions, LinkedMultiValueMap<String, Object> requestData) throws DSCIQuoteException {

		// null check and getting operationType

		String operationType = requestData.containsKey("operationType")
				? (String) requestData.get("operationType").get(0) : "";

		if (!opTypeMap.containsKey(Actions.valueOf(operationType.toUpperCase()))) {
			throw new IllegalArgumentException(String.format("%s is not supported", actions));
		}
		return opTypeMap.get(actions).apply(restTemplate, requestData);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R viewReferUW(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws RuntimeException {

		debug("Request:in viewReferUW method:" + requestData);
		
		ObjectMapper mapper = new ObjectMapper();
		requestData.remove("elementType");

		// setting URL
		String url = CreateElementService.getEnvironment().getProperty("url.referToUW");

		HttpSession session = (HttpSession) requestData.get("session").get(0);
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();
		
		/*HashMap<String,String> tokens = new HashMap<>();
		tokens = handOffService.dcAuthentication("GAU0368", "utica1");
		String bearerToken1 = tokens.get("JsonWebToken");
		session.setAttribute("JsonWebToken", "Bearer " +"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJhZG1pbiIsIlNJRCI6IjZDQTMxRjZFOjgwNUM0NUVBOkJGQzUxRUMyOjExMDZGODE5OjdBMzVEODk3OjQ4RTkwQzQxIiwiU3lzdGVtSUQiOiJEU0NJIiwiQ0xJRU5USUQiOiIiLCJRVU9URUlEIjoiIiwieFBhc3MiOiIiLCJuYmYiOjE1MzAyNTg2MjcsImV4cCI6MTUzMDM0NTAyNywiaWF0IjoxNTMwMjU4NjI3LCJpc3MiOiJGT0FQSUFTIiwiYXVkIjoiRk9BUEkifQ.XMRSmG6bkNZIdJUGwnzhgaVbxJvECz7Nu0ksJWDfy-g");
		
		String policyId1="5397198";
		session.setAttribute("Id", policyId1);
		session.setAttribute("quoteBindable", "true");
				
		String sessionData = "{\"businessName\": null,   \"businessClass\": \"Bookbinding (Printing)\",   \"state\": \"NY\",   \"channel\": \"Independent_Agent\",   \"lob\": null,   \"productId\": null,   \"operationCode\": null,   \"businessAddress\": {     \"streetAddress\": \"129 W 29TH ST FL 2\",     \"suite\": \"\",     \"city\": \"NEW YORK\",     \"state\": \"NY\",     \"zip\": \"10001\",     \"businessName\": \"ONE MEDICAL GROUP\"   },   \"businessInfo\": {     \"year\": \"1998\",     \"sales\": \"$3,457,457\",     \"phone\": \"679-347-3466\"   },   \"effectiveStartDate\": null,   \"effectiveEndDate\": null,   \"lobs\": null,   \"beforeYouBeginData\": {     \"businessOwnersPolicy\": true,     \"state\": \"NY\",     \"effectiveDate\": \"2018-06-21\",     \"howShouldWeContactYou\": 1,     \"contactEmail\": \"asd@asd.com\",     \"contactPhoneNumber\": \"123123\",     \"contactName\": \"Abhilash \"   },   \"locationData\": null,   \"premiumValue\": null,   \"locationMap\": null,   \"subjectType\": null }";
		
		HashMap<String,Object> pricingMap = new HashMap<>();
		pricingMap.put("stp", "False");
		pricingMap.put("errorCode", null);
		pricingMap.put("message", "Bindable");
		pricingMap.put("status", 0);
		pricingMap.put("lossRatio", null);
		session.setAttribute("stpDetails", (pricingMap));*/
		
				
		HashMap<String, List<HashMap<String,Object>>> responseMap = new HashMap();
		SessionData sessionDataObj = null;
		String policyId = (String) session.getAttribute("Id");
		String bearerToken = session.getAttribute("JsonWebToken").toString();
		
		String appianData = getDataFromAppian(restTemplate, policyId);
		if(!appianData.equals("noData")){
			return (R) new ResponseEntity<String>(appianData, HttpStatus.OK); 
		}
		
		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}
		
		requestData.add("policyId", policyId);
		requestData.remove("session");
		requestData.add("bearerToken", bearerToken);

		LinkedMultiValueMap<String, Object> request = new LinkedMultiValueMap<String, Object>();

		

		// requestData = updateRequestDataWithJWTToken(requestData);
		// converting URL to URI
		URI uri = DSCIService.toURI(url);

		// preparing header for rest call
		HttpEntity saveEntity = getSaveHttpEntity(requestData);
		
		ResponseEntity<String> response = restTemplate.postForEntity(uri, saveEntity, String.class);
		
		try {
			responseMap = mapper.readValue(response.getBody(), HashMap.class);
			
			sessionDataObj = mapper.readValue(toJson(sessionData), SessionData.class);
			//sessionDataObj = mapper.readValue(sessionData, SessionData.class);
		
		} catch (IOException e) {
			e.printStackTrace();
		}
		String contactName = sessionDataObj.getBeforeYouBeginData().get("contactName").toString();
		String ContactPhoneNumber = sessionDataObj.getBeforeYouBeginData().get("contactPhoneNumber").toString();
		String ContactEmail = sessionDataObj.getBeforeYouBeginData().get("contactEmail").toString();
		String HowShouldWeContactYou_temp = sessionDataObj.getBeforeYouBeginData().get("howShouldWeContactYou").toString();
		String HowShouldWeContactYou;
		if(HowShouldWeContactYou_temp.equals("1")) HowShouldWeContactYou = "Phone";
		else HowShouldWeContactYou  ="Email";
		
		
		
		responseMap.get("agentContactInfo").parallelStream().forEach(x->{
			
			String systemRefId = x.get("systemRefId").toString();
			
			if(systemRefId.equals("UNIG_PolicyUndQuestionsInput.ContactName")) x.put("userValue",contactName);
			else if(systemRefId.equals("UNIG_PolicyUndQuestionsInput.ContactPhoneNumber")) x.put("userValue",ContactPhoneNumber);
			else if(systemRefId.equals("UNIG_PolicyUndQuestionsInput.ContactEmail")) x.put("userValue",ContactEmail);
			else if(systemRefId.equals("UNIG_PolicyUndQuestionsInput.HowShouldWeContactYou")) x.put("userValue",HowShouldWeContactYou);
		
		});
		
		
		
		String businessName =sessionDataObj.getBusinessAddress().getBusinessName();
		
		List<HashMap<String,Object>> mappedNamedInsured = (List<HashMap<String, Object>>) responseMap.get("insuredInformationMetadata").get(0).get("insuredInformation");
		mappedNamedInsured.parallelStream().filter(x->x.containsKey("systemRefId")).filter(x->x.get("systemRefId").equals("AdditionalOtherInterestInput.Name")).forEach(x->{
			
			x.put("userValue", businessName);
		});
		responseMap.get("insuredInformationMetadata").get(0).put("insuredInformation", mappedNamedInsured);
		debug("Response:in viewReferUW method:");
		
		String dataToSave = toJson(responseMap)+"##SessionDetails##"+toJson(sessionData);
		saveDataInAppian(restTemplate, dataToSave,policyId);
		return (R) new ResponseEntity(toJson(responseMap),HttpStatus.OK);
		// return (R) new ResponseEntity<>(response.getBody(), HttpStatus.OK);
	}


	// For Name Insured
	@SuppressWarnings({ "unchecked" })
	public R viewNameInsured(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws RuntimeException {

		debug("Request:in viewNameInsured method:" + requestData);

		requestData.remove("elementType");

		// setting URL
		String url = CreateElementService.getEnvironment().getProperty("url.referToUW");

		ObjectMapper mapper = new ObjectMapper();
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		String policyId = (String) session.getAttribute("Id");
		
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();
		
		String businessName =sessionData.getBusinessAddress().getBusinessName();
		String bearerToken = session.getAttribute("JsonWebToken").toString();
		requestData.add("bearerToken", bearerToken);
		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}

		// converting URL to URI
		URI uri = DSCIService.toURI(url);
		requestData.add("policyId", policyId);
		requestData.remove("session");
		// preparing header for rest call
		LinkedMultiValueMap<String, Object> request = new LinkedMultiValueMap<String, Object>();

		requestData.add("bearerToken", bearerToken);
		HttpEntity saveEntity = getSaveHttpEntity(requestData);

		ResponseEntity<String> response = restTemplate.postForEntity(uri, saveEntity, String.class);
		
		HashMap<String,List<HashMap<String,Object>>> resMap = new HashMap<>();
	
		try {
			resMap = mapper.readValue(response.getBody(), HashMap.class);
		} catch (IOException e) {
			throw new DSCIQuoteException("Error while mapping response of view secondary named insured");
		}
		resMap.get("insuredInformation").parallelStream().filter(x->x.containsKey("systemRefId")).filter(x->x.get("systemRefId").equals("AdditionalOtherInterestInput.Name")).forEach(x->{	
			x.put("userValue", businessName);
		});
		debug("Response:in viewNameInsured method:" + response);
		String dataToSave = response.getBody()+"##SessionDetails##"+toJson(sessionData);
		saveDataInAppian(restTemplate, dataToSave,policyId);
		return (R) response;
		// return (R) new ResponseEntity<>(response.getBody(), HttpStatus.OK);
	}

	

	@SuppressWarnings({ "unchecked" })
	public R savePrimaryNamed(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws RuntimeException {
		debug("Request:in saveAdditionalNamedInsured method:" + requestData);
		
		HashMap<String, List<HashMap<String,Object>>> responseMap = new HashMap();
		
		ObjectMapper mapper = new ObjectMapper();
		
	
		// setting URL
		String url = CreateElementService.getEnvironment().getProperty("url.referToUW");

		HttpSession session = (HttpSession) requestData.get("session").get(0);
		String policyId = (String) session.getAttribute("Id");
		
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();
		
		String bearerToken = session.getAttribute("JsonWebToken").toString();
		requestData.add("bearerToken", bearerToken);
		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}

		// converting URL to URI
		URI uri = DSCIService.toURI(url);

		requestData.add("policyId", policyId);
		requestData.remove("session");
		// preparing header for rest call
		

		
		HttpEntity saveEntity = getSaveHttpEntity(requestData);

	

		ResponseEntity<String> response = restTemplate.postForEntity(uri, saveEntity, String.class);

		// ResponseEntity<String> response2 =
		// restTemplate.exchange("http://localhost:9102/referUnderwriter/",
		// HttpMethod.POST, saveEntity, String.class);
		String dataToSave = toJson(requestData.get("data").get(0))+"##SessionDetails##"+toJson(sessionData);
		saveDataInAppian(restTemplate, dataToSave,policyId);
		HashMap<String,Object> responseFromDC = new HashMap<>();
		try {
			responseMap = mapper.readValue(toJson(requestData.get("data").get(0)), HashMap.class);
			responseFromDC= mapper.readValue(response.getBody(), HashMap.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		requestData.remove("elementType");
		responseMap.get("licensedProducerSigningApplication").get(0).put("userValue", responseFromDC.get("producerList"));
		return (R) new ResponseEntity<>(response.getBody(), HttpStatus.OK);
	}

	// To Save Additonal Name Insured
	@SuppressWarnings({ "unchecked" })
	public R saveAdditionalNamedInsured(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws RuntimeException {
		debug("Request:in saveAdditionalNamedInsured method:" + requestData);

		requestData.remove("elementType");

		// setting URL
		String url = CreateElementService.getEnvironment().getProperty("url.referToUW");

		HttpSession session = (HttpSession) requestData.get("session").get(0);
		String policyId = (String) session.getAttribute("Id");
	
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();
		
		String bearerToken = session.getAttribute("JsonWebToken").toString();
		requestData.add("bearerToken", bearerToken);
		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}

		// converting URL to URI
		URI uri = DSCIService.toURI(url);
		requestData.add("policyId", policyId);
		requestData.remove("session");

		// preparing header for rest call
		

		
		HttpEntity saveEntity = getSaveHttpEntity(requestData);
		ResponseEntity<String> response = restTemplate.postForEntity(uri, saveEntity, String.class);
		String dataToSave = toJson(requestData.get("data").get(0))+"##SessionDetails##"+toJson(sessionData);
		saveDataInAppian(restTemplate, dataToSave,policyId);
		debug("Response:in saveAdditionalNamedInsured method:");
		return (R) response;
		// return (R) new ResponseEntity<>(response.getBody(), HttpStatus.OK);
	}

	// To save the whole Page
	@SuppressWarnings({ "unchecked" })
	public R saveReferUnderwriter(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws RuntimeException {

		debug("Request:in saveReferUnderwriter method:" + requestData);

		requestData.remove("elementType");

		// setting URL
		String url = CreateElementService.getEnvironment().getProperty("url.referToUW");

		HttpSession session = (HttpSession) requestData.get("session").get(0);
		String policyId = (String) session.getAttribute("Id");
		//String quoteBindable = session.getAttribute("quoteBindable").toString();
		
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();
		String bearerToken = session.getAttribute("JsonWebToken").toString();
		String quoteBindable = sessionData.getQuoteBindable().toString();
	
		
		HashMap<String,Object> stpDetails = (HashMap<String, Object>) sessionData.getStpDetails();//session.getAttribute("stpDetails");
		
		if(stpDetails==null){
			throw new DSCIQuoteException("Session Data not found from pricing");
		}
		
		requestData.add("bearerToken", bearerToken);
		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}

		String lossRatio;
		if(stpDetails.get("lossRatio")==null) lossRatio= "null";
		else lossRatio = stpDetails.get("lossRatio").toString();
		
		// converting URL to URI
		URI uri = DSCIService.toURI(url);

		requestData.add("policyId", policyId);
		requestData.add("quoteBindable", quoteBindable);
		requestData.add("lossRatio", lossRatio);
		requestData.remove("session");

		// preparing header for rest call
		LinkedMultiValueMap<String, Object> request = new LinkedMultiValueMap<String, Object>();

		
		HttpEntity saveEntity = getSaveHttpEntity(requestData);

		ResponseEntity<String> response = restTemplate.postForEntity(uri, saveEntity, String.class);
		//String dataToSave = toJson(requestData.get("data").get(0))+"##SessionDetails##"+toJson(sessionData);
		//saveDataInAppian(restTemplate, dataToSave,policyId);
		return (R) response;
		// return (R) new ResponseEntity<>(response.getBody(), HttpStatus.OK);
	}

	// To save the whole Page
	@SuppressWarnings({ "unchecked" })
	public R editNamedInsured(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws RuntimeException {

		debug("Request:in saveReferUnderwriter method:" + requestData);

		requestData.remove("elementType");

		// setting URL
		String url = CreateElementService.getEnvironment().getProperty("url.referToUW");

		HttpSession session = (HttpSession) requestData.get("session").get(0);
		
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();
		String policyId = (String) session.getAttribute("Id");
		String bearerToken = session.getAttribute("JsonWebToken").toString();
		requestData.add("bearerToken", bearerToken);
		
		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}

		// converting URL to URI
		URI uri = DSCIService.toURI(url);

		requestData.add("policyId", policyId);
		requestData.remove("session");

		// preparing header for rest call
		

		
		HttpEntity saveEntity = getSaveHttpEntity(requestData);

		ResponseEntity<String> response = restTemplate.postForEntity(uri, saveEntity, String.class);
		String dataToSave = toJson(requestData.get("data").get(0))+"##SessionDetails##"+toJson(sessionData);
		saveDataInAppian(restTemplate, dataToSave,policyId);
		return (R) response;
		// return (R) new ResponseEntity<>(response.getBody(), HttpStatus.OK);
	}
	
	/**
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return
	 */
	public String saveDataInAppian(RestTemplate restTemplate, String dataToSave,String policyId){
		// removing control data
		
		String QuoteSection = "14";
		String saveURL = CreateElementService.getEnvironment().getProperty("url.saveQuoteDetails");
		saveURL += "?policy_id=" + policyId + "&quote_section=" + QuoteSection + "&action=SC";
		URI uri = toURI(saveURL);
		
		
		try {
			restTemplate.exchange(uri, HttpMethod.POST, getSaveHttpEntity(dataToSave), String.class);
		} catch (RestClientException e) {
			
			e.printStackTrace();
		}
		return "Success";
	}
	
	public String getDataFromAppian(RestTemplate restTemplate,String policyId){
	
		
		String url = CreateElementService.getEnvironment().getProperty("url.getQuestion");
		String QuoteSection = "14";
		url += "?policy_id=" + policyId + "&quote_section=" + QuoteSection + "&fetch_master=No";
		URI uri = toURI(url);
		
		ResponseEntity<String> resp = restTemplate.exchange(uri, HttpMethod.POST, getSaveHttpEntity(new HashMap()), String.class);
		if(resp.getBody().equals("##SessionData##")) return "noData";
		
		String[] newIn = resp.getBody().split("##SessionData##");

		String responseFromAppian = resp.getBody();
		String[] withOutSession = responseFromAppian.split("##SessionData##");
		if(withOutSession[0].length()<1) return "noData";
		else{
			
			return withOutSession[0];
		}
	}
	//this is actually BYIP delete
	public R deleteNamedInsuredInAppian(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData){
		// removing control data
		requestData.remove("elementType");
		requestData.remove("operationType");
		
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();
		
		String policyId = (String) session.getAttribute("Id");
		/*SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();*/
		// setting URL
		
		Object data = requestData.get("data").get(0);
		String duckId = requestData.get("duckID").get(0).toString();
		ObjectMapper mapper = new ObjectMapper();
		HashMap<String,List<HashMap<String,Object>>> mappedObject = new HashMap<>();
	
		try {
			mappedObject = mapper.readValue(toJson(data), HashMap.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		mappedObject.get("insuredInformationMetadata").removeIf(p->p.get("duckCreekId").equals(duckId));
		String dataToSave = toJson(mappedObject)  +"##SessionDetails##" +toJson(sessionData);
		saveDataInAppian(restTemplate, dataToSave,policyId);
		return (R) new ResponseEntity<String>(toJson(mappedObject),HttpStatus.OK);
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public R deleteSecondaryNamedInsuredRefer(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {

	
		debug("Request:in saveBeforeYouIssuePolicyData method:");
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		String duckId = requestData.get("duckID").get(0).toString();
		String policyId = (String) session.getAttribute("Id");
		String bearerToken = session.getAttribute("JsonWebToken").toString();
		
		String url2 = CreateElementService.getEnvironment().getProperty("url.before.you.issue.policy");

		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}

		LinkedMultiValueMap requestData1 = new LinkedMultiValueMap<>();
		requestData1.add("key", "removeNI");
		requestData1.add("policyId", policyId);
		requestData1.add("duckID", duckId);
		requestData1.add("bearerToken", bearerToken);
		

		HttpEntity saveEntity = getSaveHttpEntity(requestData1);
		ResponseEntity<String> response = restTemplate.exchange(url2, HttpMethod.POST, saveEntity, String.class);

		if (response.getBody().isEmpty()) {
			debug("Error: response is empty in saveBeforeYouIssuePolicyData method.");
			String message = CreateElementService.getEnvironment().getProperty("response.null");
			throw new DSCIQuoteException(message);
		}

		deleteNamedInsuredInAppian(restTemplate, requestData);
		
		debug("Response:in saveBeforeYouIssuePolicyData method:");
		return (R) response;

	}
	
	public List<HashMap<String,String>> insertInMasterNamedInsured(List<HashMap<String,String>> input,String sysRefIdName,String valueName){
	
		
		input.parallelStream().filter(x->x.containsKey("systemRefId")).filter(x->x.get("systemRefId").equals(sysRefIdName)).forEach(x->{
		
			if(x.get("systemRefId").equals(sysRefIdName)) {x.put("userValue", valueName);}
			
			
		});
		
		return input;
	}
	
	

	/**
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return
	 */
	public R saveAndExit(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData){
		
HttpSession session = (HttpSession) requestData.get("session").get(0);
		
		String policyId = (String) session.getAttribute("Id");
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();
		String dataToSave = toJson(requestData.get("data").get(0))  +"##SessionDetails##" +toJson(sessionData);
		
		
		
		String QuoteSection = "14";
		String saveURL = CreateElementService.getEnvironment().getProperty("url.saveQuoteDetails");
		saveURL += "?policy_id=" + policyId + "&quote_section=" + QuoteSection + "&action=SE";
		URI uri = toURI(saveURL);
	
		
		try {
			restTemplate.exchange(uri, HttpMethod.POST, getSaveHttpEntity(dataToSave), String.class);
		} catch (RestClientException e) {
			
			e.printStackTrace();
		}
		session.invalidate();
		return (R) new ResponseEntity<String>("{\"status\":\"Success\"}",HttpStatus.OK);
	}
	
	@SuppressWarnings("rawtypes")
	public static HttpEntity getSaveHttpEntityForUi(Object data) {

		HttpHeaders httpHeaders = new HttpHeaders() {
			private static final long serialVersionUID = 1L;
			{

			}
		};
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON_UTF8));
		return new HttpEntity<>(data, httpHeaders);
	}
}