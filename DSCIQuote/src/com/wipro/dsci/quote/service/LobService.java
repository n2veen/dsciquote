package com.wipro.dsci.quote.service;

import static com.wipro.dsci.quote.service.DSCIService.debug;
import static com.wipro.dsci.quote.service.DSCIService.getSaveHttpEntity;
import static com.wipro.dsci.quote.service.DSCIService.toJson;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wipro.dsci.quote.enums.Actions;
import com.wipro.dsci.quote.enums.QuoteSection;
import com.wipro.dsci.quote.exception.DSCIQuoteException;
import com.wipro.dsci.quote.model.SessionData;
import com.wipro.dsci.quote.util.DSCIUtility;

@org.springframework.stereotype.Service("LobService")
public class LobService<R, E> implements DSCIService<R, DSCIQuoteException> {

	@Autowired
	RestTemplate restTemplate;

	private Map<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>> opTypeMap = new HashMap<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>>() {

		private static final long serialVersionUID = 1L;
		{

			put(Actions.VIEWLOBDETAILS, (a, b) -> {
				return viewLobDetails(a, b);
			});
			put(Actions.SAVELOBDETAILS, (a, b) -> {
				return saveLobDetails(a, b);
			});
		}

	};

	@Override
	public R delegate(Actions actions, LinkedMultiValueMap<String, Object> requestData) throws DSCIQuoteException {

		// null check and getting operationType

		String operationType = requestData.containsKey("operationType")
				? (String) requestData.get("operationType").get(0) : "";

		if (!opTypeMap.containsKey(Actions.valueOf(operationType.toUpperCase()))) {
			throw new IllegalArgumentException(String.format("%s is not supported", actions));
		}
		return opTypeMap.get(actions).apply(restTemplate, requestData);
	}

	/**
	 * @param restTemplate
	 * @param requestData
	 * @return R
	 */
	@SuppressWarnings("unchecked")
	public R viewLobDetails(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {

		// fetch session details
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		SessionData sessionData = (SessionData) session.getAttribute("sessionData");
		String policyId = (String) session.getAttribute("Id");
		Boolean qualificationQuestionsAnswered = (Boolean) session.getAttribute("QQAnswered");
		List<Map<String, Object>> updatedResponse = null;
		String lobResponse = null;

		// fetch appian url
		String appianURL = CreateElementService.getEnvironment().getProperty("url.getQuestion") + "?quote_section=%s"
				+ "&policy_id=" + policyId + "&fetch_master=No";

		// Id should not be null
		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}

		if (qualificationQuestionsAnswered != null && qualificationQuestionsAnswered) {
			appianURL = String.format(appianURL, QuoteSection.LOB.getValue());
			ResponseEntity<String> lobResponseEntity = restTemplate.exchange(appianURL, HttpMethod.POST,
					getSaveHttpEntity(new HashMap<>()), String.class);
			lobResponse = parseAppianResponse(lobResponseEntity.getBody());
		} else {
			String businessDetailsResponse = parseAppianResponse((String) session.getAttribute("businessDetailsJson"));

			// rest call
			LinkedMultiValueMap<String, Object> request = new LinkedMultiValueMap<>();
			request.add("data", businessDetailsResponse);
			request.add("BusinessOwners", session.getAttribute("BusinessOwners"));
			request.add("clientId", session.getAttribute("clientId"));
			request = DSCIUtility.updateRequestDataWithJWTToken(request);

			String lobUrl = CreateElementService.getEnvironment().getProperty("url.lob");
			lobResponse = restTemplate.exchange(lobUrl, HttpMethod.POST, getSaveHttpEntity(request), String.class)
					.getBody();
		}

		try {
			updatedResponse = new ObjectMapper().readValue(lobResponse, new TypeReference<List<Map<String, Object>>>() {
			});
		} catch (IOException exception) {
			throw new DSCIQuoteException("Json Conversion failed");
		}

		if (updatedResponse != null && !CollectionUtils.isEmpty(sessionData.getLobs())) {
			for (Map<String, Object> entry : updatedResponse) {
				if (sessionData.getLobs().contains(entry.get("datasetName"))) {
					entry.put("editable", "N");
				}
			}
		}

		return (R) new ResponseEntity<Object>(updatedResponse, HttpStatus.OK);
	}

	/**
	 * @param restTemplate
	 * @param requestData
	 * @return R
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R saveLobDetails(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in saveLobDetails method:" + requestData);
		requestData.remove("elementType");
		requestData.remove("operationType");
		requestData.remove("action");

		// Fetch transaction data from appian
		String QuoteSection = "2";
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		String policyId = (String) session.getAttribute("Id");
		String url = CreateElementService.getEnvironment().getProperty("url.getQuestion");
		url += "?quote_section=" + QuoteSection + "&policy_id=" + policyId + "&fetch_master=No";

		LinkedMultiValueMap<String, Object> data = new LinkedMultiValueMap<>();
		data.add("operationType", "VIEWBUSINESSDETAILS");
		HttpEntity entity = getSaveHttpEntity(data);
		debug("Connecting: viewBusinessDetails : to APPIAN with HttpEntity: " + entity);
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

		// rest call
		URI uri = DSCIService.toURI(url);
		ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);

		// ResponseEntity<String> response =
		// restTemplate.postForEntity("http://localhost:9090/businessdetails/",null,String.class);
		String[] separatedResponse = null;

		debug("Exit: viewBusinessDetails method of BusinessClassService in DSCIQuote with reponse: " + response);
		if (response.getBody().isEmpty()) {
			debug("Error: response is empty in viewBusinessDetails method.");
			String message = CreateElementService.getEnvironment().getProperty("response.null");
			throw new DSCIQuoteException(message);
		} else {
			separatedResponse = response.getBody().split("##SessionData##");
		}

		ResponseEntity<String> responseString;

		String[] separatedSessionAndTransactionalResponse = separatedResponse[0].split("--MidPayload--");
		if (separatedSessionAndTransactionalResponse.length > 1) {
			debug("sending transactional data");

			responseString = new ResponseEntity<String>(separatedSessionAndTransactionalResponse[1], HttpStatus.OK);
		} else {
			debug("sending master data");
			responseString = new ResponseEntity<String>(separatedSessionAndTransactionalResponse[0], HttpStatus.OK);
		}
		debug("Response:in viewBusinessDetails method:" + responseString);

		String lobUrl = CreateElementService.getEnvironment().getProperty("url.lob");

		LinkedMultiValueMap<String, Object> saveRequest = new LinkedMultiValueMap<>();
		saveRequest.add("data", requestData.get("data"));
		saveRequest.add("transactionData", responseString.getBody());
		saveRequest.add("clientId", session.getAttribute("clientId"));

		saveRequest = DSCIUtility.updateRequestDataWithJWTToken(saveRequest);

		HttpEntity saveEntity = getSaveHttpEntity(saveRequest);

		// rest call
		ResponseEntity<String> lobResponse = restTemplate.exchange(lobUrl, HttpMethod.PUT, saveEntity, String.class);

		// Save to appian
		saveDataToAppian(requestData);

		debug("Response:in saveLobDetails method:" + response);
		return (R) lobResponse;
	}

	private void saveDataToAppian(LinkedMultiValueMap<String, Object> requestData) {
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();
		String saveURL = CreateElementService.getEnvironment().getProperty("url.saveQuoteDetails");
		String policyId = session.getAttribute("Id").toString();
		String QuoteSection = "3";
		saveURL += "?&policy_id=" + policyId + "&quote_section=" + QuoteSection;
		String dataToSave = toJson(requestData.get("data")) + "##SessionDetails##" + toJson(sessionData);
		HttpEntity<?> saveEntity = getSaveHttpEntity(dataToSave);
		restTemplate.exchange(DSCIService.toURI(saveURL), HttpMethod.POST, saveEntity, String.class);
	}

	/**
	 * 
	 * @param responseEntity
	 * @return
	 */
	private String parseAppianResponse(String response) {
		String[] separatedResponse = null;
		if (response.isEmpty()) {
			String message = CreateElementService.getEnvironment().getProperty("response.null");
			throw new DSCIQuoteException(message);
		} else {
			separatedResponse = response.split("##SessionData##");
		}

		String responseString = null;

		String[] separatedSessionAndTransactionalResponse = separatedResponse[0].split("--MidPayload--");
		if (separatedSessionAndTransactionalResponse.length > 1) {
			responseString = separatedSessionAndTransactionalResponse[1];
		} else {
			responseString = separatedSessionAndTransactionalResponse[0];
		}
		return responseString;
	}

}
