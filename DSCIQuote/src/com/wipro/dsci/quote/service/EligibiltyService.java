package com.wipro.dsci.quote.service;

import static com.wipro.dsci.quote.service.DSCIService.debug;
import static com.wipro.dsci.quote.service.DSCIService.getSaveHttpEntity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.wipro.dsci.quote.enums.Actions;
import com.wipro.dsci.quote.exception.DSCIQuoteException;
import com.wipro.dsci.quote.model.SessionData;
import com.wipro.dsci.quote.util.DSCIUtility;

@org.springframework.stereotype.Service("EligibiltyService")
public class EligibiltyService<R, E> implements DSCIService<R, DSCIQuoteException> {
	
	@Autowired
	RestTemplate restTemplate;

	private Map<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>> opTypeMap = new HashMap<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>>() {

		private static final long serialVersionUID = 1L;
		{

			put(Actions.VIEWELIGIBILTY, (a, b) -> {
				return viewEligibiltyInAppian(a, b);
			});
			put(Actions.SAVEELIGIBILTY, (a, b) -> {
				return saveEligibiltyInAppian(a, b);
			});
			put(Actions.SAVERERENDERELIGIBILITY,(a, b)->{
				return saveReRenderEligibiltyInAppian(a, b);
			});
		}
	};

	@Override
	public R delegate(Actions actions, LinkedMultiValueMap<String, Object> requestData) {

		// null check and getting operationType

		String operationType = requestData.containsKey("operationType")
				? (String) requestData.get("operationType").get(0) : "";

		if (!opTypeMap.containsKey(Actions.valueOf(operationType.toUpperCase()))) {
			throw new IllegalArgumentException(String.format("%s is not supported", actions));
		}
		return opTypeMap.get(actions).apply(restTemplate, requestData);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R viewEligibiltyInAppian(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws RuntimeException {

		debug("Request:in viewEligibiltyInAppian method:" + requestData);

		// removing control data
		requestData.remove("elementType");
		requestData.remove("operationType");

		String eligibilityUrl = CreateElementService.getEnvironment().getProperty("url.eligibility");

		HttpSession session = (HttpSession) requestData.get("session").get(0);
		LinkedMultiValueMap<String, Object> data = new LinkedMultiValueMap<>();
		data.add("operationType", "VIEWELIGIBILITY");
		if (session.getAttribute("QQAnswered") != null && (Boolean) session.getAttribute("QQAnswered")) {
            data.remove("operationType");
			data.add("operationType", "RERENDERELGIBILITY");
		}
		data.add("clientId", session.getAttribute("clientId").toString());
		data = DSCIUtility.updateRequestDataWithJWTToken(data);
		HttpEntity entity = getSaveHttpEntity(data);
		debug("Connecting: Qualification Question HttpEntity: " + entity);
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

		// rest call
		// ResponseEntity<String> response = restTemplate.exchange(uri,
		// HttpMethod.POST, entity, String.class);

		ResponseEntity<String> response = restTemplate.postForEntity(eligibilityUrl, data, String.class);

		String[] separatedResponse = null;

		debug("Exit: viewEligibiltyInAppian method of EligibilityService in DSCIQuote with reponse: " + response);
		if (response.getBody().isEmpty()) {
			debug("Error: response is empty in viewEligibiltyInAppian method.");
			String message = CreateElementService.getEnvironment().getProperty("response.null");
			throw new DSCIQuoteException(message);
		} else {
			separatedResponse = response.getBody().split("##SessionData##");
		}

		ResponseEntity<String> responseString;

		String[] separatedSessionAndTransactionalResponse = separatedResponse[0].split("--MidPayload--");
		if (separatedSessionAndTransactionalResponse.length > 1) {
			debug("sending transactional data");

			responseString = new ResponseEntity<String>(separatedSessionAndTransactionalResponse[1], HttpStatus.OK);
		} else {
			debug("sending master data");
			responseString = new ResponseEntity<String>(separatedSessionAndTransactionalResponse[0], HttpStatus.OK);
		}
		debug("Response:in viewEligibiltyInAppian method:" + responseString);
		return (R) responseString;

		/*
		 * // setting URL String saveURL =
		 * CreateElementService.getEnvironment().getProperty("url.getQuestion");
		 * HttpSession session = (HttpSession)
		 * requestData.get("session").get(0);
		 * 
		 * String policyId = (String) session.getAttribute("Id");
		 * if(policyId==null) { throw new DSCIQuoteException("ID not found"); }
		 * String QuoteSection = "4";
		 * 
		 * saveURL += "?quote_section=" + QuoteSection + "&policy_id="
		 * +policyId;
		 * 
		 * // converting URL to URI URI uri = DSCIService.toURI(saveURL);
		 * 
		 * // preparing header for rest call
		 * 
		 * 
		 * 
		 * SessionData sessionData = session.getAttribute("sessionData") != null
		 * ? (SessionData) session.getAttribute("sessionData") : new
		 * SessionData();
		 * 
		 * LinkedMultiValueMap<String, Object> appian_data = new
		 * LinkedMultiValueMap<String, Object>();
		 * 
		 * // setting business class if (sessionData.getBusinessClass() == null)
		 * { throw new DSCIQuoteException("Business class is not in session"); }
		 * else { appian_data.add("businessClass",
		 * sessionData.getBusinessClass()); }
		 * 
		 * // setting state if (sessionData.getState() == null) { throw new
		 * DSCIQuoteException("State is not in session"); } else {
		 * appian_data.add("state", sessionData.getState()); }
		 * 
		 * // setting channel if (sessionData.getChannel() == null) { throw new
		 * DSCIQuoteException("Channel is not in session"); } else {
		 * appian_data.add("channel", sessionData.getChannel()); }
		 * 
		 * // setting lob name if (sessionData.getLob() == null) { throw new
		 * DSCIQuoteException("Lob name is not in session"); } else {
		 * appian_data.add("lob", sessionData.getLob()); }
		 * 
		 * 
		 * 
		 * HttpEntity saveEntity = getSaveHttpEntity(appian_data);
		 * 
		 * // rest call ResponseEntity<String> response =
		 * restTemplate.exchange(uri, HttpMethod.POST, saveEntity,
		 * String.class); // HttpEntity saveEntity = new
		 * HttpEntity<>(httpHeaders); // ResponseEntity<String> response =
		 * restTemplate.exchange(uri, // HttpMethod.GET, saveEntity,
		 * String.class);
		 * 
		 * if (response.getBody().isEmpty()) {
		 * debug("Error: response is empty in viewEligibiltyInAppian method.");
		 * String message =
		 * CreateElementService.getEnvironment().getProperty("response.null");
		 * throw new DSCIQuoteException(message); }
		 * 
		 * String[] separatedResponse = null; if (response.getBody().isEmpty())
		 * { throw new DSCIQuoteException("Check your internet coonection"); }
		 * else { separatedResponse =
		 * response.getBody().split("##SessionData##"); } ResponseEntity<String>
		 * responseString;
		 * 
		 * String[] separatedSessionAndTransactionalResponse =
		 * separatedResponse[0].split("--MidPayload--"); if
		 * (separatedSessionAndTransactionalResponse.length > 1) {
		 * 
		 * responseString = new
		 * ResponseEntity<String>(separatedSessionAndTransactionalResponse[1],
		 * HttpStatus.OK); } else { responseString = new
		 * ResponseEntity<String>(separatedSessionAndTransactionalResponse[0],
		 * HttpStatus.OK); } debug("Response:in viewEligibiltyInAppian method:"
		 * + responseString);
		 * 
		 * HashMap<String, Object> res; try { ObjectMapper mapper = new
		 * ObjectMapper(); res = mapper.readValue(responseString.getBody(),
		 * HashMap.class); } catch (IOException ex) { throw new
		 * DSCIQuoteException("Error while converting string to object!"); }
		 * 
		 * String productId = res.get("productId").toString();
		 * 
		 * System.out.println("ProductId: " + productId); // setting product id
		 * in session sessionData.setProductId(productId);
		 * session.setAttribute("sessionData", sessionData);
		 * 
		 * 
		 * return (R) responseString;
		 */
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R saveEligibiltyInAppian(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {

		debug("Request:in saveEligibiltyInAppian method:" + requestData);

		// removing control data
		requestData.remove("elementType");
		requestData.remove("operationType");
		String eligibilityUrl = CreateElementService.getEnvironment().getProperty("url.eligibility");

		List<Map<String, Object>> eligibilityData = (List<Map<String, Object>>) requestData.get("data").get(0);

		LinkedMultiValueMap<String, Object> data = new LinkedMultiValueMap<>();
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		data.add("operationType", "SAVEELIGIBILITY");
		data.add("clientId", session.getAttribute("clientId").toString());
		data.add("data", eligibilityData);
		data = DSCIUtility.updateRequestDataWithJWTToken(data);
		HttpEntity entity = getSaveHttpEntity(data);
		debug("Connecting: Qualification Question HttpEntity: " + entity);

		ResponseEntity<String> response = restTemplate.postForEntity(eligibilityUrl, entity, String.class);

		// Save all the lobs with questions answered in session
		List<String> lobs = new ArrayList<String>();
		for (Map<String, Object> entry : eligibilityData) {
			lobs.add((String) entry.get("lob"));
		}

		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();
		sessionData.setLobs(lobs);
		return (R) response;
	}
	
	public R saveReRenderEligibiltyInAppian(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {

		debug("Request:in saveEligibiltyInAppian method:" + requestData);

		// removing control data
		requestData.remove("elementType");
		requestData.remove("operationType");
		String eligibilityUrl = CreateElementService.getEnvironment().getProperty("url.eligibility");

		List<Map<String, Object>> eligibilityData = (List<Map<String, Object>>) requestData.get("data").get(0);

		LinkedMultiValueMap<String, Object> data = new LinkedMultiValueMap<>();
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		data.add("operationType", "SAVERERENDERELIGIBILITY");
		data.add("clientId", session.getAttribute("clientId").toString());
		data.add("data", eligibilityData);
		data = DSCIUtility.updateRequestDataWithJWTToken(data);
		HttpEntity entity = getSaveHttpEntity(data);
		debug("Connecting: Qualification Question HttpEntity: " + entity);

		ResponseEntity<String> response = restTemplate.postForEntity(eligibilityUrl, entity, String.class);

		// Save all the lobs with questions answered in session
		List<String> lobs = new ArrayList<String>();
		for (Map<String, Object> entry : eligibilityData) {
			lobs.add((String) entry.get("lob"));
		}

		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();
		sessionData.setLobs(lobs);
		return (R) response;
	}
}
