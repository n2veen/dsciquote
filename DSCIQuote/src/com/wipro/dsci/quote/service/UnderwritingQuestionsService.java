package com.wipro.dsci.quote.service;

import static com.wipro.dsci.quote.service.DSCIService.debug;
import static com.wipro.dsci.quote.service.DSCIService.getSaveHttpEntity;
import static com.wipro.dsci.quote.service.DSCIService.toJson;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wipro.dsci.quote.enums.Actions;
import com.wipro.dsci.quote.exception.DSCIQuoteException;
import com.wipro.dsci.quote.model.SessionData;
import com.wipro.dsci.quote.util.DSCIUtility;

import static com.wipro.dsci.quote.service.DSCIService.debug;
import static com.wipro.dsci.quote.service.DSCIService.getViewHttpEntity;
import static com.wipro.dsci.quote.service.DSCIService.toJson;
import static com.wipro.dsci.quote.service.DSCIService.getSaveHttpEntity;

@org.springframework.stereotype.Service("UnderwritingQuestionsService")
public class UnderwritingQuestionsService<R, E> implements DSCIService<R, DSCIQuoteException> {

	private static void debug(String format, Object... args) {
		Logger logger = LoggerFactory.getLogger("DSCIQuote_dblogger");
		if (logger.isDebugEnabled()) {
			logger.debug(format, args);
		}
	}

	@Autowired
	RestTemplate restTemplate;

	private Map<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>> opTypeMap = new HashMap<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>>() {

		private static final long serialVersionUID = 1L;
		{
			put(Actions.VIEW_UNDERWRITING_QUESTIONS, (a, b) -> {
				return viewUnderwritingQuestions(a, b);
			});
			put(Actions.VIEW_BUILD_UNDERWRITING_QUESTIONS, (a, b) -> {
				return viewBuildUnderwritingQuestions(a, b);
			});
			put(Actions.SAVE_UW_QUESTIONS_IN_APPIAN, (a, b) -> {
				return saveUnderwritingQuestions(a, b);
			});

			put(Actions.SAVE_BUILD_UNDERWRITING_QUESTIONS, (a, b) -> {
				return saveBuildUnderwritingQuestions(a, b);
			});
			put(Actions.VIEW_BUILD_UNDERWRITING_CHILD, (a, b) -> {
				return viewBuildUnderwritingChild(a, b);
			});
			put(Actions.BR_CHILD_HAVE_ALL, (a, b) -> {
				return viewAppianRuleHaveAllD(a, b);
			});
			put(Actions.BR_WHICH_OF_FOLLOWING, (a, b) -> {
				return viewAppianWhichOfTheD(a, b);
			});
			put(Actions.BR_APPIAN_IS_MORE, (a, b) -> {
				return viewAppianIsMoreThanD(a, b);
			});
			put(Actions.BR_APPIAN_ANNUAL_SALES, (a, b) -> {
				return viewAppianAnnualSales(a, b);
			});
				
			put(Actions.VIEW_BUILD_UNDERWRITING_ANNUAL, (a, b) -> {
				return calculateBuildingAnnualSales(a, b);
			});
			
			
			
			
			

		}
	};

	@Override
	public R delegate(Actions actions, LinkedMultiValueMap<String, Object> requestData) throws RuntimeException {

		// null check and getting operationType
		String operationType = requestData.containsKey("operationType")
				? (String) requestData.get("operationType").get(0) : "";
		System.out.println("operationType is " + operationType);

		if (!opTypeMap.containsKey(Actions.valueOf(operationType.toUpperCase()))) {
			throw new IllegalArgumentException(String.format("%s is not supported", actions));
		}
		return opTypeMap.get(actions).apply(restTemplate, requestData);
	}
	
	
	//Appain rule d
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R viewAppianAnnualSales(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws RuntimeException {

              debug("Request:in VIEW APPIAN  Annaul sales  method:" + requestData);

		

		// removing control data
		requestData.remove("elementType");
		requestData.remove("operationType");
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();

		System.out.println("Before List");
	
		String  value	= ((String) ((Map)(requestData.get("data").get(0))).get("BuildingInput.UNIG_BuildingAnnualSales").toString() );	
		String userval = value.replaceAll("[$,]|\\s", "");
		
		
		
		ObjectMapper mapper = new ObjectMapper();
		HashMap<String, Object> final_response = new HashMap<>();
		
		HttpEntity httpEntity = getViewHttpEntity();
		

		System.out.println("Apian call started");
	    String fetchurl = CreateElementService.getEnvironment().getProperty("url.uw.rule.anualsale");
		fetchurl = fetchurl + "?var1=" + userval;
		
		ResponseEntity<String> response = restTemplate.exchange(fetchurl, HttpMethod.GET, httpEntity, String.class);

		System.out.println(response.getBody());
		
		

	
	
		
		debug("Connecting: VIEW APPAIN ANNUAL SALES: " + response);

		

		return (R) response;
	}
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R viewAppianIsMoreThanD(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws RuntimeException {

     debug("Request:in VIEW APPIAN  page  method:" + requestData);

		

		// removing control data
		requestData.remove("elementType");
		requestData.remove("operationType");
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();

		System.out.println("Before List");
        //  String  value	= ((String) ((Map)(requestData.get("data").get(0))).get("UNIG_BuildingUndQuestionsInput") );
		String  value	= ((String) ((Map)(requestData.get("data").get(0))).get("UNIG_BuildingUndQuestionsInput.VacantOrUnoccupiedBuilding") );
		
		
		System.out.println(value);
				
		
		ObjectMapper mapper = new ObjectMapper();
		HashMap<String, Object> final_response = new HashMap<>();
		
		HttpEntity httpEntity = getViewHttpEntity();
		

		System.out.println("Apian call started");
		String fetchurl = CreateElementService.getEnvironment().getProperty("url.uw.rule.ismore");

         fetchurl = fetchurl + "?var1=" + value;


		
		ResponseEntity<String> response = restTemplate.exchange(fetchurl, HttpMethod.GET, httpEntity, String.class);

		System.out.println(response.getBody());
		
	

	
	
		
		debug("Connecting: VIEW APPAIN HAVE ALL: " + response);

		

		return (R) response;
	}
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R viewAppianWhichOfTheD(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws RuntimeException {

		debug("Request:Which of the following Been  :" + requestData);
		System.out.println("Request:Which of the following Been  :" + requestData);
		// removing control data
		requestData.remove("elementType");
		requestData.remove("operationType");
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();

		   System.out.println("Before List");
          
             
             Boolean  value1	= ((Boolean) ((Map)(requestData.get("data").get(0))).get("Electrical") );
             Boolean  value2	= ((Boolean) ((Map)(requestData.get("data").get(0))).get("Plumbing") );
             Boolean  value3	= ((Boolean) ((Map)(requestData.get("data").get(0))).get("Roof") );
             Boolean  value4	= ((Boolean) ((Map)(requestData.get("data").get(0))).get("Furnace") );
             
		   System.out.println("value1"+value1); 
		   System.out.println("value2"+value2); 
		   System.out.println("value3"+value3);
		   System.out.println("value4"+value4);
				
		
		ObjectMapper mapper = new ObjectMapper();
		HashMap<String, Object> final_response = new HashMap<>();
		
		HttpEntity httpEntity = getViewHttpEntity();
		

		System.out.println("Apian call started");
		String fetchurl = CreateElementService.getEnvironment().getProperty("url.uw.rule.whichof");

	   fetchurl = fetchurl + "?var1="+value1+"&var2="+value2+"&var3="+value3+"&var4="+value4;
		
			
			ResponseEntity<String> response = restTemplate.exchange(fetchurl, HttpMethod.GET, httpEntity, String.class);

			System.out.println(response.getBody());
			
			debug("Connecting: VIEW APPAIN WHICh OF: " + response);

			

			return (R) response;
	}

	
	// Have All child
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R viewAppianRuleHaveAllD(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws RuntimeException {
		ObjectMapper mapper = new ObjectMapper();
		
		debug("Request:in VIEW APPIAN  page  method:" + requestData);



		// removing control data
		requestData.remove("elementType");
		requestData.remove("operationType");
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();

		System.out.println("Before List");
        String value = ((String) ((Map)(requestData.get("data").get(0))).get("Have_all_of_the_following_been") );	
		
		
		System.out.println(value);
				
		
		/*  avoid appian call to mono*/
		
		
	
		
		
		LinkedMultiValueMap<String, Object> BuildData = new LinkedMultiValueMap<String, Object>();

		BuildData.add("operationType", "VIEW_BUILD_HAVE_CHILD");
		BuildData.add("value", value);
		BuildData = DSCIUtility.updateRequestDataWithJWTToken(BuildData);

		HttpEntity save_entity = getSaveHttpEntity(BuildData);

		ResponseEntity<String> response = restTemplate.exchange(
				ViewElementService.getEnvironment().getProperty("url.underwriting"), HttpMethod.POST, save_entity,
				String.class);
		
		System.out.println(response);
		return (R) response;
	}

	
	
	
	
	
	//
	

	/**
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return
	 * @throws RuntimeException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R viewUnderwritingQuestions(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws RuntimeException {
		debug("Request:in viewUnderwritingQuestions method:" + requestData);
		System.out.println("Inside dsci view underwriting ");
		requestData.remove("elementType");
		requestData.remove("operationType");

		HttpSession session = (HttpSession) requestData.get("session").get(0);
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();

		System.out.println("sessionData-------" + sessionData);

		LinkedMultiValueMap<String, Object> data_get = new LinkedMultiValueMap<String, Object>();

		String policyId = (String) session.getAttribute("Id");
		

		data_get.add("operationType", "VIEW_UNDERWRITING_QUESTIONS");
		data_get.add("policyId", policyId);

		String url = ViewElementService.getEnvironment().getProperty("url.getQuestion");

		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}
		System.out.println("Policy ID id id" + policyId);
		String QuoteSection = "10";

		url += "?quote_section=" + QuoteSection + "&policy_id=" + policyId;

		// converting URL to URI
		URI uri = DSCIService.toURI(url);

		data_get = DSCIUtility.updateRequestDataWithJWTToken(data_get);

		HttpEntity save_entity = getSaveHttpEntity(data_get);

		
		ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.POST, save_entity, String.class);
		String[] separatedResponse = response.getBody().split("##SessionData##");
		String[] separatedSessionAndTransactionalResponse = separatedResponse[0].split("--MidPayload--");
		if (separatedSessionAndTransactionalResponse.length > 1) {
			return (R) new ResponseEntity<String>(separatedSessionAndTransactionalResponse[1], HttpStatus.OK);
		} else {
			return (R) restTemplate.exchange(ViewElementService.getEnvironment().getProperty("url.underwriting"),
					HttpMethod.POST, save_entity, String.class);
		}

	}

	/**
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return
	 * @throws RuntimeException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R viewBuildUnderwritingQuestions(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws RuntimeException {
		debug("Request:in viewBuild UnderwritingQuestions method:" + requestData);
		// requestData.remove("elementType");
		// requestData.remove("operationType");
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();
		System.out.println("Before rowID n");
		String rowId = requestData.get("buildingId").get(0).toString();
		String locationId = requestData.get("locationId").get(0).toString();
		if (rowId == null) {
			String message = "No rowId ID found in Building Underwriting  Service";
			throw new DSCIQuoteException(message);
		}
		System.out.println("row ID is " + rowId);

		String policyId = (String) session.getAttribute("Id");
		//get prdId from Sessiondata for making P&C master data async call
		Integer productId = Integer.parseInt(sessionData.getProductId());
		
		System.out.println("policy ID is :" + policyId);
		if (policyId == null) {
			String message = "No policy ID found in Building Underwriting  Service";
			throw new DSCIQuoteException(message);
		}

		System.out.println("sessionData-------" + sessionData);

		LinkedMultiValueMap<String, Object> BuildData = new LinkedMultiValueMap<String, Object>();

		BuildData.add("operationType", "VIEW_BUILD_UNDERWRITING_QUESTIONS");
		BuildData.add("policyId", policyId);
		BuildData.add("rowId", rowId);
		BuildData.add("child", rowId);
		//productId for making Async call to P&C page master data from DSCI DB
		BuildData.add("productId", productId);
		BuildData.add("locationId",locationId);
		//

		String url = ViewElementService.getEnvironment().getProperty("url.getQuestion");

		String QuoteSection = "9";

		url += "?quote_section=" + QuoteSection + "&policy_id=" + policyId;

		// converting URL to URI
		URI uri = DSCIService.toURI(url);

		BuildData = DSCIUtility.updateRequestDataWithJWTToken(BuildData);

		HttpEntity save_entity = getSaveHttpEntity(BuildData);

		ResponseEntity<String> response = restTemplate.exchange(
				ViewElementService.getEnvironment().getProperty("url.underwriting"), HttpMethod.POST, save_entity,
				String.class);
		return (R) response;

	}
	
	//
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R calculateBuildingAnnualSales(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws RuntimeException {
		debug("Request:in viewBuild AnnaulSales method:" + requestData);
		// requestData.remove("elementType");
		// requestData.remove("operationType");
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();
		

		String policyId = (String) session.getAttribute("Id");
		
		System.out.println("policy ID is :" + policyId);
		if (policyId == null) {
			String message = "No policy ID found in Building Underwriting  Service";
			throw new DSCIQuoteException(message);
		}

		System.out.println("sessionData-------" + sessionData);

		LinkedMultiValueMap<String, Object> BuildData = new LinkedMultiValueMap<String, Object>();

		BuildData.add("operationType", "VIEW_BUILD_UNDERWRITING_ANNUAL");
		BuildData.add("policyId", policyId);
	
	

	

		BuildData = DSCIUtility.updateRequestDataWithJWTToken(BuildData);

		HttpEntity save_entity = getSaveHttpEntity(BuildData);

		ResponseEntity<String> response = restTemplate.exchange(
				ViewElementService.getEnvironment().getProperty("url.underwriting"), HttpMethod.POST, save_entity,
				String.class);
		
		
		
		return (R) response;

	}
	
	//
	
	
	
	
	//

	/**
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return
	 * @throws RuntimeException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R viewBuildUnderwritingChild(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws RuntimeException {
		debug("Request:in VIEW BUILD CHILD   DSCI QUOTE method:" + requestData);

		System.out.println("Request:in VIEW BUILD CHILD page  DSCI QUOTE method:" + requestData);
		requestData.remove("elementType");
		requestData.remove("operationType");
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();

		String policyId = (String) session.getAttribute("Id");
		//String policyId = "5388598";
		System.out.println("inside DSCI Policy ID" + policyId);
		String underwritingUrl = CreateElementService.getEnvironment().getProperty("url.underwriting");
		System.out.println("underwriting URL is :" + underwritingUrl);

		System.out.println("before rowID");
		
		String locationId = ((Map<String, Object>) requestData.get("data").get(0)).get("locationId").toString();
		String rowId = ((Map<String, Object>) requestData.get("data").get(0)).get("buildingId").toString();
		System.out.println("row id inside save  building" + rowId);
		List building_underwriting = (List) ((Map<String, Object>) requestData.get("data").get(0))
				.get("BuildingQuestions");
		System.out.println(building_underwriting);

		LinkedMultiValueMap<String, Object> data = new LinkedMultiValueMap<>();

		data.add("operationType", "VIEW_BUILD_UNDERWRITING_CHILD");
		data.add("policyId", policyId);
		data.add("rowId", rowId);
		data.add("locationId", locationId);
		
		
		data.add("underwriting_questions", building_underwriting);
		data = DSCIUtility.updateRequestDataWithJWTToken(data);
		HttpEntity entity = getSaveHttpEntity(data);
		debug("Connecting: Underwriting  Question HttpEntity: " + entity);

		ResponseEntity<String> response = restTemplate.postForEntity(
				ViewElementService.getEnvironment().getProperty("url.underwriting"), entity, String.class);
		

		return (R) response;
	}

	//

	public LinkedHashMap<String, List<LinkedHashMap<String, Object>>> insertData(
			LinkedHashMap<String, List<LinkedHashMap<String, Object>>> res, String SystemRefId, String value) {

		List<LinkedHashMap<String, Object>> question = res.get("prior_policy").stream()
				.filter(p -> p.get("systemRefId").equals(SystemRefId)).collect(Collectors.toList());

		LinkedHashMap<String, Object> question_object = question.get(0);
		question_object.put("userValue", value);
		List<LinkedHashMap<String, Object>> underwritingquestions = res.get("prior_policy");
		res.remove(underwritingquestions);
		underwritingquestions.removeIf(p -> p.get("systemRefId").equals(SystemRefId));
		underwritingquestions.add(question_object);
		res.put("prior_policy", underwritingquestions);
		return res;
	}

	/**
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return
	 * @throws RuntimeException
	 */

	//

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R saveUnderwritingQuestions(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws RuntimeException {

		debug("Request:in saveUnderwritingQuestions page  method:" + requestData);

		

		// removing control data
		requestData.remove("elementType");
		requestData.remove("operationType");
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();

		String policyId = (String) session.getAttribute("Id");
		String QuoteSection = "10";
		
		String saveURL = CreateElementService.getEnvironment().getProperty("url.saveQuoteDetails");
		saveURL += "?policy_id=" + policyId + "&quote_section=" + QuoteSection;
		
		if (requestData.containsKey("saveAndExit")) {
			session.invalidate();
			saveURL += "&action=SE";
			URI uri = DSCIService.toURI(saveURL);
			String dataToSave = toJson(requestData.get("data").get(0)) + "##SessionDetails##" + toJson(sessionData);
			return (R) restTemplate.exchange(uri, HttpMethod.POST, getSaveHttpEntity(dataToSave), String.class);
		} else {
			saveURL += "&action=SC";
		}
		//		String policyId="5388598";
		String PriorLosses=(String) sessionData.getPriorLosses();
		
		String bankrupcy =(String) sessionData.getBankruptcyCount();
		String liensCount=(String) sessionData.getLiensCount();
		
		
		System.out.println("bankrupcy"+bankrupcy);
		System.out.println("inside DSCI Policy ID" + policyId);

		System.out.println("Before List");
		List underwriting_questions = (List) ((Map<String, Object>) requestData.get("data").get(0))
				.get("underwriting_questions");
		List prior_policy = (List) ((Map<String, Object>) requestData.get("data").get(0)).get("prior_policy");
		System.out.println("before rowID");
		String rowId = ((Map<String, Object>) requestData.get("data").get(0)).get("rowId").toString();
		System.out.println("rowid is" + rowId);
		System.out.println("request data is underwriting " + underwriting_questions);
		System.out.println("request data is prior_policy " + prior_policy);
		LinkedMultiValueMap<String, Object> data = new LinkedMultiValueMap<>();

		data.add("operationType", "SAVE_UNDERWRITING_QUESTIONS");
		data.add("policyId", policyId);
		// data.add("sessionId",session.getAttribute("dcSessionId").toString());
		data.add("underwriting_questions", underwriting_questions);
		data.add("prior_policy", prior_policy);
		data.add("rowId", rowId);
		data.add("priorLosses", PriorLosses);
          if(sessionData.getBankruptcyCount()!=null){
	       data.add("bankrupcyCount", bankrupcy);
		}
          if(sessionData.getLiensCount()!=null){
	data.add("liensCount", liensCount);
		}
	
		
		data = DSCIUtility.updateRequestDataWithJWTToken(data);
		HttpEntity entity = getSaveHttpEntity(data);
		debug("Connecting: Underwriting  Question HttpEntity: " + entity);
		ResponseEntity<String> response = restTemplate.postForEntity(
				ViewElementService.getEnvironment().getProperty("url.underwriting"), entity, String.class);
		
		// Save Data in appian
		String dataToSave = toJson(requestData.get("data").get(0)) + "##SessionDetails##" + toJson(sessionData);
		restTemplate.exchange(DSCIService.toURI(saveURL), HttpMethod.POST, getSaveHttpEntity(dataToSave), String.class);

		return (R) response;
	}

	//
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R saveBuildUnderwritingQuestions(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws RuntimeException {

		debug("Request:in saveUnderwritingQuestions page  DSCI QUOTE method:" + requestData);

		System.out.println("Request:in saveUnderwritingQuestions page  DSCI QUOTE method:" + requestData);
		requestData.remove("elementType");
		requestData.remove("operationType");
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData") : new SessionData();

		String policyId = (String) session.getAttribute("Id");
		//String policyId = "5388598";
		System.out.println("inside DSCI Policy ID" + policyId);
		String underwritingUrl = CreateElementService.getEnvironment().getProperty("url.underwriting");
		System.out.println("underwriting URL is :" + underwritingUrl);

		System.out.println("before rowID");
		String rowId = ((Map<String, Object>) requestData.get("data").get(0)).get("buildingId").toString();
		System.out.println("row id inside save  building" + rowId);
		List building_underwriting = (List) ((Map<String, Object>) requestData.get("data").get(0))
				.get("BuildingQuestions");
		Object building_underwriting_questions = requestData.containsKey("data") ? requestData.get("data").get(0) : "";
		System.out.println("request data is " + building_underwriting_questions);
		HashMap<String, Object> buildingData = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			buildingData = mapper.readValue(toJson(building_underwriting_questions), HashMap.class);
		} catch (IOException e1) {
			throw new DSCIQuoteException(e1);
		}

		System.out.println("building  data inside dsci  is " + buildingData);
		LinkedMultiValueMap<String, Object> data = new LinkedMultiValueMap<>();

		data.add("operationType", "SAVE_BUILD_UNDERWRITING_QUESTIONS");
		data.add("policyId", policyId);
		data.add("rowId", rowId);
		
		data.add("underwriting_questions", building_underwriting);
		data = DSCIUtility.updateRequestDataWithJWTToken(data);
		HttpEntity entity = getSaveHttpEntity(data);
		debug("Connecting: Underwriting  Question HttpEntity: " + entity);

		ResponseEntity<String> response = restTemplate.postForEntity(
				ViewElementService.getEnvironment().getProperty("url.underwriting"), entity, String.class);
		// ResponseEntity<String> response =restTemplate.postForEntity(
		// "http://localhost:9102/underwriting-questions/",entity,String.class);

		return (R) response;
	}
	//

	public RestTemplate getTemplate() {
		return restTemplate;
	}

}
