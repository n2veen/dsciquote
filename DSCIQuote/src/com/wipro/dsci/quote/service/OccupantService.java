package com.wipro.dsci.quote.service;

import static com.wipro.dsci.quote.service.DSCIService.debug;
import static com.wipro.dsci.quote.service.DSCIService.toJson;
import static com.wipro.dsci.quote.util.DSCIUtility.updateRequestMapDataWithJWTToken;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wipro.dsci.quote.enums.Actions;
import com.wipro.dsci.quote.exception.DSCIQuoteException;

@org.springframework.stereotype.Service("OccupantService")
public class OccupantService<R, E> implements DSCIService<R, DSCIQuoteException> {

	RestTemplate restTemplate = new RestTemplate();

	private Map<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>> opTypeMap = new HashMap<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>>() {

		private static final long serialVersionUID = 1L;
		{
			put(Actions.VIEW_OCCUPANT, (a, b) -> {
				return viewOccupant(a, b);
			});
			put(Actions.SAVE_OCCUPANT, (a, b) -> {
				return saveOccupant(a, b);
			});
			put(Actions.OCCUPANT_BUSINESSCLASSLOOKUP, (a, b) -> {
				return occupantBusinessClassLookup(a, b);
			});
			put(Actions.ENABLE_EDIT_OCCUPANT, (a, b) -> {
				return enableEditOccupant(a, b);
			});
			put(Actions.DELETEOCCUPANT, (a, b) -> {
				return deleteOccupant(a, b);
			});
		}
	};

	@Override
	public R delegate(Actions actions, LinkedMultiValueMap<String, Object> requestData) {

		// null check and getting operationType
		String operationType = requestData.containsKey("operationType")
				? (String) requestData.get("operationType").get(0)
				: "";

		if (!opTypeMap.containsKey(Actions.valueOf(operationType.toUpperCase()))) {
			throw new IllegalArgumentException(String.format("%s is not supported", actions));
		}
		return opTypeMap.get(actions).apply(restTemplate, requestData);
	}

	/**
	 * To get the occupant business class
	 * 
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return
	 */

	protected R occupantBusinessClassLookup(RestTemplate restTemplate,
			LinkedMultiValueMap<String, Object> requestData) {

		debug("Request:in enableEditOccupant method:" + requestData);

		HttpSession session = (HttpSession) requestData.get("session").get(0);

		String policyId = (String) session.getAttribute("Id");
		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}

		HashMap<String, Object> req = new HashMap<>();
		req.put("elementType", "OCCUPANT");
		req.put("operationType", "OCCUPANT_BUSINESSCLASSLOOKUP");
		req.put("policyId", policyId);

		ResponseEntity<String> businessClassLookupResponse = restTemplate.postForEntity(
				CreateElementService.getEnvironment().getProperty("url.location"),
				updateRequestMapDataWithJWTToken(req), String.class);

		debug("Response:in occupantBusinessClassLookup method:" + businessClassLookupResponse);

		ObjectMapper mapper = new ObjectMapper();
		List<HashMap<String, Object>> resMap = null;
		try {
			resMap = mapper.readValue(businessClassLookupResponse.getBody(), List.class);
		} catch (IOException e) {
			throw new DSCIQuoteException();
		}

		if (businessClassLookupResponse.getStatusCodeValue() == 200) {
			return (R) new ResponseEntity<>(toJson(resMap), HttpStatus.OK);
		} else {
			throw new DSCIQuoteException("occupantBusinessClassLookup is not fetched");
		}

	}

	/**
	 * To get occupantId
	 * 
	 * this method performs the below operations saves the default building data and
	 * primary occupant in dsci db saves the default building data in dc generates
	 * the occupant id in dc save current occupant data in dsci db
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R viewOccupant(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {
		debug("Request:in viewOccupant  method:" + requestData);

		HttpSession session = (HttpSession) requestData.get("session").get(0);

		String policyId = (String) session.getAttribute("Id");
		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}

		HashMap<String, Object> requestData1 = new HashMap<>();
		requestData1.put("elementType", "OCCUPANT");
		requestData1.put("operationType", "VIEW_OCCUPANT");
		requestData1.put("locBldgParam", "VIEW_OCCUPANT");
		requestData1.put("policyId", policyId);
		//requestData1.put("occupantIndex", requestData.get("occupantIndex").get(0).toString());
		//requestData1.put("firstOccupant",requestData.get("firstOccupant").get(0).toString());
		requestData1.put("buildingId",requestData.get("buildingId").get(0).toString());
		
		System.out.println("request data " + requestData1);
	
		ResponseEntity<String> response = restTemplate.postForEntity(
				CreateElementService.getEnvironment().getProperty("url.location"),
				updateRequestMapDataWithJWTToken(requestData1), String.class);

		ObjectMapper mapper = new ObjectMapper();
		HashMap<String, Object> resMap = null;
		try {
			resMap = mapper.readValue(response.getBody(), HashMap.class);
		} catch (IOException e) {
			throw new DSCIQuoteException();
		}

		debug("Response:in viewOccupant method:" + toJson(resMap));
		return (R) new ResponseEntity<String>(toJson(resMap), HttpStatus.OK);
	}

	/**
	 * This method saves the occupant in dc and edit the occupant data in dsci db
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return
	 * @throws DSCIQuoteException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R saveOccupant(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws DSCIQuoteException {
		debug("Request:in saveOccupant method:" + requestData);

		HttpSession session = (HttpSession) requestData.get("session").get(0);

		String policyId = (String) session.getAttribute("Id");// 5390218
		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}

		// null check and getting data for saving
		Object data = requestData.containsKey("data") ? requestData.get("data").get(0) : "";

		ObjectMapper mapper = new ObjectMapper();

		HashMap<String, Object> occupantData = null;

		try {
			occupantData = mapper.readValue(toJson(data), HashMap.class);
		} catch (IOException e1) {
			throw new DSCIQuoteException(e1);
		}

		HashMap<String, Object> req = new HashMap<>();
		req.put("elementType", "OCCUPANT");
		req.put("operationType", "SAVE_OCCUPANT");
		req.put("locBldgParam", "saveOccupant");
		req.put("policyId", policyId);
		req.put("buildingId", requestData.get("buildingId").get(0).toString());
		req.put("occupantId", requestData.get("occupantId").get(0).toString());
		req.put("occupantData", occupantData.get("occupantQuestions"));
		ResponseEntity<String> saveResponseInDb = restTemplate.postForEntity(CreateElementService.getEnvironment().getProperty("url.location"),
				updateRequestMapDataWithJWTToken(req), String.class);

		debug("Response:in saveOccupant method:" + saveResponseInDb);

		if (saveResponseInDb.getStatusCodeValue() == 200) {
			return (R) new ResponseEntity<>(toJson(occupantData), HttpStatus.OK);
		} else {
			throw new DSCIQuoteException("Occupant is not saved properly");
		}

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R enableEditOccupant(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws DSCIQuoteException {
		debug("Request:in enableEditOccupant method:" + requestData);

		HttpSession session = (HttpSession) requestData.get("session").get(0);

		String policyId = (String) session.getAttribute("Id");
		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}

		HashMap<String, Object> req = new HashMap<>();
		req.put("elementType", "OCCUPANT");
		req.put("operationType", "ENABLE_EDIT_OCCUPANT");
		req.put("locBldgParam", "ENABLE_EDIT_OCCUPANT");
		req.put("policyId", policyId);
		req.put("occupantId", requestData.get("occupantId").get(0).toString());
		ResponseEntity<String> enableEditResponse = restTemplate.postForEntity(
				CreateElementService.getEnvironment().getProperty("url.location"),
				updateRequestMapDataWithJWTToken(req), String.class);

		debug("Response:in enableEditOccupant method:" + enableEditResponse);

		ObjectMapper mapper = new ObjectMapper();
		List<HashMap<String, Object>> resMap = null;
		try {
			resMap = mapper.readValue(enableEditResponse.getBody(), List.class);
		} catch (IOException e) {
			throw new DSCIQuoteException();
		}

		if (enableEditResponse.getStatusCodeValue() == 200) {
			return (R) new ResponseEntity<>(toJson(resMap), HttpStatus.OK);
		} else {
			throw new DSCIQuoteException("Editing of Occupant is not enable");
		}

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public R deleteOccupant(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws DSCIQuoteException {
		debug("Request:in deleteOccupant method:" + requestData);

		HttpSession session = (HttpSession) requestData.get("session").get(0);

		String policyId = (String) session.getAttribute("Id");
		if (policyId == null) {
			throw new DSCIQuoteException("ID not found");
		}

		HashMap<String, Object> req = new HashMap<>();
		req.put("elementType", "OCCUPANT");
		req.put("operationType", "DELETE_OCCUPANT");
		req.put("locBldgParam", "DELETE_OCCUPANT");
		req.put("policyId", policyId);
		req.put("occupantId", requestData.get("occupantId").get(0).toString());
		ResponseEntity<String> deleteResponseInDb = restTemplate.postForEntity(
				CreateElementService.getEnvironment().getProperty("url.location"),
				updateRequestMapDataWithJWTToken(req), String.class);

		debug("Response:in deleteOccupant method:" + deleteResponseInDb);

		if (deleteResponseInDb.getStatusCodeValue() == 200) {
			return (R) new ResponseEntity<String>("{\"status\":\"Occupant deleted successfully\"}", HttpStatus.OK);
		} else {
			throw new DSCIQuoteException("Occupant is not deleted properly");
		}

	}
}
