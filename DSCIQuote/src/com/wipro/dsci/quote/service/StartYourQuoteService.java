package com.wipro.dsci.quote.service;

import static com.wipro.dsci.quote.service.DSCIService.debug;
import static com.wipro.dsci.quote.service.DSCIService.getSaveHttpEntity;
import static com.wipro.dsci.quote.service.DSCIService.getViewHttpEntity;
import static com.wipro.dsci.quote.service.DSCIService.toJson;

import java.net.URI;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.wipro.dsci.quote.enums.Actions;
import com.wipro.dsci.quote.exception.DSCIQuoteException;
import com.wipro.dsci.quote.model.SessionData;
import com.wipro.dsci.quote.util.DSCIUtility;

@org.springframework.stereotype.Service("StartYourQuoteService")
public class StartYourQuoteService<R, E> implements DSCIService<R, DSCIQuoteException> {

	@Autowired
	RestTemplate restTemplate;

	private Map<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>> opTypeMap = new HashMap<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>>() {

		private static final long serialVersionUID = 1L;
		{
			put(Actions.SAVESTARTYOURQUOTE, (a, b) -> {
				return saveStartYourQuote(a, b);
			});

			put(Actions.VIEWSTARTYOURQUOTE, (a, b) -> {
				return viewStartYourQuote(a, b);
			});
		}
	};

	@Override
	public R delegate(Actions actions, LinkedMultiValueMap<String, Object> requestData) throws DSCIQuoteException {

		// null check and getting operationType

		String operationType = requestData.containsKey("operationType")
				? (String) requestData.get("operationType").get(0)
				: "";

		if (!opTypeMap.containsKey(Actions.valueOf(operationType.toUpperCase()))) {
			throw new IllegalArgumentException(String.format("%s is not supported", actions));
		}
		return opTypeMap.get(actions).apply(restTemplate, requestData);
	}

	@SuppressWarnings({ "unchecked" })
	public R viewStartYourQuote(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws RuntimeException {
		debug("Request:in viewStartYourQuote method:" + requestData);

		ResponseEntity<String> response = null;
		String url = CreateElementService.getEnvironment().getProperty("url.startYourQuote");
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData")
				: new SessionData();
		session.setAttribute("QQAnswered", true);
		String policyId = (String)session.getAttribute("Id");
		if(policyId== null){
			policyId = UUID.randomUUID().toString();
		}
		if (session.getAttribute("quoteCreated") != null && (Boolean) session.getAttribute("quoteCreated")) {
			// fetch transaction data
			String QuoteSection = "5";
			url = CreateElementService.getEnvironment().getProperty("url.getQuestion") + "?quote_section="
					+ QuoteSection + "&policy_id=" + policyId + "&fetch_Master=No";
			URI uri = DSCIService.toURI(url);
			LinkedMultiValueMap<String, Object> requestMap = new LinkedMultiValueMap();
			requestMap.add("clientId", session.getAttribute("clientId"));
			requestMap.add("sessionData", sessionData);
			requestMap = DSCIUtility.updateRequestDataWithJWTToken(requestMap);
			response = restTemplate.exchange(uri, HttpMethod.POST, getSaveHttpEntity(requestMap), String.class);
		} else {
			// converting URL to URI
			URI uri = DSCIService.toURI(url);
			HashMap<String, Object> requestMap = new HashMap<>();
			requestMap.put("clientId", session.getAttribute("clientId"));
			requestMap.put("sessionData", sessionData);
			requestMap = DSCIUtility.updateRequestMapDataWithJWTToken(requestMap);
			response = restTemplate.exchange(uri, HttpMethod.POST, getSaveHttpEntity(requestMap), String.class);
		}

		// preparing header for rest call
		String[] separatedResponse = null;
		if (response.getBody().isEmpty()) {
			debug("Error: response is empty in viewBusinessDetails method.");
			String message = CreateElementService.getEnvironment().getProperty("response.null");
			throw new DSCIQuoteException(message);
		} else {
			separatedResponse = response.getBody().split("##SessionData##");
		}

		ResponseEntity<String> responseString;

		String[] separatedSessionAndTransactionalResponse = separatedResponse[0].split("--MidPayload--");
		if (separatedSessionAndTransactionalResponse.length > 1) {
			responseString = new ResponseEntity<String>(separatedSessionAndTransactionalResponse[1], HttpStatus.OK);
		} else {
			responseString = new ResponseEntity<String>(separatedSessionAndTransactionalResponse[0], HttpStatus.OK);
		}
		debug("Response in View Start your Quote:" + responseString);
		return (R) responseString;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public R saveStartYourQuote(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData) {

		debug("Request:in saveStartYourQuote method:" + requestData);
		String startYourQuoteUrl = CreateElementService.getEnvironment().getProperty("url.startYourQuote");
		// setting URL
		String appianSaveURL = CreateElementService.getEnvironment().getProperty("url.saveQuoteDetails");

		HttpSession session = (HttpSession) requestData.get("session").get(0);
		String policyId = session.getAttribute("Id").toString();
		String QuoteSection = "5";
		appianSaveURL += "?&policy_id=" + policyId + "&quote_section=" + QuoteSection;

		LinkedHashMap<String, List<LinkedHashMap>> extractData = (LinkedHashMap<String, List<LinkedHashMap>>) requestData
				.get("data").get(0);

		String state;
		String endDate, startDate;

		state = extractData.get("policyType").stream().filter(p -> p.get("datasetName").equals("PRIMARY RISK STATE"))
				.map(pm -> pm.get("userValue")).collect(Collectors.toList()).get(0).toString();

		endDate = extractData.get("policyType").stream().filter(p -> p.get("systemRefId").equals("policy_end_date"))
				.map(pm -> pm.get("userValue")).collect(Collectors.toList()).get(0).toString();

		startDate = extractData.get("policyType").stream()
				.filter(p -> p.get("systemRefId").equals("policy_effective_date")).map(pm -> pm.get("userValue"))
				.collect(Collectors.toList()).get(0).toString();

		Map<String, Object> pastPolicyInfo = (Map<String, Object>) extractData.get("pastPolicyInfo");
		String priorLosses = (String) pastPolicyInfo.get("userValue");

		Object data = requestData.containsKey("data") ? requestData.get("data").get(0) : "";

		SessionData sessionData = session.getAttribute("sessionData") != null
				? (SessionData) session.getAttribute("sessionData")
				: new SessionData();

		// setting primary risk state
		sessionData.setState(state);

		// setting effective end date and start date
		sessionData.setEffectiveEndDate(endDate);
		sessionData.setEffectiveStartDate(startDate);
		sessionData.setPriorLosses(priorLosses);
		session.setAttribute("startDate", startDate);
		session.setAttribute("endDate", endDate);
		session.setAttribute("state", state);
		session.setAttribute("sessionData", sessionData);
		// converting URL to URI
		URI uri = DSCIService.toURI(appianSaveURL);

		HashMap<String, Object> requestMap = new HashMap<String, Object>();

		requestMap.put("operationType", "SAVE");
		requestMap.put("clientId", session.getAttribute("clientId"));
		requestMap.put("sessionData", sessionData);
		requestMap.put("data", data);
		requestMap.put("policyId", session.getAttribute("quoteCreated") != null ? policyId : null);
		requestMap = DSCIUtility.updateRequestMapDataWithJWTToken(requestMap);

		// preparing header for rest call
		HttpEntity saveEntity = getSaveHttpEntity(requestMap);

		ParameterizedTypeReference<Map<String, String>> parameterizedTypeReference = new ParameterizedTypeReference<Map<String, String>>() {
		};
		// rest call
		ResponseEntity<Map<String, String>> response = restTemplate.exchange(startYourQuoteUrl, HttpMethod.PUT,
				saveEntity, parameterizedTypeReference);
		if (response.getBody().isEmpty()) {
			debug("Error: response is empty in saveStartYourQuote method.");
			String message = CreateElementService.getEnvironment().getProperty("response.null");
			throw new DSCIQuoteException(message);
		} else {
			Map<String, String> policy = response.getBody();
			// Save Data in Appian
			String dataToSave = toJson(data) + "##SessionDetails##" + toJson(sessionData);
			HttpEntity entity = getSaveHttpEntity(dataToSave);
			ResponseEntity<String> appianResponse = restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
			if (appianResponse.getBody().isEmpty()) {
				debug("Error: response is empty in saveBusinessDetailsInAppian method.");
				String message = CreateElementService.getEnvironment().getProperty("response.null");
				throw new DSCIQuoteException(message);
			}

			// update policy id in APPIAN
			if (session.getAttribute("quoteCreated") == null) {
				UriComponentsBuilder builder = UriComponentsBuilder.fromUri(
						DSCIService.toURI(CreateElementService.getEnvironment().getProperty("url.updatePolicyId")));
				builder.queryParam("policy_id", session.getAttribute("Id"));
				builder.queryParam("quote_id", policy.get("policyId"));
				// update policy Id in APPIAN
				restTemplate.exchange(builder.buildAndExpand(new HashMap<>()).toUri(), HttpMethod.POST,
						getViewHttpEntity(), String.class);
				// update policy id in session
				session.setAttribute("Id", policy.get("policyId"));
				sessionData.setProductId(policy.get("productId"));
				sessionData.setOperationCode(policy.get("operationCode"));
				session.setAttribute("quoteCreated", true);
			}
		}

		debug("Response:in saveStartYourQuote method:" + response);
		return (R) response;
	}
}
