package com.wipro.dsci.quote.service;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import static com.wipro.dsci.quote.util.DSCIUtility.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wipro.dsci.quote.enums.Actions;
import com.wipro.dsci.quote.exception.DSCIQuoteException;
import com.wipro.dsci.quote.model.SessionData;

@org.springframework.stereotype.Service("AccountDashboardService")
@SuppressWarnings({ "unchecked", "rawtypes" })
public class AccountDashboardService<R, E> implements DSCIService<R, DSCIQuoteException> {

	private static void debug(String format, Object... args) {
		Logger logger = LoggerFactory.getLogger("DSCIQuote_dblogger");
		if (logger.isDebugEnabled()) {
			logger.debug(format, args);
		}
	}
	
	public AccountDashboardService(){
		
	}
	
	private static Environment environment;
	
	@Autowired
	public AccountDashboardService(Environment environment) {
		AccountDashboardService.environment = environment;
	}
	
	@Autowired
	RestTemplate restTemplate;

	private Map<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>> opTypeMap = new HashMap<Actions, BiFunction<RestTemplate, LinkedMultiValueMap<String, Object>, R>>() {

		private static final long serialVersionUID = 1L;
		{
			put(Actions.VIEWACCOUNT, (a, b) -> {
				return viewAccount(a, b);
			});
			put(Actions.RESUMEQUOTE, (a, b) -> {
				return resumeQuote(a, b);
			});
			put(Actions.EDITACCOUNT, (a, b) -> {
				return editAccount(a, b);
			});
		}
	};

	@Override
	public R delegate(Actions actions, LinkedMultiValueMap<String, Object> requestData) throws RuntimeException {

		// null check and getting operationType
		String operationType = requestData.containsKey("operationType")
				? (String) requestData.get("operationType").get(0) : "";
		if (!opTypeMap.containsKey(Actions.valueOf(operationType.toUpperCase()))) {
			throw new IllegalArgumentException(String.format("%s is not supported", actions));
		}
		return opTypeMap.get(actions).apply(restTemplate, requestData);
	}

	/**
	 * To view accounts through DC wrapper integration
	 * @param restTemplate
	 * @param requestData
	 * @return
	 * @throws RuntimeException
	 */
	public R viewAccount(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws RuntimeException {
		debug("Request:in viewAccount  method:" + requestData);
		
		HttpSession session = (HttpSession) requestData.get("session").get(0);
		clearCache(session);
		
      
		// Adding for testing GIT access thru outside wipro network - Home network
		String accountURL = environment.getProperty("url.account");

		LinkedMultiValueMap<String, Object> requestValue =  new LinkedMultiValueMap<String, Object>();
		
		URI uri = DSCIService.toURI(accountURL);
		
		List<Object> valueList = new ArrayList<Object>();
		valueList.add("VIEWACCOUNT");

		requestValue.put("operationType", valueList);
		requestValue =	updateRequestDataWithJWTToken(requestValue);
		
		HttpEntity entity = DSCIService.getSaveHttpEntity(requestValue);

		ResponseEntity<String> accountResponse = restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
		if(accountResponse.getBody() != null && accountResponse.getBody().length() < 3000)
			debug("Response:in viewAccount method:" + accountResponse);
		return (R) accountResponse;
	}
	
	/**
	 * To view accounts through DC wrapper integration
	 * @param restTemplate
	 * @param requestData
	 * @return
	 * @throws RuntimeException
	 */
	public R resumeQuote(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws RuntimeException {
		debug("Request:in resumeQuote  method:" + requestData);
	
		String policy_id = (String) requestData.get("policy_id").get(0);
		String action = (String) requestData.get("action").get(0);

		String accountURL = environment.getProperty("url.resume") +"?policy_id=" + policy_id + "&action=" + action.replace(" ","_");

		URI uri = DSCIService.toURI(accountURL);
		HttpEntity entity = DSCIService.getSaveHttpEntity(new HashMap());
		
		String appState;
		String sessionData = null;

		ResponseEntity<String> accountResponse = restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);

		String appianResponse = accountResponse.getBody();
		String[] splitResponse = appianResponse.split("#####");

		if (splitResponse.length >= 2) {
			appState = splitResponse[0];
			sessionData = splitResponse[1];
		}
		else {
			appState = splitResponse[0];
		}

		SessionData sessionDataNew = new SessionData();
		ObjectMapper mapper1 = new ObjectMapper();
		try {
			sessionDataNew = mapper1.readValue(sessionData, SessionData.class);
		} catch (Exception e) {
			debug("Exception in mapping session data values :" + e.getMessage());
		}
		
		ServletRequestAttributes requestAttribute = (ServletRequestAttributes) RequestContextHolder
				.currentRequestAttributes();
		
		HttpSession session = requestAttribute.getRequest().getSession();
		session.setAttribute("sessionData", sessionDataNew);
		session.setAttribute("Id", policy_id);

		debug("Response:in resumeQuote method:" + accountResponse);

		return (R) new ResponseEntity(appState, HttpStatus.OK);
	
	}

	/* To edit account through DC wrapper integration
	 * 
	 * @param restTemplate
	 * @param requestData
	 * @return
	 * @throws RuntimeException
	 */
	public R editAccount(RestTemplate restTemplate, LinkedMultiValueMap<String, Object> requestData)
			throws RuntimeException {
		debug("Request:in editAccount  method:" + requestData);
		
		String clientId = (String) requestData.get("clientId").get(0);
		
		ServletRequestAttributes requestAttribute = (ServletRequestAttributes) RequestContextHolder
				.currentRequestAttributes();
		
		HttpSession session = requestAttribute.getRequest().getSession();
		session.setAttribute("clientId", clientId);
		
		String accountURL = environment.getProperty("url.account");
		
		LinkedMultiValueMap<String, Object> requestValue =  new LinkedMultiValueMap<String, Object>();
		List<Object> valueList = new ArrayList<Object>();
		valueList.add("EDITACCOUNT");
		List<Object> inputList = new ArrayList<Object>();
		inputList.add(clientId);
		
		requestValue.put("operationType", valueList);
		requestValue.put("clientId", inputList);
		
		URI uri = DSCIService.toURI(accountURL);
		
		requestValue =	updateRequestDataWithJWTToken(requestValue);
		HttpEntity entity = DSCIService.getSaveHttpEntity(requestValue);

		ResponseEntity<String> editResponse = restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
		debug("Response:in editAccount method:" + editResponse);
		return (R) editResponse;
	}
	
	/**
	 * @param restTemplate
	 * @param requestData
	 * @return
	 * @throws RuntimeException
	 */
	public RestTemplate getTemplate() {
		return restTemplate;
	}
	
	/**
	 * To clear Http session
	 * @param session
	 */
	private void clearCache(HttpSession session){		
		session.removeAttribute("sessionData");
		session.removeAttribute("Id");
		session.removeAttribute("clientId");
        session.removeAttribute("quoteCreated");	
        session.removeAttribute("QQAnswered");
	}

}
