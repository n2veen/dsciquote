package com.wipro.dsci.quote.model;

import java.io.Serializable;

public class RiskDatasetT implements Serializable {

    private static final long serialVersionUID = 1L;

   
    private Long id;

   
    private String riskdatasetName;


	private Integer riskID;

    private Integer typeSeqID;

   
    private Integer controlTypeID;

  
    private Integer valueSeqID;

    
    private Integer validationSeqID;

   
    private String navigationID;

   
    private Integer associationID;

   
    private String additionalInfo;

    
    private String defaultValue;
    
    
    private Integer parentDatasetID;
    
    public Integer getParentDatasetID() {
		return parentDatasetID;
	}
    
    public RiskDatasetT parentDatasetID(Integer parentDatasetID) {
    	this.parentDatasetID = parentDatasetID;
		return this;
	}
    
	public void setParentDatasetID(Integer parentDatasetID) {
		this.parentDatasetID = parentDatasetID;
	}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRiskdatasetName() {
        return riskdatasetName;
    }

    public RiskDatasetT riskdatasetName(String riskdatasetName) {
        this.riskdatasetName = riskdatasetName;
        return this;
    }

    public void setRiskdatasetName(String riskdatasetName) {
        this.riskdatasetName = riskdatasetName;
    }

    public Integer getRiskID() {
        return riskID;
    }

    public RiskDatasetT riskID(Integer riskID) {
        this.riskID = riskID;
        return this;
    }

    public void setRiskID(Integer riskID) {
        this.riskID = riskID;
    }

    public Integer getTypeSeqID() {
        return typeSeqID;
    }

    public RiskDatasetT typeSeqID(Integer typeSeqID) {
        this.typeSeqID = typeSeqID;
        return this;
    }

    public void setTypeSeqID(Integer typeSeqID) {
        this.typeSeqID = typeSeqID;
    }

    public Integer getControlTypeID() {
        return controlTypeID;
    }

    public RiskDatasetT controlTypeID(Integer controlTypeID) {
        this.controlTypeID = controlTypeID;
        return this;
    }

    public void setControlTypeID(Integer controlTypeID) {
        this.controlTypeID = controlTypeID;
    }

    public Integer getValueSeqID() {
        return valueSeqID;
    }

    public RiskDatasetT valueSeqID(Integer valueSeqID) {
        this.valueSeqID = valueSeqID;
        return this;
    }

    public void setValueSeqID(Integer valueSeqID) {
        this.valueSeqID = valueSeqID;
    }

    public Integer getValidationSeqID() {
        return validationSeqID;
    }

    public RiskDatasetT validationSeqID(Integer validationSeqID) {
        this.validationSeqID = validationSeqID;
        return this;
    }

    public void setValidationSeqID(Integer validationSeqID) {
        this.validationSeqID = validationSeqID;
    }

    public String getNavigationID() {
        return navigationID;
    }

    public RiskDatasetT navigationID(String navigationID) {
        this.navigationID = navigationID;
        return this;
    }

    public void setNavigationID(String navigationID) {
        this.navigationID = navigationID;
    }

    public Integer getAssociationID() {
        return associationID;
    }

    public RiskDatasetT associationID(Integer associationID) {
        this.associationID = associationID;
        return this;
    }

    public void setAssociationID(Integer associationID) {
        this.associationID = associationID;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public RiskDatasetT additionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
        return this;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public RiskDatasetT defaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
        return this;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }


    
    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((additionalInfo == null) ? 0 : additionalInfo.hashCode());
		result = prime * result + ((associationID == null) ? 0 : associationID.hashCode());
		result = prime * result + ((controlTypeID == null) ? 0 : controlTypeID.hashCode());
		result = prime * result + ((defaultValue == null) ? 0 : defaultValue.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((navigationID == null) ? 0 : navigationID.hashCode());
		result = prime * result + ((parentDatasetID == null) ? 0 : parentDatasetID.hashCode());
		result = prime * result + ((riskID == null) ? 0 : riskID.hashCode());
		result = prime * result + ((riskdatasetName == null) ? 0 : riskdatasetName.hashCode());
		result = prime * result + ((typeSeqID == null) ? 0 : typeSeqID.hashCode());
		result = prime * result + ((validationSeqID == null) ? 0 : validationSeqID.hashCode());
		result = prime * result + ((valueSeqID == null) ? 0 : valueSeqID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RiskDatasetT other = (RiskDatasetT) obj;
		if (additionalInfo == null) {
			if (other.additionalInfo != null)
				return false;
		} else if (!additionalInfo.equals(other.additionalInfo))
			return false;
		if (associationID == null) {
			if (other.associationID != null)
				return false;
		} else if (!associationID.equals(other.associationID))
			return false;
		if (controlTypeID == null) {
			if (other.controlTypeID != null)
				return false;
		} else if (!controlTypeID.equals(other.controlTypeID))
			return false;
		if (defaultValue == null) {
			if (other.defaultValue != null)
				return false;
		} else if (!defaultValue.equals(other.defaultValue))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (navigationID == null) {
			if (other.navigationID != null)
				return false;
		} else if (!navigationID.equals(other.navigationID))
			return false;
		if (parentDatasetID == null) {
			if (other.parentDatasetID != null)
				return false;
		} else if (!parentDatasetID.equals(other.parentDatasetID))
			return false;
		if (riskID == null) {
			if (other.riskID != null)
				return false;
		} else if (!riskID.equals(other.riskID))
			return false;
		if (riskdatasetName == null) {
			if (other.riskdatasetName != null)
				return false;
		} else if (!riskdatasetName.equals(other.riskdatasetName))
			return false;
		if (typeSeqID == null) {
			if (other.typeSeqID != null)
				return false;
		} else if (!typeSeqID.equals(other.typeSeqID))
			return false;
		if (validationSeqID == null) {
			if (other.validationSeqID != null)
				return false;
		} else if (!validationSeqID.equals(other.validationSeqID))
			return false;
		if (valueSeqID == null) {
			if (other.valueSeqID != null)
				return false;
		} else if (!valueSeqID.equals(other.valueSeqID))
			return false;
		return true;
	}
}
