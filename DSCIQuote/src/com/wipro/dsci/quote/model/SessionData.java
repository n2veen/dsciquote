package com.wipro.dsci.quote.model;


import java.util.HashMap;
import java.util.List;
import java.util.Map;



public class SessionData {

	private String businessName;
	private String businessClass;
	private String state;
	private String channel;
	private String lob;
	private String productId;
	private String operationCode;
	private BusinessAddress businessAddress;
	private BusinessInfo businessInfo;
	private String effectiveStartDate;
	private String effectiveEndDate;
	// key = systemrefId ,value=value of that question
	private List<String> lobs;
	private Map<String, Object> beforeYouBeginData;
	private List<HashMap<String, Object>> locationData;
	private String premiumValue;
	private Map<String,String> locationMap;
	private String subjectType;
	private String priorLosses;
	private String liensCount;
	private String bankruptcyCount;
	private HashMap<String,Object> stpDetails;
	private String quoteBindable;
	private String QuoteAmount;
	private String isQuoteEditable;
	private List<String> locationIds;

	public List<String> getLocationIds() {
		return locationIds;
	}

	public void setLocationIds(List<String> locationIds) {
		this.locationIds = locationIds;
	}

	public String getIsQuoteEditable() {
		return isQuoteEditable;
	}

	public void setIsQuoteEditable(String isQuoteEditable) {
		this.isQuoteEditable = isQuoteEditable;
	}

	public String getQuoteAmount() {
		return QuoteAmount;
	}

	public void setQuoteAmount(String quoteAmount) {
		QuoteAmount = quoteAmount;
	}

	public HashMap<String, Object> getStpDetails() {
		return stpDetails;
	}

	public void setStpDetails(HashMap<String, Object> stpDetails) {
		this.stpDetails = stpDetails;
	}

	public String getQuoteBindable() {
		return quoteBindable;
	}

	public void setQuoteBindable(String quoteBindable) {
		this.quoteBindable = quoteBindable;
	}

	public String getSubjectType() {
		return subjectType;
	}

	public void setSubjectType(String subjectType) {
		this.subjectType = subjectType;
	}

	/**
	 * 
	 * @return locationData
	 */
	
	public List<HashMap<String, Object>> getLocationData() {
		return locationData;
	}
    
	/**
     * 
     * @param locationData
     */
	public void setLocationData(List<HashMap<String, Object>> locationData) {
		this.locationData = locationData;
	}

	public Map<String, Object> getBeforeYouBeginData() {
		return beforeYouBeginData;
	}

	public void setBeforeYouBeginData(Map<String, Object> beforeYouBeginData) {
		this.beforeYouBeginData = beforeYouBeginData;
	}

	public String getBusinessName() {
		return businessName;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	
	/**
	 * @return the businessClass
	 */
	public String getBusinessClass() {
		return businessClass;
	}

	/**
	 * @param businessClass
	 *            the businessClass to set
	 */
	public void setBusinessClass(String businessClass) {
		this.businessClass = businessClass;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the channel
	 */
	public String getChannel() {
		return channel;
	}

	/**
	 * @param channel
	 *            the channel to set
	 */
	public void setChannel(String channel) {
		this.channel = channel;
	}

	/**
	 * @return the lob
	 */
	public String getLob() {
		return lob;
	}

	/**
	 * @param lob
	 *            the lob to set
	 */
	public void setLob(String lob) {
		this.lob = lob;
	}

	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * @param productId
	 *            the productId to set
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}
	

	public String getOperationCode() {
		return operationCode;
	}

	public void setOperationCode(String operationCode) {
		this.operationCode = operationCode;
	}

	/**
	 * @return the businessAddress
	 */
	public BusinessAddress getBusinessAddress() {
		return businessAddress;
	}

	/**
	 * @param businessAddress
	 *            the businessAddress to set
	 */
	public void setBusinessAddress(BusinessAddress businessAddress) {
		this.businessAddress = businessAddress;
	}

	/**
	 * @return the businessInfo
	 */
	public BusinessInfo getBusinessInfo() {
		return businessInfo;
	}

	/**
	 * @param businessInfo
	 *            the businessInfo to set
	 */
	public void setBusinessInfo(BusinessInfo businessInfo) {
		this.businessInfo = businessInfo;
	}

	/**
	 * @return the effectiveStartDate
	 */
	public String getEffectiveStartDate() {
		return effectiveStartDate;
	}

	/**
	 * @param effectiveStartDate
	 *            the effectiveStartDate to set
	 */
	public void setEffectiveStartDate(String effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}

	/**
	 * @return the effectiveEndDate
	 */
	public String getEffectiveEndDate() {
		return effectiveEndDate;
	}

	/**
	 * @param effectiveEndDate
	 *            the effectiveEndDate to set
	 */
	public void setEffectiveEndDate(String effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}

	public List<String> getLobs() {
		return lobs;
	}

	public void setLobs(List<String> lobs) {
		this.lobs = lobs;
	}

	public String getPremiumValue() {
		return premiumValue;
	}
	public void setPremiumValue(String premiumValue) {
		this.premiumValue = premiumValue;
	}
	
	
	public Map<String, String> getLocationMap() {
		return locationMap;
	}

	public void setLocationMap(Map<String, String> locationMap) {
		this.locationMap = locationMap;
	}

	public String getPriorLosses() {
		return priorLosses;
	}

	public void setPriorLosses(String priorLosses) {
		this.priorLosses = priorLosses;
	}
	
	public String getLiensCount() {
		return liensCount;
	}

	public void setLiensCount(String liensCount) {
		this.liensCount = liensCount;
	}

	public String getBankruptcyCount() {
		return bankruptcyCount;
	}

	public void setBankruptcyCount(String bankruptcyCount) {
		this.bankruptcyCount = bankruptcyCount;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((businessAddress == null) ? 0 : businessAddress.hashCode());
		result = prime * result + ((businessClass == null) ? 0 : businessClass.hashCode());
		result = prime * result + ((businessInfo == null) ? 0 : businessInfo.hashCode());
		result = prime * result + ((businessName == null) ? 0 : businessName.hashCode());
		result = prime * result + ((channel == null) ? 0 : channel.hashCode());
		result = prime * result + ((effectiveEndDate == null) ? 0 : effectiveEndDate.hashCode());
		result = prime * result + ((effectiveStartDate == null) ? 0 : effectiveStartDate.hashCode());
		result = prime * result + ((lob == null) ? 0 : lob.hashCode());
		result = prime * result + ((locationData == null) ? 0 : locationData.hashCode());
		result = prime * result + ((premiumValue == null) ? 0 : premiumValue.hashCode());
		result = prime * result + ((productId == null) ? 0 : productId.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SessionData other = (SessionData) obj;
		if (businessAddress == null) {
			if (other.businessAddress != null)
				return false;
		} else if (!businessAddress.equals(other.businessAddress))
			return false;
		if (businessClass == null) {
			if (other.businessClass != null)
				return false;
		} else if (!businessClass.equals(other.businessClass))
			return false;
		if (businessInfo == null) {
			if (other.businessInfo != null)
				return false;
		} else if (!businessInfo.equals(other.businessInfo))
			return false;
		if (businessName == null) {
			if (other.businessName != null)
				return false;
		} else if (!businessName.equals(other.businessName))
			return false;
		if (channel == null) {
			if (other.channel != null)
				return false;
		} else if (!channel.equals(other.channel))
			return false;
		if (effectiveEndDate == null) {
			if (other.effectiveEndDate != null)
				return false;
		} else if (!effectiveEndDate.equals(other.effectiveEndDate))
			return false;
		if (effectiveStartDate == null) {
			if (other.effectiveStartDate != null)
				return false;
		} else if (!effectiveStartDate.equals(other.effectiveStartDate))
			return false;
		if (lob == null) {
			if (other.lob != null)
				return false;
		} else if (!lob.equals(other.lob))
			return false;
		if (locationData == null) {
			if (other.locationData != null)
				return false;
		} else if (!locationData.equals(other.locationData))
			return false;
		if (premiumValue == null) {
			if (other.premiumValue != null)
				return false;
		} else if (!premiumValue.equals(other.premiumValue))
			return false;
		if (productId == null) {
			if (other.productId != null)
				return false;
		} else if (!productId.equals(other.productId))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SessionData [businessClass=" + businessClass + ", state=" + state + ", channel=" + channel + ", lob="
				+ lob + ", productId=" + productId + ", businessAddress=" + businessAddress + ", businessInfo="
				+ businessInfo + ", effectiveStartDate=" + effectiveStartDate + ", effectiveEndDate=" + effectiveEndDate
				+ ", locationData=" + locationData + ", businessName=" + businessName
				+ ", premiumValue=" + premiumValue +"]";
	}

}