package com.wipro.dsci.quote.model;

import java.io.Serializable;

public class BCDatasetT implements Serializable {

    private static final long serialVersionUID = 1L;

    
   
    private Integer bcDatasetID;

    
    private Integer policyID;

 
    private String bcDatasetName;

 
    private Integer typeSeqID;

   
    private Integer controlTypeID;

   
    private Integer valueSeqID;

   
    private Integer validationSeqID;

   
    private Integer associationID;

   
    private String navigationID;

    
    private String additionalInfo;

    
    private String defaultValue;
    
   
    private Integer parentDatasetID;

    public Integer getParentDatasetID() {
		return parentDatasetID;
	}

    public BCDatasetT parentDatasetID(Integer parentDatasetID) {
        this.parentDatasetID = parentDatasetID;
        return this;
    }

	public void setParentDatasetID(Integer parentDatasetID) {
		this.parentDatasetID = parentDatasetID;
	}
   
    public Integer getPolicyID() {
        return policyID;
    }
    
    public BCDatasetT policyID(Integer policyID) {
        this.policyID = policyID;
        return this;
    }

    public void setPolicyID(Integer policyID) {
        this.policyID = policyID;
    }

    public Integer getBcDatasetID() {
        return bcDatasetID;
    }

    public BCDatasetT bcDatasetID(Integer bcDatasetID) {
        this.bcDatasetID = bcDatasetID;
        return this;
    }

    public void setBcDatasetID(Integer bcDatasetID) {
        this.bcDatasetID = bcDatasetID;
    }

    public String getBcDatasetName() {
        return bcDatasetName;
    }

    public BCDatasetT bcDatasetName(String bcDatasetName) {
        this.bcDatasetName = bcDatasetName;
        return this;
    }

    public void setBcDatasetName(String bcDatasetName) {
        this.bcDatasetName = bcDatasetName;
    }

    public Integer getTypeSeqID() {
        return typeSeqID;
    }

    public BCDatasetT typeSeqID(Integer typeSeqID) {
        this.typeSeqID = typeSeqID;
        return this;
    }

    public void setTypeSeqID(Integer typeSeqID) {
        this.typeSeqID = typeSeqID;
    }

    public Integer getControlTypeID() {
        return controlTypeID;
    }

    public BCDatasetT controlTypeID(Integer controlTypeID) {
        this.controlTypeID = controlTypeID;
        return this;
    }

    public void setControlTypeID(Integer controlTypeID) {
        this.controlTypeID = controlTypeID;
    }

    public Integer getValueSeqID() {
        return valueSeqID;
    }

    public BCDatasetT valueSeqID(Integer valueSeqID) {
        this.valueSeqID = valueSeqID;
        return this;
    }

    public void setValueSeqID(Integer valueSeqID) {
        this.valueSeqID = valueSeqID;
    }

    public Integer getValidationSeqID() {
        return validationSeqID;
    }

    public BCDatasetT validationSeqID(Integer validationSeqID) {
        this.validationSeqID = validationSeqID;
        return this;
    }

    public void setValidationSeqID(Integer validationSeqID) {
        this.validationSeqID = validationSeqID;
    }

    public Integer getAssociationID() {
        return associationID;
    }

    public BCDatasetT associationID(Integer associationID) {
        this.associationID = associationID;
        return this;
    }

    public void setAssociationID(Integer associationID) {
        this.associationID = associationID;
    }

    public String getNavigationID() {
        return navigationID;
    }

    public BCDatasetT navigationID(String navigationID) {
        this.navigationID = navigationID;
        return this;
    }

    public void setNavigationID(String navigationID) {
        this.navigationID = navigationID;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public BCDatasetT additionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
        return this;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public BCDatasetT defaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
        return this;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((additionalInfo == null) ? 0 : additionalInfo.hashCode());
		result = prime * result + ((associationID == null) ? 0 : associationID.hashCode());
		result = prime * result + ((bcDatasetID == null) ? 0 : bcDatasetID.hashCode());
		result = prime * result + ((bcDatasetName == null) ? 0 : bcDatasetName.hashCode());
		result = prime * result + ((controlTypeID == null) ? 0 : controlTypeID.hashCode());
		result = prime * result + ((defaultValue == null) ? 0 : defaultValue.hashCode());
		result = prime * result + ((navigationID == null) ? 0 : navigationID.hashCode());
		result = prime * result + ((parentDatasetID == null) ? 0 : parentDatasetID.hashCode());
		result = prime * result + ((policyID == null) ? 0 : policyID.hashCode());
		result = prime * result + ((typeSeqID == null) ? 0 : typeSeqID.hashCode());
		result = prime * result + ((validationSeqID == null) ? 0 : validationSeqID.hashCode());
		result = prime * result + ((valueSeqID == null) ? 0 : valueSeqID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BCDatasetT other = (BCDatasetT) obj;
		if (additionalInfo == null) {
			if (other.additionalInfo != null)
				return false;
		} else if (!additionalInfo.equals(other.additionalInfo))
			return false;
		if (associationID == null) {
			if (other.associationID != null)
				return false;
		} else if (!associationID.equals(other.associationID))
			return false;
		if (bcDatasetID == null) {
			if (other.bcDatasetID != null)
				return false;
		} else if (!bcDatasetID.equals(other.bcDatasetID))
			return false;
		if (bcDatasetName == null) {
			if (other.bcDatasetName != null)
				return false;
		} else if (!bcDatasetName.equals(other.bcDatasetName))
			return false;
		if (controlTypeID == null) {
			if (other.controlTypeID != null)
				return false;
		} else if (!controlTypeID.equals(other.controlTypeID))
			return false;
		if (defaultValue == null) {
			if (other.defaultValue != null)
				return false;
		} else if (!defaultValue.equals(other.defaultValue))
			return false;
		if (navigationID == null) {
			if (other.navigationID != null)
				return false;
		} else if (!navigationID.equals(other.navigationID))
			return false;
		if (parentDatasetID == null) {
			if (other.parentDatasetID != null)
				return false;
		} else if (!parentDatasetID.equals(other.parentDatasetID))
			return false;
		if (policyID == null) {
			if (other.policyID != null)
				return false;
		} else if (!policyID.equals(other.policyID))
			return false;
		if (typeSeqID == null) {
			if (other.typeSeqID != null)
				return false;
		} else if (!typeSeqID.equals(other.typeSeqID))
			return false;
		if (validationSeqID == null) {
			if (other.validationSeqID != null)
				return false;
		} else if (!validationSeqID.equals(other.validationSeqID))
			return false;
		if (valueSeqID == null) {
			if (other.valueSeqID != null)
				return false;
		} else if (!valueSeqID.equals(other.valueSeqID))
			return false;
		return true;
	}
    
    
    
}
