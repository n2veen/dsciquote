package com.wipro.dsci.quote.controller;

import java.io.IOException;
import java.security.Key;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wipro.dsci.quote.exception.DSCIQuoteException;
import com.wipro.dsci.quote.model.SessionData;
import com.wipro.dsci.quote.service.HandOffService;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Controller
public class HandOffController {

	static Logger log = LoggerFactory.getLogger(HandOffController.class.getName());

	@Autowired
	public HandOffService handoffService;

	@Autowired
	RestTemplate restTemplate;

	/*
	 * This method is to validate the incoming token from @YS or CE
	 * @see com.wipro.dsci.quote.controller.HandOffController#validator
	 */
	@CrossOrigin
	@RequestMapping(value = "/accountview", method = {RequestMethod.POST, RequestMethod.GET})
	public ModelAndView validator(HttpServletRequest request,
			@CookieValue(value = "Authorization", defaultValue = "") String jwtCookie) {

		log.info("Request:in HandOffController#validator :");
		String token = "";
		String action = "";
		String appState = "";
		HashMap<String, String> requestMap = new HashMap<String, String>();
		if (request.getParameter("Auth-Token") != null){
			token = request.getParameter("Auth-Token");
		} else if (request.getHeader(HttpHeaders.AUTHORIZATION) != null) {
			token = request.getHeader(HttpHeaders.AUTHORIZATION);
		} else if (jwtCookie != null) {
			token = "Bearer " + jwtCookie;
		}
		String tokenStatus = handoffService.validateToken(token);
		if (tokenStatus.equals("success")) {
			HttpSession session = request.getSession(true);
			session.setAttribute("JsonWebToken", token);

			String[] split_string = token.split(" ");
			token = split_string[1];

			HashMap<String, String> decodeMap = decodeJWTToken(token);
			String clientId = decodeMap.get("CLIENTID");
			String agentID = decodeMap.get("sub");
			String quoteId = decodeMap.get("QUOTEID");
			if (quoteId != null && quoteId.equalsIgnoreCase("")) {
				quoteId = null;
			}
			if (clientId != null && clientId.equalsIgnoreCase("")) {
				clientId = null;
			}
			if (clientId != null && quoteId == null) {
				action = "Create quote";
				requestMap.put("action", action);
				appState = handoffService.dsciLandingPage(restTemplate, requestMap);
				log.info("if true then land upon Start Your Quote Page :" + clientId);
				
				LinkedMultiValueMap<String, Object> requestData = new LinkedMultiValueMap<String, Object>();
				List<Object> valueList = new ArrayList<Object>();
				valueList.add(clientId);
				requestData.put("clientId", valueList);
				List<Object> tokenlist = new ArrayList<Object>();
				tokenlist.add(token);
				requestData.put("jwtToken", tokenlist);

				ResponseEntity<String> responseFromFetch = (ResponseEntity<String>) handoffService
						.fetchClientDetails(restTemplate, requestData);
				Map<String, Object> responseMapping = null;
				ObjectMapper mapper = new ObjectMapper();
				mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
				try {
					responseMapping = mapper.readValue((String) responseFromFetch.getBody(),
							new TypeReference<Map<String, Object>>() {
							});
				} catch (IOException exception) {
					throw new DSCIQuoteException("Json Conversion failed");
				}
				SessionData sessionData = new SessionData();
				sessionData.setBusinessClass((String) responseMapping.get("BusinessClass"));
				sessionData.setChannel("Independent_Agent");
				sessionData.setState((String) responseMapping.get("PrimaryRiskState"));
				session.setAttribute("businessClass", responseMapping.get("BusinessClass"));
				session.setAttribute("channel", "Independent_Agent");
				session.setAttribute("state", responseMapping.get("PrimaryRiskState"));
				log.info("POST PrimaryRiskState : " + responseMapping.get("PrimaryRiskState")); 
				session.setAttribute("clientId", clientId);
				session.setAttribute("agentID",agentID);
				session.setAttribute("sessionData", sessionData);
				log.info("if true then land upon Start Your Quote Page :" + clientId);
				return new ModelAndView("redirect:" + "/dashboard#!/quote");
			} else if (clientId != null && quoteId != null) {
				action = "Resume quote";
				requestMap.put("action", action);
				requestMap.put("policy_id", quoteId);
				session.setAttribute("Id", quoteId);
				session.setAttribute("clientId", clientId);
				session.setAttribute("agentID",agentID);
				appState = handoffService.dsciLandingPage(restTemplate, requestMap);
				String url = fetchUrlBasedOnState(appState);
				log.info("if true then land upon Last Accessed Page :" + quoteId);
				return new ModelAndView("redirect:/dashboard#!" + url);
			} else {
				session.setAttribute("agentID",agentID);
				log.info("if true then land upon Customer Search Page :" + clientId);
				return new ModelAndView("redirect:/dashboard");
			}
		}
		log.info("Exit :from HandOffController#validator:" + tokenStatus);
		return new ModelAndView("redirect:/login");
	} 
	
	/*
	 * Login Flow (Added for handoff )
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView showLogin(HttpServletRequest request, HttpServletResponse response) {
		log.info("Rest request inside Login");
		ModelAndView mav = new ModelAndView("login");
		return mav;
	}

	/*
	 * This method is used for user authentication
	 * @see com.wipro.dsci.quote.controller.HandOffController#login
	 */
	@RequestMapping(value = "/userAuthentication", method = RequestMethod.POST)
	public ModelAndView login(HttpServletRequest request, ModelMap model) {

		log.info("Request:in HandOffController#login:");

		String action = "";
		String appState = "";
		HashMap<String, String> requestMap = new HashMap<String, String>();

		String userName = request.getParameter("userName");
		String password = request.getParameter("password");
		HashMap<String, String> responseMap = new LinkedHashMap<String, String>();
		responseMap = handoffService.dcAuthentication(userName, password);
		log.info("response :in HandOffController#login:" + responseMap);
		if (responseMap.get("Status").equals("success")) {
			HttpSession session = request.getSession(true);
			session.setAttribute("JsonWebToken", "Bearer " + responseMap.get("JsonWebToken"));
			HashMap<String, String> decodeMap = decodeJWTToken(responseMap.get("JsonWebToken"));
			String clientId = decodeMap.get("CLIENTID");
			String quoteId = decodeMap.get("QUOTEID");
            String agentID = decodeMap.get("sub");
			if (quoteId != null && quoteId.equalsIgnoreCase("")) {
				quoteId = null;
			}
			if (clientId != null && clientId.equalsIgnoreCase("")) {
				clientId = null;
			}

			if (clientId != null && quoteId == null) {

				action = "Create quote";
				requestMap.put("action", action);
				appState = handoffService.dsciLandingPage(restTemplate, requestMap);

				LinkedMultiValueMap<String, Object> requestData = new LinkedMultiValueMap<String, Object>();
				List<Object> valueList = new ArrayList<Object>();
				valueList.add(clientId);
				requestData.put("clientId", valueList);
				List<Object> tokenlist = new ArrayList<Object>();
				tokenlist.add("Bearer " + responseMap.get("JsonWebToken"));
				requestData.put("jwtToken", tokenlist);
				ResponseEntity<Object> responseFromFetch = (ResponseEntity<Object>) handoffService
						.fetchClientDetails(restTemplate, requestData);
				Map<String, Object> responseMapping = null;
				ObjectMapper mapper = new ObjectMapper();
				mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
				try {
					responseMapping = mapper.readValue((String) responseFromFetch.getBody(),
							new TypeReference<Map<String, Object>>() {
							});
				} catch (IOException exception) {
					throw new DSCIQuoteException("Json Conversion failed");
				}

				SessionData sessionData = new SessionData();
				sessionData.setBusinessClass((String) responseMapping.get("BusinessClass"));
				sessionData.setChannel("Independent_Agent");
				sessionData.setState((String) responseMapping.get("PrimaryRiskState"));
				log.info("LOGIN PrimaryRiskState : " + responseMapping.get("PrimaryRiskState")); 
				session.setAttribute("businessClass", responseMapping.get("BusinessClass"));
				session.setAttribute("channel", "Independent_Agent");
				session.setAttribute("state", responseMapping.get("PrimaryRiskState"));
				session.setAttribute("clientId", clientId);
				session.setAttribute("agentID",agentID);
				session.setAttribute("sessionData", sessionData);

				return new ModelAndView("redirect:" + "/dashboard#!/quote");
			} else if (clientId != null && quoteId != null) {
				action = "Resume quote";
				requestMap.put("action", action);
				requestMap.put("policy_id", quoteId);
				session.setAttribute("Id", quoteId);
				session.setAttribute("clientId", clientId);
                session.setAttribute("agentID",agentID);
				appState = handoffService.dsciLandingPage(restTemplate, requestMap);
				String url = fetchUrlBasedOnState(appState);
				log.info("if true then land upon Last Accessed Page :" + quoteId);
				return new ModelAndView("redirect:/dashboard#!" + url);
			} else {
                session.setAttribute("agentID",agentID);
				log.info("if true then land upon Customer Search Page :" + clientId);
				return new ModelAndView("redirect:/dashboard");
			}
		}
		model.addAttribute("errorMsg", "Please enter valid credentials");
		return new ModelAndView("redirect:/login");
	}

	/*
	 * This method is used to decode token and fetch required values
	 * @see com.wipro.dsci.quote.controller.HandOffController#decodeJWTToken
	 */
	@SuppressWarnings("unchecked")
	private static HashMap<String, String> decodeJWTToken(String jwtToken) {
		log.info("Request:in HandOffController#decodeJWTToken :" + jwtToken);

		String[] split_string = jwtToken.split("\\.");

		String header = split_string[0];
		String payload = split_string[1];
		String signature = split_string[2];
		String jwtHeader = new String(Base64.getUrlDecoder().decode(header));
		log.info("JWT Header : " + jwtHeader);
		String jwtBody = new String(Base64.getUrlDecoder().decode(payload));
		log.info("JWT Payload : " + jwtBody);
		String jwtKey = new String(Base64.getUrlDecoder().decode(signature));
		log.info("JWT Signature : " + jwtKey);

		HashMap<String, String> payloadMap = new LinkedHashMap<String, String>();
		ObjectMapper mapper = new ObjectMapper();
		try {
			payloadMap = mapper.readValue(jwtBody, HashMap.class);
		} catch (Exception e) {
			log.info("Exception from HandOffController#decodeJWTToken :" + e.getMessage());
		}
		log.info("Exit from HandOffController#decodeJWTToken :" + payloadMap.size());
		return payloadMap;

	}

	/*
	 * This method is used to fetch corresponding url's to appState
	 * com.wipro.dsci.quote.controller.HandOffController#fetchUrlBasedOnState
	 */
	private static String fetchUrlBasedOnState(String appState) {

		String url = "/dashboard";
		if (appState != null) {
			switch (appState) {
			case "app.quote":
				url = "/quote";
				return url;
			case "app.location":
				url = "/location";
				return url;
			case "app.additionalinterest":
				url = "/additionalinterest";
				return url;
			case "app.underwriting":
				url = "/underwriting";
				return url;
			case "app.pricingandcoverage":
				url = "/pricingandcoverage";
				return url;
			case "app.premiumsummary":
				url = "/premiumsummary";
				return url;
			case "app.beforeyouissue":
				url = "/beforeyouissue";
				return url;
			case "app.refer":
				url = "/refer";
				return url;
			default:
				return url;
			}
		}
		return url;
	}

	/*
	 * This method is for Handoff Outbound calls
	 * @see com.wipro.dsci.quote.controller.HandOffController#createOutBoundCall
	 */
	@CrossOrigin
	@RequestMapping(value = "/outbound", method = RequestMethod.GET)
	public ModelAndView createOutBoundCall(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "landingPageInCE", required = false) String landingPageInCE, @RequestParam(value = "lob", required = false) String lob,@RequestParam(value = "quoteId", required = false) String quoteId) throws Throwable {

		log.info("Request in : createOutBoundCall : landingPageInCE : " + landingPageInCE + " lob : " +  lob + " quoteId : " + quoteId); 
		String ceURL = handoffService.getOutBoundURL();
		
		String token = (String) request.getSession().getAttribute("JsonWebToken");
		String clientId = (String) request.getSession().getAttribute("clientId"); 
		String qId = "";
		
		if (quoteId != null){
			qId = quoteId;
		}
		
		if (request.getSession().getAttribute("quoteCreated") != null && (Boolean) request.getSession().getAttribute("quoteCreated")) {
			 qId = (String) request.getSession().getAttribute("Id");
		}

		String tokenStatus = handoffService.validateToken(token);

		if (tokenStatus.equals("success") && landingPageInCE != null && !landingPageInCE.equalsIgnoreCase("")) {
			String jwtToken = createJWT(token, landingPageInCE, 0, lob,clientId, qId);
			log.info("OutBound token created : jwtToken : " + jwtToken ); 
			if (qId != null){
				String responseStatus = handoffService.updateLandingPageInCE(jwtToken, qId);
				log.info("responseStatus " + responseStatus + " qId " + qId ); 
			}
			ModelAndView mav = new ModelAndView("ceoutbound");
			mav.addObject("authToken", "Bearer " + jwtToken);
			mav.addObject("ceURL", ceURL);
			return mav;
		} else {
			return new ModelAndView("redirect:/login");
		}
	}

	/*
	 * This method is to get header from token
	 * @see com.wipro.dsci.quote.controller.HandOffController#getTokenHeader
	 */
	private static String getTokenHeader(String jwtToken) {
		String[] split_string = jwtToken.split("\\.");
		String header = split_string[0];
		String jwtHeader = new String(Base64.getUrlDecoder().decode(header));
		return jwtHeader;
	}

	/*
	 * This method is to get signature key from token
	 * @see com.wipro.dsci.quote.controller.HandOffController#getTokenSignatureKey
	 */
	private static String getTokenSignatureKey(String jwtToken) {
		String[] split_string = jwtToken.split("\\.");
		String signature = split_string[2];
		String jwtKey = new String(Base64.getUrlDecoder().decode(signature));
		return jwtKey;
	}

	/*
	 * This method to create Json web token
	 * @see com.wipro.dsci.quote.controller.HandOffController#createJWT
	 */
	private String createJWT(String token, String landingPageInCE, long ttlMillis, String lob, String clientId, String quoteId) {

		String[] split_string = token.split(" ");
		token = split_string[1];

		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

		long nowMillis = System.currentTimeMillis();
		Date now = new Date(nowMillis);
		HashMap<String, String> stringClaims = decodeJWTToken(token);
		Map<String, Object> claims = new HashMap<String, Object>();
		claims.putAll(stringClaims);
		String jwtHeader = getTokenHeader(token);
		String jwtSignature = "1fTiS2clmPTUlNcpwYzd5i4AEFJ2DEsd8TcUsllmaKQ=";

		byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(jwtSignature);
		Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
		Map<String, Object> outboundData = handoffService.getOutBoundData(landingPageInCE, lob);

		String page = (String) outboundData.get("page");
		String pageset = (String) outboundData.get("pageset");
		String targetPage = (String) outboundData.get("targetPage"); 
		String manuscriptID = (String) outboundData.get("manuscript");
	
		String evnKey = handoffService.getEnvironmentKey();

		claims.put("Page", page);
		claims.put("PageSet", pageset);
		claims.put("TargetPage", targetPage);
		claims.put("ManuscriptID", manuscriptID);
		claims.put("SystemID", "DSCI");
		claims.put("CLIENTID", clientId); 
		claims.put("QUOTEID", quoteId);
		claims.put("ENV", evnKey);
		
		JwtBuilder builder = Jwts.builder().setIssuedAt(now).setClaims(claims).signWith(signatureAlgorithm, signingKey);

		if (ttlMillis >= 0) {
			long expMillis = nowMillis + ttlMillis;
			Date exp = new Date(expMillis);
			builder.setExpiration(exp);
		}
		return builder.compact();
	}
}
