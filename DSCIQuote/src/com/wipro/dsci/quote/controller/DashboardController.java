package com.wipro.dsci.quote.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import static com.wipro.dsci.quote.service.DSCIService.debug;

@Controller
public class DashboardController {

	static Logger log = LoggerFactory.getLogger(CreateElementController.class.getName());
	
	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
	  public ModelAndView showLogin(HttpServletRequest request, HttpServletResponse response) {
	  debug("Rest request inside Dashboard");
		ModelAndView mav = new ModelAndView("index");
	    
	    return mav;
	  }
}
