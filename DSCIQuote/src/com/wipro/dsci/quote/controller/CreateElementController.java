package com.wipro.dsci.quote.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import com.wipro.dsci.quote.model.SessionData;
import com.wipro.dsci.quote.service.CreateElementService;
import static com.wipro.dsci.quote.service.DSCIService.debug;


@Controller
public class CreateElementController {
	
	
	
	@Autowired
	public CreateElementService createElementService;
	
	/**
	 * @param request
	 * @param response
	 * @param requestData
	 * @return String
	 * @throws Throwable 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@CrossOrigin
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public @ResponseBody Object createCall(HttpServletRequest request, HttpServletResponse response,
			@RequestBody LinkedMultiValueMap<String, Object> requestData) throws Throwable {
		
		debug("Request:in createCall method:"+requestData);
		createElementService = new CreateElementService();
		HttpSession session = request.getSession(true);
		if(requestData != null && !requestData.isEmpty()){
			if(session.getAttribute("sessionData") == null) {
				session.setAttribute("sessionData", new SessionData());
			}
			requestData.add("session", session);
			ResponseEntity<List> resposne = (ResponseEntity<List>)createElementService.callCreateService(requestData);
			//debug("Response:in createCall method:" + response);
			return resposne.getBody();
		}
		debug("Response:in createCall method:requestData is null or empty");
		return null;

	}
}
