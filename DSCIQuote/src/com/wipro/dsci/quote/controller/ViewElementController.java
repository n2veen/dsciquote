package com.wipro.dsci.quote.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.*;
import com.wipro.dsci.quote.model.SessionData;
import com.wipro.dsci.quote.service.ViewElementService;
import static com.wipro.dsci.quote.service.DSCIService.debug;

@Controller
public class ViewElementController {

	@Autowired
	public ViewElementService viewElementService;

	/**
	 * @param request
	 * @param response
	 * @param requestData
	 * @return ResponseEntity<List>
	 * @throws Throwable
	 */
	@CrossOrigin
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/view", method = RequestMethod.POST)
	public @ResponseBody Object createCall(HttpServletRequest request, HttpServletResponse response,
			@RequestBody LinkedMultiValueMap<String, Object> requestData) throws Throwable {

		debug("Request:in createCall method:" + requestData);
		viewElementService = new ViewElementService();

		HttpSession session = request.getSession(true);
		if (session.getAttribute("sessionData") == null) {
			session.setAttribute("sessionData", new SessionData());
		}
		requestData.add("session", session);
		if (requestData != null && !requestData.isEmpty()) {
			ResponseEntity<List> output = (ResponseEntity<List>) viewElementService.callViewService(requestData);
			//debug("Response:in createCall method:" + output.getBody());
			return output.getBody();
		}
		debug("Response:in createCall method:requestData is null or empty");
		return null;
	}
}
