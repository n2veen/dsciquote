package com.wipro.dsci.quote.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wipro.dsci.quote.exception.DSCIQuoteException;

@ControllerAdvice
public class GlobalExceptionHandler {

	static final Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);

	@ExceptionHandler(value = DSCIQuoteException.class)
	public ResponseEntity<Object> handleAllExceptions(DSCIQuoteException ex) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("errorMessage", "User Action failed");
		map.put("errorDescription", ex.getClass().getSimpleName().toString());
		return new ResponseEntity<Object>(map, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(value = HttpServerErrorException.class)
	public ResponseEntity<Object> handleHttpServerException(HttpServerErrorException exception)
			throws JsonParseException, JsonMappingException, IOException {
		Map<String, String> map = new HashMap<String, String>();
		if (exception.getResponseBodyAsString() != null && !exception.getResponseBodyAsString().isEmpty()) {
			Map<String, Object> errorMessage = new ObjectMapper().readValue(exception.getResponseBodyAsString(),
					new TypeReference<HashMap<String, Object>>() {
					});
			if (errorMessage.get("errorMessage") != null && errorMessage.get("errorCode") != null) {
				map.put("errorMessage", "User Action failed");
				map.put("errorDescription", (String) errorMessage.get("errorMessage"));
				return new ResponseEntity<Object>(map, HttpStatus.valueOf((Integer) errorMessage.get("errorCode")));
			}

		}
		map.put("errorMessage", "User Action failed");
		map.put("errorDescription", exception.getClass().getSimpleName().toString());
		return new ResponseEntity<Object>(map, HttpStatus.valueOf(exception.getStatusCode().value()));
	}

	@ExceptionHandler(value = HttpClientErrorException.class)
	public ResponseEntity<Object> handleHttpClientException(HttpClientErrorException exception)
			throws JsonParseException, JsonMappingException, IOException {
		Map<String, String> map = new HashMap<String, String>();
		if (exception.getResponseBodyAsString() != null && !exception.getResponseBodyAsString().isEmpty()) {
			Map<String, Object> errorMessage = new ObjectMapper().readValue(exception.getResponseBodyAsString(),
					new TypeReference<HashMap<String, Object>>() {
					});
			if (errorMessage.get("errorMessage") != null && errorMessage.get("errorCode") != null) {
				map.put("errorMessage", "User Action failed");
				map.put("errorDescription", (String) errorMessage.get("errorMessage"));
				return new ResponseEntity<Object>(map, HttpStatus.valueOf((Integer) errorMessage.get("errorCode")));
			}

		}
		map.put("errorMessage", "User Action failed");
		map.put("errorDescription", exception.getClass().getSimpleName().toString());
		return new ResponseEntity<Object>(map, HttpStatus.valueOf(exception.getStatusCode().value()));
	}

	/**
	 * 
	 * @param ex
	 * @return
	 */
	@ExceptionHandler(value = Throwable.class)
	public ResponseEntity<Object> handleAllExceptionsAndErrors(Throwable ex) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("errorMessage", "User Action failed");
		map.put("errorDescription", ex.getClass().getSimpleName().toString());
		return new ResponseEntity<Object>(map, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}