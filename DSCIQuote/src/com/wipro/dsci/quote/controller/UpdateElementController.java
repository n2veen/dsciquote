package com.wipro.dsci.quote.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.wipro.dsci.quote.service.UpdateElementService;
import static com.wipro.dsci.quote.service.DSCIService.debug;

@Controller
public class UpdateElementController {

	
	@Autowired
	UpdateElementService updateElementService;
	
	/**
	 * @param requestData
	 * @return ResponseEntity<String>
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	@PostMapping("/update")
	public ResponseEntity<String> update(@RequestBody LinkedMultiValueMap<String, Object> requestData) throws Throwable {

		debug("Request:in update method:"+requestData);

		if (requestData != null && !requestData.isEmpty()){
			ResponseEntity<String> response = (ResponseEntity<String>) updateElementService.callUpdateService(requestData);
			//debug("Response:in update method:"+response);
			return response;
		}
		debug("Response:in update method:request data is null or empty");
		return null;
	}
}





