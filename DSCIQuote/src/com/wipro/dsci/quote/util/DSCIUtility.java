package com.wipro.dsci.quote.util;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.wipro.dsci.quote.service.DSCIService.debug;

public class DSCIUtility {

    public static LinkedMultiValueMap<String, Object> updateRequestDataWithJWTToken(
            LinkedMultiValueMap<String, Object> requestData) throws RuntimeException {
        debug("Request:in updateRequestDataWithJWTToken method:" + requestData);
        String jwtToken = "";
        if (requestData != null) {
            ServletRequestAttributes requestAttribute = (ServletRequestAttributes) RequestContextHolder
                    .currentRequestAttributes();
            jwtToken = (String) requestAttribute.getRequest().getSession().getAttribute("JsonWebToken");

            List<Object> tokenList = new ArrayList<Object>();
            tokenList.add(jwtToken);
            requestData.put("jwtToken", tokenList);
        } else {
            requestData = new LinkedMultiValueMap<String, Object>();
            List<Object> tokenList = new ArrayList<Object>();
            tokenList.add(jwtToken);
            requestData.put("jwtToken", tokenList);
        }
        debug("Response:in updateRequestDataWithJWTToken method:" + requestData);
        return requestData;
    }
    
   

    public static HashMap<String, Object> updateRequestMapDataWithJWTToken(
            HashMap<String, Object> requestData) throws RuntimeException {
        debug("Request:in updateRequestDataWithJWTToken method:" + requestData);
        String jwtToken = "";
        if (requestData != null) {
            ServletRequestAttributes requestAttribute = (ServletRequestAttributes) RequestContextHolder
                    .currentRequestAttributes();
            jwtToken = (String) requestAttribute.getRequest().getSession().getAttribute("JsonWebToken");

            requestData.put("jwtToken", jwtToken);
        } else {
            requestData = new HashMap<String, Object>();
            requestData.put("jwtToken", jwtToken);
        }
        debug("Response:in updateRequestDataWithJWTToken method:" + requestData);
        return requestData;
    }
    
    public static HashMap<String, String> updateRequestStringMapDataWithJWTToken(
            HashMap<String, String> requestData) throws RuntimeException {
        debug("Request:in updateRequestDataWithJWTToken method:" + requestData);
        String jwtToken = "";
        if (requestData != null) {
            ServletRequestAttributes requestAttribute = (ServletRequestAttributes) RequestContextHolder
                    .currentRequestAttributes();
            jwtToken = (String) requestAttribute.getRequest().getSession().getAttribute("JsonWebToken");

            requestData.put("jwtToken", jwtToken);
        } else {
            requestData = new HashMap<String, String>();
            requestData.put("jwtToken", jwtToken);
        }
        debug("Response:in updateRequestStringMapDataWithJWTToken method:" + requestData);
        return requestData;
    }

}
