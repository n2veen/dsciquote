package com.wipro.dsci.quote.test;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.env.MockEnvironment;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wipro.dsci.quote.service.CreateElementService;
import com.wipro.dsci.quote.service.RiskService;


@RunWith(MockitoJUnitRunner.class)

public class RiskServiceTest {

	private MockMvc mockMvc;
	private MockRestServiceServer mockServer;

	@InjectMocks
	RiskService<?, ?> riskService;

	// @Mock
	RestTemplate restTemplate;
	
	@Autowired
	LinkedMultiValueMap<String, Object> obj;
	
	@Autowired
	List<String> result;
	
	@Before
	public void init() {
		mockMvc = MockMvcBuilders.standaloneSetup(riskService).build();
		restTemplate = riskService.getTemplate();
		mockServer = MockRestServiceServer.createServer(restTemplate);
	}

	@Test
	public void createCallMethodTest() throws Exception {
		obj = new LinkedMultiValueMap<String, Object>();
		obj.add("elementType", "RISK");
		obj.add("operationType", "ADDBUILDING");

		LinkedHashMap<String, String> map = new LinkedHashMap<>();
		map.put("riskDatasetName", "name1");
		map.put("riskID", "1091");
		map.put("typeSeqID", "101");
		map.put("controlTypeID", "1");
		map.put("valueSeqID", "123");
		map.put("validationSeqID", "101");
		map.put("navigationID", "22");
		map.put("associationID", "44");
		map.put("additionalInfo", "check");
		map.put("defaultValue", "value");
		map.put("parentDatasetID", "1");
		obj.add("model", map);

		String tempurl = "http://dsciopsvm.eastus.cloudapp.azure.com:9096/create/";

		MockEnvironment environment = new MockEnvironment();

		environment.setProperty("url.create", tempurl);

		CreateElementService.setEnvironment(environment);

		ResponseEntity<String> expectedresponse = new ResponseEntity<String>("Success", HttpStatus.OK);
	
		mockServer.expect(requestTo(tempurl))
        .andExpect(method(HttpMethod.POST))
        .andRespond(withStatus(HttpStatus.OK).body("Success"));
		
		assertEquals(expectedresponse,riskService.createCall(restTemplate, obj));
	}
	
	@Test
	public void viewCallMethodTest() throws Exception {

		String url = "http://dsciopsvm.eastus.cloudapp.azure.com:9095/view/";

		// intializing objects
		obj= new LinkedMultiValueMap<>();
		obj.add("elementType", "RISK");
		obj.add("operationType", "VIEWRISKDATASET");

		result = new ArrayList<String>();
		result.add("test-data1");
		result.add("test-data2");

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");
		ResponseEntity<List> response = new ResponseEntity<List>(result, headers, HttpStatus.OK);

		// mocking environment variable
		MockEnvironment environment = new MockEnvironment();

		// setting the url values in the environment
		environment.setProperty("url.view", url);

		// added set environment to method to the viewelement service
		CreateElementService.setEnvironment(environment);

		// Mocking post call in the widget service
		mockServer.expect(requestTo(url)).andExpect(method(HttpMethod.POST))
				.andRespond(withStatus(HttpStatus.OK).body(toJson(result)).contentType(MediaType.APPLICATION_JSON));

		assertEquals(response, riskService.viewCall(restTemplate, obj));
	}
	
	public static String toJson(final Object obj) {
		try {
			final ObjectMapper mapper = new ObjectMapper();
			return mapper.writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
