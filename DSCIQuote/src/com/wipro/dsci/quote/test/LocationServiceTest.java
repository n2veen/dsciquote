package com.wipro.dsci.quote.test;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

import java.util.LinkedHashMap;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.env.MockEnvironment;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wipro.dsci.quote.service.CreateElementService;
import com.wipro.dsci.quote.service.LocationService;

@RunWith(MockitoJUnitRunner.class)

public class LocationServiceTest {

	private MockMvc mockMvc;
	private MockRestServiceServer mockServer;

	@InjectMocks
	LocationService<?, ?> locationService;

	// @Mock
	RestTemplate restTemplate;

	@Autowired
	LinkedMultiValueMap<String, Object> obj;

	@Autowired
	List<String> result;

	@Before
	public void init() {
		mockMvc = MockMvcBuilders.standaloneSetup(locationService).build();
		restTemplate = locationService.getTemplate();
		mockServer = MockRestServiceServer.createServer(restTemplate);
	}

	@Test
	public void updateCallMethodTest() throws Exception {
		obj = new LinkedMultiValueMap<String, Object>();
		obj.add("elementType", "LOCATION");
		obj.add("operationType", "UPDATE_LOCATION");
		obj.add("locationNumber", 123);

		LinkedHashMap<String, String> model = new LinkedHashMap<>();
		model.put("policyID", "123");
		model.put("bcDatasetID", "123");
		model.put("bcDatasetName", "bz");
		model.put("validationSeqID", "123");
		model.put("typeSeqID", "123");
		model.put("controlTypeID", "123");
		model.put("valueSeqID", "123");
		model.put("associationID", "113");
		model.put("navigationID", "bz");
		model.put("additionalInfo", "bz");
		model.put("defaultValue", "bz");
		model.put("parentDatasetID", "113");
		obj.add("model", model);

		String tempurl = "http://dsciopsvm.eastus.cloudapp.azure.com:9097/update/";

		MockEnvironment environment = new MockEnvironment();

		environment.setProperty("url.update", tempurl);

		CreateElementService.setEnvironment(environment);

		ResponseEntity<String> expectedresponse = new ResponseEntity<String>("Success", HttpStatus.OK);

		mockServer.expect(requestTo(tempurl)).andExpect(method(HttpMethod.POST))
				.andRespond(withStatus(HttpStatus.OK).body("Success"));

		assertEquals(expectedresponse, locationService.updateCall(restTemplate, obj));
	}

	@Test
	public void saveOccupancyForNoMultipleOccupancy() throws Exception {
		obj = new LinkedMultiValueMap<String, Object>();
		obj.add("applicantInterest", "Tenant");
		obj.add("multipleOccupancy", "No");

		String tempurl = "https://wiprocasemgmt.appiancloud.com/suite/webapi/validatebuildingdetails";
		MockEnvironment environment = new MockEnvironment();

		environment.setProperty("url.saveOccupancyDetails", tempurl);

		CreateElementService.setEnvironment(environment);

		ResponseEntity<String> expectedresponse = new ResponseEntity<String>("Located_in_a_self_enclosed_mall:hide",
				HttpStatus.OK);

		mockServer.expect(requestTo(tempurl + "?var1=Tenant&var2=No")).andExpect(method(HttpMethod.GET))
				.andRespond(withStatus(HttpStatus.OK).body(("Located_in_a_self_enclosed_mall:hide")));

		assertEquals(expectedresponse, locationService.saveOccupancyDetailsInAppian(restTemplate, obj));
	}

	@Test
	public void saveOccupancyForMultipleOccupancy() throws Exception {
		obj = new LinkedMultiValueMap<String, Object>();
		obj.add("applicantInterest", "Lessor's Risk Only");
		obj.add("multipleOccupancy", "Yes");

		String tempurl = "https://wiprocasemgmt.appiancloud.com/suite/webapi/validatebuildingdetails";
		MockEnvironment environment = new MockEnvironment();

		environment.setProperty("url.saveOccupancyDetails", tempurl);

		CreateElementService.setEnvironment(environment);

		ResponseEntity<String> expectedresponse = new ResponseEntity<String>("Is_this_occupant_the_applicant:yes",
				HttpStatus.OK);

		mockServer.expect(requestTo(tempurl + "?var1=Lessor's%20Risk%20Only&var2=Yes"))
				.andExpect(method(HttpMethod.GET))
				.andRespond(withStatus(HttpStatus.OK).body(("Is_this_occupant_the_applicant:yes")));

		assertEquals(expectedresponse, locationService.saveOccupancyDetailsInAppian(restTemplate, obj));
	}

	public static String toJson(final Object obj) {
		try {
			final ObjectMapper mapper = new ObjectMapper();
			return mapper.writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}