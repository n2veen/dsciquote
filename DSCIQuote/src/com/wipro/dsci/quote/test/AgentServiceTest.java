package com.wipro.dsci.quote.test;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.env.MockEnvironment;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wipro.dsci.quote.service.AgentService;
import com.wipro.dsci.quote.service.CreateElementService;


@RunWith(MockitoJUnitRunner.class)

public class AgentServiceTest {

	private MockMvc mockMvc;
	private MockRestServiceServer mockServer;

	@InjectMocks
	AgentService<?, ?> agentService;

	// @Mock
	RestTemplate restTemplate;
	
	@Autowired
	LinkedMultiValueMap<String, Object> obj;
	
	@Autowired
	List<String> result;
	
	@Before
	public void init() {
		mockMvc = MockMvcBuilders.standaloneSetup(agentService).build();
		restTemplate = agentService.getTemplate();
		mockServer = MockRestServiceServer.createServer(restTemplate);
	}
	
	@Test
	public void viewApCodeTest() throws Exception {

		String url = "http://dsciopsvm.eastus.cloudapp.azure.com:9890/view/";

		// intializing objects
		obj= new LinkedMultiValueMap<>();
		obj.add("elementType", "ISDSCIAGENT");
		obj.add("operationType", "VIEWAPCODE");

		result = new ArrayList<String>();
		result.add("test-data1");
		result.add("test-data2");

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");
		ResponseEntity<List> response = new ResponseEntity<List>(result, headers, HttpStatus.OK);

		// mocking environment variable
		MockEnvironment environment = new MockEnvironment();

		// setting the url values in the environment
		environment.setProperty("isdsciagent.url.view", url);

		// added set environment to method to the viewelement service
		CreateElementService.setEnvironment(environment);

		// Mocking post call in the widget service
		mockServer.expect(requestTo(url)).andExpect(method(HttpMethod.POST))
				.andRespond(withStatus(HttpStatus.OK).body(toJson(result)).contentType(MediaType.APPLICATION_JSON));

		assertEquals(response, agentService.viewApCode(restTemplate, obj));
	}
	
	public static String toJson(final Object obj) {
		try {
			final ObjectMapper mapper = new ObjectMapper();
			return mapper.writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
