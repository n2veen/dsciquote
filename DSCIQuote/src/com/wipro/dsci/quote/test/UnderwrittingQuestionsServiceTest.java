package com.wipro.dsci.quote.service;

import static org.junit.Assert.*;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.env.MockEnvironment;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wipro.dsci.quote.exception.DSCIQuoteException;

@RunWith(MockitoJUnitRunner.class)
public class UnderwrittingQuestionsServiceTest {
	
	private MockMvc mockMvc;
	private List<String> list;
	private MockRestServiceServer mockServer;
	private String url;
	private ResponseEntity<String> response;
	private MockEnvironment environment;
	private String quoteSection = "5";
	private String policyID = "1001";
	private String result;
	private HttpHeaders headers;
	private LinkedHashMap<String, String> map;

	@InjectMocks
	private UnderwritingQuestionsService underwritingQuestionsService;
	
	@Autowired
	LinkedMultiValueMap<String, Object> input;
	
	@Mock
	RestTemplate restTemplateMock;
	
	@Before
	public void init() {
		headers = new HttpHeaders();
		
		//mocking environment variable
		environment = new MockEnvironment();
		
		mockMvc = MockMvcBuilders.standaloneSetup(underwritingQuestionsService).build();
		restTemplateMock = new RestTemplate();
		mockServer = MockRestServiceServer.createServer(restTemplateMock);
	}
	
	@Test
	public void testViewUnderwritingQuestions_BasicPositiveScenario() {
		
		url = "http://localhost:8789/DSCIQuote/view";

		input = new LinkedMultiValueMap<String, Object>();
		input.add("elementType", "UNDERWRITINGQUESTIONS");
		input.add("operationType", "VIEW_UNDERWRITING_QUESTIONS");

		list = new ArrayList<>();

	    //headers.add("Content-Type", "application/json");
		response = new ResponseEntity<String>("Test Body",headers,HttpStatus.OK);
		//setting the url values in the environment
		environment.setProperty("url.getQuestion",url);
		
		url += "?quote_section=" + quoteSection  + "&policy_id=9876543";
	  
		//added set environment to method to the viewelement service 
		ViewElementService.setEnvironment(environment);
						
		//Mocking post call in the widget service
		mockServer.expect(requestTo(url)).andExpect(method(HttpMethod.POST)).
		andRespond(withStatus(HttpStatus.OK).body("Test Body").contentType(MediaType.APPLICATION_JSON));
		
		assertEquals(response,underwritingQuestionsService.viewUnderwritingQuestions(restTemplateMock, input));	

	}
	
	@Test
	public void testSaveUnderwritingQuestionsInAppian() {

		map = new LinkedHashMap<>();
		input = new LinkedMultiValueMap<String, Object>();
		
		// preparing request data
		input.add("elementType", "UNDERWRITINGQUESTIONS");
		input.add("operationType", "SAVE_UW_QUESTIONS_IN_APPIAN");
		
		map.put("sample1", "sample1");
		map.put("sample2", "sample2");
		map.put("sample3", "sample3");
		
		input.add("data", map);

		url = "http://localhost:8789/DSCIQuote/create";
		
		headers.add("Content-Type", "application/json");
		ResponseEntity<String> res = new ResponseEntity<String>("Success", headers, HttpStatus.OK);

		environment.setProperty("url.saveQuoteDetails",url);
		url += "?policy_id=" + policyID +  "&quote_section==" + quoteSection;

		ViewElementService.setEnvironment(environment);

		mockServer.expect(requestTo(url)).andExpect(method(HttpMethod.POST))
		.andRespond(withStatus(HttpStatus.OK).body("Success").contentType(MediaType.APPLICATION_JSON));

		assertEquals(res, underwritingQuestionsService.saveUnderwritingQuestionsInAppian(restTemplateMock, input));

	}

}
