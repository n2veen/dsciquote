package com.wipro.dsci.quote.enums;


/**
 * Enum to maintain Appian Quote Section 
 * 
 * @author VI951789
 *
 */
public enum QuoteSection {

	BUSINESS_DETAILS(2), LOB(3);

	private Integer pageNumber;

	QuoteSection(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Integer getValue() {
		return pageNumber;
	}

}
