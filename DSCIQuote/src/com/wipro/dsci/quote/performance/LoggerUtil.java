package com.wipro.dsci.quote.performance;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * LoggerUtil takes care of the various levels of logging -error,debug the
 * methods of the class are called to log the object or log the object and the
 * corresponding message for the debug and the error levels of logger
 * 
 */
public final class LoggerUtil {
	// private static final Object nullObject = null;
	// initiate log object of this class to handle null objects of
	// other classes
//	private static final Log LOGGER = LogFactory.getLog(LoggerUtil.class);
	static Logger LOGGER = LoggerFactory.getLogger(LoggerUtil.class.getName());

	public static boolean displayException = true;

	/**
	 * Prevents public creation of Logger class.
	 */
	private LoggerUtil() {
	};

	public static boolean isDebugEnabled(String path) {
		if (path != null) {
			Logger log = LoggerFactory.getLogger(path);
			return log.isDebugEnabled();
		}
		return false;
	}

	/**
	 * handles the Logged object and the corresponding message for the error
	 * level of logger
	 * 
	 * @param logObject
	 * @param message
	 * 
	 */
	public static void debug(Object logObject, String message) {
		Object nullObject = null;
		if (LOGGER.isDebugEnabled()) {
			// Log the error message
			LOGGER.debug(message + ":" + (logObject != nullObject ? logObject.toString() : "null"));
		}
	}

	/**
	 * handles the Logged object and the corresponding message for the info
	 * level of logger
	 * 
	 * @param message
	 * 
	 */
	public static void info(Logger log, String message) {
		if (log.isInfoEnabled()) {
			// Log the error message
			log.info(message);
		}
	}

	/**
	 * handles the Logged object and the corresponding message for the error
	 * level of logger
	 * 
	 * @param logObject
	 * @param message
	 * @param Log
	 *            object of the corresponding class
	 */
	public static void debug(Object logObject, String message, Logger log) {
		Object nullObject = null;
		if (log != null && log.isDebugEnabled()) {
			// Log the error message
			log.debug(message + ":" + (logObject != nullObject ? logObject.toString() : "null"));
		} else if (log == null && LOGGER.isDebugEnabled()) {
			LOGGER.debug(message + ":" + (logObject != nullObject ? logObject.toString() : "null"));
		}
	}




	/**
	 * handles the Logged object and the corresponding message for the error
	 * level of logger
	 * 
	 * @param logObject
	 * @param Log
	 *            object of the corresponding class
	 */
	public static void debug(Object logObject, Logger log) {
		Object nullObject = null;
		if (log != null && log.isDebugEnabled()) {
			// Log the error message
			log.debug(logObject != nullObject ? logObject.toString() : "null");
		} else if (log == null && LOGGER.isDebugEnabled()) {
			LOGGER.debug(logObject != nullObject ? logObject.toString() : "null");
		}
	}

}