package com.wipro.dsci.quote.performance;

import java.net.URI;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;



/**
 * @author SREG
 *
 */
@Aspect
@Component
public class MethodExecutionCalculationAspect {

	static Logger logger = LoggerFactory.getLogger(MethodExecutionCalculationAspect.class.getName());

	@Around("execution(* com.wipro.dsci.quote.controller.*.*(..))")
	public Object logExecutionTime(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
		long startTime = System.currentTimeMillis();
	 
	    Object value = null;
	    
	    try {
            value = proceedingJoinPoint.proceed();
        } catch (Throwable throwable) {
            throw throwable;
        } finally {
        	String methodName = proceedingJoinPoint.getSignature().getDeclaringType().getSimpleName() +"."+
                    proceedingJoinPoint.getSignature().getName();
            TimingCollector.stopTimerAndReportResults(startTime, this.getClass().getName(), methodName, "1. DSCIQuote - E2E Controller timing");
        }
	    return value;
	}
	
	@Around("execution(* org.springframework.web.client.RestTemplate.*(..))")
	public Object logRestCallTime(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
		long startTime = System.currentTimeMillis();
	    Object value = null;
	    String methodName = null;
	    
	    try {
            value = proceedingJoinPoint.proceed();
        } catch (Throwable throwable) {
            throw throwable;
        } finally {
        	Object[] args = proceedingJoinPoint.getArgs();
        	if (args != null && args.length > 0 && (args[0] instanceof String || args[0] instanceof URI)) {
        		methodName = trim(args[0].toString());
        		TimingCollector.stopTimerAndReportResults(startTime, this.getClass().getName(), methodName, "2. DSCIQuote - REST APIs called from DSCI Quote");
        	}        	
        }
	    return value;
	}
	
	private String trim(String url) {
		int endIndex = url.indexOf("?");
		if (endIndex > 0) {
			url = url.substring(0, endIndex);
		}
		return url;
	}
}