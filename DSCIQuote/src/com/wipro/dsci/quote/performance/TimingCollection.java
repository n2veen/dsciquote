package com.wipro.dsci.quote.performance;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

public class TimingCollection {
	private Map<String, Map<String, TimingData>> timingData = new Hashtable<String, Map<String, TimingData>>();

	private Date lastResetDateTime = new Date();

	public Map<String, Map<String, TimingData>> getTimingData() {
		return timingData;
	}

	public void setTimingData(Map<String, Map<String, TimingData>> timingData) {
		this.timingData = timingData;
	}

	public Date getLastResetDateTime() {
		return lastResetDateTime;
	}

	public String getFormattedLastResetDateTime() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.dd 'at' HH:mm:ss.S z");
		return format.format(lastResetDateTime);
	}

	public void setLastResetDateTime(Date lastResetDateTime) {
		this.lastResetDateTime = lastResetDateTime;
	}
	
	public void reset() {
		timingData.clear();
		lastResetDateTime = new Date();
	}

}
