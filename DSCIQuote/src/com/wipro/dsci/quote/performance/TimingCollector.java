package com.wipro.dsci.quote.performance;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 
 * This class holds the timing statistics for various events
 */
public class TimingCollector {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TimingCollector.class);

	private static TimingCollection timingCollection = new TimingCollection();

	public static void stopTimerAndReportResults(final long startTime, final String className, final String msg,
			String category) {
		if (LOGGER.isInfoEnabled() || Audit.isMetricsEnabled()) {
			
			long endTime = System.currentTimeMillis();
			long duration = endTime - startTime;
			if (LOGGER.isDebugEnabled()) {
				StringBuffer buf = new StringBuffer(className).append(": ").append(category)
						.append(": ").append(createLogEntry(msg, startTime, endTime, duration));
				Audit.metrics(buf.toString());
			}
			addTiming(msg, category, duration);
		}
	}
	
	
	public static TimingCollection retrieveTimingCollection() {
		TimingCollection newCollection = new TimingCollection();
		newCollection.setTimingData(cloneTiming());
		newCollection.setLastResetDateTime(timingCollection.getLastResetDateTime());
		return newCollection;
	}


	public static void clearTiming() {
		timingCollection.reset();
	}


	private static Map<String, Map<String, TimingData>> cloneTiming() {
		TreeMap<String, Map<String, TimingData>> timingDataClone = new TreeMap<String, Map<String, TimingData>>();

		for (Entry<String, Map<String, TimingData>> entry : timingCollection.getTimingData().entrySet()) {
			String key = entry.getKey();
			Map<String, TimingData> value = entry.getValue();

			TreeMap<String, TimingData> treeMap = new TreeMap<String, TimingData>(String.CASE_INSENSITIVE_ORDER);
			treeMap.putAll(value);

			timingDataClone.put(key, treeMap);
		}

		return timingDataClone;
	}


	private static void addTiming(String event, String eventType, long duration) {
		Map<String, TimingData> eventTiming = timingCollection.getTimingData().get(eventType);
		if (eventTiming != null) {
			TimingData data = eventTiming.get(event);
			if (data != null) {
				data.addTiming(duration);
			} else {
				data = new TimingData();
				data.addTiming(duration);
				eventTiming.put(event, data);
			}
		} else {
			eventTiming = new Hashtable<String, TimingData>();
			TimingData data = new TimingData();
			data.addTiming(duration);
			eventTiming.put(event, data);
			timingCollection.getTimingData().put(eventType, eventTiming);
		}

	}
	
	
	private static String createLogEntry(String name, long begin, long end, long duration) {
		StringBuffer strBuff = new StringBuffer();

		strBuff.append("Timing {");

		if (name != null) {
			strBuff.append(" name=").append(name);
		}

		if (begin > 0) {
			strBuff.append(" begin=").append(formatDatemilliToString(begin));
		}

		if (end > 0) {
			strBuff.append(" end=").append(formatDatemilliToString(end));
		}

		if (duration >= 0) {
			strBuff.append(" duration=").append(duration);
		} else if (begin > 0) {
			strBuff.append("** still running **");
		} else {
			strBuff.append("** not started **");
		}

		strBuff.append(" } |").append(duration);

		return strBuff.toString();
	}
	
	  private static String formatDatemilliToString(long millis) {

	        long hrs = TimeUnit.MILLISECONDS.toHours(millis) % 24;
	        long min = TimeUnit.MILLISECONDS.toMinutes(millis) % 60;
	        long sec = TimeUnit.MILLISECONDS.toSeconds(millis) % 60;
	        long mls = millis % 1000;
	        String toRet = String.format("%02d:%02d:%02d:%03d", hrs, min, sec, mls);
	        return toRet;
	    }
}