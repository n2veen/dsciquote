package com.wipro.dsci.quote.performance;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Metrics contains a static logger used to gather application METRICS.
 */
public final class Audit {
	/** logger for general audit information. */
	private static final Logger LOGGER = LoggerFactory.getLogger(Audit.class);

	/** logger for METRICS audit information. */
	private static final Logger METRICS = LoggerFactory.getLogger("METRICS");

	private Audit() {
	}

	/**
	 * Record general audit information.
	 * 
	 * @param message
	 *            for audit LOGGER
	 */
	public static void audit(final String message) {
		LoggerUtil.info(LOGGER, message);
	}

	/**
	 * Determine if Audit logging is enabled.
	 * 
	 * @return true if audit logging is enabled
	 */
	public static boolean isAuditEnabled() {
		return LOGGER.isInfoEnabled();
	}

	/**
	 * Record METRICS audit information.
	 * 
	 * @param message
	 *            for METRICS LOGGER
	 */
	public static void metrics(final String message) {
		METRICS.info(message);
	}

	/**
	 * Determine if Metric logging is enabled.
	 * 
	 * @return true if metric logging is enabled
	 */
	public static boolean isMetricsEnabled() {
		return METRICS.isInfoEnabled();
	}
}