package com.wipro.dsci.quote.performance;

/**
 * This class holds the individual timings for an event
 */
public class TimingData {
	private long minValue = 999999999999999999L;

	private long maxValue;

	private long numOfInvocations;

	private long total;

	public void addTiming(long duration) {
		numOfInvocations++;
		total = total + duration;
		if (duration < minValue) {
			minValue = duration;
		}
		if (duration > maxValue) {
			maxValue = duration;
		}
	}

	public long getAverage() {
		return total / numOfInvocations;
	}

	/**
	 * @return Returns the maxValue.
	 */
	public long getMaxValue() {
		return maxValue;
	}

	/**
	 * @return Returns the minValue.
	 */
	public long getMinValue() {
		return minValue;
	}

	/**
	 * @return Returns the numOfInvocations.
	 */
	public long getNumOfInvocations() {
		return numOfInvocations;
	}

	/**
	 * @return Returns the total.
	 */
	public long getTotal() {
		return total;
	}
}