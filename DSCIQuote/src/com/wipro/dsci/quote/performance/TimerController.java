package com.wipro.dsci.quote.performance;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TimerController {
	
	@Autowired
	private Environment environment;
	
	private String remoteAPIURL;


	@RequestMapping("/timer")
	public ModelAndView displayTimer(HttpServletRequest request, HttpServletResponse response) {
		TimingCollection timingData = null;
		
		boolean isRemoteConnectionReqd = true;
		
		if ("false".equalsIgnoreCase(request.getParameter("combo")))
			isRemoteConnectionReqd = false;

		if ("reset".equalsIgnoreCase(request.getParameter("command"))) {
			timingData = resetTimer(isRemoteConnectionReqd);
		} else {
			timingData = retrieveLatestTimingData(isRemoteConnectionReqd);
		}
		request.setAttribute("timingData", timingData);

		ModelAndView mav = new ModelAndView("Timer");
		return mav;
	}

	private TimingCollection resetTimer(boolean isRemoteConnectionReqd) {
		if (isRemoteConnectionReqd)
			resetRemoteTimer();
		TimingCollector.clearTiming();
		return TimingCollector.retrieveTimingCollection();
	}

	private TimingCollection retrieveLatestTimingData(boolean isRemoteConnectionReqd) {
		TimingCollection resultTiming = null;
		if (isRemoteConnectionReqd) {
			TimingCollection remoteTiming = getRemoteTimingCollection();
			resultTiming = TimingCollector.retrieveTimingCollection();
	
			if (remoteTiming != null && remoteTiming.getTimingData() != null) {
				resultTiming.getTimingData().putAll(remoteTiming.getTimingData());
			}

		} else {
			resultTiming = TimingCollector.retrieveTimingCollection();
		}
		return resultTiming;
	}

	private TimingCollection getRemoteTimingCollection() {
		TimingCollection timingData = null;

		try {
			String url = getRemoteAPIURL();
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<TimingCollection> response = restTemplate.getForEntity(url, TimingCollection.class);
			timingData = response.getBody();
		} catch (Exception exp) {
			logMessage("Unable to retrive Timing data from Remote Application", exp);
		}
		return timingData;
	}

	private void resetRemoteTimer() {
		
		try {
			String url = getRemoteAPIURL();
			MultiValueMap<String, String> requestParam = new LinkedMultiValueMap<String, String>();
			requestParam.add("command", "reset");
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> response = restTemplate.postForEntity(url, requestParam, String.class);
		} catch (Exception exp) {
			logMessage("Unable to reset Timer in Remote Application", exp);
		}
	}
	
	private String getRemoteAPIURL() {
		if (remoteAPIURL == null) {
			try {
				String accountURL = environment.getProperty("url.account");
				System.out.println("Environment entry " + accountURL);
				if (accountURL != null) {
					remoteAPIURL = accountURL.substring(0, accountURL.indexOf("/account")) + "timer";
				}
			} catch (Exception exp) {
				logMessage("Unable to retrieve URL of Remote Application", exp);
			}
		}
		return remoteAPIURL;
	}
		
	private void logMessage(String message, Exception exp) {
		Logger logger = LoggerFactory.getLogger("TimerController");
		if (logger.isDebugEnabled())
			logger.debug(message, exp);
	}
	
	
}
