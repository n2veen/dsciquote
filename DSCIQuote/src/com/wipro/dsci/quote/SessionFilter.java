package com.wipro.dsci.quote;

import org.springframework.http.HttpHeaders;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class SessionFilter {
    private ArrayList<String> urlList;

    /**
     * Default constructor.
     */
    public SessionFilter() {
        // TODO Auto-generated constructor stub
    }

    /**
     * @see Filter#destroy()
     */
    public void destroy() {
        // TODO Auto-generated method stub
    }

    /**
     * @see Filter#doFilter(ServletReques, ServletResponse, FilterChain)
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        String url = req.getServletPath();
        boolean allowedRequest = false;

        if (urlList!=null && !urlList.isEmpty()) {

            if (urlList.contains(url)) {
                allowedRequest = true;
            }

            if (!allowedRequest) {
                HttpSession session = req.getSession(true);
                String jsonWebToken=(String) session.getAttribute("JsonWebToken");
                if(req.getHeader(HttpHeaders.AUTHORIZATION) != null){
                    chain.doFilter(request, response);
                }else if(isJWTPresentinCookie(req)){
                    chain.doFilter(request, response);
                }else if(jsonWebToken!=null)
                {
                    chain.doFilter(request, response);
                }else {
                    RequestDispatcher dispatch =req.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
                    dispatch.forward(req, res);
                }
            }
        }else
        {
            chain.doFilter(request, response);
        }
        chain.doFilter(request, response);
    }


    /**
     * @see Filter#init(FilterConfig)
     */
    public void init(FilterConfig fConfig) throws ServletException {
        String urls = fConfig.getInitParameter("avoid-urls");

        if (urls!=null) {
            StringTokenizer token = new StringTokenizer(urls, ",");
            urlList = new ArrayList<String>();
            if (!urlList.isEmpty()) {
                while (token.hasMoreTokens()) {
                    urlList.add(token.nextToken());

                }

            }
        }else
        {
            urlList = new ArrayList<String>();
            urlList.add("/login.jsp");
            urlList.add("/userAuthentication");
            urlList.add("/login");
        }
    }

    public boolean isJWTPresentinCookie(HttpServletRequest req) {
        Cookie cookie = null;
        Cookie[] cookies = null;

        // Get an array of Cookies associated with this domain
        cookies = req.getCookies();

        if (cookies != null) {
            for (int i = 0; i < cookies.length; i++) {
                cookie = cookies[i];
                if (cookie.getName().equalsIgnoreCase("Authorization") && cookie.getValue() != null) {
                    return true;
                }

            }
            return false;
        }
        return false;
    }

}
