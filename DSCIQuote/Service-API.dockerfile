# Pull base image
From tomcat:8-jre8

# Copy to images tomcat
RUN rm -rf /usr/local/tomcat/webapps/ROOT

ARG arg

ADD $arg   /usr/local/tomcat/webapps/



EXPOSE 8080

# And run tomcat
CMD ["/usr/local/tomcat/bin/catalina.sh", "run"]
